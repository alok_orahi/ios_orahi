//
//  AppDelegate.swift
//  Orahi
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit
import CoreData
import Branch
import UserNotifications
import FacebookCore
import FBNotifications
import FBSDKCoreKit

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate, NotificationServiceDelegate,UNUserNotificationCenterDelegate {
    var window: UIWindow?
    var dialogue : QBChatDialog?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
       /**
         The first thing which needs to be called when the app starts is
         Acf().prepareForStartUp
        */
        Acf().prepareForStartUp(application,didFinishLaunchingWithOptions: launchOptions)
        self.setupForNotifications()
        if isUserLoggedIn(){
            if isNotNull(launchOptions){
                if let url = launchOptions?[UIApplicationLaunchOptionsKey.url] as? URL{
                    handleDeepLinking(url)
                }
                if isNotNull(launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification]){
                    let remoteNotification: NSDictionary! = launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] as? NSDictionary
                    logMessage("didFinishLaunchingWithOptions")
                    logMessage("userInfo \(remoteNotification)")
                    if let dialogID = remoteNotification["dialog_id"] as? String {
                        //Quick Blox ==> Start
                        ServicesManager.instance().notificationService?.pushDialogID = dialogID
                        execMain({[weak self] in guard let `self` = self else { return }

                            ServicesManager.instance().notificationService?.handlePushNotificationWithDelegate(self)
                        })
                        //Quick Blox <== End
                    }
                    self.handleNotification(userInfo: remoteNotification as! [AnyHashable : Any])
                }
            }
        }
        UIApplication.shared.applicationIconBadgeNumber = 0
        //to make sure app runs in background for few more seconds after getting launched in background
        Lem().doRequiredProcessing()
        Lem().startLocationUpdateWithHighAccuracyForFewSeconds()
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        Lem().doRequiredProcessing()
        Acf().updateBadgeValues()
        Dbm().saveChanges()
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        Acf().updateBadgeValues()
        Acf().connectWithQuickBlox()
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        Lem().doRequiredProcessing()
        Acf().checkForAnyPendingEventsOnGroupsAndSendItAgain()
        AppEventsLogger.activate(application)
        if let date = CacheManager.sharedInstance.loadObject(KEY_LAST_UPDATE_OF_SYNC_GROUPS_AND_PLACES) as? Date {
            if date.isEarlierThanDate(Date().dateBySubtractingMinutes(AUTO_SYNC_MINIMUM_POM_SYNC_TIME_INTERVAL)){
                Acf().syncGroupsAndPlaces{}
                Acf().locationCache.removeAllObjects()
            }
        }
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        func checkForDeeplinking()->Bool{
             if userActivity.webpageURL != nil {
                return Dlm().handleDeeplinkUrl(url: userActivity.webpageURL!)
            }
            return false
        }
        var areWeHandling = false
        if !areWeHandling {
            areWeHandling = Branch.getInstance().continue(userActivity)
        }else{
            Branch.getInstance().continue(userActivity)
        }
        Acf().checkForReferals()
        if !areWeHandling {
            areWeHandling = checkForDeeplinking()
        }else{
            checkForDeeplinking()
        }
        return areWeHandling
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        handleDeepLinking(url)
        Branch.getInstance().handleDeepLink(url)
        Dlm().handleDeeplinkUrl(url: url)
        return true
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        UserDefaults.standard.synchronize()
        Dbm().saveChanges()
    }

    //MARK: - APNS setup and delegate methods
    func setupForNotifications(){
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            UNUserNotificationCenter.current().requestAuthorization(options: [.sound,.alert,.badge]) { (granted, error) in
                if error == nil {
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
        }else {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Swift.Void){
        Lem().doRequiredProcessing()
        completionHandler(.newData)
    }
    
    func application(_ application: UIApplication,didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        setDeviceToken(deviceTokenUsingData(deviceToken))
        Acf().deviceToken = deviceToken
        Acf().managePushNotificationsOnQuickBlox()
        FBSDKAppEvents.setPushNotificationsDeviceToken(deviceToken)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        handleNotification(userInfo: response.notification.request.content.userInfo)
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Swift.Void){
        if (application.applicationState == .inactive || application.applicationState == .background){
            handleNotification(userInfo: userInfo)
        }else{
            handleNotification(userInfo: userInfo,onlyShowBanner:true)
        }
    }
    
    func handleNotification(userInfo: [AnyHashable : Any],onlyShowBanner:Bool=false) {
        FBNotificationsManager.shared().presentPushCard(forRemoteNotificationPayload: userInfo, from: nil) { viewController, error in }
        Acf().handleNotification(userInfo as NSDictionary?,onlyShowBanner: onlyShowBanner)
        //Quick Blox Start  ==>
        if UIApplication.shared.applicationState == .inactive {
            if isUserLoggedIn(){
                if let dialogID = userInfo["dialog_id"] as? String {
                    if isNotNull(dialogID) {
                        let dialogWithIDWasEntered: String? = ServicesManager.instance().currentDialogID
                        if dialogWithIDWasEntered == dialogID { return }
                        ServicesManager.instance().notificationService?.pushDialogID = dialogID
                        execMain({[weak self] in guard let `self` = self else { return }

                            ServicesManager.instance().notificationService?.handlePushNotificationWithDelegate(self)
                        })
                    }
                }
            }
        }
        //Quick Blox End  <==
    }

    // MARK: Quick Blox : NotificationServiceDelegate protocol
    
    func notificationServiceDidStartLoadingDialogFromServer() {
    }
    
    func notificationServiceDidFinishLoadingDialogFromServer() {
    }
    
    func notificationServiceDidSucceedFetchingDialog(_ chatDialog: QBChatDialog!) {
        dialogue = chatDialog
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(AppDelegate.openFetchedDialogue), object: nil)
        self.perform(#selector(AppDelegate.openFetchedDialogue), with: nil, afterDelay: 1)
    }
    
    func openFetchedDialogue(){
        if dialogue != nil {
            Acf().showChatScreen(dialogue!)
        }
    }
    
    func notificationServiceDidFailFetchingDialog() {
    }
    
    func applicationDidReceiveMemoryWarning(_ application: UIApplication) {
        SDImageCache.shared().clearMemory()
        SDImageCache.shared().clearDisk()
        SDImageCache.shared().cleanDisk()
        URLCache.shared.removeAllCachedResponses()
    }
}
