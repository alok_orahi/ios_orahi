//
//  DatabaseManager.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

//MARK: - DatabaseManager : This class handles communication of application with its database (Core Data).

import Foundation
import UIKit
import MagicalRecord
import CoreData

//MARK: - Completion block
typealias DMCompletionBlock = (_ returnedData :Any?) ->()

func Dbm()->DatabaseManager{
    return DatabaseManager.sharedInstance
}

class DatabaseManager: NSObject{
    
    static let sharedInstance : DatabaseManager = {
        let instance = DatabaseManager()
        return instance
    }()
    
    fileprivate override init() {
        
    }
    
    var completionBlock: DatabaseManager?
    var managedObjectContext : NSManagedObjectContext?
    
    func setupCoreDataDatabase() {
        let isDataBaseValid = checkAndValidateDatabase()
        MagicalRecord.setupCoreDataStack(withAutoMigratingSqliteStoreNamed: self.dbStore())
        #if DEBUG
            MagicalRecord.setLoggingLevel(MagicalRecordLoggingLevel.all)
        #else
            MagicalRecord.setLoggingLevel(MagicalRecordLoggingLevel.error)
        #endif
        self.managedObjectContext = NSManagedObjectContext.mr_default()
        if isDataBaseValid == false {
            resetDatabase()
        }
        self.managedObjectContext = NSManagedObjectContext.mr_default()
        importantInitialisations()
    }
    
    func importantInitialisations(){
        getSetting()
    }
    
    func checkAndValidateDatabase()->Bool{
        let savedBuildInformation = UserDefaults.standard.object(forKey: APP_UNIQUE_BUILD_IDENTIFIER) as? String
        if isNotNull(savedBuildInformation){
            if savedBuildInformation! == UIApplication.versionBuild(){
                // all is well , dont worry
            }else{
                // reset every thing , please
                checkAndAutoLoginIfRequired()
                deleteCompleteDatabaseFile()
                UserDefaults.standard.set(UIApplication.versionBuild(), forKey: APP_UNIQUE_BUILD_IDENTIFIER)
                return false
            }
        }else{
            checkAndAutoLoginIfRequired()
            deleteCompleteDatabaseFile()
            UserDefaults.standard.set(UIApplication.versionBuild(), forKey: APP_UNIQUE_BUILD_IDENTIFIER)
            return false
        }
        return true
    }

    func checkAndAutoLoginIfRequired() {
        if let cachedUserDetails = CacheManager.sharedInstance.loadObject(CACHED_USER_PROFILE_DATA) as? NSDictionary{
            showActivityIndicator("Initialising")
            execMain({[weak self] in guard let `self` = self else { return }

                if let settingsInfo = cachedUserDetails.object(forKey: "informationForSettings") as? NSDictionary {
                    Dbm().setSetting(settingsInfo)
                }
                if let profileInformation = cachedUserDetails.object(forKey: "profileInformation") as? NSMutableDictionary{
                    if let _ = profileInformation.object(forKey: "userId") as? String{
                        Dbm().setUserInfo(profileInformation)
                    }else{
                        if let picture = profileInformation.object(forKey: "picture") as? NSString{
                            let components = picture.components(separatedBy: "_")
                            if components.count > 0 {
                                if let userId = components[0] as? NSString{
                                    if userId.length > 0 && userId.intValue > 0{
                                        profileInformation.setObject(userId, forKey: "userId" as NSCopying)
                                        Dbm().setUserInfo(profileInformation)
                                    }
                                }
                            }
                        }
                    }
                }
                Acf().showHomeScreen()
                hideActivityIndicator()
            },delay:4)
        }
    }
    
    func deleteCompleteDatabaseFile() {
        let dbStore = self.dbStore()
        let url = NSPersistentStore.mr_url(forStoreName: dbStore)
        let walURL = url!.deletingPathExtension().appendingPathExtension("sqlite-wal")
        let shmURL = url!.deletingPathExtension().appendingPathExtension("sqlite-shm")
        var removeError: NSError?
        MagicalRecord.cleanUp()
        let deleteSuccess: Bool
        do {
            try FileManager.default.removeItem(at: url!)
            try FileManager.default.removeItem(at: walURL)
            try FileManager.default.removeItem(at: shmURL)
            deleteSuccess = true
        } catch let error as NSError {
            removeError = error
            deleteSuccess = false
        }
        if deleteSuccess {
            logMessage("database resetted successfully")
        } else {
            logMessage("An error has occured while deleting \(dbStore)")
            logMessage("Error description: \(removeError?.description)")
        }
    }
    
    func dbStore() -> String {
        return "\(self.bundleID()).sqlite"
    }
    
    func bundleID() -> String {
        return Bundle.main.bundleIdentifier!
    }
    
    
    /**
     * UserInfo
     */
    func getUserInfo()->(UserInfo?){
        let userInfo = UserInfo.mr_findFirst()
        if isNotNull(userInfo)&&isNull(userInfo?.userId){
            UserInfo.mr_truncateAll()
            saveChanges()
            return nil
        }
        return UserInfo.mr_findFirst()
    }
    func setUserInfo(_ info:NSDictionary){
        var object : UserInfo? = UserInfo.mr_findFirst()
        if isNull(object){
            object = UserInfo.mr_createEntity(in: managedObjectContext!)
        }
        let dataToSet = NSMutableDictionary()
        copyData(info, sourceKey: "id", destinationDictionary: dataToSet, destinationKey: "userId", methodName:#function)
        copyData(info, sourceKey: "userId", destinationDictionary: dataToSet, destinationKey: "userId", methodName:#function)
        copyData(info, sourceKey: "name", destinationDictionary: dataToSet, destinationKey: "name", methodName:#function)
        copyData(info, sourceKey: "email", destinationDictionary: dataToSet, destinationKey: "email", methodName:#function)
        copyData(info, sourceKey: "phone", destinationDictionary: dataToSet, destinationKey: "phone", methodName:#function)
        copyData(info, sourceKey: "isPassenger", destinationDictionary: dataToSet, destinationKey: "isPassenger", methodName:#function)
        copyData(info, sourceKey: "genderTforMaleFForFemale", destinationDictionary: dataToSet, destinationKey: "genderTforMaleFForFemale", methodName:#function)
        copyData(info, sourceKey: "destinationName", destinationDictionary: dataToSet, destinationKey: "destinationName", methodName:#function)
        copyData(info, sourceKey: "facebookId", destinationDictionary: dataToSet, destinationKey: "facebookId", methodName:#function)
        copyData(info, sourceKey: "picture", destinationDictionary: dataToSet, destinationKey: "picture", methodName:#function)
        copyData(info, sourceKey: "status", destinationDictionary: dataToSet, destinationKey: "status", methodName:#function)
        copyData(info, sourceKey: "kmsShared", destinationDictionary: dataToSet, destinationKey: "kmsShared", methodName:#function)
        copyData(info, sourceKey: "rank", destinationDictionary: dataToSet, destinationKey: "rank", methodName:#function)
        copyData(info, sourceKey: "rating", destinationDictionary: dataToSet, destinationKey: "rating", methodName:#function)
        copyData(info, sourceKey: "ridesDone", destinationDictionary: dataToSet, destinationKey: "ridesDone", methodName:#function)
        copyData(info, sourceKey: "trustShieldFactor1", destinationDictionary: dataToSet, destinationKey: "trustShieldFactor1", methodName:#function)
        copyData(info, sourceKey: "trustShieldFactor2", destinationDictionary: dataToSet, destinationKey: "trustShieldFactor2", methodName:#function)
        copyData(info, sourceKey: "trustShieldFactor3", destinationDictionary: dataToSet, destinationKey: "trustShieldFactor3", methodName:#function)
        copyData(info, sourceKey: "communityRankingPosition", destinationDictionary: dataToSet, destinationKey: "communityRankingPosition", methodName:#function)
        copyData(info, sourceKey: "refferalCode", destinationDictionary: dataToSet, destinationKey: "refferalCode", methodName:#function)
        
        if isNull(dataToSet.object(forKey: "userId")) && isUserLoggedIn() {
            copyData(loggedInUserId() , destinationDictionary: dataToSet, destinationKey: "userId", methodName:#function)
        }
        
        object?.setValuesForKeys((dataToSet as? [String:Any])!)
        saveChanges()
    }
    
    func removeUserInfo(){
        UserInfo.mr_truncateAll(in: managedObjectContext!)
        saveChanges()
    }
    
    /**
     * Setting
     */
    func setSetting(_ info:NSDictionary){
        var object : Setting? = Setting.mr_findFirst()
        if isNull(object){
            object = Setting.mr_createEntity(in: managedObjectContext!)
        }
        let dataToSet = NSMutableDictionary()
        
        copyData(info, sourceKey: "carRegistrationNo", destinationDictionary: dataToSet, destinationKey: "carRegistrationNo", methodName:#function)
        copyData(info, sourceKey: "carMake", destinationDictionary: dataToSet, destinationKey: "carMake", methodName:#function)
        copyData(info, sourceKey: "carModel", destinationDictionary: dataToSet, destinationKey: "carModel", methodName:#function)
        copyData(info, sourceKey: "distance", destinationDictionary: dataToSet, destinationKey: "distance", methodName:#function)
        
        
        copyData(info, sourceKey: "destinationTime", destinationDictionary: dataToSet, destinationKey: "destinationTime", methodName:#function)
        copyData(info, sourceKey: "destinationLocationLongitude", destinationDictionary: dataToSet, destinationKey: "destinationLocationLongitude", methodName:#function)
        copyData(info, sourceKey: "destinationLocationLatitude", destinationDictionary: dataToSet, destinationKey: "destinationLocationLatitude", methodName:#function)
        copyData(info, sourceKey: "destinationAddressPickUpPoint", destinationDictionary: dataToSet, destinationKey: "destinationAddressPickUpPoint", methodName:#function)
        copyData(info, sourceKey: "destinationAddress", destinationDictionary: dataToSet, destinationKey: "destinationAddress", methodName:#function)
        
        copyData(info, sourceKey: "homeTime", destinationDictionary: dataToSet, destinationKey: "homeTime", methodName:#function)
        copyData(info, sourceKey: "homeLocationLongitude", destinationDictionary: dataToSet, destinationKey: "homeLocationLongitude", methodName:#function)
        copyData(info, sourceKey: "homeLocationLatitude", destinationDictionary: dataToSet, destinationKey: "homeLocationLatitude", methodName:#function)
        copyData(info, sourceKey: "homeAddressPickUpPoint", destinationDictionary: dataToSet, destinationKey: "homeAddressPickUpPoint", methodName:#function)
        copyData(info, sourceKey: "homeAddress", destinationDictionary: dataToSet, destinationKey: "homeAddress", methodName:#function)
        
        
        copyData(info, sourceKey: "needToUpdateOnServer", destinationDictionary: dataToSet, destinationKey: "needToUpdateOnServer", methodName:#function)
        copyData(info, sourceKey: "notificationForMobileOption1", destinationDictionary: dataToSet, destinationKey: "notificationForMobileOption1", methodName:#function)
        copyData(info, sourceKey: "notificationForMobileOption2", destinationDictionary: dataToSet, destinationKey: "notificationForMobileOption2", methodName:#function)
        copyData(info, sourceKey: "notificationForMobileOption3", destinationDictionary: dataToSet, destinationKey: "notificationForMobileOption3", methodName:#function)
        copyData(info, sourceKey: "notificationForMobileOption4", destinationDictionary: dataToSet, destinationKey: "notificationForMobileOption4", methodName:#function)
        copyData(info, sourceKey: "notificationForMobileOption5", destinationDictionary: dataToSet, destinationKey: "notificationForMobileOption5", methodName:#function)
        copyData(info, sourceKey: "notificationForMobileOption6", destinationDictionary: dataToSet, destinationKey: "notificationForMobileOption6", methodName:#function)
        copyData(info, sourceKey: "notificationForMobileOption7", destinationDictionary: dataToSet, destinationKey: "notificationForMobileOption7", methodName:#function)
        copyData(info, sourceKey: "notificationForMobileOption8", destinationDictionary: dataToSet, destinationKey: "notificationForMobileOption8", methodName:#function)
        copyData(info, sourceKey: "notificationForMobileOption9", destinationDictionary: dataToSet, destinationKey: "notificationForMobileOption9", methodName:#function)
        copyData(info, sourceKey: "notificationForMobileOption10", destinationDictionary: dataToSet, destinationKey: "notificationForMobileOption10", methodName:#function)
        copyData(info, sourceKey: "notificationForMobileOption11", destinationDictionary: dataToSet, destinationKey: "notificationForMobileOption11", methodName:#function)
        copyData(info, sourceKey: "notificationForMobileOption12", destinationDictionary: dataToSet, destinationKey: "notificationForMobileOption12", methodName:#function)
        copyData(info, sourceKey: "notificationForMobileOption13", destinationDictionary: dataToSet, destinationKey: "notificationForMobileOption13", methodName:#function)
        
        
        copyData(info, sourceKey: "notificationForWebsiteOption1", destinationDictionary: dataToSet, destinationKey: "notificationForWebsiteOption1", methodName:#function)
        copyData(info, sourceKey: "notificationForWebsiteOption2", destinationDictionary: dataToSet, destinationKey: "notificationForWebsiteOption2", methodName:#function)
        copyData(info, sourceKey: "notificationForWebsiteOption3", destinationDictionary: dataToSet, destinationKey: "notificationForWebsiteOption3", methodName:#function)
        copyData(info, sourceKey: "notificationForWebsiteOption4", destinationDictionary: dataToSet, destinationKey: "notificationForWebsiteOption4", methodName:#function)
        copyData(info, sourceKey: "notificationForWebsiteOption5", destinationDictionary: dataToSet, destinationKey: "notificationForWebsiteOption5", methodName:#function)
        copyData(info, sourceKey: "notificationForWebsiteOption6", destinationDictionary: dataToSet, destinationKey: "notificationForWebsiteOption6", methodName:#function)
        copyData(info, sourceKey: "notificationForWebsiteOption7", destinationDictionary: dataToSet, destinationKey: "notificationForWebsiteOption7", methodName:#function)
        copyData(info, sourceKey: "notificationForWebsiteOption8", destinationDictionary: dataToSet, destinationKey: "notificationForWebsiteOption8", methodName:#function)
        copyData(info, sourceKey: "notificationForWebsiteOption9", destinationDictionary: dataToSet, destinationKey: "notificationForWebsiteOption9", methodName:#function)
        copyData(info, sourceKey: "notificationForWebsiteOption10", destinationDictionary: dataToSet, destinationKey: "notificationForWebsiteOption10", methodName:#function)
        copyData(info, sourceKey: "notificationForWebsiteOption11", destinationDictionary: dataToSet, destinationKey: "notificationForWebsiteOption11", methodName:#function)
        copyData(info, sourceKey: "notificationForWebsiteOption12", destinationDictionary: dataToSet, destinationKey: "notificationForWebsiteOption12", methodName:#function)
        copyData(info, sourceKey: "notificationForWebsiteOption13", destinationDictionary: dataToSet, destinationKey: "notificationForWebsiteOption13", methodName:#function)
        
        
        
        copyData(info, sourceKey: "notificationForEmailOption1", destinationDictionary: dataToSet, destinationKey: "notificationForEmailOption1", methodName:#function)
        copyData(info, sourceKey: "notificationForEmailOption2", destinationDictionary: dataToSet, destinationKey: "notificationForEmailOption2", methodName:#function)
        copyData(info, sourceKey: "notificationForEmailOption3", destinationDictionary: dataToSet, destinationKey: "notificationForEmailOption3", methodName:#function)
        copyData(info, sourceKey: "notificationForEmailOption4", destinationDictionary: dataToSet, destinationKey: "notificationForEmailOption4", methodName:#function)
        copyData(info, sourceKey: "notificationForEmailOption5", destinationDictionary: dataToSet, destinationKey: "notificationForEmailOption5", methodName:#function)
        copyData(info, sourceKey: "notificationForEmailOption6", destinationDictionary: dataToSet, destinationKey: "notificationForEmailOption6", methodName:#function)
        copyData(info, sourceKey: "notificationForEmailOption7", destinationDictionary: dataToSet, destinationKey: "notificationForEmailOption7", methodName:#function)
        copyData(info, sourceKey: "notificationForEmailOption8", destinationDictionary: dataToSet, destinationKey: "notificationForEmailOption8", methodName:#function)
        copyData(info, sourceKey: "notificationForEmailOption9", destinationDictionary: dataToSet, destinationKey: "notificationForEmailOption9", methodName:#function)
        copyData(info, sourceKey: "notificationForEmailOption10", destinationDictionary: dataToSet, destinationKey: "notificationForEmailOption10", methodName:#function)
        copyData(info, sourceKey: "notificationForEmailOption11", destinationDictionary: dataToSet, destinationKey: "notificationForEmailOption11", methodName:#function)
        copyData(info, sourceKey: "notificationForEmailOption12", destinationDictionary: dataToSet, destinationKey: "notificationForEmailOption12", methodName:#function)
        copyData(info, sourceKey: "notificationForEmailOption13", destinationDictionary: dataToSet, destinationKey: "notificationForEmailOption13", methodName:#function)
        
        
        
        copyData(info, sourceKey: "notificationForSMSOption1", destinationDictionary: dataToSet, destinationKey: "notificationForSMSOption1", methodName:#function)
        copyData(info, sourceKey: "notificationForSMSOption2", destinationDictionary: dataToSet, destinationKey: "notificationForSMSOption2", methodName:#function)
        copyData(info, sourceKey: "notificationForSMSOption3", destinationDictionary: dataToSet, destinationKey: "notificationForSMSOption3", methodName:#function)
        copyData(info, sourceKey: "notificationForSMSOption4", destinationDictionary: dataToSet, destinationKey: "notificationForSMSOption4", methodName:#function)
        copyData(info, sourceKey: "notificationForSMSOption5", destinationDictionary: dataToSet, destinationKey: "notificationForSMSOption5", methodName:#function)
        copyData(info, sourceKey: "notificationForSMSOption6", destinationDictionary: dataToSet, destinationKey: "notificationForSMSOption6", methodName:#function)
        copyData(info, sourceKey: "notificationForSMSOption7", destinationDictionary: dataToSet, destinationKey: "notificationForSMSOption7", methodName:#function)
        copyData(info, sourceKey: "notificationForSMSOption8", destinationDictionary: dataToSet, destinationKey: "notificationForSMSOption8", methodName:#function)
        copyData(info, sourceKey: "notificationForSMSOption9", destinationDictionary: dataToSet, destinationKey: "notificationForSMSOption9", methodName:#function)
        copyData(info, sourceKey: "notificationForSMSOption10", destinationDictionary: dataToSet, destinationKey: "notificationForSMSOption10", methodName:#function)
        copyData(info, sourceKey: "notificationForSMSOption11", destinationDictionary: dataToSet, destinationKey: "notificationForSMSOption11", methodName:#function)
        copyData(info, sourceKey: "notificationForSMSOption12", destinationDictionary: dataToSet, destinationKey: "notificationForSMSOption12", methodName:#function)
        copyData(info, sourceKey: "notificationForSMSOption13", destinationDictionary: dataToSet, destinationKey: "notificationForSMSOption13", methodName:#function)
        
        
        copyData(info, sourceKey: "doNotDisturb", destinationDictionary: dataToSet, destinationKey: "doNotDisturb", methodName:#function)
        copyData(info, sourceKey: "selection1CachedValue", destinationDictionary: dataToSet, destinationKey: "selection1CachedValue", methodName:#function)
        copyData(info, sourceKey: "notificationVibrations", destinationDictionary: dataToSet, destinationKey: "notificationVibrations", methodName:#function)
        copyData(info, sourceKey: "notificationSounds", destinationDictionary: dataToSet, destinationKey: "notificationSounds", methodName:#function)
        
        copyData(info, sourceKey: "verifiedUserDesignation", destinationDictionary: dataToSet, destinationKey: "verifiedUserDesignation", methodName:#function)
        copyData(info, sourceKey: "verifiedGovernmentIdProofDocument", destinationDictionary: dataToSet, destinationKey: "verifiedGovernmentIdProofDocument", methodName:#function)
        copyData(info, sourceKey: "verifiedMobileNumber", destinationDictionary: dataToSet, destinationKey: "verifiedMobileNumber", methodName:#function)
        copyData(info, sourceKey: "intraCompany", destinationDictionary: dataToSet, destinationKey: "intraCompany", methodName:#function)
        copyData(info, sourceKey: "companyId", destinationDictionary: dataToSet, destinationKey: "companyId", methodName:#function)
        copyData(info, sourceKey: "userType", destinationDictionary: dataToSet, destinationKey: "userType", methodName:#function)
        copyData(info, sourceKey: "eprSettingOption1", destinationDictionary: dataToSet, destinationKey: "eprSettingOption1", methodName:#function)
        copyData(info, sourceKey: "eprSettingOption2", destinationDictionary: dataToSet, destinationKey: "eprSettingOption2", methodName:#function)
        copyData(info, sourceKey: "showMyNumber", destinationDictionary: dataToSet, destinationKey: "showMyNumber", methodName:#function)
        
        
        if isNull(object?.homeTime){
            object!.homeTime = Date().change(nil, month: nil, day: nil, hour: 8, minute: 0, second: 0)?.stringValue()
        }
        if isNull(object?.destinationTime){
            object!.destinationTime =  Date().change(nil, month: nil, day: nil, hour: 18, minute: 0, second: 0)?.stringValue()
        }
        
        object?.setValuesForKeys((dataToSet as? [String:Any])!)
        
        saveChanges()
    }
    
    func getSetting()->(Setting){
        var object : Setting? =  Setting.mr_findFirst()
        if isNull(object){
            setSetting(NSDictionary())
            object =  Setting.mr_findFirst()
        }
        return object!
    }
    
    /**
     * Status
     */
    func addStatus(_ info:NSDictionary){
        if isNotNull(info.object(forKey: "status")){
            if isNull(getStatus(info.object(forKey: "status") as! String)){
                let object = Status.mr_createEntity(in: managedObjectContext!)
                let dataToSet = NSMutableDictionary()
                copyData(info, sourceKey: "status", destinationDictionary: dataToSet, destinationKey: "status", methodName:#function)
                object?.setValuesForKeys((dataToSet as? [String:Any])!)
                saveChanges()
            }
        }
    }
    
    func getStatus(_ status:String)->Status?{
        return Status.mr_findFirst(byAttribute: "status", withValue: status)
    }
    
    func getAllStatus()->NSMutableArray {
        return (Status.mr_findAll()! as [NSManagedObject]) as! NSMutableArray
    }
    
    /**
     * UserId
     */
    func addUserId(_ userId:String){
        if isNull(getUserId(userId)){
            let object = UserId.mr_createEntity(in: managedObjectContext!)
            let dataToSet = NSMutableDictionary()
            copyData(userId , destinationDictionary: dataToSet, destinationKey: "value", methodName: #function)
            object?.setValuesForKeys((dataToSet as? [String:Any])!)
            saveChangesLazily()
        }
    }
    
    func getUserId(_ userId:String)->UserId?{
        return UserId.mr_findFirst(byAttribute: "value", withValue: userId)
    }
    
    func getAllUserIds()->NSMutableArray {
        return (UserId.mr_findAll()! as [NSManagedObject]) as! NSMutableArray
    }
    
    /**
     * City
     */
    func addCity(_ info:NSDictionary){
        var object = getCity(info)
        if isNull(object){
            object = City.mr_createEntity(in: managedObjectContext!)
        }
        let dataToSet = NSMutableDictionary()
        copyData(info.object(forKey: "id") , destinationDictionary: dataToSet, destinationKey: "id", methodName: #function)
        copyData(info.object(forKey: "city_name") , destinationDictionary: dataToSet, destinationKey: "name", methodName: #function)
        object?.setValuesForKeys((dataToSet as? [String:Any])!)
        saveChangesLazily()
    }
    
    func getCity(_ info:NSDictionary)->City?{
        return City.mr_findFirst(byAttribute: "id", withValue: info.object(forKey: "id")!)
    }
    
    func getAllCities()->NSMutableArray {
        return (UserId.mr_findAll()! as [NSManagedObject]) as! NSMutableArray
    }
    
    /**
     * Company
     */
    func addCompany(_ info:NSDictionary){
        if let destinationName = (info.object(forKey: "comp_name") as? String){
            if destinationName.length > 0{
                var object = getCompany(info)
                if isNull(object){
                    object = Company.mr_createEntity(in: managedObjectContext!)
                }
                let dataToSet = NSMutableDictionary()
                copyData(info.object(forKey: "comp_id") , destinationDictionary: dataToSet, destinationKey: "id", methodName: #function)
                copyData((info.object(forKey: "comp_name") as! String), destinationDictionary: dataToSet, destinationKey: "name", methodName: #function)
                copyData(info.object(forKey: "address") , destinationDictionary: dataToSet, destinationKey: "address", methodName: #function)
                object?.setValuesForKeys((dataToSet as? [String:Any])!)
                saveChangesLazily()
            }
        }
    }
    
    func getCompany(_ info:NSDictionary)->Company?{
        return Company.mr_findFirst(byAttribute: "id", withValue: info.object(forKey: "comp_id")!)
    }
    
    func getAllCompanies()->NSMutableArray {
        return (Company.mr_findAll()! as [NSManagedObject]) as! NSMutableArray
    }
    
    func getCompanies(_ search:String)->NSMutableArray {
        return NSMutableArray(array:(Company.mr_findAll()! as [NSManagedObject]).filter({ (company) -> Bool in
            return (company as! Company).name!.lowercased().contains(search)
        }))
    }
    
    /**
     * College
     */
    func addCollege(_ info:NSDictionary){
        if let destinationName = (info.object(forKey: "clg_name") as? String){
            if destinationName.length > 0{
                var object = getCollege(info)
                if isNull(object){
                    object = College.mr_createEntity(in: managedObjectContext!)
                }
                let dataToSet = NSMutableDictionary()
                copyData(info.object(forKey: "clg_id") , destinationDictionary: dataToSet, destinationKey: "id", methodName: #function)
                copyData((info.object(forKey: "clg_name") as! String), destinationDictionary: dataToSet, destinationKey: "name", methodName: #function)
                copyData(info.object(forKey: "address") , destinationDictionary: dataToSet, destinationKey: "address", methodName: #function)
                object?.setValuesForKeys((dataToSet as? [String:Any])!)
                saveChangesLazily()
            }
        }
    }
    
    func getCollege(_ info:NSDictionary)->College?{
        return College.mr_findFirst(byAttribute: "id", withValue: info.object(forKey: "clg_id")!)
    }
    
    func getAllColleges()->NSMutableArray {
        return (College.mr_findAll()! as [NSManagedObject]) as! NSMutableArray
    }
    
    func getColleges(_ search:String)->NSMutableArray {
        return NSMutableArray(array:(College.mr_findAll()! as [NSManagedObject]).filter({ (college) -> Bool in
            return (college as! College).name!.lowercased().contains(search)
        }))
    }
    
    /**
     * CarMake
     */
    func addCarMake(_ info:NSDictionary){
        var object = getCarMake(info)
        if isNull(object){
            object = CarMake.mr_createEntity(in: managedObjectContext!)
        }
        let dataToSet = NSMutableDictionary()
        copyData(info.object(forKey: "car_mk_id") , destinationDictionary: dataToSet, destinationKey: "mkId", methodName: #function)
        copyData(info.object(forKey: "car_mk_name") , destinationDictionary: dataToSet, destinationKey: "name", methodName: #function)
        object?.setValuesForKeys((dataToSet as? [String:Any])!)
        saveChangesLazily()
    }
    
    func getCarMake(_ info:NSDictionary)->CarMake?{
        return CarMake.mr_findFirst(byAttribute: "mkId", withValue: info.object(forKey: "car_mk_id")!)
    }
    
    func getAllCarMake()->NSMutableArray {
        return (CarMake.mr_findAll()! as [NSManagedObject]) as! NSMutableArray
    }
    
    /**
     * CarModel
     */
    func addCarModel(_ info:NSDictionary){
        var object = getCarModel(info)
        if isNull(object){
            object = CarModel.mr_createEntity(in: managedObjectContext!)
        }
        let dataToSet = NSMutableDictionary()
        copyData(info.object(forKey: "car_mk_id") , destinationDictionary: dataToSet, destinationKey: "mkId", methodName: #function)
        copyData(info.object(forKey: "car_md_id") , destinationDictionary: dataToSet, destinationKey: "mdId", methodName: #function)
        copyData(info.object(forKey: "car_md_name") , destinationDictionary: dataToSet, destinationKey: "name", methodName: #function)
        object?.setValuesForKeys((dataToSet as? [String:Any])!)
        saveChangesLazily()
    }
    
    func getCarModel(_ info:NSDictionary)->CarModel?{
        return CarModel.mr_findFirst(byAttribute: "mdId", withValue: info.object(forKey: "car_md_id")!)
    }
    
    func getAllCarModel()->NSMutableArray {
        return (CarModel.mr_findAll()! as [NSManagedObject]) as! NSMutableArray
    }

    func deleteCompanyCitiesCarMakeCarModel() {
        City.mr_truncateAll()
        Company.mr_truncateAll()
        CarMake.mr_truncateAll()
        CarModel.mr_truncateAll()
        saveChanges()
    }
    
    /**
     * Notification
     */
    func addNotification(_ info:NSDictionary) {
        var object = getNotification(info)
        if isNull(object){
            object = Notification.mr_createEntity(in: managedObjectContext!)
        }
        let dataToSet = NSMutableDictionary()
        copyData(info, sourceKey: "notificationId", destinationDictionary: dataToSet, destinationKey: "notificationId", methodName:#function,asString:true)
        copyData(info, sourceKey: "title", destinationDictionary: dataToSet, destinationKey: "title", methodName:#function,asString:true)
        copyData(info, sourceKey: "message", destinationDictionary: dataToSet, destinationKey: "message", methodName:#function,asString:true)
        copyData(info, sourceKey: "timestamp", destinationDictionary: dataToSet, destinationKey: "timestamp", methodName:#function,asString:true)
        copyData(info, sourceKey: "notificationType", destinationDictionary: dataToSet, destinationKey: "notificationType", methodName:#function,asString:true)
        copyData(info, sourceKey: "placeId", destinationDictionary: dataToSet, destinationKey: "placeId", methodName:#function,asString:true)
        copyData(info, sourceKey: "groupId", destinationDictionary: dataToSet, destinationKey: "groupId", methodName:#function,asString:true)
        copyData(info, sourceKey: "associatedUserId", destinationDictionary: dataToSet, destinationKey: "associatedUserId", methodName:#function,asString:true)
        copyData(info, sourceKey: "associatedUserName", destinationDictionary: dataToSet, destinationKey: "associatedUserName", methodName:#function,asString:true)
        copyData(info, sourceKey: "associatedUserPicture", destinationDictionary: dataToSet, destinationKey: "associatedUserPicture", methodName:#function,asString:true)
        copyData(info, sourceKey: "address", destinationDictionary: dataToSet, destinationKey: "address", methodName:#function,asString:true)
        copyData(info, sourceKey: "addressName", destinationDictionary: dataToSet, destinationKey: "addressName", methodName:#function,asString:true)
        copyData(info, sourceKey: "addressLocationLatitude", destinationDictionary: dataToSet, destinationKey: "addressLocationLatitude", methodName:#function,asString:true)
        copyData(info, sourceKey: "addressLocationLongitude", destinationDictionary: dataToSet, destinationKey: "addressLocationLongitude", methodName:#function,asString:true)
        copyData(info, sourceKey: "travelMode", destinationDictionary: dataToSet, destinationKey: "travelMode", methodName:#function,asString:true)
        copyData(info, sourceKey: "travelDayDate", destinationDictionary: dataToSet, destinationKey: "travelDayDate", methodName:#function,asString:true)
        copyData(info, sourceKey: "travelDayTime", destinationDictionary: dataToSet, destinationKey: "travelDayTime", methodName:#function,asString:true)
        object?.setValuesForKeys((dataToSet as? [String:Any])!)
        saveChanges()
    }
    
    func getNotification(_ info:NSDictionary)->Notification?{
        return Notification.mr_findFirst(byAttribute: "notificationId", withValue: info.object(forKey: "notificationId")!)
    }

    func getAllNotifications()->NSMutableArray {
        let predicateBuilder = DKPredicateBuilder()
        predicateBuilder.limit = NSNumber(value: MAXIMUM_NOTIFICATIONS_AT_A_TIME)
        let predicateFormat = predicateBuilder.compoundPredicate().predicateFormat
        return NSMutableArray(array:(Notification.mr_findAll(with: NSPredicate(format:predicateFormat)) as! [Notification]).sorted(by: { (n1, n2) -> Bool in
            return safeInt(n1.value(forKey: "timestamp")) < safeInt(n2.value(forKey: "timestamp"))
        }))
    }
    
    func getAllNotifications(groupId:String)->NSMutableArray {
        let predicateBuilder = DKPredicateBuilder()
        predicateBuilder.dk_where("groupId", equals: groupId)
        predicateBuilder.limit = NSNumber(value: MAXIMUM_NOTIFICATIONS_AT_A_TIME)
        let predicateFormat = predicateBuilder.compoundPredicate().predicateFormat
        return NSMutableArray(array:(Notification.mr_findAll(with: NSPredicate(format:predicateFormat)) as! [Notification]).sorted(by: { (n1, n2) -> Bool in
            return safeInt(n1.value(forKey: "timestamp")) < safeInt(n2.value(forKey: "timestamp"))
        }))
    }
    
    func getAllNotifications(groupId:String,memberId:String)->NSMutableArray {
        let predicateBuilder = DKPredicateBuilder()
        predicateBuilder.dk_where("groupId", equals: groupId)
        predicateBuilder.dk_where("associatedUserId", equals: memberId)
        predicateBuilder.limit = NSNumber(value: MAXIMUM_NOTIFICATIONS_AT_A_TIME)
        let predicateFormat = predicateBuilder.compoundPredicate().predicateFormat
        return NSMutableArray(array:(Notification.mr_findAll(with: NSPredicate(format:predicateFormat)) as! [Notification]).sorted(by: { (n1, n2) -> Bool in
            return safeInt(n1.value(forKey: "timestamp")) < safeInt(n2.value(forKey: "timestamp"))
        }))
    }
    
    func getUnreadNotificationsCount()->Int {
        let unreadNotifications = Notification.mr_find(byAttribute: "read", withValue: NSNumber(value: false))
        if isNotNull(unreadNotifications){
            return Int(unreadNotifications!.count)
        }else{
            return 0
        }
    }
    
    func getUnreadNotificationsCount(groupId:String)->Int {
        let predicateBuilder = DKPredicateBuilder()
        predicateBuilder.dk_where("groupId", equals: groupId)
        predicateBuilder.dk_where("read", equals: NSNumber(value: false))
        let predicateFormat = predicateBuilder.compoundPredicate().predicateFormat
        return safeInt(Notification.mr_findAll(with: NSPredicate(format:predicateFormat))?.count)
    }
    
    func getUnreadNotificationsCount(groupId:String,memberId:String)->Int {
        let predicateBuilder = DKPredicateBuilder()
        predicateBuilder.dk_where("groupId", equals: groupId)
        predicateBuilder.dk_where("associatedUserId", equals: memberId)
        predicateBuilder.dk_where("read", equals: NSNumber(value: false))
        let predicateFormat = predicateBuilder.compoundPredicate().predicateFormat
        return safeInt(Notification.mr_findAll(with: NSPredicate(format:predicateFormat))?.count)
    }
    
    /**
     * Favourites
     */
    func addFavourites(_ info:NSDictionary){
        var object = getFavourite(info)
        if isNull(object){
            object = Favourites.mr_createEntity(in: managedObjectContext!)
        }
        let dataToSet = NSMutableDictionary()
        copyData(info, sourceKey: "user_name", destinationDictionary: dataToSet, destinationKey: "name", methodName:#function)
        copyData(Acf().getUserIdFor(info) , destinationDictionary: dataToSet, destinationKey: "userId", methodName: #function)
        copyData(info, sourceKey: "user_photo", destinationDictionary: dataToSet, destinationKey: "picture", methodName:#function)
        copyData(info, sourceKey: "priority", destinationDictionary: dataToSet, destinationKey: "priority", methodName:#function)
        if isNotNull(dataToSet.object(forKey: "priority")){
            var priority = NSNumber(value: ("\(dataToSet.object(forKey: "priority")!)" as NSString) .integerValue as Int)
            if priority.intValue == 0 {
                priority = NSNumber(value: 1024 as Int)
            }
            dataToSet.setObject(priority, forKey: "priority" as NSCopying)
        }
        object?.setValuesForKeys((dataToSet as? [String:Any])!)
        saveChanges()
    }
    
    func getFavourite(_ info:NSDictionary)->Favourites?{
        return Favourites.mr_findFirst(byAttribute: "userId", withValue: Acf().getUserIdFor(info))
    }
    
    func removeFavourite(_ info:NSDictionary){
        let object =  Favourites.mr_findFirst(byAttribute: "userId", withValue: Acf().getUserIdFor(info))
        if isNotNull(object){
            managedObjectContext?.delete(object!)
        }
        saveChanges()
    }
    
    func isFavourite(_ info:NSDictionary)->Bool?{
        return isNotNull(Favourites.mr_findFirst(byAttribute: "userId", withValue: Acf().getUserIdFor(info)))
    }
    
    func getAllFavourites()->NSMutableArray {
        return NSMutableArray(array:(Favourites.mr_findAll()! as [NSManagedObject]).sorted(by: { (favouriteObject1, favouriteObject2) -> Bool in
            return (favouriteObject1.value(forKey: "priority") as! Int) < (favouriteObject2.value(forKey: "priority") as! Int)
        }))
    }
    
    func removeAllFavourites() {
        Favourites.mr_truncateAll()
    }
    
    /**
     * UserStatus
     */
    func addUserStatus(_ info:NSDictionary){
        if isNotNull(info.object(forKey: "status")){
            if isNotNull(info.object(forKey: "userId")){
                var object = getUserStatus(info)
                if isNull(object){
                    object = UserStatus.mr_createEntity(in: managedObjectContext!)
                }
                let dataToSet = NSMutableDictionary()
                copyData(Acf().getUserIdFor(info) , destinationDictionary: dataToSet, destinationKey: "userId", methodName: #function)
                copyData(info, sourceKey: "status", destinationDictionary: dataToSet, destinationKey: "status", methodName:#function)
                object?.setValuesForKeys((dataToSet as? [String:Any])!)
                saveChangesLazily()
            }
        }
    }
    
    func getUserStatus(_ info:NSDictionary)->UserStatus?{
        return UserStatus.mr_findFirst(byAttribute: "userId", withValue: Acf().getUserIdFor(info))
    }
    
    /**
     * UserName
     */
    func addUserName(_ info:NSDictionary){
        var object = getUserName(info)
        if isNull(object){
            object = UserName.mr_createEntity(in: managedObjectContext!)
        }
        let dataToSet = NSMutableDictionary()
        copyData(Acf().getUserIdFor(info) , destinationDictionary: dataToSet, destinationKey: "userId", methodName: #function)
        copyData(info, sourceKey: "name", destinationDictionary: dataToSet, destinationKey: "name", methodName:#function)
        object?.setValuesForKeys((dataToSet as? [String:Any])!)
        saveChangesLazily()
    }
    
    func getUserName(_ info:NSDictionary)->UserName?{
        return UserName.mr_findFirst(byAttribute: "userId", withValue: Acf().getUserIdFor(info))
    }
    
    /**
     * UserPicture
     */
    func addUserPicture(_ info:NSDictionary){
        var object = getUserPicture(info)
        if isNull(object){
            object = UserPicture.mr_createEntity(in: managedObjectContext!)
        }
        let dataToSet = NSMutableDictionary()
        copyData(Acf().getUserIdFor(info) , destinationDictionary: dataToSet, destinationKey: "userId", methodName: #function)
        copyData(info, sourceKey: "url", destinationDictionary: dataToSet, destinationKey: "url", methodName:#function)
        object?.setValuesForKeys((dataToSet as? [String:Any])!)
        saveChangesLazily()
    }
    
    func getUserPicture(_ info:NSDictionary)->UserPicture?{
        return UserPicture.mr_findFirst(byAttribute: "userId", withValue: Acf().getUserIdFor(info))
    }
    
    /**
     * FailedQBMessage
     */
    func addFailedQBMessage(_ info:NSDictionary){
        var object = getFailedQBMessage(info)
        if isNull(object){
            object = FailedQBMessage.mr_createEntity(in: managedObjectContext!)
        }
        let dataToSet = NSMutableDictionary()
        copyData(info, sourceKey: "messageId", destinationDictionary: dataToSet, destinationKey: "messageId", methodName:#function)
        object?.setValuesForKeys((dataToSet as? [String:Any])!)
        saveChanges()
    }
    
    func getFailedQBMessage(_ info:NSDictionary)->FailedQBMessage?{
        return FailedQBMessage.mr_findFirst(byAttribute: "messageId", withValue:info.object(forKey: "messageId") as! String)
    }
    
    func deleteFailedQBMessage(_ info:NSDictionary){
        if let message = FailedQBMessage.mr_findFirst(byAttribute: "messageId", withValue:info.object(forKey: "messageId") as! String) {
            managedObjectContext?.delete(message)
            saveChanges()
        }
    }
    
    
    /**
     * TrackedEvents
     */
    func addTrackedEvents(_ info:NSDictionary){
        let object = TrackedEvents.mr_createEntity(in: managedObjectContext!)
        let dataToSet = NSMutableDictionary()
        copyData(info, sourceKey: "tForHomeFforDestination", destinationDictionary: dataToSet, destinationKey: "tForHomeFforDestination", methodName:#function)
        copyData(info, sourceKey: "tForEnterFforExit", destinationDictionary: dataToSet, destinationKey: "tForEnterFforExit", methodName:#function)
        copyData(info, sourceKey: "date", destinationDictionary: dataToSet, destinationKey: "date", methodName:#function)
        object?.setValuesForKeys((dataToSet as? [String:Any])!)
        saveChanges()
    }
    
    func getTrackedEvents()->[TrackedEvents] {
        return (TrackedEvents.mr_findAll()! as! [TrackedEvents])
    }
    
    func removeTrackedEvents() {
        TrackedEvents.mr_truncateAll()
        saveChanges()
    }
    
    
    /**
     * Group
     */
    
    func removeAllGroups() {
        Group.mr_truncateAll()
    }
    
    func addUpdateGroup(_ info:NSDictionary){
        var object = getGroup(info)
        if isNull(object){
            object = Group.mr_createEntity(in: managedObjectContext!)
        }
        let dataToSet = NSMutableDictionary()
        copyData(info, sourceKey: "groupId", destinationDictionary: dataToSet, destinationKey: "groupId", methodName:#function)
        copyData(info, sourceKey: "name", destinationDictionary: dataToSet, destinationKey: "name", methodName:#function)
        copyData(info, sourceKey: "picture", destinationDictionary: dataToSet, destinationKey: "picture", methodName:#function)
        copyData(info, sourceKey: "themeColor", destinationDictionary: dataToSet, destinationKey: "themeColor", methodName:#function)
        copyData(info, sourceKey: "isAdmin", destinationDictionary: dataToSet, destinationKey: "isAdmin", methodName:#function)
        copyData(info, sourceKey: "isMuted", destinationDictionary: dataToSet, destinationKey: "isMuted", methodName:#function)
        copyData(info, sourceKey: "locationSharingEnabled", destinationDictionary: dataToSet, destinationKey: "locationSharingEnabled", methodName:#function)
        
        
        var membersArray = [Member]()
        if let members = info.object(forKey: "members") as? NSArray {
            for m in members {
                let mAsD = m as! NSDictionary
                let object = Member.mr_createEntity(in: managedObjectContext!)
                let dataToSet = NSMutableDictionary()
                copyData(mAsD, sourceKey: "userId", destinationDictionary: dataToSet, destinationKey: "userId", methodName:#function)
                copyData(mAsD, sourceKey: "name", destinationDictionary: dataToSet, destinationKey: "name", methodName:#function)
                copyData(mAsD, sourceKey: "picture", destinationDictionary: dataToSet, destinationKey: "picture", methodName:#function)
                copyData(mAsD, sourceKey: "lastKnownTimeStamp", destinationDictionary: dataToSet, destinationKey: "lastKnownTimeStamp", methodName:#function)
                copyData(mAsD, sourceKey: "lastKnownBatteryStatus", destinationDictionary: dataToSet, destinationKey: "lastKnownBatteryStatus", methodName:#function)
                copyData(mAsD, sourceKey: "lastKnownLocationAddress", destinationDictionary: dataToSet, destinationKey: "lastKnownLocationAddress", methodName:#function)
                if isNotNull(mAsD.object(forKey: "lastKnownLocationLatitude")){
                    copyData(mAsD, sourceKey: "lastKnownLocationLatitude", destinationDictionary: dataToSet, destinationKey: "lastKnownLocationLatitude", methodName:#function)
                }
                if isNotNull(mAsD.object(forKey: "lastKnownLocationLongitude")){
                    copyData(mAsD, sourceKey: "lastKnownLocationLongitude", destinationDictionary: dataToSet, destinationKey: "lastKnownLocationLongitude", methodName:#function)
                }
                copyData(NSNumber(value: safeBool(mAsD.object(forKey: "locationDisplay"))), destinationDictionary: dataToSet, destinationKey: "locationDisplay", methodName:#function)
                object?.setValuesForKeys((dataToSet as? [String:Any])!)
                membersArray.append(object!)
            }
        }
        
        var associatedPlacesArray = [AssociatedPlaces]()
        if let associatedPlaces = info.object(forKey: "associatedPlaces") as? NSArray {
            for p in associatedPlaces {
                let pAsD = p as! NSDictionary
                let object = AssociatedPlaces.mr_createEntity(in: managedObjectContext!)
                let dataToSet = NSMutableDictionary()
                copyData(pAsD, sourceKey: "placeId", destinationDictionary: dataToSet, destinationKey: "placeId", methodName:#function)
                copyData(pAsD, sourceKey: "entrySharingEnabled", destinationDictionary: dataToSet, destinationKey: "entrySharingEnabled", methodName:#function)
                copyData(pAsD, sourceKey: "exitSharingEnabled", destinationDictionary: dataToSet, destinationKey: "exitSharingEnabled", methodName:#function)
                copyData(pAsD, sourceKey: "isAdmin", destinationDictionary: dataToSet, destinationKey: "isAdmin", methodName:#function)
                object?.setValuesForKeys((dataToSet as? [String:Any])!)
                associatedPlacesArray.append(object!)
            }
        }

        object?.members = NSOrderedSet(array: membersArray)
        object?.associatedPlaces = NSOrderedSet(array: associatedPlacesArray)
        object?.setValuesForKeys((dataToSet as? [String:Any])!)
        saveChanges()
    }
    
    func addAssociatedPlaceToGroup(group:Group,_ info:NSDictionary){
        let pAsD = info
        let object = AssociatedPlaces.mr_createEntity(in: managedObjectContext!)
        let dataToSet = NSMutableDictionary()
        copyData(pAsD, sourceKey: "placeId", destinationDictionary: dataToSet, destinationKey: "placeId", methodName:#function)
        copyData(pAsD, sourceKey: "entrySharingEnabled", destinationDictionary: dataToSet, destinationKey: "entrySharingEnabled", methodName:#function)
        copyData(pAsD, sourceKey: "exitSharingEnabled", destinationDictionary: dataToSet, destinationKey: "exitSharingEnabled", methodName:#function)
        copyData(pAsD, sourceKey: "isAdmin", destinationDictionary: dataToSet, destinationKey: "isAdmin", methodName:#function)
        object?.setValuesForKeys((dataToSet as? [String:Any])!)
        group.addToAssociatedPlaces(object!)
        saveChanges()
    }
    
    func getGroup(_ info:NSDictionary)->Group?{
        return Group.mr_findFirst(byAttribute: "groupId", withValue:info.object(forKey: "groupId") as! String)
    }
    
    func getAllGroups()->[Group] {
        return Group.mr_findAll()! as! [Group]
    }
    
    func addUpdatePlace(info:NSDictionary){
        let pAsD = info
        var object = getPlace(info)
        if isNull(object){
            object = Place.mr_createEntity(in: managedObjectContext!)
        }
        let dataToSet = NSMutableDictionary()
        copyData(pAsD, sourceKey: "placeId", destinationDictionary: dataToSet, destinationKey: "placeId", methodName:#function)
        copyData(pAsD, sourceKey: "name", destinationDictionary: dataToSet, destinationKey: "name", methodName:#function)
        copyData(pAsD, sourceKey: "placeAddress", destinationDictionary: dataToSet, destinationKey: "placeAddress", methodName:#function)
        copyData(pAsD, sourceKey: "placeLatitude", destinationDictionary: dataToSet, destinationKey: "placeLatitude", methodName:#function)
        copyData(pAsD, sourceKey: "placeLongitude", destinationDictionary: dataToSet, destinationKey: "placeLongitude", methodName:#function)
        object?.setValuesForKeys((dataToSet as? [String:Any])!)
        saveChanges()
    }
    
    func getPlace(_ info:NSDictionary)->Place?{
        return Place.mr_findFirst(byAttribute: "placeId", withValue:info.object(forKey: "placeId") as! String)
    }
    
    func getCorrespondingPlace(_ associatedPlace:AssociatedPlaces)->Place?{
        return Place.mr_findFirst(byAttribute: "placeId", withValue:safeString(associatedPlace.placeId))
    }
    
    func getAllPlaces()->[Place] {
        return Place.mr_findAll()! as! [Place]
    }
    
    func removeAllPlaces() {
        Place.mr_truncateAll()
    }
    

    func searchPlaces(text:String?)->[Place] {
        if isNotNull(text) {
            let predicateBuilder = DKPredicateBuilder()
            predicateBuilder.dk_where("name", contains: text!)
            let predicateFormat = predicateBuilder.compoundPredicate().predicateFormat
            return Place.mr_findAll(with: NSPredicate(format:predicateFormat)) as! [Place]
        }else{
            return getAllPlaces()
        }
    }
    
    /**
     * Common Database Operations
     */
    
    func deleteObject(_ object:NSManagedObject){
        managedObjectContext?.delete(object)
        saveChanges()
    }

    func resetDatabase(){
        developerOptionsPerformAnyTaskBeforeReSettingDatabase()
        UserInfo.mr_truncateAll()
        Setting.mr_truncateAll()
        Notification.mr_truncateAll()
        Favourites.mr_truncateAll()
        TrackedEvents.mr_truncateAll()
        Group.mr_truncateAll()
        Place.mr_truncateAll()
        CacheManager.sharedInstance.resetDatabase()
        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        UserDefaults.standard.synchronize()
        saveChanges()
        CacheManager.sharedInstance.saveObject(Date() , identifier:"lastSyncDateUpdateCityCompanyCollegeCarOptionsList")
        UserDefaults.standard.set(UIApplication.versionBuild(), forKey: APP_UNIQUE_BUILD_IDENTIFIER)
        setDeviceToken("PLACEHOLDER")
        importantInitialisations()
    }
    
    func saveChangesLazily(){
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(DatabaseManager.saveChangesPrivate), object: nil)
        self.perform(#selector(DatabaseManager.saveChangesPrivate), with: nil, afterDelay:0.3)
    }
    
    @objc fileprivate func saveChangesPrivate(){
        execMain({[weak self]  in guard let `self` = self else { return }
            self.saveChanges()
        },delay:0.0)
    }
    
    func saveChanges(){
        self.managedObjectContext?.mr_saveToPersistentStoreAndWait()
    }
}

