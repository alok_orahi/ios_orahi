//
//  RideValidationManager.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import Foundation
import UIKit

typealias RVMCompletionBlock = (_ completed :Bool,_ users:NSMutableArray?) ->()

class RideValidationManager: NSObject , CLLocationManagerDelegate{
   
    var allRideEventMessages = NSMutableArray()
    var trueForOwnerFalseforPassenger = false
    var trueForHomeToDestinationFalseforDestinationToHome = false
    var groupChatDialogue: QBChatDialog?
    var dateOfRide = Date()
    var rideId = ""
    var completion : RVMCompletionBlock?
    var rideCompletedWithUsers = NSMutableArray()
   
    static let sharedInstance : RideValidationManager = {
        let instance = RideValidationManager()
        return instance
    }()
    
    fileprivate override init() {
        
    }
    
    func checkMyRideCompletionStatus(_ isOwner:Bool,isHomeToDestination:Bool,dateOfRide:Date,groupChatDialogue:QBChatDialog,completion:@escaping RVMCompletionBlock) {
        self.dateOfRide = dateOfRide
        self.trueForOwnerFalseforPassenger = isOwner
        self.trueForHomeToDestinationFalseforDestinationToHome = isHomeToDestination
        self.groupChatDialogue = groupChatDialogue
        rideId = generateRideId(self.dateOfRide, trueForHomeToDestinationFalseforDestinationToHome: self.trueForHomeToDestinationFalseforDestinationToHome)
        self.completion = completion
        prepare()
        execMain({[weak self]  in guard let `self` = self else { return }
            //Step 1
            self.loadEventMessges()
            //Step 2
            completion(self.checkIfMyRideIsCompleted(), self.rideCompletedWithUsers)
            },delay: 6)
    }
    
    func checkIfMyRideIsCompleted() -> Bool {
        rideCompletedWithUsers.removeAllObjects()
        var isMyRideCompleted = false
        if trueForOwnerFalseforPassenger{
            let userIdsIStartedFrom = NSMutableOrderedSet()
            let userIdsIReachedTo = NSMutableOrderedSet()
            let userIdsTheyStartedFrom = NSMutableOrderedSet()
            let userIdsTheyReachedTo = NSMutableOrderedSet()
            let userIdsInvolved = NSMutableOrderedSet()
            let userIdsCompletedTheRide = NSMutableOrderedSet()
            for m in allRideEventMessages {
                if let message = m as? QBChatMessage{
                    logMessage(message.text!)
                    if let eventId = message.customParameters!.object(forKey: "eventId") as? String {
                        let tForSelfFforOthers = getValueFortForSelfFforOthers(eventId)
                        let tForHomeToDestinationFforDestinationToHome = getValueFortForHomeToDestinationFforDestinationToHome(eventId)
                        let tForSourceFForDestination = getValueFortForSourceFForDestination(eventId)
                        let tForEnterFforExit = getValueFortForEnterFforExit(eventId)
                        let tForOwnerFforPassenger = getValueFortForOwnerFforPassenger(eventId)
                        let eventForUserId = message.customParameters!.object(forKey: "eventForUserId") as! String

                        if tForSelfFforOthers == false && tForSourceFForDestination && tForOwnerFforPassenger && tForEnterFforExit == false {
                            //car owner started from passengers source
                            userIdsIStartedFrom.add(eventForUserId)
                        }
                        if tForSelfFforOthers == false && tForSourceFForDestination == false && tForOwnerFforPassenger && tForEnterFforExit {
                            //car owner reached to passenger destination
                            userIdsIReachedTo.add(eventForUserId)
                        }
                        if tForSelfFforOthers && tForSourceFForDestination && tForOwnerFforPassenger == false && tForEnterFforExit == false {
                            //passenger started from source
                            userIdsTheyStartedFrom.add(eventForUserId)
                        }
                        if tForSelfFforOthers && tForSourceFForDestination == false && tForOwnerFforPassenger == false && tForEnterFforExit {
                            //passenger reached to destination
                            userIdsTheyReachedTo.add(eventForUserId)
                        }
                        userIdsInvolved.add(eventForUserId)
                    }
                }
            }
            
            for u in userIdsInvolved {
                if (u as! NSString).isEqual(to: loggedInUserId()) == false {
                    if userIdsIStartedFrom.index(of: u) != NSNotFound {
                        if userIdsTheyStartedFrom.index(of: u) != NSNotFound {
                            if userIdsIReachedTo.index(of: u) != NSNotFound {
                                if userIdsTheyReachedTo.index(of: u) != NSNotFound {
                                    userIdsCompletedTheRide.add(u)
                                }
                            }
                        }
                    }
                }
            }
            if userIdsCompletedTheRide.count > 0 {
                logMessage("My ride as car owner is completed with passenger having ids \(userIdsCompletedTheRide)")
                isMyRideCompleted = true
                rideCompletedWithUsers.addObjects(from: userIdsCompletedTheRide.array)
            }
        }else{
            var carOwnerUserId:String?
            var isCarOwnerStartedFromMySourceLocation = false
            var isCarOwnerReachedToMyDestinationLocation = false
            var didIStartedFromMySourceLocation = false
            var didIReachedToMyDestinationLocation = false
            for m in allRideEventMessages {
                if let message = m as? QBChatMessage{
                    logMessage(message.text!)
                    if let eventId = message.customParameters!.object(forKey: "eventId") as? String {
                        let tForSelfFforOthers = getValueFortForSelfFforOthers(eventId)
                        let tForHomeToDestinationFforDestinationToHome = getValueFortForHomeToDestinationFforDestinationToHome(eventId)
                        let tForSourceFForDestination = getValueFortForSourceFForDestination(eventId)
                        let tForEnterFforExit = getValueFortForEnterFforExit(eventId)
                        let tForOwnerFforPassenger = getValueFortForOwnerFforPassenger(eventId)
                        let eventForUserId = message.customParameters!.object(forKey: "eventForUserId") as! String
                        
                        if isNull(carOwnerUserId) && tForSelfFforOthers && tForOwnerFforPassenger {
                            if let userId = message.customParameters!.object(forKey: "userId") as? String {
                                carOwnerUserId = userId
                            }
                        }
                        
                        if !tForSelfFforOthers && !tForEnterFforExit && tForOwnerFforPassenger && tForSourceFForDestination && eventForUserId == loggedInUserId() {
                            // to check if car owner started from my source (destination/home) location
                            isCarOwnerStartedFromMySourceLocation = true; continue
                        }
                        if !tForSelfFforOthers && tForEnterFforExit && tForOwnerFforPassenger && !tForSourceFForDestination && eventForUserId == loggedInUserId() {
                            // to check if car owner reached to my destination (destination/home) location
                            isCarOwnerReachedToMyDestinationLocation = true; continue
                        }
                        if tForSelfFforOthers && !tForEnterFforExit && !tForOwnerFforPassenger && tForSourceFForDestination && eventForUserId == loggedInUserId() {
                            // to check if i started from to my source (destination/home) location
                            didIStartedFromMySourceLocation = true; continue
                        }
                        if tForSelfFforOthers && tForEnterFforExit && !tForOwnerFforPassenger && !tForSourceFForDestination && eventForUserId == loggedInUserId() {
                            // to check if i reached to my destination (destination/home) location
                            didIReachedToMyDestinationLocation = true; continue
                        }
                    }
                }
            }
            if isCarOwnerStartedFromMySourceLocation && isCarOwnerReachedToMyDestinationLocation && didIStartedFromMySourceLocation && didIReachedToMyDestinationLocation {
                isMyRideCompleted = true
                if isNotNull(carOwnerUserId){
                    rideCompletedWithUsers = [carOwnerUserId!]
                }
            }
        }
        return isMyRideCompleted
    }
    
    func prepare() {
        if allRideEventMessages.count == 0 {
            ServicesManager.instance().chatService.earlierMessages(withChatDialogID: self.groupChatDialogue!.id!) { (response, messages) in
                if isNotNull(messages) && messages!.count > 0{
                    ServicesManager.instance().chatService.earlierMessages(withChatDialogID: self.groupChatDialogue!.id!) { (response, messages) in
                        if isNotNull(messages) && messages!.count > 0{
                            ServicesManager.instance().chatService.earlierMessages(withChatDialogID: self.groupChatDialogue!.id!) { (response, messages) in
                                if isNotNull(messages) && messages!.count > 0{
                                    ServicesManager.instance().chatService.earlierMessages(withChatDialogID: self.groupChatDialogue!.id!) { (response, messages) in
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    func loadEventMessges() {
        allRideEventMessages.removeAllObjects()
        if let messages = ServicesManager.instance().chatService.messagesMemoryStorage.messages(withDialogID: self.groupChatDialogue!.id!) as [QBChatMessage]? {
            for m in messages{
                if let messageType = m.customParameters?.object(forKey: MESSAGE_TYPE_KEY){
                    if messageType as! String == GroupEvents.rideGroupEvent {
                        if let rideIdOnMessage = m.customParameters?.object(forKey: "rideId"){
                            if rideIdOnMessage as! String == self.rideId {
                                self.allRideEventMessages.add(m)
                            }
                        }
                    }
                }
            }
        }
        allRideEventMessages = NSMutableArray(array:(allRideEventMessages).sorted(by: { (m1, m2) -> Bool in
            return ((m1 as! QBChatMessage).dateSent!.isEarlierThanDate((m2 as! QBChatMessage).dateSent!))
        }))
    }
    
    func getValueFortForSelfFforOthers(_ eventId:String)->Bool {
        let bitValue1 = eventId.substring(0, to: 0) as NSString
        return bitValue1.isEqual(to: "1")
    }
    
    func getValueFortForHomeToDestinationFforDestinationToHome(_ eventId:String)->Bool {
        let bitValue2 = eventId.substring(1, to: 1) as NSString
        return bitValue2.isEqual(to: "1")
    }
    
    func getValueFortForSourceFForDestination(_ eventId:String)->Bool {
        let bitValue3 = eventId.substring(2, to: 2) as NSString
        return bitValue3.isEqual(to: "0")
    }
    
    func getValueFortForEnterFforExit(_ eventId:String)->Bool {
        let bitValue4 = eventId.substring(3, to: 3) as NSString
        return bitValue4.isEqual(to: "0")
    }
    
    func getValueFortForOwnerFforPassenger(_ eventId:String)->Bool {
        let bitValue5 = eventId.substring(4, to: 4) as NSString
        return bitValue5.isEqual(to: "0")
    }
}
