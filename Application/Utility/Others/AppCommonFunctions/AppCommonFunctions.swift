//
//  AppCommonFunctions.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

//MARK: - AppCommonFunctions : This singleton class implements some app specific functions which are frequently needed in application.

import Foundation
import UIKit
import Fabric
import Crashlytics
import Branch
import CTFeedback
import iVersion
import IQKeyboardManagerSwift
import Intents
import Accounts
import Social
import SDCAlertView
import SKSplashView
import URBMediaFocusViewController
import iRate
import NSLogger

//MARK: - Completion block
typealias ACFCompletionBlockType2 = () ->()
typealias ACFCompletionBlock = (_ returnedData :Any?) ->()
typealias ACFModificationBlock = (_ viewControllerObject :Any?) ->()
typealias ACFPreviewActionsDelegate = (_ viewControllerObject :Any?) ->Any
typealias RidePlannerModificationBlock = (_ selectedDayDate : Date , _ selectedDayTime : Date , _ isHomeToDestination : Bool) ->()

enum NextRideEstimatedMoment{
    case morning
    case evening
}

enum TravelRequestStatus {
    case notInitialisedYet
    case travelRequestSent
    case travelRequestReceived
    case travelRequestRejected
    case travelRequestRejectedByMe
    case travelRequestAccepted
    case travelCancelled
    case travelCompleted
}

enum TravelRequestAvailableAction {
    case invite
    case ignore
    case accept
    case reject
    case cancel
    case settle
    case feedback
    case none
}

enum UserType {
    case carOwner
    case passenger
    case unIdentified
}

enum RideStatus {
    case none
    case invitationSent
    case invitationReceived
    case confirmed
    case confirmedButDelayed
    case completed
}

enum OfferStatus {
    case available
    case notApplicable
    case notAvailable
    case expired
}

enum UserBoardingType {
    case carpool
    case peaceOfMind
    case regular
}

struct GroupEvents {
    static let location = "location"
    static let rideGroupEvent = "event"
    static let groupUpdateEvent = "group_event"
}

struct ThemeColor {
    
    static let colorType1 = UIColorHex(0x6F75B1)
    static let colorType2 = UIColorHex(0xDB92BD)
    static let colorType3 = UIColorHex(0xFFC66A)
    static let colorType4 = UIColorHex(0x595893)
    static let colorType5 = UIColorHex(0xF6617E)
    static let colorType6 = UIColorHex(0x59C6BB)
    
    static let colorType1String = "#6F75B1"
    static let colorType2String = "#DB92BD"
    static let colorType3String = "#FFC66A"
    static let colorType4String = "#595893"
    static let colorType5String = "#F6617E"
    static let colorType6String = "#59C6BB"
    
    public func getColor(type:String)->UIColor {
        switch type {
        case ThemeColor.colorType1String:
            return ThemeColor.colorType1
        case ThemeColor.colorType2String:
            return ThemeColor.colorType2
        case ThemeColor.colorType3String:
            return ThemeColor.colorType3
        case ThemeColor.colorType4String:
            return ThemeColor.colorType4
        case ThemeColor.colorType5String:
            return ThemeColor.colorType5
        case ThemeColor.colorType6String:
            return ThemeColor.colorType6
        default:
            return ThemeColor.colorType1
        }
    }
    
    public func getThemeColorName(index:Int)->String {
        switch index {
        case 1:
            return ThemeColor.colorType1String
        case 2:
            return ThemeColor.colorType2String
        case 3:
            return ThemeColor.colorType3String
        case 4:
            return ThemeColor.colorType4String
        case 5:
            return ThemeColor.colorType5String
        case 6:
            return ThemeColor.colorType6String
        default:
            return ThemeColor.colorType1String
        }
    }
    
    public func getIndexColorName(color:String)->Int {
        switch color {
        case ThemeColor.colorType1String:
            return 1
        case ThemeColor.colorType2String:
            return 2
        case ThemeColor.colorType3String:
            return 3
        case ThemeColor.colorType4String:
            return 4
        case ThemeColor.colorType5String:
            return 5
        case ThemeColor.colorType6String:
            return 6
        default:
            return 1
        }
    }
    
}

func Acf()->AppCommonFunctions{
    return AppCommonFunctions.sharedInstance
}

class AppCommonFunctions: NSObject , UITabBarControllerDelegate, QMChatServiceDelegate, QMChatConnectionDelegate , RESideMenuDelegate{
    
    var completionBlock: ACFCompletionBlock?
    var navigationController: UINavigationController?
    let mediaFocusController = URBMediaFocusViewController()
    var locationCache = NSMutableDictionary()
    var deviceToken : Data?
    var presenceInformation = NSMutableDictionary()
    var referUrl:String?
    var showPushNotificationsOnPopup = false
    var feedbackPopupShown = NSMutableDictionary()
    var launchOptions:[AnyHashable: Any]?
    var isUpdatingCityCompanyCollegeCarOptionsList = false
    var unReadMessageCount : Int?
    var name:String?
    var oUPVNavigationControllerKey = UINavigationController()
    var oUPVUserInfoKey = NSDictionary()
    var oUPVUserIdKey = ""
    var travellersListProcessingDetails = NSMutableDictionary()
    let locationManager = CLLocationManager()
    
    weak var sideMenuViewController : SideMenuViewController?
    weak var sideMenuController : RESideMenu?
    
    var carpoolHomeScreenController : UITabBarController?
    weak var rideHomeVC : RideHomeViewController?
    weak var rideDetailVC : RideDetailViewController?
    
    var pOMHomeScreenController : UINavigationController?
    
    static let sharedInstance : AppCommonFunctions = {
        let instance = AppCommonFunctions()
        return instance
    }()
    
    var timeFormatter: DateFormatter {
        struct Static {
            static let instance : DateFormatter = {
                let formatter = DateFormatter()
                formatter.dateFormat = "HH:mm:ss"
                return formatter
            }()
        }
        return Static.instance
    }
    
    var dateFormatter: DateFormatter {
        struct Static {
            static let instance : DateFormatter = {
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd"
                return formatter
            }()
        }
        return Static.instance
    }
    
    fileprivate override init() {
        
    }
    
    func prepareForStartUp(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [AnyHashable: Any]?){
        developerOptionsLoadAppRequirementsIfAny()
        self.launchOptions = launchOptions
        setupDatabase()
        initialSetup()
        setupForBranch(application, didFinishLaunchingWithOptions: launchOptions)
        setUpVersionReminder()
        setupForAnalytics()
        setupOtherSettings()
        setupQuickBlox()
        showRequiredScreen()
        setUpLogger()
        execMain({[weak self]  in guard let `self` = self else { return }
            self.syncNotificationsFromServer()
            },delay:0)
        execMain({[weak self]  in guard let `self` = self else { return }
            self.updateCityCompanyCollegeCarOptionsList()
            },delay:1)
        execMain({[weak self]  in guard self != nil else { return }
            validateCurrentUser()
        })
        execMain({[weak self]  in guard self != nil else { return }
            Lem().doRequiredProcessing()
        })
        execMain({[weak self]  in guard let `self` = self else { return }
            self.setupURBMediaFocusController()
        })
        self.setupIQKeyboardManagerEnabled()
        execMain({[weak self]  in guard let `self` = self else { return }
            self.setupForApplicationRater()
            self.requestLocationPermissionInAdvance()
            },delay:7)
        execMain({[weak self]  in guard let `self` = self else { return }
            self.setupAppUserStatusOptions()
        })
        execMain({[weak self]  in guard let `self` = self else { return }
            self.setupCrashlytics()
        })
        execMain({[weak self]  in guard let `self` = self else { return }
            self.syncSetting()
        })
        execMain({[weak self]  in guard let `self` = self else { return }
            self.setupForWeeklyPlannerNotifications()
        })
        execMain({[weak self]  in guard let `self` = self else { return }
            self.updateBadgeValues()
            },delay:3)
        execMain({[weak self]  in guard let `self` = self else { return }
            self.performNecessaryUpdateToServer()
            },delay:18)
        execMain({[weak self]  in guard let `self` = self else { return }
            self.performNecessaryUpdateFromTheServer()
            },delay:48)
    }
    
    func setUpLogger(){
        if ENABLE_LOGGING {
            LoggerSetupBonjourForBuildUser()
        }
    }
    
    func setUpVersionReminder(){
        iVersion.sharedInstance().updatePriority = .medium
    }
    
    func setupDatabase() {
        Dbm().setupCoreDataDatabase()
    }
    
    func retryInitialisingBranch() {
        setupForBranch(UIApplication.shared, didFinishLaunchingWithOptions: self.launchOptions)
    }
    
    func checkAndUpdateBranchShareUrlIfRequired() {
        execBackground({[weak self] in guard let `self` = self else { return }
            if isNull(self.referUrl){
                self.referUrl = CacheManager.sharedInstance.loadObject(CACHED_REFER_SHARE_URL) as? String
            }
            if isUserLoggedIn(){
                let userInfo = Dbm().getUserInfo()
                if isNull(self.referUrl) && isNotNull(userInfo!.refferalCode){
                    execBackground({[weak self] in guard let `self` = self else { return }
                        if let urlToShare = Branch.getInstance().getShortURL(withParams: ["referralCode":userInfo!.refferalCode!]) {
                            self.referUrl = urlToShare
                            CacheManager.sharedInstance.saveObject(self.referUrl , identifier: CACHED_REFER_SHARE_URL)
                        }
                    })
                }
            }
        })
    }
    
    func setupForBranch(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [AnyHashable: Any]?){
        execBackground({[weak self] in guard let `self` = self else { return }
            self.launchOptions = launchOptions
            Branch.getInstance().initSession(launchOptions: launchOptions) { (params, error) in
                if error != nil {
                    logMessage(error)
                }else{
                    self.checkAndUpdateBranchShareUrlIfRequired()
                    self.checkForReferals()
                    checkAndReportCampaignInstallEventIfRequired()
                }
            }
        })
    }
    
    func checkForReferals() {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(AppCommonFunctions.checkForReferalsPrivate), object: nil)
        self.perform(#selector(AppCommonFunctions.checkForReferalsPrivate), with: nil, afterDelay: 2)
    }
    
    func checkForReferalsPrivate() {
        execMain({[weak self] in guard let `self` = self else { return }
            if let parameters = Branch.getInstance().getFirstReferringParams(){
                logMessage("\nFIRST REFERRING DETAILS : \n\(parameters as! NSDictionary)\n")
                self.checkForReferals(parameters: parameters)
            }
            if let parameters = Branch.getInstance().getLatestReferringParams(){
                logMessage("\nLATEST REFERRING DETAILS : \n\(parameters as! NSDictionary)\n")
                self.checkForReferals(parameters: parameters)
            }
            },delay:0)
    }
    
    func checkForReferals(parameters:[AnyHashable:Any]){
        if let referalType = parameters["referral_type"] as? String{
            if referalType == "group_invitation" {
                self.checkExpectedOnBoardingTypeAndUpdateAppPreferences(parameters: parameters)
                self.checkGroupInvitationAndProcessIfRequired(parameters: parameters)
            }else if referalType == "app_download" {
                self.checkExpectedOnBoardingTypeAndUpdateAppPreferences(parameters: parameters)
            }
        }
    }
    
    func checkExpectedOnBoardingTypeAndUpdateAppPreferences(parameters:[AnyHashable : Any]){
        let clickedTimestamp = safeString(parameters["+click_timestamp"])
        if isNotNull(clickedTimestamp){
            let date = Date(timeIntervalSince1970: safeDouble(clickedTimestamp))
            if date.isLaterThanDate(Date().dateBySubtractingMinutes(MAXIMUM_DELAY_ALLOWED_FOR_REFFERALS)){
                let setting = Dbm().getSetting()
                if let boardingType = parameters["on_boarding_type"] as? String{
                    if boardingType == "carpool" {
                        setting.userBoardingType = "carpool"
                    }else if boardingType == "pom" {
                        setting.userBoardingType = "pom"
                    }else{
                        setting.userBoardingType = "regular"
                    }
                }else{
                    setting.userBoardingType = "regular"
                }
                Dbm().saveChanges()
                NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_APP_DOWNLOAD_REFFERAL_UPDATED), object: nil)
            }
        }
    }
    
    func checkGroupInvitationAndProcessIfRequired(parameters:[AnyHashable : Any]){
        if isUserLoggedIn(){
            let groupId = parameters["group_id"]
            let clickedTimestamp = safeString(parameters["+click_timestamp"])
            if isNotNull(clickedTimestamp){
                let date = Date(timeIntervalSince1970: safeDouble(clickedTimestamp))
                if date.isLaterThanDate(Date().dateBySubtractingMinutes(MAXIMUM_DELAY_ALLOWED_FOR_REFFERALS)){
                    execMain({
                        let information = NSMutableDictionary()
                        copyData(groupId, destinationDictionary: information, destinationKey: "groupId", methodName: #function)
                        let scm = ServerCommunicationManager()
                        scm.responseErrorOption = ResponseErrorOption.dontShowErrorResponseMessage
                        scm.joinGroup(information) { (responseData) in
                            if isNotNull(responseData){
                                self.syncGroupsAndPlaces {
                                    Lem().doRequiredProcessing()
                                }
                            }
                        }
                    },delay:Double.random(min: 1.0, max: 5.0))
                }
            }
        }
    }
    
    func setupURBMediaFocusController(){
        mediaFocusController.shouldBlurBackground = true
        mediaFocusController.shouldDismissOnTap  = true
        mediaFocusController.shouldDismissOnImageTap  = true
    }
    
    func setupForApplicationRater(){
        iRate.sharedInstance().daysUntilPrompt = 5
        iRate.sharedInstance().usesUntilPrompt = 5
    }
    
    func askUserForHisExperience(){
        trackScreenLaunchEvent(SLE_RATE_US_SCREEN)
        iRate.sharedInstance().promptForRating()
    }
    
    func performNecessaryUpdateToServer(){
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(AppCommonFunctions.performNecessaryUpdateToServerPrivate), object: nil)
        self.perform(#selector(AppCommonFunctions.performNecessaryUpdateToServerPrivate), with: nil, afterDelay: 10)
    }
    
    func performNecessaryUpdateFromTheServer(){
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(AppCommonFunctions.performNecessaryUpdateFromTheServerPrivate), object: nil)
        self.perform(#selector(AppCommonFunctions.performNecessaryUpdateFromTheServerPrivate), with: nil, afterDelay: 10)
    }
    
    func performNecessaryUpdateToServerPrivate(){
        if(isInternetConnectivityAvailable(true)==false){return}
        execMain({[weak self]  in guard let `self` = self else { return }
            self.updateMyTravelPreferences()
            },delay:0)
        
        execMain({[weak self]  in guard let `self` = self else { return }
            self.updateSettingsToServerIfRequired()
            },delay:6)
        
        execMain({[weak self]  in guard let `self` = self else { return }
            self.updateFavouriteUsersPrioritySettingsOnServerIfRequired()
            },delay:13)
        
        if isSystemReadyToProcessThis(){
            execMain({[weak self]  in guard self != nil else { return }
                Acf().updateProfileToServerIfRequired({ (returnedData) in})
                },delay:21)
        }
        execMain({[weak self]  in guard let `self` = self else { return }
            self.updateUserProfilePictureIfRequired()
            },delay:29)
    }
    
    
    func performNecessaryUpdateFromTheServerPrivate(){
        if(isInternetConnectivityAvailable(true)==false){return}
        execMain({[weak self]  in guard let `self` = self else { return }
            self.updateUserProfileInformationFromServerIfRequired()
            },delay:1)
        execMain({[weak self]  in guard let `self` = self else { return }
            self.updateSettingsFromServerIfRequired()
            },delay:13)
        execMain({[weak self]  in guard let `self` = self else { return }
            self.updateQBUsersCacheFromServerIfRequired(nil)
            },delay:25)
        execMain({[weak self]  in guard let `self` = self else { return }
            self.updateCityCompanyCollegeCarOptionsList()
            },delay:37)
    }
    
    func setupAppUserStatusOptions(){
        execMain({[weak self]  in guard self != nil else { return }
            let statusOptions = Dbm().getAllStatus()
            if statusOptions.count < 10 {
                Dbm().addStatus(["status":"Available"])
                Dbm().addStatus(["status":"Can't talk , Chat only"])
                Dbm().addStatus(["status":"Driving"])
                Dbm().addStatus(["status":"Busy"])
                Dbm().addStatus(["status":"Do Not Disturb"])
                Dbm().addStatus(["status":"On Leave"])
                Dbm().addStatus(["status":"Looking for Co-traveller"])
                Dbm().addStatus(["status":"In a Meeting"])
                Dbm().addStatus(["status":"May be i can't ride Today"])
                Dbm().addStatus(["status":"At the Movies"])
            }
        })
    }
    
    //MARK: - Quickblox setup
    
    func managePushNotificationsOnQuickBlox(){
        func suscribeToPushNotifications(){
            let deviceIdentifier: String = UIDevice.current.identifierForVendor!.uuidString
            let subscription: QBMSubscription! = QBMSubscription()
            subscription.notificationChannel = .APNS
            subscription.deviceUDID = deviceIdentifier
            subscription.deviceToken = deviceToken
            QBRequest.createSubscription(subscription, successBlock: { (response: QBResponse!, objects: [QBMSubscription]?) -> Void in
            }) { (response: QBResponse!) -> Void in
            }
        }
        func unSuscribeFromPushNotifications(){
            let deviceIdentifier = UIDevice.current.identifierForVendor!.uuidString
            QBRequest.unregisterSubscription(forUniqueDeviceIdentifier: deviceIdentifier, successBlock: { (response) in
            }, errorBlock: { (error) in
            })
        }
        let settings = Dbm().getSetting()
        if isNotNull(deviceToken) {
            if isUserLoggedIn(){
                if ServicesManager.instance().isAuthorized(){
                    if safeBool(settings.notificationForMobileOption13) {
                        suscribeToPushNotifications()
                    }else{
                        unSuscribeFromPushNotifications()
                    }
                }
            }
        }
    }
    
    func validateAndUpdateEventMessagesDateSent(){
        //first of all Y i am doing this
        //sometimes when event message got delayed at sender end ,and sent later , the date sent gets updated with real send date but not real event date
        //here i will write logic to detect such case and correct the same in local database , so that in chat screen it looks in correct order.
        
        let userInfo = Dbm().getUserInfo()
        if isNotNull(userInfo?.userId) &&
            isNotNull(userInfo?.name) &&
            isQBReadyForItsUserDependentServices() &&
            ServicesManager.instance().isAuthorized() {
            let allRidersGroup = NSMutableArray()
            let allDialogues = dialogs() as! [QBChatDialog]
            for cd in allDialogues {
                if cd.type != .private {
                    if let dialogueName = cd.name{
                        if (dialogueName as NSString).contains(RIDE_GROUP_SUFFIX) {
                            allRidersGroup.add(cd)
                        }
                    }
                }
            }
            for d in allRidersGroup {
                if let messages = ServicesManager.instance().chatService.messagesMemoryStorage.messages(withDialogID: (d as! QBChatDialog).id!) as [QBChatMessage]? {
                    for m in messages{
                        if let messageType = m.customParameters?.object(forKey: MESSAGE_TYPE_KEY){
                            if messageType as! String == GroupEvents.rideGroupEvent ||
                                messageType as! String == GroupEvents.groupUpdateEvent
                            {
                                var eventDate = m.dateSent
                                if let eD = m.customParameters?.object(forKey: "eventDate") as? String {
                                    eventDate = eD.dateValueFromMilliSeconds() as Date
                                }
                                m.dateSent = eventDate
                                ServicesManager.instance().chatService.messagesMemoryStorage.update(m)
                            }
                        }
                    }
                }
            }
        }
    }
    
    func checkForAnyPendingEventsOnGroupsAndSendItAgain() {
        execMain({[weak self]  in guard let `self` = self else { return }
            let userInfo = Dbm().getUserInfo()
            if isNotNull(userInfo?.userId) &&
                isNotNull(userInfo?.name) &&
                self.isQBReadyForItsUserDependentServices() &&
                ServicesManager.instance().isAuthorized() {
                let groups = NSMutableArray()
                let allDialogues = self.dialogs() as! [QBChatDialog]
                for cd in allDialogues {
                    if cd.type != .private {
                        groups.add(cd)
                    }
                }
                var delay = 0.0
                for d in groups {
                    if let messages = ServicesManager.instance().chatService.messagesMemoryStorage.messages(withDialogID: (d as! QBChatDialog).id!) as [QBChatMessage]? {
                        for m in messages{
                            if let messageType = m.customParameters?.object(forKey: MESSAGE_TYPE_KEY) as? String{
                                if messageType == GroupEvents.rideGroupEvent ||
                                    messageType == GroupEvents.rideGroupEvent
                                {
                                    if isNotNull(Dbm().getFailedQBMessage(["messageId":m.id!])) {
                                        delay = delay + GAP_IN_BETWEEN_CONSECUTIVE_RIDE_EVENT_MESSAGES
                                        execMain({[weak self]  in guard let `self` = self else { return }
                                            if isNotNull(Dbm().getFailedQBMessage(["messageId":m.id!])) {
                                                self.sendMessage(m, chatDialogue: d as! QBChatDialog)
                                            }
                                            },delay:delay)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        })
    }
    
    func checkForProfilePictureUpdate(){
        
        func showImage(image:UIImage){
            
            let alertController = UIAlertController(title: "This looks awesome 😃", message: "\nWould you like to use this as your profile picture?", preferredStyle: .actionSheet)
            
            let imageView = UIImageView(image: image)
            imageView.backgroundColor = UIColor.white
            imageView.layer.cornerRadius = 12
            imageView.layer.borderWidth = 4
            imageView.layer.borderColor = UIColor.white.cgColor
            imageView.layer.masksToBounds = true
            imageView.clipsToBounds = true
            
            alertController.view.addSubview(imageView)
            
            imageView.contentMode = .scaleAspectFill
            imageView.translatesAutoresizingMaskIntoConstraints = false
            imageView.centerXAnchor.constraint(equalTo: alertController.view.centerXAnchor,constant:0).isActive = true
            imageView.topAnchor.constraint(equalTo: alertController.view.topAnchor,constant:-20-DEVICE_WIDTH).isActive = true
            imageView.heightAnchor.constraint(equalToConstant: DEVICE_WIDTH - 20 ).isActive = true
            imageView.widthAnchor.constraint(equalToConstant: DEVICE_WIDTH - 20 ).isActive = true
            
            alertController.addAction(UIAlertAction(title: "Set as Profile Picture", style: UIAlertActionStyle.cancel, handler: { (action) in
                
                execMain({[weak self]  in guard let `self` = self else { return }
                    
                    let picname = "\(Dbm().getUserInfo()!.userId!)_\(currentTimeStamp()).jpg"
                    let information = ["image":image,"imageName":picname] as NSDictionary
                    showNotification(MESSAGE_TEXT___UPLOADING_YOUR_NEW_PROFILE_PICTURE, showOnNavigation: false, showAsError: false,duration: 5)
                    let scm = ServerCommunicationManager()
                    scm.responseErrorOption = .dontShowErrorResponseMessage
                    scm.returnFailureResponseAlso = true
                    scm.returnFailureUnParsedDataIfParsingFails = true
                    scm.uploadImage(information) { [weak self] (responseData) -> () in
                        if isNotNull(responseData){
                            let information = ["imageName":picname] as NSDictionary
                            let scm = ServerCommunicationManager()
                            scm.returnFailureResponseAlso = true
                            scm.responseErrorOption = .dontShowErrorResponseMessage
                            scm.updateProfilePicture(information) { (responseData) -> () in
                                Acf().updateUserProfileInformationFromServerIfRequired()
                                execMain({[weak self] in guard let `self` = self else { return }
                                    
                                    showNotification(MESSAGE_TEXT___PROFILE_PICTURE_COMMENT, showOnNavigation: false, showAsError: false,duration: 5)
                                    }, delay: 5)
                            }
                        }
                    }
                    
                    },delay: 0.6)
                
            }))
            
            alertController.addAction(UIAlertAction(title: "dismiss", style: UIAlertActionStyle.destructive, handler: { (action) in
                
            }))
            
            self.navigationController?.present(alertController, animated: true, completion: nil)
            
        }
        
        if ENABLE_FACEBOOK_PROFILE_PICTURE_SUGGESTION && isUserLoggedIn() && isInternetConnectivityAvailable(false){
            let userInfo = Dbm().getUserInfo()
            if safeString(userInfo?.picture).contains(loggedInUserId()) {
                //user already set his profile picture
            }else{
                let accountStore = ACAccountStore()
                let facebookTypeAccount = accountStore.accountType(withAccountTypeIdentifier: ACAccountTypeIdentifierFacebook)
                accountStore.requestAccessToAccounts(with: facebookTypeAccount, options: [ACFacebookAppIdKey : "162084094285925",ACFacebookPermissionsKey:["email"]]) { (status, error) in
                    if status {
                        if accountStore.accounts(with: facebookTypeAccount).count > 0 {
                            let account = accountStore.accounts(with: facebookTypeAccount).last! as! ACAccount
                            if let userId = account.value(forKeyPath: "properties.uid") as? AnyObject{
                                let facebookProfilePictureUrl = "http://graph.facebook.com/\(userId)/picture?width=9999"
                                SDWebImageManager.shared().downloadImage(with: URL(string:facebookProfilePictureUrl), options: SDWebImageOptions.allowInvalidSSLCertificates, progress: { (x, y) in
                                    
                                }, completed: { (image, e, c, com, url) in
                                    if image != nil {
                                        if canPerformThisEvent(48*60, eventName: "imageSuggestionFacebook") {
                                            showImage(image: image!)
                                        }
                                    }
                                })
                            }
                        }
                    }
                }
            }
        }
    }
    
    func setupQuickBlox(){
        QBSettings.setApplicationID(QUICKBLOX_APPLICATION_ID)
        QBSettings.setAuthKey(QUICKBLOX_AUTH_KEY)
        QBSettings.setAuthSecret(QUICKBLOX_AUTH_SECRET)
        QBSettings.setAccountKey(QUICKBLOX_ACCOUNT_KEY)
        QBSettings.setAutoReconnectEnabled(true)
        QBSettings.setLogLevel(QBLogLevel.nothing)
        QBSettings.disableXMPPLogging()
        QBSettings.setApiEndpoint(QUICKBLOX_API_END_POINT, chatEndpoint: QUICKBLOX_CHAT_END_POINT, forServiceZone: QBConnectionZoneType.production)
        QBSettings.setServiceZone(QBConnectionZoneType.production)
        NotificationCenter.default.addObserver(self, selector: #selector(AppCommonFunctions.someMessageReceived(_:)), name: NSNotification.Name(rawValue: NOTIFICATION_SOME_MESSAGE_RECEIVED), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AppCommonFunctions.logoutAndThenLoginAgainQuickBlox), name: NSNotification.Name(rawValue: "RESET_QB"), object: nil)
        loginToQuickBlox()
    }
    
    func chatGroupName(group:Group)->(String){
        return "\(safeString(group.groupId))"
    }
    
    func ridersGroupName()->(String){
        let userId = loggedInUserId()
        return "\(userId)\(RIDE_GROUP_SEPERATOR)\(RIDE_GROUP_SUFFIX)"
    }
    
    func ridersGroupNameCarOwner(_ userId:String)->(String){
        return "\(userId)\(RIDE_GROUP_SEPERATOR)\(RIDE_GROUP_SUFFIX)"
    }
    
    func updateQBUserDetails(){
        managePushNotificationsOnQuickBlox()
        let userInfo = Dbm().getUserInfo()
        if isNotNull(userInfo?.userId) &&
            isNotNull(userInfo?.name) &&
            isQBReadyForItsUserDependentServices() &&
            ServicesManager.instance().isAuthorized() {
            ServicesManager.instance().currentUser()!.login = Acf().getQuickBloxUserName(loggedInUserId())
            ServicesManager.instance().currentUser()!.password = Acf().getQuickBloxPassword(loggedInUserId())
            let userInfo = Dbm().getUserInfo()
            let qBUpdateUserParameters = QBUpdateUserParameters()
            qBUpdateUserParameters.fullName = userInfo?.name
            qBUpdateUserParameters.website = getUserProfilePictureUrlFromFileName(userInfo?.picture)
            qBUpdateUserParameters.email = "\(loggedInUserId())@\(loggedInUserId()).com"
            let appVersion = UIApplication.appVersion()
            let appBuild = UIApplication.appBuild()
            #if DEBUG
                let tag = String("iOSV\(appVersion)B\(appBuild)D").replacingOccurrences(of: ".", with: "")
            #else
                let tag = String("iOSV\(appVersion)B\(appBuild)P").replacingOccurrences(of: ".", with: "")
            #endif
            qBUpdateUserParameters.tags = [tag]
            QBRequest.updateCurrentUser(qBUpdateUserParameters, successBlock: { (response, user) in
                logMessage("UpdateCurrentUser : success")
            }) { (response) in
                logMessage("UpdateCurrentUser : failed \(response)")
            }
        }
    }
    
    func disConnectWithQuickBlox() {
        let userInfo = Dbm().getUserInfo()
        if isNotNull(userInfo?.userId)&&isNotNull(userInfo?.name)&&isNotNull(userInfo?.email)&&isQBReadyForItsUserDependentServices(){
            if isUserLoggedIn() {
                ServicesManager.instance().currentUser()!.password = Acf().getQuickBloxPassword(loggedInUserId())
                ServicesManager.instance().chatService.disconnect { (error) in
                    if isNotNull(error){
                        logMessage("disConnectWithQuickBlox ERROR: " + error.debugDescription)
                    }else{
                        logMessage("QuickBlox Disconnected Successfully")
                    }
                }
            }
        }
    }
    
    func connectWithQuickBlox() {
        let userInfo = Dbm().getUserInfo()
        if isNotNull(userInfo?.userId)&&isNotNull(userInfo?.name)&&isNotNull(userInfo?.email)&&isQBReadyForItsUserDependentServices(){
            if isUserLoggedIn() {
                ServicesManager.instance().currentUser()!.password = Acf().getQuickBloxPassword(loggedInUserId())
                ServicesManager.instance().chatService.connect { (error) in
                    if isNotNull(error){
                        logMessage("connectWithQuickBlox ERROR: " + error.debugDescription)
                    }else{
                        logMessage("QuickBlox Connected Successfully")
                    }
                }
            }
        }
    }
    
    func logoutAndThenLoginAgainQuickBlox() {
        logoutFromQuickBlox()
        execMain({[weak self]  in guard let `self` = self else { return }
            self.loginToQuickBlox()
            },delay: 2)
    }
    
    fileprivate var delayCounter = 1.0
    fileprivate var dateWhenStarted : Date?
    fileprivate var dateWhenFinished : Date?
    
    func loginToQuickBlox(){
        logMessage("LQB_START")
        if isNull(dateWhenStarted){
            dateWhenStarted = Date()
        }
        func cancelScheduledCallToLoginToQuickBlox(){
            logMessage("cancelScheduledCallToLoginToQuickBlox")
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(AppCommonFunctions.loginToQuickBlox), object: nil)
        }
        func reScheduleLoginToQuickBlox(){
            cancelScheduledCallToLoginToQuickBlox()
            logMessage("reScheduleLoginToQuickBlox => after \(delayCounter) seconds")
            self.perform(#selector(AppCommonFunctions.loginToQuickBlox), with: nil, afterDelay: delayCounter)
        }
        let userInfo = Dbm().getUserInfo()
        if isNotNull(userInfo?.userId)&&isNotNull(userInfo?.name)&&isNotNull(userInfo?.email)&&ServicesManager.instance().isAuthorized()==false{
            if(isInternetConnectivityAvailable(false)==false){
                reScheduleLoginToQuickBlox()
                return
            }
            let user = Dbm().getUserInfo()
            let qbUser = QBUUser()
            qbUser.login = getQuickBloxUserName((user?.userId)!)
            qbUser.password = getQuickBloxPassword((user?.userId)!)
            qbUser.fullName = userInfo?.name
            qbUser.website = getUserProfilePictureUrlFromFileName(userInfo?.picture)
            func login(){
                logMessage("trying to login in quickblox")
                ServicesManager.instance().logIn(with: qbUser, completion:{
                    [unowned self] (success:Bool,  errorMessage: String?) -> Void in
                    if (success) {
                        logMessage("login succeeded on quickblox")
                        logMessage("LQB_END")
                        self.dateWhenFinished = Date()
                        logMessage("Quickblox took \(self.dateWhenFinished!.secondsAfterDate(self.dateWhenStarted!)) seconds to complete the login process")
                        ServicesManager.instance().chatService.removeDelegate(self)
                        ServicesManager.instance().chatService.addDelegate(self)
                        self.fetchDialogs()
                        self.updateQBUserDetails()
                        ServicesManager.instance().contactListService.refreshPresence()
                    }else{
                        logMessage("login to quickblox  failed ! trying signup...")
                        signup()
                    }
                })
            }
            func signup(){
                logMessage("trying to signup in quickblox")
                QBRequest.signUp(qbUser, successBlock: { (response, userCreated) -> Void in
                    logMessage("signup succeeded on quickblox")
                    login()
                }, errorBlock: { (response) -> Void in
                    logMessage("signUp to quickblox  failed !")
                    reScheduleLoginToQuickBlox()
                })
            }
            delayCounter = delayCounter*2
            if delayCounter > 64 {
                delayCounter = 1.0
            }
            cancelScheduledCallToLoginToQuickBlox()
            login()
        }else if isNull(userInfo){
            logoutFromQuickBlox()
        }
    }
    
    func getWeeklyPlannerNotificationCount() -> UInt {
        var notificationCount = 0 as UInt
        let plans = CacheManager.sharedInstance.loadObject("WEEKLY_RIDE_PLAN_CACHE") as? NSMutableArray
        if isNotNull(plans){
            for p in plans! {
                if isAnyUnreadNotifications(p as! NSDictionary){
                    notificationCount = notificationCount + 1
                }
            }
        }
        return notificationCount
    }
    
    func setupForWeeklyPlannerNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(AppCommonFunctions.fetchWeeklyPlannerDataAndUpdateNotifications), name: NSNotification.Name(rawValue: NOTIFICATION_NEED_USER_UPDATE), object: nil)
        fetchWeeklyPlannerDataAndUpdateNotifications()
    }
    
    func isAnyUnreadNotifications(_ plan:NSDictionary) -> Bool {
        let date = plan.object(forKey: "date") as? NSString
        let mode = plan.object(forKey: "mode") as? NSString
        if isNotNull(date) && isNotNull(mode){
            let key_notification = "\(date!)_\(mode!)_notification"
            return UserDefaults.standard.bool(forKey: key_notification)
        }
        return false
    }
    
    func setWeeklyPlanNotificationsAsRead(_ plans:NSMutableArray){
        for p in plans {
            let date = (p as AnyObject).object(forKey: "date") as? NSString
            let mode = (p as AnyObject).object(forKey: "mode") as? NSString
            if isNotNull(date) && isNotNull(mode){
                let key_notification = "\(date!)_\(mode!)_notification"
                UserDefaults.standard.set(false, forKey: key_notification)
            }
        }
        NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_WEEKLY_NOTIFICATION_COUNT_UPDATED), object: nil)
    }
    
    func getTomorrowsEstimatedTimeForModeAsPerWeeklyPlanner(_ trueForHomeToDestinationFalseforDestinationToHome:Bool)->Date?{
        var estimatedDate : Date?
        let plans = CacheManager.sharedInstance.loadObject("WEEKLY_RIDE_PLAN_CACHE") as? NSMutableArray
        if isNotNull(plans){
            for p in plans! {
                let date = (p as AnyObject).object(forKey: "date") as? String
                let time = (p as AnyObject).object(forKey: "etd") as? String
                let mode = (p as AnyObject).object(forKey: "mode") as? String
                if isNotNull(date) && isNotNull(time) && isNotNull(mode){
                    let dayDate = date!.dateValueType1()
                    let timeDate = getDate(time!)
                    let modeString = trueForHomeToDestinationFalseforDestinationToHome ? "0" : "1"
                    if dayDate.isTomorrow() && mode == modeString {
                        estimatedDate = Date.tomorrow().setTimeOfDate(timeDate.hour(), minute: timeDate.minute(), second: timeDate.second)
                    }
                }
            }
        }
        return estimatedDate
    }
    
    func getTodaysEstimatedTimeForModeAsPerWeeklyPlanner(_ trueForHomeToDestinationFalseforDestinationToHome:Bool)->Date?{
        var estimatedDate : Date?
        let plans = CacheManager.sharedInstance.loadObject("WEEKLY_RIDE_PLAN_CACHE") as? NSMutableArray
        if isNotNull(plans){
            for p in plans! {
                let date = (p as AnyObject).object(forKey: "date") as? String
                let time = (p as AnyObject).object(forKey: "etd") as? String
                let mode = (p as AnyObject).object(forKey: "mode") as? String
                if isNotNull(date) && isNotNull(time) && isNotNull(mode){
                    let dayDate = date!.dateValueType1()
                    let timeDate = getDate(time!)
                    let modeString = trueForHomeToDestinationFalseforDestinationToHome ? "0" : "1"
                    if dayDate.isToday() && mode == modeString {
                        estimatedDate = Date().setTimeOfDate(timeDate.hour(), minute: timeDate.minute(), second: timeDate.second)
                    }
                }
            }
        }
        return estimatedDate
    }
    
    func isTodaysRideCompleted(_ trueForHomeToDestinationFalseforDestinationToHome:Bool)->Bool{
        let plans = CacheManager.sharedInstance.loadObject("WEEKLY_RIDE_PLAN_CACHE") as? NSMutableArray
        if isNotNull(plans){
            for p in plans! {
                let date = (p as AnyObject).object(forKey: "date") as? String
                let time = (p as AnyObject).object(forKey: "etd") as? String
                let mode = (p as AnyObject).object(forKey: "mode") as? String
                if isNotNull(date) && isNotNull(time) && isNotNull(mode){
                    let dayDate = date!.dateValueType1()
                    let modeString = trueForHomeToDestinationFalseforDestinationToHome ? "0" : "1"
                    if dayDate.isToday() && mode == modeString {
                        let travelRequestStatus = getTravelRequestStatus(p as! NSDictionary)
                        if travelRequestStatus == .travelCompleted{
                            return true
                        }
                    }
                }
            }
        }
        return false
    }
    
    func checkAndUpdateRequiredWeeklyViewNotifications(_ plans:NSMutableArray){
        for p in plans {
            let date = (p as AnyObject).object(forKey: "date") as? NSString
            let mode = (p as AnyObject).object(forKey: "mode") as? NSString
            let rs_status = (p as AnyObject).object(forKey: "rs_status") as? NSString
            if isNotNull(date) && isNotNull(mode){
                let key_rs_status = "\(date!)_\(mode!)_rs_status"
                let key_notification = "\(date!)_\(mode!)_notification"
                if isNotNull(rs_status){
                    var lastSavedStatus = UserDefaults.standard.object(forKey: key_rs_status) as? String
                    if isNull(lastSavedStatus){
                        lastSavedStatus = "nullValue"
                    }
                    if (lastSavedStatus! as NSString).isEqual(to: rs_status as! String) {
                        //no update in rs_status
                    }else{
                        //there is an update in rs status , we should notify user for that
                        let situation = Acf().getTravelRequestStatus(p as! NSDictionary)
                        if situation == .notInitialisedYet {
                            // not a notification for user
                        }
                        else if situation == .travelRequestSent {
                            // not a notification for user
                        }
                        else if situation == .travelRequestReceived {
                            UserDefaults.standard.set(true, forKey: key_notification)
                        }
                        else if situation == .travelRequestRejected {
                            UserDefaults.standard.set(true, forKey: key_notification)
                        }
                        else if situation == .travelRequestRejectedByMe {
                            // not a notification for user
                        }
                        else if situation == .travelRequestAccepted {
                            UserDefaults.standard.set(true, forKey: key_notification)
                        }
                        else if situation == .travelCancelled {
                            UserDefaults.standard.set(true, forKey: key_notification)
                        }
                        else if situation == .travelCompleted {
                            UserDefaults.standard.set(true, forKey: key_notification)
                        }
                    }
                    UserDefaults.standard.set(rs_status!, forKey: key_rs_status)
                }else{
                    UserDefaults.standard.set("nullValue", forKey: key_rs_status)
                }
            }
        }
        NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_WEEKLY_NOTIFICATION_COUNT_UPDATED), object: nil)
    }
    
    func fetchWeeklyPlannerDataAndUpdateNotifications() {
        if isSystemReadyToProcessThis() {
            var plans = NSMutableArray()
            func prepareDataForDisplay(){
                var plansFromServer = CacheManager.sharedInstance.loadObject("WEEKLY_RIDE_PLAN_CACHE") as? NSMutableArray
                if isNull(plansFromServer){
                    plansFromServer = NSMutableArray()
                }
                let weeklyPlansTemplate = NSMutableArray()
                for i in 0...13 {
                    let isMorning = (i%2 == 0)
                    let date = Date().dateByAddingDays(Int(i/2))
                    let plan = NSMutableDictionary()
                    plan.setObject(date.weekdayToString(), forKey: "day" as NSCopying)
                    plan.setObject(date.toStringValue("yyyy-MM-dd"), forKey: "date" as NSCopying)
                    for p in plansFromServer! {
                        let wpDate = plan.object(forKey: "date") as? String
                        let pDate = (p as AnyObject).object(forKey: "date") as? String
                        let pEtd = (p as AnyObject).object(forKey: "etd") as? String
                        let mode = (p as AnyObject).object(forKey: "mode") as! NSString
                        if isNotNull(pDate) && isNotNull(pEtd) && isNotNull(mode){
                            if isMorning{
                                if (pDate as! NSString).isEqual(to: wpDate!) && mode.isEqual(to: "0"){
                                    if mode.isEqual(to: "0"){
                                        plan.removeAllObjects()
                                        plan.addEntries(from: p as! [AnyHashable: Any])
                                        break
                                    }
                                }
                            }else{
                                if (pDate as! NSString).isEqual(to: wpDate!) && mode.isEqual(to: "1"){
                                    plan.removeAllObjects()
                                    plan.addEntries(from: p as! [AnyHashable: Any])
                                    break
                                }
                            }
                        }
                    }
                    weeklyPlansTemplate.add(plan)
                }
                plans = weeklyPlansTemplate
                checkAndUpdateRequiredWeeklyViewNotifications(plans)
            }
            if isSystemReadyToProcessThis() {
                let scm = ServerCommunicationManager()
                scm.responseErrorOption = .dontShowErrorResponseMessage
                scm.getWeeklyRidePlan([:]) {[weak self] (responseData) -> () in guard self != nil else { return }
                    if let _ = responseData {
                        let plansFromServer = responseData?.object(forKey: "data") as? NSMutableArray
                        CacheManager.sharedInstance.saveObject(plansFromServer, identifier: "WEEKLY_RIDE_PLAN_CACHE")
                        saveThisDataForExtensionsAccess(plansFromServer, key: KEY_WEEKLY_PLANNER)
                        prepareDataForDisplay()
                    }
                }
            }
            prepareDataForDisplay()
        }
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(AppCommonFunctions.fetchWeeklyPlannerDataAndUpdateNotifications), object: nil)
        self.perform(#selector(AppCommonFunctions.fetchWeeklyPlannerDataAndUpdateNotifications), with: nil, afterDelay: WEEKLY_PLANS_AUTO_REFRESH_TIME)
    }
    
    func updateNotificationsInDatabase(notifications:[NSDictionary]?){
        if notifications !=  nil && notifications!.count > 0 {
            for notification in notifications! {
                let title = safeString(notification["title"])
                let message = safeString(notification["message"])
                let photo = safeString(notification["photo"])
                let date = safeString(notification["date"])
                let notifId = safeString(notification["notifId"])
                Dbm().addNotification(["title":"\(title)","message":"\(message)","photo":"\(photo)","date":"\(date)","notifId":"\(notifId)"])
            }
        }
    }
    
    func showRiderGroup(){
        Acf().showGroupChatScreen(ridersGroupName())
    }
    
    func getRidersGroup()->QBChatDialog?{
        let riderGroupName = self.ridersGroupName()
        let allDialogs = dialogs()
        if allDialogs.count > 0{
            for dialog in allDialogs {
                if (dialog as AnyObject).name == riderGroupName {
                    return dialog as? QBChatDialog
                }
            }
        }
        return nil
    }
    
    func getRidersGroupCarOwner(_ userId:String)->QBChatDialog?{
        let riderGroupName = self.ridersGroupNameCarOwner(userId)
        let allDialogs = dialogs()
        if allDialogs.count > 0{
            for dialog in allDialogs {
                if (dialog as AnyObject).name == riderGroupName {
                    return dialog as? QBChatDialog
                }
            }
        }
        fetchDialogs()
        return nil
    }
    
    func createRiderGroupIfNotExist(){
        if isUserLoggedIn() {
            if getChatDialogue(ridersGroupName()) == nil {
                createNewGroup(ridersGroupName(), userIds: [QB_USER_ADMIN_QBID])
            }
        }
    }
    
    func createPoMChatGroupsIfNotExist(){
        if isUserLoggedIn() {
            let groups = Dbm().getAllGroups()
            for g in groups{
                if safeBool(g.isAdmin) {
                    let name = chatGroupName(group: g)
                    if getChatDialogue(name) == nil {
                        createNewGroup(name, userIds: [QB_USER_ADMIN_QBID])
                    }
                }
            }
        }
    }
    
    func fetchDialogs() {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(AppCommonFunctions.fetchDialogsPrivate), object: nil)
        self.perform(#selector(AppCommonFunctions.fetchDialogsPrivate), with: nil, afterDelay: 0.5)
    }
    
    func fetchDialogsPrivate() {
        if isUserLoggedIn() {
            ServicesManager.instance().chatService.allDialogs(withPageLimit: kDialogsPageLimit, extendedRequest: nil, iterationBlock: { (response: QBResponse?, dialogObjects: [QBChatDialog]?, dialogsUsersIDS: Set<NSNumber>?, stop: UnsafeMutablePointer<ObjCBool>) -> Void in
            }, completion: { (response: QBResponse?) -> Void in
                guard response != nil && response!.isSuccess else { return }
                if (ServicesManager.instance().isAuthorized()) {
                    ServicesManager.instance().lastActivityDate = Date()
                    self.createRiderGroupIfNotExist()
                    self.createPoMChatGroupsIfNotExist()
                    self.updatePoMChatGroups()
                    scanAndDeleteChatDialogsWhoesPoMGroupsGotDeleted()
                }
            })
        }
    }
    
    func logoutFromQuickBlox(){
        let deviceIdentifier: String = UIDevice.current.identifierForVendor!.uuidString
        QBRequest.unregisterSubscription(forUniqueDeviceIdentifier: deviceIdentifier, successBlock: { (response: QBResponse!) -> Void in
            ServicesManager.instance().logout { () -> Void in }
        }) { (error: QBError?) -> Void in
            ServicesManager.instance().logout { () -> Void in }
        }
        ServicesManager.instance().lastActivityDate = nil
    }
    
    func getQuickBloxUserName(_ userId:String)->(String){
        return QB_USER_NAME_PREFIX + "\(userId)"
    }
    
    func getQuickBloxPassword(_ userId:String)->(String){
        return QB_USER_NAME_PREFIX + "\(userId)"
    }
    
    func getUserIdFromUserLogin(_ login:String)->(String){
        return login.replacingOccurrences(of: QB_USER_NAME_PREFIX, with: "")
    }
    
    func createNewGroup(_ name:String,userIds:NSArray){
        let logins = NSMutableArray()
        for userId in userIds {
            logins.add(getQuickBloxUserName(userId as! String))
        }
        func newGroup(_ name:String,qbUsers:[QBUUser]){
            NewChatViewController.createChat(name, users: qbUsers, completion: { (response, createdDialog) in
                if createdDialog != nil {
                    logMessage("successfully created a group \(name)")
                }
                if response != nil && response?.error != nil {
                    logMessage(response?.error?.error?.localizedDescription)
                }
            }, createGroup: true)
        }
        ServicesManager.instance().downloadUsers(logins, success: { (users) -> Void in
            if isNotNull(users){
                let newUsersTemporary = users
                let newUsers = NSMutableArray()
                for ut in newUsersTemporary! {
                    for l in logins {
                        if (ut.login as! NSString).isEqual(to: l as! String){
                            newUsers.add(ut)
                            break
                        }
                    }
                }
                if newUsers.count == 1 {
                    newGroup(name,qbUsers: newUsers as! [QBUUser])
                }else{
                    newGroup(name,qbUsers:[])
                }
            }
        }, error: { (error) -> Void in
            logMessage(error?.localizedDescription)
        })
    }
    
    func showChatScreen(_ dialog:QBChatDialog){
        if isNotNull(self.navigationController) && isSystemReadyToProcessThis() {
            if isQBReadyForItsUserDependentServices() {
                Acf().pushVC("ChatViewController", navigationController: self.navigationController, isRootViewController: false, animated: true, modifyObject: { (vc) in
                    (vc as! ChatViewController).dialog = dialog as? QBChatDialog
                })
            }
        }
    }
    
    func showWebViewController(_ workingMode:WorkingMode,customUrl:String?="",customTitle:String?=""){
        if isNotNull(self.navigationController) {
            Acf().pushVC("WebViewViewController", navigationController: self.navigationController, isRootViewController: false, animated: true, modifyObject: { (viewController) in
                (viewController as! WebViewViewController).workingMode = workingMode
                (viewController as! WebViewViewController).customUrl = customUrl
                (viewController as! WebViewViewController).customTitle = customTitle
            })
        }
    }
    
    func isQBReadyForItsUserDependentServices(loginImmediately:Bool=false)->Bool{
        if let _ = ServicesManager.instance().currentUser() {
            return true
        }
        if isUserLoggedIn(){
            if loginImmediately {
                NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(AppCommonFunctions.loginToQuickBlox), object: nil)
                self.perform(#selector(AppCommonFunctions.loginToQuickBlox), with: nil, afterDelay: 0)
            }else{
                NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(AppCommonFunctions.loginToQuickBlox), object: nil)
                self.perform(#selector(AppCommonFunctions.loginToQuickBlox), with: nil, afterDelay: 5)
            }
        }
        return false
    }
    
    func getChatDialog(dialogueName:String)->QBChatDialog?{
        if isQBReadyForItsUserDependentServices() {
            let allDialogs = dialogs()
            if allDialogs.count > 0{
                for dialog in allDialogs {
                    if (dialog as! QBChatDialog).name == name {
                        return (dialog as! QBChatDialog)
                    }
                }
            }
        }
        return nil
    }
    
    func showGroupChatScreen(_ name:String,from:UIViewController? = nil,themeColor:UIColor=APP_THEME_VOILET_COLOR,group:Group?=nil){
        if isQBReadyForItsUserDependentServices() {
            let allDialogs = dialogs()
            if allDialogs.count > 0{
                for dialog in allDialogs {
                    if (dialog as AnyObject).name == name {
                        Acf().pushVC("ChatViewController", navigationController: from?.navigationController ?? self.navigationController, isRootViewController: false, animated: true, modifyObject: { (viewController) in
                            (viewController as! ChatViewController).dialog = dialog as? QBChatDialog
                            (viewController as! ChatViewController).themeColor = themeColor
                            (viewController as! ChatViewController).group = group
                        })
                        break
                    }
                }
            }
        }
    }
    
    func showChatScreen(_ userId:String,from:UIViewController? = nil,themeColor:UIColor=APP_THEME_VOILET_COLOR,group:Group?=nil){
        if (userId as NSString).isEqual(to: loggedInUserId()){
            showNotification("You can not chat with your self !", showOnNavigation: false, showAsError: true)
        }else if isQBReadyForItsUserDependentServices() {
            Dbm().addUserId(userId)
            func showChatScreen(_ qbUser:QBUUser){
                NewChatViewController.createChat(nameToDisplay(qbUser, chatDialogue: nil), users: [qbUser], completion: { (response, createdDialog) in
                    if createdDialog != nil {
                        var navigationControllerToUse = self.navigationController
                        if isNotNull(from){
                            navigationControllerToUse?.navigationController
                        }
                        let viewController = getViewController("ChatViewController") as! ChatViewController
                        viewController.dialog = createdDialog
                        viewController.themeColor = themeColor
                        viewController.group = group
                        
                        if navigationControllerToUse!.viewControllers.last! is ChatViewController {
                            //already pushed , skip this time
                        }else{
                            navigationControllerToUse?.pushViewController(viewController, animated: true)
                        }
                    }
                    if response != nil && response?.error != nil {
                        logMessage(response?.error?.error?.localizedDescription)
                    }
                })
            }
            
            if isSystemReadyToProcessThis() {
                if ServicesManager.instance().isAuthorized() {
                    var qbuser : QBUUser?
                    var users: [QBUUser] = ServicesManager.instance().usersService.usersMemoryStorage.users(withLogins: [getQuickBloxUserName(userId)])
                    if isNotNull(users)&&users.count > 0{
                        qbuser = users[0]
                    }else{
                        qbuser = nil
                    }
                    if isNotNull(qbuser){
                        showChatScreen(qbuser!)
                    }else{
                        updateQBUsersCacheFromServerIfRequired({ (returnedData) -> () in
                            users = ServicesManager.instance().usersService.usersMemoryStorage.users(withLogins: [self.getQuickBloxUserName(userId)])
                            if isNotNull(users)&&users.count > 0{
                                qbuser = users[0]
                            }else{
                                qbuser = nil
                                showNotification(MESSAGE_TEXT___USER_NOT_AVAILABLE_FOR_CHAT, showOnNavigation: false, showAsError: true)
                                if(isInternetConnectivityAvailable(false)==false){return}
                                let information = ["toUserId":userId]
                                let scm = ServerCommunicationManager()
                                scm.responseErrorOption = .dontShowErrorResponseMessage
                                scm.inviteForChat(information as NSDictionary) {[weak self](responseData)->() in guard self != nil else { return }}
                            }
                            if isNotNull(qbuser){
                                showChatScreen(qbuser!)
                            }else{
                                logMessage("Failed to download user")
                            }
                        })
                    }
                }else{
                    loginToQuickBlox()
                }
            }
        }
    }
    
    
    func getUsersWithQBIDs(_ ids:[String])->[QBUUser]{
        var idsAsNSNumber = [NSNumber]()
        for s in ids{
            idsAsNSNumber.append(NSNumber(value: s.toInt() as Int))
        }
        let users = ServicesManager.instance().usersService.usersMemoryStorage.users(withIDs: idsAsNSNumber) as? [QBUUser]
        if isNotNull(users) && users!.count > 0{
            return users! as [QBUUser]
        }
        return NSArray() as! [QBUUser]
    }
    
    func getUserWithLogin(_ login:String)->QBUUser?{
        let users = ServicesManager.instance().usersService.usersMemoryStorage.users(withLogins: [login]) as? [QBUUser]
        if isNotNull(users) && users!.count > 0{
            return users![0]
        }
        return nil
    }
    
    func dialogs() -> [QBChatDialog] {
        return ServicesManager.instance().chatService.dialogsMemoryStorage.dialogsSortByUpdatedAt(withAscending: false) as [QBChatDialog]
    }
    
    func updatePoMChatGroups(){
        let groups = Dbm().getAllGroups()
        if isNotNull(groups) {
            for g in groups {
                managePoMChatGroup(group: g)
            }
        }
    }
    
    func managePoMChatGroup(group:Group){
        if safeBool(group.isAdmin) == false {return;}
        var usersIds = [String]()
        if isNotNull(group.members) {
            for m in group.members! {
                usersIds.append(safeString((m as! Member).userId))
            }
        }
        let chatGroup = getChatDialogue(chatGroupName(group: group))
        if isUserLoggedIn() && isNotNull(chatGroup) && usersIds.count > 0 && isQBReadyForItsUserDependentServices() && chatGroup?.userID == ServicesManager.instance().currentUser()!.id {
            let userLogins = NSMutableArray()
            var olderUsers = NSMutableArray()
            let newUsers = NSMutableArray()
            let newUsersToAdd = NSMutableArray()
            let oldUsersToRemove = NSMutableArray()
            for userId in usersIds {
                userLogins.add(getQuickBloxUserName((userId as? String)!))
            }
            userLogins.add(getQuickBloxUserName(loggedInUserId()))
            //add self in the list , just to make sure its not getting removed accidently
            userLogins.add(getQuickBloxUserName(QB_USER_ADMIN_QBID))
            //add super admin always , just to make sure its not getting removed accidently
            
            ServicesManager.instance().searchUsersWithIDs(chatGroup!.occupantIDs!, success: { (users) -> Void in
                if isNotNull(users){
                    olderUsers = users as! NSMutableArray
                    ServicesManager.instance().searchUsersWithLogins(userLogins as! [NSString], success: { (users) -> Void in
                        if isNotNull(users){
                            let newUsersTemporary = users
                            for ut in newUsersTemporary! {
                                for l in userLogins {
                                    if (ut.login as! NSString).isEqual(to: l as! String){
                                        newUsers.add(ut)
                                        break
                                    }
                                }
                            }
                            
                            let newUsersX = newUsers as! [QBUUser]
                            let olderUsersX = olderUsers as! [QBUUser]
                            for id in newUsersX {
                                var shouldAddUser = true
                                for _id in olderUsersX {
                                    if id.id == _id.id {
                                        shouldAddUser = false
                                    }
                                }
                                if shouldAddUser {
                                    newUsersToAdd.add(id)
                                }
                            }
                            for id in olderUsersX {
                                var shouldKeepUser = false
                                for _id in newUsersX {
                                    if id.id == _id.id {
                                        shouldKeepUser = true
                                    }
                                }
                                if shouldKeepUser == false {
                                    oldUsersToRemove.add(id)
                                }
                            }
                            
                            
                            let printDetailedLog = false
                            if printDetailedLog {
                                logMessage("\n\nExisting user on the group\n")
                                for user in (chatGroup?.occupantIDs)!{
                                    logMessage(user)
                                }
                                logMessage("\n\nNew user to add on the group\n")
                                for user in newUsersToAdd{
                                    logMessage((user as! QBUUser).id)
                                }
                                logMessage("\n\nOld users to remove from the group\n")
                                for user in oldUsersToRemove{
                                    logMessage((user as! QBUUser).id)
                                }
                            }
                            
                            //add users
                            if newUsersToAdd.count > 0 {
                                NewChatViewController.updateDialogAddUsers(chatGroup, newUsers: newUsersToAdd as! [QBUUser], completion: { (response, dialog) -> Void in
                                    logMessage("updateDialogAddUsers : \(response)")
                                })
                            }
                            //remove users
                            var shouldRemoveUsersImmediately = true
                            if shouldRemoveUsersImmediately && oldUsersToRemove.count > 0 {
                                NewChatViewController.updateDialogRemoveUsers(chatGroup, oldUsers: oldUsersToRemove as! [QBUUser], completion: { (response, dialog) -> Void in
                                    logMessage("updateDialogRemoveUsers : \(String(describing: response))")
                                })
                            }
                        }
                    }, error: { (error) -> Void in
                        logMessage("\(String(describing: error))")
                    })
                }
            }, error: { (error) -> Void in
                logMessage("\(String(describing: error))")
            })
        }else{
            logMessage("unable to create/update : \(chatGroupName(group: group))")
        }
    }
    
    
    func manageRidersGroup(_ usersIds:NSArray,dateOfRide:Date?){
        let ridersGroup = getChatDialogue(ridersGroupName())
        if isUserLoggedIn() && isNotNull(ridersGroup) && usersIds.count > 0 && isQBReadyForItsUserDependentServices() && ridersGroup?.userID == ServicesManager.instance().currentUser()!.id {
            let userLogins = NSMutableArray()
            var olderUsers = NSMutableArray()
            let newUsers = NSMutableArray()
            let newUsersToAdd = NSMutableArray()
            let oldUsersToRemove = NSMutableArray()
            for userId in usersIds {
                userLogins.add(getQuickBloxUserName((userId as? String)!))
            }
            userLogins.add(getQuickBloxUserName(loggedInUserId()))
            //add self in the list , just to make sure its not getting removed accidently
            userLogins.add(getQuickBloxUserName(QB_USER_ADMIN_QBID))
            //add super admin always , just to make sure its not getting removed accidently
            
            ServicesManager.instance().searchUsersWithIDs(ridersGroup!.occupantIDs!, success: { (users) -> Void in
                if isNotNull(users){
                    olderUsers = users as! NSMutableArray
                    ServicesManager.instance().searchUsersWithLogins(userLogins as! [NSString], success: { (users) -> Void in
                        if isNotNull(users){
                            let newUsersTemporary = users
                            for ut in newUsersTemporary! {
                                for l in userLogins {
                                    if (ut.login as! NSString).isEqual(to: l as! String){
                                        newUsers.add(ut)
                                        break
                                    }
                                }
                            }
                            
                            let newUsersX = newUsers as! [QBUUser]
                            let olderUsersX = olderUsers as! [QBUUser]
                            for id in newUsersX {
                                var shouldAddUser = true
                                for _id in olderUsersX {
                                    if id.id == _id.id {
                                        shouldAddUser = false
                                    }
                                }
                                if shouldAddUser {
                                    newUsersToAdd.add(id)
                                }
                            }
                            for id in olderUsersX {
                                var shouldKeepUser = false
                                for _id in newUsersX {
                                    if id.id == _id.id {
                                        shouldKeepUser = true
                                    }
                                }
                                if shouldKeepUser == false {
                                    oldUsersToRemove.add(id)
                                }
                            }
                            
                            
                            let printDetailedLog = false
                            if printDetailedLog {
                                logMessage("\n\nExisting user on the group\n")
                                for user in (ridersGroup?.occupantIDs)!{
                                    logMessage(user)
                                }
                                logMessage("\n\nNew user to add on the group\n")
                                for user in newUsersToAdd{
                                    logMessage((user as! QBUUser).id)
                                }
                                logMessage("\n\nOld users to remove from the group\n")
                                for user in oldUsersToRemove{
                                    logMessage((user as! QBUUser).id)
                                }
                            }
                            
                            //add users
                            if newUsersToAdd.count > 0 {
                                NewChatViewController.updateDialogAddUsers(ridersGroup, newUsers: newUsersToAdd as! [QBUUser], completion: { (response, dialog) -> Void in
                                    logMessage("updateDialogAddUsers : \(response)")
                                })
                            }
                            //remove users
                            var shouldRemoveUsersImmediately = true
                            if isNotNull(dateOfRide){
                                if dateOfRide!.dateByAddingMinutes(60).isEarlierThanDate(Date()){
                                    shouldRemoveUsersImmediately = true
                                }else{
                                    shouldRemoveUsersImmediately = false
                                }
                            }
                            if shouldRemoveUsersImmediately && oldUsersToRemove.count > 0 {
                                NewChatViewController.updateDialogRemoveUsers(ridersGroup, oldUsers: oldUsersToRemove as! [QBUUser], completion: { (response, dialog) -> Void in
                                    logMessage("updateDialogRemoveUsers : \(String(describing: response))")
                                })
                            }
                        }
                    }, error: { (error) -> Void in
                        logMessage("\(String(describing: error))")
                    })
                }
            }, error: { (error) -> Void in
                logMessage("\(String(describing: error))")
            })
        }else{
            logMessage("unable to create/update : \(ridersGroupName)")
        }
    }
    
    func getChatDialogue(_ name:String)->QBChatDialog?{
        let allDialogs = dialogs()
        if allDialogs.count > 0{
            for dialog in allDialogs {
                if (dialog as AnyObject).name == name {
                    return dialog as? QBChatDialog
                }
            }
        }
        return nil
    }
    
    //MARK: - QMChatServiceDelegate
    
    func chatService(_ chatService: QMChatService, didUpdateChatDialogInMemoryStorage chatDialog: QBChatDialog) {
    }
    
    func chatService(_ chatService: QMChatService, didAddChatDialogsToMemoryStorage chatDialogs: [QBChatDialog]) {
    }
    
    func chatService(_ chatService: QMChatService, didAddChatDialogToMemoryStorage chatDialog: QBChatDialog) {
    }
    
    func chatService(_ chatService: QMChatService, didDeleteChatDialogWithIDFromMemoryStorage chatDialogID: String) {
    }
    
    func chatService(_ chatService: QMChatService, didAddMessagesToMemoryStorage messages: [QBChatMessage], forDialogID dialogID: String) {
        updateBadgeValues()
    }
    
    func chatService(_ chatService: QMChatService, didAddMessageToMemoryStorage message: QBChatMessage, forDialogID dialogID: String) {
        RideEventManager.sharedInstance.checkForRideEventNotification(message)
        updateBadgeValues()
    }
    
    //MARK: - QMChatConnectionDelegate
    
    func chatServiceChatDidAccidentallyDisconnect(_ chatService: QMChatService) {
        if isInternetConnectivityAvailable(false){
            execMain({[weak self]  in guard self != nil else { return }
                Acf().connectWithQuickBlox()
                },delay: 5)
        }
    }
    
    func chatServiceChatDidConnect(_ chatService: QMChatService) {
    }
    
    func chatService(_ chatService: QMChatService, chatDidNotConnectWithError error: Error) {
    }
    
    func chatServiceChatDidReconnect(_ chatService: QMChatService) {
    }
    
    func setupKeyboardNextButtonHandler(){
        NotificationCenter.default.addObserver(self, selector: Selector(("viewLoadedNotification")), name: NSNotification.Name(rawValue: "viewLoaded"), object: nil)
    }
    
    func setupIQKeyboardManagerEnabled(){
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
        IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = true
        IQKeyboardManager.sharedManager().shouldPlayInputClicks = true
        IQKeyboardManager.sharedManager().disabledToolbarClasses.append(StatusEditorViewController.self)
        IQKeyboardManager.sharedManager().disabledToolbarClasses.append(ChatViewController.self)
        IQKeyboardManager.sharedManager().disabledToolbarClasses.append(AddEditGroupViewController.self)
        IQKeyboardManager.sharedManager().disabledToolbarClasses.append(AddEditPlaceViewController.self)
        IQKeyboardManager.sharedManager().disabledTouchResignedClasses.append(AddEditPlaceViewController.self)
        IQKeyboardManager.sharedManager().disabledDistanceHandlingClasses.append(StatusEditorViewController.self)
    }
    
    func setupIQKeyboardManagerDisabled(){
        IQKeyboardManager.sharedManager().enable = false
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = false
        IQKeyboardManager.sharedManager().shouldPlayInputClicks = false
    }
    
    func initialSetup(){
        initialiseUserBoardingTypeIfRequired()
    }
    
    func initialiseUserBoardingTypeIfRequired(){
        let setting = Dbm().getSetting()
        if setting.userBoardingType == nil {
            setting.userBoardingType = "regular"
            Dbm().saveChanges()
        }
    }
    
    func setupOtherSettings(){
        UIDevice.current.isBatteryMonitoringEnabled = true
        checkForProfilePictureUpdate()
        disableAutoCorrectionsAndTextSuggestionGlobally()
        UISegmentedControl.appearance().setTitleTextAttributes([NSFontAttributeName: UIFont.init(name: FONT_REGULAR, size: 13)!], for: UIControlState.normal)
        navigationController = APPDELEGATE.window?.rootViewController as? UINavigationController
        navigationController?.view.backgroundColor = UIColor.white
        windowObject()?.backgroundColor = UIColor.white
        NotificationCenter.default.addObserver(self, selector: #selector(AppCommonFunctions.performNecessaryUpdateFromTheServer), name: NSNotification.Name(rawValue: NOTIFICATION_USER_SIGNED_IN), object: nil)
    }
    
    func showRequiredScreen(){
        if !isUserLoggedIn() {
            trackFunctionalEvent(FE_APP_LAUNCH_PRE_REG, information: nil)
            showPreSignInFlow()
            self.carpoolHomeScreenController = nil
            self.sideMenuController = nil
            self.sideMenuViewController = nil
        }else{
            trackFunctionalEvent(FE_APP_LAUNCH_POST_REG, information: nil)
            decideAndShowNextScreen(self.navigationController!,isStartup:true)
        }
        let splashView = SKSplashView(backgroundImage: UIImage(named: "backGroundImage")!, animationType: SKSplashAnimationType.fade)
        self.navigationController?.view.addSubview(splashView!)
        splashView?.startAnimation()
    }
    
    func showHomeScreen(){
        var homeViewController : UIViewController!
        
        func preparePoMasHome(){
            if self.pOMHomeScreenController == nil {
                self.pOMHomeScreenController = UINavigationController(rootViewController: getViewController("PeaceOfMindHomeViewController"))
            }
        }
        
        func prepareCarpoolAsHome(){
            if userType() != USER_TYPE_OTHER {
                if self.carpoolHomeScreenController == nil {
                    self.carpoolHomeScreenController = getViewController("TabBarController") as? UITabBarController
                    self.carpoolHomeScreenController?.delegate = self
                }
            }
        }
        
        prepareCarpoolAsHome()
        preparePoMasHome()
        
        let isValueExistSignupCompletedCarpool = isNotNull(CacheManager.sharedInstance.loadObject(KEY_CARPOOL_SIGNUP_COMPLETED))
        let isValueExistSignupCompletedPoM = isNotNull(CacheManager.sharedInstance.loadObject(KEY_POM_SIGNUP_COMPLETED))
        
        if getUserOnBoardingType() == .carpool && isValueExistSignupCompletedCarpool {
            trackKochavaEvent(KE_CARPOOL_SIGNUP_COMPLETED)
        }else if getUserOnBoardingType() == .peaceOfMind && isValueExistSignupCompletedPoM{
            trackKochavaEvent(KE_POM_SIGNUP_COMPLETED)
        }
        
        CacheManager.sharedInstance.saveObject(nil, identifier: KEY_CARPOOL_SIGNUP_COMPLETED)
        CacheManager.sharedInstance.saveObject(nil, identifier: KEY_POM_SIGNUP_COMPLETED)
        
        if userType() == USER_TYPE_OTHER {
            homeViewController = pOMHomeScreenController
        }else{
            if safeString(USER_DEFAULTS.object(forKey: "DefaultScreen"),alternate: "Carpool") == "Carpool" {
                homeViewController = self.carpoolHomeScreenController
            }else{
                homeViewController = pOMHomeScreenController
            }
        }
        
        let sideMenuViewController = getViewController("SideMenuViewController") as? SideMenuViewController
        let sideMenuController = RESideMenu(contentViewController: homeViewController, leftMenuViewController: sideMenuViewController, rightMenuViewController: nil)
        sideMenuController?.delegate = self
        sideMenuController?.panGestureEnabled = false
        
        self.sideMenuController = sideMenuController
        self.sideMenuViewController = sideMenuViewController
        
        self.navigationController?.dismissPopupViewController(.fade)
        self.navigationController?.setViewControllers([sideMenuController!], animated: false)
        saveThisDataForExtensionsAccess(loggedInUserId() , key: "userId")
        
        let delay = 1.0
        if homeViewController == self.carpoolHomeScreenController {
            execMain ({ (completed) in
                self.switchHomeScreenToPoM()
                execMain ({ (completed) in
                    self.switchHomeScreenToCarpool()
                },delay:delay)
            },delay:delay)
        }else{
            if self.carpoolHomeScreenController != nil {
                execMain ({ (completed) in
                    self.switchHomeScreenToCarpool()
                    execMain ({ (completed) in
                        self.switchHomeScreenToPoM()
                    },delay:delay)
                },delay:delay)
            }
        }
        checkForReferals()
    }
    
    func switchHomeScreenToPoM(hideSideMenu:Bool=true){
        if self.pOMHomeScreenController == nil {
            self.pOMHomeScreenController = UINavigationController(rootViewController: getViewController("PeaceOfMindHomeViewController"))
        }
        self.sideMenuController?.setContentViewController(self.pOMHomeScreenController, animated: false)
        if hideSideMenu {
            self.sideMenuController?.hideViewController()
        }
        USER_DEFAULTS.set("PoM", forKey: "DefaultScreen")
    }
    
    func switchHomeScreenToCarpool(hideSideMenu:Bool=true){
        if self.carpoolHomeScreenController == nil {
            self.carpoolHomeScreenController = getViewController("TabBarController") as? UITabBarController
            self.carpoolHomeScreenController?.delegate = self
        }
        self.sideMenuController?.setContentViewController(self.carpoolHomeScreenController, animated: false)
        if hideSideMenu {
            self.sideMenuController?.hideViewController()
        }
        USER_DEFAULTS.set("Carpool", forKey: "DefaultScreen")
    }
    
    func sideMenu(_ sideMenu: RESideMenu!, willShowMenuViewController menuViewController: UIViewController!){
        menuViewController.viewWillAppear(true)
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController){
    }
    
    func showPreSignInFlow() {
        pushVC("WalkThroughViewController", navigationController: self.navigationController, isRootViewController: true, animated: false, modifyObject: nil)
    }
    
    func hidePopupViewController(){
        self.navigationController!.dismissPopupViewController(.fade)
    }
    
    func isPopUpViewControllerShowing()->Bool {
        return self.navigationController!.isPopUpViewControllerShowing()
    }
    
    func requestLocationPermissionInAdvance(){
        locationManager.requestAlwaysAuthorization()
    }
    
    func isAddressInputScreenNeeded()->Bool{
        if userType() == USER_TYPE_OTHER { return false }
        let settings = Dbm().getSetting()
        if (isNull(settings.homeTime) || isNull(settings.homeAddress) || isNull(settings.destinationAddress) || isNull(settings.destinationTime) || isNull(settings.homeLocationLatitude) || isNull(settings.homeLocationLongitude) || isNull(settings.destinationLocationLatitude) || isNull(settings.destinationLocationLongitude)) {
            return true
        }
        return false
    }
    
    func isAddressAdded()->Bool{
        let settings = Dbm().getSetting()
        if (isNull(settings.homeTime) || isNull(settings.homeAddress) || isNull(settings.destinationAddress) || isNull(settings.destinationTime) || isNull(settings.homeLocationLatitude) || isNull(settings.homeLocationLongitude) || isNull(settings.destinationLocationLatitude) || isNull(settings.destinationLocationLongitude)) {
            return false
        }
        return true
    }
    
    
    func setupCrashlytics(){
        Fabric.with([Branch.self,Crashlytics.self])
        updateUserInfoOnCrashlytics()
    }
    
    func updateUserInfoOnCrashlytics(){
        if isUserLoggedIn(){
            let userInfo =  Dbm().getUserInfo()
            Crashlytics.sharedInstance().setUserName(safeString(userInfo?.name))
            Crashlytics.sharedInstance().setUserIdentifier(safeString(userInfo?.userId))
        }
    }
    
    func showSettleRidePopup(_ users:NSMutableArray,isCarOwner:Bool,rideDate:Date){
        if isSystemReadyToProcessThis() {
            let gapX = getRequiredPopupSideGap(#function)
            let gapY = getRequiredPopupVerticleGap(#function)
            let viewController = getViewController("SettleRideViewController") as! SettleRideViewController
            viewController.users = users
            viewController.isCarOwner = isCarOwner
            viewController.rideDate = rideDate
            let navigationController = UINavigationController(rootViewController: viewController)
            navigationController.view.frame = CGRect(x: gapX,y: gapY,width: DEVICE_WIDTH-2*gapX, height: DEVICE_HEIGHT-2*gapY)
            navigationController.view.layer.cornerRadius = POP_UP_VIEW_CORNER_RADIUS
            navigationController.view.layer.masksToBounds = true
            self.navigationController!.presentPopupViewController(navigationController, animationType: .fade, completion: { () -> Void in
            })
        }
    }
    
    func showStatusUpdateScreen(){
        if isSystemReadyToProcessThis() {
            let gapX = getRequiredPopupSideGap(#function)
            let gapY = getRequiredPopupVerticleGap(#function)
            let viewController = getViewController("StatusEditorViewController")
            let navigationController = UINavigationController(rootViewController: viewController)
            navigationController.view.frame = CGRect(x: gapX,y: gapY,width: DEVICE_WIDTH-2*gapX, height: DEVICE_HEIGHT-2*gapY)
            navigationController.view.layer.cornerRadius = POP_UP_VIEW_CORNER_RADIUS
            navigationController.view.layer.masksToBounds = true
            self.navigationController!.presentPopupViewController(navigationController, animationType: .fade, completion: { () -> Void in
            })
        }
    }
    
    func showViewControllerAsPopup(_ viewController:UIViewController){
        let gapX = 10.0 as CGFloat
        let gapY = 35.0 as CGFloat
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.view.frame = CGRect(x: gapX,y: gapY,width: DEVICE_WIDTH-2*gapX, height: DEVICE_HEIGHT-2*gapY)
        navigationController.view.layer.cornerRadius = POP_UP_VIEW_CORNER_RADIUS
        navigationController.view.layer.masksToBounds = true
        self.navigationController!.presentPopupViewController(navigationController, animationType: .fade, completion: { () -> Void in
        })
    }
    
    func showVCAsPopup(viewController:UIViewController){
        if isSystemReadyToProcessThis() {
            let gapX = getRequiredPopupSideGap(#function)
            let gapY = getRequiredPopupVerticleGap(#function)
            let navigationController = UINavigationController(rootViewController: viewController)
            navigationController.view.frame = CGRect(x: gapX,y: gapY,width: DEVICE_WIDTH-2*gapX, height: DEVICE_HEIGHT-2*gapY)
            navigationController.view.layer.cornerRadius = POP_UP_VIEW_CORNER_RADIUS
            navigationController.view.layer.masksToBounds = true
            self.navigationController!.presentPopupViewController(navigationController, animationType: .fade, completion: { () -> Void in
            })
        }
    }
    func showOfferDetails(userInfo:NSDictionary){
        if(isInternetConnectivityAvailable(true)==false){return}
        let offerType = safeString(userInfo.object(forKey: "offer_type"), alternate: "cashback")
        var viewController : UIViewController!
        if offerType == "cashback" {
            let vc = getViewController("OfferDetailsType1ViewController") as! OfferDetailsType1ViewController
            vc.userInfo = userInfo
            viewController = vc
        }else if offerType == "no_offer" {
            let vc = getViewController("OfferDetailsType1ViewController") as! OfferDetailsType1ViewController
            vc.userInfo = userInfo
            viewController = vc
        }else if offerType == "free" {
            let vc = getViewController("OfferDetailsType2ViewController") as! OfferDetailsType2ViewController
            vc.userInfo = userInfo
            viewController = vc
        }
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.view.frame = CGRect(x: 0,y: DEVICE_HEIGHT - 420/2 ,width: DEVICE_WIDTH, height: 420)
        navigationController.view.layer.masksToBounds = true
        navigationController.view.backgroundColor = UIColor.clear
        self.navigationController!.presentPopupViewController(navigationController, animationType: .fade, completion: { () -> Void in
        })
    }
    
    func getOfferDetails(withUserId:String,offerId:String,completion:@escaping ACFCompletionBlock){
        let cacheKey = "\(Date().day())\(loggedInUserId())\(withUserId)"
        if let response = CacheManager.sharedInstance.loadObject(cacheKey) as? NSDictionary {
            completion(response)
        }
        let scm = ServerCommunicationManager()
        scm.getRideCostDetails(["withUserId":withUserId,"offerId":offerId]) { (response) in
            if isNotNull(response){
                CacheManager.sharedInstance.saveObject(response, identifier: cacheKey)
                completion(response)
            }else{
                completion(nil)
            }
        }
    }
    
    func showFeedbackScreenIfNotAlreadyAsked(_ user:NSDictionary)->Bool{
        var shouldShowFeedback = false
        let key = getUserIdFor(user)
        if isNull(feedbackPopupShown.object(forKey: key)){
            shouldShowFeedback = true
            feedbackPopupShown.setObject(Date(), forKey: key as NSCopying)
            showFeedbackScreen(user)
        }else if isNotNull(feedbackPopupShown.object(forKey: key)){
            let lastDate = feedbackPopupShown.object(forKey: key) as! Date
            if lastDate.isEarlierThanDate(Date().dateBySubtractingHours(4)){
                shouldShowFeedback = true
                feedbackPopupShown.setObject(Date(), forKey: key as NSCopying)
                showFeedbackScreen(user)
            }
        }
        return shouldShowFeedback
    }
    
    func sendFeedback(_ information:NSDictionary) {
        let scm = ServerCommunicationManager()
        scm.responseErrorOption = .dontShowErrorResponseMessage
        scm.sendFeedback(information) {[weak self] (responseData) -> () in guard self != nil else { return }
            if let _ = responseData {
                NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_NEED_USER_UPDATE), object: nil)
            }
        }
    }
    
    func showFeedbackScreen(_ user:NSDictionary){
        let gapX = getRequiredPopupSideGap(#function)
        let gapY = 40.0 as CGFloat
        let viewController = getViewController("FeedbackViewController") as! FeedbackViewController
        viewController.userInfo = user
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.view.frame = CGRect(x: gapX,y: gapY,width: DEVICE_WIDTH-2*gapX, height: DEVICE_HEIGHT-2*gapY)
        navigationController.view.layer.cornerRadius = POP_UP_VIEW_CORNER_RADIUS
        navigationController.view.layer.masksToBounds = true
        self.navigationController!.presentPopupViewController(navigationController, animationType: .fade, completion: { () -> Void in
        })
    }
    
    func showCommonPickerScreen(_ optionList:NSMutableArray,titleToShow:String,nc:UINavigationController?,completion:ACFCompletionBlock?){
        let viewController = getViewController("CommonPickerViewController") as! CommonPickerViewController
        viewController.completion = completion
        viewController.optionList = optionList.mutableCopy() as! NSMutableArray
        viewController.titleToShow = titleToShow
        nc?.pushViewController(viewController, animated: true)
    }
    
    func showNotificationScreen(nc:UINavigationController?,themeColor:UIColor=APP_THEME_VOILET_COLOR,group:Group?=nil,member:Member?=nil){
        let gapX = getRequiredPopupSideGap(#function)
        let gapY = getRequiredPopupVerticleGap(#function)
        let viewController = getViewController("NotificationsViewController") as! NotificationsViewController
        viewController.themeColor = themeColor
        viewController.group = group
        viewController.member = member
        nc?.pushViewController(viewController, animated: true)
    }
    
    func showRideEventViewerScreen(_ users:NSMutableArray,isOwner:Bool,isHomeToDestination:Bool,selectedDayDate:Date,groupChatDialogue:QBChatDialog) {
        let gapX = 20.0 as CGFloat
        let gapY = 40.0 as CGFloat
        let viewController = getViewController("RideEventViewerViewController") as! RideEventViewerViewController
        viewController.usersForRide = users.mutableCopy() as! NSMutableArray
        viewController.trueForOwnerFalseforPassenger = isOwner
        viewController.trueForHomeToDestinationFalseforDestinationToHome = isHomeToDestination
        viewController.groupChatDialogue = groupChatDialogue
        viewController.selectedDayDate = selectedDayDate
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.view.frame = CGRect(x: gapX,y: gapY,width: DEVICE_WIDTH-2*gapX, height: DEVICE_HEIGHT-2*gapY)
        navigationController.view.layer.cornerRadius = POP_UP_VIEW_CORNER_RADIUS
        navigationController.view.layer.masksToBounds = true
        self.navigationController!.presentPopupViewController(navigationController, animationType: .fade, completion: { () -> Void in
        })
    }
    
    func showWeeklyPlannerScreen(){
        let gapX = 5.0 as CGFloat
        let gapY = 20.0 as CGFloat
        let viewController = getViewController("WeeklyPlannerViewController")
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.view.frame = CGRect(x: gapX,y: gapY,width: DEVICE_WIDTH-2*gapX, height: DEVICE_HEIGHT-2*gapY)
        navigationController.view.layer.cornerRadius = POP_UP_VIEW_CORNER_RADIUS
        navigationController.view.layer.masksToBounds = true
        self.navigationController!.presentPopupViewController(navigationController, animationType: .fade, completion: { () -> Void in
        })
    }
    
    
    func showTermsAndConditionsScreen(){
        let gapX = getRequiredPopupSideGap(#function)
        let gapY = getRequiredPopupVerticleGap(#function)
        let viewController = getViewController("WebViewViewController") as! WebViewViewController
        viewController.workingMode = WorkingMode.termsAndConditions
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.view.frame = CGRect(x: gapX,y: gapY,width: DEVICE_WIDTH-2*gapX, height: DEVICE_HEIGHT-2*gapY)
        navigationController.view.layer.cornerRadius = POP_UP_VIEW_CORNER_RADIUS
        navigationController.view.layer.masksToBounds = true
        self.navigationController!.presentPopupViewController(navigationController, animationType: .fade, completion: { () -> Void in
        })
    }
    
    func getParsedValues(_ text:String) -> NSMutableDictionary {
        let information = NSMutableDictionary()
        let componentsLevel1 = (text as NSString).components(separatedBy: "&&")
        for componentLevel1 in componentsLevel1 {
            let componentsLevel2 = (componentLevel1 as NSString).components(separatedBy: "=")
            if componentsLevel2.count == 2 {
                information.setObject(componentsLevel2[1],forKey: componentsLevel2[0] as NSCopying)
            }
        }
        return information as NSMutableDictionary
    }
    
    func handleNotification(_ notificationObject:Any?,onlyShowBanner:Bool=false){
        SwiftTryCatch.tryThis({
            if !isSystemReadyToProcessThis() {return} // return if user is not logged in
            let processed = Drnm().processIfDRN(notification: notificationObject)
            if !processed {
                if notificationObject != nil {
                    if onlyShowBanner {
                        let topMostController = windowObject()?.topMostController()
                        var message = (notificationObject as? NSDictionary)?.value(forKeyPath: "aps.alert") as? String
                        if isNull(message){
                            let title = (notificationObject as? NSDictionary)?.value(forKeyPath: "aps.alert.title") as? String
                            let description = (notificationObject as? NSDictionary)?.value(forKeyPath: "aps.alert.body") as? String
                            if isNotNull(description){
                                message = "\(safeString(title)) \(description!)"
                            }
                        }
                        if topMostController != nil && message != nil {
                            showInformationBannerOnBottom(topMostController!, style: .blurLight, duration: 4, title: "Notification", message: message!)
                        }
                    }else{
                        handlePushNotificationsNonDRN(notification: notificationObject as! NSDictionary)
                    }
                }
            }
        }, catchThis: {error in},finally:{})
    }
    
    func getRequiredPopupVerticleGap(_ tag:String?)->(CGFloat){
        return (UIScreen.main.bounds.size.height-450)/2
    }
    
    func getRequiredPopupSideGap(_ tag:String?)->(CGFloat){
        return 10
    }
    
    func presentVC(_ identifier:String , viewController:UIViewController? , animated:Bool , modifyObject:ACFModificationBlock?){
        let vc = getViewController(identifier as NSString)
        if let _ = modifyObject{
            modifyObject?(vc)
        }
        viewController?.present(UINavigationController(rootViewController: vc), animated: animated, completion: nil)
    }
    
    func pushVC(_ identifier:String,navigationController:UINavigationController?,isRootViewController:Bool,animated:Bool,modifyObject:ACFModificationBlock?){
        let vc = getViewController(identifier as NSString)
        if let _ = modifyObject{
            modifyObject?(vc)
        }
        if isRootViewController{
            navigationController?.setViewControllers([vc], animated: animated)
        }else{
            navigationController?.pushViewController(vc, animated: animated)
        }
    }
    
    func updateAppearanceOfTextFieldType1(_ textField:MKTextField?){
        if textField!.isFirstResponder {
            textField!.tintColor = APP_THEME_VOILET_COLOR
            textField!.bottomBorderEnabled = true
            textField!.bottomBorderColor = APP_THEME_VOILET_COLOR
            textField!.placeholder = textField!.placeholder
            textField!.floatingPlaceholderEnabled = true
            textField!.attributedPlaceholder = NSAttributedString(string:textField!.placeholder!,
                                                                  attributes:[NSForegroundColorAttributeName: APP_THEME_VOILET_COLOR])
        }else {
            textField!.tintColor = APP_THEME_LIGHT_GRAY_COLOR
            textField!.bottomBorderEnabled = true
            textField!.bottomBorderColor = APP_THEME_LIGHT_GRAY_COLOR
            textField!.placeholder = textField!.placeholder
            textField!.floatingPlaceholderEnabled = true
            textField!.attributedPlaceholder = NSAttributedString(string:textField!.placeholder!,attributes:[NSForegroundColorAttributeName: APP_THEME_LIGHT_GRAY_COLOR])
        }
    }
    
    func updateAppearanceOfTextFieldType2(_ textField:MKTextField?){
        if textField!.isFirstResponder {
            textField!.tintColor = APP_THEME_VOILET_COLOR
            textField!.bottomBorderEnabled = true
            textField!.bottomBorderColor = APP_THEME_VOILET_COLOR
            textField!.placeholder = textField!.placeholder
            textField!.floatingPlaceholderEnabled = false
        }else {
            textField!.tintColor = APP_THEME_LIGHT_GRAY_COLOR
            textField!.bottomBorderEnabled = true
            textField!.bottomBorderColor = APP_THEME_LIGHT_GRAY_COLOR
            textField!.placeholder = textField!.placeholder
            textField!.floatingPlaceholderEnabled = false
        }
    }
    
    func disableAutoCorrectionsAndTextSuggestionGlobally () {
        NotificationCenter.default.addObserver(self, selector: #selector(AppCommonFunctions.notificationWhenTextViewDidBeginEditing(_:)), name: NSNotification.Name.UITextViewTextDidBeginEditing, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AppCommonFunctions.notificationWhenTextFieldDidBeginEditing(_:)), name: NSNotification.Name.UITextFieldTextDidBeginEditing, object: nil)
    }
    
    func notificationWhenTextFieldDidBeginEditing (_ notification:Foundation.Notification) {
        let textField = notification.object as? UITextField
        textField?.autocorrectionType = UITextAutocorrectionType.no
    }
    
    func notificationWhenTextViewDidBeginEditing (_ notification:Foundation.Notification) {
        let textView = notification.object as? UITextView
        textView?.autocorrectionType = UITextAutocorrectionType.no
    }
    
    func setImageOfChatDialogMessage(_ chatDialog:QBChatDialog , imageView:UIImageView){
        if chatDialog.type == .private {
            var user:QBUUser?
            if chatDialog.recipientID > 0 {
                user = ServicesManager.instance().usersService.usersMemoryStorage.user(withID: UInt(chatDialog.recipientID))
            }
            if isNotNull(user)&&isNotNull(user!.website){
                imageView.sd_setImage(with: URL(string: user!.website!))
            }else{
                imageView.image = USER_PLACEHOLDER_IMAGE
            }
        }else {
            imageView.image = UIImage(named: "group")
        }
    }
    
    func getSuggestedLoginIds()->NSMutableArray{
        let suggestedLoginIds = NSMutableArray()
        let userIds = Dbm().getAllUserIds()
        var counter = 0
        for userId in userIds {
            let login = getQuickBloxUserName((userId as AnyObject).value)
            var isNeededToDownload = true
            let results = ServicesManager.instance().usersService.usersMemoryStorage.users(withLogins: [login])
            if isNotNull(results) && results.count > 0 {
                isNeededToDownload = false
            }
            if isNeededToDownload {
                counter = counter + 1
                if counter > 60 {
                    break
                }
                suggestedLoginIds.add(login)
            }
        }
        return suggestedLoginIds
    }
    
    func updateBadgeValues(){
        execMain({[weak self]  in guard let `self` = self else { return }
            if isUserLoggedIn() && isNotNull(self.carpoolHomeScreenController){
                let tabBarItem1 = (self.carpoolHomeScreenController?.tabBar.items![1])! as UITabBarItem
                if self.getDialogsCountHavingUnreadChatMessages() > 0 {
                    tabBarItem1.badgeValue = "\(self.getDialogsCountHavingUnreadChatMessages())"
                }else{
                    tabBarItem1.badgeValue = nil
                }
            }
        })
    }
    
    func getDialogsCountHavingUnreadChatMessages()->(UInt){
        var dialogsCountHavingUnreadChatMessages : UInt?
        dialogsCountHavingUnreadChatMessages = 0
        for dialog in RecentChatsViewController.dialogs() {
            if dialog.unreadMessagesCount > 0{
                dialogsCountHavingUnreadChatMessages = dialogsCountHavingUnreadChatMessages! + 1
            }
        }
        return dialogsCountHavingUnreadChatMessages!
    }
    
    func updateInAppNotificationCount(){
        updateBadgeValues()
    }
    
    func setShowImageOfImageView(imageView:UIImageView?){
        if isNotNull(imageView){
            imageView?.gestureRecognizers?.removeAll()
            let tapGestureRecogniser = UITapGestureRecognizer(target: self, action: #selector(AppCommonFunctions.imageViewTappedForViewing(sender:)))
            tapGestureRecogniser.numberOfTapsRequired = 1
            imageView?.addGestureRecognizer(tapGestureRecogniser)
            imageView?.isUserInteractionEnabled = true
        }
    }
    
    func imageViewTappedForViewing(sender:UITapGestureRecognizer){
        if let imageView = sender.view as? UIImageView {
            if let image = imageView.image {
                resignKeyboard()
                mediaFocusController.show(image, from: imageView)
            }
        }
    }
}
