//
//  AppCommonFunctions.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import Foundation
import AFNetworking

extension AppCommonFunctions {
    
    func updateDNDSettings(){
        let settingsObject = Dbm().getSetting()
        if settingsObject.isDoNotDisturbSettingsUpdated!.boolValue {
            func updateDailyDNDSettings() {
                //step 1 update dnd daily notifications settings
                var valuesToUpdate = NSMutableString()
                
                let date1 = Date().toStringValue("yyyy-MM-dd")
                let date2 = Date().dateByAddingDays(1).toStringValue("yyyy-MM-dd")
                let date3 = Date().dateByAddingDays(2).toStringValue("yyyy-MM-dd")
                let date4 = Date().dateByAddingDays(3).toStringValue("yyyy-MM-dd")
                let date5 = Date().dateByAddingDays(4).toStringValue("yyyy-MM-dd")
                let date6 = Date().dateByAddingDays(5).toStringValue("yyyy-MM-dd")
                let date7 = Date().dateByAddingDays(6).toStringValue("yyyy-MM-dd")
                
                
                if settingsObject.doNotDisturbForDailyOption1!.boolValue{
                    valuesToUpdate.append("\(date1):")
                }
                if settingsObject.doNotDisturbForDailyOption2!.boolValue{
                    valuesToUpdate.append("\(date2):")
                }
                if settingsObject.doNotDisturbForDailyOption3!.boolValue{
                    valuesToUpdate.append("\(date3):")
                }
                if settingsObject.doNotDisturbForDailyOption4!.boolValue{
                    valuesToUpdate.append("\(date4):")
                }
                if settingsObject.doNotDisturbForDailyOption5!.boolValue{
                    valuesToUpdate.append("\(date5):")
                }
                if settingsObject.doNotDisturbForDailyOption6!.boolValue{
                    valuesToUpdate.append("\(date6):")
                }
                if settingsObject.doNotDisturbForDailyOption7!.boolValue{
                    valuesToUpdate.append("\(date7):")
                }
                
                if valuesToUpdate.length > 0 {
                    valuesToUpdate = (valuesToUpdate.substring(to: valuesToUpdate.length-1) as NSString).mutableCopy() as! NSMutableString
                }else{
                    valuesToUpdate = NULL_OBJECT_TO_SEND_ON_SERVER
                }
                
                let information = NSMutableDictionary()
                information.setObject(valuesToUpdate, forKey: "value" as NSCopying)
                let scm = ServerCommunicationManager()
                scm.updateDNDDailySettings(information, completionBlock: {[weak self] (responseData) -> () in guard let `self` = self else { return }
                    let settingsObject = Dbm().getSetting()
                    settingsObject.isDoNotDisturbSettingsUpdated = NSNumber(value: false)
                    Dbm().saveChanges()
                })
            }
            
            func updateWeeklyDNDSettings() {
                //step 1 update dnd weekly notifications settings
                var valuesToUpdate = NSMutableString()
                
                let day1 = "monday"
                let day2 = "tuesday"
                let day3 = "wednesday"
                let day4 = "thursday"
                let day5 = "friday"
                let day6 = "saturday"
                let day7 = "sunday"
                
                if settingsObject.doNotDisturbForWeeklyOption1!.boolValue{
                    valuesToUpdate.append("\(day1):")
                }
                if settingsObject.doNotDisturbForWeeklyOption2!.boolValue{
                    valuesToUpdate.append("\(day2):")
                }
                if settingsObject.doNotDisturbForWeeklyOption3!.boolValue{
                    valuesToUpdate.append("\(day3):")
                }
                if settingsObject.doNotDisturbForWeeklyOption4!.boolValue{
                    valuesToUpdate.append("\(day4):")
                }
                if settingsObject.doNotDisturbForWeeklyOption5!.boolValue{
                    valuesToUpdate.append("\(day5):")
                }
                if settingsObject.doNotDisturbForWeeklyOption6!.boolValue{
                    valuesToUpdate.append("\(day6):")
                }
                if settingsObject.doNotDisturbForWeeklyOption7!.boolValue{
                    valuesToUpdate.append("\(day7):")
                }
                
                if valuesToUpdate.length > 0 {
                    valuesToUpdate = (valuesToUpdate.substring(to: valuesToUpdate.length-1) as NSString).mutableCopy() as! NSMutableString
                }else{
                    valuesToUpdate = NULL_OBJECT_TO_SEND_ON_SERVER
                }
                
                let information = NSMutableDictionary()
                information.setObject(valuesToUpdate, forKey: "value" as NSCopying)
                let scm = ServerCommunicationManager()
                scm.updateDNDWeeklySettings(information, completionBlock: {[weak self] (responseData) -> () in guard let `self` = self else { return }
                    let settingsObject = Dbm().getSetting()
                    settingsObject.isDoNotDisturbSettingsUpdated = NSNumber(value: false)
                    Dbm().saveChanges()
                })
            }
            
            updateDailyDNDSettings()
            
            execMain({[weak self]  in guard let `self` = self else { return }
                updateWeeklyDNDSettings()
                },delay: 3)
        }
    }
    
    func updateEmailSettings(){
        let settingsObject = Dbm().getSetting()
        if settingsObject.isNotificationsSettingsEmailUpdated!.boolValue {
            //step 1 update mobile notifications settings
            var valuesToUpdate = NSMutableString()
            
            /*
             Email
             */
            if settingsObject.notificationForEmailOption1!.boolValue{
                valuesToUpdate.append("cotravelerpickedup:")
            }
            if settingsObject.notificationForEmailOption2!.boolValue{
                valuesToUpdate.append("emailFeedback:")
            }
            if settingsObject.notificationForEmailOption3!.boolValue{
                valuesToUpdate.append("emaillowBalance:")
            }
            if settingsObject.notificationForEmailOption4!.boolValue{
                valuesToUpdate.append("emailrecharge:")
            }
            if settingsObject.notificationForEmailOption5!.boolValue{
                valuesToUpdate.append("emailcancel:")
            }
            if settingsObject.notificationForEmailOption6!.boolValue{
                valuesToUpdate.append("completeyourtravel:")
            }
            if settingsObject.notificationForEmailOption7!.boolValue{
                valuesToUpdate.append("emailreqacp:")
            }
            if settingsObject.notificationForEmailOption8!.boolValue {
                valuesToUpdate.append("emailreqrec:")
            }
            if settingsObject.notificationForEmailOption9!.boolValue{
                valuesToUpdate.append("emailtravelPayment:")
            }
            if settingsObject.notificationForEmailOption10!.boolValue{
                valuesToUpdate.append("emailreqrej:")
            }
            if settingsObject.notificationForEmailOption11!.boolValue{
                valuesToUpdate.append("emailgreenTknRec:")
            }
            if settingsObject.notificationForEmailOption12!.boolValue{
                valuesToUpdate.append("emailWithdrawel:")
            }
            if settingsObject.notificationForEmailOption13!.boolValue {
                valuesToUpdate.append("chatalertsmail:")
            }
            
            if valuesToUpdate.length > 0 {
                valuesToUpdate = (valuesToUpdate.substring(to: valuesToUpdate.length-1) as NSString) as! NSMutableString
            }else{
                valuesToUpdate = NULL_OBJECT_TO_SEND_ON_SERVER
            }
            
            let information = NSMutableDictionary()
            information.setObject(valuesToUpdate, forKey: "value" as NSCopying)
            let scm = ServerCommunicationManager()
            scm.responseErrorOption = ResponseErrorOption.dontShowErrorResponseMessage
            scm.updateEmailSettings(information, completionBlock: {[weak self] (responseData) -> () in guard let `self` = self else { return }
                let settingsObject = Dbm().getSetting()
                settingsObject.isNotificationsSettingsEmailUpdated = NSNumber(value: false)
                Dbm().saveChanges()
            })
        }
    }
    
    func updateWebsiteSettings(){
        let settingsObject = Dbm().getSetting()
        if settingsObject.isNotificationsSettingsWebsiteUpdated!.boolValue {
            //step 1 update mobile notifications settings
            var valuesToUpdate = NSMutableString()
            
            /*
             Website
             */
            if settingsObject.notificationForWebsiteOption1!.boolValue{
                valuesToUpdate.append("cotravelerpickedup:")
            }
            if settingsObject.notificationForWebsiteOption2!.boolValue{
                valuesToUpdate.append("receivedfeedback:")
            }
            if settingsObject.notificationForWebsiteOption3!.boolValue{
                valuesToUpdate.append("lowbalance:")
            }
            if settingsObject.notificationForWebsiteOption4!.boolValue{
                valuesToUpdate.append("recharge:")
            }
            if settingsObject.notificationForWebsiteOption5!.boolValue{
                valuesToUpdate.append("travelcancelled:")
            }
            if settingsObject.notificationForWebsiteOption6!.boolValue{
                valuesToUpdate.append("completeyourtravel:")
            }
            if settingsObject.notificationForWebsiteOption7!.boolValue{
                valuesToUpdate.append("requestaccepted:")
            }
            if settingsObject.notificationForWebsiteOption8!.boolValue {
                valuesToUpdate.append("requestrecieved:")
            }
            if settingsObject.notificationForWebsiteOption9!.boolValue{
                valuesToUpdate.append("paymentdone:")
            }
            if settingsObject.notificationForWebsiteOption10!.boolValue{
                valuesToUpdate.append("requestrejected:")
            }
            if settingsObject.notificationForWebsiteOption11!.boolValue{
                valuesToUpdate.append("greentokenrecieved:")
            }
            if settingsObject.notificationForWebsiteOption12!.boolValue{
                valuesToUpdate.append("withdrawlrequest:")
            }
            if settingsObject.notificationForWebsiteOption13!.boolValue {
                valuesToUpdate.append("chatalertsweb:")
            }
            
            if valuesToUpdate.length > 0 {
                valuesToUpdate = (valuesToUpdate.substring(to: valuesToUpdate.length-1) as NSString) as! NSMutableString
            }else{
                valuesToUpdate = NULL_OBJECT_TO_SEND_ON_SERVER
            }
            
            let information = NSMutableDictionary()
            information.setObject(valuesToUpdate, forKey: "value" as NSCopying)
            let scm = ServerCommunicationManager()
            scm.responseErrorOption = ResponseErrorOption.dontShowErrorResponseMessage
            scm.updateWebSettings(information, completionBlock:{[weak self] (responseData) -> () in guard let `self` = self else { return }
                let settingsObject = Dbm().getSetting()
                settingsObject.isNotificationsSettingsWebsiteUpdated = NSNumber(value: false)
                Dbm().saveChanges()
            })
        }
    }
    
    
    func updateSMSSettings(){
        let settingsObject = Dbm().getSetting()
        if settingsObject.isNotificationsSettingsSMSUpdated!.boolValue {
            //step 1 update mobile notifications settings
            var valuesToUpdate = NSMutableString()
            
            /*
             SMS
             */
            if settingsObject.notificationForSMSOption1!.boolValue{
                valuesToUpdate.append("cotravelerpickedup:")
            }
            if settingsObject.notificationForSMSOption2!.boolValue{
                valuesToUpdate.append("receivedfeedback:")
            }
            if settingsObject.notificationForSMSOption3!.boolValue{
                valuesToUpdate.append("lowbalance:")
            }
            if settingsObject.notificationForSMSOption4!.boolValue{
                valuesToUpdate.append("recharge:")
            }
            if settingsObject.notificationForSMSOption5!.boolValue{
                valuesToUpdate.append("travelcancelled:")
            }
            if settingsObject.notificationForSMSOption6!.boolValue{
                valuesToUpdate.append("completeyourtravel:")
            }
            if settingsObject.notificationForSMSOption7!.boolValue{
                valuesToUpdate.append("requestaccepted:")
            }
            if settingsObject.notificationForSMSOption8!.boolValue {
                valuesToUpdate.append("requestrecieved:")
            }
            if settingsObject.notificationForSMSOption9!.boolValue{
                valuesToUpdate.append("paymentdone:")
            }
            if settingsObject.notificationForSMSOption10!.boolValue{
                valuesToUpdate.append("requestrejected:")
            }
            if settingsObject.notificationForSMSOption11!.boolValue{
                valuesToUpdate.append("greentokenrecieved:")
            }
            if settingsObject.notificationForSMSOption12!.boolValue{
                valuesToUpdate.append("withdrawlrequest:")
            }
            if settingsObject.notificationForSMSOption13!.boolValue {
                valuesToUpdate.append("chatalertssms:")
            }
            
            if valuesToUpdate.length > 0 {
                valuesToUpdate = (valuesToUpdate.substring(to: valuesToUpdate.length-1) as NSString) as! NSMutableString
            }else{
                valuesToUpdate = NULL_OBJECT_TO_SEND_ON_SERVER
            }
            
            let information = NSMutableDictionary()
            information.setObject(valuesToUpdate, forKey: "value" as NSCopying)
            let scm = ServerCommunicationManager()
            scm.responseErrorOption = ResponseErrorOption.dontShowErrorResponseMessage
            scm.updateSMSSettings(information, completionBlock: {[weak self] (responseData) -> () in guard let `self` = self else { return }
                let settingsObject = Dbm().getSetting()
                settingsObject.isNotificationsSettingsSMSUpdated = NSNumber(value: false)
                Dbm().saveChanges()
            })
        }
    }
    
    func updateMobileSettings(){
        let settingsObject = Dbm().getSetting()
        if settingsObject.isNotificationsSettingsMobileUpdated!.boolValue {
            //step 1 update mobile notifications settings
            var valuesToUpdate = NSMutableString()
            
            /*
             Mobile
             */
            if settingsObject.notificationForMobileOption1!.boolValue{
                valuesToUpdate.append("cotravelerpickedup:")
            }
            if settingsObject.notificationForMobileOption2!.boolValue{
                valuesToUpdate.append("receivedfeedback:")
            }
            if settingsObject.notificationForMobileOption3!.boolValue{
                valuesToUpdate.append("lowbalance:")
            }
            if settingsObject.notificationForMobileOption4!.boolValue{
                valuesToUpdate.append("recharge:")
            }
            if settingsObject.notificationForMobileOption5!.boolValue{
                valuesToUpdate.append("travelcancelled:")
            }
            if settingsObject.notificationForMobileOption6!.boolValue{
                valuesToUpdate.append("completeyourtravel:")
            }
            if settingsObject.notificationForMobileOption7!.boolValue{
                valuesToUpdate.append("requestaccepted:")
            }
            if settingsObject.notificationForMobileOption8!.boolValue {
                valuesToUpdate.append("requestrecieved:")
            }
            if settingsObject.notificationForMobileOption9!.boolValue{
                valuesToUpdate.append("paymentdone:")
            }
            if settingsObject.notificationForMobileOption10!.boolValue{
                valuesToUpdate.append("requestrejected:")
            }
            if settingsObject.notificationForMobileOption11!.boolValue{
                valuesToUpdate.append("greentokenrecieved:")
            }
            if settingsObject.notificationForMobileOption12!.boolValue{
                valuesToUpdate.append("withdrawlrequest:")
            }
            if settingsObject.notificationForMobileOption13!.boolValue {
                valuesToUpdate.append("chatalertsmob:")
            }
            
            if valuesToUpdate.length > 0 {
                valuesToUpdate = (valuesToUpdate.substring(to: valuesToUpdate.length-1) as NSString) as! NSMutableString
            }else{
                valuesToUpdate = NULL_OBJECT_TO_SEND_ON_SERVER
            }
            
            let information = NSMutableDictionary()
            information.setObject(valuesToUpdate, forKey: "value" as NSCopying)
            let scm = ServerCommunicationManager()
            scm.responseErrorOption = ResponseErrorOption.dontShowErrorResponseMessage
            scm.updateMobileSettings(information, completionBlock: {[weak self] (responseData) -> () in guard let `self` = self else { return }
                let settingsObject = Dbm().getSetting()
                settingsObject.isNotificationsSettingsMobileUpdated = NSNumber(value: false)
                Dbm().saveChanges()
            })
        }
    }
    
    func updateProfileToServerIfRequired(_ completion:@escaping ACFCompletionBlock) {
        if isUserLoggedIn() && isAddressInputScreenNeeded() == false {
            let settings = Dbm().getSetting()
            let userInfo = Dbm().getUserInfo()
            if settings.isProfileUpdated!.boolValue {
                let information = NSMutableDictionary()
                let trueForOwnerFalseforPassenger = safeString(userInfo?.isPassenger).isEqual("0")
                copyData(trueForOwnerFalseforPassenger ? "carowner" : "passenger" , destinationDictionary: information, destinationKey: "travelMode", methodName: #function)
                copyData((userInfo!.genderTforMaleFForFemale!.boolValue) ? "male" : "female" , destinationDictionary: information, destinationKey: "gender", methodName: #function)
                copyData(userInfo?.phone , destinationDictionary: information, destinationKey: "mobile", methodName: #function)
                copyData(settings.homeAddress , destinationDictionary: information, destinationKey: "home", methodName: #function)
                copyData(settings.destinationAddress , destinationDictionary: information, destinationKey: "destination", methodName: #function)
                copyData("{\"lat\":\"\(settings.homeLocationLatitude!.doubleValue)\",\"lng\":\"\(settings.homeLocationLongitude!.doubleValue)\"}" , destinationDictionary: information, destinationKey: "home_loc", methodName: #function)
                copyData("{\"lat\":\"\(settings.destinationLocationLatitude!.doubleValue)\",\"lng\":\"\(settings.destinationLocationLongitude!.doubleValue)\"}" , destinationDictionary: information, destinationKey: "ofc_loc", methodName: #function)
                copyData(settings.homeAddressPickUpPoint , destinationDictionary: information, destinationKey: "homePickUp", methodName: #function)
                copyData(settings.destinationAddressPickUpPoint , destinationDictionary: information, destinationKey: "destinationPickUp", methodName: #function)
                copyData(settings.carRegistrationNo , destinationDictionary: information, destinationKey: "carRegistrationNumber", methodName: #function)
                copyData(settings.homeTime?.dateValue().stringTimeOnly24HFormatValue() , destinationDictionary: information, destinationKey: "homeTime", methodName: #function)
                copyData(settings.destinationTime?.dateValue().stringTimeOnly24HFormatValue() , destinationDictionary: information, destinationKey: "destinationTime", methodName: #function)
                copyData(userType() , destinationDictionary: information, destinationKey: "userType", methodName: #function)
                for carMake in Dbm().getAllCarMake(){
                    if isNotNull(settings.carMake)&&(settings.carMake?.length > 0) && ((carMake as! CarMake).name as! NSString).isEqual(to: settings.carMake!){
                        copyData((carMake as! CarMake).mkId , destinationDictionary: information, destinationKey: "make", methodName: #function)
                        break
                    }
                }
                for carModel in Dbm().getAllCarModel(){
                    if isNotNull(settings.carModel)&&(settings.carModel?.length > 0) && ((carModel as! CarModel).name as! NSString).isEqual(to: settings.carModel!){
                        copyData((carModel as! CarModel).mdId , destinationDictionary: information, destinationKey: "model", methodName: #function)
                        break
                    }
                }
                if userType() as String == USER_TYPE_CORPORATE {
                    var companyIdFound = false
                    for company in Dbm().getAllCompanies(){
                        if isNotNull(userInfo?.destinationName)&&(userInfo?.destinationName!.length > 0) && ((company as! Company).name as! NSString).isEqual(to: userInfo!.destinationName!){
                            if ((company as! Company).id as! NSString).contains("-") == false{
                                copyData((company as! Company).id , destinationDictionary: information, destinationKey: "companyId", methodName: #function)
                                companyIdFound = true
                                break
                            }
                        }
                    }
                    if companyIdFound == false {
                        copyData("other" , destinationDictionary: information, destinationKey: "companyId", methodName: #function)
                        copyData(userInfo!.destinationName , destinationDictionary: information, destinationKey: "destinationName", methodName: #function)
                    }
                }else if userType() as String == USER_TYPE_STUDENT {
                    var collegeIdFound = false
                    for college in Dbm().getAllColleges(){
                        if isNotNull(userInfo?.destinationName)&&(userInfo?.destinationName!.length > 0) && ((college as! College).name as! NSString).isEqual(to: userInfo!.destinationName!){
                            if ((college as! College).id as! NSString).contains("-") == false{
                                copyData((college as! College).id , destinationDictionary: information, destinationKey: "companyId", methodName: #function)
                                collegeIdFound = true
                                break
                            }
                        }
                    }
                    if collegeIdFound == false {
                        copyData("other" , destinationDictionary: information, destinationKey: "companyId", methodName: #function)
                        copyData(userInfo!.destinationName , destinationDictionary: information, destinationKey: "destinationName", methodName: #function)
                    }
                }
                
                copyData(trueForOwnerFalseforPassenger ? "1" : "0" , destinationDictionary: information, destinationKey: "user_is_passenger", methodName: #function)
                
                let scm = ServerCommunicationManager()
                scm.returnFailureResponseAlso = true
                scm.updateUserProfile(information) { (responseData) -> () in
                    if isNotNull(responseData){
                        trackFunctionalEvent(FE_PROFILE_UPDATE, information: nil)
                        settings.isProfileUpdated = NSNumber(value: false)
                        execMain({[weak self]  in guard let `self` = self else { return }
                            self.updateUserProfileInformationFromServerIfRequired()
                            },delay: 1)
                        completion(responseData)
                    }else{
                        trackFunctionalEvent(FE_PROFILE_UPDATE, information: nil,isSuccess: false)
                        completion(nil)
                    }
                }
            }
        }else{
            completion(nil)
        }
    }
    
    func updateSettingsToServerIfRequired(){
        /*
         Notification
         */
        updateMobileSettings()
        updateEmailSettings()
        updateSMSSettings()
        updateWebsiteSettings()
        updateDNDSettings()
        managePushNotificationsOnQuickBlox()
    }
    
    func syncSetting() {
        if isSettingsPendingToBeSyncOnServer() {
            updateSettingsToServerIfRequired()
        }else{
            updateSettingsFromServerIfRequired()
        }
    }
    
    func isSettingsPendingToBeSyncOnServer()->Bool{
        let settings = Dbm().getSetting()
        if settings.isNotificationsSettingsMobileUpdated!.boolValue ||
            settings.isNotificationsSettingsWebsiteUpdated!.boolValue ||
            settings.isNotificationsSettingsSMSUpdated!.boolValue ||
            settings.isNotificationsSettingsEmailUpdated!.boolValue ||
            settings.isDoNotDisturbSettingsUpdated!.boolValue {
            return true
        }
        return false
    }
    
    func updateMyTravelPreferences(){
        if isSystemReadyToProcessThis() {
            let information = NSMutableDictionary()
            let scm = ServerCommunicationManager()
            scm.responseErrorOption = ResponseErrorOption.dontShowErrorResponseMessage
            scm.getMyTravelPreferences(information, completionBlock: {[weak self] (responseData) -> () in guard let `self` = self else { return }
                if let _ = responseData {
                    let myDestinationOnly = (responseData!.object(forKey: "data")! as AnyObject).object(forKey: "companyFlag") as! Bool
                    let myGroupOnly = (responseData!.object(forKey: "data")! as AnyObject).object(forKey: "groupFlag") as! Bool
                    let settings = Dbm().getSetting()
                    settings.travelOptionType1 = NSNumber(value: myGroupOnly as Bool)
                    settings.travelOptionType2 = NSNumber(value: myDestinationOnly as Bool)
                    Dbm().saveChanges()
                }
            })
        }
    }
    
    func updateSettingsFromServerIfRequired(){
        if isUserLoggedIn() {
            let information = NSMutableDictionary()
            let userInfo = Dbm().getUserInfo()
            copyData(userInfo!.userId , destinationDictionary: information, destinationKey: "userId", methodName: #function)
            let scm = ServerCommunicationManager()
            scm.responseErrorOption = ResponseErrorOption.dontShowErrorResponseMessage
            scm.getAllSettings(information, completionBlock: {[weak self] (responseData) -> () in guard let `self` = self else { return }
                if let _ = responseData {
                    if self.isSettingsPendingToBeSyncOnServer() {
                        return
                    }
                    
                    let settingsObject = Dbm().getSetting()
                    let settingData = responseData!["data"] as! NSDictionary
                    
                    /*
                     Notification
                     */
                    let mailSetting = dictionary(settingData.object(forKey: "uss_mail"))
                    let mobileSetting = dictionary(settingData.object(forKey: "uss_mobile"))
                    let smsSetting = dictionary(settingData.object(forKey: "uss_sms"))
                    let websiteSetting = dictionary(settingData.object(forKey: "uss_web"))
                    
                    /*
                     Email
                     */
                    
                    
                    if isNotNull(mailSetting.object(forKey: "cotravelerpickedup")){
                        settingsObject.notificationForEmailOption1 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForEmailOption1 = NSNumber(value: false)
                    }
                    if isNotNull(mailSetting.object(forKey: "emailFeedback")){
                        settingsObject.notificationForEmailOption2 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForEmailOption2 = NSNumber(value: false)
                    }
                    if isNotNull(mailSetting.object(forKey: "emaillowBalance")){
                        settingsObject.notificationForEmailOption3 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForEmailOption3 = NSNumber(value: false)
                    }
                    if isNotNull(mailSetting.object(forKey: "emailrecharge")){
                        settingsObject.notificationForEmailOption4 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForEmailOption4 = NSNumber(value: false)
                    }
                    if isNotNull(mailSetting.object(forKey: "emailcancel")){
                        settingsObject.notificationForEmailOption5 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForEmailOption5 = NSNumber(value: false)
                    }
                    if isNotNull(mailSetting.object(forKey: "completeyourtravel")){
                        settingsObject.notificationForEmailOption6 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForEmailOption6 = NSNumber(value: false)
                    }
                    if isNotNull(mailSetting.object(forKey: "emailreqacp")){
                        settingsObject.notificationForEmailOption7 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForEmailOption7 = NSNumber(value: false)
                    }
                    if isNotNull(mailSetting.object(forKey: "emailreqrec")){
                        settingsObject.notificationForEmailOption8 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForEmailOption8 = NSNumber(value: false)
                    }
                    if isNotNull(mailSetting.object(forKey: "emailtravelPayment")){
                        settingsObject.notificationForEmailOption9 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForEmailOption9 = NSNumber(value: false)
                    }
                    if isNotNull(mailSetting.object(forKey: "emailreqrej")){
                        settingsObject.notificationForEmailOption10 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForEmailOption10 = NSNumber(value: false)
                    }
                    if isNotNull(mailSetting.object(forKey: "emailgreenTknRec")){
                        settingsObject.notificationForEmailOption11 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForEmailOption11 = NSNumber(value: false)
                    }
                    if isNotNull(mailSetting.object(forKey: "emailWithdrawel")){
                        settingsObject.notificationForEmailOption12 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForEmailOption12 = NSNumber(value: false)
                    }
                    if isNotNull(mailSetting.object(forKey: "chatalertsmail")){
                        settingsObject.notificationForEmailOption13 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForEmailOption13 = NSNumber(value: false)
                    }
                    
                    /*
                     Mobile
                     */
                    if isNotNull(mobileSetting.object(forKey: "cotravelerpickedup")){
                        settingsObject.notificationForMobileOption1 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForMobileOption1 = NSNumber(value: false)
                    }
                    if isNotNull(mobileSetting.object(forKey: "receivedfeedback")){
                        settingsObject.notificationForMobileOption2 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForMobileOption2 = NSNumber(value: false)
                    }
                    if isNotNull(mobileSetting.object(forKey: "lowbalance")){
                        settingsObject.notificationForMobileOption3 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForMobileOption3 = NSNumber(value: false)
                    }
                    if isNotNull(mobileSetting.object(forKey: "recharge")){
                        settingsObject.notificationForMobileOption4 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForMobileOption4 = NSNumber(value: false)
                    }
                    if isNotNull(mobileSetting.object(forKey: "travelcancelled")){
                        settingsObject.notificationForMobileOption5 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForMobileOption5 = NSNumber(value: false)
                    }
                    if isNotNull(mobileSetting.object(forKey: "completeyourtravel")){
                        settingsObject.notificationForMobileOption6 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForMobileOption6 = NSNumber(value: false)
                    }
                    if isNotNull(mobileSetting.object(forKey: "requestaccepted")){
                        settingsObject.notificationForMobileOption7 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForMobileOption7 = NSNumber(value: false)
                    }
                    if isNotNull(mobileSetting.object(forKey: "requestrecieved")){
                        settingsObject.notificationForMobileOption8 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForMobileOption8 = NSNumber(value: false)
                    }
                    if isNotNull(mobileSetting.object(forKey: "paymentdone")){
                        settingsObject.notificationForMobileOption9 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForMobileOption9 = NSNumber(value: false)
                    }
                    if isNotNull(mobileSetting.object(forKey: "requestrejected")){
                        settingsObject.notificationForMobileOption10 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForMobileOption10 = NSNumber(value: false)
                    }
                    if isNotNull(mobileSetting.object(forKey: "greentokenrecieved")){
                        settingsObject.notificationForMobileOption11 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForMobileOption11 = NSNumber(value: false)
                    }
                    if isNotNull(mobileSetting.object(forKey: "withdrawlrequest")){
                        settingsObject.notificationForMobileOption12 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForMobileOption12 = NSNumber(value: false)
                    }
                    if isNotNull(mobileSetting.object(forKey: "chatalertsmob")){
                        settingsObject.notificationForMobileOption13 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForMobileOption13 = NSNumber(value: false)
                    }
                    
                    
                    /*
                     Website
                     */
                    if isNotNull(websiteSetting.object(forKey: "cotravelerpickedup")){
                        settingsObject.notificationForWebsiteOption1 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForWebsiteOption1 = NSNumber(value: false)
                    }
                    if isNotNull(websiteSetting.object(forKey: "receivedfeedback")){
                        settingsObject.notificationForWebsiteOption2 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForWebsiteOption2 = NSNumber(value: false)
                    }
                    if isNotNull(websiteSetting.object(forKey: "lowbalance")){
                        settingsObject.notificationForWebsiteOption3 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForWebsiteOption3 = NSNumber(value: false)
                    }
                    if isNotNull(websiteSetting.object(forKey: "recharge")){
                        settingsObject.notificationForWebsiteOption4 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForWebsiteOption4 = NSNumber(value: false)
                    }
                    if isNotNull(websiteSetting.object(forKey: "travelcancelled")){
                        settingsObject.notificationForWebsiteOption5 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForWebsiteOption5 = NSNumber(value: false)
                    }
                    if isNotNull(websiteSetting.object(forKey: "completeyourtravel")){
                        settingsObject.notificationForWebsiteOption6 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForWebsiteOption6 = NSNumber(value: false)
                    }
                    if isNotNull(websiteSetting.object(forKey: "requestaccepted")){
                        settingsObject.notificationForWebsiteOption7 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForWebsiteOption7 = NSNumber(value: false)
                    }
                    if isNotNull(websiteSetting.object(forKey: "requestrecieved")){
                        settingsObject.notificationForWebsiteOption8 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForWebsiteOption8 = NSNumber(value: false)
                    }
                    if isNotNull(websiteSetting.object(forKey: "paymentdone")){
                        settingsObject.notificationForWebsiteOption9 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForWebsiteOption9 = NSNumber(value: false)
                    }
                    if isNotNull(websiteSetting.object(forKey: "requestrejected")){
                        settingsObject.notificationForWebsiteOption10 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForWebsiteOption10 = NSNumber(value: false)
                    }
                    if isNotNull(websiteSetting.object(forKey: "greentokenrecieved")){
                        settingsObject.notificationForWebsiteOption11 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForWebsiteOption11 = NSNumber(value: false)
                    }
                    if isNotNull(websiteSetting.object(forKey: "withdrawlrequest")){
                        settingsObject.notificationForWebsiteOption12 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForWebsiteOption12 = NSNumber(value: false)
                    }
                    if isNotNull(websiteSetting.object(forKey: "chatalertsweb")){
                        settingsObject.notificationForWebsiteOption13 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForWebsiteOption13 = NSNumber(value: false)
                    }
                    
                    
                    /*
                     SMS
                     */
                    if isNotNull(smsSetting.object(forKey: "cotravelerpickedup")){
                        settingsObject.notificationForSMSOption1 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForSMSOption1 = NSNumber(value: false)
                    }
                    if isNotNull(smsSetting.object(forKey: "receivedfeedback")){
                        settingsObject.notificationForSMSOption2 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForSMSOption2 = NSNumber(value: false)
                    }
                    if isNotNull(smsSetting.object(forKey: "lowbalance")){
                        settingsObject.notificationForSMSOption3 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForSMSOption3 = NSNumber(value: false)
                    }
                    if isNotNull(smsSetting.object(forKey: "recharge")){
                        settingsObject.notificationForSMSOption4 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForSMSOption4 = NSNumber(value: false)
                    }
                    if isNotNull(smsSetting.object(forKey: "travelcancelled")){
                        settingsObject.notificationForSMSOption5 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForSMSOption5 = NSNumber(value: false)
                    }
                    if isNotNull(smsSetting.object(forKey: "completeyourtravel")){
                        settingsObject.notificationForSMSOption6 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForSMSOption6 = NSNumber(value: false)
                    }
                    if isNotNull(smsSetting.object(forKey: "requestaccepted")){
                        settingsObject.notificationForSMSOption7 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForSMSOption7 = NSNumber(value: false)
                    }
                    if isNotNull(smsSetting.object(forKey: "requestrecieved")){
                        settingsObject.notificationForSMSOption8 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForSMSOption8 = NSNumber(value: false)
                    }
                    if isNotNull(smsSetting.object(forKey: "paymentdone")){
                        settingsObject.notificationForSMSOption9 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForSMSOption9 = NSNumber(value: false)
                    }
                    if isNotNull(smsSetting.object(forKey: "requestrejected")){
                        settingsObject.notificationForSMSOption10 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForSMSOption10 = NSNumber(value: false)
                    }
                    if isNotNull(smsSetting.object(forKey: "greentokenrecieved")){
                        settingsObject.notificationForSMSOption11 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForSMSOption11 = NSNumber(value: false)
                    }
                    if isNotNull(smsSetting.object(forKey: "withdrawlrequest")){
                        settingsObject.notificationForSMSOption12 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForSMSOption12 = NSNumber(value: false)
                    }
                    if isNotNull(smsSetting.object(forKey: "chatalertssms")){
                        settingsObject.notificationForSMSOption13 = NSNumber(value: true as Bool)
                    }else{
                        settingsObject.notificationForSMSOption13 = NSNumber(value: false)
                    }
                    
                    
                    /*
                     DoNotDisturb
                     */
                    var weekDayDNDSettings = NSDictionary()
                    let noSetWeekDays = (settingData.object(forKey: "nosetweekday") as! NSArray)
                    if noSetWeekDays.count > 0{
                        if noSetWeekDays.object(at: 0) is NSDictionary {
                            weekDayDNDSettings = noSetWeekDays.object(at: 0) as! NSDictionary
                        }
                    }
                    
                    let date1 = Date().toStringValue("yyyy-MM-dd")
                    let date2 = Date().dateByAddingDays(1).toStringValue("yyyy-MM-dd")
                    let date3 = Date().dateByAddingDays(2).toStringValue("yyyy-MM-dd")
                    let date4 = Date().dateByAddingDays(3).toStringValue("yyyy-MM-dd")
                    let date5 = Date().dateByAddingDays(4).toStringValue("yyyy-MM-dd")
                    let date6 = Date().dateByAddingDays(5).toStringValue("yyyy-MM-dd")
                    let date7 = Date().dateByAddingDays(6).toStringValue("yyyy-MM-dd")
                    
                    settingsObject.doNotDisturbForDailyOption1 = NSNumber(value: false)
                    settingsObject.doNotDisturbForDailyOption2 = NSNumber(value: false)
                    settingsObject.doNotDisturbForDailyOption3 = NSNumber(value: false)
                    settingsObject.doNotDisturbForDailyOption4 = NSNumber(value: false)
                    settingsObject.doNotDisturbForDailyOption5 = NSNumber(value: false)
                    settingsObject.doNotDisturbForDailyOption6 = NSNumber(value: false)
                    settingsObject.doNotDisturbForDailyOption7 = NSNumber(value: false)
                    
                    for day in weekDayDNDSettings.allValues {
                        if (date1 as NSString).contains(day as! String){
                            settingsObject.doNotDisturbForDailyOption1 = NSNumber(value: true as Bool)
                        }
                        else if (date2 as NSString).contains(day as! String){
                            settingsObject.doNotDisturbForDailyOption2 = NSNumber(value: true as Bool)
                        }
                        else if (date3 as NSString).contains(day as! String){
                            settingsObject.doNotDisturbForDailyOption3 = NSNumber(value: true as Bool)
                        }
                        else if (date4 as NSString).contains(day as! String){
                            settingsObject.doNotDisturbForDailyOption4 = NSNumber(value: true as Bool)
                        }
                        else if (date5 as NSString).contains(day as! String){
                            settingsObject.doNotDisturbForDailyOption5 = NSNumber(value: true as Bool)
                        }
                        else if (date6 as NSString).contains(day as! String){
                            settingsObject.doNotDisturbForDailyOption6 = NSNumber(value: true as Bool)
                        }
                        else if (date7 as NSString).contains(day as! String){
                            settingsObject.doNotDisturbForDailyOption7 = NSNumber(value: true as Bool)
                        }
                    }
                    
                    
                    
                    var weeklyDNDSettings = NSDictionary()
                    let noSetDays = (settingData.object(forKey: "nosetdays") as! NSArray)
                    if noSetDays.count > 0{
                        if noSetDays.object(at: 0) is NSDictionary {
                            weeklyDNDSettings = noSetDays.object(at: 0) as! NSDictionary
                        }
                    }
                    
                    settingsObject.doNotDisturbForWeeklyOption1 = NSNumber(value: false)
                    settingsObject.doNotDisturbForWeeklyOption2 = NSNumber(value: false)
                    settingsObject.doNotDisturbForWeeklyOption3 = NSNumber(value: false)
                    settingsObject.doNotDisturbForWeeklyOption4 = NSNumber(value: false)
                    settingsObject.doNotDisturbForWeeklyOption5 = NSNumber(value: false)
                    settingsObject.doNotDisturbForWeeklyOption6 = NSNumber(value: false)
                    settingsObject.doNotDisturbForWeeklyOption7 = NSNumber(value: false)
                    
                    for day in weeklyDNDSettings.allValues {
                        let dayString = (day as! NSString).lowercased
                        if dayString.contains("mon"){
                            settingsObject.doNotDisturbForWeeklyOption1 = NSNumber(value: true as Bool)
                        }
                        else if dayString.contains("tue"){
                            settingsObject.doNotDisturbForWeeklyOption2 = NSNumber(value: true as Bool)
                        }
                        else if dayString.contains("wed"){
                            settingsObject.doNotDisturbForWeeklyOption3 = NSNumber(value: true as Bool)
                        }
                        else if dayString.contains("thu"){
                            settingsObject.doNotDisturbForWeeklyOption4 = NSNumber(value: true as Bool)
                        }
                        else if dayString.contains("fri"){
                            settingsObject.doNotDisturbForWeeklyOption5 = NSNumber(value: true as Bool)
                        }
                        else if dayString.contains("sat"){
                            settingsObject.doNotDisturbForWeeklyOption6 = NSNumber(value: true as Bool)
                        }
                        else if dayString.contains("sun"){
                            settingsObject.doNotDisturbForWeeklyOption7 = NSNumber(value: true as Bool)
                        }
                    }
                    
                    Dbm().saveChanges()
                }
            })
        }
    }
    
    func updateQBUsersCacheFromServerIfRequired(_ completion:ACFCompletionBlock?){
        if(isInternetConnectivityAvailable(false)==false){return}
        ServicesManager.instance().usersService.loadFromCache()
        let suggestedLoginIds = Acf().getSuggestedLoginIds()
        if suggestedLoginIds.count > 0 {
            ServicesManager.instance().downloadUsers(suggestedLoginIds, success: { (users) -> Void in
                ServicesManager.instance().usersService.addUsers(onCache: users)
                ServicesManager.instance().usersService.loadFromCache()
                if completion != nil && isNotNull(users) && users!.count > 0{
                    completion!([users] as NSArray)
                }
            }, error: { (error) -> Void in
            })
        }
    }
    
    func playSoundAndVibrateForNewMessageIfNeeded(){
        let settings = Dbm().getSetting()
        if settings.notificationVibrations!.boolValue {
            vibrate()
        }
        if settings.notificationSounds!.boolValue {
            playNewMessageSound()
        }
    }
    
    func playNewMessageSound(){
        SAMSoundEffect.playNamed("alertSound1.mp3")
    }
    
    func playDefaultAlertSound(){
        SAMSoundEffect.playNamed("alertSound2.mp3")
    }
    
    func dialogMessage(_ dialogID: String!) -> QBChatDialog? {
        return ServicesManager.instance().chatService.dialogsMemoryStorage.chatDialog(withID: dialogID)
    }
    
    func setImageWithPresence(_ imageView:UIImageView,chatDialog:QBChatDialog?){
        imageView.contentMode = .scaleAspectFill
        let image = USER_PLACEHOLDER_IMAGE
        setPresence(2, view: imageView)
        if chatDialog != nil {
            if chatDialog!.type == .private {
                var user:QBUUser?
                if chatDialog!.recipientID > 0 {
                    user = ServicesManager.instance().usersService.usersMemoryStorage.user(withID: UInt(chatDialog!.recipientID))
                }
                if isNotNull(user){
                    let userId = Acf().getUserIdFrom(user!.login)
                    let userPicture = Dbm().getUserPicture(["userId":userId])
                    if isNotNull(userPicture){
                        imageView.sd_setImage(with: URL(string:getUserProfilePictureUrlFromFileName(userPicture!.url!)), placeholderImage: image)
                    }else if isNotNull(user!.website){
                        imageView.sd_setImage(with: URL(string: user!.website!), placeholderImage: image)
                    }else{
                        imageView.image = image
                    }
                }else{
                    imageView.image = image
                }
                if ENABLE_QUICKBLOX_CONTACT_PRESENCE_DISPLAY {
                    if getPresenceQBUser(user) {
                        setPresence(1, view: imageView)
                    }else{
                        setPresence(0, view: imageView)
                    }
                }
            }else {
                let groupNameToDisplay = displayNameFromName(chatDialog!.name)
                if (groupNameToDisplay as! NSString).lowercased.contains(RIDE_GROUP_SUFFIX.lowercased()){
                    imageView.image = UIImage(named: "group")
                }else if (groupNameToDisplay as! NSString).lowercased.contains(RIDE_GROUP_DISPLAY_NAME_OTHERS_PREFIX.lowercased()){
                    imageView.image = UIImage(named: "group")
                }else if (groupNameToDisplay as! NSString).lowercased.contains(RIDE_GROUP_DISPLAY_NAME_SELF_SUFFIX.lowercased()){
                    imageView.image = UIImage(named: "group")
                }else{
                    imageView.image = UIImage(named: "groupIcon")
                }
            }
        }else{
            imageView.image = image
        }
    }
    
    func getPresenceUser(_ user:NSDictionary)->Bool {
        if isNotNull(user) && isNotNull(user.object(forKey: "qbid")){
            let key = "\(user.object(forKey: "qbid")!)"
            if isNotNull(presenceInformation.object(forKey: key)){
                return (presenceInformation.object(forKey: key)! as AnyObject).boolValue
            }else{
                return false
            }
        }else{
            return false
        }
    }
    
    func getPresenceQBUser(_ qbUser:QBUUser?)->Bool {
        if isNotNull(qbUser){
            let key = "\(qbUser!.id)"
            if isNotNull(presenceInformation.object(forKey: key)){
                return (presenceInformation.object(forKey: key)! as AnyObject).boolValue
            }else{
                return false
            }
        }else{
            return false
        }
    }
    
    func setImageWithPresence(_ imageView:UIImageView,user:QBUUser?){
        let image = USER_PLACEHOLDER_IMAGE
        if isNotNull(user){
            let userId = Acf().getUserIdFrom(user!.login)
            let userPicture = Dbm().getUserPicture(["userId":userId])
            if isNotNull(userPicture){
                imageView.sd_setImage(with: URL(string:getUserProfilePictureUrlFromFileName(userPicture!.url!)), placeholderImage: image)
            }else if isNotNull(user!.website){
                imageView.sd_setImage(with: URL(string: user!.website!), placeholderImage: image)
            }else{
                imageView.image = image
            }
        }else{
            imageView.image = image
        }
        if ENABLE_QUICKBLOX_CONTACT_PRESENCE_DISPLAY {
            if isNotNull(user) && getPresenceQBUser(user!){
                setPresence(1, view: imageView)
            }else{
                setPresence(0, view: imageView)
            }
        }else{
            setPresence(2, view: imageView)
        }
    }
    
    func setPresence(_ presence:Int,view:UIView){
        //presence 0 offline , 1 online , 2 nothing
        view.badge.badgeValue = 0
        view.badge.outlineWidth = 2
        view.badge.minimumDiameter = 16
        view.badge.outlineColor = UIColor.white
        view.badge.displayWhenZero = true
        view.badge.isHidden = false
        view.layer.masksToBounds = true
        view.layer.cornerRadius = view.frame.size.height/2
        view.clipsToBounds = true
        if presence == 0 {
            view.badge.badgeColor = APP_THEME_LIGHT_GRAY_COLOR.withAlphaComponent(0.8)
        }else if presence == 1 {
            view.badge.badgeColor = UIColor(colorLiteralRed: 100/255.0, green: 186/255.0, blue: 85/255.0, alpha: 1.0)
        }else {
            view.badge.outlineColor = UIColor.clear
            view.badge.badgeColor = UIColor.clear
        }
        view.badge.setNeedsDisplay()
        view.badge.setNeedsLayout()
        view.badge.layoutIfNeeded()
    }
    
    func sendMessage(_ message:QBChatMessage?,chatDialogue:QBChatDialog){
        if isNotNull(message){
            if(isInternetConnectivityAvailable(true)==false){return}
            Dbm().deleteFailedQBMessage(["messageId":message!.id!])
            ServicesManager.instance().chatService.send(message!, toDialogID: chatDialogue.id!, saveToHistory: true, saveToStorage: true, completion: { (error) in
                if (error != nil) {
                    Dbm().addFailedQBMessage(["messageId":message!.id!])
                }
            })
        }
    }
    
    func updateFavouriteUsersPrioritySettingsOnServerIfRequired(){
        if(isInternetConnectivityAvailable(true)==false){return}
        if isUserLoggedIn() {
            let settings = Dbm().getSetting()
            if settings.isFavouriteListUpdated!.boolValue {
                let users = Dbm().getAllFavourites()
                let dataToSend = NSMutableString()
                dataToSend.append("{")
                for user in users{
                    let p = "\"\((user as! Favourites).priority!)\""
                    let u = "\"\((user as! Favourites).userId!)\""
                    dataToSend.append("\(p):\(u),")
                }
                if dataToSend.length > 1 {
                    dataToSend.deleteCharacters(in: NSMakeRange(dataToSend.length-1, 1))
                }
                dataToSend.append("}")
                let information = ["userIds":dataToSend] as NSDictionary
                let scm = ServerCommunicationManager()
                scm.responseErrorOption = ResponseErrorOption.dontShowErrorResponseMessage
                scm.updatePriority(information, completionBlock: {[weak self] (responseData) -> () in guard let `self` = self else { return }
                    if let _ = responseData {
                        trackFunctionalEvent(FE_UPDATE_FAVOURITES_TO_SERVER, information: nil)
                        settings.isFavouriteListUpdated = NSNumber(value: false)
                        Dbm().saveChanges()
                    }else{
                        trackFunctionalEvent(FE_UPDATE_FAVOURITES_TO_SERVER, information: nil,isSuccess:false)
                    }
                })
            }
        }
    }
    
    func unSetOpenUserProfileOfView(_ view:UIView?){
        if isNotNull(view){
            view?.gestureRecognizers?.removeAll()
        }
    }
    
    func setOpenUserProfileOfView(_ view:UIView? , navigationController:UINavigationController? , userInfo : NSDictionary? , userId : NSString?){
        if isNotNull(view){
            view?.setAssociatedValue(navigationController, forKey: &oUPVNavigationControllerKey)
            view?.setAssociatedValue(userInfo, forKey: &oUPVUserInfoKey)
            view?.setAssociatedValue(userId, forKey: &oUPVUserIdKey)
            view?.gestureRecognizers?.removeAll()
            let tapGestureRecogniser = UITapGestureRecognizer(target: self, action: #selector(AppCommonFunctions.viewTappedToOpenUserProfile(_:)))
            tapGestureRecogniser.numberOfTapsRequired = 1
            view?.addGestureRecognizer(tapGestureRecogniser)
            view?.isUserInteractionEnabled = true
        }
    }
    
    func viewTappedToOpenUserProfile(_ sender:UITapGestureRecognizer){
        if let view = sender.view {
            performAnimatedClickEffectType1(view)
            let navigationController = view.associatedValue(forKey: &oUPVNavigationControllerKey) as! UINavigationController
            let userInfo = view.associatedValue(forKey: &oUPVUserInfoKey) as? NSDictionary
            let userId = view.associatedValue(forKey: &oUPVUserIdKey) as? NSString
            if isNotNull(userId){
                let information = ["userId":userId!] as NSDictionary
                let scm = ServerCommunicationManager()
                scm.progressIndicatorText = ""
                scm.responseErrorOption = ResponseErrorOption.dontShowErrorResponseMessage
                scm.getUserProfile(information, completionBlock: {[weak self] (responseData) -> () in guard let `self` = self else { return }
                    if let _ = responseData {
                        self.pushVC("ProfileDetailsPublicViewController", navigationController: navigationController, isRootViewController: false, animated: true, modifyObject: { (viewControllerObject) -> () in
                            let reponseData = NSMutableDictionary(dictionary: responseData!.object(forKey: "data") as! NSDictionary)
                            reponseData.setObject(userId!, forKey: "userId" as NSCopying)
                        })
                    }
                })
            }else if isNotNull(userInfo){
                pushVC("ProfileDetailsPublicViewController", navigationController: navigationController, isRootViewController: false, animated: true, modifyObject: { (viewControllerObject) -> () in
                })
            }
        }
    }
    
    func getCurrentCityCode() ->(String) {
        return "1"
    }
    
    func showLocationOnPopupMapScreen(_ location:CLLocationCoordinate2D?,address:String?){
        let gapX = getRequiredPopupSideGap(#function)
        let gapY = getRequiredPopupVerticleGap(#function)
        let mapViewViewController = getViewController("MapViewViewController") as! MapViewViewController
        mapViewViewController.locationCoordinate = location
        mapViewViewController.address = address
        let navigationController = UINavigationController(rootViewController: mapViewViewController)
        navigationController.view.frame = CGRect(x: gapX,y: gapY,width: DEVICE_WIDTH-2*gapX, height: DEVICE_HEIGHT-2*gapY)
        navigationController.view.layer.cornerRadius = POP_UP_VIEW_CORNER_RADIUS
        navigationController.view.layer.masksToBounds = true
        self.navigationController!.presentPopupViewController(navigationController, animationType: .fade, completion: { () -> Void in
        })
    }
    
    func setFavourite(_ userInfo:NSDictionary?,isFavourite:Bool){
        if(isInternetConnectivityAvailable(true)==false){return}
        let information = ["toUserId":getUserId(userInfo)]
        let scm = ServerCommunicationManager()
        scm.responseErrorOption = .dontShowErrorResponseMessage
        if isFavourite {
            Dbm().addFavourites(userInfo!)
            scm.setUserFavourite(information as NSDictionary) {[weak self](responseData)->() in guard let `self` = self else { return } }
        }else{
            Dbm().removeFavourite(userInfo!)
            scm.setUserUnFavourite(information as NSDictionary) {[weak self](responseData)->() in guard let `self` = self else { return }}
        }
        NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_FAVOURITES_UPDATED), object: nil)
    }
    
    func getBlockedUsers()->NSMutableArray{
        let qbUsersList = NSMutableArray()
        if let blockedList = ServicesManager.instance().contactListService.blockedUserListLocalCache {
            for qbIdString in blockedList {
                if let recipient = ServicesManager.instance().usersService.usersMemoryStorage.user(withID: UInt((qbIdString as! String).toInt())) {
                    qbUsersList.add(recipient)
                }
            }
        }
        return qbUsersList
    }
    
    func block(_ userInfo:NSDictionary?,isBlock:Bool){
        if(isInternetConnectivityAvailable(true)==false){return}
        func performBlockUnBlockOnOrahi(){
            let information = ["toUserId":getUserId(userInfo)]
            let scm = ServerCommunicationManager()
            if isBlock {
                scm.progressIndicatorText = "\(isBlock ? "Blocking" : "UnBlocking") : Rides"
                scm.returnFailureResponseAlso = true
                scm.responseErrorOption = ResponseErrorOption.dontShowErrorResponseMessage
                scm.setUserBlocked(information as NSDictionary) {(responseData)->() in
                    if isNotNull(responseData){
                        if let status = responseData?.object(forKey: "status") {
                            if ("\(status)" as NSString).isEqual(to: "1"){
                                performBlockUnBlockOnQuickBlox()
                            }else if let message = responseData?.object(forKey: "message") as? NSString{
                                if message.contains("already"){
                                    performBlockUnBlockOnQuickBlox()
                                }
                            }
                        }
                    }
                }
            }else{
                scm.progressIndicatorText = "\(isBlock ? "Blocking" : "UnBlocking") : Rides"
                scm.returnFailureResponseAlso = true
                scm.responseErrorOption = ResponseErrorOption.dontShowErrorResponseMessage
                scm.setUserUnBlocked(information as NSDictionary) {(responseData)->() in
                    performBlockUnBlockOnQuickBlox()
                }
            }
        }
        
        func performBlockUnBlockOnQuickBlox(){
            let action = isBlock ? "Blocking" : "UnBlocking"
            showActivityIndicator("\(action) : Chats")
            let login = getQuickBloxUserName(getUserId(userInfo))
            ServicesManager.instance().searchUsersWithLogins([login as NSString], success: { (users) -> Void in
                hideActivityIndicator()
                if isNotNull(users) && users!.count > 0 {
                    let user = users![0]
                    ServicesManager.instance().contactListService.performUserAction(user.id, block: isBlock)
                    if isBlock {
                        performAfterBlockActionForUser(user)
                    }else{
                        performAfterUnBlockActionForUser(user)
                    }
                }
            }, error: { (error) -> Void in
                hideActivityIndicator()
                logMessage("\(error)")
            })
            removePresenceInformation()
        }
        func removePresenceInformation(){
            Acf().presenceInformation.removeObject(forKey: "\(getUserId(userInfo))")
        }
        performBlockUnBlockOnOrahi()
    }
    
    func estimateUserExpectedTravelMode()->NextRideEstimatedMoment{
        return estimateUserExpectedTravelModeForDate(date: Date())
    }
    
    func estimateUserExpectedTravelModeForDate(date:Date)->NextRideEstimatedMoment{
        if date.hour() <= 11 {
            return NextRideEstimatedMoment.morning
        }else{
            return NextRideEstimatedMoment.evening
        }
    }
    
    func syncGroupsAndPlacesFromServer(){
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(AppCommonFunctions.syncGroupsAndPlacesFromServerPrivate), object: nil)
        self.perform(#selector(AppCommonFunctions.syncGroupsAndPlacesFromServerPrivate), with: nil, afterDelay: 2)
    }
    
    func syncGroupsAndPlacesFromServerPrivate(){
        syncGroupsAndPlaces {}
    }
    
    func syncGroupsAndPlaces(completion:@escaping ACFCompletionBlockType2){
        if(isInternetConnectivityAvailable(false)==false){completion();return}
        if isUserLoggedIn() {
            let qbId = safeString(ServicesManager.instance().currentUser()?.id)
            let information = NSMutableDictionary()
            copyData(qbId, destinationDictionary: information, destinationKey: "qbId", methodName: #function)
            let scm = ServerCommunicationManager()
            scm.responseErrorOption = ResponseErrorOption.dontShowErrorResponseMessage
            scm.returnFailureResponseAlso = true
            scm.getAllGroups(information, completionBlock: {[weak self] (responseData) -> () in guard let `self` = self else { return }
                if let _ = responseData {
                    if let groups = responseData?.value(forKeyPath: "data.groups") as? NSArray {
                        Dbm().removeAllGroups()
                        for g in groups {
                            let info = g as! NSDictionary
                            
                            let dataToSet = NSMutableDictionary()
                            copyData(info, sourceKey: "group_id", destinationDictionary: dataToSet, destinationKey: "groupId", methodName:#function)
                            copyData(info, sourceKey: "name", destinationDictionary: dataToSet, destinationKey: "name", methodName:#function)
                            copyData(info, sourceKey: "picture", destinationDictionary: dataToSet, destinationKey: "picture", methodName:#function)
                            copyData(info, sourceKey: "theme_color", destinationDictionary: dataToSet, destinationKey: "themeColor", methodName:#function)
                            copyData(info, sourceKey: "is_admin", destinationDictionary: dataToSet, destinationKey: "isAdmin", methodName:#function)
                            copyData((safeDouble(info.object(forKey: "muted_hour")) > 0.0) , destinationDictionary: dataToSet, destinationKey: "isMuted", methodName:#function)
                            copyData(safeBool(info.object(forKey: "loc_sharing")) , destinationDictionary: dataToSet, destinationKey: "locationSharingEnabled", methodName:#function)

                            if let members = info.object(forKey: "members") as? NSArray {
                                let membersArray = NSMutableArray()
                                for m in members {
                                    let info = m as! NSDictionary
                                    let dataToSet = NSMutableDictionary()
                                    if let userId = info.object(forKey: "user_id") as? String{
                                        Dbm().addUserId(userId)
                                    }                                    
                                    copyData(info, sourceKey: "user_id", destinationDictionary: dataToSet, destinationKey: "userId", methodName:#function)
                                    copyData(info, sourceKey: "name", destinationDictionary: dataToSet, destinationKey: "name", methodName:#function)
                                    copyData(info, sourceKey: "picture", destinationDictionary: dataToSet, destinationKey: "picture", methodName:#function)
                                    copyData(info, sourceKey: "last_known_data_time_stamp", destinationDictionary: dataToSet, destinationKey: "lastKnownTimeStamp", methodName:#function)
                                    copyData(info, sourceKey: "last_known_battery_status", destinationDictionary: dataToSet, destinationKey: "lastKnownBatteryStatus", methodName:#function)
                                    copyData(info, sourceKey: "last_known_address", destinationDictionary: dataToSet, destinationKey: "lastKnownLocationAddress", methodName:#function)
                                    copyData(info, sourceKey: "last_known_loc_lat", destinationDictionary: dataToSet, destinationKey: "lastKnownLocationLatitude", methodName:#function)
                                    copyData(info, sourceKey: "last_known_loc_long", destinationDictionary: dataToSet, destinationKey: "lastKnownLocationLongitude", methodName:#function)
                                    copyData(NSNumber(value: safeBool(info.object(forKey: "loc_sharing_enabled"))), destinationDictionary: dataToSet, destinationKey: "locationDisplay", methodName:#function)
                                    membersArray.add(dataToSet)
                                }
                                dataToSet.setObject(membersArray, forKey: "members" as NSCopying)
                            }
                            
                            if let associatedPlaces = info.object(forKey: "places") as? NSArray {
                                let associatedPlacesArray = NSMutableArray()
                                for ap in associatedPlaces {
                                    let info = ap as! NSDictionary
                                    let dataToSet = NSMutableDictionary()
                                    copyData(info, sourceKey: "place_id", destinationDictionary: dataToSet, destinationKey: "placeId", methodName:#function)
                                    copyData(info, sourceKey: "entry_sharing", destinationDictionary: dataToSet, destinationKey: "entrySharingEnabled", methodName:#function)
                                    copyData(info, sourceKey: "exit_sharing", destinationDictionary: dataToSet, destinationKey: "exitSharingEnabled", methodName:#function)
                                    copyData(info, sourceKey: "is_admin", destinationDictionary: dataToSet, destinationKey: "isAdmin", methodName:#function)
                                    associatedPlacesArray.add(dataToSet)
                                }
                                dataToSet.setObject(associatedPlacesArray, forKey: "associatedPlaces" as NSCopying)
                            }
                            
                            Dbm().addUpdateGroup(dataToSet)
                        }
                    }
                    scanAndDeleteChatDialogsWhoesPoMGroupsGotDeleted()
                }
                
                execMain({
                    Dbm().saveChanges()
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: NOTIFICATION_GROUPS_PLACES_UPDATED), object: nil)
                }, delay: 0.2)

                CacheManager.sharedInstance.saveObject(NSDate(), identifier: KEY_LAST_UPDATE_OF_SYNC_GROUPS_AND_PLACES)
                
                let information = NSMutableDictionary()
                let scm = ServerCommunicationManager()
                scm.responseErrorOption = ResponseErrorOption.dontShowErrorResponseMessage
                scm.returnFailureResponseAlso = true
                scm.getAllPlaces(information, completionBlock: {[weak self] (responseData) -> () in guard let `self` = self else { return }
                    if let _ = responseData {
                        if let places = responseData?.value(forKeyPath: "data.places") as? NSArray {
                            Dbm().removeAllPlaces()
                            for p in places {
                                let info = p as! NSDictionary
                                
                                let dataToSet = NSMutableDictionary()
                                copyData(info, sourceKey: "place_id", destinationDictionary: dataToSet, destinationKey: "placeId", methodName:#function)
                                copyData(info, sourceKey: "name", destinationDictionary: dataToSet, destinationKey: "name", methodName:#function)
                                copyData(info, sourceKey: "address", destinationDictionary: dataToSet, destinationKey: "placeAddress", methodName:#function)
                                copyData(info, sourceKey: "loc_lat", destinationDictionary: dataToSet, destinationKey: "placeLatitude", methodName:#function)
                                copyData(info, sourceKey: "loc_lon", destinationDictionary: dataToSet, destinationKey: "placeLongitude", methodName:#function)
                                
                                Dbm().addUpdatePlace(info: dataToSet)
                            }
                            execMain({
                                Dbm().saveChanges()
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NOTIFICATION_GROUPS_PLACES_UPDATED), object: nil)
                            }, delay: 0.2)
                        }
                    }
                    completion()
                })
            })
        }else{
            completion()
        }
    }
    
    func syncNotificationsFromServer(){
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(AppCommonFunctions.syncNotificationsFromServerPrivate), object: nil)
        self.perform(#selector(AppCommonFunctions.syncNotificationsFromServerPrivate), with: nil, afterDelay: 1)
    }
    
    func syncNotificationsFromServerPrivate(){
        syncNotifications {}
    }
    
    func syncNotifications(completion:@escaping ACFCompletionBlockType2){
        if(isInternetConnectivityAvailable(false)==false){completion();return}
        if isUserLoggedIn() {
            let settings = Dbm().getSetting()

            let information = NSMutableDictionary()
            let scm = ServerCommunicationManager()
            scm.responseErrorOption = ResponseErrorOption.dontShowErrorResponseMessage
            scm.returnFailureResponseAlso = true
            copyData(settings.notificationLastSyncTimeStamp, destinationDictionary: information, destinationKey: "notificationLastSyncTimeStamp", methodName: #function)
            scm.getNotifications(information, completionBlock: {[weak self] (responseData) -> () in guard let `self` = self else { return }
                if let _ = responseData {
                    if let notifications = responseData?.value(forKeyPath: "data.notification") as? NSArray {
                        for n in notifications {
                            let info = n as! NSDictionary
                            let dataToSet = NSMutableDictionary()
                            copyData(info, sourceKey: "notification_id", destinationDictionary: dataToSet, destinationKey: "notificationId", methodName:#function)
                            copyData(info, sourceKey: "title", destinationDictionary: dataToSet, destinationKey: "title", methodName:#function)
                            copyData(info, sourceKey: "message", destinationDictionary: dataToSet, destinationKey: "message", methodName:#function)
                            copyData(info, sourceKey: "timestamp", destinationDictionary: dataToSet, destinationKey: "timestamp", methodName:#function)
                            copyData(info, sourceKey: "notif_type", destinationDictionary: dataToSet, destinationKey: "notificationType", methodName:#function)
                            copyData(info, sourceKey: "place_id", destinationDictionary: dataToSet, destinationKey: "placeId", methodName:#function)
                            copyData(info, sourceKey: "group_id", destinationDictionary: dataToSet, destinationKey: "groupId", methodName:#function)
                            copyData(info, sourceKey: "user_id", destinationDictionary: dataToSet, destinationKey: "associatedUserId", methodName:#function)
                            copyData(info, sourceKey: "name", destinationDictionary: dataToSet, destinationKey: "associatedUserName", methodName:#function)
                            copyData(info, sourceKey: "picture", destinationDictionary: dataToSet, destinationKey: "associatedUserPicture", methodName:#function)
                            copyData(info, sourceKey: "address", destinationDictionary: dataToSet, destinationKey: "address", methodName:#function)
                            copyData(info, sourceKey: "address_name", destinationDictionary: dataToSet, destinationKey: "addressName", methodName:#function)
                            copyData(info, sourceKey: "address_lat", destinationDictionary: dataToSet, destinationKey: "addressLocationLatitude", methodName:#function)
                            copyData(info, sourceKey: "address_long", destinationDictionary: dataToSet, destinationKey: "addressLocationLongitude", methodName:#function)
                            copyData(info, sourceKey: "travel_mode", destinationDictionary: dataToSet, destinationKey: "travelMode", methodName:#function)
                            copyData(info, sourceKey: "travel_date", destinationDictionary: dataToSet, destinationKey: "travelDayDate", methodName:#function)
                            copyData(info, sourceKey: "travel_etd", destinationDictionary: dataToSet, destinationKey: "travelDayTime", methodName:#function)
                            Dbm().addNotification(dataToSet)
                        }
                        let settings = Dbm().getSetting()
                        settings.notificationLastSyncTimeStamp = safeString(currentTimeStamp())
                        Dbm().saveChanges()
                    }
                }
                NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_NEED_NOTIFICATIONS_UPDATE), object: nil)
                completion()
            })
        }else{
            completion()
        }
    }
    
    func updateUserProfileInformationFromServerIfRequired(){
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(AppCommonFunctions.updateUserProfileInformationFromServerIfRequiredPrivate), object: nil)
        self.perform(#selector(AppCommonFunctions.updateUserProfileInformationFromServerIfRequiredPrivate), with: nil, afterDelay: 2)
    }
    
    func updateUserProfileInformationFromServerIfRequiredPrivate(){
        if(isInternetConnectivityAvailable(false)==false){return}
        let userInfo = Dbm().getUserInfo()
        if isNotNull(userInfo){
            getUserProfileDetails(loggedInUserId(), completion: { (returnedData) -> () in
                if isNotNull(returnedData){
                    if let allDetails = returnedData as? NSMutableDictionary{
                        saveProfileInformation(allDetails)
                    }
                    let profileInfo = (returnedData as! NSDictionary).object(forKey: "profileInformation") as! NSDictionary
                    let settingsInfo = (returnedData as! NSDictionary).object(forKey: "informationForSettings") as! NSDictionary
                    if isNotNull(profileInfo.object(forKey: "statusText")){
                        execMain({[weak self]  in guard let `self` = self else { return }
                            Dbm().addUserStatus(["userId":loggedInUserId(),"status":profileInfo.object(forKey: "statusText")!])
                            },delay: 1.0)
                    }
                    Dbm().setUserInfo(profileInfo)
                    Dbm().setSetting(settingsInfo)
                    self.loginToQuickBlox()
                    self.updateQBUserDetails()
                    self.updateUserInfoOnCrashlytics()
                    NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_PROFILE_UPDATED), object: nil)
                }
            },useCache:false)
        }
    }
    
    func showInputTextController(_ completionBlock:@escaping TICompletionBlock,text:String?,limit:Int,editorTitle:String?,actionButtonTitle:String?){
        let gapX = 0 as CGFloat
        let gapY = 0 as CGFloat
        let textInputViewController = getViewController("TextInputViewController") as! TextInputViewController
        textInputViewController.limit = limit
        textInputViewController.text = text
        textInputViewController.editTitle = editorTitle
        textInputViewController.completion = completionBlock
        textInputViewController.actionButtonTitle = actionButtonTitle
        let navigationController = UINavigationController(rootViewController: textInputViewController)
        navigationController.view.frame = CGRect(x: gapX,y: gapY,width: DEVICE_WIDTH-2*gapX, height: DEVICE_HEIGHT-2*gapY)
        navigationController.view.layer.cornerRadius = POP_UP_VIEW_CORNER_RADIUS
        navigationController.view.layer.masksToBounds = true
        navigationController.view.backgroundColor = UIColor.clear
        self.navigationController!.presentPopupViewController(navigationController, animationType: .fade, completion: { () -> Void in
        })
        self.navigationController?.popupBackgroundView?.backgroundColor = UIColor.clear
    }
    
    func reset(){
        Acf().carpoolHomeScreenController = nil
        Acf().pOMHomeScreenController = nil
        trackFunctionalEvent(FE_SIGN_OUT, information: nil)
        NSObject.cancelPreviousPerformRequests(withTarget: Acf().rideHomeVC)
        NSObject.cancelPreviousPerformRequests(withTarget: Acf().rideDetailVC)
        Acf().logoutFromQuickBlox()
        Dbm().resetDatabase()
        Acf().showRequiredScreen()
    }
    
    func getShortUserProfileDetails(_ userId:String,completion:@escaping ACFCompletionBlock,useCache:Bool=true){
        let cachedData = CacheManager.sharedInstance.loadObject("ShortProfileInfoCache\(userId)")
        if useCache && isNotNull(cachedData) {
            completion(cachedData)
        }
        let information = NSMutableDictionary()
        copyData(userId , destinationDictionary: information, destinationKey: "userId", methodName: #function)
        let scm = ServerCommunicationManager()
        scm.responseErrorOption = ResponseErrorOption.dontShowErrorResponseMessage
        scm.getShortProfile(information, completionBlock: {[weak self] (responseData) -> () in guard let `self` = self else { return }
            if let _ = responseData {
                CacheManager.sharedInstance.saveObject(responseData, identifier: "ShortProfileInfoCache\(userId)")
                completion(responseData)
            }
        })
    }
    
    func getUserProfileDetails(_ userId:String,completion:@escaping ACFCompletionBlock,useCache:Bool=true){
        let cachedData = CacheManager.sharedInstance.loadObject("ProfileInfoCache\(userId)")
        if useCache && isNotNull(cachedData) {
            completion(cachedData)
        }
        let information = NSMutableDictionary()
        copyData(userId , destinationDictionary: information, destinationKey: "userId", methodName: #function)
        let scm = ServerCommunicationManager()
        scm.responseErrorOption = ResponseErrorOption.dontShowErrorResponseMessage
        scm.getUserProfile(information, completionBlock: {[weak self] (responseData) -> () in guard let `self` = self else { return }
            if let _ = responseData {
                
                trackFunctionalEvent(FE_SCREEN_DATA_LOADED, information: ["screen":"profile_screen"])
                
                func getData(_ object:Any?)->NSDictionary{
                    if isNotNull(object)&&object! is NSArray && (object as! NSArray).count > 0 {
                        return  ((object as! NSArray)[0] as! NSDictionary).removeNullValues()
                    }else if isNotNull(object) &&  object! is NSArray{
                        return NSDictionary()
                    }else if isNotNull(object) &&  object! is NSDictionary{
                        return (object as! NSDictionary).removeNullValues()
                    }else if isNull(object){
                        return NSDictionary()
                    }
                    return NSDictionary()
                }
                
                
                var personalDetails = getData((responseData?.object(forKey: "data")! as! NSDictionary).object(forKey: "personal")).removeNullValues()
                var temporaryCompanyDetails = getData((responseData?.object(forKey: "data")! as! NSDictionary).object(forKey: "tempcompany")).removeNullValues()
                var userDetailsOthers = getData((responseData?.object(forKey: "data")! as! NSDictionary).object(forKey: "userDetails")).removeNullValues()
                var userETDDetails = getData((responseData?.object(forKey: "data")! as! NSDictionary).object(forKey: "userEtdDetails")).removeNullValues()
                var userAddressDetails = getData((responseData?.object(forKey: "data")! as! NSDictionary).object(forKey: "useraddress")).removeNullValues()
                var verificationStatus = getData((responseData?.object(forKey: "data")! as! NSDictionary).object(forKey: "verification_status")).removeNullValues()
                var affinity = getData((responseData?.object(forKey: "data")! as! NSDictionary).object(forKey: "affinity")).removeNullValues()
                var drnStatus = getData((responseData?.object(forKey: "data")! as! NSDictionary).object(forKey: "drn_status")).removeNullValues()
                
                /*
                 *
                 *
                 *Step 2 : Start : Fill all details required by userInfo
                 *
                 */
                
                let informationForUserInfo = NSMutableDictionary()
                
                
                //1
                if let emailId = personalDetails["Email"] as? String {
                    copyData(emailId, destinationDictionary: informationForUserInfo, destinationKey: "email", methodName: #function)
                }else{
                    copyData("" , destinationDictionary: informationForUserInfo, destinationKey: "email", methodName: #function)
                }
                
                //1
                
                //2
                if let kilometersShared = userDetailsOthers["kms_shared"] as? String {
                    if isNotNull(kilometersShared){
                        let componentsSeperatedBySpaces = kilometersShared.components(separatedBy: " ")
                        if componentsSeperatedBySpaces.count > 0{
                            copyData((componentsSeperatedBySpaces[0]).toInt(), destinationDictionary: informationForUserInfo, destinationKey: "kmsShared", methodName: #function)
                        }else{
                            copyData(0 , destinationDictionary: informationForUserInfo, destinationKey: "kmsShared", methodName: #function)
                        }
                    }else{
                        copyData(0 , destinationDictionary: informationForUserInfo, destinationKey: "kmsShared", methodName: #function)
                    }
                }else{
                    copyData(0 , destinationDictionary: informationForUserInfo, destinationKey: "kmsShared", methodName: #function)
                }
                //2
                
                //3
                if let userName = userDetailsOthers["user_name"] as? String {
                    copyData(userName, destinationDictionary: informationForUserInfo, destinationKey: "name", methodName: #function)
                }else{
                    copyData("" , destinationDictionary: informationForUserInfo, destinationKey: "name", methodName: #function)
                }
                //3
                
                //4
                if let mobileNumber = personalDetails["mobile"] as? String {
                    copyData(mobileNumber, destinationDictionary: informationForUserInfo, destinationKey: "phone", methodName: #function)
                }else{
                    copyData("" , destinationDictionary: informationForUserInfo, destinationKey: "phone", methodName: #function)
                }
                //4
                
                //5
                if let profilePictureUrl = userDetailsOthers["Photo"] as? String {
                    copyData(profilePictureUrl, destinationDictionary: informationForUserInfo, destinationKey: "picture", methodName: #function)
                }else{
                    copyData("" , destinationDictionary: informationForUserInfo, destinationKey: "picture", methodName: #function)
                }
                //5
                
                //6
                if let userRating = userDetailsOthers["Rating"] as? String {
                    copyData(userRating.toFloat(), destinationDictionary: informationForUserInfo, destinationKey: "rating", methodName: #function)
                }else{
                    copyData(0.0 , destinationDictionary: informationForUserInfo, destinationKey: "rating", methodName: #function)
                }
                //6
                
                //7
                if let ridesDone = userDetailsOthers["Rides"] as? String {
                    copyData(ridesDone, destinationDictionary: informationForUserInfo, destinationKey: "ridesDone", methodName: #function)
                }else{
                    copyData("0" , destinationDictionary: informationForUserInfo, destinationKey: "ridesDone", methodName: #function)
                }
                //7
                
                //8
                if let directionalRank = userDetailsOthers["direction_rank"] as? String {
                    copyData(directionalRank, destinationDictionary: informationForUserInfo, destinationKey: "rank", methodName: #function)
                }else{
                    copyData("" , destinationDictionary: informationForUserInfo, destinationKey: "rank", methodName: #function)
                }
                //8
                
                //9
                if let destinationName = userDetailsOthers["Company_Name"] as? String {
                    copyData(destinationName, destinationDictionary: informationForUserInfo, destinationKey: "destinationName", methodName: #function)
                }else{
                    copyData("" , destinationDictionary: informationForUserInfo, destinationKey: "destinationName", methodName: #function)
                }
                //9
                
                //10
                if let gender = userDetailsOthers["Gender"] as? NSString {
                    if gender.isEqual(to: "M"){
                        copyData(NSNumber(value: true as Bool), destinationDictionary: informationForUserInfo, destinationKey: "genderTforMaleFForFemale", methodName: #function)
                    }else{
                        copyData(NSNumber(value: false), destinationDictionary: informationForUserInfo, destinationKey: "genderTforMaleFForFemale", methodName: #function)
                    }
                }else{
                    copyData(NSNumber(value: true as Bool), destinationDictionary: informationForUserInfo, destinationKey: "genderTforMaleFForFemale", methodName: #function)
                }
                //10
                
                //11
                if let statusText = personalDetails["status_text"] as? String {
                    copyData(statusText, destinationDictionary: informationForUserInfo, destinationKey: "statusText", methodName: #function)
                }else{
                    copyData("" , destinationDictionary: informationForUserInfo, destinationKey: "statusText", methodName: #function)
                }
                //11
                
                //12
                if let etdD = userETDDetails as? NSArray {
                    if etdD.count == 2 {
                        for etdo in etdD {
                            if let mode = ((etdo as! NSDictionary)["mode"] as? NSString){
                                if mode.isEqual(to: "0") {
                                    if let etd0 = (etdo as! NSDictionary)["etd"] as? String{
                                        copyData(etd0, destinationDictionary: informationForUserInfo, destinationKey: "etd_mode_0", methodName: #function)
                                    }
                                }
                                if mode.isEqual(to: "1") {
                                    if let etd1 = (etdo as! NSDictionary)["etd"] as? String{
                                        copyData(etd1, destinationDictionary: informationForUserInfo, destinationKey: "etd_mode_1", methodName: #function)
                                    }
                                }
                            }
                        }
                    }
                }
                //12
                
                //13
                if let isBlocked = affinity["is_block"] {
                    if isNotNull(isBlocked){
                        informationForUserInfo.setObject("\(isBlocked)", forKey: "blockedByMe" as NSCopying)
                    }else{
                        informationForUserInfo.setObject("0", forKey: "blockedByMe" as NSCopying)
                    }
                }else{
                    informationForUserInfo.setObject("0", forKey: "blockedByMe" as NSCopying)
                }
                //13
                
                //14
                if let userIsPassenger = personalDetails["user_is_passenger"] as? String {
                    let isPassengerString = "\(userIsPassenger)" as NSString
                    copyData(isPassengerString.isEqual(to: "0") ? "1" : "0", destinationDictionary: informationForUserInfo, destinationKey: "isPassenger", methodName: #function)
                }else if let userIsPassenger = personalDetails["user_is_passenger"] as? Int {
                    let isPassengerString = "\(userIsPassenger)" as NSString
                    copyData(isPassengerString.isEqual(to: "0") ? "1" : "0", destinationDictionary: informationForUserInfo, destinationKey: "isPassenger", methodName: #function)
                }else{
                    copyData("1" , destinationDictionary: informationForUserInfo, destinationKey: "isPassenger", methodName: #function)
                }
                //14
                
                //15
                if let refferalCode = userDetailsOthers["refcode"] as? String {
                    informationForUserInfo.setObject(refferalCode, forKey: "refferalCode" as NSCopying)
                }else{
                    informationForUserInfo.setObject("RefferalCodePlaceholder", forKey: "refferalCode" as NSCopying)
                }
                //15
                
                
                /*
                 *
                 *
                 *Step 2 : Finish
                 *
                 */
                
                
                /*
                 *
                 *
                 *Step 3 : Start : Fill all details required by settings
                 *
                 */
                
                let informationForSettings = NSMutableDictionary()
                
                
                //1
                if let homeAddress = userDetailsOthers["Home_Address"] as? String {
                    copyData(homeAddress, destinationDictionary: informationForSettings, destinationKey: "homeAddress", methodName: #function)
                }else{
                    copyData("" , destinationDictionary: informationForSettings, destinationKey: "homeAddress", methodName: #function)
                }
                //1
                
                //2
                if let homeLocationLatitude = userAddressDetails["par_home_lat"] as? String {
                    copyData(homeLocationLatitude.toDouble(), destinationDictionary: informationForSettings, destinationKey: "homeLocationLatitude", methodName: #function)
                }else{
                    copyData(("0.0").toDouble() , destinationDictionary: informationForSettings, destinationKey: "homeLocationLatitude", methodName: #function)
                }
                //2
                
                //3
                if let homeLocationLongitude = userAddressDetails["par_home_lng"] as? String {
                    copyData(homeLocationLongitude.toDouble(), destinationDictionary: informationForSettings, destinationKey: "homeLocationLongitude", methodName: #function)
                }else{
                    copyData(("0.0").toDouble() , destinationDictionary: informationForSettings, destinationKey: "homeLocationLongitude", methodName: #function)
                }
                //3
                
                //4
                if let destinationAddress = userDetailsOthers["Office_Address"] as? String {
                    copyData(destinationAddress, destinationDictionary: informationForSettings, destinationKey: "destinationAddress", methodName: #function)
                }else{
                    copyData("" , destinationDictionary: informationForSettings, destinationKey: "destinationAddress", methodName: #function)
                }
                //4
                
                //5
                if let destinationLocationLatitude = userAddressDetails["par_ofc_lat"] as? String {
                    copyData(destinationLocationLatitude.toDouble(), destinationDictionary: informationForSettings, destinationKey: "destinationLocationLatitude", methodName: #function)
                }else{
                    copyData(("0.0").toDouble() , destinationDictionary: informationForSettings, destinationKey: "destinationLocationLatitude", methodName: #function)
                }
                //5
                
                //6
                if let destinationLocationLongitude = userAddressDetails["par_ofc_lng"] as? String {
                    copyData(destinationLocationLongitude.toDouble(), destinationDictionary: informationForSettings, destinationKey: "destinationLocationLongitude", methodName: #function)
                }else{
                    copyData(("0.0").toDouble() , destinationDictionary: informationForSettings, destinationKey: "destinationLocationLongitude", methodName: #function)
                }
                //6
                
                //7
                if let homePickup = userDetailsOthers["Home_Picup"] as? String {
                    copyData(homePickup, destinationDictionary: informationForSettings, destinationKey: "homeAddressPickUpPoint", methodName: #function)
                }else{
                    copyData("" , destinationDictionary: informationForSettings, destinationKey: "homeAddressPickUpPoint", methodName: #function)
                }
                //7
                
                //8
                if let destinationPickup = userDetailsOthers["Office_Pickup"] as? String {
                    copyData(destinationPickup, destinationDictionary: informationForSettings, destinationKey: "destinationAddressPickUpPoint", methodName: #function)
                }else{
                    copyData("" , destinationDictionary: informationForSettings, destinationKey: "destinationAddressPickUpPoint", methodName: #function)
                }
                //8
                
                //9
                if let documentStatus = verificationStatus["Document_status"] as? String {
                    copyData(documentStatus, destinationDictionary: informationForSettings, destinationKey: "verifiedGovernmentIdProofDocument", methodName: #function)
                }else{
                    if let documentStatus = userDetailsOthers["Document_Name_Status"] as? String {
                        copyData(documentStatus, destinationDictionary: informationForSettings, destinationKey: "verifiedGovernmentIdProofDocument", methodName: #function)
                    }else{
                        copyData("0" , destinationDictionary: informationForSettings, destinationKey: "verifiedGovernmentIdProofDocument", methodName: #function)
                    }
                }
                //9
                
                //10
                if let mobileVerificationStatus = verificationStatus["Sms_status"] as? String {
                    copyData(mobileVerificationStatus, destinationDictionary: informationForSettings, destinationKey: "verifiedMobileNumber", methodName: #function)
                }else{
                    if let mobileVerificationStatus = userDetailsOthers["SMS_Code_Status"] as? String {
                        copyData(mobileVerificationStatus, destinationDictionary: informationForSettings, destinationKey: "verifiedMobileNumber", methodName: #function)
                    }else{
                        copyData("0" , destinationDictionary: informationForSettings, destinationKey: "verifiedMobileNumber", methodName: #function)
                    }
                }
                //10
                
                //11
                if let emailVerificationStatus = verificationStatus["Email_status"] as? String {
                    copyData(emailVerificationStatus, destinationDictionary: informationForSettings, destinationKey: "verifiedUserDesignation", methodName: #function)
                }else{
                    if let emailVerificationStatus = userDetailsOthers["Corporate_Email_Status"] as? String {
                        copyData(emailVerificationStatus, destinationDictionary: informationForSettings, destinationKey: "verifiedUserDesignation", methodName: #function)
                    }else{
                        copyData("0" , destinationDictionary: informationForSettings, destinationKey: "verifiedUserDesignation", methodName: #function)
                    }
                }
                //11
                
                //12
                if let carRegistrationNo = personalDetails["user_car_num"] as? String {
                    copyData(carRegistrationNo, destinationDictionary: informationForSettings, destinationKey: "carRegistrationNo", methodName: #function)
                }else{
                    copyData("" , destinationDictionary: informationForSettings, destinationKey: "carRegistrationNo", methodName: #function)
                }
                //12
                
                //13
                if let carMakeName = personalDetails["car_make_name"] as? String {
                    copyData(carMakeName, destinationDictionary: informationForSettings, destinationKey: "carMake", methodName: #function)
                }else{
                    copyData("" , destinationDictionary: informationForSettings, destinationKey: "carMake", methodName: #function)
                }
                //13
                
                //14
                if let carModelName = personalDetails["car_model_name"] as? String {
                    copyData(carModelName, destinationDictionary: informationForSettings, destinationKey: "carModel", methodName: #function)
                }else{
                    copyData("" , destinationDictionary: informationForSettings, destinationKey: "carModel", methodName: #function)
                }
                //14
                
                //15
                if let userTravellingDistance = userAddressDetails["par_distance"] as? String {
                    copyData(userTravellingDistance, destinationDictionary: informationForSettings, destinationKey: "distance", methodName: #function)
                }else{
                    copyData("" , destinationDictionary: informationForSettings, destinationKey: "distance", methodName: #function)
                }
                //15
                
                //16
                copyData(NSNumber(value: false), destinationDictionary: informationForSettings, destinationKey: "intraCompany", methodName: #function)
                if let intraCompany = userDetailsOthers["Intra_Company_Only"] as? Bool {
                    copyData(NSNumber(value: intraCompany as Bool), destinationDictionary: informationForSettings, destinationKey: "intraCompany", methodName: #function)
                }
                //16
                
                //17
                if let userType = userDetailsOthers["user_type"] as? String {
                    copyData(userType, destinationDictionary: informationForSettings, destinationKey: "userType", methodName: #function)
                }
                //17
                
                //18
                if let companyId = personalDetails["comp_id"] as? String {
                    copyData("\(companyId)", destinationDictionary: informationForSettings, destinationKey: "companyId", methodName: #function)
                }
                else if let companyId = personalDetails["comp_id"] as? Int {
                    copyData("\(companyId)", destinationDictionary: informationForSettings, destinationKey: "companyId", methodName: #function)
                }
                //18
                
                //19
                copyData(safeBool(drnStatus["drn_status_send"], alternate: true), destinationDictionary: informationForSettings, destinationKey: "eprSettingOption1", methodName: #function)
                //19
                
                
                //20
                copyData(safeBool(drnStatus["drn_status_receive"], alternate: true), destinationDictionary: informationForSettings, destinationKey: "eprSettingOption2", methodName: #function)
                //20
                
                //21
                copyData(safeBool(personalDetails["show_phone"], alternate: false), destinationDictionary: informationForSettings, destinationKey: "showMyNumber", methodName: #function)
                //21
                /*
                 *
                 *
                 *Step 3 : Finish
                 *
                 */
                
                
                let completeInformation = NSMutableDictionary()
                copyData(informationForUserInfo, destinationDictionary: completeInformation, destinationKey: "profileInformation", methodName: #function)
                copyData(informationForSettings, destinationDictionary: completeInformation, destinationKey: "informationForSettings", methodName: #function)
                
                if let travelledWith = responseData?.value(forKeyPath: "data.travelled") as? NSArray {
                    copyData(travelledWith , destinationDictionary: completeInformation, destinationKey: "travelledWith", methodName: #function)
                }else{
                    copyData(NSMutableArray() , destinationDictionary: completeInformation, destinationKey: "travelledWith", methodName: #function)
                }
                
                CacheManager.sharedInstance.saveObject(completeInformation, identifier: "ProfileInfoCache\(userId)")
                completion(completeInformation)
                /*
                 *
                 *
                 * * * * * * * * * * * * *    BEST OF LUCK :):)   * * * * * * * * * * *
                 *
                 */
            }else{
                trackFunctionalEvent(FE_SCREEN_DATA_NOT_LOADED, information: ["screen":"profile_screen"])
                completion(nil)
            }
        })
    }
    
    
    func updateUserProfilePictureIfRequired(){
        let image = CacheManager.sharedInstance.loadObject(CACHED_IMAGE_TO_UPLOAD_LATER)
        if isNotNull(image) && isSystemReadyToProcessThis(){
            if(isInternetConnectivityAvailable(false)==false){return}
            execMain({[weak self] in guard let `self` = self else { return }
                
                let picname = "\(Dbm().getUserInfo()!.userId!)_\(currentTimeStamp()).jpg"
                let information = ["image":image!,"imageName":picname] as NSDictionary
                let scm = ServerCommunicationManager()
                scm.responseErrorOption = .dontShowErrorResponseMessage
                scm.returnFailureResponseAlso = true
                scm.returnFailureUnParsedDataIfParsingFails = true
                scm.uploadImage(information) { (responseData) -> () in
                    if isNotNull(responseData){
                        let information = ["imageName":picname] as NSDictionary
                        let scm = ServerCommunicationManager()
                        scm.returnFailureResponseAlso = true
                        scm.updateProfilePicture(information) { (responseData) -> () in
                            Acf().updateUserProfileInformationFromServerIfRequired()
                            CacheManager.sharedInstance.saveObject(nil, identifier: CACHED_IMAGE_TO_UPLOAD_LATER)
                            self.updateQBUserDetails()
                        }
                    }
                }
            })
        }
    }
    
    func updateCityCompanyCollegeCarOptionsList(){
        if isUpdatingCityCompanyCollegeCarOptionsList{return;}
        var forceUpdate = false
        if let lastSyncDate = CacheManager.sharedInstance.loadObject("lastSyncDateUpdateCityCompanyCollegeCarOptionsList") as? Date{
            forceUpdate = lastSyncDate.isEarlierThanDate(Date().dateBySubtractingDays(7))
        }
        //Alok singh
        //Work around : because in version 4.2.0 , our server side implementation is not ready to support list of colleges , here i am avoiding the dependency of college list in logic.
        //I must add this later.
        //12 Sep 2016
        //&& Dbm().getAllColleges().count > 0
        if Dbm().getAllCompanies().count > 0 && Dbm().getAllCarModel().count > 0 && Dbm().getAllCarMake().count > 0 /* &&
             Dbm().getAllColleges().count > 0 */ && forceUpdate == false {
        }else{
            if(isInternetConnectivityAvailable(false)==false){return}
            let information = NSMutableDictionary()
            if let lastSyncDate = CacheManager.sharedInstance.loadObject("lastSyncDateUpdateCityCompanyCollegeCarOptionsList") as? Date{
                copyData("\(Int64(lastSyncDate.timeIntervalSince1970))" , destinationDictionary: information, destinationKey: "lastSyncDate", methodName: #function)
            }
            isUpdatingCityCompanyCollegeCarOptionsList = true
            let scm = ServerCommunicationManager()
            scm.responseErrorOption = ResponseErrorOption.dontShowErrorResponseMessage
            scm.returnFailureResponseAlso = true
            scm.getOptionsList(information, completionBlock: {[weak self] (responseData) -> () in guard let `self` = self else { return }
                execMain({[weak self]  in guard let `self` = self else { return }
                    showActivityIndicator("syncing")
                    execMain({[weak self]  in guard let `self` = self else { return }
                        if let _ = responseData {
                            if let _ = responseData?.object(forKey: "makelist") as? NSArray{
                                Dbm().deleteCompanyCitiesCarMakeCarModel()
                                if let carMakeList = responseData?.object(forKey: "makelist") as? NSArray{
                                    for carMake in carMakeList{
                                        Dbm().addCarMake(carMake as! NSDictionary)
                                    }
                                }
                                if let carModelList = responseData?.object(forKey: "modellist") as? NSArray {
                                    for carModel in carModelList{
                                        Dbm().addCarModel(carModel as! NSDictionary)
                                    }
                                }
                                if let destinationList = responseData?.object(forKey: "companylist") as? NSArray{
                                    for company in destinationList{
                                        Dbm().addCompany(company as! NSDictionary)
                                    }
                                }
                                if let collegeList = responseData?.object(forKey: "collegelist") as? NSArray{
                                    for college in collegeList{
                                        Dbm().addCollege(college as! NSDictionary)
                                    }
                                }
                                if let cityList = responseData?.object(forKey: "citylist") as? NSArray{
                                    for city in cityList{
                                        Dbm().addCity(city as! NSDictionary)
                                    }
                                }
                                CacheManager.sharedInstance.saveObject(Date() , identifier:"lastSyncDateUpdateCityCompanyCollegeCarOptionsList")
                            }else{
                                if let _ = responseData?.object(forKey: "updated") as? NSString {
                                    CacheManager.sharedInstance.saveObject(Date() , identifier:"lastSyncDateUpdateCityCompanyCollegeCarOptionsList")
                                }
                            }
                        }
                        if Dbm().getAllCompanies().count > 0 && Dbm().getAllCarModel().count > 0 && Dbm().getAllCarMake().count > 0 {
                            showNotification("syncing completed 👍🏻", showOnNavigation: false, showAsError: false, duration: 1)
                        }
                        hideActivityIndicator()
                        self.isUpdatingCityCompanyCollegeCarOptionsList = false
                        },delay:0.5)
                    },delay: 1)
            })
        }
    }
    
    func getFavouritesUsers(_ completion:ACFCompletionBlock?){
        if isUserLoggedIn() {
            completion?(Dbm().getAllFavourites())
            if(isInternetConnectivityAvailable(false)==false){return}
            let information = NSDictionary()
            let scm = ServerCommunicationManager()
            scm.responseErrorOption = ResponseErrorOption.dontShowErrorResponseMessage
            scm.getFavourites(information, completionBlock: {[weak self] (responseData) -> () in guard let `self` = self else { return }
                if let _ = responseData {
                    Dbm().removeAllFavourites()
                    let favouritesArray = responseData?.value(forKeyPath: "data.LIST") as! NSArray
                    for user in favouritesArray {
                        Dbm().addFavourites(user as! NSDictionary)
                    }
                    completion?(Dbm().getAllFavourites())
                    NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_FAVOURITES_UPDATED), object: nil)
                }
            })
        }
    }
    
    func colorForUserAsPerTravelRequestStatus(_ user:NSDictionary)->UIColor{
        let situation = Acf().getTravelRequestStatus(user)
        if situation == .notInitialisedYet {
            return UIColor.clear
        }else if situation == .travelRequestSent {
            return  APP_THEME_YELLOW_COLOR
        }else if situation == .travelRequestReceived {
            return  APP_THEME_YELLOW_COLOR
        }else if situation == .travelRequestRejected {
            return  APP_THEME_RED_COLOR
        }else if situation == .travelRequestRejectedByMe {
            return UIColor.clear
        }else if situation == .travelRequestAccepted {
            return  APP_THEME_GREEN_COLOR
        }else if situation == .travelCancelled {
            return  APP_THEME_RED_COLOR
        }else if situation == .travelCompleted {
            return  APP_THEME_VOILET_COLOR
        }else{
            return UIColor.clear
        }
    }
    
    func colorForRideStatus(_ user:NSDictionary)->UIColor{
        let situation = Acf().getTravelRequestStatus(user)
        if situation == .notInitialisedYet {
            return APP_THEME_VOILET_COLOR
        }else if situation == .travelRequestSent {
            return  APP_THEME_GRAY_COLOR
        }else if situation == .travelRequestReceived {
            return  APP_THEME_VOILET_COLOR
        }else if situation == .travelRequestRejected {
            return  APP_THEME_GRAY_COLOR
        }else if situation == .travelRequestRejectedByMe {
            return APP_THEME_VOILET_COLOR
        }else if situation == .travelRequestAccepted {
            return  APP_THEME_GRAY_COLOR
        }else if situation == .travelCancelled {
            return  APP_THEME_GRAY_COLOR
        }else if situation == .travelCompleted {
            return  APP_THEME_VOILET_COLOR
        }else{
            return UIColor.clear
        }
    }
    
    
    func getRideStatus(_ users:NSMutableArray?,dateOfRide:Date)->RideStatus{
        var invitationSent = false
        var invitationReceived = false
        var confirmed = false
        var completed = false
        if let _ = users {
            for user in users! {
                let situation = Acf().getTravelRequestStatus(user as! NSDictionary)
                if situation == .travelRequestSent{
                    invitationSent = true
                }else if situation == .travelRequestReceived{
                    invitationReceived = true
                }else if situation == .travelRequestAccepted{
                    confirmed = true
                }else if situation == .travelCompleted{
                    completed = true
                }
            }
        }
        if completed && !confirmed {
            return RideStatus.completed
        }
        if confirmed {
            if dateOfRide.isEarlierThanDate(Date().dateBySubtractingHours(6)){
                return RideStatus.confirmedButDelayed
            }
            return RideStatus.confirmed
        }
        if invitationSent {
            return RideStatus.invitationSent
        }
        if invitationReceived {
            return RideStatus.invitationReceived
        }
        return RideStatus.none
    }
    
    func decideAndSetUserRideStatus(_ user:NSDictionary,statusLabel:UILabel){
        let situation = Acf().getTravelRequestStatus(user)
        if situation == .notInitialisedYet {
            statusLabel.text = ""
        }else if situation == .travelRequestSent {
            statusLabel.text  = "Awaiting Confirmation"
        }else if situation == .travelRequestReceived {
            statusLabel.text  = "Invitation Received"
        }else if situation == .travelRequestRejected {
            statusLabel.text  = "Sorry can't ride today"
        }else if situation == .travelRequestRejectedByMe {
            statusLabel.text  = ""
        }else if situation == .travelRequestAccepted {
            statusLabel.text  = "Ride Confirmed"
        }else if situation == .travelCancelled {
            if isUserRidingWithOther(user){
                statusLabel.text  = "Already Planned"
            }else{
                statusLabel.text  = "Sorry can't ride today"
            }
        }else if situation == .travelCompleted {
            statusLabel.text  = "Ride Completed"
        }else{
            statusLabel.text = ""
        }
        statusLabel.textColor = colorForRideStatus(user)
    }
    
    func decideAndSetUserMiniRideStatus(_ user:NSDictionary,button:UIButton){
        let situation = Acf().getTravelRequestStatus(user)
        if situation == .notInitialisedYet {
            button.setTitle("", for: UIControlState.normal)
        }else if situation == .travelRequestSent {
            button.setTitle("Awaiting", for: UIControlState.normal)
        }else if situation == .travelRequestReceived {
            button.setTitle("Received", for: UIControlState.normal)
        }else if situation == .travelRequestRejected {
            button.setTitle("Can't ride", for: UIControlState.normal)
        }else if situation == .travelRequestRejectedByMe {
            button.setTitle("", for: UIControlState.normal)
        }else if situation == .travelRequestAccepted {
            button.setTitle("Confirmed", for: UIControlState.normal)
        }else if situation == .travelCancelled {
            if isUserRidingWithOther(user){
                button.setTitle("Already Planned", for: UIControlState.normal)
            }else{
                button.setTitle("Can't ride", for: UIControlState.normal)
            }
        }else if situation == .travelCompleted {
            button.setTitle("Completed", for: UIControlState.normal)
        }else{
            button.setTitle("", for: UIControlState.normal)
        }
    }
    
    func decideAndSetUserRideStatusForWeeklyPlannerCell(_ user:NSDictionary,button:TKTransitionSubmitButton,colorStatusView:UIView){
        var font = UIFont.init(name: FONT_SEMI_BOLD, size: 13)
        var title = ""
        var enabled = false
        var textColor = APP_THEME_VOILET_COLOR
        var statusColor = UIColor.clear
        var backGroundColor = UIColor.white.withAlphaComponent(0.2)
        let situation = Acf().getTravelRequestStatus(user)
        if situation == .notInitialisedYet || situation == .travelRequestRejected || situation == .travelRequestRejectedByMe || situation == .travelCancelled {
            font = UIFont.init(name: FONT_BOLD, size: 16)
            title = "Fix My Ride"
            textColor = APP_THEME_VOILET_COLOR
            statusColor = APP_THEME_LIGHT_GRAY_COLOR.withAlphaComponent(0.2)
            backGroundColor = UIColor.clear
            enabled = true
        }else if situation == .travelRequestSent {
            font = UIFont.init(name: FONT_SEMI_BOLD, size: 13)
            title = "Awaiting"
            textColor = UIColor.white
            statusColor = APP_THEME_YELLOW_COLOR
            enabled = false
        }else if situation == .travelRequestReceived {
            font = UIFont.init(name: FONT_SEMI_BOLD, size: 13)
            title = "Received"
            textColor = UIColor.white
            statusColor = APP_THEME_YELLOW_COLOR
            enabled = false
        }else if situation == .travelRequestAccepted {
            font = UIFont.init(name: FONT_SEMI_BOLD, size: 13)
            title = "Fixed"
            textColor = UIColor.white
            statusColor = APP_THEME_GREEN_COLOR
            enabled = false
        }else if situation == .travelCompleted {
            font = UIFont.init(name: FONT_SEMI_BOLD, size: 13)
            title = "Completed"
            textColor = UIColor.white
            statusColor = APP_THEME_VOILET_COLOR
            enabled = false
        }
        
        if Acf().isAnyUnreadNotifications(user) {
            backGroundColor = APP_THEME_VOILET_COLOR
        }
        
        colorStatusView.backgroundColor = statusColor
        button.setTitle(title, for: UIControlState.normal)
        button.setTitleColor(textColor, for: UIControlState.normal)
        button.setBackgroundColor(backGroundColor, forState: UIControlState.normal)
        button.setBackgroundColor(backGroundColor, forState: .highlighted)
        button.setBackgroundColor(backGroundColor, forState: .disabled)
        button.isEnabled = enabled
        button.titleLabel!.font = font
        button.setNeedsDisplay()
        button.layoutIfNeeded()
    }
    
    
    func decideAndSetUserInformationBanner(_ users:NSMutableArray,bannerLabel:UILabel){
        var invitationReceived = false
        var confirmed = false
        var isRideCompleted = false
        var messageToShow = "To invite people you want to ride with , SWIPE RIGHT"
        for user in users {
            let travelRequestStatus = getTravelRequestStatus(user as! NSDictionary)
            if travelRequestStatus == .travelCompleted {
                isRideCompleted = true
            }else if travelRequestStatus == .travelRequestAccepted {
                confirmed = true
            }else if travelRequestStatus == .travelRequestReceived {
                invitationReceived = true
            }
        }
        if confirmed {
            messageToShow = "To settle the ride , SWIPE RIGHT"
        }else if isRideCompleted {
            messageToShow = "To give a user feedback , SWIPE RIGHT"
        }else if invitationReceived {
            messageToShow = "To accept ride invitation , SWIPE RIGHT"
        }
        bannerLabel.text = messageToShow
    }
    
    func getTravelRequestStatus(_ user:NSDictionary)->TravelRequestStatus{
        let status = getTravelRequestStatusForOtherCases(user)
        if status == TravelRequestStatus.travelCancelled {
            return status
        }
        if isNotNull(user.object(forKey: "rs_status")){
            let rs_status = user.object(forKey: "rs_status") as! NSString
            if rs_status.isEqual(to: "0"){
                return TravelRequestStatus.travelRequestSent
            }
            else if rs_status.isEqual(to: "1"){
                return TravelRequestStatus.travelRequestReceived
            }
            else if rs_status.isEqual(to: "2"){
                return TravelRequestStatus.travelRequestRejected
            }
            else if rs_status.isEqual(to: "3"){
                return TravelRequestStatus.travelRequestAccepted
            }
            else if rs_status.isEqual(to: "4"){
                return TravelRequestStatus.travelRequestRejectedByMe
            }
            else if rs_status.isEqual(to: "5"){
                return TravelRequestStatus.travelCancelled
            }
            else if rs_status.isEqual(to: "6"){
                return TravelRequestStatus.travelCompleted
            }
        }
        return status
    }
    
    func getOfferStatus(_ offer:NSDictionary)->OfferStatus{
        if let offerDetails = offer.object(forKey: "details") as? NSDictionary{
            if isNotNull(offerDetails.object(forKey: "status")){
                let status = "\(offerDetails.object(forKey: "status")!)" as NSString
                if status.isEqual(to: "0"){
                    return OfferStatus.notAvailable
                }
                else if status.isEqual(to: "1"){
                    return OfferStatus.available
                }
                else if status.isEqual(to: "2"){
                    return OfferStatus.expired
                }
                else if status.isEqual(to: "3"){
                    return OfferStatus.notApplicable
                }
            }
        }
        return OfferStatus.notAvailable
    }
    
    func getTravelRequestStatusForOtherCases(_ user:NSDictionary)->TravelRequestStatus{
        if isUserRidingWithOther(user){
            return TravelRequestStatus.travelCancelled
        }
        return TravelRequestStatus.notInitialisedYet
    }
    
    func isUserRidingWithOther(_ user:NSDictionary)->Bool{
        if let travelLock = user.object(forKey: "travel_lock") as? String{
            if travelLock.toInt() == 1{
                func isMe(_ passenger:String) -> Bool{
                    if let value = user.object(forKey: passenger) as? String{
                        if loggedInUserId() == value {
                            // riding with me
                            return true
                        }
                    }
                    // not riding with me
                    return false
                }
                if isMe("passenger_one"){return false;}else{
                    if isMe("passenger_two"){return false;}else{
                        if isMe("passenger_three"){return false;}else{
                            if isMe("passenger_four"){return false;}else{
                                if let _ = user.object(forKey: "passenger_one") as? String{
                                    return true
                                }
                            }
                        }
                    }
                }
            }
        }
        return false
    }
    
    func getLeftSwipeOption(_ user:NSDictionary)->TravelRequestAvailableAction{
        let status = getTravelRequestStatusForOtherCases(user)
        if status == TravelRequestStatus.travelCancelled {
            return TravelRequestAvailableAction.none
        }
        if isNotNull(user.object(forKey: "rs_status")){
            let rs_status = user.object(forKey: "rs_status") as! NSString
            if rs_status.isEqual(to: "0"){
                return TravelRequestAvailableAction.none
            }
            else if rs_status.isEqual(to: "1"){
                return TravelRequestAvailableAction.reject
            }
            else if rs_status.isEqual(to: "2"){
                return TravelRequestAvailableAction.none
            }
            else if rs_status.isEqual(to: "3"){
                return TravelRequestAvailableAction.cancel
            }
            else if rs_status.isEqual(to: "4"){
                return TravelRequestAvailableAction.none
            }
            else if rs_status.isEqual(to: "5"){
                return TravelRequestAvailableAction.none
            }
            else if rs_status.isEqual(to: "6"){
                return TravelRequestAvailableAction.none
            }
        }
        return TravelRequestAvailableAction.none
    }
    
    func getRightSwipeOption(_ user:NSDictionary)->TravelRequestAvailableAction{
        let status = getTravelRequestStatusForOtherCases(user)
        if status == TravelRequestStatus.travelCancelled {
            return TravelRequestAvailableAction.none
        }
        if isNotNull(user.object(forKey: "rs_status")){
            let rs_status = user.object(forKey: "rs_status") as! NSString
            if rs_status.isEqual(to: "0"){
                return TravelRequestAvailableAction.none
            }
            else if rs_status.isEqual(to: "1"){
                return TravelRequestAvailableAction.accept
            }
            else if rs_status.isEqual(to: "2"){
                return TravelRequestAvailableAction.none
            }
            else if rs_status.isEqual(to: "3"){
                return TravelRequestAvailableAction.settle
            }
            else if rs_status.isEqual(to: "4"){
                return TravelRequestAvailableAction.invite
            }
            else if rs_status.isEqual(to: "5"){
                return TravelRequestAvailableAction.none
            }
            else if rs_status.isEqual(to: "6"){
                if isAlreadyProvidedFeedback(user){
                    return TravelRequestAvailableAction.none
                }else{
                    return TravelRequestAvailableAction.feedback
                }
            }
        }
        return TravelRequestAvailableAction.invite
    }
    
    
    func askForPermissionsAndUpdateUserLocationOnMap(_ mapView:MKMapView){
        UserTrackingManager.sharedInstance.askForRequiredPermissionsForTracking()
    }
    
    func updateMapRegion(_ location:CLLocationCoordinate2D ,mapView:MKMapView){
        let latDelta:CLLocationDegrees = 0.02
        let lonDelta:CLLocationDegrees = 0.02
        let span:MKCoordinateSpan = MKCoordinateSpanMake(latDelta, lonDelta)
        let region:MKCoordinateRegion = MKCoordinateRegionMake(location, span)
        mapView.setRegion(region, animated: false)
    }
    
    func getUserIdFor(_ userInfo:NSDictionary?)->String{
        return getUserId(userInfo)
    }
    
    func getUserIdFrom(_ login:String?)->String{
        if isNotNull(login){
            return login!.replacingOccurrences(of: QB_USER_NAME_PREFIX, with: "")
        }else{
            return "0"
        }
    }
    
    func someMessageReceived(_ notification:Foundation.Notification?){
        if let message = notification?.object as? QBChatMessage {
            if let messageType = message.customParameters!.object(forKey: MESSAGE_TYPE_KEY) as? String {
                if messageType == GroupEvents.location {
                    message.isRead = true
                    let information = message.customParameters
                    if let _ = information {
                        if let _ = information!.object(forKey: "id") as? NSString {
                            if (information!.object(forKey: "id") as! NSString).isEqual(to: loggedInUserId() as String){
                                
                            }else{
                                logMessage("***********LOCATION INFORMATION RECEIVED*************\n\n")
                                logMessage(information!)
                                self.locationInformationReceived(information!)
                            }
                        }else{
                            logMessage("***********INVALID LOCATION INFORMATION RECEIVED*************\n\(information)\n\n")
                        }
                    }else{
                        logMessage("***********INVALID LOCATION INFORMATION RECEIVED*************\n\(information)\n\n")
                    }
                }else if isQBReadyForItsUserDependentServices() && messageType == GroupEvents.rideGroupEvent && message.senderID != ServicesManager.instance().currentUser()!.id {
                    if let dateSent = message.dateSent as Date? {
                        if dateSent.isLaterThanDate(Date().dateBySubtractingMinutes(10)){
                            handleUpdateEventMessage(message.text,title: "Ride Update")
                        }
                    }
                }
            }
        }
    }
    
    func handleUpdateEventMessage(_ message:String?,title:String){
        if let messageToShow = message as String? {
            LocalNotificationHelper.sharedInstance().scheduleNotification(title: "", message: messageToShow, date: Date().dateByAddingSeconds(5) as NSDate, userInfo: nil)
            showInformationBannerOnBottom(Acf().navigationController!, style: .blurLight, duration: 7, title: title, message: messageToShow)
            Acf().playSoundAndVibrateForNewMessageIfNeeded()
            execMain({[weak self]  in guard let `self` = self else { return }
                NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_RIDE_EVENT_UPDATED), object: nil)
                },delay: 3.0)
        }
    }
    
    func locationInformationReceived(_ info:NSDictionary){
        locationCache.setObject(info,forKey:info.object(forKey: "id") as! String as NSCopying)
    }
    
    func openRideHomePage(_ dayDate:Date,timeDate:Date,mode:String,hidePopup:Bool=true){
        let userInfo = Dbm().getUserInfo()
        if isNotNull(userInfo){
            if isNotNull(rideHomeVC){
                if hidePopup{
                    hidePopupViewController()
                }
                sideMenuController?.hideViewController()
                carpoolHomeScreenController?.selectedIndex = 0
                rideHomeVC?.loadUsing(dayDate,timeDate:timeDate, mode: mode)
            }
        }
    }
    
    func showDeveloperOptionsViewController(){
        func showOptions(){
            let gapX = getRequiredPopupSideGap(#function)
            let gapY = getRequiredPopupVerticleGap(#function)
            let viewController = getViewController("DeveloperOptionsViewController") as! DeveloperOptionsViewController
            let navigationController = UINavigationController(rootViewController: viewController)
            navigationController.view.frame = CGRect(x: gapX,y: gapY,width: DEVICE_WIDTH-2*gapX, height: DEVICE_HEIGHT-2*gapY)
            navigationController.view.layer.cornerRadius = POP_UP_VIEW_CORNER_RADIUS
            navigationController.view.layer.masksToBounds = true
            self.navigationController!.presentPopupViewController(navigationController, animationType: .fade, completion: { () -> Void in
            })
        }
        if ENABLE_DEVELOPER_OPTION_WITH_OUT_PASSWORD {
            showOptions()
        }else{
            let prompt = UIAlertController(title: "Enter Pin", message: MESSAGE_TEXT___ENTER_PIN_DEVELOPER_OPTIONS_INFO, preferredStyle: UIAlertControllerStyle.alert)
            prompt.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
            prompt.addAction(UIAlertAction(title: "Verify", style: UIAlertActionStyle.cancel, handler: { (action) -> Void in
                let identifier = "Access Pin"
                let enteredText = (prompt.textFields![0] as UITextField).text
                if validateNumber(enteredText as NSString?, identifier: identifier as NSString?){
                    if enteredText! == DEVELOPER_OPTION_PIN {
                        showOptions()
                    }
                }
            }))
            prompt.addTextField(configurationHandler: {(textField: UITextField!) in
                textField.placeholder = "Pin"
                textField.keyboardType = UIKeyboardType.numberPad
            })
            Acf().navigationController!.present(prompt, animated: true, completion: nil)
        }
    }
    
    func setupDeveloperSecretOption(_ vc:UIView){
        let tapGestureRecognizer = UITapGestureRecognizer()
        tapGestureRecognizer.addTarget(self, action:  #selector(self.showDeveloperOptionsViewController))
        tapGestureRecognizer.numberOfTapsRequired = 2
        tapGestureRecognizer.numberOfTouchesRequired = 2
        vc.addGestureRecognizer(tapGestureRecognizer)
    }
    
    func updateTravelWithSettingsOnServer(){
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(AppCommonFunctions.updateTravelWithSettingsOnServerPrivate), object: nil)
        self.perform(#selector(AppCommonFunctions.updateTravelWithSettingsOnServerPrivate), with: nil, afterDelay: 5.0)
    }
    
    func updateTravelWithSettingsOnServerPrivate(){
        if (isInternetConnectivityAvailable(true)==false){return}
        let information = NSMutableDictionary()
        let settings = Dbm().getSetting()
        information.setObject(settings.travelOptionType1!.boolValue ? "true" : "false" , forKey: "groupFlag" as NSCopying)
        information.setObject(settings.travelOptionType2!.boolValue ? "true" : "false" , forKey: "companyFlag" as NSCopying)
        let scm = ServerCommunicationManager()
        scm.responseErrorOption = ResponseErrorOption.dontShowErrorResponseMessage
        scm.setMyTravelPreferences(information, completionBlock: {(responseData) -> () in
            NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_NEED_USER_UPDATE), object: nil)
            self.updateUserProfileInformationFromServerIfRequiredPrivate()
        })
    }
    
    func updateExpertRideAlertsSettingOnServer(){
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(AppCommonFunctions.updateExpertRideAlertsSettingOnServerPrivate), object: nil)
        self.perform(#selector(AppCommonFunctions.updateExpertRideAlertsSettingOnServerPrivate), with: nil, afterDelay: 5.0)
    }
    
    func updateExpertRideAlertsSettingOnServerPrivate(){
        if (isInternetConnectivityAvailable(true)==false){return}
        let information = NSMutableDictionary()
        let settings = Dbm().getSetting()
        information.setObject(settings.eprSettingOption1!.boolValue ? "true" : "false" , forKey: "eprSettingOption1" as NSCopying)
        information.setObject(settings.eprSettingOption2!.boolValue ? "true" : "false" , forKey: "eprSettingOption2" as NSCopying)
        let scm = ServerCommunicationManager()
        scm.responseErrorOption = ResponseErrorOption.dontShowErrorResponseMessage
        scm.setExpertRideAlertsPreferences(information, completionBlock: {(responseData) -> () in
        })
    }
    
    
    func updateShowMyNumberSettingOnServer(){
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(AppCommonFunctions.updateShowMyNumberSettingOnServerPrivate), object: nil)
        self.perform(#selector(AppCommonFunctions.updateShowMyNumberSettingOnServerPrivate), with: nil, afterDelay: 5.0)
    }
    
    func updateShowMyNumberSettingOnServerPrivate(){
        if (isInternetConnectivityAvailable(true)==false){return}
        let information = NSMutableDictionary()
        let settings = Dbm().getSetting()
        information.setObject(settings.showMyNumber!.boolValue ? "true" : "false" , forKey: "showMyNumber" as NSCopying)
        let scm = ServerCommunicationManager()
        scm.responseErrorOption = ResponseErrorOption.dontShowErrorResponseMessage
        scm.setShowPhoneNumberPreferences(information, completionBlock: {(responseData) -> () in
        })
    }
    
    
    func updateSavings(){
        if isUserLoggedIn() {
            if (isInternetConnectivityAvailable(true)==false){return}
            let cacheExist = isNotNull(CacheManager.sharedInstance.loadObject(CACHED_USER_SAVINGS_DATA))
            if isUserLoggedIn() && canPerformThisEvent(60*4, eventName: #function) || !cacheExist {
                let information = NSMutableDictionary()
                let scm = ServerCommunicationManager()
                scm.responseErrorOption = ResponseErrorOption.dontShowErrorResponseMessage
                scm.returnFailureResponseAlso = true
                scm.getSavings(information, completionBlock: {(responseData) -> () in
                    if isNotNull(responseData){
                        if isNotNull(responseData!.object(forKey: "KmSaved")) {
                            CacheManager.sharedInstance.saveObject(responseData!, identifier: CACHED_USER_SAVINGS_DATA)
                            NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_SAVINGS_UPDATED), object: nil)
                        }
                    }
                })
            }
        }
    }
    
    func updateAccountSummary(){
        if isUserLoggedIn() {
            if (isInternetConnectivityAvailable(true)==false){return}
            let information = NSMutableDictionary()
            copyData(loggedInUserId() , destinationDictionary: information, destinationKey: "userId", methodName: #function)
            let scm = ServerCommunicationManager()
            scm.responseErrorOption = ResponseErrorOption.dontShowErrorResponseMessage
            scm.returnFailureResponseAlso = true
            scm.getAccountSummary(information, completionBlock: {[weak self] (responseData) -> () in guard let `self` = self else { return }
                if isNotNull(responseData?.object(forKey: "data")){
                    let data = (responseData?.object(forKey: "data")) as! NSDictionary
                    CacheManager.sharedInstance.saveObject(data, identifier: "ACCOUNT_SUMMARY")
                    NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_ACCOUNT_SUMMARY_UPDATED), object: nil)
                }
            })
        }
    }
    
    func searchCollegeAndCacheThemToDatabase(_ collegeName:String,completion:@escaping ACFCompletionBlock){
        let latitude = "28.6139"
        let longitude = "77.2090"
        let urlString = "https://maps.googleapis.com/maps/api/place/textsearch/json?location=\(latitude),\(longitude)&radius=50000&sensor=true&query=\(collegeName)&key=\(GOOGLE_API_KEY)".aURLReady()
        let manager = AFHTTPSessionManager()
        manager.responseSerializer = AFHTTPResponseSerializer()
        manager.get(urlString, parameters: nil, progress: { (progress) -> Void in
        }, success: { (urlSessionDataTask, responseObject) -> Void in
            let response = parsedJson(responseObject as? Data, methodName: "")
            if response != nil {
                if let result = response as? [String: Any], let status = result["status"] as? String {
                    switch status {
                    case PlaceStatusCodes.OK.rawValue:
                        if let results = result["results"] as? [[String: Any]] {
                            for place in results {
                                let dictPlace = place as NSDictionary
                                if let name = place["name"] as? String,
                                    let address = place["formatted_address"] as? String,
                                    let lat = dictPlace.value(forKeyPath: "geometry.location.lat") as? Double,
                                    let lon = dictPlace.value(forKeyPath: "geometry.location.lng") as? Double {
                                    Dbm().addCollege(["clg_name":name,"address":address,"clg_id":"-\(abs(Int64(lat*1000000)+Int64(lon*1000000)))"])
                                }
                            }
                            completion(true)
                        }
                        break
                    default:
                        break
                    }
                }
            }
        }){(urlSessionDataTask, error) -> Void in
        }
    }
    
    func processingImmediatelyAfterSignInOrSignUp(){
        Acf().loginToQuickBlox()
        Acf().updateUserProfilePictureIfRequired()
        NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_USER_SIGNED_IN), object: nil)
        saveThisDataForExtensionsAccess(loggedInUserId() , key: "userId")
    }
    
    func canContinueCase1()->Bool{
        if self.carpoolHomeScreenController?.selectedIndex == RIDE_VIEW_TAB_INDEX &&
            sideMenuController?.leftMenuVisible == false && isPopUpViewControllerShowing() == false && Drnm().isDRNActive() == false {
            return true
        }
        return false
    }
    
    func canContinueCase2()->Bool{
        if self.carpoolHomeScreenController?.selectedIndex == RIDE_VIEW_TAB_INDEX &&
            sideMenuController?.leftMenuVisible == false && Drnm().isDRNActive() == false {
            return true
        }
        return false
    }
    
    func validateResponseForSecurityPurpose(response:Any?){
        if isUserLoggedIn() {
            if let responseAsDictionary = response as? NSDictionary {
                if let data = responseAsDictionary.object(forKey: "data") as? NSDictionary {
                    if safeBool(data.object(forKey: "force_logout")) {
                        execMain ({ (completed) in
                            Acf().reset()
                            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(AppCommonFunctions.showSessionExpiredMessage), object: nil)
                            self.perform(#selector(AppCommonFunctions.showSessionExpiredMessage), with: nil, afterDelay: 8)
                        })
                    }
                }
            }
        }
    }
    
    func showSessionExpiredMessage(){
        showAlert(MESSAGE_TITLE___SESSION_EXPIRED, message: MESSAGE_TEXT___SESSION_EXPIRED)
    }
}
