//
//  AppCommonFunctions.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import Foundation
import Branch

extension AppCommonFunctions {
    
}

func isUserLoggedIn()->Bool{
    if let user = Dbm().getUserInfo() {
        if let _ = user.userId {
            if let _ = user.name {
                return true
            }
        }
    }
    return false
}

func isSystemReadyToProcessThis()->Bool{
    if isUserLoggedIn() && !Acf().isAddressInputScreenNeeded() {
        return true
    }
    return false
}

let statusCacheDictionary = NSMutableDictionary()

func getStatusTextFor(_ userInfo:NSDictionary?)->String{
    let userId = getUserId(userInfo)
    if let status = statusCacheDictionary.object(forKey: userId) as? String  {
        return status
    }
    if userInfo != nil {
        if let userStatus = userInfo?.object(forKey: "status_text") as? String{
            Dbm().addUserStatus(["status":userStatus,"userId":getUserId(userInfo)])
        }
        let userStatus = Dbm().getUserStatus(userInfo!)
        let statusValue = safeString(userStatus?.status)
        statusCacheDictionary.setObject(statusValue, forKey: userId as NSCopying)
        return statusValue
    }
    return USER_DEFAULT_STATUS
}

func getStatusText()->String{
    let userInfo = Dbm().getUserInfo()
    if isNotNull(userInfo?.userId){
        let userStatus = Dbm().getUserStatus(["userId":loggedInUserId()])
        if isNotNull(userStatus?.status){
            return userStatus!.status!
        }else{
            return USER_DEFAULT_STATUS
        }
    }else{
        return USER_DEFAULT_STATUS
    }
}

func getUserId(_ userInfo:NSDictionary?)->String{
    if let userId = userInfo?.object(forKey: "traveller_id") as? String{
        return userId
    }else if let userId = userInfo?.object(forKey: "userId") as? String{
        return userId
    }else if let userId = userInfo?.object(forKey: "user_id") as? String{
        return userId
    }else if let userId = userInfo?.object(forKey: "Fk_user_id") as? String{
        return userId
    }else if let userId = userInfo?.object(forKey: "userid") as? String{
        return userId
    }
    else {
        logMessage("God help me !!!")
        return ""
    }
}

func homeLocationCoordinate()->CLLocationCoordinate2D{
    let settings = Dbm().getSetting()
    return CLLocationCoordinate2DMake(settings.homeLocationLatitude!.doubleValue, settings.homeLocationLongitude!.doubleValue)
}

func destinationLocationCoordinate()->CLLocationCoordinate2D{
    let settings = Dbm().getSetting()
    return CLLocationCoordinate2DMake(settings.destinationLocationLatitude!.doubleValue, settings.destinationLocationLongitude!.doubleValue)
}

func fetchAndCacheAccountSummary(){
    if(isInternetConnectivityAvailable(true)==false){return}
    let information = NSMutableDictionary()
    copyData(loggedInUserId() , destinationDictionary: information, destinationKey: "userId", methodName: #function)
    let scm = ServerCommunicationManager()
    scm.responseErrorOption = ResponseErrorOption.dontShowErrorResponseMessage
    scm.returnFailureResponseAlso = true
    scm.getAccountSummary(information, completionBlock: { (responseData) -> () in
        if isNotNull(responseData?.object(forKey: "data")){
            let data = (responseData?.object(forKey: "data")) as! NSDictionary
            CacheManager.sharedInstance.saveObject(data, identifier: "ACCOUNT_SUMMARY")
        }
    })
}

func getPictureUrlFromPoMFileName(_ imageName:String?)->String{
    if imageName?.contains("http") == false {
        return "\(BASE_URL_IMAGE_POM)uploads/profile/thumb_big_\(imageName!)"
    }else{
        if isNotNull(imageName){
            return imageName!
        }else{
            return APP_NO_IMAGE_URL
        }
    }
}

func getGroupPictureUrlFromFileName(_ imageName:String?)->String{
    if imageName?.contains("http") == false {
        return "\(BASE_URL_IMAGE_POM)uploads/\(imageName!)"
    }else{
        if isNotNull(imageName){
            return imageName!
        }else{
            return APP_NO_IMAGE_URL
        }
    }
}

func getUserProfilePictureUrlFromFileName(_ imageName:String?)->String{
    if imageName?.contains("http") == false {
        return "\(BASE_URL_PROFILE_IMAGE)uploads/profile/thumb_big_\(imageName!)"
    }else{
        if isNotNull(imageName){
            return imageName!
        }else{
            return APP_NO_IMAGE_URL
        }
    }
}

func getOfferImageUrlFromFileName(_ imageName:String?)->String{
    if imageName?.contains("http") == false {
        return "\(BASE_URL_FOR_OFFER_IMAGES)\(imageName!)"
    }else{
        if isNotNull(imageName){
            return imageName!
        }else{
            return APP_NO_IMAGE_URL
        }
    }
}

func getDRNPictureUrlFromFileName(_ imageName:String?)->String{
    if imageName?.contains("http") == false {
        return "https://www.orahi.com/images/\(imageName!)"
    }else{
        if isNotNull(imageName){
            return imageName!
        }else{
            return APP_NO_IMAGE_URL
        }
    }
}


func loggedInUserId()->String{
    return safeString(Dbm().getUserInfo()?.userId, alternate: "0")
}

func getCarOwnerId(_ usersRiding:NSArray?)->String{
    if isNotNull(usersRiding){
        if usersRiding!.count > 0{
            for user in usersRiding!{
                return getUserId(user as! NSDictionary)
            }
        }
    }
    return ""
}

func getCarOwner(_ usersRiding:NSArray?)->NSDictionary?{
    if isNotNull(usersRiding){
        if usersRiding!.count > 0{
            for user in usersRiding!{
                return user as! NSDictionary
            }
        }
    }
    return nil
}

func showUserProfile(_ userInfo:NSDictionary,navigationController:UINavigationController){
    if(isInternetConnectivityAvailable(true)==false){return}
    Acf().pushVC("ProfileOthersViewController", navigationController: navigationController, isRootViewController: false, animated: true, modifyObject: { (viewControllerObject) -> () in
        (viewControllerObject as! ProfileOthersViewController).userInfo = userInfo
    })
}

func vibrate(){
    let settings = Dbm().getSetting()
    if settings.notificationVibrations!.boolValue {
        AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
    }
}

func setLevelImage(_ level:String,imageView:UIImageView){
    imageView.isHidden = true
}

func setRankImage(_ rank:String,imageView:UIImageView){
    imageView.isHidden = false
    imageView.contentMode = UIViewContentMode.scaleAspectFit
    let rankInteger = (rank as NSString).intValue
    if rankInteger >= 5 && rankInteger <= 9  {
        imageView.image = UIImage(named:"directionalCaptainActive")
    }else if rankInteger >= 2 && rankInteger <= 4  {
        imageView.image = UIImage(named:"directionalMajorActive")
    }else if rankInteger == 1{
        imageView.image = UIImage(named:"directionalColonelActive")
    }else{
        imageView.isHidden = true
    }
}

func setRankName(_ rank:String,label:UILabel){
    label.isHidden = false
    let rankInteger = (rank as NSString).intValue
    if rankInteger >= 5 && rankInteger <= 9  {
        label.text = "Captain"
    }else if rankInteger >= 2 && rankInteger <= 4  {
        label.text = "Major"
    }else if rankInteger == 1{
        label.text = "Colonel"
    }else{
        label.isHidden = true
    }
}

func getRandomTag()->String{
    switch Int(arc4random_uniform(UInt32(5))) {
    case 0:
        return "PUNCTUAL"
    case 1:
        return "VETERAN"
    case 2:
        return "HELPFUL"
    case 3:
        return "PLEASANT"
    case 4:
        return "TRUSTWORTHY"
    default:
        return "PUNCTUAL"
    }
}

func setUserTagDetails(_ info:NSDictionary?,tagImageView:UIImageView,tagLabel:UILabel){
    if let userTag = info?.object(forKey: "user_tag") as? String  , userTag.length > 0 {
        tagLabel.isHidden = false
        tagImageView.isHidden = false
        tagImageView.image = UIImage(named:"userTag")
        tagLabel.text = userTag.uppercased()
    }else{
        if isUserLoggedIn() {
            tagLabel.isHidden = true
            tagImageView.isHidden = true
        }else{
            tagLabel.isHidden = false
            tagImageView.isHidden = false
            tagImageView.image = UIImage(named:"userTag")
            tagLabel.text = getRandomTag()
        }
    }
}

func setUserTagDetails(_ tag:String?,tagImageView:UIImageView,tagLabel:UILabel){
    if tag != nil && tag!.length > 0 {
        tagLabel.isHidden = false
        tagImageView.isHidden = false
        tagImageView.image = UIImage(named:"userTag")
        tagLabel.text = tag!.uppercased()
    }else{
        tagLabel.isHidden = true
        tagImageView.isHidden = true
    }
}

func isRidingWithDetailsExist(_ info:NSDictionary?)->Bool{
    if isNotNull(info){
        let p1Exist = isNotNull(info!.object(forKey: "passenger_one_detail"))
        let p2Exist = isNotNull(info!.object(forKey: "passenger_two_detail"))
        let p3Exist = isNotNull(info!.object(forKey: "passenger_three_detail"))
        let p4Exist = isNotNull(info!.object(forKey: "passenger_four_detail"))
        return p1Exist || p2Exist || p3Exist || p4Exist
    }
    return false
}

func setRidingWithDetails(_ info:NSDictionary,img1:UIImageView,img2:UIImageView,img3:UIImageView,img4:UIImageView){
    let p1Exist = isNotNull(info.object(forKey: "passenger_one_detail"))
    let p2Exist = isNotNull(info.object(forKey: "passenger_two_detail"))
    let p3Exist = isNotNull(info.object(forKey: "passenger_three_detail"))
    let p4Exist = isNotNull(info.object(forKey: "passenger_four_detail"))
    var p1ImageName = ""
    var p2ImageName = ""
    var p3ImageName = ""
    var p4ImageName = ""
    
    func setImage(_ img:UIImageView,url:String?,exist:Bool){
        if exist {
            img.isHidden = false
            img.sd_setImage(with: URL(string:url!), placeholderImage: USER_PLACEHOLDER_IMAGE)
        }else{
            img.isHidden = true
        }
    }
    if p1Exist{
        p1ImageName = ((info.object(forKey: "passenger_one_detail") as! NSString).components(separatedBy: "|")  as NSArray).object(at: 1) as! String
    }
    if p2Exist{
        p2ImageName = ((info.object(forKey: "passenger_two_detail") as! NSString).components(separatedBy: "|")  as NSArray).object(at: 1) as! String
    }
    if p3Exist{
        p3ImageName = ((info.object(forKey: "passenger_three_detail") as! NSString).components(separatedBy: "|")  as NSArray).object(at: 1) as! String
    }
    if p4Exist{
        p4ImageName = ((info.object(forKey: "passenger_four_detail") as! NSString).components(separatedBy: "|")  as NSArray).object(at: 1) as! String
    }
    setImage(img4, url: getUserProfilePictureUrlFromFileName(p1ImageName), exist: p1Exist)
    setImage(img3, url: getUserProfilePictureUrlFromFileName(p2ImageName), exist: p2Exist)
    setImage(img2, url: getUserProfilePictureUrlFromFileName(p3ImageName), exist: p3Exist)
    setImage(img1, url: getUserProfilePictureUrlFromFileName(p4ImageName), exist: p4Exist)
}

func nameToDisplay(_ qbUser:QBUUser?,chatDialogue:QBChatDialog?,onlyFirstName:Bool = true)->String{
    if isNotNull(qbUser?.fullName){
        if onlyFirstName {
            return qbUser!.fullName!.firstName()
        }else{
            return qbUser!.fullName!
        }
    }
    var userName:UserName?
    if isNotNull(qbUser?.login){
        userName = Dbm().getUserName(["userId":qbUser!.login!.replacingOccurrences(of: "orahi_", with:"")])
    }
    if isNotNull(userName){
        if onlyFirstName {
            return userName!.name!.firstName()
        }else{
            return userName!.name!
        }
    }
    if let name = chatDialogue?.name {
        if chatDialogue!.type == .private{
            if name.contains(QB_USER_NAME_PREFIX) == false {
                return name
            }
        }
    }
    return "User"
}

func isAlreadyProvidedFeedback(_ user:NSDictionary?)->Bool{
    if isNotNull(user){
        if let feedbackRating = user!.object(forKey: "feedback_rating") {
            if ("\(feedbackRating)" as NSString).isEqual(to: "0"){
                return false
            }else{
                return true
            }
        }
        return false
    }
    return false
}

func validateCurrentUser(){
    let userInfo = Dbm().getUserInfo()
    if isNotNull(userInfo){
        if isNull(userInfo?.userId) || isNull(userInfo?.name){
            Dbm().resetDatabase()
        }
    }
}

func displayNameFromName(_ name:String?)->String?{
    if name != nil{
        if name!.contains(RIDE_GROUP_SUFFIX){
            let components = name!.components(separatedBy: RIDE_GROUP_SEPERATOR)
            if isNotNull(components) && components.count > 0{
                let ownerId = components[0]
                let ownerName = Dbm().getUserName(["userId":ownerId])
                if ownerName == nil {//it seems this qbuser is not cached yet , let add it to db , app logic will fetch/cache user later.
                    Dbm().addUserId(ownerId)
                }
                if (ownerId as NSString).isEqual(to: loggedInUserId()){
                    return "\(RIDE_GROUP_DISPLAY_NAME_SELF_PREFIX) \(RIDE_GROUP_DISPLAY_NAME_SELF_SUFFIX)".lowercased().enhancedString()
                }
                if isNotNull(ownerName?.name){
                    return "\(RIDE_GROUP_DISPLAY_NAME_OTHERS_PREFIX) \((ownerName!.name! as String).firstName())".lowercased().enhancedString()
                }
                return RIDE_GROUP_SUFFIX
            }
        }else {
            let group = Dbm().getGroup(["groupId":"\(name!)"])
            if group != nil {
                return group!.name
            }
        }
    }
    if isNull(name){
        return "Group"
    }
    return name
}

func setupRideInfoLabels(_ l1:UILabel,l2:UILabel,l3:UILabel,day:Date,time:Date,towardsDestination:Bool?,isOwner:Bool?,changeModeButton:UIButton){
    
    var l1text = ""
    var l2text = ""
    var l3text = ""
    
    if day.isToday(){
        l1text = "Today"
    }else if day.isTomorrow(){
        l1text = "Tomorrow"
    }else{
        l1text = "\(day.toStringValue("EEEE")),\(day.toStringValue("dd MMM"))"
    }
    
    if isNotNull(towardsDestination){
        if towardsDestination! {
            l2text = "Home - \(destinationCapitalizedName())"
        }else{
            l2text = "\(destinationCapitalizedName()) - Home"
        }
    }else{
        l2text = "-- --"
    }
    
    l3text = time.toStringValue("h:mm a")
    
    // attributes for labels
    var attributesType1 = Dictionary<String, AnyObject>()
    attributesType1[NSForegroundColorAttributeName] = UIColor.gray
    attributesType1[NSFontAttributeName] = UIFont.init(name: l1.font!.fontName, size: 13)
    
    var attributesType2 = Dictionary<String, AnyObject>()
    attributesType2[NSForegroundColorAttributeName] = APP_THEME_VOILET_COLOR
    attributesType2[NSFontAttributeName] = UIFont.init(name: l1.font!.fontName, size: 15)
    
    l1.attributedText =  NSAttributedString(string: l1text, attributes: attributesType2)
    l2.attributedText =  NSAttributedString(string: l2text, attributes: attributesType1)
    l3.attributedText = NSAttributedString(string: l3text, attributes: attributesType2)
    
    l1.textAlignment = .left
    l2.textAlignment = .center
    l3.textAlignment = .right
    
    if isNotNull(isOwner){
        if isOwner! {
            changeModeButton.setTitle("Car Owner ↓", for: UIControlState.normal)
        }else{
            changeModeButton.setTitle("Passenger ↓", for: UIControlState.normal)
        }
    }else{
        changeModeButton.setTitle("-- -- ↓", for: UIControlState.normal)
    }
}

func getTimeFormatInAmPm(_ time:String?)->String{
    if isNotNull(time) {
        let components = time!.components(separatedBy: ":")
        if components.count >= 3{
            let hours = components[0].toInt()
            let minutes =  components[1].toInt()
            let seconds =  components[2].toInt()
            return Date().dateAtStartOfDay().change( nil, month: nil, day: nil, hour: hours, minute: minutes, second: seconds).toStringValue("h:mm a")
        }else{
            return time ?? ""
        }
    }else{
        return ""
    }
}


func getDate(_ time:String?)->Date{
    if isNotNull(time) {
        let components = time!.components(separatedBy: ":")
        if components.count >= 3{
            let hours = components[0].toInt()
            let minutes =  components[1].toInt()
            let seconds =  components[2].toInt()
            return Date().dateAtStartOfDay().change( nil, month: nil, day: nil, hour: hours, minute: minutes, second: seconds)
        }else{
            return Date()
        }
    }else{
        return Date()
    }
}

func updateRideActionButton(_ button:UIButton,userInfo:NSDictionary,isfullWidthCard:Bool = true) {
    let action = Acf().getRightSwipeOption(userInfo)
    button.titleLabel?.font = UIFont.init(name: FONT_BOLD,size: 16)
    button.setTitleColor(APP_THEME_VOILET_COLOR, for:UIControlState.normal)
    button.setTitleColor(APP_THEME_VOILET_COLOR.withAlphaComponent(0.5), for:.highlighted)
    button.isEnabled = true
    button.titleLabel?.textAlignment = .right
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignment.right
    button.titleEdgeInsets = UIEdgeInsetsMake(0,0,0,13)
    button.titleLabel?.minimumScaleFactor = 0.5
    
    if action == .accept {
        button.setTitle("Accept", for: UIControlState.normal)
    }else if action == .settle {
        button.setTitle("Settle payment", for: UIControlState.normal)
    }else if action == .invite {
        button.setTitle("Ride with me", for: UIControlState.normal)
    }else if action == .feedback {
        button.setTitle("Send feedback", for: UIControlState.normal)
        var rating : String?
        if isNotNull(userInfo.object(forKey: "feedback_rating")){
            rating = "\(userInfo.object(forKey: "feedback_rating")!)"
        }
        if isNotNull(rating) &&
            rating!.length > 0 &&
            (rating!.isEqual("0") == false) {
            button.titleLabel?.font = UIFont.init(name: FONT_REGULAR,size: 13)
            button.setTitleColor(Acf().colorForRideStatus(userInfo), for:UIControlState.normal)
            button.setTitleColor(APP_THEME_GRAY_COLOR, for:.highlighted)
            button.isEnabled = false
            button.setTitle("Completed", for: UIControlState.normal)
        }
    }else{
        button.titleLabel?.font = UIFont.init(name: FONT_REGULAR,size: 13)
        button.setTitleColor(Acf().colorForRideStatus(userInfo), for:UIControlState.normal)
        button.setTitleColor(APP_THEME_GRAY_COLOR, for:.highlighted)
        Acf().decideAndSetUserMiniRideStatus(userInfo, button:button)
    }
    
    if let actionState = userInfo.object(forKey: "actionState") as? String {
        if actionState == "success" {
            button.isEnabled = false
            button.setTitle("Success 👍🏻", for: .normal)
            button.titleLabel?.font = UIFont.init(name: FONT_REGULAR,size: 13)
            button.setTitleColor(APP_THEME_GRAY_COLOR, for:.normal)
        }
    }
}

func updateModeLabelType1(_ info:NSDictionary,modeLabel:UILabel){
    if let travelStatus = (info.object(forKey: "travel_status") as? NSString){
        if travelStatus.isEqual(to: "0")  {
            modeLabel.text = "Passenger"
        }else{
            modeLabel.text = "Car Owner"
        }
    }else{
        modeLabel.text = ""
    }
}

func updateModeLabelType2(_ info:NSDictionary,userInfo:NSDictionary?,modeLabel:UILabel){
    if isNotNull(info.object(forKey: "isPassenger")){
        modeLabel.isHidden = false
        let isPassenger = (info.object(forKey: "isPassenger") as! NSString)
        if isPassenger.isEqual(to: "1")  {
            modeLabel.text = "Passenger"
        }else{
            modeLabel.text = "Car Owner"
        }
    }else{
        modeLabel.isHidden = true
    }
}

func generateRideId(_ selectedDayDate:Date,trueForHomeToDestinationFalseforDestinationToHome:Bool) -> String {
    return "\(selectedDayDate.day())\(selectedDayDate.month())\(selectedDayDate.year())\(trueForHomeToDestinationFalseforDestinationToHome == true ? "0" : "1")"
}

func isInForeground()->Bool{
    return UIApplication.shared.applicationState == .active
}

func reportRideCompletionToServer(_ coRiders:NSMutableArray,carOwnerId:String,currentLocation:CLLocation,rideId:String){
    if coRiders.count > 0 && isNotNull(carOwnerId) && rideId.length > 0{
        let coRiderIds = NSMutableString()
        for user in coRiders {
            coRiderIds.append("\(getUserId(user as! NSDictionary)),")
        }
        if coRiderIds.length > 0{
            coRiderIds.deleteCharacters(in: NSMakeRange(coRiderIds.length - 1, 1))
        }
        
        let information = NSMutableDictionary()
        copyData(loggedInUserId() , destinationDictionary: information, destinationKey: "id", methodName:#function)
        copyData("\(currentTimeStamp())" , destinationDictionary: information, destinationKey: "timeStamp", methodName:#function)
        copyData(coRiderIds, destinationDictionary: information, destinationKey: "coRiders", methodName:#function)
        copyData(carOwnerId , destinationDictionary: information, destinationKey: "carOwner", methodName:#function)
        copyData("\(currentLocation.coordinate.latitude)", destinationDictionary: information, destinationKey: "lat", methodName:#function)
        copyData("\(currentLocation.coordinate.longitude)", destinationDictionary: information, destinationKey: "lng", methodName:#function)
        copyData(rideId , destinationDictionary: information, destinationKey: "rideId", methodName:#function)
        
        let scm = ServerCommunicationManager()
        scm.responseErrorOption = .dontShowErrorResponseMessage
        scm.notifyServerForRideCompletionEvent(information, completionBlock: { (responseData) in
        })
    }
}

func saveThisDataForExtensionsAccess(_ data:Any?,key:String) {
    func performTask(){
        if let userDefaults = UserDefaults(suiteName: USER_DEFAULTS_EXTENSION_GROUP_ID) {
            if isNotNull(data){
                if let contentAsData = NSKeyedArchiver.archivedData(withRootObject: data!) as? Data{
                    userDefaults.set(contentAsData, forKey: key)
                }
            }else{
                userDefaults.removeObject(forKey: key)
            }
            userDefaults.synchronize()
        }
    }
    performTask()
}

func handleDeepLinking(_ url:URL){
    if let urlString = url.absoluteString as? NSString{
        if urlString.contains("actionFrom:Extension1"){
            //the call to open app , was from extension1 , which is related to weekly planner.
            //lets open weekly view screen.
            execMain({
                if isUserLoggedIn() {
                    Acf().showWeeklyPlannerScreen()
                }
            },delay:1)
        }
    }
}

func performAfterBlockActionForUser(_ user:QBUUser){
    let activeDialogs = Acf().dialogs()
    for d in activeDialogs {
        if let dialog = d as? QBChatDialog {
            if ("\(dialog.recipientID)" as NSString).isEqual(to: "\(user.id)"){
                dialog.leave(completionBlock: { (error) in
                    logMessage(error?.localizedDescription)
                })
            }
        }
    }
}

func performAfterUnBlockActionForUser(_ user:QBUUser){
    let activeDialogs = Acf().dialogs()
    for d in activeDialogs {
        if let dialog = d as? QBChatDialog {
            if ("\(dialog.recipientID)" as NSString).isEqual(to: "\(user.id)"){
                dialog.join(completionBlock: { (error) in
                    logMessage(error?.localizedDescription)
                })
            }
        }
    }
}

func saveProfileInformation(_ data:NSMutableDictionary){
    SwiftTryCatch.tryThis({
        if let allDetails = data as NSMutableDictionary?{
            if let profileInformation = allDetails.object(forKey: "profileInformation") as? NSMutableDictionary {
                if isSystemReadyToProcessThis(){
                    profileInformation.setObject(loggedInUserId(), forKey: "userId" as NSCopying)
                }
                allDetails.setObject(profileInformation, forKey: "profileInformation" as NSCopying)
                CacheManager.sharedInstance.saveObject(allDetails, identifier: CACHED_USER_PROFILE_DATA)
            }
        }
    }, catchThis: {error in},finally:{})
}

func canPerformThisEvent(_ reOccuringGapInMinutes:NSInteger,eventName:String)->Bool {
    let key = "canPerform\(eventName)"
    if let lastEventDate = CacheManager.sharedInstance.loadObject(key) as? Date{
        if  lastEventDate.isEarlierThanDate(Date().dateBySubtractingMinutes(reOccuringGapInMinutes)) {
            CacheManager.sharedInstance.saveObject(Date() , identifier: key)
            return true
        }
    }else{
        CacheManager.sharedInstance.saveObject(Date() , identifier: key)
        return true
    }
    return false
}

func destinationNameForUser(_ user:NSDictionary?)->String{
    if let userType = user?.object(forKey: "user_type") as? NSString {
        if userType.isEqual(to: USER_TYPE_CORPORATE){
            return "office"
        }else if userType.isEqual(to: USER_TYPE_STUDENT){
            return "college"
        }
    }else if let userType = user?.object(forKey: "userType") as? NSString {
        if userType.isEqual(to: USER_TYPE_CORPORATE){
            return "office"
        }else if userType.isEqual(to: USER_TYPE_STUDENT){
            return "college"
        }
    }
    return "destination"
}

func destinationCapitalizedNameForUser(_ user:NSDictionary?)->String{
    return destinationNameForUser(user).capitalized
}

func destinationName()->String{
    let settings = Dbm().getSetting()
    if let userType = settings.userType as? NSString {
        if userType.isEqual(to: USER_TYPE_CORPORATE){
            return "office"
        }else if userType.isEqual(to: USER_TYPE_STUDENT){
            return "college"
        }
    }
    return "office"
}

func destinationCapitalizedName()->String{
    return destinationName().capitalized
}

func userType()->String{
    let settings = Dbm().getSetting()
    let userTypeObject = safeString(settings.userType)
    if userTypeObject.isEqual(USER_TYPE_CORPORATE){
        return USER_TYPE_CORPORATE
    }else if userTypeObject.isEqual(USER_TYPE_STUDENT){
        return USER_TYPE_STUDENT
    }else if userTypeObject.isEqual(USER_TYPE_OTHER){
        return USER_TYPE_OTHER
    }else{
        return USER_TYPE_CORPORATE
    }
}

func updateTravelPlan(_ information:NSMutableDictionary,selectedDayDate : Date , selectedDayTime : Date , isHomeToDestination : Bool, updatePlanner:Bool,completion:RidePlannerModificationBlock?){
    completion!(selectedDayDate,selectedDayTime,isHomeToDestination)
    if updatePlanner {
        showNotification(MESSAGE_TEXT___UPDATING_RIDE_PLAN, showOnNavigation: false, showAsError: false, duration: 2)
        let scm = ServerCommunicationManager()
        scm.showSuccessResponseMessage = true
        scm.updatePlan(information, completionBlock: {(responseData) -> () in
            completion!(selectedDayDate,selectedDayTime,isHomeToDestination)
        })
    }
}

func showDateTimePicker(_ selectedDateAndTime:Date = Date(),mode:UIDatePickerMode,completion:@escaping (_ selectedDate :Date) ->()){
    let style = RMActionControllerStyle.white
    let actionController = RMDateSelectionViewController(style: style, title: "Select Travel Plan", message: "Please choose and press 'Select' or 'Cancel'.",select: RMAction(title: "Select", style: .cancel) { (controller) in
        
        if let dateController = controller as? RMDateSelectionViewController {
            let date = dateController.datePicker.date
            completion(date)
        }
        
        }, andCancel: RMAction(title: "Cancel", style: .done) { (controller) in
            
            
    })!
    
    if mode == .time {
        let in15MinAction = RMAction(title: "15 Min", style: .additional) { controller -> Void in
            actionController.datePicker.date = Date(timeIntervalSinceNow: 15*60)
        }
        in15MinAction!.dismissesActionController = false
        
        let in30MinAction = RMAction(title: "30 Min", style: .additional) { controller -> Void in
            actionController.datePicker.date = Date(timeIntervalSinceNow: 30*60)
        }
        in30MinAction!.dismissesActionController = false
        
        let in45MinAction = RMAction(title: "45 Min", style: .additional) { controller -> Void in
            actionController.datePicker.date = Date(timeIntervalSinceNow: 45*60)
        }
        in45MinAction!.dismissesActionController = false
        
        let in60MinAction = RMAction(title: "60 Min", style: .additional) { controller -> Void in
            actionController.datePicker.date = Date(timeIntervalSinceNow: 60*60)
        }
        in60MinAction!.dismissesActionController = false
        
        let groupedAction = RMGroupedAction(style: .additional, andActions: [in15MinAction!, in30MinAction!, in45MinAction!, in60MinAction!])
        actionController.addAction(groupedAction! as! RMAction<UIDatePicker>)
        let nowAction = RMAction(title: "Now", style: .additional) { controller -> Void in
            actionController.datePicker.date = Date()
        }
        nowAction!.dismissesActionController = false
        actionController.addAction(nowAction! as! RMAction<UIDatePicker>)
    }else if mode == .date {
        let todayAction = RMAction(title: "Today", style: .additional) { controller -> Void in
            actionController.datePicker.date = Date()
        }
        todayAction!.dismissesActionController = false
        actionController.addAction(todayAction! as! RMAction<UIDatePicker>)
        let tommorowAction = RMAction(title: "Tommorow", style: .additional) { controller -> Void in
            actionController.datePicker.date = Date().dateByAddingDays(1)
        }
        tommorowAction!.dismissesActionController = false
        actionController.addAction(tommorowAction! as! RMAction<UIDatePicker>)
    }
    
    actionController.disableBouncingEffects = true
    actionController.disableMotionEffects = true
    actionController.disableBlurEffects = true
    actionController.disableBlurEffectsForBackgroundView = true
    actionController.datePicker.datePickerMode = mode
    actionController.datePicker.minuteInterval = 5
    actionController.datePicker.minimumDate = Date().dateAtStartOfDay()
    actionController.datePicker.date = selectedDateAndTime
    
    if actionController.responds(to: Selector(("popoverPresentationController:"))) && UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad {
        actionController.modalPresentationStyle = UIModalPresentationStyle.popover
    }
    Acf().navigationController?.present(actionController, animated: true, completion: nil)
}

func updateUserProfile(_ userId:String,completion:@escaping ACFCompletionBlock){
    Acf().getUserProfileDetails(userId, completion: { (returnedData) -> () in
        if isNotNull(returnedData){
            let profileInfo = (returnedData as! NSDictionary).object(forKey: "profileInformation") as! NSMutableDictionary
            copyData(userId , destinationDictionary: profileInfo, destinationKey: "id", methodName: #function)
            let settingsInfo = (returnedData as! NSDictionary).object(forKey: "informationForSettings") as! NSDictionary
            Dbm().setUserInfo(profileInfo)
            Dbm().setSetting(settingsInfo)
            if isUserLoggedIn() {
                completion(true)
            }else{
                completion(nil)
            }
        }else{
            completion(nil)
        }
    },useCache:false)
}

func shouldShowOnBoardingProfileUpdateStep1()->Bool{
    let userInfo = Dbm().getUserInfo()
    return !(isNotNull(userInfo?.name) && isNotNull(userInfo?.email) && isNotNull(userInfo?.genderTforMaleFForFemale))
}

func shouldShowOnBoardingProfileUpdateStep2()->Bool{
    let settings = Dbm().getSetting()
    if getUserOnBoardingType() == .regular || getUserOnBoardingType() == .carpool {
        //if user is for regular or carpool only flow
        //if settings are already saved then do not show this screen else ask user wether he is corporate or student
        return !(isNotNull(settings.userType))
    }else if getUserOnBoardingType() == .peaceOfMind && isNull(settings.userType){
        //if user is for pom
        //In this case we never need to show this screen
        //But we have to detect a state and fix it here , i.e if user intended for PoM but some how userType was not set
        //if it is null , then set it to others
        settings.userType = USER_TYPE_OTHER
        settings.isProfileUpdated = NSNumber(value: true)
        Dbm().saveChanges()
        return false
    }else{
        return !(isNotNull(settings.userType))
    }
}

func shouldShowOnBoardingProfileUpdateStep3()->Bool{
    let userInfo = Dbm().getUserInfo()
    return !(isNotNull(userInfo?.isPassenger))
}

func shouldContinueWithPoM()->Bool{
    let settings = Dbm().getSetting()
    return safeString(settings.userType) == USER_TYPE_OTHER
}

func shouldShowOnBoardingProfileUpdateStep4()->Bool{
    let userInfo = Dbm().getUserInfo()
    let settings = Dbm().getSetting()
    if isNotNull(userInfo?.isPassenger){
        if !safeBool(userInfo!.isPassenger!.toBool()){
            return !(isNotNull(settings.carMake) && isNotNull(settings.carModel) && isNotNull(settings.carRegistrationNo))
        }
    }
    return false
}

func shouldShowOnBoardingProfileUpdateStep6()->Bool{
    let settings = Dbm().getSetting()
    return !(isNotNull(settings.homeAddress) && isNotNull(settings.homeTime))
}

func shouldShowOnBoardingProfileUpdateStep7()->Bool{
    let settings = Dbm().getSetting()
    return !(isNotNull(settings.destinationAddress) && isNotNull(settings.destinationTime))
}

func decideAndShowNextScreen(_ navigationController:UINavigationController,isStartup:Bool=false){
    if UserDefaults.standard.bool(forKey: "ClearTemporaryAddressCaches") {
        let settings = Dbm().getSetting()
        settings.destinationTime = nil
        settings.destinationAddress = nil
        Dbm().saveChanges()
        UserDefaults.standard.set(false, forKey: "ClearTemporaryAddressCaches")
    }
    var isUserResumingSignupProcess = false
    if shouldShowOnBoardingProfileUpdateStep1(){
        isUserResumingSignupProcess = true
        Acf().pushVC("AuthenticationStep3ViewController", navigationController: navigationController, isRootViewController: false, animated: true, modifyObject: nil)
        CacheManager.sharedInstance.saveObject("true", identifier: "SignUpProcessStarted")
    }else if shouldShowOnBoardingProfileUpdateStep2(){
        isUserResumingSignupProcess = true
        Acf().pushVC("AuthenticationStep4ViewController", navigationController: navigationController, isRootViewController: false, animated: true, modifyObject: { (viewControllerObject) in
            (viewControllerObject as! AuthenticationStep4ViewController).workingMode = .userType
        })
    }else if shouldContinueWithPoM(){
        let settings = Dbm().getSetting()
        if safeBool(settings.isProfileUpdated){
            Acf().updateProfileToServerIfRequired({ (returnedData) in })
        }
        Acf().showHomeScreen()
    }else if shouldShowOnBoardingProfileUpdateStep3(){
        isUserResumingSignupProcess = true
        Acf().pushVC("AuthenticationStep4ViewController", navigationController: navigationController, isRootViewController: false, animated: true, modifyObject: { (viewControllerObject) in
            (viewControllerObject as! AuthenticationStep4ViewController).workingMode = .travellerMode
        })
    }else {
        let userInfo = Dbm().getUserInfo()
        let isPassenger = userInfo!.isPassenger!
        if isPassenger == "0" && shouldShowOnBoardingProfileUpdateStep4(){
            isUserResumingSignupProcess = true
            Acf().pushVC("AuthenticationStep5ViewController", navigationController: navigationController, isRootViewController: false, animated: true, modifyObject: nil)
        }else{
            if shouldShowOnBoardingProfileUpdateStep6(){
                isUserResumingSignupProcess = true
                Acf().pushVC("AuthenticationStep6ViewController", navigationController: navigationController, isRootViewController: false, animated: true, modifyObject: nil)
            }else if shouldShowOnBoardingProfileUpdateStep7(){
                isUserResumingSignupProcess = true
                Acf().pushVC("AuthenticationStep7ViewController", navigationController: navigationController, isRootViewController: false, animated: true, modifyObject: nil)
            }else{
                if safeBool(CacheManager.sharedInstance.loadObject("SignUpProcessResumed")) {
                    CacheManager.sharedInstance.saveObject(nil, identifier: "SignUpProcessResumed")
                    trackFunctionalEvent(FE_USER_ONBOARD_SMOOTH, information: nil,isSuccess:false)
                }else if safeBool(CacheManager.sharedInstance.loadObject("SignUpProcessStarted")) {
                    CacheManager.sharedInstance.saveObject(nil, identifier: "SignUpProcessStarted")
                    trackFunctionalEvent(FE_USER_ONBOARD_SMOOTH, information: nil)
                }
                Acf().showHomeScreen()
            }
        }
    }
    if isStartup && isUserResumingSignupProcess {
        CacheManager.sharedInstance.saveObject("true", identifier: "SignUpProcessResumed")
        trackFunctionalEvent(FE_SIGNUP_PROCESS_RESUMED, information: nil)
    }
}

func getProfilePictureUrl()->String?{
    let userInfo = Dbm().getUserInfo()
    if isNotNull(userInfo!.picture){
        return getUserProfilePictureUrlFromFileName(userInfo!.picture)
    }else{
        return getUserProfilePictureUrlFromFileName("noImage.jpg")
    }
}

public extension UILabel{
    
    func setAttributedTextWithOptimisationsType1(){
        let string = safeString(self.text).replacingOccurrences(of: " Rs ", with: " ₹ ").replacingOccurrences(of: "Rs.", with: "₹")
        let attributes = [NSFontAttributeName:self.font,
                          NSForegroundColorAttributeName:self.textColor] as NSDictionary
        let attributedText = NSMutableAttributedString(string:string)
        attributedText.applyAttributesBySearching(string, attributes: attributes)
        
        let attributesStar = [NSFontAttributeName:self.font,
                              NSForegroundColorAttributeName:APP_THEME_YELLOW_COLOR] as NSDictionary
        attributedText.applyAttributesBySearching("★", attributes: attributesStar)
        
        let attributesRupeeSymbol = [NSFontAttributeName:UIFont(name: FONT_REGULAR, size: 14),
                                     NSForegroundColorAttributeName:self.textColor] as NSDictionary
        attributedText.applyAttributesBySearching("₹ (\\d{1,3})", attributes: attributesRupeeSymbol)
        
        self.attributedText = attributedText
    }
    
    func setAttributedTextWithOptimisationsType2(fontSize:CGFloat=8){
        let string = safeString(self.text)
        let attributes = [NSFontAttributeName:self.font,
                          NSForegroundColorAttributeName:self.textColor] as NSDictionary
        let attributedText = NSMutableAttributedString(string:string)
        attributedText.applyAttributesBySearching(string, attributes: attributes)
        let attributesSymbol = [NSFontAttributeName:UIFont(name: FONT_SEMI_BOLD, size: fontSize),
                                NSForegroundColorAttributeName:self.textColor,NSBaselineOffsetAttributeName:3] as NSDictionary
        attributedText.applyAttributesBySearching("₹", attributes: attributesSymbol)
        attributedText.applyAttributesBySearching("%", attributes: attributesSymbol)
        self.attributedText = attributedText
    }
    
    func setAttributedTextWithOptimisationsType3(textToChange:String,fontSize:CGFloat=8){
        let string = safeString(self.text)
        let attributes = [NSFontAttributeName:self.font,
                          NSForegroundColorAttributeName:self.textColor] as NSDictionary
        let attributedText = NSMutableAttributedString(string:string)
        attributedText.applyAttributesBySearching(string, attributes: attributes)
        let attributesOtherText = [NSFontAttributeName:UIFont(name: FONT_BOLD, size: fontSize)] as NSDictionary
        attributedText.applyAttributesBySearching(textToChange, attributes: attributesOtherText)
        let attributesSymbol = [NSFontAttributeName:UIFont(name: FONT_BOLD, size: fontSize),
                                NSForegroundColorAttributeName:self.textColor] as NSDictionary
        attributedText.applyAttributesBySearching("%", attributes: attributesSymbol)
        attributedText.applyAttributesBySearching("₹", attributes: attributesSymbol)
        self.attributedText = attributedText
    }
    
}

func getInput(inputMessage:String,viewController:UIViewController,completionBlock:@escaping ACFCompletionBlock){
    let prompt = UIAlertController(title: "Input", message: "\n\(inputMessage)", preferredStyle: UIAlertControllerStyle.alert)
    prompt.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
    prompt.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.cancel, handler: { (action) -> Void in
        let enteredText = (prompt.textFields![0] as UITextField).text
        if enteredText != nil && enteredText!.length > 0 {
            completionBlock(enteredText!)
        }
    }
    ))
    prompt.addTextField(configurationHandler: {(textField: UITextField!) in
        textField.keyboardType = UIKeyboardType.emailAddress
    })
    viewController.present(prompt, animated: true, completion: nil)
}

func minimumRechargeValue()->Int{
    var minimumRechargeValue = 100
    if let details = CacheManager.sharedInstance.loadObject("ACCOUNT_SUMMARY") as? NSDictionary{
        minimumRechargeValue = safeInt(details.object(forKey: "min_recharge_value"),alternate: 100)
    }
    return minimumRechargeValue
}

func defaultRechargeValue()->Int{
    var defaultRechargeValue = 400
    if let details = CacheManager.sharedInstance.loadObject("ACCOUNT_SUMMARY") as? NSDictionary{
        defaultRechargeValue = safeInt(details.object(forKey: "default_recharge_value"),alternate: 400)
    }
    return defaultRechargeValue
}

func getUserOnBoardingType()->UserBoardingType {
    let settings = Dbm().getSetting()
    if settings.userBoardingType != nil {
        if settings.userBoardingType! == "carpool" {
            return UserBoardingType.carpool
        }else if settings.userBoardingType! == "pom" {
            return UserBoardingType.peaceOfMind
        }else if settings.userBoardingType! == "regular" {
            return UserBoardingType.regular
        }
    }
    return UserBoardingType.regular
}

func showEnableCarpoolPopupAndDoRequiredProcessing(){
    let prompt = UIAlertController(title: "Enable Carpool?", message: "\nDo you want to enable Orahi's Carpool services?", preferredStyle: UIAlertControllerStyle.alert)
    prompt.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
    prompt.addAction(UIAlertAction(title: "Enable", style: UIAlertActionStyle.cancel, handler: { (action) -> Void in
        Acf().pushVC("AuthenticationStep4ViewController", navigationController: Acf().navigationController!, isRootViewController: false, animated: true, modifyObject: { (viewControllerObject) in
            (viewControllerObject as! AuthenticationStep4ViewController).workingMode = .userType
            (viewControllerObject as! AuthenticationStep4ViewController).showCarpoolOptionOnly = true
        })
        Acf().playDefaultAlertSound()
    }))
    Acf().navigationController!.present(prompt, animated: true, completion: nil)
}

func showGroupDetailScreen(group:Group,navigationController:UINavigationController){
    let vc = getViewController("GroupDetailViewController") as! GroupDetailViewController
    vc.group = group
    let v = vc.view
    navigationController.pushViewController(vc, animated: true)
}

func showMemberDetailScreen(group:Group,member:Member,navigationController:UINavigationController){
    let vc = getViewController("MemberDetailViewController") as! MemberDetailViewController
    vc.group = group
    vc.member = member
    let v = vc.view
    navigationController.pushViewController(vc, animated: true)
}

func showCarpoolShareOptions(){
    if isSystemReadyToProcessThis() &&
        (isInternetConnectivityAvailable(true)){
        if let urlToShare = Acf().referUrl {
            let userInfo = Dbm().getUserInfo()
            trackFunctionalEvent(FE_REFFERAL_CODE_SHARED, information: ["referralCode":userInfo!.refferalCode!,"url":urlToShare])
            var sharingItems = [Any]()
            let description = "Hey! Join 100,000 members on Orahi Carpool community. Sign up via this link \n\(urlToShare)\n to get your first 2 rides FREE. #LetsCarpool."
            if isNotNull(description){
                sharingItems.append(description as AnyObject)
            }
            let activityViewController = UIActivityViewController(activityItems: sharingItems, applicationActivities: nil)
            Acf().navigationController?.present(activityViewController, animated: true, completion: nil)
        }else{
            Acf().retryInitialisingBranch()
        }
    }
}

func showPoMShareOptions(){
    if(isInternetConnectivityAvailable(true)==false){return}
    if let urlToShare = Branch.getInstance().getShortURL(withParams:
        [
            "referral_type":"app_download",
            "url_creation_time_stamp":"\(currentTimeStamp())",
            "on_boarding_type" : "pom",
            "$og_title" : "Orahi - Peace of mind"
        ]) {
        var sharingItems = [Any]()
        let description = "Hi! Peace of Mind has helped me and my family find our #MannKiShanti while travelling daily. Try it for FREE\n\(urlToShare)"
        if isNotNull(description){
            sharingItems.append(description as AnyObject)
        }
        let activityViewController = UIActivityViewController(activityItems: sharingItems, applicationActivities: nil)
        Acf().navigationController?.present(activityViewController, animated: true, completion: nil)
    }else{
        Acf().retryInitialisingBranch()
    }
}

func sendGroupJoinInvitations(group:Group){
    if(isInternetConnectivityAvailable(true)==false){return}
    if let urlToShare = Branch.getInstance().getShortURL(withParams:
        [
            "referral_type":"group_invitation",
            "group_id":safeString(group.groupId),
            "url_creation_time_stamp":"\(currentTimeStamp())",
            "on_boarding_type" : "pom",
            "$og_title" : "Orahi - Peace of mind"
        ]) {
        var sharingItems = [Any]()
        let description = "Join my peace of mind '\(safeString(group.name))' group, and stay connected with me at all times. Click link to download \n\(urlToShare)"
        if isNotNull(description){
            sharingItems.append(description as AnyObject)
        }
        let activityViewController = UIActivityViewController(activityItems: sharingItems, applicationActivities: nil)
        Acf().navigationController?.present(activityViewController, animated: true, completion: nil)
    }else{
        Acf().retryInitialisingBranch()
    }
}

func showRequestLocationAccessPopupAndDoRequiredProcessing(group:Group,member:Member){
    if(isInternetConnectivityAvailable(true)==false){return}
    let prompt = UIAlertController(title: "REQUEST LOCATION 📍", message: "\nWould you like \n\(safeString(member.name).firstName())\n to turn on location sharing?", preferredStyle: UIAlertControllerStyle.alert)
    prompt.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
    prompt.addAction(UIAlertAction(title: "Request", style: UIAlertActionStyle.cancel, handler: { (action) -> Void in
        let information = NSMutableDictionary()
        copyData(group.groupId , destinationDictionary: information, destinationKey: "groupId", methodName: #function)
        copyData(member.userId , destinationDictionary: information, destinationKey: "userIdTo", methodName: #function)
        let scm = ServerCommunicationManager()
        scm.responseErrorOption = ResponseErrorOption.dontShowErrorResponseMessage
        scm.requestToShareLocation(information) { (responseData) in
            if let _ = responseData {
                showNotification("😃 You request to Turn on location sharing has been sent to \(safeString(member.name).firstName()).", showOnNavigation: true, showAsError: false)
            }
        }
    }))
    Acf().navigationController!.present(prompt, animated: true, completion: nil)
}

func updateLocationSharing(group:Group){
    if(isInternetConnectivityAvailable(true)==false){return}
    let information = NSMutableDictionary()
    copyData(group.groupId , destinationDictionary: information, destinationKey: "groupId", methodName: #function)
    copyData(group.locationSharingEnabled ? "true" : "false" , destinationDictionary: information, destinationKey: "locationSharingEnabled", methodName: #function)
    let scm = ServerCommunicationManager()
    scm.responseErrorOption = ResponseErrorOption.dontShowErrorResponseMessage
    scm.returnFailureResponseAlso = true
    scm.configureGroup(information) { (responseData) in
        Lem().doRequiredProcessing()
    }
}

func updateEntryExitConfigurationToServer(group:Group,associatedPlace:AssociatedPlaces){
    if(isInternetConnectivityAvailable(true)==false){return}
    let information = NSMutableDictionary()
    copyData(group.groupId , destinationDictionary: information, destinationKey: "groupId", methodName: #function)
    copyData(associatedPlace.placeId , destinationDictionary: information, destinationKey: "placeId", methodName: #function)
    copyData(associatedPlace.entrySharingEnabled ? "true" : "false" , destinationDictionary: information, destinationKey: "entrySharingEnabled", methodName: #function)
    copyData(associatedPlace.exitSharingEnabled ? "true" : "false" , destinationDictionary: information, destinationKey: "exitSharingEnabled", methodName: #function)
    let scm = ServerCommunicationManager()
    scm.responseErrorOption = ResponseErrorOption.dontShowErrorResponseMessage
    scm.returnFailureResponseAlso = true
    scm.configurePlace(information, completionBlock: { (responseData) -> () in
    })
}

func canShowThisNotification(notification:Notification)->Bool{
    if notification.notificationType == "carpool" {
        if Date(timeIntervalSince1970: safeDouble(notification.timestamp)).isLaterThanDate(Date.yesterday()){
            return true
        }else{
            return false
        }
    }
    return (safeString(notification.groupId).length > 0) || (safeString(notification.associatedUserId).length > 0) || (safeString(notification.placeId).length > 0)
}

func coordinate(place:Place)->CLLocationCoordinate2D{
    return CLLocationCoordinate2D(latitude: safeDouble(place.placeLatitude), longitude: safeDouble(place.placeLongitude))
}

func sendEventMessageOnPoMChatGroup(eventType:String,_ messageToSend:String,group:Group){
    let groupChatDialogue = Acf().getChatDialogue(Acf().chatGroupName(group: group))
    if isNotNull(groupChatDialogue) && Acf().isQBReadyForItsUserDependentServices() {
        let userInfo = Dbm().getUserInfo()
        let message = QBChatMessage()
        let senderID = ServicesManager.instance().currentUser()!.id
        message.text = messageToSend
        message.senderID = senderID
        message.markable = true
        message.dateSent = Date()
        #if DEBUG
            let mos = "iOSV\(UIApplication.appVersion())B\(UIApplication.appBuild())D".replacingOccurrences(of: ".", with: "")
        #else
            let mos = "iOSV\(UIApplication.appVersion())B\(UIApplication.appBuild())P".replacingOccurrences(of: ".", with: "")
        #endif
        message.customParameters = [MESSAGE_TYPE_KEY:eventType,"eventDate":"\(currentTimeStamp())","userId":loggedInUserId(),"sendersName":userInfo!.name!.firstName(),"mos":mos]
        ServicesManager.instance().chatService.send(message, to: groupChatDialogue!, saveToHistory: true, saveToStorage: true, completion: { (error) in
            if (error != nil) {
                logMessage("\(error)")
                Dbm().addFailedQBMessage(["messageId":message.id!])
            }
        })
    }
}

func generateAlertsForDebuggingPurpose(message:String){
    LocalNotificationHelper.sharedInstance().scheduleNotification(title: "Debugging Information", message: message, date: Date().dateByAddingSeconds(5) as NSDate, userInfo: nil)
    showInformationBannerOnBottom(Acf().navigationController!, style: .blurLight, duration: 10, title: "Debugging Information", message: message)
}

func createServerNotificationForPoMEntryExitEvent(placeId:String,isEntry:Bool,altitude:String,speed:String,accuracy:String){
    execMain({
        if(isInternetConnectivityAvailable(false)==false){
            showPoMSpecialDebuggingMessage(message: "😡😡 : CREATE NOTIFICATION : NO INTERNET : \(safeString(placeId.place()?.name))");return
        }
        showPoMSpecialDebuggingMessage(message: "👍🏻 : CREATE NOTIFICATION : HITTING API : \(safeString(placeId.place()?.name))")
        let information = NSMutableDictionary()
        copyData(placeId , destinationDictionary: information, destinationKey: "placeId", methodName: #function)
        copyData(isEntry ? "place_entry" : "place_exit" , destinationDictionary: information, destinationKey: "notificationType", methodName: #function)
        copyData(safeString(currentTimeStamp()) , destinationDictionary: information, destinationKey: "timestamp", methodName: #function)
        copyData("true" , destinationDictionary: information, destinationKey: "sendPushNotification", methodName: #function)
        copyData(altitude , destinationDictionary: information, destinationKey: "altitude", methodName: #function)
        copyData(speed , destinationDictionary: information, destinationKey: "speed", methodName: #function)
        copyData(accuracy , destinationDictionary: information, destinationKey: "accuracy", methodName: #function)
        let scm = ServerCommunicationManager()
        if ENABLE_FEATURE_DEBUG_INFO {
            scm.responseErrorOption = ResponseErrorOption.showErrorResponseUsingLocalNotification
        }
        scm.createNotification(information, completionBlock: { (responseData) -> () in
            if isNotNull(responseData){
                trackFunctionalEvent(FE_CREATE_NOTIFICATIONS_SUCCESS, information: information)
                showPoMSpecialDebuggingMessage(message: "👍🏻👍🏻👍🏻 SERVER : CREATE NOTIFICATION : \(safeString(placeId.place()?.name))")
            }else{
                trackFunctionalEvent(FE_CREATE_NOTIFICATIONS_FAILED, information: information)
                showPoMSpecialDebuggingMessage(message: "😡😡😡 SERVER : CREATE NOTIFICATION : \(safeString(placeId.place()?.name))")
            }
        })
    }, delay: isEntry ? 10.0 : 0.0 )
}

var lastUpdatedLocation:CLLocation?
var lastUpdatedDate:Date?

func updateSignificantLocationToServer(location:CLLocation,activity:String,timeStamp:String,battery:Int){
    if(isInternetConnectivityAvailable(false)==false){return}
    var canContinue = false
    if isNull(lastUpdatedLocation)||isNull(lastUpdatedDate){
        canContinue = true
    }
    
    let c1 = !canContinue
    let c2 = c1 && (lastUpdatedLocation!.distance(from: location) > SIGNIFICANT_LOCATION_DIFFERENCE_THRESHOLD_METERS)
    let c3 = c1 && (lastUpdatedDate!.isEarlierThanDate(Date().dateBySubtractingMinutes(SIGNIFICANT_LOCATION_MAXIMUM_UPDATE_IN_MINUTES)))
    
    if c2 && c3{
        canContinue = true
    }

    if !canContinue && isNotNull(lastUpdatedDate) && (lastUpdatedDate!.isEarlierThanDate(Date().dateBySubtractingMinutes(SIGNIFICANT_LOCATION_MINIMUM_UPDATE_IN_MINUTES))){
        canContinue = true
    }
    
    if canContinue {
        lastUpdatedLocation = location
        lastUpdatedDate = Date()
        let information = NSMutableDictionary()
        copyData(activity , destinationDictionary: information, destinationKey: "activity", methodName: #function)
        copyData(timeStamp , destinationDictionary: information, destinationKey: "timeStamp", methodName: #function)
        copyData(safeString(location.coordinate.latitude) , destinationDictionary: information, destinationKey: "locationLatitude", methodName: #function)
        copyData(safeString(location.coordinate.longitude) , destinationDictionary: information, destinationKey: "locationLongitude", methodName: #function)
        copyData(safeString(battery) , destinationDictionary: information, destinationKey: "batteryStatus", methodName: #function)
        copyData(safeString(location.altitude), destinationDictionary: information, destinationKey: "altitude", methodName:#function)
        copyData(safeString(location.speed), destinationDictionary: information, destinationKey: "speed", methodName:#function)
        copyData(safeString(location.horizontalAccuracy), destinationDictionary: information, destinationKey: "accuracy", methodName:#function)
        let scm = ServerCommunicationManager()
        scm.responseErrorOption = ResponseErrorOption.dontShowErrorResponseMessage
        scm.updateLocation(information, completionBlock: { (responseData) -> () in
            if isNotNull(responseData){
                trackFunctionalEvent(FE_LOCATION_UPDATE_SUCCESS, information: information)
            }else{
                trackFunctionalEvent(FE_LOCATION_UPDATE_FAILED, information: information)
            }
        })
    }
}

func getGroupNameType1(group:Group)->String{
    return "\(safeBool(group.isMuted) ?"🔇":"")\(safeString(group.name).uppercased())"
}

func parseSourceForImageAndAttachEncodedImage(s:NSDictionary,skey:String,d:NSMutableDictionary,dkey:String){
    if s.object(forKey: skey) != nil {
        let image = s.object(forKey: skey) as! UIImage
        let imageData = UIImageJPEGRepresentation(image.resizeWithWidth(640),0)
        copyData(encodeStringToBase64(imageData!), destinationDictionary: d, destinationKey: dkey, methodName: #function)
    }
    copyData("jpeg", destinationDictionary: d, destinationKey: "\(dkey)_mime_type", methodName: #function)
}

func hokColor(color:UIColor)->HOKColors{
    return HOKColors(
        backGroundColor: color,
        buttonColor: color,
        cancelButtonColor: color,
        fontColor: UIColorHex(0xffffff)
    )
}

func getValidTravellersArray(_ results:NSMutableArray?) -> NSMutableArray {
    if results != nil && results!.count == 1 {
        if results![0] is NSDictionary {
            if (results![0] as! NSDictionary).allKeys.count > 1 {
                return results!
            }else{
                return NSMutableArray()
            }
        }else{
            return NSMutableArray()
        }
    }
    if results != nil && results!.count > 1{
        return results!
    }
    return NSMutableArray()
}

func openPlaces(groupId:String,navigationController:UINavigationController){
    let group = Dbm().getGroup(["groupId":groupId])
    if group != nil {
        Acf().pushVC("PlacesViewController", navigationController: navigationController, isRootViewController: false, animated: true) { (viewController) in
            (viewController as! PlacesViewController).group = group
        }
    }
}

func openGroupDetail(groupId:String,navigationController:UINavigationController){
    let group = Dbm().getGroup(["groupId":groupId])
    if group != nil {
        showGroupDetailScreen(group:group!, navigationController: navigationController)
    }
}

func openMemberDetail(memberId:String,groupId:String,navigationController:UINavigationController){
    let group = Dbm().getGroup(["groupId":groupId])
    if group != nil {
        var member:Member?
        if group?.members != nil {
            for m in group!.members! {
                if safeString((m as! Member).userId) == memberId {
                    member = m as! Member
                    break
                }
            }
        }
        if member != nil {
            showMemberDetailScreen(group: group!, member: member!, navigationController: navigationController)
        }else{
            showGroupDetailScreen(group:group!, navigationController: navigationController)
        }
    }else if isNotNull(memberId) {
        let allGroups = Dbm().getAllGroups()
        for g in allGroups {
            if g.members != nil{
                for m in g.members! {
                    if (m as! Member).userId == memberId {
                        showMemberDetailScreen(group: g, member: (m as! Member), navigationController: navigationController)
                        return
                    }
                }
            }
        }
    }
}

extension Group {
    func getGroupColor()->UIColor{
        return ThemeColor().getColor(type: safeString(self.themeColor))
    }
    func hokColor()->HOKColors{
        return HOKColors(
            backGroundColor: getGroupColor(),
            buttonColor: getGroupColor(),
            cancelButtonColor: getGroupColor(),
            fontColor: UIColorHex(0xffffff)
        )
    }
}

extension Place {
    func location()->CLLocation{
        return CLLocation(latitude: safeDouble(self.placeLatitude), longitude: safeDouble(self.placeLongitude))
    }
    func coordinate()->CLLocationCoordinate2D{
        return CLLocationCoordinate2D(latitude: safeDouble(self.placeLatitude), longitude: safeDouble(self.placeLongitude))
    }
}

extension AssociatedPlaces {
    func placeObject()->Place?{
        return Dbm().getPlace(["placeId":safeString(self.placeId)])
    }
}

func updateNotificationBadge(notificationButton:UIView?,ax:CGFloat=0,ay:CGFloat=0,groupId:String?=nil,memberId:String?=nil) {
    notificationButton?.badge.badgeValue = 0
    notificationButton?.badge.isHidden = true
    execMain ({
        if isNotNull(groupId) && isNotNull(memberId){
            notificationButton?.badge.badgeValue = Dbm().getUnreadNotificationsCount(groupId: safeString(groupId), memberId: safeString(memberId))
        }else if isNotNull(groupId){
            notificationButton?.badge.badgeValue = Dbm().getUnreadNotificationsCount(groupId: safeString(groupId))
        }else{
            notificationButton?.badge.badgeValue = Dbm().getUnreadNotificationsCount()
        }
        execMain ({
            notificationButton?.badge.isHidden = false
            if notificationButton != nil {
                let c = notificationButton!.badge.center
                notificationButton?.badge.center = CGPoint(x:c.x+ax,y:c.y+ay)
            }
            notificationButton?.badge.performAppearAnimationType6()
        },delay: 0.2)
    },delay: 0.2)
}

func handlePushNotificationsNonDRN(notification:NSDictionary){
    if let info = notification.object(forKey: "info") as? NSDictionary {
        let notiType = safeString(info.object(forKey: "notif_type"))
        let mID = safeString(info.object(forKey: "user_id"))
        let gID = safeString(info.object(forKey: "group_id"))
        let nc = Acf().navigationController
        if notiType == "carpool" {
            let rideDate = Date(timeIntervalSince1970: safeDouble(info.object(forKey: "travel_date")))
            let rideTime = Date(timeIntervalSince1970: safeDouble(info.object(forKey: "travel_etd")))
            let rideMode = (rideTime.hour() > 12) ? "1" : "0"
            Acf().openRideHomePage(rideDate,timeDate:rideTime,mode:rideMode)
        }else if notiType == "place_exit" {
            openMemberDetail(memberId:mID , groupId:gID, navigationController:nc!)
        }else if notiType == "place_entry" {
            openMemberDetail(memberId:mID , groupId:gID, navigationController:nc!)
        }else if notiType == "checkin" {
            openMemberDetail(memberId:mID , groupId:gID, navigationController:nc!)
        }else if notiType == "member_added" {
            Acf().syncGroupsAndPlaces {
                openMemberDetail(memberId:mID , groupId:gID, navigationController:nc!)
            }
        }else if notiType == "place_added" {
            Acf().syncGroupsAndPlaces {
                openPlaces(groupId:gID, navigationController:nc!)
            }
        }else if notiType == "location_request" {
            openPlaces(groupId:gID, navigationController:nc!)
        }else if notiType == "sos" {
            openMemberDetail(memberId:mID , groupId:gID, navigationController:nc!)
        }else{
            if isNotNull(gID) {
                openMemberDetail(memberId:mID , groupId:gID, navigationController:nc!)
            }
        }
    }
}

func isThisAPoMGroupName(name:String)->Bool{
    func doStringContainsExpectedCharacters( string : String) -> Bool{
        #if DEBUG
            return (string.rangeOfCharacter(from: NSCharacterSet.decimalDigits) != nil)
        #else
            return (string.rangeOfCharacter(from: NSCharacterSet.decimalDigits) != nil)&&(string.rangeOfCharacter(from: NSCharacterSet.alphanumerics) != nil)
        #endif
    }
    func isNameLengthInValid( name : String) -> Bool{
        #if DEBUG
            return (name.length < 6)
        #else
            return (name.length < 24)
        #endif
    }
    if name.contains(" "){
        return false
    }else if isNameLengthInValid(name:name) {
        return false
    }else if !doStringContainsExpectedCharacters(string: name) {
        return false
    }
    return true
}

func scanAndDeleteChatDialogsWhoesPoMGroupsGotDeleted(){
    func deleteDialog(d:QBChatDialog){
        ServicesManager.instance().chatService.deleteDialog(withID: d.id!, completion: { (response: QBResponse!) -> Void in
            if !response.isSuccess {
                logMessage(response.error?.error)
            }
        })
    }
    if Acf().isQBReadyForItsUserDependentServices() {
        let allDialogs = Acf().dialogs()
        for d in allDialogs {
            if d.type == .group &&
                isThisAPoMGroupName(name: safeString(d.name)) {
                if Dbm().getGroup(["groupId":safeString(d.name)]) == nil{
                    deleteDialog(d: d)
                }
            }
        }
    }
}

















