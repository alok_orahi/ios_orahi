//
//  LocationEventsManager.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import Foundation
import CoreLocation
import CoreMotion

typealias LEMGeofenceEvent = (_ identifier :String) ->()
typealias LEMLocationEvent = (_ location :CLLocation,_ activity:String) ->()

extension String{
    func place()->Place?{
        return Dbm().getPlace(["placeId":self])
    }
}

class PlaceEvent {
    var exitSent = false
    var entrySent = false
    var isDeleted = false
    var readyToSendExit = false
    var tForEntryFforExit = true
    var timeStamp = Date()
    var placeId : String!
    func place()->Place? {
        return Dbm().getPlace(["placeId":placeId])
    }
}

func Lem()->LocationEventsManager{
    return LocationEventsManager.sharedInstance
}

class LocationEventsManager: NSObject,CLLocationManagerDelegate{
    
    static let sharedInstance : LocationEventsManager = {
        let instance = LocationEventsManager()
        return instance
    }()
    
    fileprivate var motionActivityManager = CMMotionActivityManager()
    fileprivate var lastActivity:CMMotionActivity?
    fileprivate var moveMentStatus = "UnKnown"
    var lm = CLLocationManager()
    fileprivate var geoFenceEntryEvent : LEMGeofenceEvent?
    fileprivate var geoFenceExitEvent : LEMGeofenceEvent?
    fileprivate var locationUpdateEvent : LEMLocationEvent?
    fileprivate var placesIdsInTracking = [String]()
    fileprivate var maximumNoOfPlacesToTrack = 16
    var lastKnownAddress : String?
    var lastKnownAddressTimeStamp : Date?
    var c = [String:PlaceEvent]()
    var isFirstTime = true
    var isHighAccuracyMode = false
    
    fileprivate override init() {
    }
    
    func doRequiredProcessingPrivate(){
        locationManagerSetUp()
        if isUserLoggedIn() {
            self.process(geoEntry: { (identifier) in
                let place = Dbm().getPlace(["placeId":identifier])
                if isNotNull(place) && self.shouldIgnoreThisEvent(place: place!, tForEntryFForExit: true) == false {
                    self.generateLocationEventForPlace(placeId: identifier, isEntry: true)
                    logMessageWithLocalNotification(message:"\(safeString(place!.name)) : Entry")
                }
            }, geoExit: { (identifier) in
                let place = Dbm().getPlace(["placeId":identifier])
                if isNotNull(place) && self.shouldIgnoreThisEvent(place: place!, tForEntryFForExit: false) == false {
                    self.generateLocationEventForPlace(placeId: identifier, isEntry: false)
                    logMessageWithLocalNotification(message:"\(safeString(place!.name)) : Exit")
                }
            }, currentLocation: { (location, activity) in
                updateSignificantLocationToServer(location: location, activity: activity, timeStamp: safeString(currentTimeStamp()), battery: batteryLevel)
            })
            self.checkAndProcessEventsMannualy()
        }
    }
    
    func doRequiredProcessing(){
        doRequiredProcessingPrivate()
    }
    
    fileprivate func process(geoEntry:@escaping LEMGeofenceEvent,geoExit:@escaping LEMGeofenceEvent,currentLocation:@escaping LEMLocationEvent){
        self.geoFenceEntryEvent = geoEntry
        self.geoFenceExitEvent = geoExit
        self.locationUpdateEvent = currentLocation
        self.placesIdsInTracking = decidePlacesIdsToTrack()
        if isHighAccuracyMode == false {
            startUpdatingSignificantLocationChanges()
        }
        updateRegionsToTrack()
        forceLocationUpdateOnce()
    }
    
}

extension LocationEventsManager /*HELPERS*/{
    
    fileprivate func updateRegionsToTrack(){
        let existingRegions = lm.monitoredRegions
        var existingPlaces = [String]()
        for r in existingRegions {
            if self.placesIdsInTracking.contains(r.identifier) == false {
                lm.stopMonitoring(for: r)
            }else{
                existingPlaces.append(r.identifier)
            }
        }
        for pId in self.placesIdsInTracking {
            if existingPlaces.contains(pId) {
                //no need to add again
            }else{
                // lets add it now
                if pId.place() != nil {
                    monitorRegion(coordinate(place: pId.place()!), identifier: safeString(pId), regionRadius: PLACES_TRACKING_REGION_RADIUS)
                }
            }
        }
    }
    
    func generateLocationEventForPlace(placeId:String,isEntry:Bool){
        let altitude = safeString(self.lm.location?.altitude)
        let speed = safeString(self.lm.location?.speed)
        let accuracy = safeString(self.lm.location?.horizontalAccuracy)
        createServerNotificationForPoMEntryExitEvent(placeId:placeId, isEntry: isEntry,altitude:altitude,speed:speed,accuracy:accuracy)
    }
    
    fileprivate func decidePlacesIdsToTrack()->[String]{
        var placesToTrack = [Place]()
        let places = Dbm().getAllPlaces()
        let groups = Dbm().getAllGroups()
        for p in places {
            for g in groups {
                var isLocationSharingEnabled = false
                if g.associatedPlaces != nil {
                    for ap in g.associatedPlaces! {
                        let apo = ap as! AssociatedPlaces
                        if safeString(apo.placeId) == safeString(p.placeId) {
                            isLocationSharingEnabled = (apo.entrySharingEnabled || apo.exitSharingEnabled)
                            break
                        }
                    }
                }
                if isLocationSharingEnabled {
                    placesToTrack.append(p)
                }
            }
        }
        
        func returnPlaceIds(places:[Place])->[String]{
            var placeIds = [String]()
            for p in places {
                placeIds.append(safeString(p.placeId))
            }
            return placeIds
        }
        
        if placesToTrack.count > maximumNoOfPlacesToTrack  {
            var sortedPlacesToTrack = [Place]()
            if self.lm.location != nil {
                let clc = self.lm.location!
                sortedPlacesToTrack =  placesToTrack.sorted(by: { (p1, p2) -> Bool in
                    return clc.distance(from: p1.location()) < clc.distance(from: p2.location())
                })
                sortedPlacesToTrack.removeLast(sortedPlacesToTrack.count-maximumNoOfPlacesToTrack)
            }
            return returnPlaceIds(places: sortedPlacesToTrack)
        }
        return returnPlaceIds(places: placesToTrack)
    }
    
    func geoCodeCurrentLocation(){
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(LocationEventsManager.geoCodeCurrentLocationPrivate), object: nil)
        self.perform(#selector(LocationEventsManager.geoCodeCurrentLocationPrivate), with: nil, afterDelay: 10)
    }
    
    func geoCodeCurrentLocationPrivate(){
        if isNotNull(lm.location){
            weak var weakSelf = self
            SwiftLocation.shared.reverseCoordinates(.googleMaps, coordinates: lm.location!.coordinate, onSuccess: { (placemark) in
                if let lines: Array<String> = placemark?.addressDictionary?["FormattedAddressLines"] as? Array<String> {
                    let placeString = lines.joined(separator: ", ")
                    weakSelf?.lastKnownAddressTimeStamp = Date()
                    weakSelf?.lastKnownAddress = placeString
                }
            }) { (error) in
            }
        }
    }
}

extension LocationEventsManager /*EVENT FILTERING AND OPTIMISATIONS*/{
    
    func shouldIgnoreThisEvent(place:Place,tForEntryFForExit:Bool)->Bool{
        let eventKey = safeString(place.placeId)
        func createPlaceState(){
            let event = PlaceEvent()
            event.tForEntryFforExit = tForEntryFForExit
            event.placeId = safeString(place.placeId)
            event.entrySent = true
            c[eventKey] = event
        }
        if c[eventKey] == nil {
            if tForEntryFForExit && (displacement(place:place,checkingForEntryCase: true) < DELAYED_ENTRY_ALLOWED_MAXIMUM_DISTANCE) {
                createPlaceState()
                let message = "Entry Event : \(safeString(place.name))'s Place : CREATED."
                showPoMSpecialDebuggingMessage(message: message)
                return false
            }
        }else{
            if tForEntryFForExit == false {
                let e = c[eventKey]!
                if e.readyToSendExit {
                    e.exitSent = true
                    self.c.removeValue(forKey: eventKey)
                    let message = "Exit Event : \(safeString(e.place()?.name))'s Place : CREATED"
                    showPoMSpecialDebuggingMessage(message: message)
                    return false
                }else if shouldSendExitEvent(e: e) {
                    let message = "Exit Event : \(safeString(e.place()?.name)))'s Place : CREATED"
                    showPoMSpecialDebuggingMessage(message: message)
                    self.c.removeValue(forKey: eventKey)
                    return false
                }else if e.timeStamp.isEarlierThanDate(Date().dateBySubtractingHours(12)) {
                    if tForEntryFForExit && (displacement(place:place,checkingForEntryCase: false) < PLACES_TRACKING_REGION_RADIUS){
                        createPlaceState()
                        let message = "Entry Event : \(safeString(place.name)) Place : OLD DATA DISCARDED : CREATED NEW"
                        showPoMSpecialDebuggingMessage(message: message)
                        return false
                    }
                    return false
                }
            }
        }
        return true
    }
    
    func shouldSendExitEvent(e:PlaceEvent)->Bool{
        if e.entrySent && e.exitSent == false && e.isDeleted == false && displacement(place: e.place(),checkingForEntryCase: false) > PLACES_EXIT_REGION_RADIUS_THRESHOLD {
            e.isDeleted = true
            e.readyToSendExit = true
            return true
        }
        return false
    }
    
    func checkAndProcessEventsMannualy(){
        for pId in placesIdsInTracking {
            if lm.location != nil {//make sure we know the current location
                let eventKey = safeString(pId)
                if c[eventKey] == nil {//make sure it doesn't contains existing event
                    if (displacement(place:pId.place(),checkingForEntryCase: true) < PLACES_TRACKING_REGION_RADIUS) {//check if we are in range with place location.
                        if isFirstTime == false {
                            let message = "Entry Event : \(safeString(pId.place()?.name)) Place : TRYING TO ADD MANUALLY WITH NOTIFICATION."
                            showPoMSpecialDebuggingMessage(message: message)
                            geoFenceEntryEvent?(safeString(pId))
                        }else{
                            let event = PlaceEvent()
                            event.tForEntryFforExit = true
                            event.placeId = pId
                            event.entrySent = true
                            c[eventKey] = event
                            let message = "Entry Event : \(safeString(pId.place()?.name)) Place : MANUALLY ADDING WITHOUT NOTIFICATION."
                            showPoMSpecialDebuggingMessage(message: message)
                        }
                    }
                }
            }
        }
        isFirstTime = false
        for e in c.values {
            if shouldSendExitEvent(e: e) {
                geoFenceExitEvent?(safeString(e.place()?.placeId))
            }
        }
    }
    
    func displacement(place:Place?,checkingForEntryCase:Bool)->Double{
        if place != nil && lm.location != nil{
            return place!.location().distance(from: lm.location!)
        }else{
            if checkingForEntryCase {
                return 10240
            }else{
                return 0
            }
        }
    }
}


extension LocationEventsManager /*SETUP*/{
    
    fileprivate func locationManagerSetUp(){
        lm.requestAlwaysAuthorization()
        lm.delegate = self
    }
    
    func startLocationUpdateWithHighAccuracyForFewSeconds(){
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(LocationEventsManager.startUpdatingAlways), object: nil)
        self.perform(#selector(LocationEventsManager.startUpdatingAlways), with: nil, afterDelay: 0)
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(LocationEventsManager.startUpdatingSignificantLocationChanges), object: nil)
        self.perform(#selector(LocationEventsManager.startUpdatingSignificantLocationChanges), with: nil, afterDelay: EXTRA_TIME_TO_HOLD_APP_EXECUTION_TO_PERFORM_IMPORTANT_EVENTS)
    }
    
    fileprivate func startActivityMonitor(){
        if CMMotionActivityManager.isActivityAvailable() {
            motionActivityManager.startActivityUpdates(to: OperationQueue.main) {[weak self]
                activityData in guard let `self` = self else { return }
                DispatchQueue.main.asyncAfter(deadline:.now()) {
                    if activityData != nil{
                        self.lastActivity = activityData
                    }
                }
            }
        }
    }
    
    @objc fileprivate func startUpdatingAlways(){
        isHighAccuracyMode = true
        lm.stopUpdatingLocation()
        lm.stopMonitoringSignificantLocationChanges()
        lm.allowsBackgroundLocationUpdates = true
        lm.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        lm.startUpdatingLocation()
        startActivityMonitor()
    }
    
    @objc fileprivate func startUpdatingSignificantLocationChanges(){
        isHighAccuracyMode = false
        lm.stopUpdatingLocation()
        lm.allowsBackgroundLocationUpdates = true
        lm.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        lm.startMonitoringSignificantLocationChanges()
        startActivityMonitor()
    }
    
    @objc fileprivate func forceLocationUpdateOnce(){
        if locationUpdateEvent != nil && lm.location != nil {
            locationUpdateEvent?(lm.location!,moveMentStatus.uppercased())
        }
    }
}

extension LocationEventsManager /*DELEGATES AND CALLBACKS*/{
    
    fileprivate func monitorRegion(_ location : CLLocationCoordinate2D, identifier : String, regionRadius: Double) {
        lm.startMonitoring(for: CLCircularRegion(center: location, radius: regionRadius, identifier: identifier))
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        geoCodeCurrentLocation()
        self.checkAndProcessEventsMannualy()
        if locationUpdateEvent != nil {
            if lastActivity != nil {
                if lastActivity!.cycling || lastActivity!.running || lastActivity!.automotive || lastActivity!.walking {
                    if moveMentStatus == "UnKnown" {
                        moveMentStatus = "Started"
                    }else if moveMentStatus == "Still" {
                        moveMentStatus = "Started"
                    }else if moveMentStatus == "Started" {
                        moveMentStatus = "Moving"
                    }else if moveMentStatus == "Moving" {
                        moveMentStatus = "Moving"
                    }else if moveMentStatus == "Stopped" {
                        moveMentStatus = "Started"
                    }
                }else if lastActivity!.stationary {
                    if moveMentStatus == "UnKnown" {
                        moveMentStatus = "Still"
                    }else if moveMentStatus == "Still" {
                        moveMentStatus = "Still"
                    }else if moveMentStatus == "Started" {
                        moveMentStatus = "Stopped"
                    }else if moveMentStatus == "Moving" {
                        moveMentStatus = "Stopped"
                    }else if moveMentStatus == "Stopped" {
                        moveMentStatus = "Still"
                    }
                }else if lastActivity!.unknown {
                    moveMentStatus = "UnKnown"
                }
            }else{
                moveMentStatus = "UnKnown"
            }
            locationUpdateEvent?(locations.last!,moveMentStatus.uppercased())
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        if geoFenceExitEvent != nil {
            startLocationUpdateWithHighAccuracyForFewSeconds()
            execMain({
                self.checkAndProcessEventsMannualy()
                self.geoFenceExitEvent?(region.identifier)
            }, delay: 1.0)
        }
        let message = "didExitRegion \n REGION : \(region.identifier)"
        showPoMSpecialDebuggingMessage(message: message)
    }
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        if geoFenceEntryEvent != nil {
            startLocationUpdateWithHighAccuracyForFewSeconds()
            execMain({
                self.checkAndProcessEventsMannualy()
                self.geoFenceEntryEvent?(region.identifier)
            }, delay: 5.0)
        }
        let message = "didEnterRegion \n REGION : \(region.identifier)"
        showPoMSpecialDebuggingMessage(message: message)
    }
    
    func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error){
        let message = "monitoringDidFailFor \n REGION : \(region?.identifier) \n ERROR : \(error.localizedDescription)"
        showPoMSpecialDebuggingMessage(message: message)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus){
        
    }
    
    func locationManager(_ manager: CLLocationManager, didStartMonitoringFor region: CLRegion){
        let message = "didStartMonitoringFor \(region.identifier)"
    }
}

func showPoMSpecialDebuggingMessage(message:String){
    if ENABLE_POM_LOCATION_SERVICE_DEBUG_INFO {
        logMessageWithLocalNotification(message: message)
    }
}
