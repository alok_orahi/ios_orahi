//
//  UserTrackingManager.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import Foundation
import UIKit

class UserTrackingManager: NSObject , CLLocationManagerDelegate{

    let locationManager = CLLocationManager()
    let timeToTrackInMorning = Date()
    let timeToTrackInEvening = Date()
    var isTracking = false
    var currentLocationCoordinate : CLLocationCoordinate2D?

    static let sharedInstance : UserTrackingManager = {
        let instance = UserTrackingManager()
        return instance
    }()
    
    fileprivate override init() {
        
    }
    
    func askForRequiredPermissionsForTracking(){
        locationManager.requestAlwaysAuthorization()
    }
    
    func startTrackingProcessIfNeeded() {
        if canTrackNow() {
            if isTracking == false {
                if !CLLocationManager.isMonitoringAvailable(for: CLCircularRegion.self) {
                    logMessage("monitoring not available , hence ride tracking will not work , returning from here")
                    return
                }
                stopTrackingRegions()
                locationManager.delegate = self
                locationManager.requestAlwaysAuthorization()
                self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                self.locationManager.allowsBackgroundLocationUpdates = true
                self.locationManager.startMonitoringSignificantLocationChanges()
                startTrackingRegions()
                checkAndUpdateToServer()
            }
        }else{
            stopTrackingRegions()
        }
    }
    
    func stopTrackingRegions() {
        for r in locationManager.monitoredRegions {
            locationManager.stopMonitoring(for: r)
        }
        locationManager.stopUpdatingLocation()
        isTracking = false
    }
    
    func startTrackingRegions() {
        trackCurrentUser()
        isTracking = true
    }
    
    func canTrackNow()->Bool{
        if !isSystemReadyToProcessThis() { return false }
        let settings = Dbm().getSetting()
        let hod = settings.homeTime!.dateValue()
        let ohd = settings.destinationTime!.dateValue()
        let thod = Date().setTimeOfDate(hod.hour(), minute: hod.minute(), second: hod.seconds())
        let tohd = Date().setTimeOfDate(ohd.hour(), minute: ohd.minute(), second: ohd.seconds())
        if Date().isLaterThanDate(thod.dateBySubtractingHours(1)) && Date().isEarlierThanDate(thod.dateByAddingHours(1)){
            return true
        }
        if Date().isLaterThanDate(tohd.dateBySubtractingHours(1)) && Date().isEarlierThanDate(tohd.dateByAddingHours(1)){
            return true
        }
        return false
    }
    
    func checkAndUpdateToServer(){
        if (isInternetConnectivityAvailable(false)==false){return}
        let trackedEvents = Dbm().getTrackedEvents() as! [TrackedEvents]
        if trackedEvents.count > 0{
            let information = NSMutableDictionary()
            let eventsArray = NSMutableArray()
            for te in trackedEvents{
                let event = NSMutableDictionary()
                event.setObject(te.tForHomeFforDestination!.boolValue ? "home" : "\(destinationName())" , forKey: "region" as NSCopying)
                event.setObject(te.tForEnterFforExit!.boolValue ? "1" : "0" , forKey: "entered" as NSCopying)
                event.setObject(te.date!.stringValue(), forKey: "date" as NSCopying)
                eventsArray.add(event)
            }
            information.setObject(eventsArray, forKey: "trackerInfo" as NSCopying)
            let scm = ServerCommunicationManager()
            scm.responseErrorOption = .dontShowErrorResponseMessage
            scm.updateUserTrackingDetails(information) { (responseData) -> () in
                Dbm().removeTrackedEvents()
            }
        }
    }

    func trackCurrentUser() {
        let homeCoordinate = homeLocationCoordinate()
        let destinationCoordinate = destinationLocationCoordinate()
        let homeRegion = CLCircularRegion(center: homeCoordinate, radius: TRACKING_REGION_RADIUS, identifier: "homeRegion")
        let destinationRegion = CLCircularRegion(center: destinationCoordinate, radius: TRACKING_REGION_RADIUS, identifier: "destinationRegion")
        startMonitoringRegion(homeRegion)
        startMonitoringRegion(destinationRegion)
    }
    
    func startMonitoringRegion(_ region:CLRegion) {
        locationManager.startMonitoring(for: region)
    }
    
    func onRegion(_ entered:Bool,identifier:String) {
        let tForHomeFforDestination = identifier.contains("home")
        let tForEnterFforExit = entered
        let information = ["tForHomeFforDestination":NSNumber(value: tForHomeFforDestination as Bool),"tForEnterFforExit":NSNumber(value: tForEnterFforExit as Bool),"date":Date()] as [String : Any]
        Dbm().addTrackedEvents(information as NSDictionary)
    }
    
    //MARK: - CLLocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        execMain({[weak self]  in guard let `self` = self else { return }
            if region is CLCircularRegion {
                self.onRegion(true,identifier: region.identifier)
            }
            logMessage(#function)
            },delay: DELAY_IN_NOTIFYING_REGION_ENTRY)
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        execMain({[weak self]  in guard let `self` = self else { return }
            if region is CLCircularRegion {
                self.onRegion(false,identifier: region.identifier)
            }
            logMessage(#function)
            },delay: DELAY_IN_NOTIFYING_REGION_EXIT)
    }
    
    func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
        logMessage(#function)
    }
    
    func locationManager(_ manager: CLLocationManager, didStartMonitoringFor region: CLRegion) {
        logMessage(#function)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if locations.count > 0 {
            currentLocationCoordinate = locations.last!.coordinate
        }
    }
}
