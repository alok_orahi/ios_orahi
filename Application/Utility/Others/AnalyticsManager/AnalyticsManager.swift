//
//  AnalyticsManager.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit
import Branch
import FacebookCore
import FBSDKCoreKit

/**
 Screen launch events
 */

let SLE_WALKTHROUGH = "SCREEN_GET_STARTED"
let SLE_SIGN_IN = "SCREEN_SIGN_IN"
let SLE_SIGN_UP = "SCREEN_SIGN_UP"
let SLE_PHONE_VERIFICATION = "SCREEN_OTP_SCREEN"
let SLE_RIDE_HOME = "SCREEN_RIDE_SCREEN"
let SLE_RIDE_DETAIL = "SCREEN_RIDE_SCREEN"
let SLE_RECENT_CHAT = "SCREEN_CHAT_LISTING"
let SLE_NEW_CHAT = "NEW_CHAT_SELECT_USER"
let SLE_CHAT = "SCREEN_CHAT_SCREEN"
let SLE_DESTINATIONAL_RANKING = "SCREEN_COMPANY_RANKING"
let SLE_DIRECTIONAL_RANKING = "SCREEN_DIRECTIONAL_RANKING"
let SLE_OFFERS = "SCREEN_OFFERS"
let SLE_WEEKLY_PLANNER = "SCREEN_WEEKLY_VIEW"
let SLE_VERIFICATION_OPTION = "SCREEN_VERIFICATION"
let SLE_BASIC_PROFILE_DETAILS = "SCREEN_PERSONAL_DETAILS"
let SLE_SELECT_USER_MODE = "SCREEN_TRAVEL_MODE"
let SLE_SELECT_USER_TYPE = "SCREEN_USER_TYPE_DIALOG"
let SLE_ENTER_CAR_DETAILS = "SCREEN_VEHICLE_DETAILS"
let SLE_EDIT_PROFILE_OTHER_OPTIONS = "SCREEN_EDIT_PROFILE"
let SLE_FEEDBACK = "SCREEN_FEEDBACK_DIALOG"
let SLE_ACCOUNT_SUMMARY = "SCREEN_ACCOUNT_SUMMARY"
let SLE_WITHDRAWL = "SCREEN_WITHDRAWAL"
let SLE_RATE_US_SCREEN = "SCREEN_RATE_APP_DIALOG"
let SLE_RECHARGE = "SCREEN_RECHARGE"
let SLE_SUPPORT = "SCREEN_SUPPORT"
let SLE_SEARCH_ORAHI_USERS = "SCREEN_SEARCH_ORAHI_USER_SCREEN"
let SLE_STATUS_EDITOR = "SCREEN_STATUS_DIALOG"
let SLE_SELF_PROFILE = "SCREEN_PROFILE"
let SLE_OTHERS_PROFILE = "PROFILE_OTHERS"
let SLE_NOTIFICATIONS = "SCREEN_NOTIFICATIONS_LIST"
let SLE_ADD_UPDATE_CHAT_GROUP_MEMBERS = "SCREEN_ADD_USERS_CHAT"
let SLE_SETTINGS = "SCREEN_SETTINGS"
let SLE_TIME_AND_ADDRESS_PICKER = "SCREEN_LOCATION_DETAILS_DIALOG"
let SLE_FAVOURITES = "SCREEN_FAVOURITE"
let SLE_BLOCKED_USERS = "SCREEN_BLOCKED"
let SLE_SELECT_TRAVEL_MODE = "SCREEN_TRAVEL_MODE"
let SLE_CONTACT_US = "SCREEN_CONTACT_US"
let SLE_COMPANY_SELECTION_SCREEN = "SCREEN_COMPANY_PICKER_DIALOG"
let SLE_COLLEGE_SELECTION_SCREEN = "SCREEN_COLLEGE_PICKER_DIALOG"
let SLE_TERMS_AND_CONDITIONS = "SCREEN_TNC"
let SLE_ABOUT_US = "SCREEN_ABOUT"
let SLE_FAQ = "SCREEN_FAQS"
let SLE_CHAT_INFORMATION = "SCREEN_GROUP_INFO"
let SLE_PROFILE_PICTURE_PICKER = "SCREEN_IMAGE_SELECTOR"
let SLE_SETTLE_RIDE = "SCREEN_SETTLE_RIDE_DIALOG"
let SLE_ORAHI_ON_FACEBOOK = "SCREEN_ORAHI_ON_FACEBOOK"
let SLE_ORAHI_ON_TWITTER = "SCREEN_ORAHI_ON_TWITTER"
let SLE_ORAHI_ON_LINKEDIN = "SCREEN_ORAHI_ON_LINKEDIN"
let SLE_ORAHI_ON_YOUTUBE = "SCREEN_ORAHI_ON_YOUTUBE"

let SLE_D_RIDE_QUEST = "DRN_QUEST"
let SLE_D_RIDE_REQUEST = "DRN_REQUEST"
let SLE_D_RIDE_SETTLE = "DRN_SETTLE"
let SLE_D_RIDE_CONFIRMATION = "DRN_CONFIRMATION"
let SLE_D_RIDE_COORDINATION = "DRN_COORDINATION"
let SLE_D_RIDE_FEEDBACK = "DRN_FEEDBACK"
let SLE_D_RIDE_APOLOGY = "DRN_APOLOGY"
let SLE_D_RIDE_BROKEN = "DRN_BROKEN"
let SLE_D_RIDE_ALREADY_CONFIRMED = "DRN_ALREADY_CONFIRMED"
let SLE_D_RIDE_SUGGESTION = "DRN_SUGGESTION"
let SLE_D_RIDE_SEND_INVITE = "DRN_SEND_INVITE"
let SLE_D_GENERIC_DRN = "DRN_GENERIC"
let SLE_D_READONLY_NOTIFICATION_DRN = "DRN_READONLY_NOTIFICATION"

///**
// Functional events
// */
let FE_GET_STARTED = "GET_STARTED"
let FE_SIGN_OUT = "LOGOUT"
let FE_SIGNUP_MOBILE_ENTERED = "REG_MOB_ENTERED"
let FE_SIGNIN_MOBILE_ENTERED = "LOGIN_MOB_ENTERED"
let FE_SEND_OTP = "SEND_OTP"
let FE_RESEND_OTP = "RESEND_OTP"
let FE_OTP_VERIFICATION = "OTP_VERIFICATION"
let FE_USER_LOGIN = "USER_LOGIN"
let FE_USER_SIGNUP = "USER_SIGNUP"
let FE_PERSONAL_DETAILS_SAVED = "PERSONAL_DETAILS_SAVED"
let FE_USER_TYPE_STUDENT_SELECTED = "CATEGORY_SELECTED_STUDENT"
let FE_USER_TYPE_CORPORATE_SELECTED = "CATEGORY_SELECTED_CORPORATE"
let FE_USER_TYPE_OTHER_SELECTED = "CATEGORY_SELECTED_OTHER"
let FE_MODE_CAR_OWNER_SELECTED = "MODE_SELECTED_CAR_OWNER"
let FE_MODE_PASSENGER_SELECTED = "MODE_SELECTED_PASSENGER"
let FE_FIX_MY_RIDE = "FIX_MY_RIDE"
let FE_OFFER_REDEEMED = "OFFER_REDEEMED"
let FE_PROFILE_UPDATE = "PROFILE_SAVE"
let FE_MANUAL_REFRESH_TRAVELLERS = "RIDE_REFRESH"
let FE_SEND_INVITE = "SEND_INVITE"
let FE_INVITE_RECEIVED = "INVITE_RECEIVED"
let FE_ACCEPT_RIDE_INVITE = "ACCEPT_INVITE"
let FE_REJECT_RIDE_INVITE = "REJECT_INVITE"
let FE_SETTLE_RIDE = "SETTLE_PAYMENT"
let FE_START_CHAT = "CHAT"
let FE_VERIFICATION_EMAIL = "EMAIL_VERIFICATION"
let FE_VERIFICATION_ID_PROOF = "GOVT_ID_VERIFICATION"
let FE_BALANCE = "BALANCE"
let FE_LOW_BALANCE = "BAL_LOW"
let FE_SETTINGS_NOTIFICATIONS_SMS = "NOTIF_SMS"
let FE_SETTINGS_NOTIFICATIONS_EMAIL = "NOTIF_MAIL"
let FE_SETTINGS_NOTIFICATIONS_MOBILE = "NOTIF_MOB_APP"
let FE_SETTINGS_NOTIFICATIONS_WEBSITE = "NOTIF_WEB"
let FE_FEEDBACK = "FEEDBACK"
let FE_UPDATE_PROFILE_PICTURE = "IMAGE_UPLOAD"
let FE_REFFERAL_CODE_SHARED = "REFFERAL_CODE_SHARED"
let FE_BLOCK_USER = "BLOCK"
let FE_FAVOURITE_USERS_RE_ARRANGED = "DRAG_IMAGE"
let FE_UPDATE_FAVOURITES_TO_SERVER = "UPDATE_FAV"
let FE_TIME_AND_ADDRESS_PICKER_CLOSED = "LOCATION_POP_UP_CLOSED"
let FE_TIME_AND_ADDRESS_PICKER_HOME_SUGGESTION_SELECTED = "LOCATION_HOME_SUGGESTION_SELECTED"
let FE_TIME_AND_ADDRESS_PICKER_DESTINATION_SUGGESTION_SELECTED = "LOCATION_OFFICE_SUGGESTION_SELECTED"
let FE_APP_LAUNCH_PRE_REG = "APP_LAUNCH_PRE_REG"
let FE_APP_LAUNCH_POST_REG = "APP_LAUNCH_POST_REG"
let FE_CHANGE_MODE = "CHANGE_MODE"
let FE_CHANGE_DATE_TIME = "CHANGE_DATE_TIME"
let FE_VIEW_DIRECTIONAL_RANKING_OVERALL = "DIR_RANK_OVERALL"
let FE_VIEW_DIRECTIONAL_RANKING_COMPANY = "DIR_RANK_COMPANY"
let FE_LOC_CHANGE = "LOC_CHANGE"
let FE_MENU_OPENED = "MENU_OPENED"
let FE_RECHARGE_AMOUNT = "RECHARGE_AMOUNT"
let FE_TRAVEL_PREFFERENCE = "TRAVEL_PREFFERENCE"
let FE_SCREEN_DATA_LOADED = "SCREEN_DATA_LOADED"
let FE_SCREEN_DATA_NOT_LOADED = "SCREEN_DATA_NOT_LOADED"
let FE_SIGNUP_PROCESS_RESUMED = "USER_BACK_AFTER_SIGNUP"
let FE_USER_ONBOARD_SMOOTH = "USER_ONBOARD_SMOOTH"
let FE_LOCATIONS_DETAILS_CAPTURED = "LOCATIONS_DETAILS_CAPTURED"

let FE_D_RECEIVED = "DRN_RECEIVED"
let FE_D_VISIBLE = "DRN_VISIBLE"
let FE_D_CLICKED = "DRN_CLICKED"
let FE_D_RECHARGE = "DRN_RECHARGE"
let FE_D_CALL                           = "DRN_CALL"
let FE_D_SMS                            = "DRN_SMS"
let FE_D_CHAT                           = "DRN_CHAT"
let FE_D_FEEDBACK                       = "DRN_FEEDBACK"
let FE_CREATE_NOTIFICATIONS_SUCCESS     = "CREATE_NOTIFICATIONS_SUCCESS"
let FE_CREATE_NOTIFICATIONS_FAILED      = "CREATE_NOTIFICATIONS_FAILED"
let FE_LOCATION_UPDATE_SUCCESS          = "FE_LOCATION_UPDATE_SUCCESS"
let FE_LOCATION_UPDATE_FAILED           = "FE_LOCATION_UPDATE_FAILED"

let KE_CARPOOL_OTP_VALIDATION_COMPLETED = "otp_completed_carpool"
let KE_CARPOOL_SIGNUP_COMPLETED         = "registration_completed_carpool"
let KE_POM_OTP_VALIDATION_COMPLETED     = "otp_completed_pom"
let KE_POM_SIGNUP_COMPLETED             = "registration_completed_pom"

var isAnalyticsReady = false

func setupForAnalytics(){
    FBSDKAppEvents.setUserID(loggedInUserId())
    var trackerParametersDictionary: [AnyHashable: Any] = [:]
    trackerParametersDictionary[kKVAParamAppGUIDStringKey] = KOCHOVA_GUID_STRING
    KochavaTracker.shared.configure(withParametersDictionary: trackerParametersDictionary, delegate: nil)
    isAnalyticsReady = true
}

func trackScreenLaunchEvent(_ name: String,isSuccess:Bool = true) {
    if isAnalyticsReady {
        let qualityOfServiceClass = DispatchQoS.QoSClass.background
        let backgroundQueue = DispatchQueue.global(qos: qualityOfServiceClass)
        backgroundQueue.async(execute: {
            let otherParameters = NSMutableDictionary()
            otherParameters.setObject("screen_view_event", forKey: "event_type" as NSCopying)
            if isSuccess {
                otherParameters.setObject("true", forKey: "success" as NSCopying)
            }else{
                otherParameters.setObject("false", forKey: "success" as NSCopying)
            }
            addCommonParameters(otherParameters)
            AppEventsLogger.log(name, parameters: getParameterInFormatType1(parameter: otherParameters as! [String:NSObject]), valueToSum: nil, accessToken: nil)
        })
    }
}

func trackFunctionalEvent(_ name: String,information:NSDictionary?,isSuccess:Bool = true) {
    if isAnalyticsReady {
        let qualityOfServiceClass = DispatchQoS.QoSClass.background
        let backgroundQueue = DispatchQueue.global(qos: qualityOfServiceClass)
        backgroundQueue.async(execute: {
            let otherParameters = NSMutableDictionary()
            otherParameters.setObject("action_event", forKey: "event_type" as NSCopying)
            if isSuccess {
                otherParameters.setObject("true", forKey: "success" as NSCopying)
            }else{
                otherParameters.setObject("false", forKey: "success" as NSCopying)
            }
            if let parameters = information as NSDictionary? {
                let allKeys = parameters.allKeys
                for key in allKeys {
                    otherParameters.setObject("\(parameters.object(forKey: key)!)", forKey: "\(key)" as NSCopying)
                }
            }
            addCommonParameters(otherParameters)
            AppEventsLogger.log(name, parameters: getParameterInFormatType1(parameter: otherParameters as! [String:NSObject]), valueToSum: nil, accessToken: nil)
        })
    }
}

func trackKochavaEvent(_ name: String) {
    let ad = "\("iOSV\(UIApplication.appVersion())B\(UIApplication.appBuild()))P")"
    let ud = loggedInUserId()
    KochavaTracker.shared.sendEvent(withNameString: name, infoString:"\(ud)_\(ad)")
}

func addCommonParameters(_ otherParameters:NSMutableDictionary){
    otherParameters.setObject(loggedInUserId(), forKey: "user_id" as NSCopying)
    otherParameters.setObject("\(Date().toString(DateFormat.custom("dd/MM/yy")))", forKey: "date" as NSCopying)
    otherParameters.setObject("\(Date().toString(DateFormat.custom("HH:mm")))", forKey: "time" as NSCopying)
    otherParameters.setObject("\(Date().stringValue())", forKey: "date_time" as NSCopying)
    if Acf().rideHomeVC != nil {
        otherParameters.setObject(Acf().rideHomeVC!.trueForOwnerFalseforPassenger ? "car_owner" : "passenger" , forKey: "travel_mode" as NSCopying)
        otherParameters.setObject(Acf().rideHomeVC!.trueForHomeToDestinationFalseforDestinationToHome ? "home_to_destination" : "destination_to_home" , forKey: "travel_status" as NSCopying)
    }
    otherParameters.setObject("\(isInForeground() ? "foreground" : "background" )", forKey: "application_state" as NSCopying)
    otherParameters.setObject(userType(), forKey: "user_type" as NSCopying)
    #if DEBUG
        otherParameters.setObject("\("iOSV\(UIApplication.appVersion())B\(UIApplication.appBuild())D")", forKey: "developer_info" as NSCopying)
    #else
        otherParameters.setObject("\("iOSV\(UIApplication.appVersion())B\(UIApplication.appBuild()))P")", forKey: "developer_info" as NSCopying)
    #endif
    otherParameters.removeObject(forKey: "regid")
}

func checkAndReportCampaignInstallEventIfRequired(){
    if let parameters = Branch.getInstance().getFirstReferringParams(){
        let key = "CampaignInstallEvent"
        if let campaignName = parameters["cmp_name"] as? String{
            if let _ = CacheManager.sharedInstance.loadObject(key){
                //it seems its already reported , lets ignore this time
            }else{
                execMain({
                    trackFunctionalEvent("\(campaignName)_install", information: nil)
                },delay:5)
                CacheManager.sharedInstance.saveObject("TRUE" , identifier: key)
            }
        }
    }
}

func checkAndReportCampaignRegisterEventIfRequired(){
    if let parameters = Branch.getInstance().getFirstReferringParams(){
        let key = "CampaignRegisterEvent"
        if let campaignName = parameters["cmp_name"] as? String{
            if let _ = CacheManager.sharedInstance.loadObject(key){
                //it seems its already reported , lets ignore this time
            }else{
                execBackground({
                    trackFunctionalEvent("\(campaignName)_reg", information: nil)
                })
                CacheManager.sharedInstance.saveObject("TRUE" , identifier: key)
            }
        }
    }
}

func getParameterInFormatType1(parameter:[String:NSObject])->[AppEventParameterName : AppEventParameterValueType]{
    let dictionaryRepresentation = parameter as NSDictionary
    var valuesAgain = [AppEventParameterName : String]()
    dictionaryRepresentation.enumerateKeysAndObjects({ (key, value, stop) in
        valuesAgain.updateValue(value as! String, forKey: AppEventParameterName(key as! String))
    })
    return valuesAgain
}

