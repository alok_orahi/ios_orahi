//
//  DeepLinkingManager.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit
import CTFeedback

enum DeepLinkingType {
    case viewScreen
    case performAction
    case popup
    case reOccuringPopup
}

enum ScreenType {
    case profile
    case editProfile
    case rideHome
    case recentChats
    case offers
    case ranking
    case chat
    case statusEditor
    case verification
    case favourites
    case payments
    case recharge
    case withdraw
    case settings
    case support
    case faq
    case feedback
    case contactUs
    case search
    case aboutUs
    case rateUs
    case termsAndConditions
}

enum ActionType {
    case loadListing
    case none
}

enum ScreenPresentation {
    case fullScreen
    case popup
}

class DeepLinkingManager: NSObject {
    
    static let sharedInstance : DeepLinkingManager = {
        let instance = DeepLinkingManager()
        return instance
    }()
    
    open func handleDeeplinkUrl(url:URL)->Bool{
        let handling = canWeHandle(url: url)
        if handling {
            self.prepare()
            execMain({[weak self] in guard let `self` = self else { return }

                let u = url
                if let qp = self.getQueryParameters(url: u) as? NSDictionary {
                    if ENABLE_FEATURE_DEBUG_INFO {
                        let message = "URL\n\(u)\n\nQUERY PARAMETERS\n\(qp != nil ?qp:nil)\n\n"
                        showNotification(message, showOnNavigation: true, showAsError: false,duration:5)
                    }
                    let deepLinkingType = self.getDeepLinkingType(url: u, qp: qp)
                    if deepLinkingType == .viewScreen {
                        self.handleScreenViewEvent(url: u, qp: qp)
                    }else if deepLinkingType == .performAction {
                        self.handleActionEvent(url: u, qp: qp)
                    }
                }
            },delay:0.4)
        }
        return handling
    }
    
    func handleScreenViewEvent(url:URL,qp:NSDictionary){
        let screenType =  self.getScreenType(url: url, qp: qp)
        if screenType == .profile {
            if let userId = qp.object(forKey: "user_id") as? String {
                if userId == loggedInUserId() {
                    let vc = getViewController("ProfileViewController") as! ProfileViewController
                    self.presentVC(vc: vc, sc: ScreenPresentation.fullScreen)
                }else{
                    let vc = getViewController("ProfileOthersViewController") as! ProfileOthersViewController
                    vc.userInfo = ["userId":userId]
                    self.presentVC(vc: vc, sc: ScreenPresentation.popup)
                }
            }
        }else if screenType == .editProfile {
            let vc = getViewController("EditProfileOtherOptionsViewController") as! EditProfileOtherOptionsViewController
            self.presentVC(vc: vc, sc: ScreenPresentation.fullScreen)
        }else if screenType == .rideHome {
            Acf().switchHomeScreenToCarpool()
            let subType = safeString(qp.object(forKey: "sub_type"),alternate: "default")
            if subType == "default"{
                Acf().carpoolHomeScreenController?.selectedIndex = 0
                Acf().rideHomeVC?.loadUsersOnMap()
            }else if subType == "list_mode" {
                Acf().carpoolHomeScreenController?.selectedIndex = 0
                Acf().rideHomeVC?.loadUsersOnMap()
            }
        }else if screenType == .recentChats {
            Acf().switchHomeScreenToCarpool()
            let subType = safeString(qp.object(forKey: "sub_type"),alternate: "default")
            if subType == "default"{
                Acf().carpoolHomeScreenController?.selectedIndex = 1
                (Acf().carpoolHomeScreenController?.viewControllers?[1] as! UINavigationController).popToRootViewController(animated: true)
            }
        }else if screenType == .offers {
            Acf().switchHomeScreenToCarpool()
            let subType = safeString(qp.object(forKey: "sub_type"),alternate: "default")
            if subType == "default"{
                Acf().carpoolHomeScreenController?.selectedIndex = 3
                (Acf().carpoolHomeScreenController?.viewControllers?[3] as! UINavigationController).popToRootViewController(animated: true)
            }
        }else if screenType == .ranking {
            Acf().switchHomeScreenToCarpool()
            let subType = safeString(qp.object(forKey: "sub_type"),alternate: "default")
            if subType == "default"{
                Acf().carpoolHomeScreenController?.selectedIndex = 2
                (Acf().carpoolHomeScreenController?.viewControllers?[2] as! UINavigationController).popToRootViewController(animated: true)
            }
        }else if screenType == .chat {
            Acf().switchHomeScreenToCarpool()
            let subType = safeString(qp.object(forKey: "sub_type"),alternate: "default")
            if subType == "default" || subType == "chat1To1" {
                if let userId = qp.object(forKey: "user_id") as? String {
                    Acf().carpoolHomeScreenController?.selectedIndex = 1
                    (Acf().carpoolHomeScreenController?.viewControllers?[1] as! UINavigationController).popToRootViewController(animated: true)
                    self.prepare()
                    Acf().showChatScreen(userId)
                }
            }else if subType == "riders_group" {
                Acf().carpoolHomeScreenController?.selectedIndex = 1
                (Acf().carpoolHomeScreenController?.viewControllers?[1] as! UINavigationController).popToRootViewController(animated: true)
                self.prepare()
                Acf().showRiderGroup()
            }else if subType == "chat_create" {
                Acf().carpoolHomeScreenController?.selectedIndex = 1
                let nc = (Acf().carpoolHomeScreenController?.viewControllers?[1] as! UINavigationController)
                nc.popToRootViewController(animated: true)
                let vc = getViewController("NewChatViewController") as! NewChatViewController
                nc.pushViewController(vc, animated: false)
            }
        }else if screenType == .statusEditor {
            Acf().switchHomeScreenToCarpool()
            Acf().showStatusUpdateScreen()
        }else if screenType == .verification {
            Acf().switchHomeScreenToCarpool()
            let vc = getViewController("VerificationOptionsViewController")
            Acf().showVCAsPopup(viewController:vc)
        }else if screenType == .favourites {
            Acf().switchHomeScreenToCarpool()
            Acf().pushVC("FavouriteUsersViewController", navigationController: Acf().navigationController, isRootViewController: false, animated: true, modifyObject: nil)
        }else if screenType == .payments {
            Acf().switchHomeScreenToCarpool()
            Acf().pushVC("PaymentOptionsViewController", navigationController: Acf().navigationController, isRootViewController: false, animated: true, modifyObject: nil)
        }else if screenType == .recharge {
            Acf().switchHomeScreenToCarpool()
            let vc = getViewController("RechargeViewController") as! RechargeViewController
            vc.preSelectionAmount = safeString("\(safeInt(qp.object(forKey: "amount")))",alternate: "\(defaultRechargeValue())")
            Acf().showVCAsPopup(viewController:vc)
        }else if screenType == .withdraw {
            Acf().switchHomeScreenToCarpool()
            let vc = getViewController("WithdrawlViewController") as! WithdrawlViewController
            vc.preSelectionAmount = safeString("\(safeInt(qp.object(forKey: "amount")))",alternate: "\(defaultRechargeValue())")
            Acf().showVCAsPopup(viewController:vc)
        }else if screenType == .settings {
            Acf().switchHomeScreenToCarpool()
            Acf().pushVC("SettingsViewController", navigationController: Acf().navigationController, isRootViewController: false, animated: true, modifyObject: nil)
        }else if screenType == .support {
            Acf().switchHomeScreenToCarpool()
            let vc = getViewController("OtherOptionsViewController") as! OtherOptionsViewController
            execMain({[weak self] in guard let `self` = self else { return }

                vc.selectRow(indexPath: IndexPath(row: 2, section: 0))
            }, delay: 0.5)
            Acf().showVCAsPopup(viewController:vc)
        }else if screenType == .faq {
            Acf().switchHomeScreenToCarpool()
            let vc = getViewController("WebViewViewController") as! WebViewViewController
            vc.workingMode = WorkingMode.faq
            Acf().showVCAsPopup(viewController:vc)
        }else if screenType == .feedback {
            Acf().switchHomeScreenToCarpool()
            let feedbackVC = CTFeedbackViewController(topics: CTFeedbackViewController.defaultTopics(),localizedTopics: CTFeedbackViewController.defaultLocalizedTopics())
            feedbackVC?.toRecipients = [SUPPORT_EMAIL]
            feedbackVC?.useHTML = false
            Acf().navigationController?.present(UINavigationController(rootViewController: feedbackVC!), animated: true, completion: nil)
        }else if screenType == .contactUs {
            Acf().switchHomeScreenToCarpool()
            let vc = getViewController("WebViewViewController") as! WebViewViewController
            vc.workingMode = WorkingMode.contactUs
            Acf().showVCAsPopup(viewController:vc)
        }else if screenType == .search {
            Acf().switchHomeScreenToCarpool()
            Acf().pushVC("SearchOrahiUsersViewController", navigationController: Acf().navigationController, isRootViewController: false, animated: true, modifyObject: nil)
        }else if screenType == .aboutUs {
            Acf().switchHomeScreenToCarpool()
            let vc = getViewController("WebViewViewController") as! WebViewViewController
            vc.workingMode = WorkingMode.aboutUs
            Acf().showVCAsPopup(viewController:vc)
        }else if screenType == .rateUs {
            Acf().switchHomeScreenToCarpool()
            let vc = getViewController("OtherOptionsViewController") as! OtherOptionsViewController
            execMain({[weak self] in guard let `self` = self else { return }
                Acf().askUserForHisExperience()
            }, delay: 0.5)
            Acf().showVCAsPopup(viewController:vc)
        }else if screenType == .termsAndConditions {
            Acf().switchHomeScreenToCarpool()
            let vc = getViewController("WebViewViewController") as! WebViewViewController
            vc.workingMode = WorkingMode.termsAndConditions
            Acf().showVCAsPopup(viewController:vc)
        }
    }
    
    func handleActionEvent(url:URL,qp:NSDictionary){
        let actionType =  self.getActionType(url: url, qp: qp)
        if actionType == .loadListing {
            Acf().switchHomeScreenToCarpool()
            Acf().carpoolHomeScreenController?.selectedIndex = 0
            let dateString = safeString(qp.object(forKey: "dat"),alternate: Date().toString(DateFormat.custom("yyyy/MM/dd")))
            let mode = (safeString(qp.object(forKey: "mode"),alternate: "evening") == "morning") ? "0" : "1"
            let date = dateString.dateValueType1()
            Acf().rideHomeVC?.loadUsing(date, timeDate: Date(), mode: mode)
        }
    }
    
    func getDeepLinkingType(url:URL,qp:NSDictionary)->DeepLinkingType {
        if let deepLinkingType = qp["type"] as? String{
            if deepLinkingType == "redirect" {
                return DeepLinkingType.viewScreen
            }else if deepLinkingType == "redirect_action" {
                return DeepLinkingType.performAction
            }else if deepLinkingType == "redirect_pop_up" {
                return DeepLinkingType.popup
            }else if deepLinkingType == "reoccuring_pop_up" {
                return DeepLinkingType.reOccuringPopup
            }
        }
        return DeepLinkingType.viewScreen
    }
    
    func getScreenType(url:URL,qp:NSDictionary)->ScreenType {
        if let screen = qp.object(forKey: "page") as? String{
            if screen == "profile" {
                return ScreenType.profile
            }else if screen == "edit_profile" {
                return ScreenType.editProfile
            }else if screen == "ride" {
                return ScreenType.rideHome
            }else if screen == "conversations" {
                return ScreenType.recentChats
            }else if screen == "chat" {
                return ScreenType.chat
            }else if screen == "rank" {
                return ScreenType.ranking
            }else if screen == "offers" {
                return ScreenType.offers
            }else if screen == "status" {
                return ScreenType.statusEditor
            }else if screen == "verification" {
                return ScreenType.verification
            }else if screen == "favourites" {
                return ScreenType.favourites
            }else if screen == "payments" {
                return ScreenType.payments
            }else if screen == "recharge" {
                return ScreenType.recharge
            }else if screen == "withdraw" {
                return ScreenType.withdraw
            }else if screen == "settings" {
                return ScreenType.settings
            }else if screen == "support" {
                return ScreenType.support
            }else if screen == "faqs" {
                return ScreenType.faq
            }else if screen == "share_feedback" {
                return ScreenType.feedback
            }else if screen == "contact_us" {
                return ScreenType.contactUs
            }else if screen == "search" {
                return ScreenType.search
            }else if screen == "about_us" {
                return ScreenType.aboutUs
            }else if screen == "rate_us" {
                return ScreenType.rateUs
            }else if screen == "tnc" {
                return ScreenType.termsAndConditions
            }
        }
        return ScreenType.rideHome
    }
    
    
    func getActionType(url:URL,qp:NSDictionary)->ActionType {
        let screen = safeString(qp.object(forKey: "page"))
        let type = safeString(qp.object(forKey: "sub_type"))
        if screen == "ride" && type == "get_listing" {
            return ActionType.loadListing
        }
        return ActionType.none
    }
    
    func prepare(){
        Acf().sideMenuController?.hideViewController()
        Acf().hidePopupViewController()
    }
    
    func presentVC(vc:UIViewController,sc:ScreenPresentation,tab:Int=0) {
        execMain ({ (completed) in
            if sc == .fullScreen{
                Acf().navigationController?.pushViewController(vc, animated: true)
            }else if sc == .popup{
                Acf().showVCAsPopup(viewController: vc)
            }
        })
    }
    
    func getQueryParameters(url:URL)->Any?{
        let queryItems = URLComponents(string: url.absoluteString)?.queryItems
        let parameterJson = queryItems?.filter({$0.name == "url_data"}).first?.value
        if parameterJson != nil {
            let parameterJsonString = parameterJson!.removingPercentEncoding
            let jsonData = parameterJsonString?.data(using: String.Encoding.utf8)!
            if jsonData != nil {
                return parsedJson(jsonData!, methodName: #function)
            }else{
                return nil
            }
        }else{
            return nil
        }
    }
    
    func canWeHandle(url:URL)->Bool{
        let queryItems = URLComponents(string: url.absoluteString)?.queryItems
        let parameter = queryItems?.filter({$0.name == "url_data"}).first
        if parameter != nil {
            return true
        }
        return false
    }
    
}

func Dlm()->DeepLinkingManager{
    return DeepLinkingManager.sharedInstance
}
