//
//  RideLocationUpdateManager.swift
//  Application
//
//  Created by Alok Singh on 28/07/16.
//  Copyright © 2016 Orahi. All rights reserved.
//

import Foundation
import UIKit
import EZSwiftExtensions

class RideLocationUpdateManager: NSObject , CLLocationManagerDelegate{
    var usersForRide = NSMutableArray()
    var trueForOwnerFalseforPassenger = false
    var trueForHomeToOfficeFalseforOfficeToHome = false
    var groupChatDialogue: QBChatDialog?
    let locationManager = CLLocationManager()
    var dateOfRide = NSDate()
    var currentLocation : CLLocation?
    var rideStatus = RideStatus.None
    var isUpdating = false

    class var sharedInstance: RideLocationUpdateManager {
        struct Static {
            static var onceToken: dispatch_once_t = 0
            static var instance: RideLocationUpdateManager? = nil
        }
        dispatch_once(&Static.onceToken) {
            Static.instance = RideLocationUpdateManager()
        }
        return Static.instance!
    }
    
    private override init() {
        
    }
    
    func startLocationUpdatingProcessIfRequired(users:NSMutableArray,isOwner:Bool,isHomeToOffice:Bool,dateOfRide:NSDate,rideStatus:RideStatus,rideId:String,groupChatDialogue:QBChatDialog) {
        if isUpdating == false || (isUpdating && ((rideId as NSString).isEqualToString(getRideId()) == false)){
            stopUpdatingLocations()
            self.dateOfRide = dateOfRide
            self.usersForRide = users.mutableCopy() as! NSMutableArray
            self.trueForOwnerFalseforPassenger = isOwner
            self.trueForHomeToOfficeFalseforOfficeToHome = isHomeToOffice
            self.groupChatDialogue = groupChatDialogue
            locationManager.delegate = self
            locationManager.requestAlwaysAuthorization()
            locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
            self.rideStatus = rideStatus
            if trueForRideWaitingFalseforStarted() == false && (rideStatus == .Confirmed || rideStatus == .ConfirmedButDelayed) {
                isUpdating = true
                startLocationUpdateToRidersGroup()
            }else{
                isUpdating = false
                stopUpdatingLocations()
            }
        }
        //need to handle stop updating locations on group.
    }
    
    func startLocationUpdateToRidersGroup(){
        locationManager.startUpdatingLocation()
        if isNull(currentLocation) {
            if isNull(CacheManager.sharedInstance.loadObject("currentLocation")){
                if trueForHomeToOfficeFalseforOfficeToHome {
                    currentLocation = CLLocation(latitude: homeLocationCoordinate().latitude, longitude: homeLocationCoordinate().longitude)
                }else{
                    currentLocation = CLLocation(latitude: officeLocationCoordinate().latitude, longitude: officeLocationCoordinate().longitude)
                }
            }else{
                currentLocation = CacheManager.sharedInstance.loadObject("currentLocation") as? CLLocation
            }
        }
        NSObject.cancelPreviousPerformRequestsWithTarget(self, selector: #selector(RideLocationUpdateManager.updateLocationToOtherViaGroupChat), object: nil)
        self.performSelector(#selector(RideLocationUpdateManager.updateLocationToOtherViaGroupChat), withObject: nil, afterDelay:3)
    }
    
    func updateLocationToOtherViaGroupChat(){
        if AppCommonFunctions.sharedInstance.isUserLoggedIn() && canContinueToUpdateLocation() && AppCommonFunctions.sharedInstance.isQBReadyForItsUserDependentServices(){
            if isNotNull(groupChatDialogue) && isNotNull(currentLocation) {
                let userInfo = DatabaseManager.sharedInstance.getUserInfo()
                let message = QBChatMessage()
                let senderID = ServicesManager.instance().currentUser().ID
                let id = "\(loggedInUserId())"
                let lat = "\(currentLocation!.coordinate.latitude)"
                let lng = "\(currentLocation!.coordinate.longitude)"
                let rideId = "\(getRideId())"
                let date = "\(currentTimeStamp())"
                let name = userInfo!.name!
                let messageType = "location"
                message.customParameters = [MESSAGE_TYPE_KEY:"\(messageType)","id":"\(id)","lat":"\(lat)","lng":"\(lng)","rideId":"\(rideId)","date":"\(date)","name":"\(name)"]
                message.senderID = senderID
                message.markable = false
                message.dateSent = NSDate()
                
                ServicesManager.instance().chatService.sendMessage(message, toDialogID: self.groupChatDialogue?.ID, saveToHistory: false, saveToStorage: false) { (error: NSError?) -> Void in
                    if (error != nil) {
                        log("\(error)")
                    }else{
                        if isNotNull(self.groupChatDialogue) {
                            log("\n\nI am \(userInfo!.name!) and UPDATING MY LOCATION ON RIDERS GROUP\n")
                        }
                    }
                }
            }
        }
        NSObject.cancelPreviousPerformRequestsWithTarget(self, selector: #selector(RideLocationUpdateManager.updateLocationToOtherViaGroupChat), object: nil)
        self.performSelector(#selector(RideLocationUpdateManager.updateLocationToOtherViaGroupChat), withObject: nil, afterDelay: RIDE_LOCATION_UPDATE_FREQUENCY)
    }
    
    func canContinueToUpdateLocation() -> Bool {
        if trueForRideWaitingFalseforStarted() == false && (rideStatus == .Confirmed || rideStatus == .ConfirmedButDelayed){
            return true
        }
        return false
    }
    
    func stopUpdatingLocations() {
        NSObject.cancelPreviousPerformRequestsWithTarget(self)
        locationManager.stopUpdatingLocation()
        NSObject.cancelPreviousPerformRequestsWithTarget(self)
        usersForRide.removeAllObjects()
        groupChatDialogue = nil
        currentLocation = nil
        trueForOwnerFalseforPassenger = false
        trueForHomeToOfficeFalseforOfficeToHome = false
        NSObject.cancelPreviousPerformRequestsWithTarget(self)
    }

    func trueForRideWaitingFalseforStarted() -> Bool {
        return  NSDate().isEarlierThanDate(dateOfRide)
    }
    
    func isAtleast1HourLeftForRideStart() -> Bool {
        return  NSDate().isLaterThanDate(dateOfRide.dateBySubtractingMinutes(60))
    }

    func getRideId() -> String {
        return "\(dateOfRide.day())\(dateOfRide.month())\(dateOfRide.year())\(trueForHomeToOfficeFalseforOfficeToHome == true ? "0" : "1")"
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if locations.count > 0 {
            currentLocation = locations.last!
        }
    }
}