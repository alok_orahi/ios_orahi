//
//  DRNManager.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit
import SDCAlertView
import CTFeedback

/*
 DRNManager : This class takes notification details as input , then process it to show DRN's
 */

class DRNManager: NSObject {
    
    var specifications = NSMutableArray()

    var customInfo = NSMutableDictionary()
    
    var presenterVC = getViewController("DRNPresenterViewController")
    
    var alertController : AlertController?
    
    var currentAccountBalance = 0
    
    var thresholdAccountBalanceToSkipRechargeView = 160
    
    var timeWhenAccountBalanceUpdated : Date?
    
    var drnClicked = false

    static let sharedInstance : DRNManager = {
        let instance = DRNManager()
        instance.startUpInitialisations()
        return instance
    }()
    
    
    fileprivate override init() {
        
    }
    
    /**
      process the notification and show if it is a valid DRN notification.
     */
    func processIfDRN(notification:Any?)->Bool{
        cacheAllImages()
        var isDRNNotification = false
        if let drn = notification as? NSDictionary {
            if drnId(drn: drn) != nil {
                isDRNNotification = true
                func proceed(){
                    drnClicked = false
                    if isInternetConnectivityAvailable(false) {
                        alertController = nil
                        customInfo.removeAllObjects()
                        showDRNPushNotificationReceivedForDebugging(drn: drn)
                        
                        //we know that every DRN starts with level 1 , i.e levelId l1
                        showDRN(drn: drn, levelId: L1)
                        
                        let drnInfo = getImportantDRNParameters(drn: drn)
                        trackFunctionalEvent(FE_D_RECEIVED, information: drnInfo)
                        trackFunctionalEvent(FE_D_VISIBLE, information: drnInfo)
                        trackScreenLaunchEvent(getDRNScreenEventName(drn:drn))
                        updateUserAccountBalance()
                    }else{
                        showInternetConnectionReachability(drn: drn)
                    }
                }
                if alertController != nil {
                    alertController?.presentingViewController?.dismiss(animated: false, completion: nil)
                    execMain({[weak self] in guard let `self` = self else { return }

                        proceed()
                    }, delay: 0.4)
                }else{
                    proceed()
                }
            }
        }
        return isDRNNotification
    }
    
    func isDRNActive()->Bool{
        let presentedVC = Acf().navigationController?.presentedViewController
        if  presentedVC != nil && presentedVC is DRNPresenterViewController {
            return true
        }else{
            return false
        }
    }
}




/*
 DRNManager : an extension of DRNManager which contains important functions
 */

fileprivate extension DRNManager {
    
    
    func showDRN(drn:NSDictionary,levelId:String){
        let level = drnLevel(drn: drn,levelId: levelId)
        if level != nil {
            showDRNBackgroundView()
            parseLevelAndShowPopup(drn: drn,level: level!)
        }
    }
    
    
    /// This method takes Drn information and its level to show
    ///
    /// - Parameters:
    ///   - drn: its contains DRN details
    ///   - level: drn is devided into number of levels , the level which needs to be shown is passed here.
    func parseLevelAndShowPopup(drn:NSDictionary,level:NSDictionary){
        //perform any initial setup which is required to process this DRN
        //sometimes for some specific cases of DRN , we want some customisation , this method detects and
        //handle that stuff.
        performAnySetupRequiredBeforeShowingNotification(drn: drn,level: level)
        
        //show the background view if not already
        showDRNBackgroundView()
        
        //create a new alert controller object
        alertController = AlertController(title: nil, message: nil)
        
        //create actual UI content to be shown and attach it to alertController
        let drnContentView = prepareContentView(drn: drn, level: level,alert:alertController!)
        
        //create and handle actions required for the DRN
        prepareAndHandleAlertActions(drn: drn, level: level, alert: alertController!,drnContentView:drnContentView)
        
        alertController!.present(animated:false)
        
    }
    
    func shouldOpenRecharge(drn:NSDictionary,level:NSDictionary,action:NSDictionary)->Bool{
        var recharge = false
        if safeBool(action.object(forKey: "check_recharge")){
            recharge = safeBool(drn.object(forKey: "recharge"))
            if recharge {
                if currentAccountBalance > thresholdAccountBalanceToSkipRechargeView {
                    if timeWhenAccountBalanceUpdated != nil {
                        if timeWhenAccountBalanceUpdated!.isLaterThanDate(Date().dateBySubtractingMinutes(10)){
                            recharge = false
                        }
                    }
                }
            }
        }
        return recharge
    }
    
    func showRechargeDRN(drn:NSDictionary,level:NSDictionary,otherInfo:NSDictionary,currentLevelId:String){
        showDRNBackgroundView()
        
        alertController = AlertController(title: nil, message: nil)
        
        let allOtherInformations = NSMutableDictionary()
        allOtherInformations.addEntries(from: drn as! [AnyHashable : Any])
        allOtherInformations.addEntries(from: otherInfo as! [AnyHashable : Any])
        copyData(level, sourceKey: "level_id", destinationDictionary: allOtherInformations, destinationKey: "level_id", methodName: #function)
        
        setContentViewType10(title: "Recharge", description: "Please recharge to continue",otherInfo:otherInfo,superView:alertController!.contentView,owner:self)
        
        let alertAction1 = AlertAction(title: "Cancel", style:AlertActionStyle.normal, handler: { (action) in
            self.showDRN(drn: drn, levelId: currentLevelId)
        })
        
        let alertAction2 = AlertAction(title: "Close", style:AlertActionStyle.destructive, handler: { (action) in
            self.hideDRNBackgroundView()
        })
        
        alertController!.add(alertAction1)
        alertController!.add(alertAction2)
        alertController!.present(animated:false)
        
        trackFunctionalEvent(FE_D_RECHARGE, information: drn)
    }
    
    func showInternetConnectionReachability(drn:NSDictionary){
        showDRNBackgroundView()
        
        alertController = AlertController(title: nil, message: nil)
        
        setContentViewType1(title: "Oops !", description: "\n\(MESSAGE_TEXT___FOR_NETWORK_NOT_REACHABILITY)",imageUrl: nil,superView:alertController!.contentView,owner:self)
        
        let alertAction1 = AlertAction(title: "Retry", style:AlertActionStyle.normal, handler: { (action) in
            self.processIfDRN(notification: drn)
        })
        
        let alertAction2 = AlertAction(title: "Close", style:AlertActionStyle.destructive, handler: { (action) in
            self.hideDRNBackgroundView()
        })
        
        alertController!.add(alertAction1)
        alertController!.add(alertAction2)
        alertController!.present(animated:false)
    }
    

    func redirect(actionInfo:NSDictionary){
        if let redirectUrl = actionInfo.object(forKey: "r_url") as? String{
            if safeBool(actionInfo.object(forKey: "d_link")){
                var url = URL(string: redirectUrl)
                if isNotNull(url){
                    Dlm().handleDeeplinkUrl(url: url!)
                }
            }else{
                showWebView(url: redirectUrl)
            }
        }
    }
    
    func showWebView(url:String){
        showDRNBackgroundView()
        
        alertController = AlertController(title: nil, message: nil)
        
        setContentViewType11(title: "", description: "",urlString:url,superView:alertController!.contentView,owner:self)
        
        let alertAction1 = AlertAction(title: "Close", style:AlertActionStyle.destructive, handler: { (action) in
            self.hideDRNBackgroundView()
        })
        
        alertController!.add(alertAction1)
        alertController!.present(animated:false)
    }
    
    func performAPIRequest(drn:NSDictionary,level:NSDictionary,action:NSDictionary,inputParameters:NSDictionary,requestType:String?,performAPIRequest:Bool=true)->NSDictionary?{
        
        func trackDRNAPIEvents(apiName:String,parameters:NSDictionary,executionStatus:Bool,failureReason:String?){
            let eventName = "DRN\(apiName.replacingOccurrences(of: "drn/", with: "_").uppercased())"
            let parametersToSend = NSMutableDictionary()
            parametersToSend.addEntries(from: parameters as! [AnyHashable : Any])
            parametersToSend.addEntries(from: getImportantDRNParameters(drn: drn) as! [AnyHashable : Any])
            parametersToSend.setObject(executionStatus, forKey: "requestStatus" as NSCopying)
            copyData(drn, sourceKey: KEY_DRN_ID, destinationDictionary: parametersToSend, destinationKey: KEY_DRN_ID, methodName: #function)
            if failureReason != nil {
                parametersToSend.setObject(failureReason!, forKey: "failureReason" as NSCopying)
            }
            trackFunctionalEvent(eventName, information: parametersToSend)
        }
        
        let information = NSMutableDictionary()
        
        let scm = ServerCommunicationManager()
        
        scm.showDebugInformationAboutRequestInNotification = debuggingEnabled()
        scm.responseErrorOption = ResponseErrorOption.dontShowErrorResponseMessage
        scm.returnFailureResponseAlso = true
        scm.returnFailureUnParsedDataIfParsingFails = true
        
        var apiName = ""
        var isPostRequest = true
        
        if isInternetConnectivityAvailable(true) {
            
            switch getAction(drn:drn,level:level,action:action) {
                
            case Action.UnIdentified:
                
                exit(0)
                
                break
                
            case Action.RideQuestion_SendInvitation_No:
                
                /*
                 type (accept/decline/later)
                 travel_mode (co/pass)
                 etd_new (revised etd, ex. 18:00 for type=accept)
                 time (next drn dispatch time for type =later)
                 <usecases>
                 
                 2. No
                 api - drn/ride_quest
                 params - user_id,date, type,travel_mode,is_morning(true/false)
                 
                 */
                
                copyData("decline", destinationDictionary: information, destinationKey: "type", methodName: #function)
                
                apiName = "drn/ride_quest"
                
                isPostRequest = true
                
                break
                
            case Action.RideQuestion_Later_No:
                
                /*
                 type (accept/decline/later)
                 travel_mode (co/pass)
                 etd_new (revised etd, ex. 18:00 for type=accept)
                 time (next drn dispatch time for type =later)
                 <usecases>
                 
                 3. Later
                 api - drn/ride_quest
                 params - user_id,date, type,travel_mode,is_morning(true/false)
                 */
                
                copyData("later", destinationDictionary: information, destinationKey: "type", methodName: #function)
                
                apiName = "drn/ride_quest"
                
                isPostRequest = true
                
                break
                
            case Action.RideQuestion_Later_Schedule:
                
                /*
                 type (accept/decline/later)
                 travel_mode (co/pass)
                 etd_new (revised etd, ex. 18:00 for type=accept)
                 time (next drn dispatch time for type =later)
                 <usecases>
                 
                 3. Later
                 api - drn/ride_quest
                 params - user_id,time,date, type,travel_mode,is_morning(true/false)
                 */
                
                copyData(getActionSpecificParameter(drn: drn, level: level, action: action), destinationDictionary: information, destinationKey: "time", methodName: #function)
                
                copyData("later", destinationDictionary: information, destinationKey: "type", methodName: #function)
                
                apiName = "drn/ride_quest"
                
                isPostRequest = true
                
                break
                
            case Action.RideQuestion_SendInvitation_Yes:
                
                /*
                 type (accept/decline/later)
                 travel_mode (co/pass)
                 etd_new (revised etd, ex. 18:00 for type=accept)
                 time (next drn dispatch time for type =later)
                 
                 1. Yes Selection
                 api - drn/ride_quest
                 params - user_id,etd_new,date,type,travel_mode,is_morning(true/false)
                 */
                
                copyData(getActionSpecificParameter(drn: drn, level: level, action: action), destinationDictionary: information, destinationKey: "etd_new", methodName: #function)
                
                copyData("accept", destinationDictionary: information, destinationKey: "type", methodName: #function)
                
                apiName = "drn/ride_quest"
                
                isPostRequest = true
                
                break
                
            case Action.RideQuestion_AcceptInvitation_Yes:
                
                /*
                 action (accept/decline/cancel)
                 travel_mode (co/pass)
                 reason(time,location,interest)
                 
                 
                 1. Yes Selection
                 api - drn/ride_request_respone
                 params -user_id,travel_mode,date,is_morning(true/false),co_traveller_id,action
                 */
                
                copyData("accept", destinationDictionary: information, destinationKey: "action", methodName: #function)
                
                apiName = "drn/ride_request_response"
                
                isPostRequest = true
                
                break
                
            case Action.RideQuestion_AcceptInvitation_No:
                
                /*
                 action (accept/decline/cancel)
                 travel_mode (co/pass)
                 reason(time,location,interest)
                 
                 2. No
                 api - drn/ride_request_response
                 params - user_id,travel_mode,date,is_morning(true/false),co_traveller_id, action, reason
                 */
                
                
                copyData("decline", destinationDictionary: information, destinationKey: "action", methodName: #function)
                
                copyData(customInfo, sourceKey: "declineReason", destinationDictionary: information, destinationKey: "reason", methodName: #function)
                
                apiName = "drn/ride_request_response"
                
                isPostRequest = true
                
                break
                
            case Action.RideQuestion_AcceptInvitation_Cancel:
                
                /*
                 action (accept/decline/cancel)
                 travel_mode (co/pass)
                 reason(time,location,interest)
                 
                 3. Cancel
                 api - drn/ride_request_response
                 params -user_id,travel_mode,date,is_morning(true/false),co_traveller_id, action, reason
                 */
                
                copyData("cancel", destinationDictionary: information, destinationKey: "action", methodName: #function)
                
                copyData(customInfo, sourceKey: "cancelReason", destinationDictionary: information, destinationKey: "reason", methodName: #function)
                
                apiName = "drn/ride_request_response"
                
                isPostRequest = true
                
                break
                
            case Action.RideConfirmation_Cancel:
                
                /*
                 action (accept/decline/cancel)
                 travel_mode (co/pass)
                 reason(time,location,interest)
                 
                 3. Cancel
                 api - drn/ride_request_response
                 params -user_id,travel_mode,date,is_morning(true/false),co_traveller_id, action, reason
                 */
                
                copyData("cancel", destinationDictionary: information, destinationKey: "action", methodName: #function)
                
                copyData(customInfo, sourceKey: "cancelReason", destinationDictionary: information, destinationKey: "reason", methodName: #function)
                
                apiName = "drn/ride_request_response"
                
                isPostRequest = true
                
                break
                
            case Action.SettlePayment_Settle:
                
                /*
                 
                 travel_mode (co,pass)
                 
                 1. Settle
                 api - drn/ride_settle
                 params - co_traveller_id,user_id,date,travel_mode,etd,request_id
                 
                 */
                
                copyData(drn, sourceKey: "req_id", destinationDictionary: information, destinationKey: "request_id", methodName: #function)
                
                apiName = "drn/ride_settle"
                
                isPostRequest = true
                
                break
                
            case Action.RideFeedback_Send:
                
                /*
                 travel_mode (co,pass)
                 reason(time,location,unavailable,change_plan) // only if rating less than 5)
                 request_id(rideid)
                 etd
                 date
                 user_id(  recipient user id)
                 
                 1. Send Feedback
                 api - drn/ride_feedback
                 params - id,travel_mode,date,user_id,etd,rating,request_id,reason
                 */
                
                copyData(inputParameters, sourceKey: "rating", destinationDictionary: information, destinationKey: "rating", methodName: #function)
                copyData(customInfo, sourceKey: "rating", destinationDictionary: information, destinationKey: "rating", methodName: #function)
                
                copyData(drn, sourceKey: "req_id", destinationDictionary: information, destinationKey: "request_id", methodName: #function)
                
                copyData(action, sourceKey: "reason", destinationDictionary: information, destinationKey: "reason", methodName: #function)
                
                apiName = "drn/ride_feedback"
                
                isPostRequest = true
                
                
                break
                
            case Action.Apology_TryNow:
                
                /*
                 travel_mode (co,pass)
                 action (yes,later)
                 time ( rescheduled drn dispatch time when action=later)
                 etd_new ( etd to set for action=yes only)
                 
                 1. Yes Selection
                 api - drn/ride_apology
                 params - id,etd_new,date, action,travel_mode
                 
                 */
                
                copyData(getActionSpecificParameter(drn: drn, level: level, action: action), destinationDictionary: information, destinationKey: "etd_new", methodName: #function)
                
                copyData("yes", destinationDictionary: information, destinationKey: "action", methodName: #function)
                
                apiName = "drn/ride_apology"
                
                isPostRequest = true
                
                break
                
            case Action.RideBroken_InviteRegular:
                
                /*
                 travel_mode (co,pass)
                 action(yes,no)
                 reason(time,location,no)
                 partner(regular,others)  //regualr or new
                 
                 
                 1. Yes(action=yes)
                 api - drn/send_invite
                 params - user_id,travel_mode,date,is_morning(true/false),co_traveller_id,etd, action,partner
                 
                 */
                
                copyData("yes", destinationDictionary: information, destinationKey: "action", methodName: #function)
                
                copyData("regular", destinationDictionary: information, destinationKey: "partner", methodName: #function)
                
                apiName = "drn/send_invite"
                
                isPostRequest = true
                
                break
                
            case Action.RideBroken_InviteNew:
                /*
                 travel_mode (co,pass)
                 action(yes,no)
                 reason(time,location,no)
                 partner(regular,others)  //regualr or new
                 
                 
                 1. Yes(action=yes)
                 api - drn/send_invite
                 params - user_id,travel_mode,date,is_morning(true/false),co_traveller_id,etd, action,partner
                 
                 */
                
                copyData("yes", destinationDictionary: information, destinationKey: "action", methodName: #function)
                
                copyData("others", destinationDictionary: information, destinationKey: "partner", methodName: #function)
                
                apiName = "drn/send_invite"
                
                isPostRequest = true
                
                break
                
            case Action.RideBroken_NotIntrested:
                
                /*
                 travel_mode (co,pass)
                 action(yes,no)
                 reason(time,location,no)
                 partner(regular,others)  //regualr or new
                 
                 2. No (action=no)
                 api - drn/send_invite
                 params - user_id,travel_mode,date,is_morning(true/false),co_traveller_id,etd, action,partner
                 
                 */
                
                
                copyData("no", destinationDictionary: information, destinationKey: "action", methodName: #function)
                
                apiName = "drn/send_invite"
                
                isPostRequest = true
                
                break
                
                
            case Action.RideSuggestion_RideTogether:
                
                /*
                 travel_mode (co,pass)
                 action(yes,no)
                 reason(time,location,no)
                 partner(regular,others)  //regualr or new
                 
                 
                 1. Yes(action=yes)
                 api - drn/send_invite
                 params - user_id,travel_mode,date,is_morning(true/false),co_traveller_id,etd, action
                 
                 */
                
                copyData("yes", destinationDictionary: information, destinationKey: "action", methodName: #function)
                
                apiName = "drn/send_invite"
                
                isPostRequest = true
                
                break
                
                
            case Action.RideSuggestion_Issue:
                
                /*
                 travel_mode (co,pass)
                 action(yes,no)
                 reason(time,location,no)
                 partner(regular,others)  //regualr or new
                 
                 2. No (action=no)
                 api - drn/send_invite
                 params - user_id,travel_mode,date,is_morning(true/false),co_traveller_id,etd, action, reason,partner
                 
                 */
                
                copyData("no", destinationDictionary: information, destinationKey: "action", methodName: #function)
                
                copyData(action, sourceKey: "reason", destinationDictionary: information, destinationKey: "reason", methodName: #function)
                
                apiName = "drn/send_invite"
                
                isPostRequest = true
                
                break
                
            case Action.AutoFix_Yes:
                
                /*
                 travel_mode (co/pass)
                 slot (evening/morning/both)
                 action (yes/no)
                 
                 Example:-
                 
                 1. Autofix Selection (action=yes)
                 api - drn/auto_fix
                 params -   user_id,co_traveller_id,date,etd,travel_mode,slot,action
                 
                 */
                
                copyData("yes", destinationDictionary: information, destinationKey: "action", methodName: #function)
                
                copyData(action, sourceKey: "slot", destinationDictionary: information, destinationKey: "slot", methodName: #function)
                
                apiName = "drn/auto_fix"
                
                isPostRequest = true
                
                break
                
            case Action.AutoFix_No:
                
                /*
                 travel_mode (co/pass)
                 slot (evening/morning/both)
                 action (yes/no)
                 
                 Example:-
                 
                 2. No (action=no)
                 api - drn/auto_fix
                 params -   user_id,co_traveller_id,date,etd,travel_mode,action
                 */
                
                copyData("no", destinationDictionary: information, destinationKey: "action", methodName: #function)
                
                apiName = "drn/auto_fix"
                
                isPostRequest = true
                
                break
                
            case Action.GenericAPIDynamicURL:
                
                apiName = "drn"
                
                if let api = action.object(forKey: "a_url") as? String{
                    apiName = api
                }
                
                isPostRequest = false
                
                break
                
            }
            
            func jsonRepresentation(object: Any)->String {
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: object, options: JSONSerialization.WritingOptions.prettyPrinted)
                    return String(data: jsonData as Data, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!
                }catch{
                    return ""
                }
            }
            
            func addCommonParameters(){
                copyData(loggedInUserId(), destinationDictionary: information, destinationKey: "user_id", methodName: #function)
                copyData(drn, sourceKey: "tr_m", destinationDictionary: information, destinationKey: "travel_mode", methodName: #function)
                copyData(drn, sourceKey: "dat", destinationDictionary: information, destinationKey: "date", methodName: #function)
                copyData(drn, sourceKey: "is_morning", destinationDictionary: information, destinationKey: "is_morning", methodName: #function)
                copyData(drn, sourceKey: "etd", destinationDictionary: information, destinationKey: "etd", methodName: #function)
                copyData(drn, sourceKey: "u_id", destinationDictionary: information, destinationKey: "co_traveller_id", methodName: #function)
                copyData(drn, sourceKey: "drn_i_id", destinationDictionary: information, destinationKey: "drn_instance_id", methodName: #function)
                copyData(drn, sourceKey: KEY_DRN_ID, destinationDictionary: information, destinationKey: KEY_DRN_ID, methodName: #function)
            }
            
            if performAPIRequest {
                NotificationCenter.default.post(name: NSNotification.Name(NOTIFICATION_DO_CLEAN_REFRESH), object: nil)
                addCommonParameters()
                scm.attachCommonDeviceDetails = false
                scm.attachCommonUserDetails = false
                scm.drnAPIRequest(information, urlPath: apiName, requestType: isPostRequest ? .post : .get , completionBlock: { (response) in
                    NotificationCenter.default.post(name: NSNotification.Name(NOTIFICATION_DO_CLEAN_REFRESH), object: nil)
                    if isNotNull(response) {
                        if ServerCommunicationManager.isSuccess(response){
                            trackDRNAPIEvents(apiName: apiName, parameters: information, executionStatus: true, failureReason: nil)
                        }else{
                            self.checkFailureResponsesAndPerformActionIfRequired(response: response)
                            trackDRNAPIEvents(apiName: apiName, parameters: information, executionStatus: false, failureReason: jsonRepresentation(object: response))
                        }
                    }else{
                        trackDRNAPIEvents(apiName: apiName, parameters: information, executionStatus: false, failureReason: "UnKnown")
                    }
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: NOTIFICATION_DRN_FINISHED), object: nil)
                })
            }
        }
        return information
    }
    
    func checkFailureResponsesAndPerformActionIfRequired(response:NSDictionary?){
        if let message = response?.object(forKey: "message") as? String{
            let errorType = safeString(response?.value(forKeyPath: "data.error"),alternate: "default")
            if errorType == "already_planned" {
                let name = safeString(response?.value(forKeyPath: "data.u_name"),alternate: "Rider")
                let information = [
                    "drn_id":"1013",
                    "drn_i_id":"0",
                    "desc":"\(name) has already planned the ride",
                    "tr_m":"pass",
                    "etd":"18:00",
                    "dat":Date().toStringValue("yyyy-MM-dd"),
                    "is_morning":"false"
                ]
                processIfDRN(notification: information)
            }else if errorType == "default" {
                showNotification(message, showOnNavigation: false, showAsError: true)
            }
        }
    }
    
    
    func performAnySetupRequiredBeforeShowingNotification(drn:NSDictionary,level:NSDictionary){
        let drnId = drn.object(forKey: KEY_DRN_ID) as! String
        let levelId = level.object(forKey: KEY_LEVEL_ID) as! String
        if drnId == DRN_ID_1004 && levelId == L1 {
            let description = level.object(forKey: KEY_DESCRIPTION) as! String
            let name = safeString(drn.object(forKey: "name"), alternate: "").firstName().capitalized
            let updatedDescription = description.replacingOccurrences(of: "[name]", with: name)
            customInfo.setObject(updatedDescription, forKey: "\(levelId)_\(KEY_DESCRIPTION)" as NSCopying)
        }
    }
    
    func calculateActionETD(action:NSDictionary,dateOfRide:Date,timeOfRide:Date,titleDelta:String)->Date{
        let currentTime = Date()
        var dateAndTime = dateOfRide
        if let titleCalculationType = action.object(forKey: KEY_TITLE_CALCULATION_TYPE) as? String {
            if titleCalculationType == KEY_TITLE_CALCULATION_TYPE_CURRENT_TIME {
                dateAndTime = dateOfRide.setTimeOfDate(currentTime.hour(), minute: currentTime.minute(), second: currentTime.second)
            }else{
                dateAndTime = dateOfRide.setTimeOfDate(timeOfRide.hour(), minute: timeOfRide.minute(), second: timeOfRide.second)
            }
        }else{
            dateAndTime = dateOfRide.setTimeOfDate(timeOfRide.hour(), minute: timeOfRide.minute(), second: timeOfRide.second)
        }
        return dateAndTime.dateByAddingMinutes(titleDelta.toInt())
    }
    
    func performAnySetupRequiredBeforeShowingNotificationDueToActions(drn:NSDictionary,level:NSDictionary,action:NSDictionary,inputParameters:NSDictionary,button:AlertAction?){
        
        let drnId = drn.object(forKey: KEY_DRN_ID) as! String
        
        let levelId = level.object(forKey: KEY_LEVEL_ID) as! String
        
        let nextLevelId = action.object(forKey: KEY_NEXT_LEVEL_ID) as? String
        
        if drnId == DRN_ID_1001 && levelId == L3 && safeString(nextLevelId, alternate: "") == L5 {
            
            if let titleType = action.object(forKey: KEY_TITLE_TYPE) as? String {
                
                if titleType == KEY_CALCULATE_ETD {
                    
                    if let titleDelta = action.object(forKey: KEY_TITLE_DELTA) as? String {
                        
                        if let time = drn.object(forKey: KEY_TIME) as? String {
                            
                            if let date = drn.object(forKey: KEY_DATE) as? String {
                                
                                let dateOfRide = getDate(dateString: date)
                              
                                let timeOfRide = getTime(timeString: time)
                                
                                let dateAndTime = calculateActionETD(action: action, dateOfRide: dateOfRide!, timeOfRide: timeOfRide!, titleDelta: titleDelta)
                                
                                let time = dateAndTime.stringTimeOnly_AM_PM_FormatValue()
                                
                                let nextLevel = drnLevel(drn: drn, levelId: nextLevelId!)
                                
                                let newTitle = nextLevel?.object(forKey: KEY_TITLE) as! String
                                
                                let updatedTitle = newTitle.replacingOccurrences(of: "[time]", with: time)
                                
                                customInfo.setObject(updatedTitle, forKey: "\(nextLevelId!)_\(KEY_TITLE)" as NSCopying)
                            }
                        }
                    }
                }
            }
        }else if (drnId == DRN_ID_1002 || drnId == DRN_ID_1003) && levelId == L3 && safeString(nextLevelId, alternate: "") == L4 {
            
            customInfo.setObject(safeString(action.object(forKey: "reason"), alternate: "unKnown"), forKey: "declineReason" as NSCopying)
            
        }else if (drnId == DRN_ID_1002 || drnId == DRN_ID_1003) && levelId == L1 && safeString(nextLevelId, alternate: "") == L2 {
            
            let nextLevel = drnLevel(drn: drn, levelId: nextLevelId!)
            
            let newDescription = nextLevel?.object(forKey: KEY_DESCRIPTION) as! String
            
            let name = safeString(drn.object(forKey: "name"), alternate: "").firstName().capitalized
            
            let updatedDescription = newDescription.replacingOccurrences(of: "[name]", with: name)
            
            customInfo.setObject(updatedDescription, forKey: "\(nextLevelId!)_\(KEY_DESCRIPTION)" as NSCopying)
            
        }else if (drnId == DRN_ID_1002 || drnId == DRN_ID_1003) && levelId == L5 && safeString(nextLevelId, alternate: "") == L6 {
            
            customInfo.setObject(safeString(action.object(forKey: "reason"), alternate: "unKnown"), forKey: "cancelReason" as NSCopying)
            
        }else if drnId == DRN_ID_1004 && levelId == L2 && safeString(nextLevelId, alternate: "") == L3 {
            
            customInfo.setObject(safeString(action.object(forKey: "reason"), alternate: "unKnown"), forKey: "cancelReason" as NSCopying)
            
        }else if drnId == DRN_ID_1009 && levelId == L1 && safeString(nextLevelId, alternate: "") == "5:l2,4:l3,3:l5,2:l5,1:l5" {
            
            var nextLevelToMoveInto = ""
           
            let ratingValue = inputParameters.object(forKey: "rating") as! String
            
            if ratingValue.toInt() == 5 {
            
                nextLevelToMoveInto = L2
            
            }else if ratingValue.toInt() >= 4 {
            
                nextLevelToMoveInto = L3
            
            }else {
            
                nextLevelToMoveInto = L5
            
            }
            
            customInfo.setObject(nextLevelToMoveInto, forKey: "\(levelId)_nextLevelToMoveInto" as NSCopying)
            
            customInfo.setObject(ratingValue, forKey: "rating" as NSCopying)
        
        }
    }
    
    
    
    
    func updatedNextLevelId(currentLevelId:String,nextlevelId:String?)->String?{
        if let updatedNextLevelId = customInfo.object(forKey: "\(currentLevelId)_nextLevelToMoveInto") as? String{
            return updatedNextLevelId
        }
        return nextlevelId
    }
    
    
    
    
    func updatedActionType(drn:NSDictionary,currentLevelId:String,nextlevelId:String?,currentActionType:DRNActionType)->DRNActionType{
     
        let drnId = drn.object(forKey: KEY_DRN_ID) as! String
     
        if drnId == DRN_ID_1009 && currentLevelId == L1 && nextlevelId == L3 {
        
            return DRNActionType.level
        
        }
        
        return currentActionType
    }
    
    
    
    func isPreffered(title:String)->Bool{
        return title.contains("yes", compareOption: .caseInsensitive) || title.contains("ok", compareOption: .caseInsensitive)
    }
    
    
    
    
    func decideTitle(drn:NSDictionary,level:NSDictionary)->String?{
        let levelId = level.object(forKey: KEY_LEVEL_ID) as! String
        if let title = customInfo.object(forKey: "\(levelId)_\(KEY_TITLE)") as? String{
            return title
        }
        if levelId == L1 , let title = drn.object(forKey: KEY_TITLE) as? String{
            return title
        }
        return level.object(forKey: KEY_TITLE) as? String
    }
    
    
    
    func decideDescription(drn:NSDictionary,level:NSDictionary)->String?{
        let levelId = level.object(forKey: KEY_LEVEL_ID) as! String
        if let desc = customInfo.object(forKey: "\(levelId)_\(KEY_DESCRIPTION)") as? String{
            return desc
        }
        if levelId == L1 , let desc = drn.object(forKey: KEY_DESCRIPTION) as? String{
            return desc
        }
        return level.object(forKey: KEY_DESCRIPTION) as? String
    }
    
    func prepareAndHandleAlertActions(drn:NSDictionary,level:NSDictionary,alert:AlertController,drnContentView:Any?){
        
        
        func performOnClickAction(action:AlertAction){
            
            if !drnClicked {
                let drnInfo = getImportantDRNParameters(drn: drn)
                trackFunctionalEvent(FE_D_CLICKED, information: drnInfo)
                drnClicked = true
            }
            
            if action.userInfo != nil {
                
                self.hideDRNBackgroundView()
                
                let actionInfo = action.userInfo! as! NSDictionary
                
                let actionType = self.getActionType(action: actionInfo)
                
                let apiRequestType = actionInfo.object(forKey: KEY_API_REQUEST_TYPE) as? String
                
                let nextLevelId = actionInfo.object(forKey: KEY_NEXT_LEVEL_ID) as? String
                
                let inputParameters = (drnContentView as! DRNContentView).getInputParameters()
                
                self.performAnySetupRequiredBeforeShowingNotificationDueToActions(drn:drn,level:level,action:actionInfo,inputParameters:inputParameters, button: action)
                
                let currentLevelId = level.object(forKey: KEY_LEVEL_ID) as! String
                
                let updateNextLevelId = self.updatedNextLevelId(currentLevelId: currentLevelId, nextlevelId: nextLevelId)
                
                let updatedActionType = self.updatedActionType(drn: drn, currentLevelId: currentLevelId, nextlevelId: updateNextLevelId, currentActionType: actionType)
                
                self.customInfo.addEntries(from: inputParameters as! [AnyHashable : Any])
                
                switch (updatedActionType){
                    
                case DRNActionType.level:
                    self.showDRN(drn: drn, levelId: updateNextLevelId!)
                    break
                    
                case DRNActionType.apiThenLevel:
                    if shouldOpenRecharge(drn:drn,level:level,action:actionInfo) {
                        let otherInfo = performAPIRequest(drn:drn,level:level,action:actionInfo,inputParameters:inputParameters,requestType:apiRequestType,performAPIRequest:false)
                        showRechargeDRN(drn: drn, level: level, otherInfo: otherInfo!, currentLevelId: currentLevelId)
                        
                    }else{
                        
                    self.performAPIRequest(drn:drn,level:level,action:actionInfo,inputParameters:inputParameters,requestType:apiRequestType)
                    self.showDRN(drn: drn, levelId: updateNextLevelId!)
                        
                    }
                    break
                    
                case DRNActionType.dismiss:
                    break
                    
                case DRNActionType.call:
                    if let phone = drn.object(forKey: "phone") as? String{
                        ACETelPrompt.callPhoneNumber(phone, call: { (duration) -> Void in }) { () -> Void in}
                    }

                    let parameters = NSMutableDictionary()
                    parameters.addEntries(from: drn as! [AnyHashable : Any])
                    parameters.addEntries(from: actionInfo as! [AnyHashable : Any])
                    trackFunctionalEvent(FE_D_CALL, information: parameters)
                    
                    break
                    
                case DRNActionType.chat:
                    if let userId = drn.object(forKey: "u_id") as? String{
                        Acf().showChatScreen(userId)
                    }
                    
                    let parameters = NSMutableDictionary()
                    parameters.addEntries(from: drn as! [AnyHashable : Any])
                    parameters.addEntries(from: actionInfo as! [AnyHashable : Any])
                    trackFunctionalEvent(FE_D_CHAT, information: parameters)
                    

                    break
                 
                case DRNActionType.feedback:
                    execMain({[weak self] in guard let `self` = self else { return }

                        let feedbackVC = CTFeedbackViewController(topics: CTFeedbackViewController.defaultTopics(),localizedTopics: CTFeedbackViewController.defaultLocalizedTopics())
                        feedbackVC?.toRecipients = [SUPPORT_EMAIL]
                        feedbackVC?.useHTML = false
                        Acf().navigationController?.present(UINavigationController(rootViewController: feedbackVC!), animated: true, completion: nil)
                    },delay: 1.0)
                    
                    let parameters = NSMutableDictionary()
                    parameters.addEntries(from: drn as! [AnyHashable : Any])
                    parameters.addEntries(from: actionInfo as! [AnyHashable : Any])
                    trackFunctionalEvent(FE_D_FEEDBACK, information: parameters)

                    break
                    
                case DRNActionType.redirectApp:
                    
                    break
                    
                    
                case DRNActionType.api:
                    
                    performAPIRequest(drn:drn,level:level,action:actionInfo,inputParameters:inputParameters,requestType:apiRequestType)

                    break
                    
                    
                case DRNActionType.redirectUrl:
                    
                        redirect(actionInfo: actionInfo)
                    
                    break
                    
                    
                case DRNActionType.redirectUrlAndApi:
                    
                    performAPIRequest(drn:drn,level:level,action:actionInfo,inputParameters:inputParameters,requestType:apiRequestType)
                    
                    redirect(actionInfo: actionInfo)
                    
                    break
                }
            }

            
        }
        
        func updateActions(actions:NSArray)->NSArray{
            return actions.sorted(by: { (action1, action2) -> Bool in
                
                let orderAction1 = safeInt((action1 as! NSDictionary).object(forKey: "button_order"), alternate: 0)
                
                let orderAction2 = safeInt((action2 as! NSDictionary).object(forKey: "button_order"), alternate: 0)
                
                logMessage("\(orderAction1)\(orderAction2)")
                
                return orderAction1 > orderAction2
            
            }) as NSArray
        }
        
        var actions = level.object(forKey: KEY_ACTIONS) as? NSArray
        //trying loading actions from DRN specifications

        if actions != nil && actions!.count > 0 {
            //actions loaded from DRN specifications
        }else{
            //actions not found DRN specifications
            //trying loading actions from DRN notification
            actions = drn.object(forKey: KEY_ACTIONS) as? NSArray
        }
        
        if actions != nil , actions!.count > 0{
            let updatedActions = updateActions(actions: actions!.reversed() as NSArray)

            for (index, action) in updatedActions.enumerated() {
                
                let title = getActionTitle(drn: drn, level: level, action: action as! NSDictionary)
                
                var needPreffered = isPreffered(title: title)
                if needPreffered == false {
                    if safeBool((action as! NSDictionary).object(forKey: "is_preffered")) {
                        needPreffered = true
                    }
                }
                
                if updatedActions.count == 1 {
                    needPreffered = true
                }
                
                let alertAction = AlertAction(title: title, style: needPreffered ? AlertActionStyle.preferred : AlertActionStyle.normal , handler: { (action) in
                    performOnClickAction(action: action)
                })
                
                alertAction.userInfo = action
                
                alert.add(alertAction)
                
            }
        }else{
            
            alert.add(AlertAction(title: "Ok", style: .normal, handler:{ (action) in
                
                self.hideDRNBackgroundView()
                
            }))
        }
    }
    
    func prepareContentView(drn:NSDictionary,level:NSDictionary,alert:AlertController)->Any{

        //decides the title for the popup
        let title = decideTitle(drn: drn, level: level)
        
        //decides the description for the popup
        let description = decideDescription(drn: drn, level: level)
        
        //decides the type of content view
        /*
         contentType1 -> standard view : it contains title,description only
         contentType2 -> customview to support star rating ,title,description
         contentType3 -> customview to support ride invitation information ,title,description
         contentType4 -> customview to support ride confirmation information ,title,description
         contentType5 -> customview to support ride coordination information ,title,description
         contentType6 -> customview to support settle payment : carowner information ,title,description
         contentType7 -> customview to support join traveller confirmation ,title,description
         contentType8 -> customview to support join traveller ,title,description,travelled with
         */
        let contentTypeRequired = getContentType(level: level)
        
        var drnContentView : Any?
        
        if contentTypeRequired == .contentType1 {
            
            /*
             contentType1 -> standard view : it contains title,description only
             */
            
            let name = safeString(drn.object(forKey: "name"), alternate: "").firstName().capitalized

            let imageUrl = decideImageUrl(drn: drn, level: level)
            
            let titleToShow = title?.replacingOccurrences(of: "[name]", with: name)

            let descriptionToShow = description?.replacingOccurrences(of: "[name]", with: name)

            drnContentView = setContentViewType1(title: titleToShow, description: descriptionToShow, imageUrl: imageUrl,superView:alert.contentView,owner:self)
            
        }else if contentTypeRequired == .contentType2 {
            
            /*
             contentType2 -> customview to support star rating ,title,description
             */
            
            let name = safeString(drn.object(forKey: "name"), alternate: "").firstName().capitalized

            let photo = safeString(drn.object(forKey: "photo"), alternate: "")
            
            let profilePicUrl = getUserProfilePictureUrlFromFileName(photo)
            
            let titleToShow = title?.replacingOccurrences(of: "[name]", with: name)
            
            let descriptionToShow = description?.replacingOccurrences(of: "[name]", with: name)
            
            drnContentView = setContentViewType2(title: titleToShow, description: descriptionToShow, imageUrl: profilePicUrl,superView:alert.contentView,owner:self)
            
        }else if contentTypeRequired == .contentType3 {
            
            /*
             contentType3 -> customview to support ride invitation information ,title,description
             */
            
            let homeAddress = safeString(drn.object(forKey: "h_add"), alternate: "")
           
            let homeTime = safeString(drn.object(forKey: "h_etd"), alternate: "").stringDateToHHMMA()
            
            let name = safeString(drn.object(forKey: "name"), alternate: "").firstName().capitalized
            
            let destinationAddress = safeString(drn.object(forKey: "o_add"), alternate: "")
            
            let destinationTime = safeString(drn.object(forKey: "o_etd"), alternate: "").stringDateToHHMMA()
            
            let photo = safeString(drn.object(forKey: "photo"), alternate: "")
            
            let tag = safeString(drn.object(forKey: "u_tag"), alternate: "")
            
            let profilePicUrl = getUserProfilePictureUrlFromFileName(photo)
            
            let titleToShow = title?.replacingOccurrences(of: "[name]", with: name)
            
            let descriptionToShow = description?.replacingOccurrences(of: "[name]", with: name)
            
            drnContentView = setContentViewType3(title: titleToShow, description: descriptionToShow, homeTime: homeTime, homeAddress: homeAddress, destinationTime: destinationTime, destinationAddress: destinationAddress, imageUrl: profilePicUrl,tag:tag, superView: alert.contentView, owner: self)
            
            
        }else if contentTypeRequired == .contentType4 {
            
            /*
             contentType4 -> customview to support ride confirmation information ,title,description
             */
            
            func getFormattedRideDateAndTime(etd:String,rideDate:Date)->String{
                let rideTime = getTime(timeString: safeString(drn.object(forKey: etd), alternate: ""))!
                let rideTimeAndDate = rideDate.change(rideDate.year(), month: rideDate.month(), day: rideDate.day(), hour: rideTime.hour(), minute: rideTime.minute(), second: rideTime.second)!
                var rideTimeAndDateString = ""
                if rideTimeAndDate.isToday(){
                    rideTimeAndDateString = "Today, \(rideTimeAndDate.toStringValue("h:mm a"))"
                }else if rideTimeAndDate.isTomorrow(){
                    rideTimeAndDateString = "Tomorrow, \(rideTimeAndDate.toStringValue("h:mm a"))"
                }else{
                    rideTimeAndDateString = "\(rideTimeAndDate.toStringValue("dd MMM")) \(rideTimeAndDate.toStringValue("h:mm a"))"
                }
                return rideTimeAndDateString
            }
            
            let rideDate = safeString(drn.object(forKey: "dat"), alternate: "").dateValueType1()
            
            let homeTime = getFormattedRideDateAndTime(etd: "h_etd",rideDate:rideDate)
            
            let isMorning = safeString(drn.object(forKey: "is_morning"), alternate: "")
            
            let name = safeString(drn.object(forKey: "name"), alternate: "").firstName().capitalized
            
            let destinationTime = getFormattedRideDateAndTime(etd: "o_etd",rideDate:rideDate)

            let photo = safeString(drn.object(forKey: "photo"), alternate: "")
            
            let profilePicUrl = getUserProfilePictureUrlFromFileName(photo)
            
            /**
             logged in user Info
             */
            let userInfo = Dbm().getUserInfo()
            
            let profilePicUrlMe = getProfilePictureUrl()
            
            let nameMe = safeString(userInfo!.name, alternate: "")
            
            let myTime = getFormattedRideDateAndTime(etd: "etd",rideDate:rideDate)

            let titleToShow = title?.replacingOccurrences(of: "[name]", with: name)
            
            let descriptionToShow = description?.replacingOccurrences(of: "[name]", with: name)
            
            if safeBool(isMorning) {
            
                drnContentView = setContentViewType4(title: titleToShow, description: descriptionToShow, ur11: profilePicUrl, name1: name, time1: homeTime, ur12:profilePicUrlMe , name2: nameMe, time2: myTime, superView: alert.contentView, owner: self)
            
            }else{
            
                drnContentView = setContentViewType4(title: titleToShow, description: descriptionToShow, ur11: profilePicUrl, name1: name, time1: destinationTime, ur12:profilePicUrlMe , name2: nameMe, time2: myTime, superView: alert.contentView, owner: self)
            
            }
            
            
        }else if contentTypeRequired == .contentType5 {
            
            /*
             contentType5 -> customview to support ride coordination information ,title,description
             */
            
            let name = safeString(drn.object(forKey: "name"), alternate: "").firstName().capitalized
         
            let photo = safeString(drn.object(forKey: "photo"), alternate: "")
            
            let profilePicUrl = getUserProfilePictureUrlFromFileName(photo)
            
            let rating = safeString(drn.object(forKey: "rating"), alternate: "")
            
            let travelMode = safeString(drn.object(forKey: "tr_m"), alternate: "")
            
            let costText = safeString(drn.object(forKey: "cost_text"), alternate: "")
            
            var details = ""
            
            if travelMode == "pass" {
                
                details = "\(rating) ★ | Car-owner | \(costText)"
                
            }else{
                
                details = "\(rating) ★ | Passenger | \(costText)"
                
            }
            
            
            let titleToShow = title?.replacingOccurrences(of: "[name]", with: name)
            
            let descriptionToShow = description?.replacingOccurrences(of: "[name]", with: name)
            
            drnContentView = setContentViewType5(title: titleToShow, description: descriptionToShow, url: profilePicUrl, name: name, details: details, superView: alert.contentView, owner: self)
            
        
        }else if contentTypeRequired == .contentType6 {
            
            /*
             contentType6 -> customview to support settle payment : carowner information ,title,description
             */
            let userInfo = Dbm().getUserInfo()
            
            let photo = safeString(drn.object(forKey: "photo"), alternate: "")
        
            let profilePicUrl1 = getUserProfilePictureUrlFromFileName(photo)
            let profilePicUrl2 = getUserProfilePictureUrlFromFileName(userInfo!.picture)
            
            let name = safeString(drn.object(forKey: "name"), alternate: "").firstName().capitalized
            
            let titleToShow = title?.replacingOccurrences(of: "[name]", with: name)
            
            let descriptionToShow = description?.replacingOccurrences(of: "[name]", with: name)
            
            drnContentView = setContentViewType6(title: titleToShow, description: descriptionToShow, ur11: profilePicUrl1, ur12: profilePicUrl2, superView: alert.contentView, owner: self)
            
        }else if contentTypeRequired == .contentType7 {
            /*
             contentType7 -> customview to support join traveller ,title,description
             */
            let name = safeString(drn.object(forKey: "name"), alternate: "").firstName().capitalized
            
            let photo = safeString(drn.object(forKey: "photo"), alternate: "")
            
            let profilePicUrl = getUserProfilePictureUrlFromFileName(photo)
            
            let rating = safeString(drn.object(forKey: "rating"), alternate: "")
            
            let travelMode = safeString(drn.object(forKey: "tr_m"), alternate: "")
            
            let costText = safeString(drn.object(forKey: "cost_text"), alternate: "")
            
            var details1 = ""
            
            if travelMode == "pass" {
                
                details1 = "\(rating) ★ | Car-owner | \(costText)"
                
            }else{
                
                details1 = "\(rating) ★ | Passenger | \(costText)"
                
            }
            
            let tag = safeString(drn.object(forKey: "u_tag"), alternate: "")

            let destinationTime = safeString(drn.object(forKey: "d_time"), alternate: "").stringDateToHHMMA()

            let destinationAddress = safeString(drn.object(forKey: "d_address"), alternate: "")
            
            let tForCoorporateFforStudent = safeString(drn.object(forKey: "user_t")) == "cp"
            
            
            let details2 = "\(tForCoorporateFforStudent ? "Office" : "College") : \(destinationTime!)"
          
            let details3 = "\(destinationAddress)"
            
            
            let titleToShow = title?.replacingOccurrences(of: "[name]", with: name)
            
            let descriptionToShow = description?.replacingOccurrences(of: "[name]", with: name)
            
            drnContentView = setContentViewType7(title: titleToShow, description: descriptionToShow,tag:tag, url: profilePicUrl, title1: name, description1: details1, description2: details2, description3: details3, superView: alert.contentView, owner: self)
            
        }else if contentTypeRequired == .contentType8 {
            /*
             contentType8 -> customview to support join traveller for car owner,title,description,travelled with
             */
            let name = safeString(drn.object(forKey: "name"), alternate: "").firstName().capitalized
            
            let photo = safeString(drn.object(forKey: "photo"), alternate: "")
            
            let profilePicUrl = getUserProfilePictureUrlFromFileName(photo)

            let rating = safeString(drn.object(forKey: "rating"), alternate: "")
            
            let travelMode = safeString(drn.object(forKey: "tr_m"), alternate: "")
            
            let costText = safeString(drn.object(forKey: "cost_text"), alternate: "")
            
            var details = ""
            
            let tag = safeString(drn.object(forKey: "u_tag"), alternate: "")

            if travelMode == "pass" {
                
                details = "\(rating) ★ | Car-owner | \(costText)"
                
            }else{
                
                details = "\(rating) ★ | Passenger | \(costText)"
                
            }
            
            var user1ProfilePicture = ""
            var user2ProfilePicture = ""
            var user3ProfilePicture = ""
            var user4ProfilePicture = ""
            
            var user1Name = ""
            var user2Name = ""
            var user3Name = ""
            var user4Name = ""
            
            if let riders = drn.object(forKey: "riders") as? NSArray{
                for (index, element) in riders.enumerated() {
                    let name = safeString((element as! NSDictionary).object(forKey: "nm"))
                    let photo = safeString((element as! NSDictionary).object(forKey: "pic"))
                    let userId = safeString((element as! NSDictionary).object(forKey: "id"))
                    if index == 0 {
                        user1ProfilePicture = getUserProfilePictureUrlFromFileName(photo)
                        user1Name = name
                    }else if index == 1 {
                        user2ProfilePicture = getUserProfilePictureUrlFromFileName(photo)
                        user2Name = name
                    }else if index == 2 {
                        user3ProfilePicture = getUserProfilePictureUrlFromFileName(photo)
                        user3Name = name
                    }else if index == 3 {
                        user4ProfilePicture = getUserProfilePictureUrlFromFileName(photo)
                        user4Name = name
                    }
                }
            }
            
            let titleToShow = title?.replacingOccurrences(of: "[name]", with: name)
            
            let descriptionToShow = description?.replacingOccurrences(of: "[name]", with: name)
            
            drnContentView = setContentViewType8(title: titleToShow, description: descriptionToShow, url: profilePicUrl, titleInfo: name, descriptionInfo: details, tag:tag , url1: user1ProfilePicture, url2: user2ProfilePicture, url3: user3ProfilePicture, url4: user4ProfilePicture,name1:user1Name,name2:user2Name,name3:user3Name,name4:user4Name, superView: alert.contentView, owner: self)
        }else if contentTypeRequired == .contentType9 {
            /*
             contentType9 -> customview to support join traveller for passenger ,title,description,travelled with
             */
            let name = safeString(drn.object(forKey: "name"), alternate: "").firstName().capitalized
            
            let photo = safeString(drn.object(forKey: "photo"), alternate: "")
            
            let profilePicUrl = getUserProfilePictureUrlFromFileName(photo)
            
            let rating = safeString(drn.object(forKey: "rating"), alternate: "")
            
            let travelMode = safeString(drn.object(forKey: "tr_m"), alternate: "")
            
            let cost = safeString(drn.object(forKey: "cost_text"), alternate: "")
            
            var details = ""
            
            let tag = safeString(drn.object(forKey: "u_tag"), alternate: "")
            
            if travelMode == "pass" {
                
                details = "\(rating) ★ | Car-owner | \(cost)"
                
            }else{
                
                details = "\(rating) ★ | Passenger | \(cost)"
                
            }
            
            var user1ProfilePicture = ""
            var user2ProfilePicture = ""
            var user3ProfilePicture = ""
            var user4ProfilePicture = ""
            
            var user1Name = ""
            var user2Name = ""
            var user3Name = ""
            var user4Name = ""
            
            if let riders = drn.object(forKey: "riders") as? NSArray{
                for (index, element) in riders.enumerated() {
                    let name = safeString((element as! NSDictionary).object(forKey: "nm"))
                    let photo = safeString((element as! NSDictionary).object(forKey: "pic"))
                    let userId = safeString((element as! NSDictionary).object(forKey: "id"))
                    if index == 0 {
                        user1ProfilePicture = getUserProfilePictureUrlFromFileName(photo)
                        user1Name = name
                    }else if index == 1 {
                        user2ProfilePicture = getUserProfilePictureUrlFromFileName(photo)
                        user2Name = name
                    }else if index == 2 {
                        user3ProfilePicture = getUserProfilePictureUrlFromFileName(photo)
                        user3Name = name
                    }else if index == 3 {
                        user4ProfilePicture = getUserProfilePictureUrlFromFileName(photo)
                        user4Name = name
                    }
                }
            }
            
            let titleToShow = title?.replacingOccurrences(of: "[name]", with: name)
            
            let descriptionToShow = description?.replacingOccurrences(of: "[name]", with: name)
            
            drnContentView = setContentViewType9(title: titleToShow, description: descriptionToShow, url: profilePicUrl, titleInfo: name, descriptionInfo: details, tag:tag , url1: user1ProfilePicture, url2: user2ProfilePicture, url3: user3ProfilePicture, url4: user4ProfilePicture,name1:user1Name,name2:user2Name,name3:user3Name,name4:user4Name, superView: alert.contentView, owner: self)
        }
        return drnContentView!
    }
    
    func getActionType(action:NSDictionary)->DRNActionType{
      
        let actionType =  safeString(action.object(forKey: KEY_ACTION_TYPE), alternate: "")
        
        let actionTag =  safeString(action.object(forKey: KEY_ACTION_TAG), alternate: "")
        
        if actionTag == "call" {
            return DRNActionType.call
        }
        
        if actionTag == "chat" {
            return DRNActionType.chat
        }
        
        if actionTag == "feedback" {
            return DRNActionType.feedback
        }
        
        if actionType == KEY_ACTION_TYPE_LEVEL {
        
            return DRNActionType.level
        
        }else if actionType == KEY_ACTION_TYPE_LEVEL_API {
        
            return DRNActionType.apiThenLevel
        
        }else if actionType == KEY_ACTION_TYPE_DISMISS {

        
        }else if actionType == KEY_ACTION_TYPE_REDIRECT {
            
            
        }else if actionType == KEY_ACTION_TYPE_REDIRECT_APP {
            
            return DRNActionType.redirectApp
            
        }else if actionType == KEY_ACTION_TYPE_API {
            
            return DRNActionType.api
            
        }else if actionType == KEY_ACTION_TYPE_REDIRECT_URL {
            
            return DRNActionType.redirectUrl
            
        }else if actionType == KEY_ACTION_TYPE_REDIRECT_URL_AND_API {
            
            return DRNActionType.redirectUrlAndApi
            
        }

        return DRNActionType.dismiss
    }
    
    
    
    
    func getContentType(level:NSDictionary)->DRNContentViewType{
       
        if let levelType = level.object(forKey: KEY_LEVEL_TYPE) as? String{
            
            if levelType == KEY_LEVEL_TYPE_STANDARD {
                
                return DRNContentViewType.contentType1
                
            }else if levelType == KEY_LEVEL_TYPE_RATE {
                
                return DRNContentViewType.contentType2
                
            }else if levelType == KEY_LEVEL_TYPE_INVITE {
                
                return DRNContentViewType.contentType3
                
            }else if levelType == KEY_LEVEL_TYPE_RIDE_CONFIRMATION {
                
                return DRNContentViewType.contentType4
                
            }else if levelType == KEY_LEVEL_TYPE_RIDE_COORDINATION {
                
                return DRNContentViewType.contentType5
                
            }else if levelType == KEY_LEVEL_TYPE_RIDE_COORDINATION {
                
                return DRNContentViewType.contentType5
                
            }else if levelType == KEY_LEVEL_TYPE_SETTLE_PAYMENT_CAR_OWNER {
                
                return DRNContentViewType.contentType6
                
            }else if levelType == KEY_LEVEL_TYPE_REGULAR_PARTNER {
                
                return DRNContentViewType.contentType5
                
            }else if levelType == KEY_LEVEL_TYPE_JOIN_TRAVELLER_CONFIRMATION {
                
                return DRNContentViewType.contentType7
                
            }else if levelType == KEY_LEVEL_TYPE_JOIN_TRAVELLER_CAROWNER {
                
                return DRNContentViewType.contentType8
                
            }else if levelType == KEY_LEVEL_TYPE_JOIN_TRAVELLER_PASSENGER {
                
                return DRNContentViewType.contentType9
                
            }
        }
        
        return DRNContentViewType.contentType1
    }
    
    
    
    
    func drnId(drn:NSDictionary)->String?{
        
        if let drnIdString = drn.object(forKey: KEY_DRN_ID) as? String{
        
            return drnIdString
        
        }else if let drnIdInt = drn.object(forKey: KEY_DRN_ID) as? Int{
            
            showNotification("drn Id is Invalid \(drnIdInt)", showOnNavigation: false, showAsError: true, duration: 5)
            return nil
        
        }
        
        return nil
    }
    
    
    
    
    func drnLevel(drn:NSDictionary,levelId:String)->NSDictionary?{
      
        if let drnIdString = drn.object(forKey: KEY_DRN_ID) as? String{
        
            for spec in self.specifications {
            
                if let drnIdSpec = (spec as! NSDictionary).object(forKey: KEY_DRN_ID) as? String{
                
                    if drnIdSpec == drnIdString {
                    
                        if let levels = (spec as! NSDictionary).object(forKey: KEY_LEVELS) as? NSArray{
                        
                            for level in levels {
                            
                                if let levelIdSpec = (level as! NSDictionary).object(forKey: KEY_LEVEL_ID) as? String {
                                
                                    if levelIdSpec == levelId {
                                    
                                        return (level as! NSDictionary)
                                    
                                    }
                                
                                }
                            
                            }
                        
                        }
                    
                    }
                
                }
            
            }
        
        }
        
        return nil
    }
    
    
    
    
    func getImageUrl(fileName:String?)->String?{
        if isNotNull(fileName) {
            return BASE_URL_DRN_IMAGE + fileName!
        }else{
            return nil
        }
    }
    
    
    
    
    func getActionTitle(drn:NSDictionary,level:NSDictionary,action:NSDictionary)->String{
        let title = action.object(forKey: KEY_TITLE) as? String
        if let titleType = action.object(forKey: KEY_TITLE_TYPE) as? String {
            if titleType == KEY_CALCULATE_ETD {
                if let titleDelta = action.object(forKey: KEY_TITLE_DELTA) as? String {
                    if let time = drn.object(forKey: KEY_TIME) as? String {
                        if let date = drn.object(forKey: KEY_DATE) as? String {
                            let dateOfRide = getDate(dateString: date)
                            let timeOfRide = getTime(timeString: time)
                            let dateAndTime = calculateActionETD(action: action, dateOfRide: dateOfRide!, timeOfRide: timeOfRide!, titleDelta: titleDelta)
                            return dateAndTime.toStringValue("h:mm a")
                        }
                    }
                }
            }
        }
        return title ?? ""
    }
    
    
    
    
    func getActionSpecificParameter(drn:NSDictionary,level:NSDictionary,action:NSDictionary)->String?{
        if let titleType = action.object(forKey: KEY_TITLE_TYPE) as? String {
            if titleType == KEY_CALCULATE_ETD {
                if let titleDelta = action.object(forKey: KEY_TITLE_DELTA) as? String {
                    if let time = drn.object(forKey: KEY_TIME) as? String {
                        if let date = drn.object(forKey: KEY_DATE) as? String {
                            let dateOfRide = getDate(dateString: date)
                            let timeOfRide = getTime(timeString: time)
                            let dateAndTime = calculateActionETD(action: action, dateOfRide: dateOfRide!, timeOfRide: timeOfRide!, titleDelta: titleDelta)
                            return dateAndTime.stringTimeOnly24HFormatValue()
                        }
                    }
                }
            }
        }
        return nil
    }

    
    
    func decideImageUrl(drn:NSDictionary,level:NSDictionary)->String?{
        if safeBool(level.object(forKey: "dynamicImage")) {
            return getImageUrl(fileName: drn.object(forKey: KEY_IMAGE_DYNAMIC) as? String)
        }else{
            return getImageUrl(fileName: level.object(forKey: KEY_IMAGE_URL) as? String)
        }
    }
    
    
    
    
    func getAction(drn:NSDictionary,level:NSDictionary,action:NSDictionary)->Action {
        if let apiRequestTag = action.object(forKey: KEY_API_REQUEST_TAG) as? String {
            if apiRequestTag == "ride_quest_no" {
                return Action.RideQuestion_SendInvitation_No
            }else if apiRequestTag == "ride_quest_later_no" {
                return Action.RideQuestion_Later_No
            }else if apiRequestTag == "ride_quest_later_schedule" {
                return Action.RideQuestion_Later_Schedule
            }else if apiRequestTag == "ride_quest_yes" {
                return Action.RideQuestion_SendInvitation_Yes
            }else if apiRequestTag == "ride_quest_accept_yes" {
                return Action.RideQuestion_AcceptInvitation_Yes
            }else if apiRequestTag == "ride_quest_accept_no" {
                return Action.RideQuestion_AcceptInvitation_No
            }else if apiRequestTag == "ride_quest_accept_cancel" {
                return Action.RideQuestion_AcceptInvitation_Cancel
            }else if apiRequestTag == "ride_quest_confirmation_cancel" {
                return Action.RideConfirmation_Cancel
            }else if apiRequestTag == "ride_settle_payment" {
                return Action.SettlePayment_Settle
            }else if apiRequestTag == "ride_feedback" {
                return Action.RideFeedback_Send
            }else if apiRequestTag == "ride_apology_now" {
                return Action.Apology_TryNow
            }else if apiRequestTag == "ride_broken_invite_regular" {
                return Action.RideBroken_InviteRegular
            }else if apiRequestTag == "ride_broken_invite_new" {
                return Action.RideBroken_InviteNew
            }else if apiRequestTag == "ride_broken_not_intrested" {
                return Action.RideBroken_NotIntrested
            }else if apiRequestTag == "ride_suggestion_ride_together" {
                return Action.RideSuggestion_RideTogether
            }else if apiRequestTag == "ride_suggestion_issue" {
                return Action.RideSuggestion_Issue
            }else if apiRequestTag == "auto_fix_yes" {
                return Action.AutoFix_Yes
            }else if apiRequestTag == "auto_fix_no" {
                return Action.AutoFix_No
            }
        }
        if let actionType = action.object(forKey: KEY_ACTION_TYPE) as? String {
            if actionType == KEY_ACTION_TYPE_API {
                return Action.GenericAPIDynamicURL
            }else if actionType == KEY_ACTION_TYPE_REDIRECT_URL_AND_API {
                return Action.GenericAPIDynamicURL
            }
        }
        return Action.UnIdentified
    }

    
    fileprivate func startUpInitialisations(){
        updateDrnSpecifications()
        let jsonFileName = KEY_JSON_FILE_NAME
        let filePath = Bundle.main.path(forResource: jsonFileName, ofType: nil)
        do{
            let responseData = parsedJson(try Data(contentsOf: URL(fileURLWithPath: filePath!)), methodName: #function)
            if let specificationsFetched = responseData?.object(forKey: "data") as? NSMutableArray {
                specifications = specificationsFetched
            }
        }catch{}
    }
    
    
    func updateDrnSpecifications(){
        if isInternetConnectivityAvailable(false) {
            let information = NSMutableDictionary()
            let scm = ServerCommunicationManager()
            scm.getDrnSpecifications(information) { (responseData) -> () in
                if let specificationsFetched = responseData?.object(forKey: "data") as? NSMutableArray {
                    CacheManager.sharedInstance.saveObject(specificationsFetched, identifier: CACHED_DRN_SPECIFICATIONS)
                }
            }
        }
    }
    
    
    func showDRNBackgroundView(){
        let presentedVC = Acf().navigationController?.presentedViewController
        if  presentedVC != nil && presentedVC is DRNPresenterViewController {
            //already presented ignore
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(DRNManager.hideDRNBackgroundView), object: nil)
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(DRNManager.hideDRNBackgroundViewPrivate), object: nil)
            presentedVC?.view.layer.opacity = 1.0
        }else{
            presentedVC?.dismiss(animated: false, completion: nil)
            presentedVC?.view.layer.opacity = 1.0
            Acf().navigationController?.present(presenterVC, animated: false, completion: nil)
        }
    }
    
    
    @objc func hideDRNBackgroundView(){
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(DRNManager.hideDRNBackgroundViewPrivate), object: nil)
        self.perform(#selector(DRNManager.hideDRNBackgroundViewPrivate), with: nil, afterDelay: 0.3)
    }
    
    
    @objc func hideDRNBackgroundViewPrivate(){
        let presentedVC = Acf().navigationController?.presentedViewController
        if  presentedVC != nil && presentedVC is DRNPresenterViewController {
            UIView.animate(withDuration: 0.6, animations: {
                presentedVC?.view.layer.opacity = 0.0
            }, completion: { (completed) in
                presentedVC?.dismiss(animated: true, completion: {
                    presentedVC?.view.layer.opacity = 1.0
                })
            })
        }
    }
    
    
    
    func showDRNPushNotificationReceivedForDebugging(drn:NSDictionary){
        if debuggingEnabled() {
            let parametersReceived = NSMutableString(string: "Parameters received with Notification\n")
            for key in drn.allKeys {
                if let value = drn.object(forKey:key) as? String {
                    parametersReceived.append("\(key) : \(value) \n")
                }
            }
            showNotification(parametersReceived as String, showOnNavigation: true, showAsError: false, duration: 5)
        }
    }
    
    
    
    
    func debuggingEnabled()->Bool{
        return ENABLE_FEATURE_DEBUG_INFO
    }
    
    func cacheAllImages(){
        if isNotNull(specifications){
            do{
                let specificationsData = try JSONSerialization.data(withJSONObject: specifications, options: JSONSerialization.WritingOptions.prettyPrinted)
                let jsonString = NSString(data: specificationsData, encoding: String.Encoding.utf8.rawValue)
                var imageNames = [String]()
                for i in DRN_ID_MIN ... DRN_ID_MAX {
                    for j in IMAGE_SEQUECE_MIN ... IMAGE_SEQUECE_MAX {
                        let imageName = "\(i)_\(j).png"
                        if jsonString!.contains(imageName) {
                            logMessage("found \(imageName)")
                            imageNames.append(imageName)
                        }
                    }
                }
                for imageName in imageNames {
                    let url = URL(string: getImageUrl(fileName: imageName)!)
                    if SDWebImageManager.shared().diskImageExists(for: url) == false {
                        logMessage("cache not exist \(imageName)")
                        SDWebImageManager.shared().downloadImage(with:url, options: SDWebImageOptions.retryFailed, progress: { (x, y) in
                        }, completed: { (i, e, c, ce, u) in
                            if e != nil {
                                logMessage("cache downloaded \(imageName)")
                            }
                        })
                    }else{
                        logMessage("cache already exist \(imageName)")
                    }
                }
            }catch{}
        }
    }
    
    func getDate(dateString:String)->Date?{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.date(from: dateString)
    }

    func getTime(timeString:String)->Date?{
        let formatter = DateFormatter()
        if timeString.length > 5 {
            formatter.dateFormat = "HH:mm:ss"
        }else{
            formatter.dateFormat = "HH:mm"
        }
        return formatter.date(from: timeString)
    }
    
    func getDRNScreenEventName(drn:NSDictionary)->String{
        if let drnId = drn.object(forKey: KEY_DRN_ID) as? String{
            if drnId == DRN_ID_1001 {
                return SLE_D_RIDE_QUEST
            }else if drnId == DRN_ID_1002{
                return SLE_D_RIDE_REQUEST
            }else if drnId == DRN_ID_1003{
                return SLE_D_RIDE_REQUEST
            }else if drnId == DRN_ID_1004{
                return SLE_D_RIDE_CONFIRMATION
            }else if drnId == DRN_ID_1005{
                return SLE_D_RIDE_CONFIRMATION
            }else if drnId == DRN_ID_1006{
                return SLE_D_RIDE_SETTLE
            }else if drnId == DRN_ID_1007{
                return SLE_D_RIDE_SETTLE
            }else if drnId == DRN_ID_1008{
                return SLE_D_RIDE_SETTLE
            }else if drnId == DRN_ID_1009{
                return SLE_D_RIDE_FEEDBACK
            }else if drnId == DRN_ID_1010{
                return SLE_D_RIDE_APOLOGY
            }else if drnId == DRN_ID_1011{
                return SLE_D_RIDE_APOLOGY
            }else if drnId == DRN_ID_1012{
                return SLE_D_RIDE_BROKEN
            }else if drnId == DRN_ID_1013{
                return SLE_D_RIDE_ALREADY_CONFIRMED
            }else if drnId == DRN_ID_1014{
                return SLE_D_RIDE_SUGGESTION
            }else if drnId == DRN_ID_1015{
                return SLE_D_RIDE_SUGGESTION
            }else if drnId == DRN_ID_1020{
                return SLE_D_GENERIC_DRN
            }else if drnId == DRN_ID_1021{
                return SLE_D_READONLY_NOTIFICATION_DRN
            }
        }
        return "unKnown"
    }
    
    func updateUserAccountBalance(){
        if isInternetConnectivityAvailable(false){
            let information = NSMutableDictionary()
            copyData(loggedInUserId() , destinationDictionary: information, destinationKey: "userId", methodName: #function)
            let scm = ServerCommunicationManager()
            scm.responseErrorOption = ResponseErrorOption.dontShowErrorResponseMessage
            scm.returnFailureResponseAlso = true
            scm.getAccountSummary(information, completionBlock: {[weak self] (responseData) -> () in guard let `self` = self else { return }
                if isNotNull(responseData?.object(forKey: "data")){
                    let data = (responseData?.object(forKey: "data")) as! NSDictionary
                    if let accountBalanceAsString = data.object(forKey: "accountBal") as? String {
                        self.currentAccountBalance = safeInt(accountBalanceAsString)
                        self.timeWhenAccountBalanceUpdated = Date()
                    }
                }
            })
        }
    }
    
    func getImportantDRNParameters(drn:NSDictionary)->NSDictionary{
        let information = NSMutableDictionary()
        copyData(drn, sourceKey: KEY_DRN_INSTANCE_ID, destinationDictionary: information, destinationKey: KEY_DRN_INSTANCE_ID, methodName: #function)
        copyData(drn, sourceKey: KEY_DRN_ID, destinationDictionary: information, destinationKey: KEY_DRN_ID, methodName: #function)
        return information
    }
}





/*
 short cut to access DRNManager shared instance
 */

func Drnm()->DRNManager{
    return DRNManager.sharedInstance
}

/*
 Some Commonly used keys,identifiers
*/

let KEY_LEVELS = "levels"
let KEY_DRN_ID = "drn_id"
let KEY_DRN_INSTANCE_ID = "drn_i_id"
let KEY_LEVEL_ID = "level_id"
let KEY_LEVEL_TYPE = "level_type"
let KEY_LEVEL_TYPE_STANDARD = "standard"
let KEY_LEVEL_TYPE_INVITE = "invite"
let KEY_LEVEL_TYPE_RIDE_CONFIRMATION = "ride_confirmation"
let KEY_LEVEL_TYPE_RIDE_COORDINATION = "ride_coordination"
let KEY_LEVEL_TYPE_SETTLE_PAYMENT_CAR_OWNER = "settle_co"
let KEY_LEVEL_TYPE_REGULAR_PARTNER = "regular_partner"
let KEY_LEVEL_TYPE_JOIN_TRAVELLER_CAROWNER = "join_traveller_carowner"
let KEY_LEVEL_TYPE_JOIN_TRAVELLER_PASSENGER = "join_traveller_passenger"
let KEY_LEVEL_TYPE_JOIN_TRAVELLER_CONFIRMATION = "join_traveller_confirmation"
let KEY_LEVEL_TYPE_RATE = "rate"
let KEY_TITLE = "title"
let KEY_API_REQUEST_TAG = "api_req_tag"
let KEY_TITLE_TYPE = "title_type"
let KEY_TITLE_DELTA = "title_delta"
let KEY_TITLE_CALCULATION_TYPE = "title_calculation_type"
let KEY_TITLE_CALCULATION_TYPE_CURRENT_TIME = "ctime"
let KEY_CALCULATE_ETD = "cal_etd"
let KEY_TIME = "etd"
let KEY_DATE = "dat"
let KEY_TRAVEL_MODE = "tr_m"
let KEY_USER_ID = "u_id"
let KEY_NEXT_LEVEL_ID = "next_level_id"
let KEY_DESCRIPTION = "desc"
let KEY_DRN_NAME = "name"
let KEY_IMAGE_URL = "img_url"
let KEY_IMAGE_DYNAMIC = "img"
let KEY_ACTIONS = "actions"
let KEY_ACTION_TYPE = "type"
let KEY_ACTION_TAG = "tag"
let KEY_API_REQUEST_TYPE = "api_req_type"
let KEY_ACTION_TYPE_LEVEL = "level"
let KEY_ACTION_TYPE_LEVEL_API = "level_api"
let KEY_ACTION_TYPE_DISMISS = "dismiss"
let KEY_ACTION_TYPE_REDIRECT = "redirect"
let KEY_ACTION_TYPE_REDIRECT_APP = "redirect_app"
let KEY_ACTION_TYPE_API = "api"
let KEY_ACTION_TYPE_REDIRECT_URL = "redirect_url"
let KEY_ACTION_TYPE_REDIRECT_URL_AND_API = "redirect_api_url"
let KEY_JSON_FILE_NAME = "DRNSpecifications.json"

let L1 = "l1"
let L2 = "l2"
let L3 = "l3"
let L4 = "l4"
let L5 = "l5"
let L6 = "l6"
let L7 = "l7"
let L8 = "l8"

let DRN_ID_1001 = "1001"
let DRN_ID_1002 = "1002"
let DRN_ID_1003 = "1003"
let DRN_ID_1004 = "1004"
let DRN_ID_1005 = "1005"
let DRN_ID_1006 = "1006"
let DRN_ID_1007 = "1007"
let DRN_ID_1008 = "1008"
let DRN_ID_1009 = "1009"
let DRN_ID_1010 = "1010"
let DRN_ID_1011 = "1011"
let DRN_ID_1012 = "1012"
let DRN_ID_1013 = "1013"
let DRN_ID_1014 = "1014"
let DRN_ID_1015 = "1015"
let DRN_ID_1020 = "1020"
let DRN_ID_1021 = "1021"

let DRN_ID_MIN = 1001
let DRN_ID_MAX = 1015
let IMAGE_SEQUECE_MIN = 1
let IMAGE_SEQUECE_MAX = 9

/*
 DRNContentViewType : An enum to easily distinguish between various custom views that DRN is using eg: rate view ,invite user view , standard etc.
 contentType1 -> standard view : it contains title,description only
 contentType2 -> customview to support star rating ,title,description
 contentType3 -> customview to support ride invitation information ,title,description
 contentType4 -> customview to support ride confirmation information ,title,description
 contentType5 -> customview to support ride coordination information ,title,description
 contentType6 -> customview to support settle payment : carowner information ,title,description
 contentType7 -> customview to support join traveller confirmation ,title,description
 contentType8 -> customview to support join traveller ,title,description,travelled with
 */
enum DRNContentViewType {
    case contentType1
    case contentType2
    case contentType3
    case contentType4
    case contentType5
    case contentType6
    case contentType7
    case contentType8
    case contentType9
}


/*
 Action : An enum to easily distinguish between various possible actions , it contains only specific actions , where app wants to perform some specific tasks related to DRNs.
 */
enum Action {
    case RideQuestion_SendInvitation_No
    case RideQuestion_SendInvitation_Yes
    case RideQuestion_AcceptInvitation_No
    case RideQuestion_AcceptInvitation_Yes
    case RideQuestion_AcceptInvitation_Cancel
    case RideQuestion_Later_No
    case RideQuestion_Later_Schedule
    case RideConfirmation_Cancel
    case SettlePayment_Settle
    case RideFeedback_Send
    case Apology_TryNow
    case RideBroken_InviteRegular
    case RideBroken_InviteNew
    case RideBroken_NotIntrested
    case RideSuggestion_RideTogether
    case RideSuggestion_Issue
    case AutoFix_Yes
    case AutoFix_No
    case GenericAPIDynamicURL
    case UnIdentified//This case should appear , if it appears that means some logic is wrong , and developer should correct it.
}


/*
 DRNActionType : An enum to easily distinguish between various action types.
 description:
 level -> open next level
 apiThenLevel -> hit an API first , then open next level
 dismiss -> dismiss the popup
 call -> call to phone number and dismiss
 chat -> initiate chat and dismiss
 feedback -> initiate feedback page and dismiss
 */
enum DRNActionType {
    case level
    case apiThenLevel
    case dismiss
    case call
    case chat
    case feedback
    case redirectApp
    case api
    case redirectUrl
    case redirectUrlAndApi
}


/*
 DRNPresenterViewController : This is a viewcontroller , with blurred screen shot of iOS Home Screen as background, just to give a feel like user is not inside of application.
 */

class DRNPresenterViewController: UIViewController {
    override var prefersStatusBarHidden : Bool {
        return true
    }
}

