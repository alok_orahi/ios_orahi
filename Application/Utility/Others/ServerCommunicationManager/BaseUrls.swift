//
//  BaseUrls.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

//DEVELOPMENT

//var BASE_URL = "http://dev.orahi.com/beta/mob/"
//var BASE_URL_POM = "http://dev.orahi.com:8080/"

//LIVE
//
var BASE_URL = "https://api.orahi.com/mob/"
var BASE_URL_POM = "https://pom.orahi.com/"

var BASE_URL_FOR_IMAGE_UPLOADS = "https://www.orahi.com/mob/"
var BASE_URL_FOR_OFFER_IMAGES = "https://www.orahi.com/images/"
var BASE_URL_PROFILE_IMAGE = "https://www.orahi.com/"
var BASE_URL_DRN_IMAGE = "https://www.orahi.com/images/drn/"
var BASE_URL_IMAGE_POM = BASE_URL_POM
var BASE_URL_RECHARGE = "https://api.orahi.com/"

//MARK: - Url's
struct ServerSupportURLS {
    //authentication
    static let AUTHENTICATION_STEP_1 = "quickonboard/sendotp/"
    static let AUTHENTICATION_STEP_2 = "quickonboard/verifyotp/"
    static let UPDATE_PROFILE = "quickonboard/updateProfile/"
    
    //user profile
    static let GET_USER_PROFILE = "profile/getProfile"
    static let GET_SHORT_USER_PROFILE = "profile/getshortprofile"
    static let UPDATE_PROFILE_PICTURE = "account/profilepicsave"
    static let UPLOAD_PICTURE = "profilesave.php"
    
    //ride
    static let SET_MY_RIDE = "travel/fixmyride"
    static let GET_AVAILABLE_USERS = "find/gettravellers"
    static let SEND_INVITE_FOR_RIDE = "travel/sendinvite"
    static let ACCEPT_INVITE_FOR_RIDE = "travel/acceptRequest"
    static let REJECT_INVITE_FOR_RIDE = "travel/rejectRequest"
    static let CANCEL_RIDE = "travel/cancelRide"
    static let SETTLE_RIDE = "travel/settleRide"
    
    //favourites
    static let UPDATE_PRIORITY = "favourites/updateFavorites"
    static let SET_FAVOURITE = "favourites/addDelFav"
    static let SET_UN_FAVOURITE = "favourites/addDelFav"
    static let GET_FAVOURITES = "favourites/getFavourites"
    
    //block
    static let SET_BLOCKED = "favourites/blockUnblock"
    static let SET_UN_BLOCKED = "favourites/blockUnblock"
    
    //feedbacks
    static let GET_FEEDBACKS = "rating/getFeedbacks/"
    
    //search
    static let SEARCH_USERS = "find/user/"
    
    //recharge
    static let RECHARGE = "account/mobrecharge"
    
    //weekly ride plan
    static let WEEKLY_RIDE_PLAN = "travel/getPlan"
    
    //update plan
    static let UPDATE_PLAN = "travel/updatePlan"
    
    //settings
    static let GET_ALL_SETTINGS = "settings/getSettings"
    static let UPDATE_MOBILE_NOTIFICATION_SETTINGS = "settings/updatemobile/"
    static let UPDATE_WEBSITE_NOTIFICATION_SETTINGS = "settings/updateweb/"
    static let UPDATE_SMS_NOTIFICATION_SETTINGS = "settings/updatesms/"
    static let UPDATE_EMAIL_NOTIFICATION_SETTINGS = "settings/updateemail/"
    static let UPDATE_DAILY_DND_SETTINGS = "settings/updateDNDdates"
    static let UPDATE_WEEKLY_DND_SETTINGS = "settings/updateDNDdays/"
    
    //accounts
    static let GET_ACCOUNT_SUMMARY = "account/details"
    static let WITHDRAWL = "account/withdrawal"
    
    //city company car
    static let GET_CITY_COMPANY_CAR_LIST = "profile/dropvalues/city/0/company/0/make/0/model/0/"
    
    //contact list
    static let GET_CONTACTS = "profile/getContacts"
    
    //ranking
    static let GET_RANKING = "support/ranking"
    
    //status
    static let SET_STATUS = "profile/updateStatus"
    
    //feedback
    static let FEEDBACK = "rating/sendFeedback"
    
    //verification
    static let VERIFY_COORPORATE_EMAIL = "account/emailverification/"
    static let UPLOAD_GOVERNMENT_ID_PROOF = "docverification.php"
    static let SAVE_GOVERNMENT_ID_PROOF = "account/docverification"
    static let UPLOAD_COLLEGE_ID_PROOF = "docverification.php"
    static let SAVE_COLLEGE_ID_PROOF = "account/docverification"
    
    //report
    static let INVITE_FOR_CHAT = "onboard/chatrequest"
    
    //offers
    static let GET_OFFERS = "offers/getOffers"
    static let GET_RIDE_COSTING_DETAILS = "travel/ride_cost_details"
    static let CLAIM_OFFER = "offers/claimOffers"
    
    //user tracking
    static let UPDATE_USER_TRACKED_DETAILS = "event/travel"
    
    //ride completion tracking
    static let TRACK_RIDE_COMPLETION_EVENT = "event/travelComplete"
    
    //savings
    static let GET_SAVINGS = "account/savings"
    
    //update userType
    static let SET_USERTYPE = "profile/setUserPreferences"

    //Travel Preferences
    static let SET_TRAVEL_PREFERENCES = "profile/setUserPreferences"
    static let GET_TRAVEL_PREFERENCES = "profile/getUserPreferences"
        
    //Expert ride Alerts
    static let SET_EXPERT_RIDE_ALERTS_PREFERENCES = "drn/update_drn_status"
    
    //Show Phone Number Preference
    static let SET_SHOW_PHONE_NUMBER_PREFERENCES = "profile/call_option"
    
    //Group
    static let MANAGE_GROUP = "group/manage_group/"
    static let GET_ALL_GROUPS = "group/get_all_groups/"
    static let REQUEST_TO_SHARE_LOCATION = "user/request_location_sharing/"
    static let CONFIGURE_GROUP_SETTINGS = "group/configure_group_setting/"
    static let LEAVE_GROUP = "group/leave_group/"
    static let JOIN_GROUP = "group/join_group/"
    static let CONFIGURE_PLACE_SETTINGS = "group/configure_place_setting/"
    
    //Places
    static let MANAGE_PLACE = "place/manage_place/"
    static let GET_ALL_PLACES = "place/get_all_places/"

    //SOS
    static let UPDATE_EMERGENCY_CONTACTS = "user/update_emergency_contacts/"
    static let GET_EMERGENCY_CONTACTS = "user/get_emergency_contacts/"
    static let PERFORM_SOS = "user/send_emergency_alerts/"
    static let CHECK_IN = "user/check_in/"

    //NOTIFICATIONS
    static let GET_NOTIFICATIONS = "notification/get_notifications/"
    static let CONFIGURE_GROUP_PUSH_NOTIFICATIONS = "notification/configure_push_notifications/"

    //OTHERS
    static let UPDATE_LATEST_LOCATION = "location/update/"
    static let CREATE_NOTIFICATION = "notification/create_notification/"
    
}
