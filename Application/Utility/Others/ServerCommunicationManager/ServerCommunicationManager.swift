//
//  ServerCommunicationManager.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

//MARK: - ServerCommunicationManager : This class handles communication of application with its Server.

import Foundation
import AFNetworking

//MARK: - ServerCommunicationConstants
struct ServerCommunicationConstants {
    static let RESPONSE_CODE_KEY = "status"
    static let RESPONSE_CODE_SUCCESS_VALUE = "Optional(1)"
    static let RESPONSE_CODE_FAILURE_VALUE = "Optional(0)"
    static let RESPONSE_MESSAGE_KEY = "message"
}

//MARK: - Response Error Handling Options
enum ResponseErrorOption {
    case dontShowErrorResponseMessage
    case showErrorResponseUsingLocalNotification
    case showErrorResponseWithUsingNotification
    case showErrorResponseWithUsingPopUp
}

enum MessageOption {
    case showSuccessResponseMessageUsingNotification
    case showSuccessResponseMessageUsingPopUp
}

//MARK: - Response Error Handling Options
enum ApiRequestType {
    case get
    case post
}

//MARK: - Completion block
typealias WSCompletionBlock = (_ responseData :NSDictionary?) ->()
typealias WSCompletionBlockForFile = (_ responseData :NSData?) ->()

//MARK: - Custom methods
extension ServerCommunicationManager {
    //MARK: - Authentication
    
    func authenticateStep1(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "phone", destinationDictionary: parameters, destinationKey: "mobile", methodName:#function)
        addCommonInformation(parameters)
        performPostRequest(parameters, urlString: BASE_URL+ServerSupportURLS.AUTHENTICATION_STEP_1, completionBlock: completionBlock!,methodName:#function)
    }
    
    
    func authenticateStep2(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "phone", destinationDictionary: parameters, destinationKey: "mobile", methodName:#function)
        copyData(information, sourceKey: "otp", destinationDictionary: parameters, destinationKey: "otp", methodName:#function)
        copyData(information, sourceKey: "coupon", destinationDictionary: parameters, destinationKey: "coupon", methodName:#function)
        addCommonInformation(parameters)
        performPostRequest(parameters, urlString: BASE_URL+ServerSupportURLS.AUTHENTICATION_STEP_2, completionBlock: completionBlock!,methodName:#function)
    }
    
    
    //MARK: - PROFILE
    
    func getUserProfile(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "userId", destinationDictionary: parameters, destinationKey: "id", methodName:#function)
        if isUserLoggedIn() {
            copyData(loggedInUserId() , destinationDictionary: parameters, destinationKey: "by", methodName: #function)
        }
        performGetRequest(parameters, urlString: BASE_URL+ServerSupportURLS.GET_USER_PROFILE, completionBlock: completionBlock,methodName:#function)
    }
    
    func getShortProfile(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "userId", destinationDictionary: parameters, destinationKey: "id", methodName:#function)
        performGetRequest(parameters, urlString: BASE_URL+ServerSupportURLS.GET_USER_PROFILE, completionBlock: completionBlock,methodName:#function)
    }
    
    func updateProfilePicture(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "imageName", destinationDictionary: parameters, destinationKey: "filename", methodName:#function)
        copyData(loggedInUserId() , destinationDictionary: parameters, destinationKey: "id", methodName: #function)
        performGetRequest(parameters, urlString: BASE_URL+ServerSupportURLS.UPDATE_PROFILE_PICTURE, completionBlock: completionBlock,methodName:#function)
        trackFunctionalEvent(FE_UPDATE_PROFILE_PICTURE, information: parameters)
    }
    
    func uploadImage(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        if information.object(forKey: "image") != nil {
            parseSourceForImageAndAttachEncodedImage(s: information, skey: "image", d: parameters, dkey: "encodedString")
            copyData(information, sourceKey: "imageName", destinationDictionary: parameters, destinationKey: "img_name", methodName:#function)
        }
        copyData(loggedInUserId() , destinationDictionary: parameters, destinationKey: "id", methodName: #function)
        performPostRequest(parameters, urlString: BASE_URL_FOR_IMAGE_UPLOADS+ServerSupportURLS.UPLOAD_PICTURE, completionBlock: completionBlock!,methodName:#function)
    }
    
    func updateUserProfile(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "name", destinationDictionary: parameters, destinationKey: "name", methodName:#function)
        copyData(information, sourceKey: "email", destinationDictionary: parameters, destinationKey: "email", methodName:#function)
        copyData(information, sourceKey: "userType", destinationDictionary: parameters, destinationKey: "user_type", methodName:#function)
        copyData(information, sourceKey: "mobile", destinationDictionary: parameters, destinationKey: "mobile", methodName:#function)
        copyData(information, sourceKey: "gender", destinationDictionary: parameters, destinationKey: "gender", methodName:#function)
        copyData(information, sourceKey: "travelMode", destinationDictionary: parameters, destinationKey: "travel_mode", methodName:#function)
        copyData(information, sourceKey: "carRegistrationNumber", destinationDictionary: parameters, destinationKey: "car_reg_no", methodName:#function)
        copyData(information, sourceKey: "make", destinationDictionary: parameters, destinationKey: "make", methodName:#function)
        copyData(information, sourceKey: "model", destinationDictionary: parameters, destinationKey: "model", methodName:#function)
        copyData(information, sourceKey: "companyId", destinationDictionary: parameters, destinationKey: "comp_id", methodName:#function)
        copyData(information, sourceKey: "destinationName", destinationDictionary: parameters, destinationKey: "company_name_other", methodName:#function)
        copyData(information, sourceKey: "home", destinationDictionary: parameters, destinationKey: "home_loc_text", methodName:#function)
        copyData(information, sourceKey: "destination", destinationDictionary: parameters, destinationKey: "office_loc_text", methodName:#function)
        copyData(information, sourceKey: "homePickUp", destinationDictionary: parameters, destinationKey: "homepickup", methodName:#function)
        copyData(information, sourceKey: "destinationPickUp", destinationDictionary: parameters, destinationKey: "officepickup", methodName:#function)
        copyData(information, sourceKey: "home_loc", destinationDictionary: parameters, destinationKey: "home_loc", methodName:#function)
        copyData(information, sourceKey: "ofc_loc", destinationDictionary: parameters, destinationKey: "ofc_loc", methodName:#function)
        copyData(information, sourceKey: "homeTime", destinationDictionary: parameters, destinationKey: "home_etd", methodName:#function)
        copyData(information, sourceKey: "destinationTime", destinationDictionary: parameters, destinationKey: "ofc_etd", methodName:#function)
        
        addCommonInformation(parameters)
        performPostRequest(parameters, urlString: BASE_URL+ServerSupportURLS.UPDATE_PROFILE, completionBlock: completionBlock!,methodName:#function)
    }
    
    
    //MARK: - FAVOURITE
    
    func updatePriority(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "userIds", destinationDictionary: parameters, destinationKey: "users", methodName:#function)
        addCommonInformation(parameters)
        performPostRequest(parameters, urlString: BASE_URL+ServerSupportURLS.UPDATE_PRIORITY, completionBlock: completionBlock!,methodName:#function)
    }
    
    func setUserFavourite(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "toUserId", destinationDictionary: parameters, destinationKey: "foruser", methodName:#function)
        copyData("add" , destinationDictionary: parameters, destinationKey: "action", methodName: #function)
        addCommonInformation(parameters)
        performPostRequest(parameters, urlString: BASE_URL+ServerSupportURLS.SET_FAVOURITE, completionBlock: completionBlock!,methodName:#function)
    }
    
    func setUserUnFavourite(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "toUserId", destinationDictionary: parameters, destinationKey: "foruser", methodName:#function)
        copyData("del" , destinationDictionary: parameters, destinationKey: "action", methodName: #function)
        addCommonInformation(parameters)
        performPostRequest(parameters, urlString: BASE_URL+ServerSupportURLS.SET_UN_FAVOURITE, completionBlock: completionBlock!,methodName:#function)
    }
    
    func getFavourites(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        addCommonInformation(parameters)
        performGetRequest(parameters, urlString: BASE_URL+ServerSupportURLS.GET_FAVOURITES, completionBlock: completionBlock,methodName:#function)
    }
    
    //MARK: - BLOCK
    func setUserBlocked(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData("block" , destinationDictionary: parameters, destinationKey: "action", methodName: #function)
        copyData(loggedInUserId() , destinationDictionary: parameters, destinationKey: "id", methodName: #function)
        copyData(information, sourceKey: "toUserId", destinationDictionary: parameters, destinationKey: "foruser", methodName:#function)
        performPostRequest(parameters, urlString: BASE_URL+ServerSupportURLS.SET_BLOCKED, completionBlock: completionBlock!,methodName:#function)
        trackFunctionalEvent(FE_BLOCK_USER, information: parameters)
    }
    
    func setUserUnBlocked(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData("unblock" , destinationDictionary: parameters, destinationKey: "action", methodName: #function)
        copyData(loggedInUserId() , destinationDictionary: parameters, destinationKey: "id", methodName: #function)
        copyData(information, sourceKey: "toUserId", destinationDictionary: parameters, destinationKey: "foruser", methodName:#function)
        performPostRequest(parameters, urlString: BASE_URL+ServerSupportURLS.SET_UN_BLOCKED, completionBlock: completionBlock!,methodName:#function)
    }
    
    //MARK: - FEED BACKS
    func getFeedbacks(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "userId", destinationDictionary: parameters, destinationKey: "id", methodName:#function)
        performGetRequest(parameters, urlString: BASE_URL+ServerSupportURLS.GET_FEEDBACKS, completionBlock: completionBlock,methodName:#function)
    }
    
    //MARK: - SEARCH
    func searchUsers(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "keyword", destinationDictionary: parameters, destinationKey: "name", methodName:#function)
        addCommonInformation(parameters)
        performGetRequest(parameters, urlString: BASE_URL+ServerSupportURLS.SEARCH_USERS, completionBlock: completionBlock,methodName:#function)
    }
    
    //MARK: - RIDE
    
    func setMyRide(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "userId", destinationDictionary: parameters, destinationKey: "id", methodName:#function)
        copyData(information, sourceKey: "date", destinationDictionary: parameters, destinationKey: "date", methodName:#function)
        copyData(information, sourceKey: "mode", destinationDictionary: parameters, destinationKey: "mode", methodName:#function)
        copyData(information, sourceKey: "via", destinationDictionary: parameters, destinationKey: "via", methodName:#function)
        addCommonInformation(parameters)
        performPostRequest(parameters, urlString: BASE_URL+ServerSupportURLS.SET_MY_RIDE, completionBlock: completionBlock!,methodName:#function)
    }
    
    func getAvailableUsers(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "date", destinationDictionary: parameters, destinationKey: "dat", methodName:#function)
        copyData(information, sourceKey: "mode", destinationDictionary: parameters, destinationKey: "mode", methodName:#function)
        copyData(information, sourceKey: "lat", destinationDictionary: parameters, destinationKey: "lat", methodName:#function)
        copyData(information, sourceKey: "lng", destinationDictionary: parameters, destinationKey: "lng", methodName:#function)
        addCommonInformation(parameters)
        performPostRequest(parameters, urlString: BASE_URL+ServerSupportURLS.GET_AVAILABLE_USERS, completionBlock: completionBlock!,methodName:#function)
    }
    
    func sendInviteForRide(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "userId", destinationDictionary: parameters, destinationKey: "from", methodName:#function)
        copyData(information, sourceKey: "mode", destinationDictionary: parameters, destinationKey: "from_mode", methodName:#function)
        copyData(information, sourceKey: "otherUserId", destinationDictionary: parameters, destinationKey: "to", methodName:#function)
        copyData(information, sourceKey: "toMode", destinationDictionary: parameters, destinationKey: "to_mode", methodName:#function)
        copyData(information, sourceKey: "date", destinationDictionary: parameters, destinationKey: "tr_date", methodName:#function)
        performPostRequest(parameters, urlString: BASE_URL+ServerSupportURLS.SEND_INVITE_FOR_RIDE, completionBlock: completionBlock!,methodName:#function)
        trackFunctionalEvent(FE_SEND_INVITE,information:parameters)
    }
    
    func acceptInvitationForRide(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "userId", destinationDictionary: parameters, destinationKey: "userid", methodName:#function)
        copyData(information, sourceKey: "requestId", destinationDictionary: parameters, destinationKey: "requestid", methodName:#function)
        performPostRequest(parameters, urlString: BASE_URL+ServerSupportURLS.ACCEPT_INVITE_FOR_RIDE, completionBlock: completionBlock!,methodName:#function)
    }
    
    func rejectInvitationForRide(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "userId", destinationDictionary: parameters, destinationKey: "userid", methodName:#function)
        copyData(information, sourceKey: "requestId", destinationDictionary: parameters, destinationKey: "requestid", methodName:#function)
        performPostRequest(parameters, urlString: BASE_URL+ServerSupportURLS.REJECT_INVITE_FOR_RIDE, completionBlock: completionBlock!,methodName:#function)
    }
    
    func cancelRide(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "userId", destinationDictionary: parameters, destinationKey: "userid", methodName:#function)
        copyData(information, sourceKey: "requestId", destinationDictionary: parameters, destinationKey: "rideid", methodName:#function)
        performPostRequest(parameters, urlString: BASE_URL+ServerSupportURLS.CANCEL_RIDE, completionBlock: completionBlock!,methodName:#function)
    }
    
    func settleRide(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "userId", destinationDictionary: parameters, destinationKey: "userid", methodName:#function)
        copyData(information, sourceKey: "requestId", destinationDictionary: parameters, destinationKey: "rideid", methodName:#function)
        copyData(information, sourceKey: "date", destinationDictionary: parameters, destinationKey: "date", methodName:#function)
        performPostRequest(parameters, urlString: BASE_URL+ServerSupportURLS.SETTLE_RIDE, completionBlock: completionBlock!,methodName:#function)
        trackFunctionalEvent(FE_SETTLE_RIDE,information:parameters)
    }
    
    //MARK: - WEEKLY RIDE PLAN
    
    func getWeeklyRidePlan(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(loggedInUserId() , destinationDictionary: parameters, destinationKey: "id", methodName:#function)
        performGetRequest(parameters, urlString: BASE_URL+ServerSupportURLS.WEEKLY_RIDE_PLAN, completionBlock: completionBlock,methodName:#function)
    }
    
    
    //MARK: - UPDATE PLAN
    
    func updatePlan(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "userId", destinationDictionary: parameters, destinationKey: "userid", methodName:#function)
        copyData(information, sourceKey: "isOwner", destinationDictionary: parameters, destinationKey: "status", methodName:#function)
        copyData(information, sourceKey: "mode", destinationDictionary: parameters, destinationKey: "mode", methodName:#function)
        copyData(information, sourceKey: "date", destinationDictionary: parameters, destinationKey: "date", methodName:#function)
        copyData(information, sourceKey: "time", destinationDictionary: parameters, destinationKey: "value", methodName:#function)
        copyData(information, sourceKey: "autoFixEnabled", destinationDictionary: parameters, destinationKey: "afx_status", methodName:#function)
        copyData(information, sourceKey: "autoFixType", destinationDictionary: parameters, destinationKey: "afx_type", methodName:#function)
        performPostRequest(parameters, urlString: BASE_URL+ServerSupportURLS.UPDATE_PLAN, completionBlock: completionBlock!,methodName:#function)
    }
    
    //MARK: - SETTINGS
    
    func getAllSettings(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "userId", destinationDictionary: parameters, destinationKey: "id", methodName:#function)
        performGetRequest(parameters, urlString: BASE_URL+ServerSupportURLS.GET_ALL_SETTINGS, completionBlock: completionBlock,methodName:#function)
    }
    
    func updateMobileSettings(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(loggedInUserId() , destinationDictionary: parameters, destinationKey: "id", methodName: #function)
        copyData(information, sourceKey: "value", destinationDictionary: parameters, destinationKey: "value", methodName:#function)
        performPostRequest(parameters, urlString: BASE_URL+ServerSupportURLS.UPDATE_MOBILE_NOTIFICATION_SETTINGS, completionBlock: completionBlock!,methodName:#function)
        trackFunctionalEvent(FE_SETTINGS_NOTIFICATIONS_MOBILE,information:parameters)
    }
    
    func updateEmailSettings(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(loggedInUserId() , destinationDictionary: parameters, destinationKey: "id", methodName: #function)
        copyData(information, sourceKey: "value", destinationDictionary: parameters, destinationKey: "value", methodName:#function)
        performPostRequest(parameters, urlString: BASE_URL+ServerSupportURLS.UPDATE_EMAIL_NOTIFICATION_SETTINGS, completionBlock: completionBlock!,methodName:#function)
        trackFunctionalEvent(FE_SETTINGS_NOTIFICATIONS_EMAIL,information:parameters)
    }
    
    func updateWebSettings(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(loggedInUserId() , destinationDictionary: parameters, destinationKey: "id", methodName: #function)
        copyData(information, sourceKey: "value", destinationDictionary: parameters, destinationKey: "value", methodName:#function)
        performPostRequest(parameters, urlString: BASE_URL+ServerSupportURLS.UPDATE_WEBSITE_NOTIFICATION_SETTINGS, completionBlock: completionBlock!,methodName:#function)
        trackFunctionalEvent(FE_SETTINGS_NOTIFICATIONS_WEBSITE,information:parameters)
    }
    
    func updateSMSSettings(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(loggedInUserId() , destinationDictionary: parameters, destinationKey: "id", methodName: #function)
        copyData(information, sourceKey: "value", destinationDictionary: parameters, destinationKey: "value", methodName:#function)
        performPostRequest(parameters, urlString: BASE_URL+ServerSupportURLS.UPDATE_SMS_NOTIFICATION_SETTINGS, completionBlock: completionBlock!,methodName:#function)
        trackFunctionalEvent(FE_SETTINGS_NOTIFICATIONS_SMS,information:parameters)
    }
    
    
    func updateDNDDailySettings(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(loggedInUserId() , destinationDictionary: parameters, destinationKey: "id", methodName: #function)
        copyData(information, sourceKey: "value", destinationDictionary: parameters, destinationKey: "value", methodName:#function)
        copyData("1" , destinationDictionary: parameters, destinationKey: "status", methodName: #function)
        performPostRequest(parameters, urlString: BASE_URL+ServerSupportURLS.UPDATE_DAILY_DND_SETTINGS, completionBlock: completionBlock!,methodName:#function)
    }
    
    func updateDNDWeeklySettings(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(loggedInUserId() , destinationDictionary: parameters, destinationKey: "id", methodName: #function)
        copyData(information, sourceKey: "value", destinationDictionary: parameters, destinationKey: "value", methodName:#function)
        copyData("1" , destinationDictionary: parameters, destinationKey: "status", methodName: #function)
        performPostRequest(parameters, urlString: BASE_URL+ServerSupportURLS.UPDATE_WEEKLY_DND_SETTINGS, completionBlock: completionBlock!,methodName:#function)
    }
    
    //MARK: - ACCOUNT
    
    func getAccountSummary(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "userId", destinationDictionary: parameters, destinationKey: "id", methodName:#function)
        performGetRequest(parameters, urlString: BASE_URL+ServerSupportURLS.GET_ACCOUNT_SUMMARY, completionBlock: completionBlock,methodName:#function)
        trackFunctionalEvent(FE_BALANCE,information:parameters)
    }
    
    func withdrawl(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "name", destinationDictionary: parameters, destinationKey: "username", methodName:#function)
        copyData(information, sourceKey: "tokens", destinationDictionary: parameters, destinationKey: "tkn", methodName:#function)
        copyData(information, sourceKey: "bankName", destinationDictionary: parameters, destinationKey: "bankname", methodName:#function)
        copyData(information, sourceKey: "accountNumber", destinationDictionary: parameters, destinationKey: "bankaccountno", methodName:#function)
        copyData(information, sourceKey: "pan", destinationDictionary: parameters, destinationKey: "panno", methodName:#function)
        copyData(information, sourceKey: "ifsc", destinationDictionary: parameters, destinationKey: "bankifsccode", methodName:#function)
        addCommonInformation(parameters)
        performPostRequest(parameters, urlString: BASE_URL+ServerSupportURLS.WITHDRAWL, completionBlock: completionBlock!,methodName:#function)
    }
    
    //MARK: - GET CITY COMPANY CAR LIST
    
    func getOptionsList(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "lastSyncDate", destinationDictionary: parameters, destinationKey: "lastSyncDate", methodName:#function)
        addCommonInformation(parameters)
        performGetRequest(parameters, urlString: BASE_URL+ServerSupportURLS.GET_CITY_COMPANY_CAR_LIST, completionBlock: completionBlock,methodName:#function)
    }
    
    //MARK: - CONTACT LIST
    
    func getContactList(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "qbId", destinationDictionary: parameters, destinationKey: "qbid", methodName:#function)
        addCommonInformation(parameters)
        performGetRequest(parameters, urlString: BASE_URL+ServerSupportURLS.GET_CONTACTS, completionBlock: completionBlock,methodName:#function)
    }
    
    //MARK: - GET RANKING DETAILS
    
    func getRankingDetails(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "type", destinationDictionary: parameters, destinationKey: "type", methodName:#function)
        addCommonInformation(parameters)
        performGetRequest(parameters, urlString: BASE_URL+ServerSupportURLS.GET_RANKING, completionBlock: completionBlock,methodName:#function)
    }
    
    //MARK: - STATUS
    
    func setStatus(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "status", destinationDictionary: parameters, destinationKey: "status", methodName:#function)
        addCommonInformation(parameters)
        performPostRequest(parameters, urlString: BASE_URL+ServerSupportURLS.SET_STATUS, completionBlock: completionBlock!,methodName:#function)
    }
    
    //MARK: - FEEDBACK
    
    func sendFeedback(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "requestId", destinationDictionary: parameters, destinationKey: "rideid", methodName:#function)
        copyData(information, sourceKey: "comment", destinationDictionary: parameters, destinationKey: "msg", methodName:#function)
        copyData(information, sourceKey: "rating", destinationDictionary: parameters, destinationKey: "star", methodName:#function)
        copyData(information, sourceKey: "fromUser", destinationDictionary: parameters, destinationKey: "by", methodName:#function)
        copyData(information, sourceKey: "toUser", destinationDictionary: parameters, destinationKey: "to", methodName:#function)
        performPostRequest(parameters, urlString: BASE_URL+ServerSupportURLS.FEEDBACK, completionBlock: completionBlock!,methodName:#function)
        trackFunctionalEvent(FE_FEEDBACK, information: parameters)
    }
    
    
    //MARK: - VERIFICATION
    
    func verifyOrganisationalEmail(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "email", destinationDictionary: parameters, destinationKey: "email", methodName:#function)
        addCommonInformation(parameters)
        performGetRequest(parameters, urlString: BASE_URL+ServerSupportURLS.VERIFY_COORPORATE_EMAIL, completionBlock: completionBlock,methodName:#function)
        trackFunctionalEvent(FE_VERIFICATION_EMAIL,information:parameters)
        
    }
    
    func uploadCollegeIdProof(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        if information.object(forKey: "image") != nil {
            let image = information.object(forKey: "image") as! UIImage
            let imageData = UIImageJPEGRepresentation(image.resizeWithWidth(640),0)
            copyData(encodeStringToBase64(imageData!), destinationDictionary: parameters, destinationKey: "doc", methodName: #function)
            copyData(information, sourceKey: "imageName", destinationDictionary: parameters, destinationKey: "filename", methodName:#function)
        }
        addCommonInformation(parameters)
        performPostRequest(parameters, urlString: BASE_URL_FOR_IMAGE_UPLOADS+ServerSupportURLS.UPLOAD_COLLEGE_ID_PROOF, completionBlock: completionBlock!,methodName:#function)
    }
    
    func saveCollegeIdProof(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "imageName", destinationDictionary: parameters, destinationKey: "filename", methodName:#function)
        copyData("IOS_doc" ,destinationDictionary: parameters, destinationKey: "originalname", methodName: #function)
        addCommonInformation(parameters)
        performGetRequest(parameters, urlString: BASE_URL+ServerSupportURLS.SAVE_COLLEGE_ID_PROOF, completionBlock: completionBlock,methodName:#function)
        trackFunctionalEvent(FE_VERIFICATION_ID_PROOF,information:parameters)
    }
    
    func uploadGovernmentIdProof(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        if information.object(forKey: "image") != nil {
            let image = information.object(forKey: "image") as! UIImage
            let imageData = UIImageJPEGRepresentation(image.resizeWithWidth(640),0)
            copyData(encodeStringToBase64(imageData!), destinationDictionary: parameters, destinationKey: "doc", methodName: #function)
            copyData(information, sourceKey: "imageName", destinationDictionary: parameters, destinationKey: "filename", methodName:#function)
        }
        addCommonInformation(parameters)
        performPostRequest(parameters, urlString: BASE_URL_FOR_IMAGE_UPLOADS+ServerSupportURLS.UPLOAD_GOVERNMENT_ID_PROOF, completionBlock: completionBlock!,methodName:#function)
    }
    
    func saveGovernmentIdProof(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "imageName", destinationDictionary: parameters, destinationKey: "filename", methodName:#function)
        copyData("IOS_doc" ,destinationDictionary: parameters, destinationKey: "originalname", methodName: #function)
        addCommonInformation(parameters)
        performGetRequest(parameters, urlString: BASE_URL+ServerSupportURLS.SAVE_GOVERNMENT_ID_PROOF, completionBlock: completionBlock,methodName:#function)
        trackFunctionalEvent(FE_VERIFICATION_ID_PROOF,information:parameters)
    }
    
    //MARK: - DAILY RIDE NOTIFICATION
    
    func requestGet(_ url:String,information:NSDictionary,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        parameters.addEntries(from: information as! [AnyHashable: Any])
        performGetRequest(parameters, urlString: BASE_URL+url, completionBlock: completionBlock,methodName:#function)
    }
    
    func requestGetWithfullUrl(_ url:String,information:NSDictionary,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        parameters.addEntries(from: information as! [AnyHashable: Any])
        performGetRequest(parameters, urlString: url , completionBlock: completionBlock,methodName:#function)
    }
    
    func requestPost(_ url:String,information:NSDictionary,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        parameters.addEntries(from: information as! [AnyHashable: Any])
        performPostRequest(parameters, urlString: BASE_URL+url, completionBlock: completionBlock!,methodName:#function)
    }
    
    //MARK: - REPORTING
    
    func inviteForChat(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(loggedInUserId() , destinationDictionary: parameters, destinationKey: "from", methodName:#function)
        copyData(information, sourceKey: "toUserId", destinationDictionary: parameters, destinationKey: "to", methodName:#function)
        addCommonInformation(parameters)
        performPostRequest(parameters, urlString: BASE_URL+ServerSupportURLS.INVITE_FOR_CHAT, completionBlock: completionBlock!,methodName:#function)
    }
    
    //MARK: - OFFERS
    
    func getOffers(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        addCommonInformation(parameters)
        performGetRequest(parameters, urlString: BASE_URL+ServerSupportURLS.GET_OFFERS, completionBlock: completionBlock,methodName:#function)
    }
    
    func claimOffer(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "by", destinationDictionary: parameters, destinationKey: "by", methodName:#function)
        copyData(information, sourceKey: "offersCode", destinationDictionary: parameters, destinationKey: "offercode", methodName:#function)
        copyData(information, sourceKey: "offersCode", destinationDictionary: parameters, destinationKey: "offerscode", methodName:#function)
        performGetRequest(parameters, urlString: BASE_URL+ServerSupportURLS.CLAIM_OFFER, completionBlock: completionBlock,methodName:#function)
    }
    
    func getRideCostDetails(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "withUserId", destinationDictionary: parameters, destinationKey: "co_traveller_id", methodName:#function)
        copyData(information, sourceKey: "offerId", destinationDictionary: parameters, destinationKey: "offer_id", methodName:#function)
        addCommonInformation(parameters)
        performPostRequest(parameters, urlString: BASE_URL+ServerSupportURLS.GET_RIDE_COSTING_DETAILS, completionBlock: completionBlock,methodName:#function)
    }
    
    
    
    //MARK: - USER TRACKING
    
    func updateUserTrackingDetails(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "trackerInfo", destinationDictionary: parameters, destinationKey: "tracker_info", methodName:#function)
        addCommonInformation(parameters)
        performPostRequest(parameters, urlString: BASE_URL+ServerSupportURLS.UPDATE_USER_TRACKED_DETAILS, completionBlock: completionBlock!,methodName:#function)
    }
    
    //MARK: - RIDE COMPLETION EVENT TRACKING
    
    func notifyServerForRideCompletionEvent(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "id", destinationDictionary: parameters, destinationKey: "id", methodName:#function)
        copyData(information, sourceKey: "timeStamp", destinationDictionary: parameters, destinationKey: "timestamp", methodName:#function)
        copyData(information, sourceKey: "coRiders", destinationDictionary: parameters, destinationKey: "coriders", methodName:#function)
        copyData(information, sourceKey: "carOwner", destinationDictionary: parameters, destinationKey: "carowner", methodName:#function)
        copyData(information, sourceKey: "lat", destinationDictionary: parameters, destinationKey: "lat", methodName:#function)
        copyData(information, sourceKey: "lng", destinationDictionary: parameters, destinationKey: "lng", methodName:#function)
        copyData(information, sourceKey: "rideId", destinationDictionary: parameters, destinationKey: "rideid", methodName:#function)
        addCommonInformation(parameters)
        performPostRequest(parameters, urlString: BASE_URL+ServerSupportURLS.TRACK_RIDE_COMPLETION_EVENT, completionBlock: completionBlock!,methodName:#function)
    }
    
    //MARK: - SAVINGS
    
    func getSavings(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        addCommonInformation(parameters)
        performGetRequest(parameters, urlString: BASE_URL+ServerSupportURLS.GET_SAVINGS, completionBlock: completionBlock,methodName:#function)
    }
    
    //MARK: - User Type
    
    func setUserType(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        addCommonInformation(parameters)
        copyData(information, sourceKey: "userType", destinationDictionary: parameters, destinationKey: "userType", methodName:#function)
        performPostRequest(parameters, urlString: BASE_URL+ServerSupportURLS.SET_USERTYPE, completionBlock: completionBlock!,methodName:#function)
    }
    
    //MARK: - Expert Ride Alerts
    
    func setExpertRideAlertsPreferences(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        addCommonInformation(parameters)
        copyData(information, sourceKey: "eprSettingOption1", destinationDictionary: parameters, destinationKey: "drn_status_send", methodName:#function)
        copyData(information, sourceKey: "eprSettingOption2", destinationDictionary: parameters, destinationKey: "drn_status_receive", methodName:#function)
        performPostRequest(parameters, urlString: BASE_URL+ServerSupportURLS.SET_EXPERT_RIDE_ALERTS_PREFERENCES, completionBlock: completionBlock!,methodName:#function)
    }
    
    //MARK: - Show Phone Number Settings
    
    func setShowPhoneNumberPreferences(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        addCommonInformation(parameters)
        copyData(information, sourceKey: "showMyNumber", destinationDictionary: parameters, destinationKey: "show_phone", methodName:#function)
        performPostRequest(parameters, urlString: BASE_URL+ServerSupportURLS.SET_SHOW_PHONE_NUMBER_PREFERENCES, completionBlock: completionBlock!,methodName:#function)
    }
    
    
    //MARK: - Travel Preferences
    
    func setMyTravelPreferences(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        addCommonInformation(parameters)
        copyData(information, sourceKey: "companyFlag", destinationDictionary: parameters, destinationKey: "companyFlag", methodName:#function)
        copyData(information, sourceKey: "groupFlag", destinationDictionary: parameters, destinationKey: "groupFlag", methodName:#function)
        performPostRequest(parameters, urlString: BASE_URL+ServerSupportURLS.SET_TRAVEL_PREFERENCES, completionBlock: completionBlock!,methodName:#function)
    }
    
    func getMyTravelPreferences(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        addCommonInformation(parameters)
        performGetRequest(parameters, urlString: BASE_URL+ServerSupportURLS.GET_TRAVEL_PREFERENCES, completionBlock: completionBlock,methodName:#function)
    }
    
    //MARK: - DRN
    func getDrnSpecifications(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let jsonFileName = KEY_JSON_FILE_NAME
        let filePath = Bundle.main.path(forResource: jsonFileName, ofType: nil)
        do{
            let data = try Data(contentsOf:URL(fileURLWithPath: filePath!))
            let parsedData = parsedJson(data, methodName: #function)
            completionBlock?(parsedData as! NSDictionary?)
        }catch{}
    }
    
    func drnAPIRequest(_ information: NSDictionary, urlPath: String,requestType : ApiRequestType ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        addCommonInformation(parameters)
        parameters.addEntries(from: information as! [AnyHashable : Any])
        let urlString = BASE_URL+urlPath
        if requestType == .get {
            performGetRequest(parameters, urlString: urlString, completionBlock: completionBlock,methodName:#function)
        }else{
            performPostRequest(parameters, urlString: urlString, completionBlock: completionBlock,methodName:#function)
        }
    }
    
    //MARK: - Group
    
    func manageGroup(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "name", destinationDictionary: parameters, destinationKey: "name", methodName:#function)
        copyData(information, sourceKey: "themeColor", destinationDictionary: parameters, destinationKey: "theme_color", methodName:#function)
        copyData(information, sourceKey: "groupId", destinationDictionary: parameters, destinationKey: "group_id", methodName:#function)
        copyData(information, sourceKey: "actionType", destinationDictionary: parameters, destinationKey: "action_type", methodName:#function)
        parseSourceForImageAndAttachEncodedImage(s: information, skey: "picture", d: parameters, dkey: "picture")
        addCommonInformation(parameters)
        performPostRequest(parameters, urlString: BASE_URL_POM+ServerSupportURLS.MANAGE_GROUP, completionBlock: completionBlock!,methodName:#function)
    }
    
    func getAllGroups(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "qbId", destinationDictionary: parameters, destinationKey: "qbid", methodName:#function)
        addCommonInformation(parameters)
        performPostRequest(parameters, urlString: BASE_URL_POM+ServerSupportURLS.GET_ALL_GROUPS, completionBlock: completionBlock!,methodName:#function)
    }
    
    func checkIn(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "name", destinationDictionary: parameters, destinationKey: "name", methodName:#function)
        copyData(information, sourceKey: "address", destinationDictionary: parameters, destinationKey: "address", methodName:#function)
        copyData(information, sourceKey: "locationLatitude", destinationDictionary: parameters, destinationKey: "loc_lat", methodName:#function)
        copyData(information, sourceKey: "locationLongitude", destinationDictionary: parameters, destinationKey: "loc_lon", methodName:#function)
        copyData(information, sourceKey: "groupId", destinationDictionary: parameters, destinationKey: "group_id", methodName:#function)
        addCommonInformation(parameters)
        performPostRequest(parameters, urlString: BASE_URL_POM+ServerSupportURLS.CHECK_IN, completionBlock: completionBlock!,methodName:#function)
    }
    
    func configureGroup(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "locationSharingEnabled", destinationDictionary: parameters, destinationKey: "loc_enabled", methodName:#function)
        copyData(information, sourceKey: "groupId", destinationDictionary: parameters, destinationKey: "group_id", methodName:#function)
        addCommonInformation(parameters)
        performPostRequest(parameters, urlString: BASE_URL_POM+ServerSupportURLS.CONFIGURE_GROUP_SETTINGS, completionBlock: completionBlock!,methodName:#function)
    }
    
    func leaveGroup(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "groupId", destinationDictionary: parameters, destinationKey: "group_id", methodName:#function)
        copyData(information, sourceKey: "actionType", destinationDictionary: parameters, destinationKey: "action_type", methodName:#function)
        addCommonInformation(parameters)
        performPostRequest(parameters, urlString: BASE_URL_POM+ServerSupportURLS.LEAVE_GROUP, completionBlock: completionBlock!,methodName:#function)
    }
    
    func configureGroupPushNotification(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "muteHours", destinationDictionary: parameters, destinationKey: "mute_hours", methodName:#function)
        copyData(information, sourceKey: "groupId", destinationDictionary: parameters, destinationKey: "group_id", methodName:#function)
        copyData(information, sourceKey: "actionType", destinationDictionary: parameters, destinationKey: "action_type", methodName:#function)
        addCommonInformation(parameters)
        performPostRequest(parameters, urlString: BASE_URL_POM+ServerSupportURLS.CONFIGURE_GROUP_PUSH_NOTIFICATIONS, completionBlock: completionBlock!,methodName:#function)
    }
    
    func joinGroup(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "groupId", destinationDictionary: parameters, destinationKey: "group_id", methodName:#function)
        copyData(safeString(currentTimeStamp()), destinationDictionary: parameters, destinationKey: "current_timestamp", methodName:#function)
        addCommonInformation(parameters)
        performPostRequest(parameters, urlString: BASE_URL_POM+ServerSupportURLS.JOIN_GROUP, completionBlock: completionBlock!,methodName:#function)
    }
    
    func requestToShareLocation(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "userIdTo", destinationDictionary: parameters, destinationKey: "user_id_to", methodName:#function)
        copyData(information, sourceKey: "groupId", destinationDictionary: parameters, destinationKey: "group_id", methodName:#function)
        addCommonInformation(parameters)
        performPostRequest(parameters, urlString: BASE_URL_POM+ServerSupportURLS.REQUEST_TO_SHARE_LOCATION, completionBlock: completionBlock!,methodName:#function)
    }
    
    //MARK: - NOTIFICATION
    
    func getNotifications(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        addCommonInformation(parameters)
        copyData(information, sourceKey: "notificationLastSyncTimeStamp", destinationDictionary: parameters, destinationKey: "last_sync_timestamp", methodName:#function)
        performPostRequest(parameters, urlString: BASE_URL_POM+ServerSupportURLS.GET_NOTIFICATIONS, completionBlock: completionBlock!,methodName:#function)
    }
    
    func createNotification(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "notificationType", destinationDictionary: parameters, destinationKey: "notif_type", methodName:#function)
        copyData(information, sourceKey: "placeId", destinationDictionary: parameters, destinationKey: "place_id", methodName:#function)
        copyData(information, sourceKey: "timestamp", destinationDictionary: parameters, destinationKey: "timestamp", methodName:#function)
        copyData(information, sourceKey: "sendPushNotification", destinationDictionary: parameters, destinationKey: "send_push_notification", methodName:#function)
        copyData(information, sourceKey: "altitude", destinationDictionary: parameters, destinationKey: "altitude", methodName:#function)
        copyData(information, sourceKey: "speed", destinationDictionary: parameters, destinationKey: "speed", methodName:#function)
        copyData(information, sourceKey: "accuracy", destinationDictionary: parameters, destinationKey: "accuracy", methodName:#function)
        addCommonInformation(parameters)
        performPostRequest(parameters, urlString: BASE_URL_POM+ServerSupportURLS.CREATE_NOTIFICATION, completionBlock: completionBlock!,methodName:#function)
    }
    
    //MARK: - LOCATION
    
    func updateLocation(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "locationLatitude", destinationDictionary: parameters, destinationKey: "loc_lat", methodName:#function)
        copyData(information, sourceKey: "locationLongitude", destinationDictionary: parameters, destinationKey: "loc_lon", methodName:#function)
        copyData(information, sourceKey: "timeStamp", destinationDictionary: parameters, destinationKey: "timestamp", methodName:#function)
        copyData(information, sourceKey: "activity", destinationDictionary: parameters, destinationKey: "activity", methodName:#function)
        copyData(information, sourceKey: "batteryStatus", destinationDictionary: parameters, destinationKey: "battery_status", methodName:#function)
        copyData(information, sourceKey: "altitude", destinationDictionary: parameters, destinationKey: "altitude", methodName:#function)
        copyData(information, sourceKey: "speed", destinationDictionary: parameters, destinationKey: "speed", methodName:#function)
        copyData(information, sourceKey: "accuracy", destinationDictionary: parameters, destinationKey: "accuracy", methodName:#function)
        addCommonInformation(parameters)
        performPostRequest(parameters, urlString: BASE_URL_POM+ServerSupportURLS.UPDATE_LATEST_LOCATION, completionBlock: completionBlock!,methodName:#function)
    }
    
    
    //MARK: - Place
    
    func managePlace(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "name", destinationDictionary: parameters, destinationKey: "name", methodName:#function)
        copyData(information, sourceKey: "address", destinationDictionary: parameters, destinationKey: "address", methodName:#function)
        copyData(information, sourceKey: "placeId", destinationDictionary: parameters, destinationKey: "place_id", methodName:#function)
        copyData(information, sourceKey: "groupId", destinationDictionary: parameters, destinationKey: "group_id", methodName:#function)
        copyData(information, sourceKey: "LocationLatitude", destinationDictionary: parameters, destinationKey: "loc_lat", methodName:#function)
        copyData(information, sourceKey: "LocationLongitude", destinationDictionary: parameters, destinationKey: "loc_lon", methodName:#function)
        copyData(information, sourceKey: "actionType", destinationDictionary: parameters, destinationKey: "action_type", methodName:#function)
        addCommonInformation(parameters)
        performPostRequest(parameters, urlString: BASE_URL_POM+ServerSupportURLS.MANAGE_PLACE, completionBlock: completionBlock!,methodName:#function)
    }
    
    func getAllPlaces(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        addCommonInformation(parameters)
        performPostRequest(parameters, urlString: BASE_URL_POM+ServerSupportURLS.GET_ALL_PLACES, completionBlock: completionBlock!,methodName:#function)
    }
    
    func configurePlace(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "groupId", destinationDictionary: parameters, destinationKey: "group_id", methodName:#function)
        copyData(information, sourceKey: "placeId", destinationDictionary: parameters, destinationKey: "place_id", methodName:#function)
        copyData(information, sourceKey: "exitSharingEnabled", destinationDictionary: parameters, destinationKey: "geo_exit", methodName:#function)
        copyData(information, sourceKey: "entrySharingEnabled", destinationDictionary: parameters, destinationKey: "geo_entry", methodName:#function)
        copyData("both", destinationDictionary: parameters, destinationKey: "type", methodName:#function)
        addCommonInformation(parameters)
        performPostRequest(parameters, urlString: BASE_URL_POM+ServerSupportURLS.CONFIGURE_PLACE_SETTINGS, completionBlock: completionBlock!,methodName:#function)
    }
    
    //MARK: - SOS
    
    func updateEmergencyContacts(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "contacts", destinationDictionary: parameters, destinationKey: "contacts", methodName:#function)
        addCommonInformation(parameters)
        performPostRequest(parameters, urlString: BASE_URL_POM+ServerSupportURLS.UPDATE_EMERGENCY_CONTACTS, completionBlock: completionBlock!,methodName:#function)
    }
    
    func getEmergencyContacts(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        addCommonInformation(parameters)
        performPostRequest(parameters, urlString: BASE_URL_POM+ServerSupportURLS.GET_EMERGENCY_CONTACTS, completionBlock: completionBlock!,methodName:#function)
    }
    
    func sos(_ information: NSDictionary ,completionBlock: WSCompletionBlock?) -> () {
        let parameters = NSMutableDictionary()
        addCommonInformation(parameters)
        performPostRequest(parameters, urlString: BASE_URL_POM+ServerSupportURLS.PERFORM_SOS, completionBlock: completionBlock!,methodName:#function)
    }
}

//MARK: - Private
class ServerCommunicationManager: NSObject {
    
    var completionBlock: WSCompletionBlock?
    var responseErrorOption: ResponseErrorOption?
    var messageOption = MessageOption.showSuccessResponseMessageUsingNotification
    var progressIndicatorText: String?
    var returnFailureResponseAlso = false
    var returnFailureUnParsedDataIfParsingFails = false
    var showSuccessResponseMessage = false
    var attachCommonUserDetails = true
    var attachCommonDeviceDetails = true
    var showDebugInformationAboutRequestInNotification = false
    
    /// Check for cache and return from cache if possible.
    ///
    /// - parameter body: parameters to set in Body of the request
    /// - returns: bool (wether cache was used or not)
    func loadFromCacheIfPossible(_ body:NSDictionary? , urlString:NSString? ,completionBlock: WSCompletionBlock? , maxAgeInSeconds:Float)->(Bool){
        return false
    }
    
    func returnFakeSuccessResponse(completionBlock: WSCompletionBlock?){
        execMain ({
            let jsonFileName = "SUCCESS_RESPONSE.json"
            let filePath = Bundle.main.path(forResource: jsonFileName, ofType: nil)
            do{
                let data = try Data(contentsOf:URL(fileURLWithPath: filePath!))
                let parsedData = parsedJson(data, methodName: #function)
                completionBlock?(parsedData as! NSDictionary?)
            }catch{}
        },delay:2)
    }
    
    func updateSecurityPolicy(_ manager:AFHTTPSessionManager){
        let securityPolicy = AFSecurityPolicy(pinningMode: AFSSLPinningMode.none)
        securityPolicy.validatesDomainName = false
        securityPolicy.allowInvalidCertificates = true
        manager.securityPolicy = securityPolicy
    }
    
    /// Perform  GET Request.
    ///
    /// - parameter body: parameters to set in Body of the request
    /// - returns: parsed server response via completionBlock
    func performGetRequest(_ body:NSDictionary? , urlString:String ,completionBlock: WSCompletionBlock?,methodName:String)->(){
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async {
            if isInternetConnectivityAvailable(true)==false {
                return;
            }
            let url = URL(string: urlString)
            
            let requestDetails = "\n\n\n                  HITTING URL\n\n \(url!.absoluteString)\n\n\n                  WITH GET REQUEST\n\n\(safeString(body, alternate: ""))\n\n"
            if ENABLE_API_REQUEST_LOGGING {logMessage(requestDetails)}
            if self.showDebugInformationAboutRequestInNotification {
                showNotification(requestDetails, showOnNavigation: true, showAsError: false, duration: 5)
            }
            
            if self.progressIndicatorText != nil{
                showActivityIndicator(self.progressIndicatorText!);
            }
            let manager = AFHTTPSessionManager()
            manager.responseSerializer = AFHTTPResponseSerializer()
            self.updateSecurityPolicy(manager)
            manager.get(urlString, parameters: body, progress: { (progress) -> Void in
                if self.progressIndicatorText != nil{
                    progressForShowingOnActivityIndicator = progress
                }
            }, success: { (urlSessionDataTask, responseObject) -> Void in
                self.verifyServerResponse(responseObject , error: nil, completionBlock: completionBlock,methodName: methodName as NSString?)
            }){(urlSessionDataTask, error) -> Void in
                self.verifyServerResponse(nil, error: error as NSError, completionBlock: completionBlock,methodName: methodName as NSString?)
            }
        }
    }
    
    /// Perform Get Request For Downloading file data.
    ///
    /// - parameter url: url to download file
    /// - returns: fileData via completionBlock
    func performDownloadGetRequest(_ urlString:NSString? ,completionBlock: WSCompletionBlockForFile?,methodName:String)->(){
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async {
            if isInternetConnectivityAvailable(true)==false {
                return;
            }
            if self.progressIndicatorText != nil{
                showActivityIndicator(self.progressIndicatorText!);
            }
            let url = URL(string: urlString as! String)
            
            let requestDetails = "\n\n\n                  HITTING URL TO DOWNLOAD FILE\n\n \((url!.absoluteString))\n\n\n"
            if ENABLE_API_REQUEST_LOGGING {logMessage(requestDetails)}
            if self.showDebugInformationAboutRequestInNotification {
                showNotification(requestDetails, showOnNavigation: true, showAsError: false, duration: 5)
            }
            
            let manager = AFHTTPSessionManager()
            manager.responseSerializer = AFHTTPResponseSerializer()
            manager.requestSerializer = AFHTTPRequestSerializer()
            self.updateSecurityPolicy(manager)
            self.addCommonInformationInRequestSerializer(manager.requestSerializer)
            manager.get(urlString as! String, parameters: nil, progress: { (progress) -> Void in
                if self.progressIndicatorText != nil{
                    progressForShowingOnActivityIndicator = progress
                }
            }, success: { (urlSessionDataTask, responseObject) -> Void in
                completionBlock?(responseObject as! Data? as NSData?)
                execMain({[weak self] in guard let `self` = self else { return }
                    
                    if self.progressIndicatorText != nil{
                        hideActivityIndicator()
                    }
                })
            }){(urlSessionDataTask, error) -> Void in
                self.showServerNotRespondingMessage()
                execMain({[weak self] in guard let `self` = self else { return }
                    
                    if self.progressIndicatorText != nil{
                        hideActivityIndicator()
                    }
                })
            }
        }
    }
    
    /// Perform Json Encoded Post Request.
    ///
    /// - parameter body: parameters to set in Body of the request
    /// - returns: parsed server response via completionBlock
    func performJsonPostRequest(_ body:NSDictionary? , urlString:String? ,completionBlock: WSCompletionBlock?,methodName:String)->(){
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async {
            if isInternetConnectivityAvailable(true)==false {
                return;
            }
            if self.progressIndicatorText != nil{
                showActivityIndicator(self.progressIndicatorText!)
            }
            let url = URL(string: urlString as! String)
            
            let requestDetails = "\n\n\n                  HITTING URL\n\n \((url!.absoluteString))\n\n\n                  WITH POST JSON BODY\n\n\(safeString(body, alternate: ""))\n\n"
            if ENABLE_API_REQUEST_LOGGING {logMessage(requestDetails)}
            if self.showDebugInformationAboutRequestInNotification {
                showNotification(requestDetails, showOnNavigation: true, showAsError: false, duration: 5)
            }
            
            let manager = AFHTTPSessionManager()
            manager.responseSerializer = AFHTTPResponseSerializer()
            manager.requestSerializer = AFJSONRequestSerializer()
            self.updateSecurityPolicy(manager)
            self.addCommonInformationInRequestSerializer(manager.requestSerializer)
            manager.post(urlString as! String, parameters: body, progress: { (progress) -> Void in
                if self.progressIndicatorText != nil{
                    progressForShowingOnActivityIndicator = progress
                }
            }, success: { (urlSessionDataTask, responseObject) -> Void in
                self.verifyServerResponse(responseObject , error: nil, completionBlock: completionBlock,methodName: methodName as NSString?)
            }) { (urlSessionDataTask, error) -> Void in
                self.verifyServerResponse(nil, error: error as NSError, completionBlock: completionBlock,methodName: methodName as NSString?)
            }
        }
    }
    /// Perform Post Request.
    ///
    /// - parameter body: parameters to set in Body of the request
    /// - returns: parsed server response via completionBlock
    func performPostRequest(_ body:NSDictionary? , urlString:String ,completionBlock: WSCompletionBlock? ,methodName:String)->(){
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async {
            if isInternetConnectivityAvailable(true)==false {
                return;
            }
            if self.progressIndicatorText != nil{
                showActivityIndicator((self.progressIndicatorText! as NSString) as String);
            }
            let url = URL(string: urlString)
            
            let requestDetails = "\n\n\n                  HITTING URL\n\n \((url!.absoluteString))\n\n\n                  WITH POST BODY\n\n\(safeString(body, alternate: ""))\n\n"
            if ENABLE_API_REQUEST_LOGGING {logMessage(requestDetails)}
            if self.showDebugInformationAboutRequestInNotification {
                showNotification(requestDetails, showOnNavigation: true, showAsError: false, duration: 5)
            }
            
            
            let manager = AFHTTPSessionManager()
            manager.responseSerializer = AFHTTPResponseSerializer()
            manager.requestSerializer = AFHTTPRequestSerializer()
            self.updateSecurityPolicy(manager)
            self.addCommonInformationInRequestSerializer(manager.requestSerializer)
            manager.post(urlString , parameters: body, progress: { (progress) -> Void in
                if self.progressIndicatorText != nil{
                    progressForShowingOnActivityIndicator = progress
                }
            }, success: { (urlSessionDataTask, responseObject) -> Void in
                self.verifyServerResponse(responseObject , error: nil, completionBlock: completionBlock,methodName: methodName as NSString?)
            }) { (urlSessionDataTask, error) -> Void in
                self.verifyServerResponse(nil, error: error as Error as NSError?, completionBlock: completionBlock,methodName: methodName as NSString?)
            }
        }
    }
    /// Perform Multipart Post Request.
    ///
    /// - parameter body: parameters to set in Body of the request
    /// - returns: parsed server response via completionBlock
    func performMultipartPostRequest(_ body:NSDictionary? , urlString:NSString? , constructBody:((AFMultipartFormData) -> Void)?,completionBlock: @escaping WSCompletionBlock ,methodName:NSString?)->(){
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async {
            if isInternetConnectivityAvailable(true)==false {
                return;
            }
            if self.progressIndicatorText != nil{
                showActivityIndicator((self.progressIndicatorText! as NSString) as String);
            }
            let url = URL(string: urlString as! String)
            
            let requestDetails = "\n\n\n                  HITTING URL\n\n \((url!.absoluteString))\n\n\n                  WITH POST BODY\n\n\(safeString(body, alternate: ""))\n\n"
            if ENABLE_API_REQUEST_LOGGING {logMessage(requestDetails)}
            if self.showDebugInformationAboutRequestInNotification {
                showNotification(requestDetails, showOnNavigation: true, showAsError: false, duration: 5)
            }
            
            let manager = AFHTTPSessionManager()
            manager.responseSerializer = AFHTTPResponseSerializer()
            manager.requestSerializer = AFHTTPRequestSerializer()
            self.updateSecurityPolicy(manager)
            self.addCommonInformationInRequestSerializer(manager.requestSerializer)
            manager.post(urlString as! String, parameters: body, constructingBodyWith: constructBody, progress: { (progress) -> Void in
                if self.progressIndicatorText != nil{
                    progressForShowingOnActivityIndicator = progress
                }
            }, success: { (urlSessionDataTask, responseObject) -> Void in
                self.verifyServerResponse(responseObject , error: nil, completionBlock: completionBlock,methodName: methodName)
            }) { (urlSessionDataTask, error) -> Void in
                self.verifyServerResponse(nil, error: error as Error as NSError?, completionBlock: completionBlock,methodName: methodName)
            }
        }
        
    }
    
    /// Add commonly used parameters to all request.
    ///
    /// - parameter information: pass the dictionary object here that you created to hold parameters required. This function will add commonly used parameter into it.
    func addCommonInformation(_ information:NSMutableDictionary?)->(){
        if attachCommonUserDetails {
            if isUserLoggedIn() {
                copyData(loggedInUserId() , destinationDictionary: information, destinationKey: "id", methodName: #function)
                copyData(loggedInUserId() , destinationDictionary: information, destinationKey: "user_id", methodName: #function)
            }
            copyData("\(deviceToken())" , destinationDictionary: information, destinationKey: "regid", methodName: #function)
        }
        if attachCommonDeviceDetails {
            copyData("\(getDeviceOperationSystem())" , destinationDictionary: information, destinationKey: "mos", methodName: #function)
            copyData("\(UIDevice.systemVersion())" , destinationDictionary: information, destinationKey: "version", methodName: #function)
            if isNotNull (UIApplication.appVersion()){
                copyData("\(UIApplication.appVersion())" , destinationDictionary: information, destinationKey: "appver", methodName: #function)
            }
        }
    }
    func addCommonInformationInRequestSerializer(_ requestSerialiser:AFHTTPRequestSerializer?)->(){
        requestSerialiser?.timeoutInterval = 120
    }
    /// To display server not responding message via notification banner.
    func showServerNotRespondingMessage(){
        DispatchQueue.main.async {
            let showMessage = false
            if showMessage {
                showNotification((MESSAGE_TEXT___FOR_SERVER_NOT_REACHABILITY as NSString) as String, showOnNavigation: false, showAsError: true)
            }
        }
    }
    /// To check wether the server operation succeeded or not.
    /// - returns: bool
    public static func isSuccess(_ response:NSDictionary?)->(Bool){
        if response != nil{
            if "\(response?.object(forKey: ServerCommunicationConstants.RESPONSE_CODE_KEY))".isEqual(ServerCommunicationConstants.RESPONSE_CODE_SUCCESS_VALUE){
                return true
            }
        }
        return false
    }
    /// To check wether the server operation failed or not.
    /// - returns: bool
    public static func isFailure(_ response:NSDictionary?)->(Bool){
        if response != nil{
            if "\(response?.object(forKey: ServerCommunicationConstants.RESPONSE_CODE_KEY))".isEqual(ServerCommunicationConstants.RESPONSE_CODE_FAILURE_VALUE){
                return true
            }
        }
        return false
    }
    /// To verify the server response received and perform action on basis of that.
    /// - parameter response: data received from server in the form of NSData
    func verifyServerResponse(_ response:Any?,error:NSError?,completionBlock: WSCompletionBlock?,methodName:NSString?)->(){
        if responseErrorOption == nil {
            responseErrorOption = ResponseErrorOption.showErrorResponseWithUsingNotification
        }
        if progressIndicatorText != nil{
            hideActivityIndicator();
        }
        if error != nil {
            if responseErrorOption != ResponseErrorOption.dontShowErrorResponseMessage {
                showServerNotRespondingMessage()
            }
            printErrorMessage(error, methodName: #function)
            DispatchQueue.main.async(execute: {
                completionBlock!(nil)
            })
        }else if response != nil {
            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async {
                let responseDictionary = parsedJsonFrom(response as? NSData as Data?,methodName: methodName!)
                if (isNotNull(responseDictionary) && responseDictionary is NSDictionary) {
                    Acf().validateResponseForSecurityPurpose(response: responseDictionary)
                    if ServerCommunicationManager.isSuccess(responseDictionary as! NSDictionary?){
                        if self.showSuccessResponseMessage {
                            var successMessage : NSString?
                            if (responseDictionary?.object(forKey: ServerCommunicationConstants.RESPONSE_MESSAGE_KEY) != nil) {
                                if (responseDictionary?.object(forKey: ServerCommunicationConstants.RESPONSE_MESSAGE_KEY) is String) {
                                    successMessage = responseDictionary?.object(forKey: ServerCommunicationConstants.RESPONSE_MESSAGE_KEY) as? NSString
                                }else{
                                    successMessage = "Successfull"
                                }
                            }else{
                                successMessage = "Successfull"
                            }
                            if self.messageOption == .showSuccessResponseMessageUsingNotification {
                                showNotification("👍🏻 \(successMessage!)", showOnNavigation: false, showAsError: false)
                            }else if self.messageOption == .showSuccessResponseMessageUsingPopUp {
                                if windowObject()?.rootViewController != nil {
                                    execMain({[weak self] in
                                        let alertView = UIAlertController.showAlert(in: windowObject()!.rootViewController!,
                                                                                    withTitle: "Success",
                                                                                    message: "\(successMessage!)",
                                            cancelButtonTitle: nil,
                                            destructiveButtonTitle: nil,
                                            otherButtonTitles: [],
                                            tap: {(controller, action, buttonIndex) in
                                        })
                                        execMain({[weak self] (completed) in
                                            alertView.dismissAnimated()
                                            },delay:3)
                                        },delay:0.1)
                                }
                            }
                        }
                        DispatchQueue.main.async(execute: {
                            completionBlock!(responseDictionary as? NSDictionary)
                        })
                    }
                    else if ServerCommunicationManager.isFailure(responseDictionary as! NSDictionary?){
                        var errorMessage : NSString?
                        if (responseDictionary?.object(forKey: ServerCommunicationConstants.RESPONSE_MESSAGE_KEY) != nil) {
                            if (responseDictionary?.object(forKey: ServerCommunicationConstants.RESPONSE_MESSAGE_KEY) is NSString) {
                                
                                errorMessage = responseDictionary?.object(forKey: ServerCommunicationConstants.RESPONSE_MESSAGE_KEY) as? NSString
                            }else{
                                errorMessage = responseDictionary?.description as NSString?
                            }
                        }
                        else {
                            errorMessage = MESSAGE_TEXT___FOR_SERVER_NOT_REACHABILITY as NSString
                        }
                        
                        if self.responseErrorOption == ResponseErrorOption.showErrorResponseWithUsingNotification {
                            showNotification("😦 \(errorMessage!)", showOnNavigation: false, showAsError: true)
                        }
                        else if self.responseErrorOption == ResponseErrorOption.showErrorResponseWithUsingPopUp {
                            showPopupAlertMessage("", message: errorMessage! as String, messageType: .error)
                        }else if self.responseErrorOption == ResponseErrorOption.showErrorResponseUsingLocalNotification {
                            let errorMessageDetailed = "API Error : \(safeString(methodName)) : \(safeString(errorMessage))"
                            showDebugInfoViaNotification(title: "API FAILURE", message: errorMessageDetailed)
                            logMessage(errorMessageDetailed)
                        }
                        if self.returnFailureResponseAlso {
                            DispatchQueue.main.async(execute: {
                                completionBlock!(responseDictionary as? NSDictionary);
                            })
                        }else{
                            DispatchQueue.main.async(execute: {
                                completionBlock!(nil)
                            })
                        }
                    }
                    else {
                        if self.responseErrorOption == ResponseErrorOption.showErrorResponseWithUsingNotification {
                            showNotification((MESSAGE_TEXT___FOR_SERVER_NOT_REACHABILITY as NSString) as String, showOnNavigation: false, showAsError: true)
                        }
                        else if self.responseErrorOption == ResponseErrorOption.showErrorResponseWithUsingPopUp {
                            showPopupAlertMessage("", message: MESSAGE_TEXT___FOR_SERVER_NOT_REACHABILITY , messageType: .error)
                        }
                        if self.returnFailureResponseAlso {
                            DispatchQueue.main.async(execute: {
                                completionBlock!(responseDictionary as? NSDictionary)
                            })
                        }else{
                            DispatchQueue.main.async(execute: {
                                completionBlock!(nil)
                            })
                        }
                    }
                }
                else {
                    if self.responseErrorOption == ResponseErrorOption.showErrorResponseWithUsingNotification {
                        showNotification((MESSAGE_TEXT___FOR_SERVER_NOT_REACHABILITY as NSString) as String, showOnNavigation: false, showAsError: true)
                    }
                    else if self.responseErrorOption == ResponseErrorOption.showErrorResponseWithUsingPopUp {
                        showPopupAlertMessage("", message: MESSAGE_TEXT___FOR_SERVER_NOT_REACHABILITY , messageType: .error)
                    }
                    if self.returnFailureUnParsedDataIfParsingFails {
                        DispatchQueue.main.async(execute: {
                            completionBlock!(["failedParsingResponseReceived":NSString(data: response as! Data!,encoding: String.Encoding.utf8.rawValue)!])
                        })
                    }else{
                        DispatchQueue.main.async(execute: {
                            completionBlock!(nil)
                        })
                    }
                }
            }
        }else {
            if responseErrorOption == ResponseErrorOption.showErrorResponseWithUsingNotification {
                showNotification((MESSAGE_TEXT___FOR_SERVER_NOT_REACHABILITY as NSString) as String, showOnNavigation: false, showAsError: true)
            }
            else if responseErrorOption == ResponseErrorOption.showErrorResponseWithUsingPopUp {
                showPopupAlertMessage("", message: MESSAGE_TEXT___FOR_SERVER_NOT_REACHABILITY , messageType: .error)
            }
            DispatchQueue.main.async(execute: {
                completionBlock!(nil)
            })
        }
    }
}

