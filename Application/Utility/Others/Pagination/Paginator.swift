//
//  Paginator.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import Foundation

enum PaginatorMode {
    case feedbacks
}

class Paginator: AKSPaginator   {
    var paginationFor : PaginatorMode?
    var userInfo : NSDictionary?
    override func fetchPage(_ page:NSInteger){
        if paginationFor == PaginatorMode.feedbacks {
            self.requestStatus = RequestStatus.requestStatusInProgress
            let information = ["userId":self.userInfo!["userId"] as! String]
            let scm = ServerCommunicationManager()
            scm.responseErrorOption = .dontShowErrorResponseMessage
            scm.getFeedbacks(information as NSDictionary) { (responseData) -> () in
                if let _ = responseData {
                    if let feedbacks = responseData!.object(forKey: "data") {
                        super.setSuccess(feedbacks as! [Any], totalResults:0)
                    }else{
                        super.setFailure()
                    }
                }else{
                    super.setFailure()
                }
            }
        }
    }
}

