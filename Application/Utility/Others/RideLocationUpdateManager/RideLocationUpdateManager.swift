//
//  RideLocationUpdateManager.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import Foundation
import UIKit

class RideLocationUpdateManager: NSObject , CLLocationManagerDelegate{
    
    static let sharedInstance : RideLocationUpdateManager = {
        let instance = RideLocationUpdateManager()
        return instance
    }()

    fileprivate override init() {
        
    }

    var usersForRide = NSMutableArray()
    var trueForOwnerFalseforPassenger = false
    var trueForHomeToDestinationFalseforDestinationToHome = false
    var groupChatDialogue: QBChatDialog?
    let locationManager = CLLocationManager()
    var dateOfRide = Date()
    var currentLocation : CLLocation?
    var rideStatus = RideStatus.none
    var isUpdating = false

    func startLocationUpdatingProcessIfRequired(_ users:NSMutableArray,isOwner:Bool,isHomeToDestination:Bool,dateOfRide:Date,rideStatus:RideStatus,rideId:String,groupChatDialogue:QBChatDialog) {
        if isUpdating == false || (isUpdating && ((rideId as NSString).isEqual(to: getRideId()) == false)){
            stopUpdatingLocations()
            self.dateOfRide = dateOfRide
            self.usersForRide = users.mutableCopy() as! NSMutableArray
            self.trueForOwnerFalseforPassenger = isOwner
            self.trueForHomeToDestinationFalseforDestinationToHome = isHomeToDestination
            self.groupChatDialogue = groupChatDialogue
            locationManager.delegate = self
            locationManager.requestAlwaysAuthorization()
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            self.rideStatus = rideStatus
            if trueForRideWaitingFalseforStarted() == false && (rideStatus == .confirmed || rideStatus == .confirmedButDelayed) {
                isUpdating = true
                startLocationUpdateToRidersGroup()
            }else{
                isUpdating = false
                stopUpdatingLocations()
            }
        }
        //need to handle stop updating locations on group.
    }
    
    func startLocationUpdateToRidersGroup(){
        self.locationManager.allowsBackgroundLocationUpdates = true
        locationManager.startMonitoringSignificantLocationChanges()
        if isNull(currentLocation) {
            if isNull(CacheManager.sharedInstance.loadObject("currentLocation")){
                if trueForHomeToDestinationFalseforDestinationToHome {
                    currentLocation = CLLocation(latitude: homeLocationCoordinate().latitude, longitude: homeLocationCoordinate().longitude)
                }else{
                    currentLocation = CLLocation(latitude: destinationLocationCoordinate().latitude, longitude: destinationLocationCoordinate().longitude)
                }
            }else{
                currentLocation = CacheManager.sharedInstance.loadObject("currentLocation") as? CLLocation
            }
        }
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(RideLocationUpdateManager.updateLocationToOtherViaGroupChat), object: nil)
        self.perform(#selector(RideLocationUpdateManager.updateLocationToOtherViaGroupChat), with: nil, afterDelay:3)
    }
    
    func updateLocationToOtherViaGroupChat(){
        if isSystemReadyToProcessThis() && canContinueToUpdateLocation() && Acf().isQBReadyForItsUserDependentServices(){
            if isNotNull(groupChatDialogue) && isNotNull(currentLocation) {
                let userInfo = Dbm().getUserInfo()
                let message = QBChatMessage()
                let senderID = ServicesManager.instance().currentUser()!.id
                let id = "\(loggedInUserId())"
                let lat = "\(currentLocation!.coordinate.latitude)"
                let lng = "\(currentLocation!.coordinate.longitude)"
                let rideId = "\(getRideId())"
                let date = "\(currentTimeStamp())"
                let name = userInfo!.name!
                let messageType = GroupEvents.location
                message.customParameters = [MESSAGE_TYPE_KEY:"\(messageType)","id":"\(id)","lat":"\(lat)","lng":"\(lng)","rideId":"\(rideId)","date":"\(date)","name":"\(name)"]
                message.senderID = senderID
                message.markable = false
                message.dateSent = Date()
                
                ServicesManager.instance().chatService.send(message, toDialogID: self.groupChatDialogue!.id!, saveToHistory: false, saveToStorage: false, completion: { (error) in
                    if (error != nil) {
                        logMessage("\(error)")
                    }else{
                        if isNotNull(self.groupChatDialogue) {
                            logMessage("\n\nI am \(userInfo!.name!) and UPDATING MY LOCATION ON RIDERS GROUP\n")
                        }
                    }
                })                
            }
        }
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(RideLocationUpdateManager.updateLocationToOtherViaGroupChat), object: nil)
        self.perform(#selector(RideLocationUpdateManager.updateLocationToOtherViaGroupChat), with: nil, afterDelay: RIDE_LOCATION_UPDATE_FREQUENCY)
    }
    
    func canContinueToUpdateLocation() -> Bool {
        if trueForRideWaitingFalseforStarted() == false && (rideStatus == .confirmed || rideStatus == .confirmedButDelayed){
            return true
        }
        return false
    }
    
    func stopUpdatingLocations() {
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        locationManager.stopUpdatingLocation()
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        usersForRide.removeAllObjects()
        groupChatDialogue = nil
        currentLocation = nil
        trueForOwnerFalseforPassenger = false
        trueForHomeToDestinationFalseforDestinationToHome = false
        NSObject.cancelPreviousPerformRequests(withTarget: self)
    }

    func trueForRideWaitingFalseforStarted() -> Bool {
        return  Date().isEarlierThanDate(dateOfRide)
    }
    
    func isAtleast30MinutesLeftForRideStart() -> Bool {
        return  Date().isLaterThanDate(dateOfRide.dateBySubtractingMinutes(60))
    }

    func getRideId() -> String {
        return "\(dateOfRide.day())\(dateOfRide.month())\(dateOfRide.year())\(trueForHomeToDestinationFalseforDestinationToHome == true ? "0" : "1")"
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if locations.count > 0 {
            currentLocation = locations.last!
        }
    }
}
