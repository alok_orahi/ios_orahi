//
//  RideEventManager.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import Foundation
import UIKit

typealias REMCompletionBlock = (_ returnedData :Any?) ->()

class RideEventManager: NSObject , CLLocationManagerDelegate{
    
    static let sharedInstance : RideEventManager = {
        let instance = RideEventManager()
        return instance
    }()
    
    fileprivate override init() {
        
    }
    
    var usersForRide = NSMutableArray()
    var trueForOwnerFalseforPassenger = false
    var trueForHomeToDestinationFalseforDestinationToHome = false
    var groupChatDialogue: QBChatDialog?
    let locationManager = CLLocationManager()
    let cacheDictionary = NSMutableDictionary()
    var isTracking = false
    var dateOfRide = Date()
    var currentLocationCoordinate : CLLocationCoordinate2D?
    var lastDate : Date?
    
    func startEventTrackingProcess(_ users:NSMutableArray,isOwner:Bool,isHomeToDestination:Bool,dateOfRide:Date,groupChatDialogue:QBChatDialog) {
        if !CLLocationManager.isMonitoringAvailable(for: CLCircularRegion.self) {
            logMessage("monitoring not available , hence ride tracking will not work , returning from here")
            return
        }
        if (getRideId() as NSString).isEqual(to: generateRideId(dateOfRide,trueForHomeToDestinationFalseforDestinationToHome: isHomeToDestination)) && usersForRide.count == users.count {
            //nothing new , tracking already started , just return
            return
        }
        stopTrackingRegions()
        self.dateOfRide = dateOfRide
        self.usersForRide = users.mutableCopy() as! NSMutableArray
        self.trueForOwnerFalseforPassenger = isOwner
        self.trueForHomeToDestinationFalseforDestinationToHome = isHomeToDestination
        self.groupChatDialogue = groupChatDialogue
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        startTrackingRegions()
    }
    
    func stopTrackingRegions() {
        for r in locationManager.monitoredRegions {
            locationManager.stopMonitoring(for: r)
        }
        locationManager.stopUpdatingLocation()
        cacheDictionary.removeAllObjects()
        usersForRide.removeAllObjects()
        groupChatDialogue = nil
        isTracking = false
        lastDate = nil
    }
    
    func startTrackingRegions() {
        isTracking = true
        Acf().checkForAnyPendingEventsOnGroupsAndSendItAgain()
        trackCurrentUser()
        if self.trueForOwnerFalseforPassenger {
            // if i am a car owner , then i have to track my location as well as other passenger locations
            for u in usersForRide {
                trackUser(u as! NSDictionary)
            }
        }
    }
    
    func getUsersLocation(_ sourceLocation:Bool,user:NSDictionary)->CLLocationCoordinate2D{
        var destinationLocationLatitude : String?
        var destinationLocationLongitude : String?
        var homeLocationLatitude : String?
        var homeLocationLongitude : String?
        func setDestinationLocation(){
            if isNotNull(user.object(forKey: "ofc_lat")){
                destinationLocationLatitude = user.object(forKey: "ofc_lat")as? String
                destinationLocationLongitude = user.object(forKey: "ofc_lng") as? String
            }
            else{
                if isNotNull(user.object(forKey: "par_ofc_lat")){
                    destinationLocationLatitude = user.object(forKey: "par_ofc_lat")as? String
                    destinationLocationLongitude = user.object(forKey: "par_ofc_lng") as? String
                }
            }
        }
        func setHomeLocation(){
            if isNotNull(user.object(forKey: "home_lat")){
                homeLocationLatitude = user.object(forKey: "home_lat")as? String
                homeLocationLongitude = user.object(forKey: "home_lng") as? String
            }else{
                if isNotNull(user.object(forKey: "par_home_lat")){
                    homeLocationLatitude = user.object(forKey: "par_home_lat")as? String
                    homeLocationLongitude = user.object(forKey: "par_home_lng") as? String
                }
            }
        }
        if trueForHomeToDestinationFalseforDestinationToHome{
            if sourceLocation {
                setHomeLocation()
            }else{
                setDestinationLocation()
            }
        }else{
            if sourceLocation {
                setDestinationLocation()
            }else{
                setHomeLocation()
            }
        }
        if isNotNull(homeLocationLatitude){
            return CLLocationCoordinate2DMake(Double(homeLocationLatitude!)!, Double(homeLocationLongitude!)!)
        }else if isNotNull(destinationLocationLatitude){
            return CLLocationCoordinate2DMake(Double(destinationLocationLatitude!)!, Double(destinationLocationLongitude!)!)
        }
        return CLLocationCoordinate2D(latitude: 0,longitude: 0)
    }
    
    func trackCurrentUser() {
        var sourceCoordinate : CLLocationCoordinate2D?
        var destinationCoordinate : CLLocationCoordinate2D?
        if trueForHomeToDestinationFalseforDestinationToHome {
            sourceCoordinate = homeLocationCoordinate()
            destinationCoordinate = destinationLocationCoordinate()
        }else{
            sourceCoordinate = destinationLocationCoordinate()
            destinationCoordinate = homeLocationCoordinate()
        }
        let userInfo = Dbm().getUserInfo()
        let sourceRegion = CLCircularRegion(center: sourceCoordinate!, radius: TRACKING_REGION_RADIUS, identifier: getSourceRegionIdentifier(userInfo!.toDictionary() as NSDictionary))
        let destinationRegion = CLCircularRegion(center: destinationCoordinate!, radius: TRACKING_REGION_RADIUS, identifier: getDestinationRegionIdentifier(userInfo!.toDictionary() as NSDictionary))
        
        startMonitoringRegion(sourceRegion)
        startMonitoringRegion(destinationRegion)
        
        logMessage("Self source coordinate \(sourceCoordinate)")
        logMessage("Self destination coordinate \(destinationCoordinate)")
        
        cacheDictionary.setObject(userInfo!.toDictionary(), forKey: sourceRegion.identifier as NSCopying)
        cacheDictionary.setObject(userInfo!.toDictionary(), forKey: destinationRegion.identifier as NSCopying)
    }
    
    func trackUser(_ user:NSDictionary) {
        let sourceCoordinate = getUsersLocation(true, user: user)
        let destinationCoordinate = getUsersLocation(false, user: user)
        let sourceRegion = CLCircularRegion(center: sourceCoordinate, radius: TRACKING_REGION_RADIUS, identifier: getSourceRegionIdentifier(user))
        let destinationRegion = CLCircularRegion(center: destinationCoordinate, radius: TRACKING_REGION_RADIUS, identifier: getDestinationRegionIdentifier(user))
        startMonitoringRegion(sourceRegion)
        startMonitoringRegion(destinationRegion)
        
        logMessage("Passenger source coordinate \(sourceCoordinate)")
        logMessage("Passenger destination coordinate \(destinationCoordinate)")
        
        cacheDictionary.setObject(user, forKey: sourceRegion.identifier as NSCopying)
        cacheDictionary.setObject(user, forKey: destinationRegion.identifier as NSCopying)
    }
    
    func getSourceRegionIdentifier(_ user:NSDictionary) -> String {
        return "source_\(getUserId(user))"
    }
    
    func getDestinationRegionIdentifier(_ user:NSDictionary) -> String {
        return "destination_\(getUserId(user))"
    }
    
    func startMonitoringRegion(_ region:CLRegion) {
        locationManager.startMonitoring(for: region)
    }
    
    func onRegion(_ entered:Bool,identifier:String) {
        let userInfo = Dbm().getUserInfo()
        var messageToShow : String?
        let tForSourceFForDestination = identifier.contains("source")
        let tForSelfFforOthers = identifier.contains(loggedInUserId())
        let tForOwnerFforPassenger = self.trueForOwnerFalseforPassenger
        let tForHomeToDestinationFforDestinationToHome = self.trueForHomeToDestinationFalseforDestinationToHome
        let tForEnterFforExit = entered
        let eventId = "\(tForSelfFforOthers ? "1" : "0")\(tForHomeToDestinationFforDestinationToHome ? "1" : "0")\(tForSourceFForDestination ? "0" : "1")\(tForEnterFforExit ? "0" : "1")\(tForOwnerFforPassenger ? "0" : "1")"
        let uniqueEventId = "\(getRideId())\(eventId)"
        let possibleNameCarOwner = userInfo!.name!.firstName()
        let otherUser = cacheDictionary.object(forKey: identifier) as? NSDictionary
        let possibleNamePassenger = getUserNameToDisplay(otherUser)
        let eventForUserId = identifier.replacingOccurrences(of: "source_", with: "").replacingOccurrences(of: "destination_", with: "")
        
        if tForSelfFforOthers{
            if tForOwnerFforPassenger{
                if tForHomeToDestinationFforDestinationToHome{
                    if tForSourceFForDestination{
                        if tForEnterFforExit{
                        }else{
                            messageToShow = "\(possibleNameCarOwner) has started from home"
                        }
                    }else{
                        if tForEnterFforExit{
                            messageToShow = "\(possibleNameCarOwner) has reached \(destinationName())"
                        }else{
                        }
                    }
                }else{
                    if tForSourceFForDestination{
                        if tForEnterFforExit{
                        }else{
                            messageToShow = "\(possibleNameCarOwner) has started from \(destinationName())"
                        }
                    }else{
                        if tForEnterFforExit{
                            messageToShow = "\(possibleNameCarOwner) has reached home"
                        }else{
                        }
                    }
                }
            }else{
                if tForHomeToDestinationFforDestinationToHome{
                    if tForSourceFForDestination{
                        if tForEnterFforExit{
                        }else{
                            messageToShow = "\(possibleNamePassenger) has started from home"
                        }
                    }else{
                        if tForEnterFforExit{
                            messageToShow = "\(possibleNamePassenger) has reached \(destinationName())"
                        }else{
                        }
                    }
                }else{
                    if tForSourceFForDestination{
                        if tForEnterFforExit{
                        }else{
                            messageToShow = "\(possibleNamePassenger) has started from \(destinationName())"
                        }
                    }else{
                        if tForEnterFforExit{
                            messageToShow = "\(possibleNamePassenger) has reached home"
                        }else{
                        }
                    }
                }
                
            }
        }else{
            if tForOwnerFforPassenger{
                if tForHomeToDestinationFforDestinationToHome{
                    if tForSourceFForDestination{
                        if tForEnterFforExit{
                            messageToShow = "\(possibleNameCarOwner) has reached \(possibleNamePassenger)'s home"
                        }else{
                            messageToShow = "\(possibleNameCarOwner) has started from \(possibleNamePassenger)'s home"
                        }
                    }else{
                        if tForEnterFforExit{
                            messageToShow = "\(possibleNameCarOwner) has reached \(possibleNamePassenger)'s \(destinationNameForUser(otherUser))"
                        }else{
                            messageToShow = "\(possibleNameCarOwner) has started from \(possibleNamePassenger)'s \(destinationNameForUser(otherUser))"
                        }
                    }
                }else{
                    if tForSourceFForDestination{
                        if tForEnterFforExit{
                            messageToShow = "\(possibleNameCarOwner) has reached \(possibleNamePassenger)'s \(destinationNameForUser(otherUser))"
                        }else{
                            messageToShow = "\(possibleNameCarOwner) has started from \(possibleNamePassenger)'s \(destinationNameForUser(otherUser))"
                        }
                    }else{
                        if tForEnterFforExit{
                            messageToShow = "\(possibleNameCarOwner) has reached \(possibleNamePassenger)'s home"
                        }else{
                            messageToShow = "\(possibleNameCarOwner) has started from \(possibleNamePassenger)'s home"
                        }
                    }
                }
            }else{
                if tForHomeToDestinationFforDestinationToHome{
                    if tForSourceFForDestination{
                        if tForEnterFforExit{
                        }else{
                        }
                    }else{
                        if tForEnterFforExit{
                        }else{
                        }
                    }
                }else{
                    if tForSourceFForDestination{
                        if tForEnterFforExit{
                        }else{
                        }
                    }else{
                        if tForEnterFforExit{
                        }else{
                        }
                    }
                }
            }
        }
        var delay = 0.0
        if !tForSelfFforOthers {
            delay = delay + 10.0
        }
        if tForEnterFforExit {
            delay = delay + 10.0
        }
        if isNotNull(messageToShow) && isEventAlreadyOccured(uniqueEventId) == false {
            execMain({[weak self]  in guard let `self` = self else { return }
                if isNotNull(messageToShow) && self.isEventAlreadyOccured(uniqueEventId) == false {
                    self.setEventOccured(uniqueEventId)
                    self.sendMessageOnRideGroup(messageToShow!, eventId: eventId, uniqueEventId: uniqueEventId,eventForUserId: eventForUserId)
                    Acf().handleUpdateEventMessage(messageToShow, title: "Ride Update")
                    self.processEventsForRideAlertAndActions(tForSourceFForDestination, tForSelfFforOthers: tForSelfFforOthers, tForOwnerFforPassenger: tForOwnerFforPassenger, tForHomeToDestinationFforDestinationToHome: tForHomeToDestinationFforDestinationToHome, tForEnterFforExit: tForEnterFforExit, identifier: identifier)
                }
                },delay: delay)
        }
    }
    
    func processEventsForRideAlertAndActions(_ tForSourceFForDestination:Bool,tForSelfFforOthers:Bool,tForOwnerFforPassenger:Bool,tForHomeToDestinationFforDestinationToHome:Bool,tForEnterFforExit:Bool,identifier:String) {
        var messageToShow : String?
        if tForSelfFforOthers{
            if tForOwnerFforPassenger{
                if tForHomeToDestinationFforDestinationToHome{
                    if tForSourceFForDestination{
                    }else{
                        if tForEnterFforExit{
                            messageToShow = "You reached \(destinationName()). Hope you enjoyed! Let's settle the payment for the ride."
                        }
                    }
                }else{
                    if tForSourceFForDestination{
                    }else{
                        if tForEnterFforExit{
                            messageToShow = "You reached home. Hope you enjoyed! Let's settle the payment for the ride."
                        }
                    }
                }
            }else{
                if tForHomeToDestinationFforDestinationToHome{
                    if tForSourceFForDestination{
                    }else{
                        if tForEnterFforExit{
                            messageToShow = "You reached \(destinationName()). Hope you enjoyed! Let's settle the payment for the ride."
                        }
                    }
                }else{
                    if tForSourceFForDestination == false{
                    }else{
                        if tForEnterFforExit{
                            messageToShow = "You reached home. Hope you enjoyed! Let's settle the payment for the ride."
                        }
                    }
                }
            }
        }
        
        if isNotNull(messageToShow) && isNotNull(self.locationManager.location){
            if self.trueForOwnerFalseforPassenger {
                reportRideCompletionToServer(self.usersForRide, carOwnerId: loggedInUserId(), currentLocation: self.locationManager.location!, rideId: self.getRideId())
            }else{
                reportRideCompletionToServer(self.usersForRide, carOwnerId: getCarOwnerId(self.usersForRide), currentLocation: self.locationManager.location!, rideId: self.getRideId())
            }
        }
    }
    
    func cancelMyRide(){
        if isTracking && trueForOwnerFalseforPassenger == false {//passenger's ride cancellation
            let eventId = "Cancellation\(trueForOwnerFalseforPassenger)\(getRideId())"
            let uniqueEventId = "\(getRideId())\(eventId)"
            if isEventAlreadyOccured(uniqueEventId) == false {
                let userInfo = Dbm().getUserInfo()
                let messageToShow = "\(trueForOwnerFalseforPassenger ? "Car Owner" : "Passenger") : \(userInfo!.name!.firstName()) cancelled the Ride"
                self.sendMessageOnRideGroup(messageToShow, eventId: eventId, uniqueEventId: uniqueEventId,eventForUserId:"-1")
            }
        }
    }
    
    func checkForRideEventNotification(_ message:QBChatMessage) {
        if let messageType = message.customParameters?.object(forKey: MESSAGE_TYPE_KEY) as? String {
            if messageType == GroupEvents.rideGroupEvent && Acf().isQBReadyForItsUserDependentServices() && message.senderID != ServicesManager.instance().currentUser()!.id {
                LocalNotificationHelper.sharedInstance().scheduleNotification(title: "", message: message.text!, date: Date().dateByAddingSeconds(5) as NSDate, soundName: "", userInfo: nil)
            }
        }
    }
    
    func getUserNameToDisplay(_ user:NSDictionary?) -> String {
        if isNotNull(user){
            if let name = user!.object(forKey: "user_name") as? String{
                return name.firstName()
            }else if let name = user!.object(forKey: "name") as? String{
                return name.firstName()
            }
        }
        return "Passenger"
    }
    
    //MARK: - CLLocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        execMain({[weak self]  in guard let `self` = self else { return }
            if region is CLCircularRegion {
                self.onRegion(true,identifier: region.identifier)
            }
            logMessage(#function)
            },delay: DELAY_IN_NOTIFYING_REGION_ENTRY)
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        execMain({[weak self]  in guard let `self` = self else { return }
            if region is CLCircularRegion {
                self.onRegion(false,identifier: region.identifier)
            }
            logMessage(#function)
            },delay: DELAY_IN_NOTIFYING_REGION_EXIT)
    }
    
    func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
        logMessage(#function)
    }
    
    func locationManager(_ manager: CLLocationManager, didStartMonitoringFor region: CLRegion) {
        logMessage(#function)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if locations.count > 0 {
            currentLocationCoordinate = locations.last!.coordinate
        }
    }
    
    func getRideId() -> String {
        return "\(dateOfRide.day())\(dateOfRide.month())\(dateOfRide.year())\(trueForHomeToDestinationFalseforDestinationToHome == true ? "0" : "1")"
    }
    
    func isEventAlreadyOccured(_ uniqueEventId:String)->Bool{
        return isNotNull(UserDefaults.standard.object(forKey: uniqueEventId))
    }
    
    func setEventOccured(_ uniqueEventId:String){
        UserDefaults.standard.set(true, forKey: uniqueEventId)
    }
    
    func sendMessageOnRideGroup(_ messageToSend:String,eventId:String,uniqueEventId:String,eventForUserId:String){
        if isNotNull(self.groupChatDialogue) && Acf().isQBReadyForItsUserDependentServices() && currentLocationCoordinate != nil {
            let userInfo = Dbm().getUserInfo()
            let message = QBChatMessage()
            let senderID = ServicesManager.instance().currentUser()!.id
            message.text = messageToSend
            message.senderID = senderID
            message.markable = true
            message.dateSent = Date()
            #if DEBUG
                let mos = "iOSV\(UIApplication.appVersion())B\(UIApplication.appBuild())D".replacingOccurrences(of: ".", with: "")
            #else
                let mos = "iOSV\(UIApplication.appVersion())B\(UIApplication.appBuild())P".replacingOccurrences(of: ".", with: "")
            #endif
            message.customParameters = [MESSAGE_TYPE_KEY:GroupEvents.rideGroupEvent,"rideId":getRideId(),"eventDate":"\(currentTimeStamp())","lat":"\(currentLocationCoordinate!.latitude)","lng":"\(currentLocationCoordinate!.longitude)","userId":loggedInUserId(),"eventForUserId":eventForUserId,"eventId":eventId,"uniqueEventId":uniqueEventId,"sendersName":userInfo!.name!.firstName(),"mos":mos]
            ServicesManager.instance().chatService.send(message, to: self.groupChatDialogue!, saveToHistory: true, saveToStorage: true, completion: { (error) in
                if (error != nil) {
                    logMessage("\(error)")
                    Dbm().addFailedQBMessage(["messageId":message.id!])
                }
            })
        }
    }
}
