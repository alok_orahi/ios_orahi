//
//  OrahiSpecificConstants.h
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit

// constants strings
let RIDE_GROUP_DISPLAY_NAME_OTHERS_PREFIX = "Ride Group : "
let RIDE_GROUP_DISPLAY_NAME_SELF_PREFIX = "My"
let RIDE_GROUP_DISPLAY_NAME_SELF_SUFFIX = "Ride Group"
let RIDE_GROUP_SUFFIX = "RIDERS GROUP"
let RIDE_GROUP_SEPERATOR = " "
let SUPPORT_EMAIL = "support@orahi.com"
let USER_DEFAULT_STATUS = ""
let GOOGLE_API_KEY = "AIzaSyCzHX7ice9Aq1H1MjWGZkHkruNQ0XigUB0"
let QB_USER_NAME_PREFIX = "orahi_"
let QB_USER_ADMIN_QBID = "00001"
let APP_NAME = "Orahi"
let DEVICE_TYPE = "iOS"
let USER_DEFAULTS_EXTENSION_GROUP_ID = "group.orahi.extension"
let FONT_BOLD = "SFUIText-Semibold"
let FONT_SEMI_BOLD = "SFUIText-Medium"
let FONT_REGULAR = "SFUIText-Regular"
let FONT_LIGHT = "SFUIText-Light"

let QUICKBLOX_APPLICATION_ID = 35886 as UInt
let QUICKBLOX_AUTH_KEY = "VeDO8rR3spCMkZg"
let QUICKBLOX_AUTH_SECRET = "SUSdYQPrXNWX-Qh"
let QUICKBLOX_ACCOUNT_KEY = "812vabYda1zfjNP9dXw2"
let QUICKBLOX_API_END_POINT = "https://apiorahi.quickblox.com"
let QUICKBLOX_CHAT_END_POINT = "chatorahi.quickblox.com"

let KOCHOVA_GUID_STRING = "koorahi-ios-xoo44vzk"

// constants keys
let KEY_USERID = "userId"
let MESSAGE_TYPE_KEY = "messageType"
let KEY_WEEKLY_PLANNER = "WEEKLY_RIDE_PLAN_CACHE"
let KEY_STORYBOARD_IDENTIFIER = "identifierToNibNameMap"

// constants cache manager keys
let CACHED_USER_PROFILE_DATA = "CACHED_USER_PROFILE_DATA"
let CACHED_USER_SAVINGS_DATA = "CACHED_USER_SAVINGS_DATA"
let CACHED_DRN_SPECIFICATIONS = "CACHED_DRN_SPECIFICATIONS"
let TEMP_PHONE_NUMBER = "TEMP_PHONE_NUMBER"
let APP_UNIQUE_BUILD_IDENTIFIER = "APP_UNIQUE_BUILD_IDENTIFIER"
let CACHED_IMAGE_TO_UPLOAD_LATER = "CACHED_IMAGE_TO_UPLOAD_LATER"
let NULL_OBJECT_TO_SEND_ON_SERVER = NSMutableString(string:"null")
let CACHED_REFER_SHARE_URL = "CACHED_REFER_SHARE_URL"
let USER_TYPE_CORPORATE = "corporate"
let USER_TYPE_STUDENT = "student"
let USER_TYPE_OTHER = "other"
let STATIC_HOME_ADDRESS = "Dwarka"
let STATIC_DESTINATION_ADDRESS = "Gurgaon"
let KEY_LAST_UPDATE_OF_SYNC_GROUPS_AND_PLACES = "KEY_LAST_UPDATE_OF_SYNC_GROUPS_AND_PLACES"
let KEY_POM_SIGNUP_COMPLETED = "key_registration_completed_pom"
let KEY_CARPOOL_SIGNUP_COMPLETED = "key_registration_completed_carpool"

// constants values
let MINOR_DELAY = 0.1
let RIDE_LOCATION_UPDATE_FREQUENCY = 120.0
let RIDE_VIEW_TAB_INDEX = 0
let CHAT_VIEW_TAB_INDEX = 1
let MINIMUM_RIDE_COUNT_FOR_ADVANCED_USER = -1
let DELAY_TIME_TO_RESET_TO_CURRENT_PLANNER = 120.0 as TimeInterval
let DELAY_TIME_TO_RESET_USER_LIST = 60.0 * 30 as TimeInterval
let MAXIMUM_NO_OF_NAMES_AT_A_TIME = 3
let PAGE_SIZE = 10
let DELAY_IN_NOTIFYING_REGION_EXIT = 0.0
let DELAY_IN_NOTIFYING_REGION_ENTRY = 3.0
let GAP_IN_BETWEEN_CONSECUTIVE_RIDE_EVENT_MESSAGES = 5.0
let AUTO_DISMISS_ALERTS_MESSAGE_DURATION = 20.0
let MINIMUM_LENGTH_LIMIT_USERNAME = 1
let MAXIMUM_LENGTH_LIMIT_USERNAME = 32
let MINIMUM_LENGTH_LIMIT_FIRST_NAME = 0
let MAXIMUM_LENGTH_LIMIT_FIRST_NAME = 64
let MINIMUM_LENGTH_LIMIT_PASSWORD = 1
let MAXIMUM_LENGTH_LIMIT_PASSWORD = 20
let MINIMUM_LENGTH_LIMIT_MOBILE_NUMBER = 7
let MAXIMUM_LENGTH_LIMIT_MOBILE_NUMBER = 14
let MINIMUM_LENGTH_LIMIT_EMAIL = 7
let MAXIMUM_LENGTH_LIMIT_EMAIL = 64
let POP_UP_VIEW_CORNER_RADIUS = 2 as CGFloat
let ANNOTATION_TRANSFORM_ADJUST = 0.7 as CGFloat
let GET_TRAVELLERS_AUTO_REFRESH_TIME_WHEN_SIGNED_IN_FOREGROUND_APP = 240.0
let GET_TRAVELLERS_AUTO_REFRESH_TIME_WHEN_SIGNED_IN_BACKGROUND_APP = 60.0 * 60.0 * 4.0
let GET_TRAVELLERS_AUTO_REFRESH_TIME_WHEN_NOT_SIGNED_IN_FOREGROUND_APP = 600.0
let GET_TRAVELLERS_AUTO_REFRESH_TIME_WHEN_NOT_SIGNED_IN_BACKGROUND_APP = 60.0 * 60.0 * 4.0
let WEEKLY_PLANS_AUTO_REFRESH_TIME = 240.0
let MAXIMUM_ANNOTATIONS_COUNT_LIMIT = 10
let TRACKING_REGION_RADIUS = 1024.0
let MAXIMUM_CONTACT_LIST_COUNT = 50
let MAXIMUM_NOTIFICATIONS_AT_A_TIME = 80
let AUTO_SYNC_MINIMUM_POM_SYNC_TIME_INTERVAL = 20
let MAXIMUM_TRAVELLERS_LIST_SUGGESTION = 25
let DEVELOPER_OPTION_PIN = "200488"
let OPACITY_WHEN_PARTIALLY_HIDDEN_FOR_LIST_VIEW = 0.6 as Float
let OPACITY_WHEN_HIDDEN_FOR_LIST_VIEW = 0.0 as Float
let OPACITY_WHEN_HIDDEN_FOR_MAP_VIEW = 0.0 as Float
let RESTORE_HIDDEN_VIEW_TIME = 1.2 as TimeInterval
let SHOW_ANIMATION_DURATION = 0.1 as TimeInterval
let HIDE_ANIMATION_DURATION = 1.0 as TimeInterval
let OFFER_VIEW_CORNER_RADIUS = 8 as CGFloat
let SWITCH_SCALE = 0.65 as CGFloat
let DELAYED_ENTRY_ALLOWED_MAXIMUM_DISTANCE = PLACES_TRACKING_REGION_RADIUS
let PLACES_TRACKING_REGION_RADIUS = 400.0
let PLACES_EXIT_REGION_RADIUS_THRESHOLD = 650.0
let MAXIMUM_DELAY_ALLOWED_FOR_REFFERALS = 45
let EXTRA_TIME_TO_HOLD_APP_EXECUTION_TO_PERFORM_IMPORTANT_EVENTS = 180.0
let SIGNIFICANT_LOCATION_DIFFERENCE_THRESHOLD_METERS = PLACES_TRACKING_REGION_RADIUS
let SIGNIFICANT_LOCATION_MAXIMUM_UPDATE_IN_MINUTES = 2
let SIGNIFICANT_LOCATION_MINIMUM_UPDATE_IN_MINUTES = 30
let DUMMY_GROUP_NAME = "My Family"
let DUMMY_GROUP_IMAGE_NAME = "demoGroupType1"

// constant feature enable/disable flags
var ENABLE_LOGGING = false
var ENABLE_API_REQUEST_LOGGING = false
var ENABLE_POM_LOCATION_SERVICE_DEBUG_INFO = false
var ENABLE_FEATURE_DEBUG_INFO = false
var ENABLE_DEVELOPER_OPTION_WITH_OUT_PASSWORD = false
var ENABLE_SECRET_RESET_BUTTON = false
var ENABLE_FACEBOOK_PROFILE_PICTURE_SUGGESTION = false
var ENABLE_QUICKBLOX_CONTACT_PRESENCE_DISPLAY = false
var ENABLE_ATTACHMENT_BUTTON_ON_CHAT_SCREEN = false

// constant urls
let APP_FACEBOOK_PAGE_LINK = "https://www.facebook.com/ridewithOrahi/?fref=ts"
let APP_TWITTER_PAGE_LINK = "https://twitter.com/ridewithOrahi"
let APP_LINKEDIN_PAGE_LINK = "https://www.linkedin.com/company/4844251"
let APP_YOUTUBE_PAGE_LINK = "https://www.youtube.com/results?search_query=orahi+news+carpooling"
let APP_WEBSITE_LINK = "https://www.orahi.com/"
let APP_ABOUT_US_PAGE_LINK = "https://www.orahi.com/?page_id=2482"
let APP_FAQ_PAGE_LINK = "https://www.orahi.com/?page_id=2566"
let APP_CONTACT_US_PAGE_LINK = "https://www.orahi.com/#contact-us"
let APP_TERMS_OF_USE_AND_PRIVACY_POLICY = "https://www.orahi.com/?page_id=2554"
let APP_NO_IMAGE_URL = "http://www.flystarbow.com/images/starbow_no_image_icon.jpg"

// constants objects
let APPDELEGATE = (UIApplication.shared.delegate as! AppDelegate)
let DEVICE_WIDTH  = UIScreen.main.bounds.size.width as CGFloat
let DEVICE_HEIGHT = UIScreen.main.bounds.size.height as CGFloat
let DEVICE_ID =  UIDevice.current.identifierForVendor!.uuidString
let USER_DEFAULTS = UserDefaults.standard
let OFFER_SCREEN_TOP_BANNER_IMAGE_URL = BASE_URL_FOR_OFFER_IMAGES + "shareReferral.jpg"
let USER_PLACEHOLDER_IMAGE = UIImage(named: "user")
let POM_GROUP_PLACEHOLDER_IMAGE = UIImage(named: "defaultPoMGroup")

// constants color
let APP_THEME_VOILET_COLOR = UIColor(red: 94/255, green: 36/255, blue: 101/255, alpha: 1.0)
let APP_THEME_RED_COLOR = UIColor(red: 230/255, green: 112/255, blue: 99/255, alpha: 1.0)
let APP_THEME_RED_COLOR_TYPE_2 = UIColor(red: 238/255, green: 52/255, blue: 74/255, alpha: 1.0)
let APP_THEME_GREEN_COLOR = UIColor(red: 22/255, green: 168/255, blue: 120/255, alpha: 1.0)
let APP_THEME_LIGHT_GREEN_COLOR = UIColor(red: 150/255, green: 201/255, blue: 178/255, alpha: 1.0)
let APP_THEME_BLUE_COLOR = UIColor(red: 32/255, green: 193/255, blue: 221/255, alpha: 1.0)
let APP_THEME_GRAY_COLOR = UIColor.gray.withAlphaComponent(0.95)
let APP_THEME_DARK_GRAY_COLOR = UIColor(red: 112/255, green: 112/255, blue: 113/255, alpha: 1.0)
let APP_THEME_LIGHT_GRAY_COLOR = UIColor.lightGray
let APP_THEME_LIGHT_GRAY_COLOR_2 = UIColor(red: 245/255, green: 245/255, blue: 245/255, alpha:1.0)
let APP_THEME_YELLOW_COLOR = UIColor(red: 250/255, green: 184/255, blue: 22/255, alpha:0.95)
let APP_THEME_YELLOW_COLOR_2 = UIColor(red: 250/255, green: 183/255, blue: 49/255, alpha:1.0)

let DRN_TITLE_COLOR = UIColor(red: 97/255, green: 97/255, blue: 97/255, alpha:1.0)
let DRN_DESCRIPTION_COLOR = UIColor(red: 94/255, green: 94/255, blue: 94/255, alpha:1.0)

// constants descriptions
let MESSAGE_TEXT___FOR_NETWORK_NOT_REACHABILITY = "😕 The Internet appears to be offline."
let MESSAGE_TEXT___FOR_SERVER_NOT_REACHABILITY = "😅 Connection failed!. Try again!"
let MESSAGE_TEXT___FOR_WITHDRAWL_SUCCESSFULL = "😃 Your Request for the withdrawl is Registered"
let MESSAGE_TEXT___FOR_FUNCTIONALLITY_PENDING_MESSAGE = "🙁 We are still developing this feature. Thanks for your patience"
let MESSAGE_TEXT___FOR_OTP_SENT = "😜 OTP has been sent to your phone number."
let MESSAGE_TEXT___FOR_SAME_ADDRESS_ERROR = "😡 Addresses can not be same , please change & try again."
let MESSAGE_TEXT___EMAIL_VERIFICATION_INFO = "\n🤔 A verification e-mail link will be sent to your e-mail address. Please click the 'verify' button in the e-mail to complete this verification stage."
let MESSAGE_TEXT___MOBILE_INPUT_INFO = "😡 Please input your phone number."
let MESSAGE_TEXT___MOBILE_VERIFICATION_INFO = "😡 Please input the OTP received on your registered mobile number."
let MESSAGE_TEXT___MOBILE_VERIFICATION_COMPLETED = "😊 You have successfully verified you mobile number"
let MESSAGE_TEXT___WHY_TO_SELECT_LOCATION = "🤔 Lets us know your location and time of traveling.We will use these informations to provide you nearby travellers suggestion."
let MESSAGE_TEXT___ADD_INFO = "\n🤗 No Worries !\nIf you don't find name in the list."
let MESSAGE_TEXT___USER_NOT_AVAILABLE_FOR_CHAT = "😓 This user is currently not available for Chat."
let MESSAGE_TITLE___YOU_ARE_NOT_PARTICIPANT_ON_THIS_GROUP = "You're no longer a participant."
let MESSAGE_TEXT___YOU_ARE_NOT_PARTICIPANT_ON_THIS_GROUP = "😭 You can't send and receive messages on this group."
let MESSAGE_TITLE___YOU_HAVE_BLOCKED_THIS_USER = "😝 You have blocked this user."
let MESSAGE_TEXT___YOU_HAVE_BLOCKED_THIS_USER = "😭 You can't send and receive messages."
let MESSAGE_TEXT___SET_RIDE_TIME_FIRST = "🙄 Please set the ride time"
let MESSAGE_TITLE___THIS_IS_YOUR_MY_RIDERS_GROUP = "\(RIDE_GROUP_DISPLAY_NAME_SELF_PREFIX) \(RIDE_GROUP_DISPLAY_NAME_SELF_SUFFIX)"
let MESSAGE_TEXT___THIS_IS_YOUR_MY_RIDERS_GROUP = "😍 This is your Riders Group, Passengers riding with you will automatically get added here."
let MESSAGE_TEXT___BLOCK_UNBLOCK_NOT_READY = "🤒 System not ready ! please try again after some time"
let MESSAGE_TEXT___UPDATING_RIDE_PLAN = "🤗 Updating your ride plan ..."
let MESSAGE_TEXT___ENTER_PIN_DEVELOPER_OPTIONS_INFO = "\n😑 Enter your Pin to proceed"
let MESSAGE_TITLE___RESEND_OTP_ALERT = "Resend OTP?"
let MESSAGE_TEXT___RESEND_OTP_ALERT = "\n🤔 Do you want us to send your OTP again?\nPlease note this action will expire your previous OTP."
let MESSAGE_TEXT___UPLOADING_YOUR_NEW_PROFILE_PICTURE = "👼🏻 Uploading your picture ,it may take few seconds."
let MESSAGE_TEXT___PROFILE_PICTURE_COMMENT = "Your profile picture is nice ! 👍🏻👍🏻👍🏻"
let MESSAGE_TITLE___ADDRESS_UPDATE_WARNING = "Please Note"
let MESSAGE_TEXT___ADDRESS_UPDATE_WARNING = "Your current Offer may not work on the next ride after modifying your addresses."
let MESSAGE_TITLE___SESSION_EXPIRED = "Session Expired"
let MESSAGE_TEXT___SESSION_EXPIRED = "Your session has been expired , Please login again"
let MESSAGE_TEXT___ERA_SEND_TRUE = "Activating this will allow our intelligent system to send automatic notifications to your potential co-traveller"
let MESSAGE_TEXT___ERA_RECEIVE_TRUE = "Activating this will allow our intelligent system to send you suggestions to increase your ride probability"
let MESSAGE_TEXT___ERA_FALSE = "Deactivating this feature will reduce your ride probability"
let MESSAGE_TEXT___PENDING_RIDE_REQUEST = "You have few pending ride requests for approval ! check out"
let MESSAGE_TITLE___POM_DETAILS = "Place"
let MESSAGE_TEXT___POM_DETAILS = "\nSend notifications to group members when you arrive or leave this place.\n"
let MESSAGE_TEXT___ACM_TRUE = "Activating this will help other members to get in touch with you even before ride confirmation."
let MESSAGE_TEXT___ACM_FALSE = "Deactivating this will stop other members to get in touch with you before ride confirmation."

// constant notifications
let NOTIFICATION_PROFILE_UPDATED = "NOTIFICATION_PROFILE_UPDATED"
let NOTIFICATION_UPDATE_PROFILE_SCREEN = "NOTIFICATION_UPDATE_PROFILE_SCREEN"
let NOTIFICATION_UPDATE_USERS = "NOTIFICATION_UPDATE_USERS"
let NOTIFICATION_ADDRESS_AND_TIMING_UPDATED = "NOTIFICATION_ADDRESS_AND_TIMING_UPDATED"
let NOTIFICATION_USER_SIGNED_IN = "NOTIFICATION_USER_SIGNED_IN"
let NOTIFICATION_NEED_USER_UPDATE = "NOTIFICATION_NEED_USER_UPDATE"
let NOTIFICATION_SAVINGS_UPDATED = "NOTIFICATION_SAVINGS_UPDATED"
let NOTIFICATION_ACCOUNT_SUMMARY_UPDATED = "NOTIFICATION_ACCOUNT_SUMMARY_UPDATED"
let NOTIFICATION_FAVOURITES_UPDATED = "NOTIFICATION_FAVOURITES_UPDATED"
let NOTIFICATION_SOME_MESSAGE_RECEIVED = "NOTIFICATION_SOME_MESSAGE_RECEIVED"
let NOTIFICATION_UPDATE_SIDE_MENU_NEEDED = "NOTIFICATION_UPDATE_SIDE_MENU_NEEDED"
let NOTIFICATION_PUSH_NOTIFICATION_RECEIVED = "NOTIFICATION_PUSH_NOTIFICATION_RECEIVED"
let NOTIFICATION_IN_APP_NOTIFICATION_COUNT_UPDATED = "NOTIFICATION_IN_APP_NOTIFICATION_COUNT_UPDATED"
let NOTIFICATION_WEEKLY_NOTIFICATION_COUNT_UPDATED = "NOTIFICATION_WEEKLY_NOTIFICATION_COUNT_UPDATED"
let NOTIFICATION_RIDE_EVENT_UPDATED = "NOTIFICATION_RIDE_EVENT_UPDATED"
let NOTIFICATION_RESET_IDLE_TIMER_RIDE_HOME = "NOTIFICATION_RESET_IDLE_TIMER_RIDE_HOME"
let NOTIFICATION_PULL_UP_VIEW_CLOSED = "NOTIFICATION_PULL_UP_VIEW_CLOSED"
let NOTIFICATION_PULL_UP_VIEW_STATE_CHANGED = "NOTIFICATION_PULL_UP_VIEW_STATE_CHANGED"
let NOTIFICATION_RELOAD_OFFERS = "NOTIFICATION_RELOAD_OFFERS"
let NOTIFICATION_MOVE_TO_NEXT_CARD_POSITIVE = "NOTIFICATION_MOVE_TO_NEXT_CARD_POSITIVE"
let NOTIFICATION_MOVE_TO_NEXT_CARD_NEGATIVE = "NOTIFICATION_MOVE_TO_NEXT_CARD_NEGATIVE"
let NOTIFICATION_POPUP_VIEW_CONTROLLER_ABOUT_TO_HIDE = "NOTIFICATION_POPUP_VIEW_CONTROLLER_ABOUT_TO_HIDE"
let NOTIFICATION_BLOCK_LIST_UPDATED = "NOTIFICATION_BLOCK_LIST_UPDATED"
let NOTIFICATION_DRN_FINISHED = "NOTIFICATION_DRN_FINISHED"
let NOTIFICATION_CLAIM_OFFER = "NOTIFICATION_CLAIM_OFFER"
let NOTIFICATION_HIDE_FOR_MAP_VIEW = "NOTIFICATION_HIDE_FOR_MAP_VIEW"
let NOTIFICATION_DO_CLEAN_REFRESH = "NOTIFICATION_DO_CLEAN_REFRESH"
let NOTIFICATION_SET_TRAVELLER_LAST_ACTION_STATE_TO_SUCCESS = "NOTIFICATION_SET_TRAVELLER_LAST_ACTION_STATE_TO_SUCCESS"
let NOTIFICATION_APP_DOWNLOAD_REFFERAL_UPDATED = "NOTIFICATION_APP_DOWNLOAD_REFFERAL_UPDATED"
let NOTIFICATION_NEED_PLACES_UPDATE = "NOTIFICATION_NEED_PLACES_UPDATE"
let NOTIFICATION_NEED_NOTIFICATIONS_UPDATE = "NOTIFICATION_NEED_NOTIFICATIONS_UPDATE"
let NOTIFICATION_GROUPS_PLACES_UPDATED = "NOTIFICATION_GROUPS_PLACES_UPDATED"
