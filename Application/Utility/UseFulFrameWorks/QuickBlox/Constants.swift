//
//  Constants.swift
//  sample-chat-swift
//
//  Created by Anton Sokolchenko on 3/30/15.
//  Copyright (c) 2015 quickblox. All rights reserved.
//

import Foundation

let kDialogsPageLimit:UInt = 100
let kUsersLimit = 15
let kMessageContainerWidthPadding:CGFloat = 40.0
