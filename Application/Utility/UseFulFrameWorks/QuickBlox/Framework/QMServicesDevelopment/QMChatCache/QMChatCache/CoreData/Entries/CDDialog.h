#import "_CDDialog.h"
#import <Quickblox/Quickblox.h>

@interface CDDialog : _CDDialog {}

- (QBChatDialog *)toQBChatDialog;
- (void)updateWithQBChatDialog:(QBChatDialog *)dialog;

@end
