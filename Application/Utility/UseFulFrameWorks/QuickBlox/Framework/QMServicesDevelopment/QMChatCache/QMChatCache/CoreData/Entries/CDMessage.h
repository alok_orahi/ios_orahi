#import "_CDMessage.h"
#import <Quickblox/Quickblox.h>
#import <Quickblox/QBChatMessage.h>

@interface CDMessage : _CDMessage {}
- (QBChatMessage *)toQBChatMessage;
- (void)updateWithQBChatMessage:(QBChatMessage *)message;
@end
