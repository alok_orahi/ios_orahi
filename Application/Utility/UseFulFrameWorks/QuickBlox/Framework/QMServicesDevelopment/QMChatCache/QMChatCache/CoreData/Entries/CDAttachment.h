
#import "_CDAttachment.h"
#import <Quickblox/Quickblox.h>

@interface CDAttachment : _CDAttachment {}

- (QBChatAttachment *)toQBChatAttachment;
- (void)updateWithQBChatAttachment:(QBChatAttachment *)attachment;

@end
