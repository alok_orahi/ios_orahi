#import "_CDUser.h"
#import <Bolts/Bolts.h>
#import <Quickblox/Quickblox.h>

@interface CDUser : _CDUser {}

- (QBUUser *)toQBUUser;
- (void)updateWithQBUser:(QBUUser *)user;

@end
