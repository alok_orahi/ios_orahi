#import "_CDContactListItem.h"
#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@class QBContactListItem;
@interface CDContactListItem : _CDContactListItem {}

- (QBContactListItem *)toQBContactListItem;
- (void)updateWithQBContactListItem:(QBContactListItem *)contactListItem;
- (BOOL)isEqualQBContactListItem:(QBContactListItem *)other;

@end
