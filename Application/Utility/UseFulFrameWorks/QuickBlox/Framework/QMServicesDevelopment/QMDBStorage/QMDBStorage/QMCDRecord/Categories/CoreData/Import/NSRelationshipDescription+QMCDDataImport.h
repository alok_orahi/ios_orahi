//
//  NSRelationshipDescription+QMCDDataImport.h
//  QMCD Record
//
//  Created by Injoit on 9/4/11.
//  Copyright (c) 2015 Quickblox Team. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Quickblox/Quickblox.h>
#import <Bolts/Bolts.h>
#import <CoreData/CoreData.h>
@interface NSRelationshipDescription (QMCDRecordDataImport)

- (NSString *) QM_primaryKey;

@end
