//
//  QMServiceManager.swift
//  sample-chat-swift
//
//  Created by Injoit on 5/22/15.
//  Copyright (c) 2015 quickblox. All rights reserved.
//

import Foundation
import Bolts

/**
 *  Implements user's memory/cache storing, error handling, show top bar notifications.
 */
class ServicesManager: QMServicesManager {
    
    var currentDialogID : String = ""
    var isProcessingLogOut = false
    var colors = [
        UIColor(red: 0.992, green:0.510, blue:0.035, alpha:1.000),
        UIColor(red: 0.039, green:0.376, blue:1.000, alpha:1.000),
        UIColor(red: 0.984, green:0.000, blue:0.498, alpha:1.000),
        UIColor(red: 0.204, green:0.644, blue:0.251, alpha:1.000),
        UIColor(red: 0.580, green:0.012, blue:0.580, alpha:1.000),
        UIColor(red: 0.396, green:0.580, blue:0.773, alpha:1.000),
        UIColor(red: 0.765, green:0.000, blue:0.086, alpha:1.000),
        UIColor.red,
        UIColor(red: 0.786, green:0.706, blue:0.000, alpha:1.000),
        UIColor(red: 0.740, green:0.624, blue:0.797, alpha:1.000)
    ]
    
    var contactListService : ContactListService!
    var notificationService: NotificationService!
    
    override init() {
        super.init()
        
        self.setupNotificationServices()
        self.setupContactServices()
        self.setupUsersServices()
    }
    
    fileprivate func setupContactServices() {
        self.contactListService = ContactListService()
    }
    
    fileprivate func setupNotificationServices() {
        self.notificationService = NotificationService()
    }
    
    fileprivate func setupUsersServices() {
        self.usersService.loadFromCache()
    }
    
    
    func handleNewMessage(_ message: QBChatMessage, dialogID: String) {
        guard self.currentDialogID != dialogID else { return }
        guard message.senderID != self.currentUser()?.id else { return }
        guard self.chatService.dialogsMemoryStorage.chatDialog(withID: dialogID) != nil else { return }
        if isNotNull(currentUser()){
            if currentUser()!.id != message.senderID {
                var shouldContinue = true
                if isNotNull(message)&&isNotNull(message.dateSent)&&(message.dateSent!.isToday() == false) || isNotNull(message)&&isNotNull(message.delayed)&&message.delayed&&isNotNull(message.dateSent)&&message.dateSent!.isEarlierThanDate(Date().dateBySubtractingMinutes(5)) {
                    shouldContinue = false
                }
                if shouldContinue {
                    Acf().playSoundAndVibrateForNewMessageIfNeeded()
                }
            }
        }
    }
    
    // MARK: Last activity date
    
    var lastActivityDate: Date? {
        get {
            let defaults = UserDefaults.standard
            return defaults.value(forKey: "last_activity_date") as! Date?
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "last_activity_date")
            defaults.synchronize()
        }
    }
    
    // MARK: QMServiceManagerProtocol
    
    override func handleErrorResponse(_ response: QBResponse) {
        super.handleErrorResponse(response)
        guard self.isAuthorized() else { return }
        var errorMessage : String
        if response.status.rawValue == 502 {
            errorMessage = "Bad Gateway, please try again"
        } else if response.status.rawValue == 0 {
            errorMessage = "Connection network error, please try again"
        } else {
            errorMessage = (response.error?.error?.localizedDescription.replacingOccurrences(of: "(", with: "", options: NSString.CompareOptions.caseInsensitive, range: nil).replacingOccurrences(of: ")", with: "", options: NSString.CompareOptions.caseInsensitive, range: nil))!
        }
        #if DEBUG
            showNotification(errorMessage, showOnNavigation: false, showAsError: true)
        #endif
    }
    
    func downloadUsers(_ ids:NSArray,success:(([QBUUser]?) -> Void)?, error:((NSError?) -> Void)?) {
        self.usersService.getUsersWithLogins(ids as! [String]).continue({
            (task : BFTask!) -> AnyObject! in
            if (task.error != nil) {
                error?(task.error as NSError?)
                return nil
            }
            success?(self.allCachedUsers())
            return nil
        })
    }
    
    func searchUsers(_ searchText:String,success:(([QBUUser]?) -> Void)?, error:((NSError?) -> Void)?) {
        self.usersService.searchUsers(withFullName: searchText).continue({
            (task : BFTask!) -> AnyObject! in
            if (task.error != nil) {
                error?(task.error as NSError?)
                return nil
            }
            success?(task.result as! [QBUUser])
            return nil
        })
    }
    
    func searchUsersWithIDs(_ IDs:[NSNumber],success:(([QBUUser]?) -> Void)?, error:((NSError?) -> Void)?) {
        self.usersService.searchUsers(withIDs: IDs).continue({
            (task : BFTask!) -> AnyObject! in
            if (task.error != nil) {
                error?(task.error as NSError?)
                return nil
            }
            success?(task.result as! [QBUUser])
            return nil
        })
    }
    
    func searchUsersWithLogins(_ logins:[NSString],success:(([QBUUser]?) -> Void)?, error:((NSError?) -> Void)?) {
        if logins.count > 0 {
            self.usersService.searchUsers(withLogins: logins).continue({
                (task : BFTask!) -> AnyObject! in
                if (task.error != nil) {
                    error?(task.error as NSError?)
                    return nil
                }
                success?(task.result as! [QBUUser])
                return nil
            })
        }else{
            success?(NSMutableArray() as! [QBUUser])
        }
    }
    
    func color(forUser user:QBUUser) -> UIColor {
        let users = self.usersService.usersMemoryStorage.unsortedUsers() as? [QBUUser]
        let userIndex = (users!).index(of: self.usersService.usersMemoryStorage.user(withID: user.id)!)
        if userIndex < self.colors.count {
            return self.colors[userIndex!]
        } else {
            return UIColor.black
        }
    }
    
    func allCachedUsers() -> [QBUUser] {
        return self.usersService.usersMemoryStorage.unsortedUsers()
    }
    
    // MARK: QMChatServiceDelegate
    
    
    /**
     Sorted users
     
     - returns: sorted [QBUUser] from usersService.usersMemoryStorage.unsortedUsers()
     */
    func sortedUsers() -> [QBUUser]? {
        
        let unsortedUsers = self.usersService.usersMemoryStorage.unsortedUsers()
        
        let sortedUsers = unsortedUsers.sorted(by: { (user1, user2) -> Bool in
            return user1.login!.compare(user2.login!, options:NSString.CompareOptions.numeric) == ComparisonResult.orderedAscending
        })
        
        return sortedUsers
    }
    
    /**
     Sorted users without current user
     
     - returns: [QBUUser]
     */
    func sortedUsersWithoutCurrentUser() -> [QBUUser]? {
        
        guard let sortedUsers = self.sortedUsers() else {
            return nil
        }
        
        let sortedUsersWithoutCurrentUser = sortedUsers.filter({ $0.id != self.currentUser()?.id})
        
        return sortedUsersWithoutCurrentUser
    }
    
    // MARK: QMChatServiceDelegate
    
    override func chatService(_ chatService: QMChatService, didAddMessageToMemoryStorage message: QBChatMessage, forDialogID dialogID: String) {
        
        super.chatService(chatService, didAddMessageToMemoryStorage: message, forDialogID: dialogID)
        
        if self.authService.isAuthorized {
            self.handleNewMessage(message, dialogID: dialogID)
        }
    }
    
    func logoutUserWithCompletion(_ completion: @escaping (_ result: Bool)->()) {
        
        if self.isProcessingLogOut {
            
            completion(false)
            return
        }
        
        self.isProcessingLogOut = true
        
        let logoutGroup = DispatchGroup()
        
        logoutGroup.enter()
        
        let deviceIdentifier = UIDevice.current.identifierForVendor!.uuidString
        
        QBRequest.unregisterSubscription(forUniqueDeviceIdentifier: deviceIdentifier, successBlock: { (response: QBResponse!) -> Void in
            //
            logMessage("Successfuly unsubscribed from push notifications")
            logoutGroup.leave()
            
        }) { (error: QBError?) -> Void in
            //
            logMessage("Push notifications unsubscribe failed")
            logoutGroup.leave()
        }
        
        logoutGroup.notify(queue: DispatchQueue.main) {
            [weak self] () -> Void in
            // Logouts from Quickblox, clears cache.
            guard let strongSelf = self else { return }
            
            strongSelf.logout {
                
                strongSelf.isProcessingLogOut = false
                
                completion(true)
                
            }
        }
    }

    
    func userServiceObject()->QMUsersService {
        return self.usersService
    }
    
    func userService()->QMUsersService {
        return self.usersService
    }
    
}
