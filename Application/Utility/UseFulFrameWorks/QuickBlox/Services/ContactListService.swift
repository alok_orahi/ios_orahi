//
//  ContactListService.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import Foundation

typealias CLSCompletionBlock = (_ returnedData :Any?) ->()

/**
 *  Service responsible for working with contacts
 */
class ContactListService : NSObject,QMServiceManagerProtocol,QMContactListServiceDelegate,QBChatDelegate {
    
    var service : QMContactListService?
    var blockedUsersList : QBPrivacyList?
    let blockedUsersListName = "blocked_list"
    var blockedUserListLocalCache : NSMutableArray?
    var completionBlock : CLSCompletionBlock!
    
    override init() {
        super.init()
        self.setupService()
    }
    
    fileprivate func setupService() {
        if ENABLE_QUICKBLOX_CONTACT_PRESENCE_DISPLAY {
            if isNotNull(self.service){
                self.service?.remove(self)
            }
            self.service = nil
            self.service = QMContactListService(serviceManager:self)
            self.service?.add(self)
        }
        QBChat.instance().removeDelegate(self)
        QBChat.instance().addDelegate(self)
        QBChat.instance().retrievePrivacyList(withName: blockedUsersListName)
        syncLocalCacheWithPrivacyList()
    }
    
    func refreshPresence() {
        if !ENABLE_QUICKBLOX_CONTACT_PRESENCE_DISPLAY {return}
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(ContactListService.refreshPresencePrivate), object: nil)
        self.perform(#selector(ContactListService.refreshPresencePrivate), with: nil, afterDelay: 8)
    }
    
    func refreshPresencePrivate() {
        if !ENABLE_QUICKBLOX_CONTACT_PRESENCE_DISPLAY {return}
        self.service?.remove(self)
        setupService()
    }
    
    func addUsersOnContactsList(_ users:[QBUUser]) {
        if !ENABLE_QUICKBLOX_CONTACT_PRESENCE_DISPLAY {return}
        for user in users {
            self.service?.addUser(toContactListRequest: user, completion: { (completed) in
            })
        }
    }
    
    func isSystemReadyToPerformBlockUnBlockOperation() -> Bool {
        let isReady = isNotNull(blockedUserListLocalCache) && isNotNull(blockedUsersList) && isInternetConnectivityAvailable(false)
        if isReady == false {
            QBChat.instance().retrievePrivacyList(withName: blockedUsersListName)
        }
        return isReady
    }
    
    func isUserBlocked(_ qbId:UInt) -> Bool {
        if isNotNull(blockedUsersList){
            for o in blockedUsersList!.privacyItems {
                if ((o).valueForType == qbId){
                    return true
                }
            }
        }else{
            if isNotNull(blockedUserListLocalCache){
                for o in blockedUserListLocalCache! {
                    if (o as! NSString).isEqual(to: "\(qbId)"){
                        return true
                    }
                }
            }
        }
        return false
    }
    
    func syncLocalCacheWithPrivacyList() {
        if let data = CacheManager.sharedInstance.loadObject("blockedUserListLocalCache") as? NSMutableString {
            blockedUserListLocalCache = data.components(separatedBy: ",") as! NSMutableArray
        }
        if isNotNull(blockedUsersList){
            blockedUserListLocalCache = NSMutableArray()
            for o in blockedUsersList!.privacyItems {
                blockedUserListLocalCache?.add("\((o).valueForType)")
            }
        }
        if isNotNull(blockedUserListLocalCache){
            blockedUserListLocalCache = NSMutableArray(array: NSSet(array:blockedUserListLocalCache! as [AnyObject]).allObjects)
        }
        if isNotNull(blockedUserListLocalCache){
            CacheManager.sharedInstance.saveObject(blockedUserListLocalCache?.componentsJoined(by: ",") , identifier: "blockedUserListLocalCache")
        }else{
            CacheManager.sharedInstance.saveObject(nil, identifier: "blockedUserListLocalCache")
        }
    }
    
    func createBlockedUserList(_ completion:@escaping CLSCompletionBlock){
        if isNull(blockedUsersList){
            blockedUsersList = QBPrivacyList(name: blockedUsersListName)
            blockedUsersList?.addObject(QBPrivacyItem(type: USER_ID, valueForType: 123, action:ALLOW))
            QBChat.instance().setPrivacyList(blockedUsersList)
            QBChat.instance().setDefaultPrivacyListWithName(blockedUsersListName)
            QBChat.instance().setActivePrivacyListWithName(blockedUsersListName)
            self.completionBlock = completion
        }
    }
    
    func performUserAction(_ qbId:UInt,block:Bool) {
        func removeFromBlockList(){
            let blockedUsersListTemp = QBPrivacyList(name: blockedUsersListName)
            for item in blockedUsersList!.privacyItems {
                if (item).valueForType == qbId {
                    //skip the item into new list which needs to be removed
                }else{
                    blockedUsersListTemp.addObject(QBPrivacyItem(type: item.type, valueForType: item.valueForType, action:item.action))
                }
            }
            QBChat.instance().setPrivacyList(blockedUsersListTemp)
            QBChat.instance().setDefaultPrivacyListWithName(self.blockedUsersListName)
            QBChat.instance().setActivePrivacyListWithName(self.blockedUsersListName)
            self.blockedUsersList = blockedUsersListTemp
            QBChat.instance().retrievePrivacyList(withName: self.blockedUsersListName)
        }
        func addToBlockList(){
            blockedUsersList!.addObject(QBPrivacyItem(type: USER_ID, valueForType: qbId, action:DENY))
            blockedUsersList!.addObject(QBPrivacyItem(type: GROUP_USER_ID, valueForType: qbId, action:DENY))
            QBChat.instance().setPrivacyList(blockedUsersList)
            QBChat.instance().setDefaultPrivacyListWithName(blockedUsersListName)
            QBChat.instance().setActivePrivacyListWithName(blockedUsersListName)
            QBChat.instance().retrievePrivacyList(withName: blockedUsersListName)
        }
        if isNotNull(blockedUsersList){
            if block {
                addToBlockList()
            }else{
                removeFromBlockList()
            }
            syncLocalCacheWithPrivacyList()
        }
    }
    
    //Mark :- QBChatDelegate
    
    func chatDidRemovedPrivacyList(withName name: String) {
        logMessage(#function)
    }
    
    func chatDidSetPrivacyList(withName name: String) {
        logMessage(#function)
        QBChat.instance().retrievePrivacyList(withName: name)
    }
    
    func chatDidNotSetPrivacyList(withName name: String, error: Any?) {
        logMessage(#function)
    }
    
    func chatDidReceive(_ privacyList: QBPrivacyList){
        logMessage(#function)
        if privacyList.name.contains(blockedUsersListName){
            blockedUsersList = privacyList
            syncLocalCacheWithPrivacyList()
            if completionBlock != nil {
                completionBlock!(true)
                self.completionBlock = nil
            }
            NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_BLOCK_LIST_UPDATED), object: nil)
            logMessage("\n\n\n****BLOCKED LIST : Blocked users count \(privacyList.privacyItems.count)*************\n\n\n")
        }
    }
    
    func chatDidNotReceivePrivacyList(withName name: String, error: Any?){
        logMessage(#function)
        if name.contains(blockedUsersListName){
            createBlockedUserList({ (returnedData) in
            })
        }
    }
    
    //Mark :- QMServiceManagerProtocol
    
    func currentUser() -> QBUUser? {
        return ServicesManager.instance().currentUser()
    }
    
    func isAuthorized() -> Bool {
        return ServicesManager.instance().isAuthorized()
    }
    
    func handleErrorResponse(_ response: QBResponse) {
    }
    
    func contactListService(_ contactListService: QMContactListService, didReceiveContactItemActivity userID: UInt, isOnline: Bool, status: String?) {
        if !ENABLE_QUICKBLOX_CONTACT_PRESENCE_DISPLAY {return}
        Acf().presenceInformation.setObject(NSNumber(value: isOnline as Bool), forKey: "\(userID)" as NSCopying)
    }
}
