//
//  ALPredictionsDelegate.swift
//  ALPlacesViewController
//
//  Created by Alex Littlejohn on 2015/07/15.
//  Copyright (c) 2015 zero. All rights reserved.
//

import UIKit

internal class ALPredictionsDelegate: ALCollectionViewDelegate, UICollectionViewDelegateFlowLayout {
   
    let predictions: [ALPrediction]
    let APIkey: String
    var onLocationPicked: ALPlacesPickerCallback?
    
    init(predictions: [ALPrediction], APIkey: String, onLocationPicked: ALPlacesPickerCallback?) {
        self.APIkey = APIkey
        self.predictions = predictions
        super.init()
        self.onLocationPicked = onLocationPicked
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let item = predictions[(indexPath as NSIndexPath).row]
        let cell = collectionView.cellForItem(at: indexPath) as! ALPredictionCollectionViewCell
        
        collectionView.isUserInteractionEnabled = false
        cell.working = true
        
        ALPlaceDetailsInteractor()
            .setPlaceID(item.id)
            .setAPIkey(APIkey)
            .onCompletion { place, error in
                
                collectionView.isUserInteractionEnabled = true
                cell.working = false
                
                if let e = error {
                    self.onLocationPicked?(nil,nil,nil, e)
                } else if let p = place {
                    self.onLocationPicked?(p.name,p.address, p.coordinate, nil)
                }
        }.details()
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return predictions.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return ALPredictionCollectionViewCell.cellSize()
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ALPredictionCollectionViewCellIdentifier, for: indexPath) as! ALPredictionCollectionViewCell
        
        let item = predictions[(indexPath as NSIndexPath).row]
        
        cell.configureWithPrediction(item)
        
        return cell
    }
}
