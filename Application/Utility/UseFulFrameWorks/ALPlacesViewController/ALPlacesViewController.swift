
//
//  ALPlacesViewController.swift
//  ALPlacesViewController
//
//  Created by Alex Littlejohn on 2015/07/10.
//  Copyright (c) 2015 zero. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import DZNEmptyDataSet

internal let ALPlaceCollectionViewCellIdentifier = "ALPlaceCollectionViewCell"
internal let ALUserCollectionViewCellIdentifier = "ALUserCollectionViewCell"
internal let ALPredictionCollectionViewCellIdentifier = "ALPredictionCollectionViewCell"
internal let ALPlaceCollectionViewHeaderIdentifier = "ALPlaceCollectionViewHeader"

public typealias ALPlacesPickerCallback = (_ name: String?,_ address: String?, _ coordinate: CLLocationCoordinate2D?, _ error: NSError?) -> Void

open class ALPlacesViewController: UIViewController,DZNEmptyDataSetSource {

    fileprivate let searchView = ALSearchBar()
    fileprivate let collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: ALStickyHeaderFlowLayout())

    fileprivate let placesDelegate = ALPlacesDelegate()
    fileprivate var predictionsDelegate: ALPredictionsDelegate?
    fileprivate let keyboardObserver = ALKeyboardObservingView()
    
    fileprivate var userLocation: ALLocation?
    fileprivate var userLocationInteractor: ALUserLocationInteractor?
    fileprivate var onLocationPicked: ALPlacesPickerCallback?
    var placeholderText = "Search here"
    
    /// Your Google API key
    /// Throws an exception if this is not set when doing web service calls to google apis
    open var APIKey: String!
    
    required public init(APIKey: String, completion: ALPlacesPickerCallback?) {
        super.init(nibName: nil, bundle: nil)
        self.APIKey = APIKey
        self.onLocationPicked = completion
        commonInit()
    }
    
    override public init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        commonInit()
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        
        onLocationPicked = nil
        userLocationInteractor = nil
        userLocation = nil
        predictionsDelegate = nil
    }
    
    internal func commonInit() {
        NotificationCenter.default.addObserver(self, selector: #selector(ALPlacesViewController.keyboardDidShow(_:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ALPlacesViewController.keyboardDidHide(_:)), name: NSNotification.Name.UIKeyboardDidHide, object: nil)
        
        collectionView.register(ALPlaceCollectionViewCell.self, forCellWithReuseIdentifier: ALPlaceCollectionViewCellIdentifier)
        collectionView.register(ALUserCollectionViewCell.self, forCellWithReuseIdentifier: ALUserCollectionViewCellIdentifier)
        collectionView.register(ALPredictionCollectionViewCell.self, forCellWithReuseIdentifier: ALPredictionCollectionViewCellIdentifier)
        collectionView.register(ALPlacesHeaderView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: ALPlaceCollectionViewHeaderIdentifier)
    }
    
    override open func viewDidLoad() {
        super.viewDidLoad()

        let headerFrame = CGRect(x: 0, y: 0, width: view.bounds.width, height: view.bounds.height/2)
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        
        layout.headerReferenceSize = headerFrame.size
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        
        collectionView.frame = view.bounds
        collectionView.delegate = placesDelegate
        collectionView.dataSource = placesDelegate
        collectionView.backgroundColor = UIColor.white
        collectionView.keyboardDismissMode = UIScrollViewKeyboardDismissMode.interactive

        searchView.onSearch = autocomplete
        searchView.frame = CGRect(x:0, y: 0, width: view.bounds.size.width, height: 0)
        searchView.layoutSearchableState()
        searchView.placeholderTextToShow = placeholderText
        searchView.onDoneButton = {
            self.onLocationPicked?(nil,nil, nil, nil)
        }
        
        view.addSubview(collectionView)
        view.addSubview(searchView)
        
        userLocationInteractor = ALUserLocationInteractor()
            .onCompletion { location, error in
                if let l = location {
                    self.populateWithLocation(l)
                    self.populatePlaces(l)
                }
            }.start()
        
        placesDelegate.onLocationPicked = onLocationPicked
        
        placesDelegate.onScroll = { offset in
            if offset.y > self.view.bounds.size.height/3 {
                UIView.animate(withDuration: 0.2, animations: {
                    self.searchView.layoutScrollingState()
                })
            } else {
                UIView.animate(withDuration: 0.2, animations: {
                    self.searchView.layoutSearchableState()
                })
            }
            self.maximiseHeader()
        }
        
        execMain ({ (completed) in
            self.minimiseHeader()
        },delay:0.4)
    
        collectionView.emptyDataSetSource = self
        collectionView.reloadEmptyDataSet()
    }
    
    //MARK: - DZNEmptyDataSet Delegate & Data Source
    public func verticalOffset(forEmptyDataSet scrollView: UIScrollView!) -> CGFloat {
        return 220
    }

    public  func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString!{
        let attributes = [NSFontAttributeName : UIFont.init(name: FONT_SEMI_BOLD, size: 20)!]
        var message = "Search"
        if safeInt(searchView.searchField.text?.length) > 0 {
            message = "No Results"
        }
        if (isInternetConnectivityAvailable(false)==false){
            message = ""
        }
        return NSAttributedString(string: message, attributes: attributes)
    }
    
    public func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString!{
        let attributes = [NSFontAttributeName : UIFont.init(name: FONT_SEMI_BOLD, size: 14)!]
        var message = "you can search address by using bar at the top"
        if safeInt(searchView.searchField.text?.length) > 0 {
            message = "try searching with different keyword"
        }
        if (isInternetConnectivityAvailable(false)==false){
            message = MESSAGE_TEXT___FOR_NETWORK_NOT_REACHABILITY
        }
        return NSAttributedString(string: message, attributes: attributes)
    }
    
    public func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage!{
        if (isInternetConnectivityAvailable(false)==false){
            return UIImage(named:"wifi")
        }
        return UIImage(named:"searchLightGray")
    }
    
    internal func minimiseHeader(){
        let minimumHeaderHeight = searchView.statusHeight + searchView.searchPaddingV + searchView.searchHeight + searchView.searchPaddingV
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            layout.headerReferenceSize.height = minimumHeaderHeight
        })
    }
    
    internal func maximiseHeader(){
        let newHeight = view.bounds.size.height/2
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            layout.headerReferenceSize.height = newHeight
        })
    }
    
    internal func keyboardDidShow(_ notification: Foundation.Notification) {
        minimiseHeader()
    }
    
    internal func keyboardDidHide(_ notification: Foundation.Notification) {
        maximiseHeader()
    }
    
    fileprivate func populatePlaces(_ location: CLLocation) {
        
        ALPlacesSearchInteractor()
            .setCoordinate(location.coordinate)
            .setAPIkey(APIKey)
            .setRadius(Meters(distance: 5000))
            .onCompletion { places, error in
                
                if let p = places {
                    self.placesDelegate.places = p
                    self.collectionView.reloadData()
                }
                
            }.search()
    }
    
    fileprivate func populateWithLocation(_ location: CLLocation) {
        userLocation = ALLocation()
        userLocation?.address = ""
        userLocation?.coordinate = location.coordinate
        
        placesDelegate.userLocation = userLocation
        collectionView.reloadData()
        
        ALGeocodingInteractor()
            .setCoordinate(location.coordinate)
            .onCompletion { (address, error) -> Void in
                self.userLocation?.address = address
                self.collectionView.reloadData()
            }
            .start()
    }
    
    internal func autocomplete(_ text: String) {
        
        collectionView.isUserInteractionEnabled = true
        
        if text.lengthOfBytes(using: String.Encoding.utf8) < 3 {
            
            if (collectionView.delegate as! NSObject) != placesDelegate {
                collectionView.delegate = placesDelegate
                collectionView.dataSource = placesDelegate
                collectionView.reloadData()
            }
            
        } else {
            let inter = ALPlacesAutocompleteInteractor()
                .setAPIkey(APIKey)
                .setInput(text)
            
            if let l = userLocation, let c = l.coordinate {
                inter.setRadius(Meters(distance: 5000))
                    .setCoordinate(c)
                
            }
            
            inter.onCompletion { predictions, error in
                var results = [ALPrediction]()
                
                if let p = predictions {
                    results = p
                }
                
                self.predictionsDelegate = ALPredictionsDelegate(predictions: results, APIkey: self.APIKey, onLocationPicked: self.onLocationPicked)
                self.collectionView.delegate = self.predictionsDelegate
                self.collectionView.dataSource = self.predictionsDelegate
                self.collectionView.reloadData()
            }.autocomplete()
        }
    }
}
