//
//  UIDevice+DeviceModel.swift
//  ALPlacesViewController
//
//  Created by Alex Littlejohn on 2015/07/14.
//  Copyright (c) 2015 zero. All rights reserved.
//

import UIKit

internal extension UIDevice {
    internal class func isIPad() -> Bool {
        return UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad
    }
    
    internal class func isIPhone() -> Bool {
        return UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone
    }
    
    internal class func isIPhone4() -> Bool {
        return UIDevice.isIPhone() && UIScreen.main.bounds.size.height < 568.0
    }
    
    internal class func floatVersion() -> Float {
        return (UIDevice.current.systemVersion as NSString).floatValue
    }
}
