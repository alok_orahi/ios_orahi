//
//  ALPlacesSearchInteractor.swift
//  ALPlacesViewController
//
//  Created by Alex Littlejohn on 2015/07/15.
//  Copyright (c) 2015 zero. All rights reserved.
//

import UIKit
import CoreLocation
import AFNetworking

typealias ALPlacesSearchCompletion = (_ places: [ALPlace]?, _ error: NSError?) -> Void
let placesSearchURL = "https://maps.googleapis.com/maps/api/place/nearbysearch/json"

class ALPlacesSearchInteractor {
    
    fileprivate let errorDomain = "ALPlacesSearchInteractor"
    
    fileprivate var APIkey: String?
    fileprivate var radius: Meters?
    fileprivate var coordinate: CLLocationCoordinate2D?
    fileprivate var completion: ALPlacesSearchCompletion?
    
    func setRadius(_ radius: Meters) -> Self {
        self.radius = radius
        return self
    }
    
    func setCoordinate(_ coordinate: CLLocationCoordinate2D) -> Self {
        self.coordinate = coordinate
        return self
    }
    
    func setAPIkey(_ APIkey: String) -> Self {
        self.APIkey = APIkey
        return self
    }
    
    func onCompletion(_ completion: @escaping ALPlacesSearchCompletion) -> Self {
        self.completion = completion
        return self
    }
    
    func search() -> Self {
        if let r = radius, let c = coordinate, let a = APIkey {
            let parameters = [
                "radius": "\(r.distance)",
                "key": a,
                "location": "\(c.latitude),\(c.longitude)"
            ]
            let manager = AFHTTPSessionManager()
            manager.responseSerializer = AFHTTPResponseSerializer()
            manager.get(placesSearchURL, parameters: parameters, progress: { (progress) -> Void in
                }, success: { (urlSessionDataTask, responseObject) -> Void in
                    let response = parsedJson(responseObject as? Data, methodName: "")
                    if response != nil {
                        if let result = response as? [String: Any], let status = result["status"] as? String {
                            switch status {
                            case PlaceStatusCodes.OK.rawValue:
                                if let results = result["results"] as? [[String: Any]] {
                                    var places = [ALPlace]()
                                    for place in results {
                                        let dictPlace = place as NSDictionary
                                        if let name = place["name"] as? String,
                                            let address = place["vicinity"] as? String,
                                            let lat = dictPlace.value(forKeyPath: "geometry.location.lat") as? Double,
                                            let lon = dictPlace.value(forKeyPath: "geometry.location.lng") as? Double {
                                            let p = ALPlace()
                                            p.name = name
                                            p.address = address
                                            p.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: lon)
                                            places.append(p)
                                        }
                                    }
                                    self.completion?(places, nil)
                                }
                                break
                            case PlaceStatusCodes.ZERO_RESULTS.rawValue:
                                self.completion?([ALPlace](), nil)
                                break
                            default:
                                let e = NSError(domain: self.errorDomain, code: 0, userInfo: [NSLocalizedDescriptionKey : status])
                                self.completion?(nil, e)
                                break
                            }
                        }
                    }
            }){(urlSessionDataTask, error) -> Void in
                (self.completion?(nil, error as NSError?))!
            }
        } else {
            assert(false, "ALPlacesSearchInteractor requires a radius, location and an APIKey")
        }
        return self
    }
}
