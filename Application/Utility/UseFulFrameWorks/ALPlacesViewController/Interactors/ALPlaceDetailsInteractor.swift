//
//  ALPlaceDetailsInteractor.swift
//  ALPlacesViewController
//
//  Created by Alex Littlejohn on 2015/07/16.
//  Copyright (c) 2015 zero. All rights reserved.
//

import UIKit
import CoreLocation
import AFNetworking

typealias ALPlaceDetailsCompletion = (_ place: ALPlace?, _ error: NSError?) -> Void
let placeDetailsURL = "https://maps.googleapis.com/maps/api/place/details/json"

class ALPlaceDetailsInteractor {
    
    fileprivate let errorDomain = "ALPlaceDetailsInteractor"
    
    fileprivate var placeID: String?
    fileprivate var APIkey: String?
    fileprivate var completion: ALPlaceDetailsCompletion?
    
    func setPlaceID(_ placeID: String) -> Self {
        self.placeID = placeID
        return self
    }
    
    func setAPIkey(_ APIkey: String) -> Self {
        self.APIkey = APIkey
        return self
    }
    
    func onCompletion(_ completion: @escaping ALPlaceDetailsCompletion) -> Self {
        self.completion = completion
        return self
    }
    
    func details() -> Self {
        
        if let id = placeID, let a = APIkey {
            let parameters = ["placeid": id, "key": a]
            let manager = AFHTTPSessionManager()
            manager.responseSerializer = AFHTTPResponseSerializer()
            manager.get(placeDetailsURL, parameters: parameters, progress: { (progress) -> Void in
                }, success: { (urlSessionDataTask, responseObject) -> Void in
                    let response = parsedJson(responseObject as? Data, methodName: "")
                    if response != nil {
                        if let result = response as? [String: Any], let status = result["status"] as? String {
                            switch status {
                            case PlaceStatusCodes.OK.rawValue:
                                
                                if let place = result["result"] as? NSDictionary,
                                    let name = place["name"] as? String,
                                    let address = place["formatted_address"] as? String,
                                    let lat = place.value(forKeyPath: "geometry.location.lat") as? Double,
                                    let lon = place.value(forKeyPath: "geometry.location.lng") as? Double {
                                    
                                    let p = ALPlace()
                                    
                                    p.name = name
                                    p.address = address
                                    p.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: lon)
                                    
                                    self.completion?(p, nil)
                                }
                                
                                break
                            default:
                                let e = NSError(domain: self.errorDomain, code: 0, userInfo: [NSLocalizedDescriptionKey : status])
                                self.completion?(nil, e)
                                break
                            }
                        }
                    }
            }){(urlSessionDataTask, error) -> Void in
                self.completion?(nil, error as NSError?)
            }
        } else {
            assert(false, "ALPlaceDetailsInteractor requires a place id and an APIKey \n • id:\(placeID) \n • key:\(APIkey)")
        }
        
        return self
    }
}
