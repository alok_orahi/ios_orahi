//
//  ALSettingsInteractor.swift
//  ALPlacesViewController
//
//  Created by Alex Littlejohn on 2015/07/15.
//  Copyright (c) 2015 zero. All rights reserved.
//

import UIKit

class ALSettingsInteractor: NSObject {
   
    func showSettings(_ title: String, message: String, inViewController: UIViewController) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: LocalizedString("settings.button"), style: .default) { (alertAction) in
            
            // THIS IS WHERE THE MAGIC HAPPENS!!!!
            if let appSettings = URL(string: UIApplicationOpenSettingsURLString) {
                UIApplication.shared.openURL(appSettings)
            }
        }
        alertController.addAction(settingsAction)
        
        let cancelAction = UIAlertAction(title: LocalizedString("settings.cancel"), style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        inViewController.present(alertController, animated: true, completion: nil)
    }
}
