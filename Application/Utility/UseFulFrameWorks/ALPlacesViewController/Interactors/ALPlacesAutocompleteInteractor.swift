//
//  ALPlacesAutocompleteInteractor.swift
//  ALPlacesViewController
//
//  Created by Alex Littlejohn on 2015/07/15.
//  Copyright (c) 2015 zero. All rights reserved.
//

import UIKit
import CoreLocation
import AFNetworking

struct Meters {
    var distance = 0
}

enum PlaceStatusCodes: String {
    case OK = "OK"
    case ZERO_RESULTS = "ZERO_RESULTS"
    case OVER_QUERY_LIMIT = "OVER_QUERY_LIMIT"
    case REQUEST_DENIED = "REQUEST_DENIED"
    case INVALID_REQUEST = "INVALID_REQUEST"
    case UNKNOWN_ERROR = "UNKNOWN_ERROR"
    case NOT_FOUND = "NOT_FOUND"
}

typealias ALPlacesAutocompleteCompletion = (_ predictions: [ALPrediction]?, _ error: NSError?) -> Void
let placesAutocompleteURL = "https://maps.googleapis.com/maps/api/place/autocomplete/json"

class ALPlacesAutocompleteInteractor {
    
    fileprivate let errorDomain = "ALPlacesAutocompleteInteractor"
    
    fileprivate var input: String?
    fileprivate var APIkey: String?
    fileprivate var radius: Meters?
    fileprivate var coordinate: CLLocationCoordinate2D?
    fileprivate var completion: ALPlacesAutocompleteCompletion?
    
    func setInput(_ input: String) -> Self {
        self.input = input
        return self
    }
    
    func setRadius(_ radius: Meters) -> Self {
        self.radius = radius
        return self
    }
    
    func setCoordinate(_ coordinate: CLLocationCoordinate2D) -> Self {
        self.coordinate = coordinate
        return self
    }
    
    func setAPIkey(_ APIkey: String) -> Self {
        self.APIkey = APIkey
        return self
    }
    
    func onCompletion(_ completion: @escaping ALPlacesAutocompleteCompletion) -> Self {
        self.completion = completion
        return self
    }
    
    func autocomplete() -> Self {
        if let i = input, let a = APIkey {
            
            var parameters = ["input": i, "key": a]
            
            if let r = radius {
                parameters["radius"] = "\(r.distance)"
            }
            
            if let c = coordinate {
                parameters["location"] = "\(c.latitude),\(c.longitude)"
            }
            
            let manager = AFHTTPSessionManager()
            manager.responseSerializer = AFHTTPResponseSerializer()
            manager.get(placesAutocompleteURL, parameters: parameters, progress: { (progress) -> Void in
                }, success: { (urlSessionDataTask, responseObject) -> Void in
                    let response = parsedJson(responseObject as? Data, methodName: "")
                    if response != nil {
                        if let result = response as? [String: Any], let status = result["status"] as? String {
                            
                            if status == PlaceStatusCodes.ZERO_RESULTS.rawValue {
                                self.completion?([ALPrediction](), nil)
                            } else if status == PlaceStatusCodes.OK.rawValue, let results = result["predictions"] as? [[String: Any]] {
                                var predictions = [ALPrediction]()
                                for r in results {
                                    if let name = r["description"] as? String, let id = r["place_id"] as? String {
                                        predictions.append(ALPrediction(id: id, name: name))
                                    }
                                }
                                self.completion?(predictions, nil)
                                
                            } else {
                                let e = NSError(domain: self.errorDomain, code: 0, userInfo: [NSLocalizedDescriptionKey : status])
                                self.completion?(nil, e)
                            }
                            
                        }
                    }
            }){(urlSessionDataTask, error) -> Void in
                (self.completion?(nil, error as NSError?))!
            }
        } else {
            assert(false, "ALPlacesAutocompleteInteractor has nil text input and/or APIkey \n • input:\(input) \n • key:\(APIkey)")
        }
        
        return self
    }
    
    
}
