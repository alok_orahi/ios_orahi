//
//  ALUserLocationInteractor.swift
//  ALPlacesViewController
//
//  Created by Alex Littlejohn on 2015/07/15.
//  Copyright (c) 2015 zero. All rights reserved.
//

import UIKit
import CoreLocation

typealias ALUserLocationCompletion = (_ location: CLLocation?, _ error: NSError?) -> Void

class ALUserLocationInteractor: NSObject, CLLocationManagerDelegate {
   
    fileprivate let errorDomain = "ALUserLocationInteractor"
    fileprivate let locationManager = CLLocationManager()
    fileprivate var completion: ALUserLocationCompletion?
    
    func onCompletion(_ completion: @escaping ALUserLocationCompletion) -> Self {
        self.completion = completion
        return self
    }
    
    func start() -> Self {
        locationManager.delegate = self as! CLLocationManagerDelegate
        locationManager.requestAlwaysAuthorization()
        self.locationManager.allowsBackgroundLocationUpdates = true
        locationManager.startUpdatingLocation()
        return self
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.denied || status == CLAuthorizationStatus.restricted {
            let errorArgs = [NSLocalizedDescriptionKey: LocalizedString("location.notauthorized.message")]
            let error = NSError(domain: errorDomain, code: 1, userInfo: errorArgs)
            locationManager.stopUpdatingLocation()
            completion?(nil, error)
        } else {
            completion?(locationManager.location, nil)
        }
        locationManager.stopUpdatingLocation()
        locationManager.delegate = nil
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations[0] as? CLLocation {
            locationManager.stopUpdatingLocation()
            locationManager.delegate = nil
            completion?(location, nil)
        }
    }
}
