//
//  ALCollectionViewCell.swift
//  ALPlacesViewController
//
//  Created by Alex Littlejohn on 2015/07/15.
//  Copyright (c) 2015 zero. All rights reserved.
//

import UIKit

class ALCollectionViewCell: UICollectionViewCell {
    static let horizontalPadding: CGFloat = 12
    static let horizontalSpacing: CGFloat = 12
    static let verticalPadding: CGFloat = 19
    static let verticalSpacing: CGFloat = 2
    
    static let nameFont = UIFont.init(name: FONT_REGULAR, size: 18)!
    static let addressFont = UIFont.init(name: FONT_REGULAR, size: 13)!
    
    static func cellSize() -> CGSize {
        let height = verticalPadding + nameFont.lineHeight + verticalSpacing + addressFont.lineHeight + verticalPadding
        let width = UIScreen.main.bounds.size.width
        return CGSize(width: width, height: height)
    }
    
    let nameLabel = UILabel()
    let addressLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
    }
}
