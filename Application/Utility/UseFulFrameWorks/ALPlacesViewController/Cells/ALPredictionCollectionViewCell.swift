//
//  ALPredictionCollectionViewCell.swift
//  ALPlacesViewController
//
//  Created by Alex Littlejohn on 2015/07/16.
//  Copyright (c) 2015 zero. All rights reserved.
//

import UIKit

class ALPredictionCollectionViewCell: UICollectionViewCell {

    static let horizontalPadding: CGFloat = 14
    static let verticalPadding: CGFloat = 14
    static let font = UIFont.init(name: FONT_REGULAR, size: 18)!
    static func cellSize() -> CGSize {
        
        let height = verticalPadding + font.lineHeight + verticalPadding
        let width = UIScreen.main.bounds.size.width
        return CGSize(width: width, height: height)
    }
    
    let iconView = UIImageView()
    var outlineView = UIView()
    let iconViewSize = CGSize(width: 30, height: 30)

    let predictionLabel = UILabel()
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
    
    var working = false {
        didSet {
            if working {
                activityIndicator.startAnimating()
            } else {
                activityIndicator.stopAnimating()
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        
        predictionLabel.font = ALPredictionCollectionViewCell.font
        predictionLabel.textColor = UIColor(white: 0.2, alpha: 1)
        predictionLabel.lineBreakMode = NSLineBreakMode.byTruncatingTail
        
        activityIndicator.hidesWhenStopped = true
        activityIndicator.stopAnimating()

        
        iconView.contentMode = UIViewContentMode.scaleAspectFit
        iconView.image = UIImage(named: "marker", in: Bundle(for: ALPlacesViewController.self), compatibleWith: nil)

        outlineView.frame = self.bounds.insetBy(dx: 5, dy: 5)
        outlineView.backgroundColor = UIColor.clear
        outlineView.layer.borderColor = APP_THEME_LIGHT_GRAY_COLOR_2.cgColor
        outlineView.layer.borderWidth = 0.8

        addSubview(outlineView)
        addSubview(iconView)
        addSubview(predictionLabel)
        addSubview(activityIndicator)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        
        let size = bounds.size
        
        let activitySize = activityIndicator.frame.size
        let activityX = size.width - (activitySize.width + ALPredictionCollectionViewCell.horizontalPadding)
        let activityY = size.height/2 - activitySize.height/2
        
        activityIndicator.frame.origin = CGPoint(x: activityX, y: activityY)

        let iconX = ALCollectionViewCell.horizontalPadding
        let iconY = size.height/2 - iconViewSize.height/2
        iconView.frame = CGRect(x: iconX, y: iconY, width: iconViewSize.width, height: iconViewSize.height)

        let labelX = ALPredictionCollectionViewCell.horizontalPadding + ALCollectionViewCell.horizontalPadding + iconViewSize.width
        let labelY = ALPredictionCollectionViewCell.verticalPadding
        let labelWidth = size.width - ALPredictionCollectionViewCell.horizontalPadding * 3 - activitySize.width
        let labelHeight = size.height - ALPredictionCollectionViewCell.verticalPadding * 2
        
        predictionLabel.frame = CGRect(x: labelX, y: labelY, width: labelWidth, height: labelHeight)
    }
    
    func configureWithPrediction(_ prediction: ALPrediction) {
        predictionLabel.text = prediction.name
        setNeedsLayout()
    }

    
}
