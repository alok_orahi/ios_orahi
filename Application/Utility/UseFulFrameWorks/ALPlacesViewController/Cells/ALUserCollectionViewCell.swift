//
//  ALUserCollectionViewCell.swift
//  ALPlacesViewController
//
//  Created by Alex Littlejohn on 2015/07/15.
//  Copyright (c) 2015 zero. All rights reserved.
//

import UIKit

class ALUserCollectionViewCell: ALCollectionViewCell {

    let iconView = UIImageView()
    let iconViewSize = CGSize(width: 30, height: 30)
    let pulsingLayer = ALPulsingHalo()
    var outlineView = UIView()

    override func commonInit() {
        nameLabel.font = ALCollectionViewCell.nameFont
        addressLabel.font = ALCollectionViewCell.addressFont
        
        nameLabel.textColor = UIColor(white: 0.2, alpha: 1)
        addressLabel.textColor = UIColor(white: 0.4, alpha: 1)
        
        nameLabel.lineBreakMode = NSLineBreakMode.byTruncatingTail
        addressLabel.lineBreakMode = NSLineBreakMode.byTruncatingTail
        
        iconView.contentMode = UIViewContentMode.center
        iconView.image = UIImage(named: "userMarker", in: Bundle(for: ALPlacesViewController.self), compatibleWith: nil)
        
        pulsingLayer.radius = 22
        

        layer.insertSublayer(pulsingLayer, at: 0)
        
        backgroundColor = UIColor.white
        
        outlineView.frame = self.bounds.insetBy(dx: 5, dy: 5)
        outlineView.backgroundColor = UIColor.clear
        outlineView.layer.borderColor = APP_THEME_LIGHT_GRAY_COLOR_2.cgColor
        outlineView.layer.borderWidth = 0.8
        
        addSubview(outlineView)
        addSubview(nameLabel)
        addSubview(addressLabel)
        addSubview(iconView)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let size = bounds.size
        
        let pulseX = ALCollectionViewCell.horizontalPadding
        let pulseY = size.height/2 - iconViewSize.height/2
        
        iconView.frame = CGRect(x: pulseX, y: pulseY, width: iconViewSize.width, height: iconViewSize.height)
        
        let pulseSize = pulsingLayer.frame.size
        let pulseLayerX = pulseX + (iconViewSize.width/2 - pulseSize.width/2)
        let pulseLayerY = pulseY + (iconViewSize.height/2 - pulseSize.height/2)
        
        pulsingLayer.frame.origin = CGPoint(x: pulseLayerX, y: pulseLayerY)
        
        let nameX = pulseX + iconViewSize.width + ALCollectionViewCell.horizontalSpacing
        let nameY = ALCollectionViewCell.verticalPadding
        let nameWidth = size.width - (ALCollectionViewCell.horizontalPadding * 3 + iconViewSize.width)
        let nameHeight = nameLabel.font.lineHeight
        
        nameLabel.frame = CGRect(x: nameX, y: nameY, width: nameWidth, height: nameHeight)
        
        let addressX = nameX
        let addressY = nameY + nameHeight + ALCollectionViewCell.verticalSpacing
        let addressWidth = nameWidth
        let addressHeight = addressLabel.font.lineHeight
        
        addressLabel.frame = CGRect(x: addressX, y: addressY, width: addressWidth, height: addressHeight)
    }
    
    func configureWithLocation(_ location: ALLocation) {
        nameLabel.text = "Current location"
        addressLabel.text = location.address
        setNeedsLayout()
    }

}
