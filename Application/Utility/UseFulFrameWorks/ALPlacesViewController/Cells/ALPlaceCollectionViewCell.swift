//
//  ALPlaceCollectionViewCell.swift
//  ALPlacesViewController
//
//  Created by Alex Littlejohn on 2015/07/13.
//  Copyright (c) 2015 zero. All rights reserved.
//

import UIKit

internal class ALPlaceCollectionViewCell: ALCollectionViewCell {
    
    let iconView = UIImageView()
    let iconViewSize = CGSize(width: 38, height: 38)
    var lineView = UIView()

    override func commonInit() {
        nameLabel.font = ALCollectionViewCell.nameFont
        addressLabel.font = ALCollectionViewCell.addressFont
        
        nameLabel.textColor = UIColor(white: 0.2, alpha: 1)
        addressLabel.textColor = UIColor(white: 0.4, alpha: 1)
        
        nameLabel.lineBreakMode = NSLineBreakMode.byTruncatingTail
        addressLabel.lineBreakMode = NSLineBreakMode.byTruncatingTail
        
        iconView.contentMode = UIViewContentMode.scaleAspectFit
        iconView.image = UIImage(named: "locationIcon2")
        
        backgroundColor = UIColor.white
        
        lineView.backgroundColor = APP_THEME_LIGHT_GRAY_COLOR_2
        
        addSubview(lineView)
        addSubview(nameLabel)
        addSubview(addressLabel)
        addSubview(iconView)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let size = bounds.size
        
        let iconX = ALCollectionViewCell.horizontalPadding
        let iconY = size.height/2 - iconViewSize.height/2
        
        iconView.frame = CGRect(x: iconX, y: iconY, width: iconViewSize.width, height: iconViewSize.height)
        
        let nameX = iconX + iconViewSize.width + ALCollectionViewCell.horizontalSpacing
        let nameY = ALCollectionViewCell.verticalPadding
        let nameWidth = size.width - (ALCollectionViewCell.horizontalPadding * 3 + iconViewSize.width)
        let nameHeight = nameLabel.font.lineHeight
        
        nameLabel.frame = CGRect(x: nameX, y: nameY, width: nameWidth, height: nameHeight)
        
        let addressX = nameX
        let addressY = nameY + nameHeight + ALCollectionViewCell.verticalSpacing
        let addressWidth = nameWidth
        let addressHeight = addressLabel.font.lineHeight
        
        addressLabel.frame = CGRect(x: addressX, y: addressY, width: addressWidth, height: addressHeight)
        
        let lineX = nameX
        let lineY = size.height-0.3
        let lineWidth = size.width-lineX
        let lineHeight = 0.3
        lineView.frame = CGRect(x: lineX, y: lineY, width: lineWidth, height: CGFloat(lineHeight))
    }
    
    func configureWithPlace(_ place: ALPlace) {
        nameLabel.text = place.name
        addressLabel.text = place.address
        setNeedsLayout()
    }
}
