//
//  ALPlacesDelegate.swift
//  ALPlacesViewController
//
//  Created by Alex Littlejohn on 2015/07/13.
//  Copyright (c) 2015 zero. All rights reserved.
//

import UIKit

internal typealias ScrollViewDidScrollCallback = (_ offset: CGPoint) -> Void

internal class ALPlacesDelegate: ALCollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    var places: [ALPlace]?
    var userLocation: ALLocation?
    
    var onScroll: ScrollViewDidScrollCallback?
    var onLocationPicked: ALPlacesPickerCallback?
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        onScroll?(scrollView.contentOffset)
    }
    
    func itemAtIndexPath(_ indexPath: IndexPath) -> ALLocation {
        
        var item = ALLocation()
        var offset = 0
        
        if userLocation != nil {
            offset = 1
        }
        
        if userLocation != nil && (indexPath as NSIndexPath).row == 0 {
            item = userLocation!
        } else if places != nil {
            item = places![(indexPath as NSIndexPath).row - offset]
        }
        
        return item
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let item = itemAtIndexPath(indexPath)
        
        if let place = item as? ALPlace {
            onLocationPicked?(place.name,"\(place.name ?? "") \(place.address ?? "")", place.coordinate, nil)
        } else {
            if item.address?.length > 0{
                onLocationPicked?(nil,item.address, item.coordinate, nil)
            }
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        var count = 0
        
        if userLocation != nil {
            count += 1
        }
        
        if places != nil {
            count += places!.count
        }
        
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let item = itemAtIndexPath(indexPath)
        
        if let place = item as? ALPlace {
            return ALPlaceCollectionViewCell.cellSize()
        } else {
            return ALUserCollectionViewCell.cellSize()
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var cell: UICollectionViewCell
        
        let item = itemAtIndexPath(indexPath)
        
        if let place = item as? ALPlace {
            let placeCell = collectionView.dequeueReusableCell(withReuseIdentifier: ALPlaceCollectionViewCellIdentifier, for: indexPath) as! ALPlaceCollectionViewCell
            
            placeCell.configureWithPlace(place)
            cell = placeCell
        } else {
            let locationCell = collectionView.dequeueReusableCell(withReuseIdentifier: ALUserCollectionViewCellIdentifier, for: indexPath) as! ALUserCollectionViewCell
            locationCell.configureWithLocation(item)
            cell = locationCell
        }
        
        return cell
    }
}
