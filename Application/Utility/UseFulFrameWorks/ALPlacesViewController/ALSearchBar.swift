//
//  ALSearchBar.swift
//  ALPlacesViewController
//
//  Created by Alex Littlejohn on 2015/07/14.
//  Copyright (c) 2015 zero. All rights reserved.
//

import UIKit

internal typealias SearchTextDidChangeCallback = (_ text: String) -> Void
internal typealias DoneButtonCallback = () -> Void

internal class ALSearchBar: UIView {

    let searchField = UITextField()
    let searchBackground = UIView()
    let doneButton = UIButton(type: .system)
    
    var onSearch: SearchTextDidChangeCallback?
    var onDoneButton: DoneButtonCallback?
    
    let searchPaddingH: CGFloat = 60
    let searchPaddingV: CGFloat = 15
    let searchTextHorizontalPadding: CGFloat = 14
    let searchTextVerticalPadding: CGFloat = 4
    let searchHeight: CGFloat = 44
    let statusHeight: CGFloat = 20
    var placeholderTextToShow = "Search here"
    let doneButtonSize = CGSize(width: 44, height: 44)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func commonInit() {
        NotificationCenter.default.addObserver(self, selector: #selector(ALSearchBar.textFieldDidChange(_:)), name: NSNotification.Name.UITextFieldTextDidChange, object: nil)
        
        searchBackground.backgroundColor = UIColor.white
        
        let attributes = [
            NSForegroundColorAttributeName: APP_THEME_LIGHT_GRAY_COLOR,
            NSFontAttributeName :UIFont.init(name: FONT_REGULAR, size: 16) // Note the !
        ]
        
        searchField.attributedPlaceholder = NSAttributedString(string: placeholderTextToShow, attributes:attributes)
        searchField.font = UIFont.init(name: FONT_REGULAR, size: 16)
        searchField.clearButtonMode = UITextFieldViewMode.whileEditing
        
        addSubview(searchBackground)
        addSubview(searchField)
        addSubview(doneButton)
        
        doneButton.setImage(UIImage(named: "backarrowblack"), for: UIControlState.normal)
        doneButton.tintColor = UIColor.black
        doneButton.addTarget(self, action: #selector(ALSearchBar.doneTapped), for: UIControlEvents.touchUpInside)
        doneButton.frame.size = doneButtonSize
        
        layoutSearchableState()
    }
    
    func layoutSearchableState() {
        
        let searchWidth = bounds.size.width - searchPaddingH - 8
        let searchY = searchPaddingV + statusHeight
        
        searchBackground.frame = CGRect(x: 5, y: searchY, width: bounds.size.width - 10, height: searchHeight)
        
        let textHeight = searchHeight - searchTextVerticalPadding * 2
        let textWidth = searchWidth - searchTextHorizontalPadding * 2 - doneButton.frame.size.width - searchTextHorizontalPadding/2
        let textX = searchPaddingH + searchTextHorizontalPadding
        let textY = statusHeight + searchPaddingV + searchTextVerticalPadding + 1
        
        searchField.frame = CGRect(x: textX, y: textY, width: textWidth, height: textHeight)
        searchField.alpha = 1
        
        let doneX : CGFloat = 25.0
        let doneY = textY + (textHeight/2 - doneButton.frame.size.height/2) - 1

        doneButton.frame.origin = CGPoint(x: doneX, y: doneY)
        doneButton.contentEdgeInsets = UIEdgeInsetsMake(0, -15, 0, 15)
        
        searchBackground.layer.borderColor = APP_THEME_LIGHT_GRAY_COLOR_2.cgColor
        searchBackground.layer.borderWidth = 1.0
        
        let attributes = [
            NSForegroundColorAttributeName: APP_THEME_LIGHT_GRAY_COLOR,
            NSFontAttributeName :UIFont.init(name: FONT_REGULAR, size: 16) // Note the !
        ]
        
        searchField.attributedPlaceholder = NSAttributedString(string: placeholderTextToShow, attributes:attributes)

        frame.size.height = textX + textHeight
    }
    
    func layoutScrollingState() {
        let searchWidth = bounds.size.width
        let searchY: CGFloat = 0
        let searchX: CGFloat = 0
        
        searchBackground.frame = CGRect(x: searchX, y: searchY, width: searchWidth, height: statusHeight)
        
        let textHeight = searchHeight - searchTextVerticalPadding * 2
        let textWidth = searchWidth - searchTextHorizontalPadding * 2
        let textX = searchTextHorizontalPadding
        let textY = searchTextVerticalPadding + 1
        
        searchField.frame = CGRect(x: textX, y: textY, width: textWidth, height: textHeight)
        searchField.alpha = 0
        searchField.resignFirstResponder()
        
        let attributes = [
            NSForegroundColorAttributeName: APP_THEME_LIGHT_GRAY_COLOR,
            NSFontAttributeName :UIFont.init(name: FONT_REGULAR, size: 16) // Note the !
        ]
        
        searchField.attributedPlaceholder = NSAttributedString(string: placeholderTextToShow, attributes:attributes)

        frame.size.height = textX + textHeight
    }
    
    internal func doneTapped() {
        onDoneButton?()
    }
    
    internal func textFieldDidChange(_ notification: Foundation.Notification) {
        if let textField = notification.object as? UITextField {
            if textField == searchField {
                onSearch?(textField.text!)
            }
        }
    }
    
}
