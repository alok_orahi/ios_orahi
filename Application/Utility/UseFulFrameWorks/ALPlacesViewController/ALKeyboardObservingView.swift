//
//  ALKeyboardObservingView.swift
//  ALPlacesViewController
//
//  Created by Alex Littlejohn on 2015/07/14.
//  Copyright (c) 2015 zero. All rights reserved.
//

import UIKit

internal let ALKeyboardFrameDidChangeNotification = "ALKeyboardFrameDidChangeNotification"

internal class ALKeyboardObservingView: UIView {

    fileprivate weak var observedView: UIView?
    
    // MARK: - Keyboard Observing -
    
    internal override func willMove(toSuperview newSuperview: UIView?) {
        
        removeKeyboardObserver()
        if let _newSuperview = newSuperview {
            addKeyboardObserver(_newSuperview)
        }
        
        super.willMove(toSuperview: newSuperview)
    }
    
    internal override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object as? NSObject == superview && keyPath == keyboardHandlingKeyPath() {
            keyboardDidChangeFrame(superview!.frame)
        } else {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        }
    }
    
    fileprivate func keyboardHandlingKeyPath() -> String {
        if UIDevice.floatVersion() >= 8.0 {
            return "center"
        } else {
            return "frame"
        }
    }
    
    fileprivate func addKeyboardObserver(_ newSuperview: UIView) {
        observedView = newSuperview
        newSuperview.addObserver(self, forKeyPath: keyboardHandlingKeyPath(), options: NSKeyValueObservingOptions.new, context: nil)
    }
    
    fileprivate func removeKeyboardObserver() {
        if observedView != nil {
            observedView!.removeObserver(self, forKeyPath: keyboardHandlingKeyPath())
            observedView = nil
        }
    }
    
    fileprivate func keyboardDidChangeFrame(_ frame: CGRect) {
        let userInfo = [UIKeyboardFrameEndUserInfoKey: NSValue(cgRect:frame)]
        NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: ALKeyboardFrameDidChangeNotification), object: nil, userInfo: userInfo)
    }
    
    deinit {
        removeKeyboardObserver()
    }

}
