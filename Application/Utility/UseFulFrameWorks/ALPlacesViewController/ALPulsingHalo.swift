//
//  ALPulsingHalo.swift
//  ALPlacesViewController
//
//  Created by Alex Littlejohn on 2015/07/16.
//  Copyright (c) 2015 zero. All rights reserved.
//

import UIKit

internal class ALPulsingHalo: CALayer ,CAAnimationDelegate {
   
    internal var radius: CGFloat = 60 {
        didSet {
            didSetRadius()
        }
    }
    internal var fromValueForRadius: CGFloat = 0
    internal var fromValueForAlpha: CGFloat = 0.45
    internal var animationDuration: CFTimeInterval = 3
    internal var keyTimeForHalfOpacity: CGFloat = 0.2
    
    fileprivate var animationGroup: CAAnimationGroup!
    
    override init() {
        super.init()
        commonInit()
    }

    override init(layer: Any) {
        super.init(layer: layer)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    fileprivate func commonInit() {
        backgroundColor = UIColor(red: 52.0/255.0, green: 183.0/255.0, blue: 250.0/255.0, alpha: 1).cgColor
        contentsScale = UIScreen.main.scale
        
        didSetRadius()

        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async {
            self.setupAnimation()
            DispatchQueue.main.async {
                self.add(self.animationGroup, forKey: "pulse")
            }
        }
    }
    
    func didSetRadius() {
        let temppos = position
        let diameter = radius * 2
        bounds = CGRect(x: 0, y: 0, width: diameter, height: diameter)
        cornerRadius = radius
        position = temppos
    }
    
    fileprivate func setupAnimation() {
        animationGroup = CAAnimationGroup()
        animationGroup.duration = animationDuration
        animationGroup.repeatCount = Float.infinity
        animationGroup.isRemovedOnCompletion = false
        animationGroup.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        
        let scaleAnimation = CABasicAnimation(keyPath: "transform.scale.xy")
        scaleAnimation.fromValue = fromValueForRadius
        scaleAnimation.toValue = 1
        scaleAnimation.duration = animationDuration
        
        let opacityAnimation = CAKeyframeAnimation(keyPath: "opacity")
        opacityAnimation.duration = animationDuration
        opacityAnimation.values = [fromValueForAlpha, CGFloat(0.45), CGFloat(0)]
        opacityAnimation.keyTimes = [NSNumber(value: 0.0), NSNumber(value: Float(keyTimeForHalfOpacity)), NSNumber(value: 1.0)]
        opacityAnimation.isRemovedOnCompletion = false
        
        animationGroup.animations = [scaleAnimation, opacityAnimation]
        animationGroup.delegate = self
    }
    
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        if flag {
            removeAllAnimations()
            removeFromSuperlayer()
        }
    }
}
