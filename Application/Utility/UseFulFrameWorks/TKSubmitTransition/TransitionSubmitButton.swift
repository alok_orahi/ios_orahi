import Foundation
import UIKit

let PINK = UIColor(red:0.992157, green: 0.215686, blue: 0.403922, alpha: 1)
let DARK_PINK = UIColor(red:0.798012, green: 0.171076, blue: 0.321758, alpha: 1)

@IBDesignable
public class TKTransitionSubmitButton : UIButton, UIViewControllerTransitioningDelegate , CAAnimationDelegate {
    
    public var didEndFinishAnimation : (()->())? = nil
    
    let springGoEase = CAMediaTimingFunction(controlPoints: 0.45, -0.36, 0.44, 0.92)
    let shrinkCurve = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
    let expandCurve = CAMediaTimingFunction(controlPoints: 0.95, 0.02, 1, 0.05)
    let shrinkDuration: CFTimeInterval  = 0.1
    
    lazy var spiner: SpinerLayerS! = {
        let s = SpinerLayerS(frame: self.frame)
        self.layer.addSublayer(s)
        return s
    }()
    
    @IBInspectable public var highlightedBackgroundColor: UIColor? = DARK_PINK {
        didSet {
            self.setBackgroundColor()
        }
    }
    @IBInspectable public var normalBackgroundColor: UIColor? = PINK {
        didSet {
            self.setBackgroundColor()
        }
    }
    
    var cachedTitle: String?
    var cachedCornerRadius: CGFloat?
    var cachedImageDefault: UIImage?
    var cachedImageSelected: UIImage?
    var cachedImageHighlighted: UIImage?
    var cachedBackgroundColor: UIColor?
    var cachedMaskToBoundsSupport : Bool?
    var requiredBackgroundColor: UIColor?
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    override public var isHighlighted: Bool {
        didSet {
            self.setBackgroundColor()
        }
    }
    
    public required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.setup()
    }
    
    func setup() {
        self.clipsToBounds = true
        self.setBackgroundColor()
    }
    
    func setBackgroundColor() {
        if (isHighlighted) {
            self.backgroundColor = highlightedBackgroundColor
        }else {
            self.backgroundColor = normalBackgroundColor
        }
    }
    
    public func startLoadingAnimation() {
        self.layoutIfNeeded()
        
        UIApplication.shared.beginIgnoringInteractionEvents()

        self.cachedCornerRadius = self.layer.cornerRadius
        self.layer.cornerRadius = self.frame.height / 2
        self.cachedTitle = title(for: .normal)
        self.cachedImageDefault = self.image(for: .normal)
        self.cachedImageSelected = self.image(for: .selected)
        self.cachedImageHighlighted = self.image(for: .highlighted)
        self.cachedBackgroundColor = self.normalBackgroundColor
        self.cachedMaskToBoundsSupport = self.layer.masksToBounds
        if isNotNull(self.requiredBackgroundColor){
            self.normalBackgroundColor = self.requiredBackgroundColor
        }
        
        self.setImage(nil, for: .normal)
        self.setImage(nil, for: .selected)
        self.setImage(nil, for: .highlighted)
        self.setTitle("", for: .normal)
        self.shrink()
        
        Timer.schedule(delay: shrinkDuration - 0.25) { timer in
            self.spiner.animation()
        }
    }
    
    public func startFinishAnimation(delay: TimeInterval, completion:(()->())?) {
        UIApplication.shared.endIgnoringInteractionEvents()
        Timer.schedule(delay: delay) { timer in
            self.didEndFinishAnimation = completion
            self.expand()
            self.spiner.stopAnimation()
        }
    }
    
    public func animate(duration: TimeInterval, completion:(()->())?) {
        startLoadingAnimation()
        startFinishAnimation(delay: duration, completion: completion)
    }
    
    public func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        let a = anim as! CABasicAnimation
        if a.keyPath == "transform.scale" {
            didEndFinishAnimation?()
            Timer.schedule(delay: 1) { timer in
                self.returnToOriginalState()
            }
        }
    }
    
    public func returnToOriginalState() {
        self.layer.removeAllAnimations()
        self.setTitle(self.cachedTitle, for: .normal)
        self.setImage(cachedImageDefault, for: .normal)
        self.setImage(cachedImageSelected, for: .selected)
        self.setImage(cachedImageHighlighted, for: .highlighted)
        self.normalBackgroundColor = self.cachedBackgroundColor
        self.layer.cornerRadius = self.cachedCornerRadius!
        self.layer.masksToBounds = self.cachedMaskToBoundsSupport!
    }
    
    public func stopIt() {
        UIApplication.shared.endIgnoringInteractionEvents()
        self.layer.removeAllAnimations()
        self.setTitle(self.cachedTitle, for: .normal)
        self.setImage(cachedImageDefault, for: .normal)
        self.setImage(cachedImageSelected, for: .selected)
        self.setImage(cachedImageHighlighted, for: .highlighted)
        self.normalBackgroundColor = self.cachedBackgroundColor
        self.layer.masksToBounds = self.cachedMaskToBoundsSupport!
        self.spiner.stopAnimation()
        self.layer.cornerRadius = self.cachedCornerRadius!
    }
    
    func shrink() {
        let shrinkAnim = CABasicAnimation(keyPath: "bounds.size.width")
        shrinkAnim.fromValue = frame.width
        shrinkAnim.toValue = frame.height
        shrinkAnim.duration = shrinkDuration
        shrinkAnim.timingFunction = shrinkCurve
        shrinkAnim.fillMode = kCAFillModeForwards
        shrinkAnim.isRemovedOnCompletion = false
        layer.add(shrinkAnim, forKey: shrinkAnim.keyPath)
    }
    
    func expand() {
        let expandAnim = CABasicAnimation(keyPath: "transform.scale")
        expandAnim.fromValue = 1.0
        expandAnim.toValue = 26.0
        expandAnim.timingFunction = expandCurve
        expandAnim.duration = 0.3
        expandAnim.delegate = self
        expandAnim.fillMode = kCAFillModeForwards
        expandAnim.isRemovedOnCompletion = false
        layer.add(expandAnim, forKey: expandAnim.keyPath)
    }
    
}
