//
//  DRCellSlideAction.m
//  DRCellSlideGestureRecognizer
//
//  Created by David Román Aguirre on 12/5/15.
//
//

#import "DRCellSlideAction.h"

@implementation DRCellSlideAction

- (instancetype)init {
    if (self = [super init]) {
        _fraction = 0.65;
        _activeBackgroundColor = [UIColor blueColor];
        _inactiveBackgroundColor = [UIColor colorWithWhite:0.94 alpha:1];
        _activeColor = _inactiveColor = [UIColor whiteColor];
        _iconMargin = 25;
    }
    
    return self;
}

- (void)setElasticity:(CGFloat)elasticity {
    _elasticity = fabs(elasticity)*[self fractionSign];
}

- (CGFloat)fractionSign {
    return self.fraction/fabs(self.fraction);
}

@end
