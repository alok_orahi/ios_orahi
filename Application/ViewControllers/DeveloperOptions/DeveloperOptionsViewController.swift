//
//  DeveloperOptionsViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit

class DeveloperOptionsViewController: UIViewController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource {
    
    //MARK: - variables and constants
    @IBOutlet fileprivate weak var tableView : UITableView?
    
    
    //MARK: - view controller life cycle methods
    
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
    }
    
    //MARK: - other methods
    private func setupForNavigationBar(){
        setAppearanceForNavigationBarType1(self.navigationController)
        setupNavigationBarTitleType1("Developer Options",viewController: self)
        if self.isBackButtonRequired() {
            addNavigationBarButton(self, image: UIImage(named: "backarrowblack"), title: nil, isLeft: true)
        }
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    func onClickOfLeftBarButton(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    private func registerForNotifications(){
        
    }
    
    private func startUpInitialisations(){
        registerNib("SingleLabelTableViewCell", tableView: tableView)
        setAppearanceForViewController(self)
        setAppearanceForTableView(tableView)
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        
    }
    
    //MARK: - UITableView Delegate & Data Source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 12
    }
    
    @objc func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SingleLabelTableViewCell") as? SingleLabelTableViewCell
        cell?.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        if (indexPath as NSIndexPath).row == 0{
            cell?.titleLabel?.text = "Change User"
        }else if (indexPath as NSIndexPath).row == 1{
            cell?.titleLabel?.text = "Change Base Url"
        }else if (indexPath as NSIndexPath).row == 2{
            cell?.titleLabel?.text = "Copy Device Token"
        }else if (indexPath as NSIndexPath).row == 3{
            cell?.titleLabel?.text = "Enable Push Notifications Debug"
        }else if (indexPath as NSIndexPath).row == 4{
            cell?.titleLabel?.text = "Show My UserId"
        }else if (indexPath as NSIndexPath).row == 5{
            cell?.titleLabel?.text = "Set User to Employee Type"
        }else if (indexPath as NSIndexPath).row == 6{
            cell?.titleLabel?.text = "Set User to Others Type"
        }else if (indexPath as NSIndexPath).row == 7{
            cell?.titleLabel?.text = "Set User to Student Type"
        }else if (indexPath as NSIndexPath).row == 8{
            if ENABLE_FEATURE_DEBUG_INFO {
                cell?.titleLabel?.text = "Disable DRN Debug Info"
            }else{
                cell?.titleLabel?.text = "Enable DRN Debug Info"
            }
        }else if (indexPath as NSIndexPath).row == 9{
            if ENABLE_FEATURE_DEBUG_INFO {
                cell?.titleLabel?.text = "Disable Deeplinking Debug Info"
            }else{
                cell?.titleLabel?.text = "Enable Deeplinking Debug Info"
            }
        }else if (indexPath as NSIndexPath).row == 10{
            cell?.titleLabel?.text = "Enable logging"
        }else if (indexPath as NSIndexPath).row == 11{
            cell?.titleLabel?.text = "Enable PoM Local notification logging"
        }
        return cell!
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as? SingleLabelTableViewCell
        performAnimatedClickEffectType1(cell!.titleLabel!)
        if (indexPath as NSIndexPath).row == 0{
            func loginWithUserId(_ userId:String){
                showActivityIndicator("Stopping Processes")
                if Acf().rideHomeVC != nil {
                    NSObject.cancelPreviousPerformRequests(withTarget: Acf().rideHomeVC!)
                    NSObject.cancelPreviousPerformRequests(withTarget: Acf().rideDetailVC!)
                }
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0.2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: { () -> Void in
                    showActivityIndicator("Logging out from QuickBlox")
                    Acf().logoutFromQuickBlox()
                    
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0.8 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: { () -> Void in
                        showActivityIndicator("Resetting the Database")
                        Dbm().resetDatabase()
                        
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0.4 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: { () -> Void in
                            showActivityIndicator("Setting New User")
                            let information = NSMutableDictionary()
                            copyData(userId , destinationDictionary: information, destinationKey: "userId", methodName: #function)
                            copyData("Temporary Name" , destinationDictionary: information, destinationKey: "name", methodName: #function)
                            copyData("temporaryEmail@temporaryDomain.com" , destinationDictionary: information, destinationKey: "email", methodName: #function)
                            copyData("9876543210" , destinationDictionary: information, destinationKey: "phone", methodName: #function)
                            Dbm().setUserInfo(information)
                            
                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0.3 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: { () -> Void in
                                showActivityIndicator("Updating user's Profile")

                                let userInfo = Dbm().getUserInfo()
                                if isNotNull(userInfo){
                                    Acf().getUserProfileDetails(loggedInUserId(), completion: { (returnedData) -> () in
                                        if isNotNull(returnedData){
                                            let profileInfo = (returnedData as! NSDictionary).object(forKey: "profileInformation") as! NSDictionary
                                            let settingsInfo = (returnedData as! NSDictionary).object(forKey: "informationForSettings") as! NSDictionary
                                            if isNotNull(profileInfo.object(forKey: "statusText")){
                                                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1.0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: { () -> Void in
                                                    Dbm().addUserStatus(["userId":loggedInUserId(),"status":profileInfo.object(forKey: "statusText")!])
                                                })
                                            }
                                            Dbm().setUserInfo(profileInfo)
                                            Dbm().setSetting(settingsInfo)
                                            Acf().loginToQuickBlox()
                                            NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_PROFILE_UPDATED), object: nil)
                                            
                                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0.4 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: { () -> Void in
                                                showActivityIndicator("Updating user's Settings")
                                                Acf().updateSettingsFromServerIfRequired()
                                                
                                                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(2.0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: { () -> Void in
                                                    showNotification("Done ! , Happy Debugging", showOnNavigation: true, showAsError: false)
                                                    Acf().showHomeScreen()
                                                    hideActivityIndicator()
                                                })
                                            })
                                        }else{
                                            showNotification("User Not Found , Please Correct UserId & Try Again.", showOnNavigation: true, showAsError: false)
                                            Dbm().resetDatabase()
                                            hideActivityIndicator()
                                        }
                                    })
                                }
                            })
                        })
                    })
                })
            }
            func promptForUserIdAndChange(){
                let prompt = UIAlertController(title: "Enter UserId to Debug", message: nil , preferredStyle: UIAlertControllerStyle.alert)
                prompt.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
                prompt.addAction(UIAlertAction(title: "Login", style: UIAlertActionStyle.cancel, handler: { (action) -> Void in
                    let enteredText = (prompt.textFields![0] as UITextField).text
                    if isNotNull(enteredText){
                        loginWithUserId(enteredText!)
                    }
                }))
                prompt.addTextField(configurationHandler: {(textField: UITextField!) in
                    textField.placeholder = "UserId , for eg: 34665"
                    textField.keyboardType = UIKeyboardType.numberPad
                    textField.textAlignment = .center
                })
                self.present(prompt, animated: true, completion: nil)
            }
            promptForUserIdAndChange()
        }else if (indexPath as NSIndexPath).row == 1 {
            let prompt = UIAlertController(title: "Enter Base Url", message: "\nWarning : changing base url will reset database and kill application." , preferredStyle: UIAlertControllerStyle.alert)
            prompt.addAction(UIAlertAction(title: "Set from Above Input", style: UIAlertActionStyle.default, handler: { (action) -> Void in
                let enteredText = (prompt.textFields![0] as UITextField).text
                if isNotNull(enteredText){
                    BASE_URL = enteredText!
                    showNotification("Base Url Updated to \(BASE_URL).", showOnNavigation: false, showAsError: false)
                    Dbm().resetDatabase()
                    UserDefaults.standard.set(BASE_URL, forKey: "baseUrlUpdatedByDeveloper")
                    terminateApplication()
                }
            }))
            prompt.addAction(UIAlertAction(title: "Set: https://api.orahi.com/mob/", style: UIAlertActionStyle.default, handler: { (action) -> Void in
                BASE_URL = "https://api.orahi.com/mob/"
                showNotification("Base Url Updated to \(BASE_URL).", showOnNavigation: false, showAsError: false)
                UserDefaults.standard.set(BASE_URL, forKey: "baseUrlUpdatedByDeveloper")
                terminateApplication()
            }))
            prompt.addAction(UIAlertAction(title: "Set: http://dev.orahi.com/beta/mob/", style: UIAlertActionStyle.default, handler: { (action) -> Void in
                BASE_URL = "http://dev.orahi.com/beta/mob/"
                showNotification("Base Url Updated to \(BASE_URL).", showOnNavigation: false, showAsError: false)
                UserDefaults.standard.set(BASE_URL, forKey: "baseUrlUpdatedByDeveloper")
                terminateApplication()
            }))
            prompt.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
            prompt.addTextField(configurationHandler: {(textField: UITextField!) in
                textField.placeholder = "Base Url , for eg: http://dev.orahi.com/beta/mob/"
                textField.text = "http://dev.orahi.com/beta/mob/"
                textField.keyboardType = UIKeyboardType.emailAddress
                textField.textAlignment = .center
            })
            self.present(prompt, animated: true, completion: nil)
        }else if (indexPath as NSIndexPath).row == 2 {
            if isNotNull(deviceToken()){
                UIPasteboard.general.string = deviceToken()
                showNotification("Device Token Copied : \(deviceToken())", showOnNavigation: false, showAsError: false)
            }else{
                showNotification("Not Registered with APNS yet", showOnNavigation: false, showAsError: true)
            }
        }else if (indexPath as NSIndexPath).row == 3 {
            Acf().showPushNotificationsOnPopup = true
            showNotification("Push Notification data will be shown via popup alert.", showOnNavigation: false, showAsError: false)
        }else if (indexPath as NSIndexPath).row == 4 {
            if isSystemReadyToProcessThis(){
                showNotification(loggedInUserId(), showOnNavigation: false, showAsError: false)
            }else{
                showNotification("user not logged in", showOnNavigation: false, showAsError: true)
            }
        }else if (indexPath as NSIndexPath).row == 5 {
            let settings = Dbm().getSetting()
            settings.userType = USER_TYPE_CORPORATE
            showNotification("changed to Coorporate Employee", showOnNavigation: false, showAsError: false)
        }else if (indexPath as NSIndexPath).row == 6 {
            let settings = Dbm().getSetting()
            settings.userType = USER_TYPE_OTHER
            showNotification("changed to other", showOnNavigation: false, showAsError: false)
        }else if (indexPath as NSIndexPath).row == 7 {
            let settings = Dbm().getSetting()
            settings.userType = USER_TYPE_STUDENT
            showNotification("changed to Student", showOnNavigation: false, showAsError: false)
        }else if (indexPath as NSIndexPath).row == 8 {
            ENABLE_FEATURE_DEBUG_INFO = !ENABLE_FEATURE_DEBUG_INFO
            tableView.reloadData()
        }else if (indexPath as NSIndexPath).row == 9 {
            ENABLE_FEATURE_DEBUG_INFO = !ENABLE_FEATURE_DEBUG_INFO
            tableView.reloadData()
        }else if (indexPath as NSIndexPath).row == 10 {
            ENABLE_LOGGING = true
            Acf().setUpLogger()
        }else if (indexPath as NSIndexPath).row == 11 {
            ENABLE_POM_LOCATION_SERVICE_DEBUG_INFO = true
        }
    }
    
    //MARK: - other functions
    
}

func terminateApplication() {
    exit(0)
}

func developerOptionsPerformAnyTaskBeforeReSettingDatabase(){
    if let baseUrlUpdatedByDeveloper = UserDefaults.standard.object(forKey: "baseUrlUpdatedByDeveloper") as? String{
        execMain({
            UserDefaults.standard.set(baseUrlUpdatedByDeveloper, forKey: "baseUrlUpdatedByDeveloper")
        })
    }
}

func developerOptionsLoadAppRequirementsIfAny(){
    if let baseUrlUpdatedByDeveloper = UserDefaults.standard.object(forKey: "baseUrlUpdatedByDeveloper") as? String{
        BASE_URL = baseUrlUpdatedByDeveloper
        showNotification("Base Url Updated to \(BASE_URL).", showOnNavigation: false, showAsError: false)
    }
}
