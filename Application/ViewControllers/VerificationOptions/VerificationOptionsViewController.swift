//
//  VerificationOptionsViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class VerificationOptionsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet fileprivate weak var tableView: UITableView!
    @IBOutlet fileprivate weak var currentVerificationLevelImageView: UIImageView!
    
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    //MARK: - view controller life cycle methods
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
        updateUserInterfaceOnScreen()
    }
    
    //MARK: - other methods
    private func setupForNavigationBar(){
        setAppearanceForNavigationBarType1(self.navigationController)
        setupNavigationBarTitleType1("Verification",viewController: self)
        if self.isBackButtonRequired() {
            addNavigationBarButton(self, image: UIImage(named: "backarrowblack"), title: nil, isLeft: true)
        }
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    private func registerForNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(VerificationOptionsViewController.updateUserInterfaceOnScreen), name: NSNotification.Name(rawValue: NOTIFICATION_PROFILE_UPDATED), object: nil)
    }
    
    private func startUpInitialisations(){
        registerNib("SingleLabelAndSwitchTableViewCell", tableView: tableView)
        setAppearanceForViewController(self)
        setAppearanceForTableView(tableView)
        trackScreenLaunchEvent(SLE_VERIFICATION_OPTION)
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        let settings = Dbm().getSetting()
        var counter = 0
        if (settings.verifiedUserDesignation! as NSString).isEqual(to: "1") {
            counter += 1
        }
        if (settings.verifiedMobileNumber! as NSString).isEqual(to: "1") {
            counter += 1
        }
        if (settings.verifiedGovernmentIdProofDocument! as NSString).isEqual(to: "1") {
            counter += 1
        }
        currentVerificationLevelImageView.image = UIImage(named:"verificationLevel\(counter)")
        tableView.reloadData()
    }
    
    //MARK: - other functions
    
    func onClickOfLeftBarButton(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - UITableView Delegate & Data Source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 66
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SingleLabelAndSwitchTableViewCell") as? SingleLabelAndSwitchTableViewCell
        var title = ""
        var description = ""
        
        if (indexPath as NSIndexPath).row == 0 {
            if userType().isEqual( USER_TYPE_CORPORATE){
                title = "Verified Corporate Employee"
            }else if userType().isEqual( USER_TYPE_STUDENT){
                title = "Verified College Student"
            }
            description = "Tap to Update/Re-Verify"
        }else if (indexPath as NSIndexPath).row == 1{
            title = "Verified Mobile Number"
            description = "Tap to Verify"
        }else if (indexPath as NSIndexPath).row == 2{
            title = "Verified Government ID Proof Document"
            description = "Tap to Update/Re-Verify"
        }
        
        let attributesTitle = [NSFontAttributeName:UIFont.init(name: FONT_BOLD, size: 13)!,
                               NSForegroundColorAttributeName:UIColor.darkGray] as NSDictionary
        let attributesDescription = [NSFontAttributeName:UIFont.init(name: FONT_REGULAR, size: 11)!,
                                     NSForegroundColorAttributeName:UIColor.gray] as NSDictionary
        
        let attributedString = NSMutableAttributedString(string:title+"\n"+description)
        attributedString.setAttributes(attributesTitle as? [String : Any], range: NSMakeRange(0, title.length))
        attributedString.setAttributes(attributesDescription as? [String : Any], range: NSMakeRange(title.length, attributedString.length-title.length))
        cell?.titleLabel?.attributedText = attributedString
        
        let setting = Dbm().getSetting()
        cell?.seperatorView?.isHidden = false
        if (indexPath as NSIndexPath).row == 0{
            cell?.controlSwitch?.isOn = (setting.verifiedUserDesignation! as NSString).isEqual(to: "1")
        }else if (indexPath as NSIndexPath).row == 1{
            cell?.controlSwitch?.isOn = (setting.verifiedMobileNumber! as NSString).isEqual(to: "1")
        }else if (indexPath as NSIndexPath).row == 2{
            cell?.controlSwitch?.isOn = (setting.verifiedGovernmentIdProofDocument! as NSString).isEqual(to: "1")
            cell?.seperatorView?.isHidden = true
        }
        cell?.controlSwitch?.isUserInteractionEnabled = false
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(isInternetConnectivityAvailable(true)==false){return}
        if (indexPath as NSIndexPath).row == 0 {
                let prompt = UIAlertController(title: "Email Id", message: MESSAGE_TEXT___EMAIL_VERIFICATION_INFO, preferredStyle: UIAlertControllerStyle.alert)
                prompt.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
                prompt.addAction(UIAlertAction(title: "Verify", style: UIAlertActionStyle.cancel, handler: { (action) -> Void in
                    var identifier = ""
                    if userType() as String == USER_TYPE_CORPORATE{
                        identifier = "Coorporate Email Id"
                    }
                    else if userType() as String == USER_TYPE_STUDENT{
                        identifier = "College Email Id"
                    }
                    let enteredText = (prompt.textFields![0] as UITextField).text
                    if validateSpecificEmail(enteredText, identifier: identifier as NSString?){
                        let information = ["email":enteredText!];
                        let scm = ServerCommunicationManager()
                        scm.returnFailureResponseAlso = true
                        scm.responseErrorOption = ResponseErrorOption.dontShowErrorResponseMessage
                        scm.verifyOrganisationalEmail(information as NSDictionary) { (responseData) -> () in
                            if let _ = responseData {
                                showNotification(MESSAGE_TEXT___EMAIL_VERIFICATION_INFO, showOnNavigation: true, showAsError: false)
                            }
                        }
                    }
                }))
                prompt.addTextField(configurationHandler: {(textField: UITextField!) in
                    textField.placeholder = "Email"
                    textField.keyboardType = UIKeyboardType.emailAddress
                })
                present(prompt, animated: true, completion: nil)
        }else if (indexPath as NSIndexPath).row == 1 {
            // This is a read only option, as we already verify phone number at signup process.
            // There is no use case where user still needs to verify his phone number.
        }else if (indexPath as NSIndexPath).row == 2{
            let pickerController = DKImagePickerController()
            pickerController.allowMultipleTypes=false
            pickerController.singleSelect=true
            pickerController.assetType = .allPhotos
            pickerController.didSelectAssets = { [unowned self] (assets: [DKAsset]) in
                for asset in assets{
                    asset.fetchFullScreenImageWithCompleteBlock({ (image, info) -> Void in
                        execMain({[weak self] in guard let `self` = self else { return }

                            let imageToUpload = fixrotation(image!)
                            let picname = "\(Dbm().getUserInfo()!.userId!)_\(currentTimeStamp()).jpg"
                            let information : NSDictionary = ["image":imageToUpload,"imageName":picname]
                            let scm = ServerCommunicationManager()
                            scm.progressIndicatorText = "uploading"
                            scm.returnFailureResponseAlso = true
                            scm.responseErrorOption = ResponseErrorOption.dontShowErrorResponseMessage
                            scm.returnFailureUnParsedDataIfParsingFails = true
                            scm.uploadGovernmentIdProof(information) { (responseData) -> () in
                                if let _ = responseData {
                                    let information : NSDictionary = ["imageName":picname]
                                    let scm = ServerCommunicationManager()
                                    scm.progressIndicatorText = "saving"
                                    scm.returnFailureResponseAlso = true
                                    scm.responseErrorOption = ResponseErrorOption.dontShowErrorResponseMessage
                                    scm.saveGovernmentIdProof(information) { (responseData) -> () in
                                        if let _ = responseData {
                                            if let message = responseData!.object(forKey: "message") as? String {
                                                showNotification(message, showOnNavigation: true, showAsError: false)
                                            }
                                        }
                                    }
                                }
                            }
                        })
                    })
                }
            }
            Acf().navigationController!.present(pickerController, animated: true) {}
        }
    }
}

