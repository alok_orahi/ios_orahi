//
//  SOSViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit

class SOSViewController: UIViewController {
    
    //MARK: - variables and constants
    @IBOutlet weak var cancelButton:UIButton!
    @IBOutlet weak var counterLabel1:UILabel!
    @IBOutlet weak var counterLabel2:UILabel!
    @IBOutlet weak var holderView:UIView!
    @IBOutlet fileprivate weak var sosButtonImageView : UIImageView!

    fileprivate var counter = 10
    
    //MARK: - view controller life cycle methods
    override internal var prefersStatusBarHidden : Bool {
        return true
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        sosButtonImageView.stopAnimation()
    }
    
    //MARK: - other methods
    private func setupForNavigationBar(){
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        sosButtonImageView.pulse(toSize: 0.85, duration: 0.25, repeat: true)
    }
    
    private func registerForNotifications(){
    }
    
    private func startUpInitialisations(){
        setAppearanceForViewController(self)
        cancelButton.layer.borderColor = UIColor.white.cgColor
        cancelButton.layer.borderWidth = 0.5
        startTimer()
    }
    
    func startTimer(){
        counter = counter - 1
        if counter >= 0 {
            self.counterLabel1.text = "\(counter)"
            self.counterLabel2.text = "\(counter)"
            self.holderView.performAppearAnimationType6()
            vibrate()
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(SOSViewController.startTimer), object: nil)
            self.perform(#selector(SOSViewController.startTimer), with: nil, afterDelay: 1)
        }else{
            sendSOSAlert()
        }
    }
    
    @IBAction fileprivate func onClickOfCancelButton(){
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(SOSViewController.startTimer), object: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    func sendSOSAlert(){
        cancelButton.isEnabled = false
        cancelButton.setTitle("Sending...", for: UIControlState.normal)
        let information = NSMutableDictionary()
        let scm = ServerCommunicationManager()
        scm.showSuccessResponseMessage = true
        scm.responseErrorOption = ResponseErrorOption.dontShowErrorResponseMessage
        scm.sos(information) { (responseData) -> () in
            if let _ = responseData {
                self.cancelButton.setTitle("ALERT SENT", for: UIControlState.normal)
                execMain({
                    self.navigationController?.popViewController(animated: true)
                },delay: 2.0)
            }else{
                self.cancelButton.setTitle("CANCEL", for: UIControlState.normal)
                self.cancelButton.isEnabled = true
            }
        }
    }
}
