//
//  OffersViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit
import Branch
import DZNEmptyDataSet

class OffersViewController: UIViewController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,DZNEmptyDataSetDelegate,DZNEmptyDataSetSource {
    
    //MARK: - variables and constants
    
    @IBOutlet fileprivate weak var topInformationHolderView: UIView!
    @IBOutlet fileprivate weak var topInformationHolderViewTopSpaceToSuperViewConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate weak var informationAboutRefferalsLabel: UILabel!
    @IBOutlet fileprivate weak var tableView: UITableView!
    
    fileprivate var offers = NSArray()
    fileprivate var headerImage: UIImage?
    fileprivate var forceUpdate = true
    
    //MARK: - view controller life cycle methods
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
        Acf().checkAndUpdateBranchShareUrlIfRequired()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupForNavigationBar()
        updateUserInterfaceOnScreen()
        Acf().checkAndUpdateBranchShareUrlIfRequired()
    }
    
    //MARK: - other methods
    private func setupForNavigationBar(){
        setAppearanceForNavigationBarType1(self.navigationController)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    private func registerForNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(OffersViewController.fetchOffersForceFully), name: NSNotification.Name(rawValue: NOTIFICATION_RELOAD_OFFERS), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(OffersViewController.claimOffer(info:)), name: NSNotification.Name(rawValue: NOTIFICATION_CLAIM_OFFER), object: nil)
    }
    
    private func startUpInitialisations(){
        registerNib("OfferType1TableViewCell", tableView: self.tableView)
        registerNib("OfferType2TableViewCell", tableView: self.tableView)
        registerNib("OfferType3TableViewCell", tableView: self.tableView)
        registerNib("OfferSeparatorTableViewCell", tableView: self.tableView)
        
        setAppearanceForViewController(self)
        setAppearanceForTableView(self.tableView)
    
        updateTableViewHeaderIfRequired()
        trackScreenLaunchEvent(SLE_OFFERS)
    }
    
    func fetchOffersForceFully() {
        self.forceUpdate = true
        fetchAndUpdateOffers()
    }
    
    func updateTableViewHeaderIfRequired() {
        if isNull(self.tableView.parallaxView){
            SDWebImageManager.shared().downloadImage(with: OFFER_SCREEN_TOP_BANNER_IMAGE_URL.asNSURL() as URL!, options: [.retryFailed], progress: { (x, y) in
            }) { (image, error, cacheType, completed,url) in
                if isNotNull(image){
                    self.headerImage = image
                    if isNotNull(self.headerImage){
                        self.tableView.addParallax(with: self.headerImage!, andHeight: self.headerImage!.aspectHeightForWidth(DEVICE_WIDTH), andShadow: false)
                    }
                }
            }
        }
        SDWebImageManager.shared().downloadImage(with: OFFER_SCREEN_TOP_BANNER_IMAGE_URL.asNSURL() as URL!, options: [.retryFailed,.refreshCached], progress: {[weak self] (x, y) in guard let `self` = self else { return }}){ (image, error, cacheType, completed,url) in
        }
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        informationAboutRefferalsLabel.text = "Invite friends to Orahi and get exciting offers."
        fetchAndUpdateOffers()
    }
    
    //MARK: - other functions
    
    @IBAction func onClickOfSideMenuButton(_ sender:Any){
        Acf().sideMenuController?.presentLeftMenuViewController()
    }
    
    @IBAction func onClickOfShareButton(_ sender:Any){
        showCarpoolShareOptions()
    }
    
    
    //MARK: - DZNEmptyDataSet Delegate & Data Source
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString!{
        let attributes = [NSFontAttributeName : UIFont.init(name: FONT_BOLD, size: 20)!]
        var message = ""
        if (isInternetConnectivityAvailable(false)==false){
            message = ""
        }
        return NSAttributedString(string: message, attributes: attributes)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString!{
        let attributes = [NSFontAttributeName : UIFont.init(name: FONT_REGULAR, size: 14)!]
        var message = "No offers available at the moment"
        if (isInternetConnectivityAvailable(false)==false){
            message = MESSAGE_TEXT___FOR_NETWORK_NOT_REACHABILITY
        }
        return NSAttributedString(string: message, attributes: attributes)
    }
    
    //MARK: - UITableView Delegate & Data Source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if offers.count > 0 {
            return offers.count + 1
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        if (indexPath as NSIndexPath).row == 0 {
            return 64
        }
        if let offerType = (offers.object(at: (indexPath as NSIndexPath).row-1) as! NSDictionary).object(forKey: "offer_design_type") as? NSString{
            if offerType.isEqual(to: "template"){
                let offer = (offers.object(at: (indexPath as NSIndexPath).row-1) as! NSMutableDictionary)
                return OfferType1TableViewCell.getRequiredHeight(offer)
            }
            if offerType.isEqual(to: "combined"){
                let offer = (offers.object(at: (indexPath as NSIndexPath).row-1) as! NSMutableDictionary)
                return OfferType2TableViewCell.getRequiredHeight(offer)
            }
            if offerType.isEqual(to: "image"){
                let offer = (offers.object(at: (indexPath as NSIndexPath).row-1) as! NSMutableDictionary)
                return OfferType3TableViewCell.getRequiredHeight(offer)
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath as NSIndexPath).row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "OfferSeparatorTableViewCell") as? OfferSeparatorTableViewCell
            return cell!
        }
        let offer = offers.object(at:indexPath.row-1) as! NSDictionary
        if let offerType = offer.object(forKey: "offer_design_type") as? NSString{
            if offerType.isEqual(to: "template"){
                let cell = tableView.dequeueReusableCell(withIdentifier: "OfferType1TableViewCell") as? OfferType1TableViewCell
                cell?.offer = offer
                return cell!
            }
            if offerType.isEqual(to: "combined"){
                let cell = tableView.dequeueReusableCell(withIdentifier: "OfferType2TableViewCell") as? OfferType2TableViewCell
                cell?.offer = offer
                return cell!
            }
            if offerType.isEqual(to: "image"){
                let cell = tableView.dequeueReusableCell(withIdentifier: "OfferType3TableViewCell") as? OfferType3TableViewCell
                cell?.offer = offer
                return cell!
            }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row > 0 {
            let offer = offers.object(at:indexPath.row-1) as! NSDictionary
            if let url = offer.object(forKey: "offer_url") as? String{
                Acf().showWebViewController(.customUrl, customUrl: url, customTitle: "Offer")
            }
        }
    }
    
    
    func claimOffer(info:NSNotification){
        var offerToClaim : NSDictionary?
        var indexOfOffer : Int = 0
        if let offerId = info.userInfo?["offerId"] as? String {
            if offerId.length > 0 {
                for (index,o) in offers.enumerated() {
                    if let offerIdLoaded = (o as! NSDictionary).object(forKey: "offer_id") as? String {
                        if offerIdLoaded == offerId {
                            offerToClaim = o as! NSDictionary
                            indexOfOffer = index + 1
                            break
                        }
                    }
                }
            }
        }
        if offerToClaim != nil {
            let indexPath = IndexPath(row: indexOfOffer, section: 0)
            guard let cell = self.tableView.cellForRow(at: indexPath) else { return }
            tableView.scrollToRow(at: indexPath , at: UITableViewScrollPosition.bottom, animated: true)
            execMain({[weak self] in guard let `self` = self else { return }

                if cell.responds(to: Selector("onClickOfDoneButton")){
                    cell.performSelector(onMainThread: Selector("onClickOfDoneButton"), with: nil, waitUntilDone: false)
                }
            })
        }
    }
    
    func fetchAndUpdateOffers(){
        tableView?.reloadData()
        tableView?.reloadEmptyDataSet()
        if isSystemReadyToProcessThis() {
            var shouldReloadTableView = false
            if let cachedOffers = (CacheManager.sharedInstance.loadObject("fetchAndUpdateOffers") as? NSArray){
                shouldReloadTableView = self.offers.count == 0
                self.offers = cachedOffers
            }
            if isInternetConnectivityAvailable(false){
                var shouldPerformNewRequest = true
                if let lastCachedDateOffers = CacheManager.sharedInstance.loadObject("lastCachedDateOffers") as? Date {
                    if lastCachedDateOffers.isLaterThanDate(Date().dateBySubtractingMinutes(1)){
                        //cache is almost latest
                        shouldPerformNewRequest = false
                    }
                }
                if forceUpdate {
                    shouldPerformNewRequest = true
                }
                if shouldPerformNewRequest {
                    let scm = ServerCommunicationManager()
                    scm.responseErrorOption = ResponseErrorOption.dontShowErrorResponseMessage
                    scm.returnFailureResponseAlso = true
                    scm.getOffers(NSDictionary()) { (responseData) -> () in
                        if isNotNull(responseData) && isNotNull(responseData?.object(forKey: "data")) {
                            if let offersArray = responseData!.object(forKey: "data") as? NSArray{
                                self.forceUpdate = false
                                self.offers = offersArray
                                CacheManager.sharedInstance.saveObject(self.offers, identifier: "fetchAndUpdateOffers")
                                CacheManager.sharedInstance.saveObject(Date() , identifier: "lastCachedDateOffers")
                                self.tableView?.reloadData()
                            }
                        }
                    }
                }
            }
            if shouldReloadTableView{
                self.tableView?.reloadData()
            }
        }
        
        execMain ({ (completed) in
            if self.offers.count > 0 {
                trackFunctionalEvent(FE_SCREEN_DATA_LOADED, information: ["screen":"offers_screen"])
            }else{
                trackFunctionalEvent(FE_SCREEN_DATA_NOT_LOADED, information: ["screen":"offers_screen"])
            }
        })

    }
}
