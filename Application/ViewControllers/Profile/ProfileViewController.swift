//
//  ProfileViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit
import DZNEmptyDataSet
import RSKImageCropper

fileprivate func <= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l <= r
    default:
        return !(rhs < lhs)
    }
}

class ProfileViewController: UIViewController,UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,DZNEmptyDataSetSource,DZNEmptyDataSetDelegate,RSKImageCropViewControllerDelegate {
    
    
    //MARK: - variables and constants
    @IBOutlet fileprivate weak var tableView : UITableView?
    @IBOutlet fileprivate weak var tableViewHeaderView : UIView?
    
    //section 1
    @IBOutlet fileprivate weak var section1ContainerView : UIView!
    @IBOutlet fileprivate weak var profilePictureImageView : UIImageView!
    @IBOutlet fileprivate weak var profilePictureBackgroundView : UIView!
    @IBOutlet fileprivate weak var fullNameLabel : UILabel!
    @IBOutlet fileprivate weak var statusTextLabel : UILabel!
    @IBOutlet fileprivate weak var ratingView : HCSStarRatingView!
    @IBOutlet fileprivate weak var userTypeLabel : UILabel!
    
    //section 2
    @IBOutlet fileprivate weak var section2ContainerView : UIView!
    @IBOutlet fileprivate weak var homeLocationLabel : UILabel!
    @IBOutlet fileprivate weak var homeLocationPickupPointLabel : UILabel!
    @IBOutlet fileprivate weak var destinationTypeLabel : UILabel!
    @IBOutlet fileprivate weak var destinationLocationLabel : UILabel!
    @IBOutlet fileprivate weak var destinationLocationPickupPointLabel : UILabel!
    
    //section 3
    @IBOutlet fileprivate weak var section3ContainerView : UIView!
    @IBOutlet fileprivate weak var trustShieldFactor1ImageView : UIImageView!
    @IBOutlet fileprivate weak var trustShieldFactor2ImageView : UIImageView!
    @IBOutlet fileprivate weak var trustShieldFactor3ImageView : UIImageView!
    
    //section 4
    @IBOutlet fileprivate weak var section4ContainerView : UIView!
    @IBOutlet fileprivate weak var rankLabel : UILabel!
    @IBOutlet fileprivate weak var ridesDoneLabel : UILabel!
    @IBOutlet fileprivate weak var kmsSharedLabel : UILabel!
    @IBOutlet fileprivate weak var communityRankingPositionLabel : UILabel!
    @IBOutlet fileprivate weak var communityRankingPositionImageView : UIImageView!
    
    //section 5
    @IBOutlet fileprivate weak var section5ContainerView : UIView!
    @IBOutlet fileprivate weak var favouritesCollectionView : UICollectionView!
    fileprivate var favouriteUsers = NSMutableArray()
    fileprivate var paginationController : Paginator?
    
    //section 6
    @IBOutlet fileprivate weak var section6ContainerView : UIView!
    
    //MARK: - view controller life cycle methods
    
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        updateUserInterfaceOnScreen()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupTableView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupForNavigationBar()
        updateUserInterfaceOnScreen()
        registerForNotifications()
    }
    
    
    //MARK: - other methods
    private func setupForNavigationBar(){
        setAppearanceForNavigationBarType1(self.navigationController)
        setupNavigationBarTitleType1("Profile",viewController: self)
        if self.isBackButtonRequired() {
            addNavigationBarButton(self, image: UIImage(named: "backarrowblack"), title: nil, isLeft: true)
        }
        if userType() != USER_TYPE_OTHER {
            addNavigationBarButton(self, image: nil , title: "Edit" , isLeft: false)
        }
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    func onClickOfLeftBarButton(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    func onClickOfRightBarButton(_ sender:Any){
        Acf().pushVC("EditProfileOtherOptionsViewController", navigationController: self.navigationController, isRootViewController: false, animated: true, modifyObject: nil)
    }
    
    private func registerForNotifications(){
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(self, selector: #selector(ProfileViewController.updateUserInterfaceOnScreen), name: NSNotification.Name(rawValue: NOTIFICATION_UPDATE_PROFILE_SCREEN), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ProfileViewController.updateUserInterfaceOnScreen), name: NSNotification.Name(rawValue: NOTIFICATION_ADDRESS_AND_TIMING_UPDATED), object: nil)
    }
    
    private func startUpInitialisations(){
        weak var weakSelf = self
        setAppearanceForViewController(weakSelf!)
        setAppearanceForTableView(tableView)
        setBorder(profilePictureBackgroundView, color: APP_THEME_VOILET_COLOR, width: 3, cornerRadius: profilePictureBackgroundView.bounds.size.width/2)
        setBorder(profilePictureImageView, color: UIColor.clear, width: 0, cornerRadius: profilePictureImageView.bounds.size.width/2)
        fullNameLabel.font = UIFont.init(name: FONT_BOLD, size: 17)
        statusTextLabel.font = UIFont.init(name: FONT_REGULAR, size: 14)
        ratingView.isUserInteractionEnabled = false
        Acf().getFavouritesUsers { (returnedData) -> () in
            if isNull(weakSelf){return;}
            weakSelf!.favouriteUsers = returnedData as! NSMutableArray
            weakSelf!.favouritesCollectionView?.reloadData()
        }
        trackScreenLaunchEvent(SLE_SELF_PROFILE)
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        
        func setGeneralInfo(){
            weak var weakSelf = self
            let userInfo = Dbm().getUserInfo()
            //section 1
            if isNotNull(userInfo?.picture){
                profilePictureImageView.sd_setImage(with: URL(string: getUserProfilePictureUrlFromFileName(userInfo!.picture!)), placeholderImage: USER_PLACEHOLDER_IMAGE)
            }else{
                profilePictureImageView.image = USER_PLACEHOLDER_IMAGE
            }
            fullNameLabel.text = userInfo?.name
            statusTextLabel.text = getStatusText()
            if statusTextLabel.text?.length == 0{
                statusTextLabel.text = "Tap edit to add Status"
            }
        }
        
        
        func setCarpoolInfo(){
            weak var weakSelf = self
            let userInfo = Dbm().getUserInfo()
            ratingView.value = CGFloat(userInfo!.rating!.doubleValue)
            
            //section 2
            let settings = Dbm().getSetting()
            homeLocationLabel.text = settings.homeAddress
            homeLocationPickupPointLabel.text = settings.homeAddressPickUpPoint
            destinationLocationLabel.text = settings.destinationAddress
            destinationLocationPickupPointLabel.text = settings.destinationAddressPickUpPoint
            destinationTypeLabel.text = destinationCapitalizedName()
            
            //section 3
            trustShieldFactor1ImageView.isHighlighted = safeNSString(settings.verifiedMobileNumber, alternate: "0").isEqual(to: "1")
            trustShieldFactor2ImageView.isHighlighted = safeNSString(settings.verifiedUserDesignation, alternate: "0").isEqual(to: "1")
            trustShieldFactor3ImageView.isHighlighted = safeNSString(settings.verifiedGovernmentIdProofDocument, alternate: "0").isEqual(to: "1")
            
            //section 4
            func setRidesDone(){
                let attributesNumbers = [NSFontAttributeName:UIFont.init(name: FONT_BOLD, size: 13)!,
                                         NSForegroundColorAttributeName:UIColor.darkGray] as NSDictionary
                let attributesInfo = [NSFontAttributeName:UIFont.init(name: FONT_REGULAR, size: 9)!,
                                      NSForegroundColorAttributeName:UIColor.gray] as NSDictionary
                let attributedNumberValue = safeString(userInfo?.ridesDone, alternate: "0")
                let attributedInfoValue = " Rides Done"
                
                let attributedString = NSMutableAttributedString(string:attributedNumberValue+attributedInfoValue)
                attributedString.setAttributes(attributesNumbers as? [String : Any], range: NSMakeRange(0, attributedNumberValue.length))
                attributedString.setAttributes(attributesInfo as? [String : Any], range: NSMakeRange(attributedNumberValue.length, attributedString.length-attributedNumberValue.length))
                ridesDoneLabel.attributedText = attributedString
            }
            
            func setKmsShared(){
                let attributesNumbers = [NSFontAttributeName:UIFont.init(name: FONT_BOLD, size: 13)!,
                                         NSForegroundColorAttributeName:UIColor.darkGray] as NSDictionary
                let attributesInfo = [NSFontAttributeName:UIFont.init(name: FONT_REGULAR, size: 9)!,
                                      NSForegroundColorAttributeName:UIColor.gray] as NSDictionary
                
                let attributedNumberValue = safeString(userInfo?.kmsShared, alternate: "0")
                let attributedInfoValue = " Kms Shared"
                
                let attributedString = NSMutableAttributedString(string:attributedNumberValue+attributedInfoValue)
                attributedString.setAttributes(attributesNumbers as? [String : Any], range: NSMakeRange(0, attributedNumberValue.length))
                attributedString.setAttributes(attributesInfo as? [String : Any], range: NSMakeRange(attributedNumberValue.length, attributedString.length-attributedNumberValue.length))
                kmsSharedLabel.attributedText = attributedString
            }
            
            setRidesDone()
            setKmsShared()
            rankLabel.text = safeString(userInfo?.rank, alternate: "0")
            setRankImage(weakSelf!.rankLabel.text!, imageView: weakSelf!.communityRankingPositionImageView)
            let trueForOwnerFalseforPassenger = safeNSString(userInfo?.isPassenger, alternate: "0").isEqual(to: "0")
            if trueForOwnerFalseforPassenger{
                userTypeLabel.text = "Car Owner"
            }else{
                userTypeLabel.text = "Passenger"
            }
        }
        
        setGeneralInfo()
        if userType() == USER_TYPE_OTHER {
            section2ContainerView.isHidden = true
            section3ContainerView.isHidden = true
            section4ContainerView.isHidden = true
            section5ContainerView.isHidden = true
            section6ContainerView.isHidden = true
            self.tableView?.isScrollEnabled = false
            self.ratingView.isHidden = true
        }else{
            section2ContainerView.isHidden = false
            section3ContainerView.isHidden = false
            section4ContainerView.isHidden = false
            section5ContainerView.isHidden = false
            section6ContainerView.isHidden = false
            self.tableView?.isScrollEnabled = true
            self.ratingView.isHidden = false
            setCarpoolInfo()
        }

    }
    
    //MARK: - other functions
    
    func setupTableView(){
        weak var weakSelf = self
        if isNull(tableView?.tableHeaderView){
            registerNib("LoadingTableViewCell", tableView: tableView)
            registerNib("UserFeedbacksTableViewCell", tableView: tableView)
            tableView?.tableHeaderView = weakSelf!.tableViewHeaderView
            setAppearanceForTableView(tableView)
            setupForPaginationController()
            fetchFirstPage()
        }
    }
    
    func setupForPaginationController(){
        weak var weakSelf = self
        paginationController = Paginator()
        paginationController?.setup(PAGE_SIZE)
        paginationController?.paginationFor = PaginatorMode.feedbacks
        paginationController?.userInfo = ["userId":loggedInUserId()]
        paginationController?.completionBlock =
            { (paginator) -> () in
                if isNull(weakSelf){return;}
                weakSelf!.tableView?.reloadData()
        }
    }
    
    //MARK: - UITableView Delegate & Data Source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if userType() == USER_TYPE_OTHER {
            return 0
        }else{
            var extraCount = 0
            if paginationController?.requestStatus == RequestStatus.requestStatusInProgress {
                extraCount = 1;
            }
            if  paginationController?.results.count > 0 {
                return (paginationController?.results.count)! + extraCount
            }else{
                return 0 + extraCount
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        var isProcessingIndicatorNeeded = false
        if paginationController?.results.count <= (indexPath as NSIndexPath).row {
            isProcessingIndicatorNeeded = true
        }
        if isProcessingIndicatorNeeded {
            return LoadingTableViewCell.getRequiredHeight()
        }
        let feedback = paginationController?.results.object(at: (indexPath as NSIndexPath).row) as? NSDictionary
        return UserFeedbacksTableViewCell.getRequiredHeight(feedback)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var isProcessingIndicatorNeeded = false
        if paginationController?.results.count <= (indexPath as NSIndexPath).row {
            isProcessingIndicatorNeeded = true
        }
        if isProcessingIndicatorNeeded {
            return (tableView .dequeueReusableCell(withIdentifier: "LoadingTableViewCell") as? LoadingTableViewCell)!
        }
        let feedback = paginationController?.results.object(at: (indexPath as NSIndexPath).row) as? NSDictionary
        let cell = tableView .dequeueReusableCell(withIdentifier: "UserFeedbacksTableViewCell") as? UserFeedbacksTableViewCell
        cell?.feedbackDetails = feedback
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let userFeedbacksTableViewCell = tableView .cellForRow(at: indexPath) as? UserFeedbacksTableViewCell {
            performAnimatedClickEffectType1(userFeedbacksTableViewCell.feedbackLabel!)
            DispatchQueue.main.async { () -> Void in
                performAnimatedClickEffectType1(userFeedbacksTableViewCell.ratingView)
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let velocity = scrollView.panGestureRecognizer.velocity(in: scrollView).y
        if velocity < 0 && scrollView.contentOffset.y > scrollView.contentSize.height - scrollView.bounds.size.height {
            if ((paginationController?.reachedLastPage())! as Bool) == false{
                if paginationController?.requestStatus == RequestStatus.requestStatusNone {
                    if scrollView == tableView && paginationController?.results.count > 0 {
                        fetchNextData()
                    }else{
                        fetchFirstPage()
                    }
                }
            }
        }
    }
    
    func fetchNextData(){
        if ((paginationController?.reachedLastPage())! as Bool) == false{
            if paginationController?.requestStatus == RequestStatus.requestStatusNone {
                if isInternetConnectivityAvailable(false) {
                    paginationController?.fetchNextPage()
                }
            }
        }
        tableView?.reloadData()
    }
    
    func fetchFirstPage(){
        paginationController?.reset()
        if isInternetConnectivityAvailable(false) {
            paginationController?.fetchFirstPage()
        }
        tableView?.reloadData()
    }
    
    //MARK: - other functions
    
    //section 1
    
    @IBAction func onClickOfProfilePicture(_ sender:Any){
        performAnimatedClickEffectType1(profilePictureImageView)
    }
    
    @IBAction func onClickOfEditProfilePicture(_ sender:Any){
        if(isInternetConnectivityAvailable(true)==false){return}
        trackScreenLaunchEvent(SLE_PROFILE_PICTURE_PICKER)
        let pickerController = DKImagePickerController()
        pickerController.allowMultipleTypes=false
        pickerController.singleSelect=true
        pickerController.assetType=DKImagePickerControllerAssetType.allPhotos
        pickerController.didSelectAssets = { [weak self] (assets: [DKAsset]) in guard let `self` = self else { return }
            for asset in assets{
                asset.fetchOriginalImageWithCompleteBlock({[weak self] (_image, info) in guard let `self` = self else { return }
                    execMain({[weak self] in guard let `self` = self else { return }
                        
                        if isNotNull(_image) {
                            let image = fixrotation(_image!)
                            let imageCropperViewController = RSKImageCropViewController(image: image, cropMode: .square)
                            imageCropperViewController.avoidEmptySpaceAroundImage = true
                            imageCropperViewController.setZoomScale(1.8)
                            imageCropperViewController.delegate = self;
                            self.present(imageCropperViewController, animated: true, completion: nil)
                        }
                    })
                })
            }
        }
        self.present(pickerController, animated: true, completion: nil)
    }
    
    func imageCropViewControllerDidCancelCrop(_ controller: RSKImageCropViewController){
        self.dismiss(animated: true)
    }
    
    func imageCropViewController(_ controller: RSKImageCropViewController, didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect){
        self.dismiss(animated: true)
        execMain({[weak self]  in guard let `self` = self else { return }
            self.profilePictureImageView.image = croppedImage
            let picname = "\(Dbm().getUserInfo()!.userId!)_\(currentTimeStamp()).jpg"
            let information = ["image":croppedImage,"imageName":picname] as NSDictionary
            showNotification(MESSAGE_TEXT___UPLOADING_YOUR_NEW_PROFILE_PICTURE, showOnNavigation: false, showAsError: false,duration: 5)
            let scm = ServerCommunicationManager()
            scm.responseErrorOption = .dontShowErrorResponseMessage
            scm.returnFailureResponseAlso = true
            scm.returnFailureUnParsedDataIfParsingFails = true
            scm.uploadImage(information) { [weak self] (responseData) -> () in
                if isNotNull(responseData){
                    let information = ["imageName":picname] as NSDictionary
                    let scm = ServerCommunicationManager()
                    scm.returnFailureResponseAlso = true
                    scm.responseErrorOption = .dontShowErrorResponseMessage
                    scm.updateProfilePicture(information) { (responseData) -> () in
                        Acf().updateUserProfileInformationFromServerIfRequired()
                        execMain({[weak self] in guard let `self` = self else { return }
                            
                            showNotification(MESSAGE_TEXT___PROFILE_PICTURE_COMMENT, showOnNavigation: false, showAsError: false,duration: 5)
                            }, delay: 5)
                    }
                }
            }
            },delay: 0.6)
    }
    
    
    @IBAction func onClickOfEditStatus(_ sender:Any){
        Acf().showStatusUpdateScreen()
    }
    
    //section 2
    
    @IBAction func onClickOfSelectHomeLocation(_ sender:Any){
        let settings = Dbm().getSetting()
        let coordinate = CLLocationCoordinate2D(latitude: settings.homeLocationLatitude!.doubleValue, longitude: settings.homeLocationLongitude!.doubleValue)
        Acf().showLocationOnPopupMapScreen(coordinate, address: settings.homeAddress)
    }
    
    @IBAction func onClickOfEditHomeLocation(_ sender:Any){
        Acf().pushVC("GetAddressViewController", navigationController: self.navigationController, isRootViewController: false, animated: true, modifyObject: nil)
    }
    
    @IBAction func onClickOfEditHomeLocationPickUpPoint(_ sender:Any){
        let settings = Dbm().getSetting()
        Acf().showInputTextController({ (enteredText) -> () in
            settings.homeAddressPickUpPoint = enteredText
            settings.isProfileUpdated = NSNumber(value: true)
            Dbm().saveChanges()
            Acf().updateProfileToServerIfRequired({ (returnedData) in})
            self.updateUserInterfaceOnScreen()
        }, text: settings.homeAddressPickUpPoint, limit: 128, editorTitle: "Edit Home Pick Up Point" ,actionButtonTitle:"Save")
        execMain({[weak self]  in guard let `self` = self else { return }
            self.tableView!.setContentOffset(CGPoint(x: self.tableView!.contentOffset.x, y: 280), animated: true)
            },delay: 0.4)
    }
    
    @IBAction func onClickOfSelectDestinationLocation(_ sender:Any){
        let settings = Dbm().getSetting()
        let coordinate = CLLocationCoordinate2D(latitude: settings.destinationLocationLatitude!.doubleValue, longitude: settings.destinationLocationLongitude!.doubleValue)
        Acf().showLocationOnPopupMapScreen(coordinate, address: settings.destinationAddress)
    }
    
    @IBAction func onClickOfEditDestinationLocation(_ sender:Any){
        Acf().pushVC("GetAddressViewController", navigationController: self.navigationController, isRootViewController: false, animated: true, modifyObject: nil)
    }
    
    @IBAction func onClickOfEditDestinationLocationPickUpPoint(_ sender:Any){
        let settings = Dbm().getSetting()
        Acf().showInputTextController({ (enteredText) -> () in
            settings.destinationAddressPickUpPoint = enteredText
            settings.isProfileUpdated = NSNumber(value: true)
            Dbm().saveChanges()
            Acf().updateProfileToServerIfRequired({ (returnedData) in})
            self.updateUserInterfaceOnScreen()
        }, text: settings.destinationAddressPickUpPoint, limit: 128, editorTitle: "Edit \(destinationCapitalizedName()) Pick Up Point" ,actionButtonTitle:"Save")
        execMain({[weak self]  in guard let `self` = self else { return }
            self.tableView!.setContentOffset(CGPoint(x: self.tableView!.contentOffset.x, y: 280), animated: true)
            },delay: 0.4)
    }
    
    //section 5
    @IBAction func onClickOfEditFavourites(_ sender:Any){
        Acf().pushVC("FavouriteUsersViewController", navigationController: self.navigationController, isRootViewController: false, animated: true, modifyObject: nil)
    }
    
    
    //MARK: - DZNEmptyDataSet Delegate & Data Source
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString!{
        let attributes = [NSFontAttributeName : UIFont.init(name: FONT_BOLD, size: 20)!]
        let title = "favourites"
        var message = "No " + title
        if (isInternetConnectivityAvailable(false)==false){
            message = "Unable to load " + title
        }
        return NSAttributedString(string: message, attributes: attributes)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString!{
        let attributes = [NSFontAttributeName : UIFont.init(name: FONT_REGULAR, size: 14)!]
        let title = "favourites"
        var message = "No " + title +  " available at the moment"
        if (isInternetConnectivityAvailable(false)==false){
            message = MESSAGE_TEXT___FOR_NETWORK_NOT_REACHABILITY
        }
        return NSAttributedString(string: message, attributes: attributes)
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage!{
        if !isUserLoggedIn() {
            return UIImage(named:"searchLightGray")
        }
        if (isInternetConnectivityAvailable(false)==false){
            return UIImage(named:"wifi")?.resizeWithWidth(45)
        }
        return UIImage(named:"searchLightGray")
    }
    
    // MARK: - UICollection View Delegate & Datasource
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int{
        return favouriteUsers.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAtIndexPath indexPath: IndexPath) -> UICollectionViewCell{
        let cell: UsersCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "UsersCollectionViewCell", for: indexPath) as! UsersCollectionViewCell
        let userInfo = favouriteUsers.object(at: (indexPath as NSIndexPath).row) as! Favourites
        cell.userFullnameLabel?.text = userInfo.name
        cell.userProfileImageView?.sd_setImage(with: URL(string: getUserProfilePictureUrlFromFileName(userInfo.picture)), placeholderImage: USER_PLACEHOLDER_IMAGE)
        return cell;
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: IndexPath){
        let user =  favouriteUsers.object(at: (indexPath as NSIndexPath).row) as? Favourites
        let userInfo =  user!.dictionaryWithValues(forKeys: Array(user!.entity.attributesByName.keys))
        showUserProfile(userInfo as NSDictionary,navigationController: self.navigationController!)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize{
        return CGSize(width: 120.0, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat{
        return 0.0;
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat{
        return 0.0;
    }
    
    
}
