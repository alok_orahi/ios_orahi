//
//  AuthenticationStep7ViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class AuthenticationStep7ViewController: UIViewController {
    
    @IBOutlet fileprivate weak var infoLabel: UILabel!
    @IBOutlet fileprivate weak var destinationNameLabel: UILabel!
    @IBOutlet fileprivate weak var destinationAddressLabel: UILabel!
    @IBOutlet fileprivate weak var destinationNamePlaceholderLabel: UILabel!
    @IBOutlet fileprivate weak var destinationAddressPlaceholderLabel: UILabel!
    @IBOutlet fileprivate weak var destinationNameIconImageView: UIImageView!
    @IBOutlet fileprivate weak var destinationAddressIconImageView: UIImageView!
    @IBOutlet fileprivate weak var timeLabel: UILabel!
    @IBOutlet fileprivate weak var amLabel: UILabel!
    @IBOutlet fileprivate weak var pmLabel: UILabel!
    @IBOutlet fileprivate weak var infoLabelTitle: UILabel!
    @IBOutlet fileprivate weak var infoLabelDescription: UILabel!
    
    @IBOutlet fileprivate weak var actionButton: TKTransitionSubmitButton!
    
    fileprivate var destinationTimingDate = Date().change( nil, month: nil, day: nil, hour: 18, minute: 0, second: 0)
    fileprivate var destinationLocationCoordinate: CLLocationCoordinate2D?
    fileprivate var destinationLocationAddress: String?
    fileprivate var locationPicker : ALPlacesViewController?
    fileprivate var arrayDestinationNameOptions = NSMutableArray()
    fileprivate var selectedIndexDestinationNameOptions = 0
    
    
    //MARK: - view controller life cycle methods
    
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
        updateContentsOnScreen()
    }
    
    //MARK: - other methods
    
    private func setupForNavigationBar(){
        setAppearanceForNavigationBarType2(self.navigationController)
        if self.isBackButtonRequired() {
            addNavigationBarButton(self, image: UIImage(named: "backarrowblack"), title: nil, isLeft: true,forceWhiteTint:false)
        }else{
            self.navigationItem.leftBarButtonItem = nil
            self.navigationItem.setHidesBackButton(true, animated:false)
        }
    }
    
    private func registerForNotifications(){
        
    }
    
    private func startUpInitialisations(){
        setAppearanceForViewController(self)
        self.actionButton.performAppearAnimationType1()
        let userInfo = Dbm().getUserInfo()
        let trueForOwnerFalseforPassenger = userInfo!.isPassenger!.isEqual("0")
        if trueForOwnerFalseforPassenger{
            infoLabel.attributedText = getAttributedTitleStringType3("CONGRATS!\n'SUBMIT' AND ENJOY CARPOOL", regularText: "CONGRATS!", anotherText:"'SUBMIT' AND ENJOY CARPOOL")
        }else{
            infoLabel.attributedText = getAttributedTitleStringType3("CONGRATS!\n'SUBMIT' AND ENJOY FREE RIDES", regularText: "CONGRATS!", anotherText:"'SUBMIT' AND ENJOY FREE RIDES")
        }
        destinationNamePlaceholderLabel.text = "Enter \(destinationName()) name"
        destinationAddressPlaceholderLabel.text = "Enter \(destinationName()) address"
        destinationNameIconImageView.image = UIImage(named: "\(destinationName())Name")
        destinationAddressIconImageView.image = UIImage(named: "\(destinationName())Address")
        addShadow(layer: timeLabel.layer,cornerRadius:timeLabel.bounds.size.height/2.0)
        updateContentsOnScreen()
    }
    
    func addShadow(layer:CALayer,cornerRadius:CGFloat){
        layer.cornerRadius = cornerRadius
        layer.shadowColor = APP_THEME_LIGHT_GRAY_COLOR.cgColor
        layer.shadowOffset = CGSize.zero
        layer.shadowOpacity = 0.3
        layer.shadowRadius = 5.0
        layer.borderWidth = 0.5
        layer.borderColor = UIColor.white.cgColor
        layer.backgroundColor = UIColor.white.cgColor
    }
    
    @objc private func updateUserInterfaceOnScreen(){
    }
    
    func onClickOfLeftBarButton(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    func canContinueForProfileUpdate()->(Bool){
        var canContinue = true
        if canContinue {
            canContinue = validateIfNull(destinationNameLabel.text , identifier: "\(destinationCapitalizedName()) name")
        }
        if canContinue {
            canContinue = validateIfNull(destinationLocationCoordinate , identifier: "\(destinationCapitalizedName()) address")
        }
        return canContinue
    }
    
    @IBAction func onClickOfDestinationNameButton(){
        resignKeyboard()
        let viewController = getViewController("DestinationPickerViewController") as! DestinationPickerViewController
        viewController.completion = { (returnedData) in
            if let company = returnedData as? Company{
                self.selectedIndexDestinationNameOptions = self.arrayDestinationNameOptions.index(of: company)
                self.destinationNameLabel.text = company.name
            }else if let college = returnedData as? College{
                self.selectedIndexDestinationNameOptions = self.arrayDestinationNameOptions.index(of: college)
                self.destinationNameLabel.text = college.name
            }
        }
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func onClickOfDestinationAddressButton(){
        resignKeyboard()
        Acf().setupIQKeyboardManagerDisabled()
        func onLocationPicked(_ name: String?, address: String?, coordinate: CLLocationCoordinate2D?, error: NSError?) {
            if error != nil {
                Acf().setupIQKeyboardManagerEnabled()
                return
            }
            Acf().navigationController!.dismiss(animated: true, completion: nil)
            Acf().setupIQKeyboardManagerEnabled()
            if isNotNull(address) && coordinate != nil {
                self.destinationLocationAddress = address
                self.destinationLocationCoordinate = coordinate
                self.updateContentsOnScreen()
                trackFunctionalEvent(FE_TIME_AND_ADDRESS_PICKER_DESTINATION_SUGGESTION_SELECTED, information: nil)
            }
        }
        
        self.locationPicker = ALPlacesViewController(APIKey: GOOGLE_API_KEY, completion: onLocationPicked)
        self.locationPicker?.placeholderText = "Enter \(destinationName()) address"
        Acf().navigationController?.present(self.locationPicker!, animated: true, completion: nil)
    }
    
    @IBAction func onClickOfActionButton(){
        if(isInternetConnectivityAvailable(true)==false){return}
        if canContinueForProfileUpdate(){
            UserDefaults.standard.set(true, forKey: "ClearTemporaryAddressCaches")
            UserDefaults.standard.synchronize()
            updateInformationOnDatabase()
            actionButton.startLoadingAnimation()
            Acf().updateProfileToServerIfRequired({ (returnedData) in
                if isNotNull(returnedData){
                    execMain({[weak self] in guard let `self` = self else { return }

                        updateUserProfile(loggedInUserId(), completion: { (returnedData) in
                            execMain({[weak self] in guard let `self` = self else { return }

                                Dbm().saveChanges()
                                if safeBool(returnedData) {
                                    UserDefaults.standard.remove("ClearTemporaryAddressCaches")
                                    UserDefaults.standard.synchronize()
                                    decideAndShowNextScreen(self.navigationController!)
                                }
                                self.actionButton.stopIt()
                            },delay:1.0)
                        })
                    },delay:1.0)
                }else{
                    self.actionButton.stopIt()
                }
            })
        }
    }
    
    @IBAction func onClickOfUpdateTimeHoursButton(){
        showDateTimePicker(destinationTimingDate!, mode: .time) { (selectedDate) in
            self.destinationTimingDate = selectedDate
            self.updateContentsOnScreen()
        }
    }
    
    @IBAction func onClickOfUpdateTimeAMButton(){
        if self.destinationTimingDate!.hour() >= 12 {
            self.destinationTimingDate = self.destinationTimingDate?.change(hour: self.destinationTimingDate!.hour()-12)
        }
        self.updateContentsOnScreen()
    }
    
    @IBAction func onClickOfUpdateTimePMButton(){
        if self.destinationTimingDate!.hour() < 12 {
            self.destinationTimingDate = self.destinationTimingDate?.change(hour: self.destinationTimingDate!.hour()+12)
        }
        self.updateContentsOnScreen()
    }
    
    func updateContentsOnScreen(){
        let customisedDarkGrayColor = UIColor(colorLiteralRed: 77/255, green: 77/255, blue: 77/255, alpha: 1.0)
        
        if self.destinationTimingDate!.hour() > 12 {
            pmLabel.makeMeRoundWith(borderColor: customisedDarkGrayColor, width: 0.0)
            pmLabel.backgroundColor = APP_THEME_VOILET_COLOR
            pmLabel.textColor = UIColor.white
            
            amLabel.makeMeRoundWith(borderColor: customisedDarkGrayColor, width: 1.0)
            amLabel.backgroundColor = UIColor.white
            amLabel.textColor = customisedDarkGrayColor
        }else{
            amLabel.makeMeRoundWith(borderColor: customisedDarkGrayColor, width: 0.0)
            amLabel.backgroundColor = APP_THEME_VOILET_COLOR
            amLabel.textColor = UIColor.white
            
            pmLabel.makeMeRoundWith(borderColor: customisedDarkGrayColor, width: 1.0)
            pmLabel.backgroundColor = UIColor.white
            pmLabel.textColor = customisedDarkGrayColor
        }
        
        timeLabel.text = self.destinationTimingDate!.stringTimeComponentOnly()
        
        infoLabelTitle.text = "\(destinationCapitalizedName()) departure time:"
        infoLabelDescription.text = "\(self.destinationTimingDate!.stringTimeOnly_AM_PM_FormatValue())"
        
        if isNotNull(destinationLocationAddress){
            destinationAddressLabel.text = destinationLocationAddress
        }
        destinationNamePlaceholderLabel.isHidden = safeString(destinationNameLabel.text).length > 0
        destinationAddressPlaceholderLabel.isHidden = safeString(destinationAddressLabel.text).length > 0
    }
    
    func updateInformationOnDatabase(){
        let settings = Dbm().getSetting()
        if isNotNull(destinationLocationAddress){
            settings.destinationAddress = destinationLocationAddress
        }
        if (destinationLocationCoordinate!.latitude != nil && destinationLocationCoordinate!.latitude != 0){
            settings.destinationLocationLatitude = NSNumber(value: destinationLocationCoordinate!.latitude as Double)
        }
        if (destinationLocationCoordinate?.longitude != nil && destinationLocationCoordinate!.longitude != 0){
            settings.destinationLocationLongitude = NSNumber(value: destinationLocationCoordinate!.longitude as Double)
        }
        if isNotNull(destinationTimingDate){
            settings.destinationTime = destinationTimingDate?.stringValue()
        }
        let userInfo = Dbm().getUserInfo()
        if isNotNull(destinationNameLabel.text){
            userInfo!.destinationName = destinationNameLabel.text!
        }
        settings.isProfileUpdated = NSNumber(value: true)
        Dbm().saveChanges()
    }
}
