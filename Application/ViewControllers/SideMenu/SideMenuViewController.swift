//
//  SideMenuViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit

fileprivate struct Section {
    var name: String!
    var items: [String]!
    var collapsed: Bool!
    init(name: String, items: [String], collapsed: Bool = true) {
        self.name = name
        self.items = items
        self.collapsed = collapsed
    }
}

class SideMenuViewController: UIViewController,UITextFieldDelegate,CollapsibleTableViewHeaderDelegate,UITableViewDataSource ,UITableViewDelegate {
    
    //MARK: - variables and constants
    @IBOutlet fileprivate weak var profilePictureImageView : UIImageView!
    @IBOutlet fileprivate weak var profilePictureBackgroundView : UIView!
    @IBOutlet fileprivate weak var fullNameLabel : UILabel!
    @IBOutlet fileprivate weak var statusTextLabel : UILabel!
    @IBOutlet fileprivate weak var tableView : UITableView!
    @IBOutlet fileprivate weak var appDescriptionLabel : UILabel!
    @IBOutlet fileprivate weak var heightConstraintBottomBannerImageView : NSLayoutConstraint!
    
    fileprivate var sections = [Section]()
    
    //MARK: - view controller life cycle methods
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
        updateUserInterfaceOnScreen()
        trackFunctionalEvent(FE_MENU_OPENED, information: nil)
    }
        
    //MARK: - other methods
    private func setupForNavigationBar(){
        setAppearanceForNavigationBarType1(self.navigationController)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    private func registerForNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(SideMenuViewController.updateViaNotification), name: NSNotification.Name(rawValue: NOTIFICATION_UPDATE_SIDE_MENU_NEEDED), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(SideMenuViewController.updateProfileChanges), name: NSNotification.Name(rawValue: NOTIFICATION_PROFILE_UPDATED), object: nil)
    }
    
    func updateViaNotification(){
        setupForNavigationBar()
        updateUserInterfaceOnScreen()
    }
    
    func updateProfileChanges(){
        updateUserInterfaceOnScreen()
    }
    
    private func startUpInitialisations(){
        prepareOptions()
        registerNib("SingleLabelAndImageTableViewCell", tableView: tableView)
        registerNibAsHeader("SingleLabelTableViewHeader", tableView: tableView)
        setAppearanceForViewController(self)
        setBorder(profilePictureBackgroundView, color: APP_THEME_YELLOW_COLOR_2, width: 3, cornerRadius: profilePictureBackgroundView.bounds.size.width/2)
        setBorder(profilePictureImageView, color: UIColor.clear, width: 1, cornerRadius: profilePictureImageView.bounds.size.width/2)
        #if DEBUG
            appDescriptionLabel?.text = "Orahi \(UIApplication.versionBuild()) #Debug mode"
        #else
            appDescriptionLabel?.text = "Orahi \(UIApplication.versionBuild())"
        #endif
    }
    
    func prepareOptions(){
        let carpoolSection = Section(name: "CARPOOL", items: ["Home","Payments","Verification","Settings"])
        let pOMSection = Section(name: "PEACE OF MIND", items: ["Home","Share","Tutorial"])
        sections = [carpoolSection,pOMSection]
        tableView.reloadData()
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        let userInfo = Dbm().getUserInfo()
        fullNameLabel.text = safeString(userInfo?.name, alternate: "Guest User").uppercased()
        statusTextLabel.text = getStatusText()
        if isNotNull(userInfo!.picture){
            profilePictureImageView.sd_setImage(with: URL(string: getUserProfilePictureUrlFromFileName(userInfo!.picture)), placeholderImage: USER_PLACEHOLDER_IMAGE)
        }else{
            profilePictureImageView.image = USER_PLACEHOLDER_IMAGE
        }
        let height = UIImage(named: "sideMenuBottomBanner")!.aspectHeightForWidth(UIScreen.main.bounds.width)
        heightConstraintBottomBannerImageView.constant = height
    }
    
    //MARK: - other functions
    
    @IBAction func onClickOfProfilePicture(_ sender:Any){
        performAnimatedClickEffectType1(profilePictureImageView)
        if !isSystemReadyToProcessThis() { return }
        Acf().updateUserProfileInformationFromServerIfRequired()
        Acf().pushVC("ProfileViewController", navigationController: Acf().navigationController, isRootViewController: false, animated: true) { (viewControllerObject) -> () in
        }
    }
    
    //MARK: - UITableView Delegate & Data Source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if sections[section].collapsed {
            return 0
        }else{
            return (sections[section].items.count+1)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let itemsCount = sections[indexPath.section].items.count
        let isForItems = indexPath.row < itemsCount
        let requiredHeight = isForItems ? 30.0 : 10.0
        return CGFloat(requiredHeight)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sectionData = sections[indexPath.section]
        let itemsCount = sectionData.items.count
        let isForItems = indexPath.row < itemsCount
        if isForItems {
            let cell = tableView .dequeueReusableCell(withIdentifier: "SingleLabelAndImageTableViewCell") as! SingleLabelAndImageTableViewCell
            let option = sectionData.items[indexPath.row]
            cell.titleLabel!.text = option.uppercased()
            let imageName = "\(option.lowercased().replacingOccurrences(of: " ", with: ""))_smIcon"
            cell.iconImageView!.image = UIImage(named:imageName)
            cell.isInitialisedOnce = true
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.titleLabel?.font = UIFont.init(name: FONT_REGULAR, size: 13)
            cell.titleLabel!.textColor = APP_THEME_DARK_GRAY_COLOR
            cell.separatorView.isHidden = true
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "SingleLabelTableViewHeader") as! SingleLabelTableViewHeader
        header.titleLabel?.text = sections[section].name
        header.titleLabel!.textColor = APP_THEME_DARK_GRAY_COLOR
        header.setCollapsed(sections[section].collapsed)
        header.section = section
        header.delegate = self
        header.separatorView?.isHidden = !(section != 0)
        if section == 1{
            header.badgeLabel?.isHidden = false
            header.badgeLabel?.text = "NEW"
            header.badgeLabelLeadingSpaceToSuperviewConstraint?.constant = 155
            header.setNeedsLayout()
            header.layoutIfNeeded()
        }else{
            header.badgeLabel?.text = ""
            header.badgeLabel?.isHidden = true
        }
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            var contentHeight:CGFloat = 0.0
            for section in (0 ..< numberOfSections(in: tableView)) {
                for row in (0 ..< self.tableView(tableView, numberOfRowsInSection: section)) {
                    let indexPath = NSIndexPath(row: row, section: section)
                    contentHeight += self.tableView(tableView, heightForRowAt: indexPath as IndexPath)
                }
            }
            let requiredHeight = (tableView.bounds.size.height - contentHeight - 47)/2
            if requiredHeight < 47{
                return 47
            }else{
                return requiredHeight
            }
        }else{
            return 47
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? SingleLabelAndImageTableViewCell {
            performAnimatedClickEffectType1(cell.titleLabel!)
            let sectionData = sections[indexPath.section]
            let option = sectionData.items[indexPath.row]
            if option == "Payments" {
                Acf().switchHomeScreenToCarpool(hideSideMenu: false)
                Acf().pushVC("PaymentOptionsViewController", navigationController: Acf().navigationController, isRootViewController: false, animated: true, modifyObject: nil)
            }else if option == "Verification" {
                Acf().switchHomeScreenToCarpool(hideSideMenu: false)
                if !isSystemReadyToProcessThis() { return }
                Acf().pushVC("VerificationOptionsViewController", navigationController: Acf().navigationController, isRootViewController: false, animated: true, modifyObject: nil)
            }else if option == "Settings" {
                Acf().switchHomeScreenToCarpool(hideSideMenu: false)
                if !isSystemReadyToProcessThis() { return }
                Acf().pushVC("SettingsViewController", navigationController: Acf().navigationController, isRootViewController: false, animated: true, modifyObject: nil)
            }else if option == "Home" && sectionData.name == "CARPOOL" {
                Acf().switchHomeScreenToCarpool()
            }else if option == "Home" && sectionData.name == "PEACE OF MIND" {
                Acf().switchHomeScreenToPoM()
            }else if option == "Tutorial" && sectionData.name == "PEACE OF MIND" {
                if !isSystemReadyToProcessThis() { return }
                Acf().pushVC("TuitorialViewController", navigationController: Acf().navigationController, isRootViewController: false, animated: true, modifyObject: nil)
            }else if option == "Share" && sectionData.name == "PEACE OF MIND" {
                showPoMShareOptions()
            }
        }
    }

    func toggleSection(_ header: SingleLabelTableViewHeader, section: Int) {
        let collapsed = !sections[section].collapsed
        if section == 0 {
            if userType() == USER_TYPE_OTHER {
                showEnableCarpoolPopupAndDoRequiredProcessing(); return
            }
        }
        for (i,_) in sections.enumerated() {
            sections[i].collapsed = true
        }
        sections[section].collapsed = collapsed
        header.setCollapsed(collapsed)
        let tableSections = NSIndexSet(indexesIn: NSMakeRange(0, self.numberOfSections(in: self.tableView)))
        self.tableView.reloadSections(tableSections as IndexSet, with: .automatic)
    }
    
    @IBAction fileprivate func onClickOfNeedHelpButton(){
        Acf().switchHomeScreenToCarpool(hideSideMenu: false)
        if !isSystemReadyToProcessThis() { return }
        Acf().pushVC("OtherOptionsViewController", navigationController: Acf().navigationController, isRootViewController: false, animated: true, modifyObject: nil)
    }
}
