//
//  AddEditPlaceViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit

enum AEPWorkingMode {
    case createNewPlace
    case editPlace
}

class AddEditPlaceViewController: UIViewController,MKMapViewDelegate,UIGestureRecognizerDelegate,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    
    //MARK: - variables and constants
    @IBOutlet fileprivate weak var upperInformationContainerView : UIView!
    @IBOutlet fileprivate weak var placeNameTextField : UITextField!
    @IBOutlet fileprivate weak var placeNameWithSuggestionsTextField : UITextField!
    @IBOutlet fileprivate weak var placeAddressTextField : UITextField!
    @IBOutlet fileprivate weak var mapView : MKMapView!
    @IBOutlet fileprivate weak var pinView : UIView!
    @IBOutlet fileprivate weak var actualNameEditorContainerView : UIView!
    @IBOutlet fileprivate weak var nameSuggestionsTableView : UITableView!
    @IBOutlet fileprivate weak var currentLocationButton: UIButton!
    @IBOutlet fileprivate weak var searchIcon: UIButton!
    @IBOutlet fileprivate weak var actionButton: TKTransitionSubmitButton!
    
    fileprivate var placesToSuggest = [Place]()
    fileprivate var placeCoordinate : CLLocationCoordinate2D?
    fileprivate var locationPicker : ALPlacesViewController?
    fileprivate var existingGroupPlaces : [Place] = []
    
    var workMode = AEPWorkingMode.createNewPlace
    var groupAssociatedWithPlace : Group?
    var groupId : String?
    var placeToEdit : Place?
    var placeId : String?

    fileprivate var existingPlaceSelected : Place?
    var ignoreRegionChange =  false
    var preFilledName : String?
    var isPlaceNameEdited = false
    
    //MARK: - view controller life cycle methods
    override internal var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
        updateUserInterfaceOnScreen()
    }
    
    //MARK: - other methods
    private func setupForNavigationBar(){
        setAppearanceForNavigationBarType1(self.navigationController,color: self.groupAssociatedWithPlace?.getGroupColor())
        if workMode == .createNewPlace {
            setupNavigationBarTitleType1("Add Place",viewController: self)
        }else{
            setupNavigationBarTitleType1("Edit Place",viewController: self)
        }
        if self.isBackButtonRequired() {
            addNavigationBarButton(self, image: UIImage(named: "backarrowblack"), title: nil, isLeft: true)
        }
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        execMain({
            if safeBool(self.placeAddressTextField.text?.length > 0){
                self.searchIcon.isHidden = true
            }else{
                self.searchIcon.isHidden = false
            }
            if safeBool(self.placeAddressTextField.text?.length > 0) && safeBool(self.placeNameTextField.text?.length > 0){
                self.actionButton.isHidden = false
            }else{
                self.actionButton.isHidden = true
            }
        }, delay: 0.2)
    }
    
    func onClickOfLeftBarButton(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    private func registerForNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(AddEditPlaceViewController.hideThingsToViewMapInBetterWay), name: NSNotification.Name(rawValue: NOTIFICATION_HIDE_FOR_MAP_VIEW), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AddEditPlaceViewController.reLoadGroupObjectFromDb), name: NSNotification.Name(rawValue: NOTIFICATION_GROUPS_PLACES_UPDATED), object: nil)
    }
    
    func reLoadGroupObjectFromDb(){
        if isNotNull(groupId){
            let updatedGroupObject = Dbm().getGroup(["groupId":safeString(groupId)])
            if isNotNull(updatedGroupObject) {
                self.groupAssociatedWithPlace = updatedGroupObject
                if isNotNull(self.placeId) {
                    self.placeToEdit = Dbm().getPlace(["placeId":safeString(self.placeId)])
                }
            }
        }
    }
    
    private func startUpInitialisations(){
        setupTableView()
        setAppearanceForViewController(self)
        self.actionButton.performAppearAnimationType1()
        if workMode == .createNewPlace {
            self.actionButton.setTitle("DONE", for: UIControlState.normal)
        }else{
            placeNameTextField.text = safeString(placeToEdit?.name)
            placeAddressTextField.text = safeString(placeToEdit?.placeAddress)
            placeCoordinate = CLLocationCoordinate2D(latitude: safeDouble(placeToEdit?.placeLatitude), longitude: safeDouble(placeToEdit?.placeLongitude))
            self.actionButton.setTitle("UPDATE", for: UIControlState.normal)
        }
        actualNameEditorContainerView.isHidden = true
        setupForMapView()
        if isNotNull(preFilledName){
            placeNameTextField.text = safeString(preFilledName)
            placeNameWithSuggestionsTextField.text = safeString(preFilledName)
            isPlaceNameEdited = true
        }
        groupId = safeString(groupAssociatedWithPlace?.groupId)
        placeId = safeString(placeToEdit?.placeId)
    }
    
    func setupTableView(){
        registerNib("TitleDescriptionImageTableViewCell", tableView: nameSuggestionsTableView)
        setAppearanceForTableView(nameSuggestionsTableView)
    }
    
    func setupForMapView() {
        mapView.showsUserLocation = true
        mapView.delegate = self
        let themeColor = ThemeColor().getColor(type:safeString(self.groupAssociatedWithPlace?.themeColor))
        pinView.makeMeRoundWith(borderColor:UIColor.white, width: 0.4)
        pinView.backgroundColor = themeColor
        pinView.layer.opacity = 0.3
        if workMode == .createNewPlace {
            execMain ({ (completed) in
                self.mapView.updateRegion(forAnnotations: self.mapView.annotations, animated: true)
            },delay: 2.0)
        }else{
            ignoreRegionChange = true
            let centerToMake = CLLocationCoordinate2D(latitude: safeDouble(placeToEdit?.placeLatitude), longitude: safeDouble(placeToEdit?.placeLongitude))
            self.mapView.updateRegion(centerToMake, animated: false, edgePadding: UIEdgeInsets(top: 30, left: 30, bottom: 100, right: 30))
            self.mapView.setCenter(centerToMake, animated: true)
            execMain ({ (completed) in
                self.ignoreRegionChange = false
            },delay: 3.0)
        }
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(self.didDragMap(sender:)))
        panGesture.delegate = self
        mapView.addGestureRecognizer(panGesture)
    }
    
    func didDragMap(sender:UIPanGestureRecognizer) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NOTIFICATION_HIDE_FOR_MAP_VIEW), object: nil)
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func takeCenterOfMapAsSelectedCoordinate(){
        setSelectedCoordinateAndFetchDetails(coordinate: self.mapView.centerCoordinate)
    }
    
    func setSelectedCoordinateAndFetchDetails(coordinate:CLLocationCoordinate2D){
        updateUserInterfaceOnScreen()
        placeCoordinate = coordinate
        weak var weakSelf = self
        SwiftLocation.shared.reverseCoordinates(.googleMaps, coordinates: self.placeCoordinate, onSuccess: { (placemark) in
            if let lines: Array<String> = placemark?.addressDictionary?["FormattedAddressLines"] as? Array<String> {
                let placeString = lines.joined(separator: ", ")
                weakSelf?.placeAddressTextField.text = placeString
                weakSelf?.updateUserInterfaceOnScreen()
            }
        }) { (error) in
        }
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool){
        if ignoreRegionChange == false {
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(AddEditPlaceViewController.takeCenterOfMapAsSelectedCoordinate), object: nil)
            self.perform(#selector(AddEditPlaceViewController.takeCenterOfMapAsSelectedCoordinate), with: nil, afterDelay: 1.0)
        }
    }
    
    func canContinueForAction()->(Bool){
        var canContinue = true
        if canContinue {
            canContinue = validateName(placeNameTextField.text, identifier: "Place Name")
        }
        if canContinue {
            canContinue = validateIfNull(placeAddressTextField.text, identifier: "Place Address")
        }
        if canContinue {
            canContinue = validateIfNull(self.placeCoordinate, identifier: "Place Address")
        }
        return canContinue
    }
    
    func resetSelectedExistingPlace() {
        self.existingPlaceSelected = nil;
        self.placeNameTextField.text = nil
        self.placeAddressTextField.text = nil
        updateUserInterfaceOnScreen()
        onClickOfMoveToCurrentLocation()
    }
    
    @IBAction func onClickOfActionButton(){
        if(isInternetConnectivityAvailable(true)==false){resetSelectedExistingPlace();return}
        if canContinueForAction(){
            if workMode == .createNewPlace {
                actionButton.startLoadingAnimation()
                let information = NSMutableDictionary()
                if isNotNull(self.existingPlaceSelected){
                    copyData(safeString(self.existingPlaceSelected?.placeId), destinationDictionary: information, destinationKey: "placeId", methodName:#function)
                    copyData(groupAssociatedWithPlace?.groupId, destinationDictionary: information, destinationKey: "groupId", methodName:#function)
                    copyData("associate", destinationDictionary: information, destinationKey: "actionType", methodName:#function)
                }else{
                    copyData(placeNameTextField.text, destinationDictionary: information, destinationKey: "name", methodName:#function)
                    copyData(placeAddressTextField.text, destinationDictionary: information, destinationKey: "address", methodName:#function)
                    copyData(groupAssociatedWithPlace?.groupId, destinationDictionary: information, destinationKey: "groupId", methodName:#function)
                    copyData(safeString(placeCoordinate?.latitude), destinationDictionary: information, destinationKey: "LocationLatitude", methodName: #function)
                    copyData(safeString(placeCoordinate?.longitude), destinationDictionary: information, destinationKey: "LocationLongitude", methodName: #function)
                    copyData("create", destinationDictionary: information, destinationKey: "actionType", methodName:#function)
                }
                let scm = ServerCommunicationManager()
                scm.managePlace(information) { (responseData) -> () in
                    if let _ = responseData {
                        Acf().syncGroupsAndPlaces(completion: {
                            self.navigationController?.popViewController(animated: true)
                            self.actionButton.stopIt()
                            Lem().doRequiredProcessing()
                        })
                    }else{
                        self.actionButton.stopIt()
                        self.resetSelectedExistingPlace()
                    }
                }
            }else{
                actionButton.startLoadingAnimation()
                let information = NSMutableDictionary()
                copyData(safeString(self.placeToEdit?.placeId), destinationDictionary: information, destinationKey: "placeId", methodName:#function)
                copyData(placeNameTextField.text, destinationDictionary: information, destinationKey: "name", methodName:#function)
                copyData(placeAddressTextField.text, destinationDictionary: information, destinationKey: "address", methodName:#function)
                copyData(safeString(placeCoordinate?.latitude), destinationDictionary: information, destinationKey: "LocationLatitude", methodName: #function)
                copyData(safeString(placeCoordinate?.longitude), destinationDictionary: information, destinationKey: "LocationLongitude", methodName: #function)
                copyData("update", destinationDictionary: information, destinationKey: "actionType", methodName:#function)
                let scm = ServerCommunicationManager()
                scm.managePlace(information) { (responseData) -> () in
                    if let _ = responseData {
                        Acf().syncGroupsAndPlaces(completion: {
                            self.navigationController?.popViewController(animated: true)
                            self.actionButton.stopIt()
                            Lem().doRequiredProcessing()
                        })
                    }else{
                        self.actionButton.stopIt()
                    }
                }
            }
        }
    }
    
    @IBAction func onClickOfMoveToCurrentLocation(){
        self.mapView.setCenter(self.mapView.userLocation.coordinate, animated: true)
        setSelectedCoordinateAndFetchDetails(coordinate: self.mapView.centerCoordinate)
    }
    
    @IBAction func onClickOfPlaceAddress(){
        resignKeyboard()
        Acf().setupIQKeyboardManagerDisabled()
        func onLocationPicked(_ name: String?, address: String?, coordinate: CLLocationCoordinate2D?, error: NSError?) {
            Acf().setupIQKeyboardManagerEnabled()
            if error != nil {
                return
            }
            Acf().navigationController!.dismiss(animated: true, completion: nil)
            if isNotNull(address) && coordinate != nil {
                self.placeAddressTextField.text = address
                self.placeCoordinate = coordinate
                self.mapView.setCenter(self.placeCoordinate!, animated: true)
            }
            if !isPlaceNameEdited && isNotNull(name) && workMode == .createNewPlace{
                func optimiseName(string:String)->String?{
                    
                    func removeSpecialCharsFromString(text: String) -> String {
                        let okayChars : Set<Character> =
                            Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890&-".characters)
                        return String(text.characters.filter {okayChars.contains($0) })
                    }
                    
                    var enhanced = string.enhancedString()
                    let components = enhanced.components(separatedBy: ",")
                    if isNotNull(components) && components.count > 0 {
                        enhanced = components[0]
                    }
                    enhanced = enhanced.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
                    enhanced = removeSpecialCharsFromString(text: enhanced)
                    if validateName(enhanced, identifier: nil,showMessage: false){
                        return enhanced
                    }else{
                        return nil
                    }
                }
                let optimisedNameOfPlace = optimiseName(string: name!)
                if isNotNull(optimisedNameOfPlace){
                    placeNameTextField.text = optimisedNameOfPlace!
                }
            }
        }
        self.locationPicker = ALPlacesViewController(APIKey: GOOGLE_API_KEY, completion: onLocationPicked)
        self.locationPicker?.placeholderText = "Enter Place address"
        Acf().navigationController?.present(self.locationPicker!, animated: true, completion: nil)
    }
    
    func hideThingsToViewMapInBetterWay(){
        UIView.animate(withDuration: HIDE_ANIMATION_DURATION, delay: 0, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.actionButton.layer.opacity = OPACITY_WHEN_HIDDEN_FOR_MAP_VIEW
            self.upperInformationContainerView.layer.opacity = OPACITY_WHEN_HIDDEN_FOR_MAP_VIEW
            self.currentLocationButton.layer.opacity = OPACITY_WHEN_HIDDEN_FOR_MAP_VIEW
            self.navigationController?.setNavigationBarHidden(true, animated: true)
        }) { (completed) in }
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(AddEditPlaceViewController.restoreHiddenComponents), object: nil)
        self.perform(#selector(AddEditPlaceViewController.restoreHiddenComponents), with: nil, afterDelay: RESTORE_HIDDEN_VIEW_TIME)
    }
    
    func restoreHiddenComponents(){
        UIView.animate(withDuration: SHOW_ANIMATION_DURATION, delay: 0, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.actionButton.layer.opacity = 1.0
            self.upperInformationContainerView.layer.opacity = 1.0
            self.currentLocationButton.layer.opacity = 1.0
            self.navigationController?.setNavigationBarHidden(false, animated: true)
        }) { (completed) in }
    }
    
    func updateSuggestionListAsPerSearchResult(){
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(AddEditPlaceViewController.updateSuggestionListAsPerSearchResultPrivate), object: nil)
        self.perform(#selector(AddEditPlaceViewController.updateSuggestionListAsPerSearchResultPrivate), with: nil, afterDelay: 0.4)
    }
    
    func updateSuggestionListAsPerSearchResultPrivate(){
        if existingGroupPlaces.count == 0 && isNotNull(self.groupAssociatedWithPlace?.associatedPlaces) {
            for ap in self.groupAssociatedWithPlace!.associatedPlaces! {
                let place = (ap as! AssociatedPlaces).placeObject()
                if place != nil {
                    existingGroupPlaces.append(place!)
                }
            }
        }
        self.placesToSuggest = Dbm().searchPlaces(text: placeNameWithSuggestionsTextField.text)
        self.placesToSuggest = self.placesToSuggest.filter({ (place) -> Bool in
            return !existingGroupPlaces.contains(place)
        })
        self.nameSuggestionsTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.placesToSuggest.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 90
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TitleDescriptionImageTableViewCell") as! TitleDescriptionImageTableViewCell
        let place = self.placesToSuggest[indexPath.row]
        let title = safeString(place.name)
        let description = safeString(place.placeAddress)
        cell.titleLabel.text = title.trimmedString()
        cell.descriptionLabel.text = description.trimmedString()
        cell.iconImageView.image = UIImage(named: "locationIcon2")
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if workMode == .createNewPlace {
            self.existingPlaceSelected = self.placesToSuggest[indexPath.row]
            self.placeNameWithSuggestionsTextField.text = safeString(self.existingPlaceSelected?.name)
            self.placeNameTextField.text = safeString(self.existingPlaceSelected?.name)
            self.placeAddressTextField.text = self.existingPlaceSelected?.placeAddress
            self.placeCoordinate = CLLocationCoordinate2D(latitude: safeDouble(existingPlaceSelected?.placeLatitude), longitude: safeDouble(existingPlaceSelected?.placeLongitude))
            updateUserInterfaceOnScreen()
            resignKeyboard()
            onClickOfActionButton()
        }else{
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        execMain({
            if self.workMode == .createNewPlace {
                if self.placeNameWithSuggestionsTextField.isFirstResponder == false{
                    self.placeNameWithSuggestionsTextField.becomeFirstResponder()
                }
            }else{
                if self.placeNameTextField.isFirstResponder == false{
                    self.placeNameTextField.becomeFirstResponder()
                }
            }
        }, delay: 0.0)
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == placeNameWithSuggestionsTextField {
            self.actualNameEditorContainerView.isHidden = false
            updateSuggestionListAsPerSearchResultPrivate()
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.actualNameEditorContainerView.isHidden = true
        if workMode == .createNewPlace {
            self.placeNameTextField.text = self.placeNameWithSuggestionsTextField.text
        }else{
        }
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        updateUserInterfaceOnScreen()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == placeNameWithSuggestionsTextField {
            updateSuggestionListAsPerSearchResult()
            updateUserInterfaceOnScreen()
        }
        isPlaceNameEdited = true
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        resignKeyboard()
        return false
    }

    
}
