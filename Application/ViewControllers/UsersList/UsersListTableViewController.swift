//
//  UsersListTableViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import Foundation
import DZNEmptyDataSet

class UsersListTableViewController: UITableViewController,DZNEmptyDataSetDelegate,DZNEmptyDataSetSource  {
    
    //MARK: - variables and constants
    internal var users : [QBUUser]?
    fileprivate var isLoading = false
    internal var isSearchingMode = false
    
    //MARK: - view controller life cycle methods
    override internal func viewDidLoad() {
        super.viewDidLoad()
        if isSearchingMode == false {
            setupForUsers()
        }
        self.tableView!.emptyDataSetSource = self
        self.tableView!.emptyDataSetDelegate = self
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    
    //MARK: - other methods
    
    func removedSuperAdmin(_ users:[QBUUser]?) -> [QBUUser] {
        let filteredUsers = NSMutableArray()
        if isNotNull(users){
            let superAdmin = Acf().getQuickBloxUserName(QB_USER_ADMIN_QBID)
            for u in users! {
                if u.login == superAdmin {
                    logMessage("ignoring super admin")
                }else{
                    filteredUsers.add(u)
                }
            }
        }
        return filteredUsers as! [QBUUser]
    }
    
    
    func setupForUsers(){
        weak var weakSelf = self
        self.isLoading = true
        self.tableView.reloadEmptyDataSet()
        if let usersToAdd = (ServicesManager.instance().usersService.usersMemoryStorage.unsortedUsers() as? [QBUUser]){
            self.setupUsers(usersToAdd)
        }
        if isInternetConnectivityAvailable(false){
            ServicesManager.instance().downloadUsers(Acf().getSuggestedLoginIds(), success: { (users) -> Void in
                if isNotNull(users){
                    self.isLoading = false
                    self.tableView.reloadEmptyDataSet()
                    weakSelf?.setupUsers(users!)
                }
                }, error: { (error) -> Void in
                    self.isLoading = false
                    self.tableView.reloadEmptyDataSet()
            })
        }else{
            self.isLoading = false
            self.tableView.reloadEmptyDataSet()
        }
    }
    
    func setupForUsers(_ searchText:String?){
        self.isLoading = true
        self.tableView.reloadEmptyDataSet()
        if let usersToAdd = (ServicesManager.instance().usersService.usersMemoryStorage.unsortedUsers() as? [QBUUser]){
            self.setupUsers(usersToAdd)
        }
        if isInternetConnectivityAvailable(false) && isNotNull(searchText){
            ServicesManager.instance().searchUsers(searchText!, success: { (users) -> Void in
                if isNotNull(users){
                    self.isLoading = false
                    self.tableView.reloadEmptyDataSet()
                    self.setupUsers(users!)
                }
                }, error: { (error) -> Void in
                    self.isLoading = false
                    self.tableView.reloadEmptyDataSet()
            })
        }else{
            self.users = nil
            self.isLoading = false
            self.tableView.reloadData()
            self.tableView.reloadEmptyDataSet()
        }
    }
    
    //MARK: - DZNEmptyDataSet Delegate & Data Source
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString!{
        let attributes = [NSFontAttributeName : UIFont.init(name: FONT_BOLD, size: 20)!]
        var message = "No users"
        if (isInternetConnectivityAvailable(false)==false){
            if isSearchingMode == false {
                message = "Unable to load users"
            }else{
                message = "Unable to search users"
            }
        }
        if isLoading {
            if isSearchingMode == false {
                message = "Loading"
            }else{
                message = "Searching"
            }
        }
        return NSAttributedString(string: message, attributes: attributes)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString!{
        let attributes = [NSFontAttributeName : UIFont.init(name: FONT_REGULAR, size: 14)!]
        var message = "No users available at the moment"
        if (isInternetConnectivityAvailable(false)==false){
            message = MESSAGE_TEXT___FOR_NETWORK_NOT_REACHABILITY
        }
        if isLoading {
            if isSearchingMode == false {
                message = "Please wait while we are updating users"
            }else{
                message = "Please wait while we are searching users"
            }
        }
        return NSAttributedString(string: message, attributes: attributes)
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage!{
        if (isInternetConnectivityAvailable(false)==false){
            return UIImage(named:"wifi")
        }
        return UIImage(named:"searchLightGray")
    }
    
    
    // MARK: - UITableViewDelegate & UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _ = self.users {
            return self.users!.count
        } else {
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserTableViewCell", for: indexPath) as! UserTableViewCell
        let user = self.users![(indexPath as NSIndexPath).row]
        cell.userDescription = nameToDisplay(user,chatDialogue: nil)
        cell.tag = (indexPath as NSIndexPath).row
        return cell
    }
    
    func setupUsers(_ users: [QBUUser]) {
        self.users = removedSuperAdmin(users)
        self.tableView.reloadData()
    }
    
}
