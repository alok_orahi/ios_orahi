//
//  CheckInViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit

class CheckInViewController: UIViewController,MKMapViewDelegate,UIGestureRecognizerDelegate,UITableViewDelegate,UITableViewDataSource {
    
    //MARK: - variables and constants
    var mapView : MKMapView!
    @IBOutlet fileprivate weak var tableView : UITableView!
    fileprivate var nearBySuggestions = NSMutableArray()
    fileprivate var currentAddress = "Obtaining current address..."
    fileprivate var currentLocationCoordinate : CLLocationCoordinate2D?
    fileprivate var locationAccuracy = -1
    
    var group : Group?
    
    //MARK: - view controller life cycle methods
    override internal var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
        updateUserInterfaceOnScreen()
        fetchCurrentAddressAndLocationCoordinates()
    }
    
    //MARK: - other methods
    private func setupForNavigationBar(){
        setAppearanceForNavigationBarType1(self.navigationController,color: self.group?.getGroupColor())
        setupNavigationBarTitleType1("Check-In",viewController: self)
        if self.isBackButtonRequired() {
            addNavigationBarButton(self, image: UIImage(named: "backarrowblack"), title: nil, isLeft: true)
        }
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        
    }
    
    func onClickOfLeftBarButton(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    private func registerForNotifications(){
    }
    
    private func startUpInitialisations(){
        setupTableView()
        setAppearanceForTableView(self.tableView)
        setAppearanceForViewController(self)
        setupForMapView()
        execMain ({
            self.fetchCurrentAddressAndLocationCoordinates()
        },delay:2)
        execMain ({
            self.fetchCurrentAddressAndLocationCoordinates()
        },delay:5)
        execMain ({
            self.fetchCurrentAddressAndLocationCoordinates()
        },delay:10)
    }
    
    func setupTableView(){
        registerNib("TitleDescriptionImageTableViewCell", tableView: tableView)
        registerNib("SingleLabelTableViewCellType2", tableView: tableView)
        tableView!.separatorInset = UIEdgeInsets.zero
        tableView!.layoutMargins = UIEdgeInsets.zero
    }
    
    func setupForMapView() {
        mapView = MKMapView(x: 0, y: 0, w: DEVICE_WIDTH, h: DEVICE_HEIGHT/3)
        mapView.showsUserLocation = true
        mapView.delegate = self
        self.tableView.addParallax(with: self.mapView, andHeight: DEVICE_HEIGHT/3)
        execMain ({ (completed) in
            self.mapView.updateRegion(forAnnotations: self.mapView.annotations, animated: true)
        },delay: 2.0)
    }
    
    func fetchCurrentAddressAndLocationCoordinates(){
        weak var weakSelf = self
        SwiftLocation.shared.reverseCoordinates(.googleMaps, coordinates: self.mapView.userLocation.coordinate, onSuccess: { (placemark) in
            if let lines: Array<String> = placemark?.addressDictionary?["FormattedAddressLines"] as? Array<String> {
                let placeString = lines.joined(separator: ", ")
                weakSelf?.currentAddress = placeString
                weakSelf?.currentLocationCoordinate = self.mapView.userLocation.coordinate
                weakSelf?.locationAccuracy = safeInt(placemark?.location?.horizontalAccuracy)
                weakSelf?.tableView.reloadData()
                weakSelf?.fetchNearByLocations()
            }
        }) { (error) in
            self.currentAddress = "Unable to locate 🤒"
        }
    }
    
    func fetchNearByLocations(){
        ALPlacesSearchInteractor()
            .setCoordinate(self.mapView.userLocation.coordinate)
            .setAPIkey(GOOGLE_API_KEY)
            .setRadius(Meters(distance: 1000))
            .onCompletion { places, error in
                if let p = places {
                    self.nearBySuggestions.addObjects(from: p)
                    self.tableView.reloadData()
                }
            }.search()
    }
    
    func canContinueForAction()->(Bool){
        return true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nearBySuggestions.count + 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        if indexPath.row == 1 {
            return 45
        }
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SingleLabelTableViewCellType2") as! SingleLabelTableViewCellType2
            cell.titleLabel?.text = "NEAR BY PLACES"
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TitleDescriptionImageTableViewCell") as! TitleDescriptionImageTableViewCell
            cell.iconImageView.makeMeRoundWith(borderColor: APP_THEME_GRAY_COLOR, width: 0.5)
            if indexPath.row == 0 {
                cell.titleLabel.text = currentAddress.capitalizeFirst.trimmedString()
                if locationAccuracy > 0 {
                    cell.descriptionLabel.text = "Check-In with current location , accurate upto \(locationAccuracy)m"
                }else{
                    cell.descriptionLabel.text = "Check-In with current location"
                }
                cell.iconImageView.image = UIImage(named: "locationArrowSmall")
            }else{
                let place = self.nearBySuggestions[indexPath.row-2] as! ALPlace
                let title = safeString(place.name).capitalized
                let address = place.address
                cell.titleLabel.text = safeString(title).capitalized.trimmedString()
                cell.descriptionLabel.text = address?.capitalizeFirst.trimmedString()
                cell.iconImageView.image = UIImage(named: "genericLocationAnnotationSmall")
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row != 1 {
            if(isInternetConnectivityAvailable(true)==false){return}
            let information = NSMutableDictionary()
            copyData(safeString(group?.groupId), destinationDictionary: information, destinationKey: "groupId", methodName:#function)
            if indexPath.row == 0 {
                if isNull(currentLocationCoordinate)||isNull(currentAddress){return;}
                copyData("Location", destinationDictionary: information, destinationKey: "name", methodName:#function)
                copyData(safeString(currentAddress), destinationDictionary: information, destinationKey: "address", methodName:#function)
                copyData(safeString(currentLocationCoordinate?.latitude), destinationDictionary: information, destinationKey: "locationLatitude", methodName:#function)
                copyData(safeString(currentLocationCoordinate?.longitude), destinationDictionary: information, destinationKey: "locationLongitude", methodName:#function)
            }else{
                let place = self.nearBySuggestions[indexPath.row-2] as! ALPlace
                if isNull(place.coordinate)||isNull(place.address){return;}
                copyData(safeString(place.name?.capitalized.trimmedString()), destinationDictionary: information, destinationKey: "name", methodName:#function)
                copyData(safeString(place.address?.capitalizeFirst.trimmedString()), destinationDictionary: information, destinationKey: "address", methodName:#function)
                copyData(safeString(place.coordinate?.latitude), destinationDictionary: information, destinationKey: "locationLatitude", methodName:#function)
                copyData(safeString(place.coordinate?.longitude), destinationDictionary: information, destinationKey: "locationLongitude", methodName:#function)
            }
            let scm = ServerCommunicationManager()
            scm.responseErrorOption = ResponseErrorOption.dontShowErrorResponseMessage
            scm.showSuccessResponseMessage = true
            scm.checkIn(information) { (responseData) in
            }
            self.navigationController?.popViewController(animated: true)
        }
    }
}
