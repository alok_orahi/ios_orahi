//
//  GroupDetailViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class GroupDetailViewController: UIViewController,MKMapViewDelegate,UIGestureRecognizerDelegate,UITableViewDataSource,UITableViewDelegate {
    
    //MARK: - variables and constants
    @IBOutlet fileprivate var navigationBarView : UIView!
    @IBOutlet fileprivate var tableView : UITableView!
    @IBOutlet fileprivate var titleLabel : UILabel!
    @IBOutlet fileprivate var notificationButton : UIButton!
    @IBOutlet fileprivate var syncButton : UIButton!
    fileprivate var mapView : MKMapView!
    fileprivate var quickActionsView : GroupDetailQuickActionsView?
    fileprivate var animateAnnotations = true
    fileprivate var mapViewGapFromBottom = 0.0 as CGFloat
    var group : Group?
    var groupId : String?
    var canLoadTableView = false
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
        updateUserInterfaceOnScreen()
        updateNotificationBadge(notificationButton: self.notificationButton,ax:-7,ay:2,groupId: safeString(self.groupId))
    }
    
    override internal func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupForNavigationBar()
        checkIfPopupsRequiredToGuideUsers()
        canLoadTableView = true
        execMain({
            self.tableView.reloadData()
            self.updateTableViewContentOffset()
        }, delay: 0.1)
    }
    
    //MARK: - other methods
    private func setupForNavigationBar(){
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        updateNavigationBarColorAndTitle()
    }
    
    private func registerForNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(GroupDetailViewController.hideThingsToViewMapInBetterWay), name: NSNotification.Name(rawValue: NOTIFICATION_HIDE_FOR_MAP_VIEW), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(GroupDetailViewController.reLoadGroupObjectFromDb), name: NSNotification.Name(rawValue: NOTIFICATION_GROUPS_PLACES_UPDATED), object: nil)
    }
    
    @IBAction func onClickOfSyncButton(_ sender:Any){
        animateSyncButton()
        if(isInternetConnectivityAvailable(true)==false){return}
        Acf().syncGroupsAndPlacesFromServer()
    }
    
    func animateSyncButton(){
        self.syncButton!.transform = CGAffineTransform.identity
        self.syncButton!.rotate(toAngle: CGFloat(M_PI * 2.0), duration: 2.0 , direction: .right, repeatCount: 1 , autoreverse: false)
    }

    func updateNavigationBarColorAndTitle(){
        self.navigationBarView.backgroundColor = self.group?.getGroupColor()
        self.titleLabel.text = getGroupNameType1(group: group!)
    }
    
    func reLoadGroupObjectFromDb(){
        let updatedGroupObject = Dbm().getGroup(["groupId":safeString(groupId)])
        if isNotNull(updatedGroupObject) {
            self.group = updatedGroupObject
            self.quickActionsView?.group = self.group
            self.tableView.reloadData()
            updateNavigationBarColorAndTitle()
            updateUserInterfaceOnScreen()
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func didDragMap(_ sender: UIGestureRecognizer) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NOTIFICATION_HIDE_FOR_MAP_VIEW), object: nil)
    }
    
    private func startUpInitialisations(){
        setupTableView()
        setAppearanceForViewController(self)
        setupForMapView()
        setupForQuickActionsView()
        groupId = safeString(group?.groupId)
    }
    
    private func setupTableView(){
        registerNib("GroupMemberTableViewCell", tableView: tableView)
        registerNib("InviteNewMemberTableViewCell", tableView: tableView)
        registerNib("GroupDetailSuggestionTableViewCell", tableView: tableView)
        tableView!.separatorInset = UIEdgeInsets.zero
        tableView!.separatorStyle = UITableViewCellSeparatorStyle.none
        tableView!.layoutMargins = UIEdgeInsets.zero
        execMain({
            self.tableView.addParallax(with: self.mapView, andHeight: DEVICE_HEIGHT - self.mapViewGapFromBottom)
            execMain({
                if self.tableView.numberOfRows(inSection: 0) > 0 {
                    self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableViewScrollPosition.bottom, animated: true)
                }
            }, delay: 0.6)
        }, delay: 0.0)
    }
    
    private func setupForMapView() {
        mapView = MKMapView(x: 0, y: 0, w: DEVICE_WIDTH, h: DEVICE_HEIGHT - mapViewGapFromBottom)
        mapView.showsUserLocation = true
        mapView.delegate = self
        let groupId = safeString(group?.groupId)
        let x = safeDouble(CacheManager.sharedInstance.loadObject("\(groupId)mapX"))
        let y = safeDouble(CacheManager.sharedInstance.loadObject("\(groupId)mapY"))
        let w = safeDouble(CacheManager.sharedInstance.loadObject("\(groupId)mapW"))
        let h = safeDouble(CacheManager.sharedInstance.loadObject("\(groupId)mapH"))
        if x > 0.0 {
            self.mapView.setVisibleMapRect(MKMapRectMake(x, y, w, h), animated: false)
        }
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(self.didDragMap(_:)))
        panGesture.delegate = self
        mapView.addGestureRecognizer(panGesture)
    }
    
    private func setupForQuickActionsView(){
        execMain ({ (returnedData) in
            self.quickActionsView = GroupDetailQuickActionsView()
            self.quickActionsView?.group = self.group
            self.view.addSubview(self.quickActionsView!)
            self.quickActionsView?.translatesAutoresizingMaskIntoConstraints = false
            self.quickActionsView?.leadingAnchor.constraint(equalTo: self.view.leadingAnchor,constant:0).isActive = true
            self.quickActionsView?.trailingAnchor.constraint(equalTo: self.view.trailingAnchor,constant:0).isActive = true
            self.quickActionsView?.bottomAnchor.constraint(equalTo: self.view.bottomAnchor,constant:0).isActive = true
            self.quickActionsView?.heightAnchor.constraint(equalToConstant: 50).isActive = true
            self.quickActionsView?.navigationController = self.navigationController
            self.quickActionsView?.layer.opacity = 0.0
            UIView.animate(withDuration: 0.4, animations: {
                self.quickActionsView?.layer.opacity = 1.0
            })
        },delay: 0.4)
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        updateMapContents()
    }
    
    func updateMapContents(){
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(GroupDetailViewController.updateMapContentsPrivate), object: nil)
        self.perform(#selector(GroupDetailViewController.updateMapContentsPrivate), with: nil, afterDelay: 0.5)
    }
    
    func updateMapContentsPrivate(){
        //step 1 : remove everything from the map
        let annotationsCountBefore = mapView.annotations.count
        mapView.removeAnnotations(mapView.annotations)
        
        //step 2 : add users annotations on map
        let annotationsToAdd = NSMutableArray()
        
        if isNotNull(group?.members) {
            for user in group!.members! {
                if (user as! Member).locationDisplay {
                    let annotation = UserType1PinAnnotation()
                    var lastKnownLocationLatitude = (user as! Member).lastKnownLocationLatitude
                    var lastKnownLocationLongitude = (user as! Member).lastKnownLocationLongitude
                    if lastKnownLocationLongitude != nil && lastKnownLocationLatitude != nil {
                        annotation.coordinate = CLLocationCoordinate2DMake(Double(lastKnownLocationLatitude!)!, Double(lastKnownLocationLongitude!)!)
                    }else{
                        continue
                    }
                    annotationsToAdd.add(annotation)
                    annotation.userInfo = (user as! Member)
                    annotation.title = (user as! Member).name?.capitalized
                }
            }
        }
        
        if isNotNull(group?.associatedPlaces) {
            for ap in group!.associatedPlaces! {
                let place = Dbm().getPlace(["placeId":"\(safeString((ap as! AssociatedPlaces).placeId))"])
                if place != nil {
                    let annotation = PlaceType1Annotation()
                    let locationLatitude = place?.placeLatitude
                    let locationLongitude = place?.placeLongitude
                    if locationLatitude != nil && locationLongitude != nil {
                        annotation.coordinate = CLLocationCoordinate2DMake(Double(locationLatitude!)!, Double(locationLongitude!)!)
                    }else{
                        continue
                    }
                    annotationsToAdd.add(annotation)
                    annotation.userInfo = place
                    annotation.title = place!.name
                }
            }
        }
        self.mapView.addAnnotations(annotationsToAdd as! [MKAnnotation])
        
        //step 3 : adjust map so that every content become visible on screen
        
        execMain({[weak self]  in guard let `self` = self else { return }
            let annotationsCountAfter = self.mapView.annotations.count
            if annotationsCountAfter != annotationsCountBefore {
                self.animateAnnotations = true
                self.mapView.updateRegion(forAnnotations: self.mapView.annotations, animated: true)
                execMain({[weak self] in guard let `self` = self else { return }
                    
                    self.animateAnnotations = false
                    execMain({[weak self] in guard let `self` = self else { return }
                        
                        let x = self.mapView.visibleMapRect.origin.x
                        let y = self.mapView.visibleMapRect.origin.y
                        let w = self.mapView.visibleMapRect.size.width
                        let h = self.mapView.visibleMapRect.size.height
                        let groupId = safeString(self.group?.groupId)
                        CacheManager.sharedInstance.saveObject(x, identifier: "\(groupId)mapX")
                        CacheManager.sharedInstance.saveObject(y, identifier: "\(groupId)mapY")
                        CacheManager.sharedInstance.saveObject(w, identifier: "\(groupId)mapW")
                        CacheManager.sharedInstance.saveObject(h, identifier: "\(groupId)mapH")
                        },delay: 2)
                })
            }
            },delay: 2)
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is UserType1PinAnnotation {
            var annotationView = self.mapView.dequeueReusableAnnotationView(withIdentifier: "userAnnotation") as! UserType2MKAnnotationView?
            if isNull(annotationView) {
                annotationView = UserType2MKAnnotationView(annotation: annotation, reuseIdentifier: "userAnnotation")
            }
            annotationView!.userInfo = (annotation as! UserType1PinAnnotation).userInfo as? Member
            annotationView!.group = self.group
            annotationView!.setup()
            annotationView?.canShowCallout = true
            if animateAnnotations {
                annotationView?.performAppearAnimationType5(Double(Float.random(3.0, 8.0)))
            }
            if (annotation as! UserType1PinAnnotation).isRecentlyMoved {
                annotationView?.transform = CGAffineTransform.identity
                annotationView?.pulse(toSize: 0.90, duration: 0.35, repeat: true)
            }else{
                annotationView?.stopAnimation()
                annotationView?.transform = CGAffineTransform(scaleX: ANNOTATION_TRANSFORM_ADJUST, y: ANNOTATION_TRANSFORM_ADJUST)
            }
            return annotationView
        }else if annotation is PlaceType1Annotation {
            var annotationView = self.mapView.dequeueReusableAnnotationView(withIdentifier: "placeAnnotation") as! PlaceTypeMKAnnotationView?
            if isNull(annotationView) {
                annotationView = PlaceTypeMKAnnotationView(annotation: annotation, reuseIdentifier: "placeAnnotation")
            }
            annotationView!.userInfo = (annotation as! PlaceType1Annotation).userInfo as? Place
            annotationView!.group = self.group
            annotationView!.setup()
            annotationView?.canShowCallout = true
            if animateAnnotations {
                annotationView?.performAppearAnimationType5(Double(Float.random(3.0, 8.0)))
            }
            annotationView?.transform = CGAffineTransform(scaleX: ANNOTATION_TRANSFORM_ADJUST, y: ANNOTATION_TRANSFORM_ADJUST)
            return annotationView
        }
        return nil
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
    }
    
    @IBAction func onClickOfBackButton(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickOfChatButton(_ sender:Any){
        Acf().showGroupChatScreen(Acf().chatGroupName(group: self.group!), from: self,themeColor: self.group!.getGroupColor(),group: self.group)
    }
    
    @IBAction func onClickOfNotificationsButton(_ sender:Any){
        Acf().showNotificationScreen(nc: self.navigationController,themeColor: self.group!.getGroupColor(),group: self.group)
    }
    
    @IBAction func onClickOfOptionsButton(_ sender:Any){
        let actionSheet = Hokusai()
        if safeBool(self.group?.isAdmin){
            actionSheet.addButton("Edit Group") {
                Acf().pushVC("AddEditGroupViewController", navigationController: self.navigationController, isRootViewController: false, animated: true, modifyObject:{ (viewControllerObject) -> () in
                    (viewControllerObject as! AddEditGroupViewController).groupToEdit = self.group
                    (viewControllerObject as! AddEditGroupViewController).workMode = .editGroup
                })
            }
        }
        actionSheet.addButton("Add Place") {
            Acf().pushVC("AddEditPlaceViewController", navigationController: self.navigationController, isRootViewController: false, animated: true, modifyObject: { (viewController) in
                (viewController as! AddEditPlaceViewController).workMode = .createNewPlace
                (viewController as! AddEditPlaceViewController).groupAssociatedWithPlace = self.group
            })
        }
        actionSheet.addButton("Leave Group") {
            self.showLeaveGroupPopupAndDoRequiredProcessing()
        }
        if safeBool(self.group?.isMuted) {
            actionSheet.addButton("Un mute") {
                execMain({
                    self.processMuteUnmuteForGroup(group: self.group!, toMute: false, muteForHours: 0)
                },delay: 0.1)
            }
        }else{
            actionSheet.addButton("Mute") {
                execMain({
                    let actionSheet = Hokusai()
                    actionSheet.addButton("8 Hours") {
                        execMain({
                            self.processMuteUnmuteForGroup(group: self.group!, toMute: true, muteForHours: 8)
                        },delay: 0.1)
                    }
                    actionSheet.addButton("1 Day") {
                        execMain({
                            self.processMuteUnmuteForGroup(group: self.group!, toMute: true, muteForHours: 24)
                        },delay: 0.1)
                    }
                    actionSheet.addButton("1 Week") {
                        execMain({
                            self.processMuteUnmuteForGroup(group: self.group!, toMute: true, muteForHours: 24*7)
                        },delay: 0.1)
                    }
                    actionSheet.cancelButtonTitle = "Cancel"
                    actionSheet.colors = self.group?.hokColor()
                    actionSheet.show()
                },delay: 1.2)
            }
        }
        actionSheet.cancelButtonTitle = "Cancel"
        actionSheet.colors = self.group?.hokColor()
        actionSheet.show()
    }
    
    func processMuteUnmuteForGroup(group:Group,toMute:Bool,muteForHours:Int){
        if(isInternetConnectivityAvailable(true)==false){return}
        let information = NSMutableDictionary()
        copyData(safeString(group.groupId), destinationDictionary: information, destinationKey: "groupId", methodName: #function)
        copyData(toMute ? "mute":"unmute", destinationDictionary: information, destinationKey: "actionType", methodName: #function)
        copyData("\(muteForHours)", destinationDictionary: information, destinationKey: "muteHours", methodName: #function)
        let scm = ServerCommunicationManager()
        scm.responseErrorOption = ResponseErrorOption.dontShowErrorResponseMessage
        scm.progressIndicatorText = ""
        scm.configureGroupPushNotification(information, completionBlock: {(responseData) -> () in
            if let _ = responseData {
                group.isMuted = toMute
                Dbm().saveChanges()
                self.setupForNavigationBar()
            }
        })
    }
    
    func showLeaveGroupPopupAndDoRequiredProcessing(){
        func leaveTheGroup(){
            if(isInternetConnectivityAvailable(false)==false){return}
            let information = NSMutableDictionary()
            copyData(safeString(group?.groupId), destinationDictionary: information, destinationKey: "groupId", methodName: #function)
            copyData("unassociate", destinationDictionary: information, destinationKey: "actionType", methodName: #function)
            let scm = ServerCommunicationManager()
            scm.responseErrorOption = ResponseErrorOption.dontShowErrorResponseMessage
            scm.progressIndicatorText = ""
            scm.leaveGroup(information, completionBlock: {[weak self] (responseData) -> () in guard let `self` = self else { return }
                if isNotNull(responseData){
                    if Acf().isQBReadyForItsUserDependentServices() {
                        let dialog = Acf().getChatDialogue(safeString(self.group?.groupId))
                        if dialog != nil {
                            ServicesManager.instance().chatService.deleteDialog(withID: dialog!.id!, completion: { (response: QBResponse!) -> Void in
                                if !response.isSuccess {
                                    logMessage(response.error?.error)
                                }
                            })
                        }
                    }
                    Acf().syncGroupsAndPlaces(completion: {
                        self.navigationController?.popViewController(animated: true)
                        Lem().doRequiredProcessing()
                    })
                }
            })
        }
        let prompt = UIAlertController(title: "💔 Leave \(safeString(self.group?.name).capitalized)", message: "\nAre you sure , you want to leave", preferredStyle: UIAlertControllerStyle.alert)
        prompt.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        prompt.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.destructive, handler: { (action) -> Void in
            leaveTheGroup()
        }))
        Acf().navigationController!.present(prompt, animated: true, completion: nil)
    }
    
    func hideThingsToViewMapInBetterWay(){
        UIView.animate(withDuration: HIDE_ANIMATION_DURATION, delay: 0, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.quickActionsView?.layer.opacity = OPACITY_WHEN_HIDDEN_FOR_MAP_VIEW
            self.navigationBarView.layer.opacity = OPACITY_WHEN_HIDDEN_FOR_MAP_VIEW
        }) { (completed) in }
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(GroupDetailViewController.restoreHiddenComponents), object: nil)
        self.perform(#selector(GroupDetailViewController.restoreHiddenComponents), with: nil, afterDelay: RESTORE_HIDDEN_VIEW_TIME)
    }
    
    func restoreHiddenComponents(){
        UIView.animate(withDuration: SHOW_ANIMATION_DURATION, delay: 0, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.quickActionsView?.layer.opacity = 1.0
            self.navigationBarView.layer.opacity = 1.0
        }) { (completed) in }
    }
    
    //MARK: - UITableView Delegate & Data Source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if canLoadTableView {
            return safeInt(self.group?.members?.count) + 2
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        if indexPath.row == 0 {
            return 81
        }else if indexPath.row <= safeInt(self.group?.members?.count) {
            return 66
        }else{
            return 476
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row < safeInt(self.group?.members?.count) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "GroupMemberTableViewCell") as! GroupMemberTableViewCell
            cell.group = self.group
            cell.member = self.group?.members?.object(at: indexPath.row) as! Member
            return cell
        }else if indexPath.row == safeInt(self.group?.members?.count) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "InviteNewMemberTableViewCell") as! InviteNewMemberTableViewCell
            cell.group = self.group
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "GroupDetailSuggestionTableViewCell") as! GroupDetailSuggestionTableViewCell
            cell.group = self.group
            cell.navigationController = self.navigationController
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row < safeInt(self.group?.members?.count) {
            let member = self.group?.members?[indexPath.row] as! Member
            if safeString(member.userId) != loggedInUserId() {
                showMemberDetailScreen(group: self.group!, member: member, navigationController: self.navigationController!)
            }
        }else if indexPath.row == safeInt(self.group?.members?.count) {
            sendGroupJoinInvitations(group: self.group!)
        }else{
            
        }
    }
    
    fileprivate func checkIfPopupsRequiredToGuideUsers(){
        execMain({
            if self.checkIfPlaceAdditionPopIfRequired() == false {
                if self.checkIfMemberAdditionPopIfRequired() == false {
                }
            }
        }, delay: 3)
    }
    
    fileprivate func checkIfPlaceAdditionPopIfRequired()->Bool{
        let key = "askedForPlace\(groupId!)"
        if !safeBool(CacheManager.sharedInstance.loadObject(key)) && safeInt(group!.associatedPlaces?.count) == 0{
            CacheManager.sharedInstance.saveObject(true, identifier: key)
            let prompt = UIAlertController(title: "ADD A PLACE", message: "\nYour group doesn't seem to have\n any places associated. Do you want to add a place?", preferredStyle: UIAlertControllerStyle.alert)
            prompt.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.default, handler: { (action) -> Void in
                Acf().pushVC("AddEditPlaceViewController", navigationController: self.navigationController, isRootViewController: false, animated: true, modifyObject: { (viewController) in
                    (viewController as! AddEditPlaceViewController).workMode = .createNewPlace
                    (viewController as! AddEditPlaceViewController).groupAssociatedWithPlace = self.group
                })
            }
            ))
            prompt.addAction(UIAlertAction(title: "Later", style: UIAlertActionStyle.cancel, handler: nil))
            self.present(prompt, animated: true, completion: nil)
            return true
        }
        return false
    }
    
    fileprivate func checkIfMemberAdditionPopIfRequired()->Bool{
        let key = "askedForMember\(groupId!)"
        if !safeBool(CacheManager.sharedInstance.loadObject(key)) && safeInt(group!.members?.count) <= 1{
            CacheManager.sharedInstance.saveObject(true, identifier: key)
            let prompt = UIAlertController(title: "ADD MEMBERS", message: "\nLets invite your family or friends to this group", preferredStyle: UIAlertControllerStyle.alert)
            prompt.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.default, handler: { (action) -> Void in
                sendGroupJoinInvitations(group: self.group!)
            }
            ))
            prompt.addAction(UIAlertAction(title: "Later", style: UIAlertActionStyle.cancel, handler: nil))
            self.present(prompt, animated: true, completion: nil)
            return true
        }
        return false
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        updateTableViewContentOffset()
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        updateTableViewContentOffset()
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool){
        updateTableViewContentOffset()
    }
    
    func updateTableViewContentOffset(){
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(GroupDetailViewController.updateTableViewContentOffsetPrivate), object: nil)
        self.perform(#selector(GroupDetailViewController.updateTableViewContentOffsetPrivate), with: nil, afterDelay: 0.3)
    }
    
    func updateTableViewContentOffsetPrivate(){
        if self.tableView.contentOffset.y < -(DEVICE_HEIGHT-88){
            if self.canLoadTableView && self.tableView.numberOfRows(inSection: 0) > 2{
                self.tableView.scrollToRow(at: IndexPath(row: 1, section: 0), at: UITableViewScrollPosition.bottom, animated: true)
            }else if self.canLoadTableView && self.tableView.numberOfRows(inSection: 0) > 0{
                self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableViewScrollPosition.bottom, animated: true)
            }else{
                self.tableView.setContentOffset(CGPoint(x: 0, y: -DEVICE_HEIGHT), animated: true)
            }
        }
    }
    
}
