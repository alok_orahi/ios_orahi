//
//  ProfileOthersViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

fileprivate func <= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l <= r
  default:
    return !(rhs < lhs)
  }
}

class ProfileOthersViewController: UIViewController,UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,DZNEmptyDataSetSource,DZNEmptyDataSetDelegate {
    
    
    //MARK: - variables and constants
    @IBOutlet fileprivate weak var tableView : UITableView?
    @IBOutlet fileprivate weak var tableViewHeaderView : UIView?
    var userInfo : NSDictionary?
    @IBOutlet fileprivate weak var section1HolderView : UIView!
    
    @IBOutlet fileprivate weak var section3HolderView : UIView!
    @IBOutlet fileprivate weak var section4HolderView : UIView!
    @IBOutlet fileprivate weak var section5HolderView : UIView!
    @IBOutlet fileprivate weak var section6HolderView : UIView!
    @IBOutlet fileprivate weak var section7HolderView : UIView!
    @IBOutlet fileprivate weak var section8HolderView : UIView!
    
    //section 1
    @IBOutlet fileprivate weak var userInformationHolderView : UIView!
    @IBOutlet fileprivate weak var userStatusColorView : UIView!
    @IBOutlet fileprivate weak var profilePictureImageView : UIImageView!
    @IBOutlet fileprivate weak var ratingView : HCSStarRatingView!
    @IBOutlet fileprivate weak var ridesDoneLabelTop : UILabel!
    @IBOutlet fileprivate weak var userLevelImage : UIImageView!
    @IBOutlet fileprivate weak var fullNameLabel : UILabel!
    @IBOutlet fileprivate weak var statusTextLabel : UILabel!
    @IBOutlet fileprivate weak var modeLabel : UILabel!
    @IBOutlet fileprivate weak var lastActiveLabel : UILabel!
    @IBOutlet fileprivate weak var ridingWithUserImageView1 : UIImageView!
    @IBOutlet fileprivate weak var ridingWithUserImageView2 : UIImageView!
    @IBOutlet fileprivate weak var ridingWithUserImageView3 : UIImageView!
    @IBOutlet fileprivate weak var ridingWithUserImageView4 : UIImageView!
    @IBOutlet fileprivate weak var callButton : UIButton!
    @IBOutlet fileprivate weak var chatButton : UIButton!
    @IBOutlet fileprivate weak var tagLabel : UILabel!
    @IBOutlet fileprivate weak var tagBgImageView : UIImageView!
    @IBOutlet fileprivate weak var heightConstraintRidingWithContainerView : NSLayoutConstraint!
    @IBOutlet fileprivate weak var heightConstraintSection1HeaderView : NSLayoutConstraint!

    //section 3
    @IBOutlet fileprivate weak var homeLocationLabel : UILabel!
    @IBOutlet fileprivate weak var homeLocationPickupPointLabel : UILabel!
    @IBOutlet fileprivate weak var pickupTimeHome : UILabel!
    @IBOutlet fileprivate weak var destinationTypeLabel : UILabel!
    @IBOutlet fileprivate weak var destinationLocationLabel : UILabel!
    @IBOutlet fileprivate weak var destinationLocationPickupPointLabel : UILabel!
    @IBOutlet fileprivate weak var pickupTimeDestination : UILabel!

    //section 4
    @IBOutlet fileprivate weak var trustShieldFactor1ImageView : UIImageView!
    @IBOutlet fileprivate weak var trustShieldFactor2ImageView : UIImageView!
    @IBOutlet fileprivate weak var trustShieldFactor3ImageView : UIImageView!
    
    //section 5
    @IBOutlet fileprivate weak var favouriteButton : DOFavoriteButton!
    @IBOutlet fileprivate weak var blockButton : UIButton!
    @IBOutlet fileprivate weak var blockUnBlockStatusLabel : UILabel!

    //section 6
    @IBOutlet fileprivate weak var ridesDoneLabel : UILabel!
    @IBOutlet fileprivate weak var kmsSharedLabel : UILabel!
    @IBOutlet fileprivate weak var communityRankLabel : UILabel!
    @IBOutlet fileprivate weak var communityRankingPositionLabel : UILabel!
    @IBOutlet fileprivate weak var communityRankingPositionImageView : UIImageView!
    
    //section7
    @IBOutlet fileprivate weak var travelledWithCollectionView : UICollectionView!

    fileprivate var travelledWithUsers = NSMutableArray()
    fileprivate var destinationLocationCoordinate : CLLocationCoordinate2D?
    fileprivate var homeLocationCoordinate : CLLocationCoordinate2D?
    fileprivate var paginationController : Paginator?
    var preViewingActions : NSMutableArray?
    
    //MARK: - view controller life cycle methods
    
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupTableView()
    }
    
    //MARK: - other methods
    private func setupForNavigationBar(){
        setAppearanceForNavigationBarType1(self.navigationController)
        setupNavigationBarTitleType1("Profile",viewController: self)
        if self.isBackButtonRequired() {
            addNavigationBarButton(self, image: UIImage(named: "backarrowblack"), title: nil, isLeft: true)
        }
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    func onClickOfLeftBarButton(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    private func registerForNotifications(){
    }
    
    private func startUpInitialisations(){
        setAppearanceForViewController(self)
        setAppearanceForTableView(tableView)
        
        setBorder(profilePictureImageView, color: UIColor.clear, width: 0, cornerRadius: profilePictureImageView.bounds.size.width/2)
        setBorder(ridingWithUserImageView1, color: UIColor.clear, width: 0, cornerRadius: ridingWithUserImageView1.bounds.size.width/2)
        setBorder(ridingWithUserImageView2, color: UIColor.clear, width: 0, cornerRadius: ridingWithUserImageView2.bounds.size.width/2)
        setBorder(ridingWithUserImageView3, color: UIColor.clear, width: 0, cornerRadius: ridingWithUserImageView3.bounds.size.width/2)
        setBorder(ridingWithUserImageView4, color: UIColor.clear, width: 0, cornerRadius: ridingWithUserImageView4.bounds.size.width/2)
        
        ratingView.isUserInteractionEnabled = false
        
        trackScreenLaunchEvent(SLE_OTHERS_PROFILE)
    }
    
    func showRidingWithSection(_ show:Bool){
        if show{
            self.heightConstraintRidingWithContainerView.constant = 63
            self.heightConstraintSection1HeaderView.constant = 223
        }else{
            self.heightConstraintRidingWithContainerView.constant = 0
            self.heightConstraintSection1HeaderView.constant = 223 - 63
        }
    }
    
    @objc private func updateUserInterfaceOnScreen(){

        self.showRidingWithSection(isRidingWithDetailsExist(self.userInfo))
        
        weak var weakSelf = self

        //section 1
        if let userPhoto = userInfo?.object(forKey: "user_photo") as? String{
            profilePictureImageView.sd_setImage(with: URL(string: getUserProfilePictureUrlFromFileName(userPhoto)), placeholderImage: USER_PLACEHOLDER_IMAGE)
        }else{
            profilePictureImageView.image = USER_PLACEHOLDER_IMAGE
        }
        if let name = userInfo?.object(forKey: "user_name") as? String{
            fullNameLabel.text = name
        }
        statusTextLabel.text = getStatusTextFor(userInfo)

        destinationLocationLabel.text = safeString(userInfo!.object(forKey: "company_name"))
        
        destinationTypeLabel.text = destinationCapitalizedNameForUser(userInfo!)

        favouriteButton.isSelected = Dbm().isFavourite(userInfo!)!
        
        section4HolderView.showActivityIndicatorAtPoint(CGPoint(x: section4HolderView.center.x,y: 22))
        section5HolderView.showActivityIndicatorAtPoint(CGPoint(x: section5HolderView.center.x,y: 22))
        section6HolderView.showActivityIndicatorAtPoint(CGPoint(x: section6HolderView.center.x,y: 22))
        
        Acf().getUserProfileDetails(getUserId(weakSelf!.userInfo),completion: { (returnedData) -> () in
            if isNull(weakSelf){return;}
            if isNull(returnedData){return;}
            
            weakSelf!.section3HolderView.hideActivityIndicator()
            weakSelf!.section4HolderView.hideActivityIndicator()
            weakSelf!.section5HolderView.hideActivityIndicator()
            weakSelf!.section6HolderView.hideActivityIndicator()
            
            let profileInfo = (returnedData as! NSDictionary).object(forKey: "profileInformation") as! NSDictionary
            let settingsInfo = (returnedData as! NSDictionary).object(forKey: "informationForSettings") as! NSDictionary
            
            
            if let travelledWith = (returnedData as! NSDictionary).object(forKey: "travelledWith") as? NSMutableArray {
                weakSelf!.travelledWithUsers = travelledWith
            }
            weakSelf!.travelledWithCollectionView.reloadData()
            
            if isNotNull(settingsInfo.object(forKey: "homeLocationLatitude")) && isNotNull(settingsInfo.object(forKey: "homeLocationLongitude")){
                weakSelf!.homeLocationCoordinate = CLLocationCoordinate2DMake((settingsInfo.object(forKey: "homeLocationLatitude") as! NSNumber).doubleValue, (settingsInfo.object(forKey: "homeLocationLongitude") as! NSNumber).doubleValue)
            }
            if isNotNull(settingsInfo.object(forKey: "destinationLocationLatitude")) && isNotNull(settingsInfo.object(forKey: "destinationLocationLongitude")){
                weakSelf!.destinationLocationCoordinate = CLLocationCoordinate2DMake((settingsInfo.object(forKey: "destinationLocationLatitude")  as! NSNumber).doubleValue, (settingsInfo.object(forKey: "destinationLocationLongitude")  as! NSNumber).doubleValue)
            }
            
            self.destinationTypeLabel.text = destinationCapitalizedNameForUser(settingsInfo)

            //section 1
            
            let info = profileInfo
            
            weakSelf!.userStatusColorView.backgroundColor = Acf().colorForUserAsPerTravelRequestStatus(weakSelf!.userInfo!)
            
            weakSelf!.profilePictureImageView.sd_setImage(with: URL(string: getUserProfilePictureUrlFromFileName(info.object(forKey: "picture") as? String)), placeholderImage: USER_PLACEHOLDER_IMAGE)
            
            if isNotNull(info.object(forKey: "rating")){
                weakSelf!.ratingView.value = CGFloat(safeString(info.object(forKey: "rating")).toFloat())
            }else{
                weakSelf!.ratingView.value = 0.0
            }
            
            if isNotNull(info.object(forKey: "ridesDone")){
                weakSelf!.ridesDoneLabelTop.text = "\(safeString(info.object(forKey: "ridesDone"))) Rides"
            }else{
                weakSelf!.ridesDoneLabelTop.text = "0 Rides"
            }
            
            weakSelf!.communityRankLabel.text = safeString(info.object(forKey: "rank"),alternate: "0")
            
            weakSelf!.fullNameLabel.text = safeString(info.object(forKey: "name"))
            
            weakSelf!.statusTextLabel.text = getStatusTextFor(weakSelf!.userInfo)
            
            weakSelf!.destinationLocationLabel.text = safeString(info.object(forKey: "companyName"))
            
            updateModeLabelType2(info, userInfo: self.userInfo, modeLabel: self.modeLabel)
            
            if isNotNull(self.userInfo!.object(forKey: "last_active")){
                weakSelf!.lastActiveLabel.isHidden = false
                let lastActive = safeString(self.userInfo!.object(forKey: "last_active"))
                if lastActive.isEqual("0")  {
                    weakSelf!.lastActiveLabel.text = "Active Today"
                }else{
                    weakSelf!.lastActiveLabel.text = "\(lastActive) days ago"
                }
            }else{
                weakSelf!.lastActiveLabel.text = ""
            }
            
            weakSelf!.pickupTimeHome.text = getTimeFormatInAmPm(info.object(forKey: "etd_mode_0") as? String)
            weakSelf!.pickupTimeDestination.text = getTimeFormatInAmPm(info.object(forKey: "etd_mode_1") as? String)
            
            setRidingWithDetails(weakSelf!.userInfo!, img1: weakSelf!.ridingWithUserImageView1, img2: weakSelf!.ridingWithUserImageView2, img3: weakSelf!.ridingWithUserImageView3, img4: weakSelf!.ridingWithUserImageView4)

            self.showRidingWithSection(isRidingWithDetailsExist(self.userInfo))
            
            setUserTagDetails(self.userInfo, tagImageView: self.tagBgImageView, tagLabel: self.tagLabel)
            
            weakSelf!.setupRequiredActions()
            
            
            
            //section 2
            weakSelf!.homeLocationLabel.text = safeString(settingsInfo.object(forKey: "homeAddress"))
            weakSelf!.homeLocationPickupPointLabel.text = safeString(settingsInfo.object(forKey: "homeAddressPickUpPoint"))
            weakSelf!.destinationLocationLabel.text = safeString(settingsInfo.object(forKey: "destinationAddress"))
            weakSelf!.destinationLocationPickupPointLabel.text = safeString(settingsInfo.object(forKey: "destinationAddressPickUpPoint"))
            
            //section 3
            weakSelf!.trustShieldFactor1ImageView.isHighlighted = safeString(settingsInfo.object(forKey: "verifiedMobileNumber")).isEqual("1")
            weakSelf!.trustShieldFactor2ImageView.isHighlighted = safeString(settingsInfo.object(forKey: "verifiedUserDesignation")).isEqual("1")
            weakSelf!.trustShieldFactor3ImageView.isHighlighted = safeString(settingsInfo.object(forKey: "verifiedGovernmentIdProofDocument")).isEqual("1")
            
            //section 4
            weakSelf!.favouriteButton.isSelected = Dbm().isFavourite(weakSelf!.userInfo!)!
            
            //section 5
            func setRidesDone(){
                let attributesNumbers = [NSFontAttributeName:UIFont.init(name: FONT_BOLD, size: 13)!,
                    NSForegroundColorAttributeName:UIColor.darkGray] as NSDictionary
                let attributesInfo = [NSFontAttributeName:UIFont.init(name: FONT_REGULAR, size: 9)!,
                    NSForegroundColorAttributeName:UIColor.gray] as NSDictionary
                
                var attributedNumberValue = "0"
                if let ridesDone = profileInfo.object(forKey: "ridesDone") {
                    attributedNumberValue = "\("\(ridesDone)".toInt())"
                }else{
                    attributedNumberValue = "0"
                }
                let attributedInfoValue = " Rides Done"
                
                let attributedString = NSMutableAttributedString(string:attributedNumberValue+attributedInfoValue)
                attributedString.setAttributes(attributesNumbers as? [String : Any], range: NSMakeRange(0, attributedNumberValue.length))
                attributedString.setAttributes(attributesInfo as? [String : Any], range: NSMakeRange(attributedNumberValue.length, attributedString.length-attributedNumberValue.length))
                weakSelf!.ridesDoneLabel.attributedText = attributedString
            }
            
            func setKmsShared(){
                let attributesNumbers = [NSFontAttributeName:UIFont.init(name: FONT_BOLD, size: 13)!,
                    NSForegroundColorAttributeName:UIColor.darkGray] as NSDictionary
                let attributesInfo = [NSFontAttributeName:UIFont.init(name: FONT_REGULAR, size: 9)!,
                    NSForegroundColorAttributeName:UIColor.gray] as NSDictionary
                
                var attributedNumberValue = "0"
                if let kmsShared = profileInfo.object(forKey: "kmsShared") {
                    attributedNumberValue = "\("\(kmsShared)".toInt())"
                }else{
                    attributedNumberValue = "0"
                }
                let attributedInfoValue = " Kms Shared"
                
                let attributedString = NSMutableAttributedString(string:attributedNumberValue+attributedInfoValue)
                attributedString.setAttributes(attributesNumbers as? [String : Any], range: NSMakeRange(0, attributedNumberValue.length))
                attributedString.setAttributes(attributesInfo as? [String : Any], range: NSMakeRange(attributedNumberValue.length, attributedString.length-attributedNumberValue.length))
                weakSelf!.kmsSharedLabel.attributedText = attributedString
            }
            
            setRidesDone()
            setKmsShared()
            
            if isNotNull(profileInfo.object(forKey: "blockedByMe")){
                if (profileInfo.object(forKey: "blockedByMe")! as! NSString).isEqual(to: "1"){
                    weakSelf!.blockButton.isSelected = true
                    weakSelf!.blockUnBlockStatusLabel.text = "Un Block"
                }else{
                    weakSelf!.blockButton.isSelected = false
                    weakSelf!.blockUnBlockStatusLabel.text = "Block"
                }
            }else{
                weakSelf!.blockButton.isSelected = false
                weakSelf!.blockUnBlockStatusLabel.text = "Block"
            }
            
            setRankImage(weakSelf!.communityRankLabel.text!, imageView: weakSelf!.communityRankingPositionImageView)
            setRankName(weakSelf!.communityRankLabel.text!,label:weakSelf!.communityRankingPositionLabel)
        })
    }
    
    //MARK: - other functions
    
    func setupTableView(){
        weak var weakSelf = self
        if isNull(tableView?.tableHeaderView){
            registerNib("LoadingTableViewCell", tableView: tableView)
            registerNib("UserFeedbacksTableViewCell", tableView: tableView)
            tableView?.tableHeaderView = weakSelf!.tableViewHeaderView
            setAppearanceForTableView(tableView)
            setupForPaginationController()
            fetchFirstPage()
        }
    }
    
    func setupForPaginationController(){
        weak var weakSelf = self
        paginationController = Paginator()
        paginationController?.setup(PAGE_SIZE)
        paginationController?.paginationFor = PaginatorMode.feedbacks
        paginationController?.userInfo = ["userId":getUserId(weakSelf!.userInfo)]
        paginationController?.completionBlock =
            { (paginator) -> () in
                if isNull(weakSelf){return;}
                weakSelf!.tableView?.reloadData()
        }
    }
    
    //MARK: - UITableView Delegate & Data Source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var extraCount = 0
        if paginationController?.requestStatus == RequestStatus.requestStatusInProgress {
            extraCount = 1;
        }
        if  paginationController?.results.count > 0 {
            logMessage((paginationController?.results.count)! + extraCount)
            return (paginationController?.results.count)! + extraCount
        }else{
            return 0 + extraCount
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        var isProcessingIndicatorNeeded = false
        if paginationController?.results.count <= (indexPath as NSIndexPath).row {
            isProcessingIndicatorNeeded = true
        }
        if isProcessingIndicatorNeeded {
            return LoadingTableViewCell.getRequiredHeight()
        }
        let feedback = paginationController?.results.object(at: (indexPath as NSIndexPath).row) as? NSDictionary
        return UserFeedbacksTableViewCell.getRequiredHeight(feedback)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var isProcessingIndicatorNeeded = false
        if paginationController?.results.count <= (indexPath as NSIndexPath).row {
            isProcessingIndicatorNeeded = true
        }
        if isProcessingIndicatorNeeded {
            return (tableView .dequeueReusableCell(withIdentifier: "LoadingTableViewCell") as? LoadingTableViewCell)!
        }
        let feedback = paginationController?.results.object(at: (indexPath as NSIndexPath).row) as? NSDictionary
        let cell = tableView .dequeueReusableCell(withIdentifier: "UserFeedbacksTableViewCell") as? UserFeedbacksTableViewCell
        cell?.feedbackDetails = feedback
        return cell!
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let velocity = scrollView.panGestureRecognizer.velocity(in: scrollView).y
        if velocity < 0 && scrollView.contentOffset.y > scrollView.contentSize.height - scrollView.bounds.size.height {
            if ((paginationController?.reachedLastPage())! as Bool) == false{
                if paginationController?.requestStatus == RequestStatus.requestStatusNone {
                    if scrollView == tableView && paginationController?.results.count > 0 {
                        fetchNextData()
                    }else{
                        fetchFirstPage()
                    }
                }
            }
        }
    }
    
    func fetchNextData(){
        if ((paginationController?.reachedLastPage())! as Bool) == false{
            if paginationController?.requestStatus == RequestStatus.requestStatusNone {
                if isInternetConnectivityAvailable(false) {
                    paginationController?.fetchNextPage()
                }
            }
        }
        tableView?.reloadData()
    }
    
    func fetchFirstPage(){
        paginationController?.reset()
        if isInternetConnectivityAvailable(false) {
            paginationController?.fetchFirstPage()
        }
        tableView?.reloadData()
    }
    
    //MARK: - other functions
    
    //section 1
    
    @IBAction func onClickOfProfilePicture(_ sender:Any){
        performAnimatedClickEffectType1(profilePictureImageView)
    }
    
    //section 3
    
    @IBAction func onClickOfSelectHomeLocation(_ sender:Any){
        if homeLocationCoordinate != nil{
            Acf().showLocationOnPopupMapScreen(homeLocationCoordinate, address: homeLocationLabel.text)
        }
    }
    
    @IBAction func onClickOfSelectDestinationLocation(_ sender:Any){
        if destinationLocationCoordinate != nil{
            Acf().showLocationOnPopupMapScreen(destinationLocationCoordinate, address: destinationLocationLabel.text)
        }
    }
    
    //section 5
    
    @IBAction func onClickOfFavouriteUnFavouriteButton(){
        if isSystemReadyToProcessThis(){
            if favouriteButton.isSelected {
                favouriteButton.deselect()
            } else {
                favouriteButton.select()
            }
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(ProfileOthersViewController.onClickOfFavouriteUnFavouriteButtonPrivate), object: nil)
            self.perform(#selector(ProfileOthersViewController.onClickOfFavouriteUnFavouriteButtonPrivate), with: nil, afterDelay: 1)
        }
    }
    
    @IBAction func onClickOfBlockUnBlockButton(){
        func showFailureError(){
            showNotification(MESSAGE_TEXT___BLOCK_UNBLOCK_NOT_READY, showOnNavigation: false, showAsError: true)
        }
        if isSystemReadyToProcessThis(){
            let isSystemReadyToPerformBlockUnBlockOperation = ServicesManager.instance().contactListService.isSystemReadyToPerformBlockUnBlockOperation()
            if isSystemReadyToPerformBlockUnBlockOperation {
                if blockButton.isSelected {
                    blockButton.isSelected = false
                    self.blockUnBlockStatusLabel.text = "Block"
                } else {
                    blockButton.isSelected = true
                    self.blockUnBlockStatusLabel.text = "Un Block"
                }
                NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(ProfileOthersViewController.onClickOfBlockUnBlockButtonPrivate), object: nil)
                self.perform(#selector(ProfileOthersViewController.onClickOfBlockUnBlockButtonPrivate), with: nil, afterDelay: 1)
            }else{
                showFailureError()
            }
        }
    }
    
    func onClickOfFavouriteUnFavouriteButtonPrivate(){
        if favouriteButton.isSelected {
            Acf().setFavourite(userInfo, isFavourite: true)
        } else {
            Acf().setFavourite(userInfo, isFavourite: false)
        }
    }
    
    func onClickOfBlockUnBlockButtonPrivate(){
        if blockButton.isSelected {
            Acf().block(userInfo, isBlock: true)
        } else {
            Acf().block(userInfo, isBlock: false)
        }
    }
    
    
    //MARK: - DZNEmptyDataSet Delegate & Data Source
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString!{
        let attributes = [NSFontAttributeName : UIFont.init(name: FONT_BOLD, size: 20)!]
        let title = "users"
        var message = "No " + title
        if (isInternetConnectivityAvailable(false)==false){
            message = "Unable to load " + title
        }
        return NSAttributedString(string: message, attributes: attributes)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString!{
        let attributes = [NSFontAttributeName : UIFont.init(name: FONT_REGULAR, size: 14)!]
        let title = "users"
        var message = "No " + title +  " available at the moment"
        if (isInternetConnectivityAvailable(false)==false){
            message = MESSAGE_TEXT___FOR_NETWORK_NOT_REACHABILITY
        }
        return NSAttributedString(string: message, attributes: attributes)
    }
        
    // MARK: - UICollection View Delegate & Datasource
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int{
        if collectionView == travelledWithCollectionView {
            return travelledWithUsers.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAtIndexPath indexPath: IndexPath) -> UICollectionViewCell{
        let cell: UsersCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "UsersCollectionViewCell", for: indexPath) as! UsersCollectionViewCell
        var userInfo : NSDictionary?
        if collectionView == travelledWithCollectionView {
            userInfo = travelledWithUsers.object(at: (indexPath as NSIndexPath).row) as? NSDictionary
            cell.userFullnameLabel?.text = userInfo!.object(forKey: "Name") as! String
            cell.userProfileImageView?.sd_setImage(with: URL(string: getUserProfilePictureUrlFromFileName(userInfo!.object(forKey: "Photo") as! String)), placeholderImage: USER_PLACEHOLDER_IMAGE)
        }
        return cell;
    }
    
    private func collectionView(_ collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: IndexPath){
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize{
        return CGSize(width: 120.0, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                               minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat{
        return 0.0;
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                               minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat{
        return 0.0;
    }
    
    func setupRequiredActions(){
        if isNotNull(self.userInfo!.object(forKey: "rs_status")){
            callButton.isEnabled = false
            let situation = Acf().getTravelRequestStatus(self.userInfo!)
            if situation == .travelRequestAccepted {
                callButton.isEnabled = true
            }else if situation == .travelCompleted {
                callButton.isEnabled = true
            }
        }else{
            callButton.isEnabled = false
        }
    }
    
    @IBAction func onClickOfCall(){
        if isSystemReadyToProcessThis(){
            performAnimatedClickEffectType1(callButton)
            ACETelPrompt.callPhoneNumber(userInfo!.object(forKey: "user_mob") as? String, call: { (duration) -> Void in }) { () -> Void in}
        }
    }
    
    @IBAction func onClickOfChat(){
        if isSystemReadyToProcessThis(){
            performAnimatedClickEffectType1(chatButton)
            if isNotNull(getUserId(self.userInfo)){
                Acf().showChatScreen(getUserId(self.userInfo))
            }
        }
    }
    
    //  MARK: - 3D Touch Support
    @available(iOS 9.0, *)
    override var previewActionItems : [UIPreviewActionItem] {
        if isNotNull(preViewingActions) {
            return preViewingActions! as! [UIPreviewActionItem]
        }
        return []
    }
}
