//
//  AuthenticationStep2ViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit
import Branch

enum ActionButtonState {
    case verify
    case timer
    case reSendOTP
}

class AuthenticationStep2ViewController: UIViewController,UITextFieldDelegate {
    
    //MARK: - variables and constants
    @IBOutlet fileprivate weak var infoLabel: UILabel!
    @IBOutlet fileprivate weak var otpTextfield: MKTextField!
    @IBOutlet fileprivate weak var actionInfoLabel: UILabel!
    @IBOutlet fileprivate weak var actionButton: TKTransitionSubmitButton!
    
    var phoneNumber = ""
    var sendOTPOnStart = false
    fileprivate var counter = 0
    fileprivate var otpValue: String?
    fileprivate var timerText: String?
    fileprivate var actionButtonState = ActionButtonState.timer
    var verificationMode = AS1WorkingMode.signIn
    
    //MARK: - view controller life cycle methods
    
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
        updateActionButton()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NSObject.cancelPreviousPerformRequests(withTarget: self)
    }
    
    //MARK: - other methods
    
    private func setupForNavigationBar(){
        setAppearanceForNavigationBarType2(self.navigationController)
        if self.isBackButtonRequired() {
            addNavigationBarButton(self, image: UIImage(named: "backarrowblack"), title: nil, isLeft: true,forceWhiteTint:false)
        }else{
            self.navigationItem.leftBarButtonItem = nil
            self.navigationItem.setHidesBackButton(true, animated:false)
            self.navigationController!.isNavigationBarHidden = true
        }
    }
    
    private func registerForNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(AuthenticationStep2ViewController.updateAppearanceOfTextField), name: NSNotification.Name.UITextViewTextDidBeginEditing, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AuthenticationStep2ViewController.updateAppearanceOfTextField), name: NSNotification.Name.UITextViewTextDidEndEditing, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AuthenticationStep2ViewController.updateAppearanceOfTextField), name: NSNotification.Name.UITextFieldTextDidBeginEditing, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AuthenticationStep2ViewController.updateAppearanceOfTextField), name: NSNotification.Name.UITextFieldTextDidEndEditing, object: nil)
    }
    
    private func startUpInitialisations(){
        setAppearanceForViewController(self)
        setAppearanceForMKTextField(otpTextfield)
        updateAppearanceOfTextField()
        otpTextfield.keyboardType = UIKeyboardType.numberPad
        if sendOTPOnStart{
            requestForOtp()
        }else{
            startTimer()
        }
        self.actionButton.performAppearAnimationType1()
        trackScreenLaunchEvent(SLE_PHONE_VERIFICATION)
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        infoLabel.attributedText = getAttributedTitleStringType2("OTP SENT TO \(phoneNumber)", regularText: "\(phoneNumber)", anotherText: "OTP SENT TO")
    }
    
    func onClickOfLeftBarButton(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    func canContinueForPhoneVerification()->(Bool){
        var canContinue = true
        if canContinue {
            canContinue = validateIfNull(otpTextfield.text , identifier: "OTP")
            if canContinue == false {
                otpTextfield.becomeFirstResponder()
            }
        }
        return canContinue
    }
    
    @IBAction func onClickOfActionButton(){
        if(isInternetConnectivityAvailable(true)==false){return}
        if actionButtonState == .verify {
            if canContinueForPhoneVerification(){
                actionButton.startLoadingAnimation()
                let information = NSMutableDictionary()
                copyData(self.phoneNumber , destinationDictionary: information, destinationKey: "phone", methodName: #function)
                copyData(self.otpTextfield.text! , destinationDictionary: information, destinationKey: "otp", methodName: #function)
                if let refferedBy = Branch.getInstance().getFirstReferringParams() as? NSDictionary{
                    copyData(refferedBy.object(forKey: "referralCode") , destinationDictionary: information, destinationKey: "coupon", methodName: #function)
                }
                let scm = ServerCommunicationManager()
                scm.authenticateStep2(information) { (responseData) -> () in
                    var userIdString : String?
                    if let userId = (responseData?.object(forKey: "data") as? NSDictionary)?.object(forKey: "id")  {
                        userIdString = "\(userId)"
                    }else if let userDetails = (responseData?.object(forKey: "data") as? NSDictionary)?.object(forKey: "userDetails") as? NSDictionary {
                        userIdString = Acf().getUserIdFor(userDetails)
                    }
                    if isNotNull(userIdString){
                        trackFunctionalEvent(FE_OTP_VERIFICATION, information: information,isSuccess: true)
                        updateUserProfile(userIdString!, completion: { (returnedData) in
                            if safeBool(returnedData) {
                                if isUserLoggedIn() {
                                    if self.verificationMode == .signIn {
                                        trackFunctionalEvent(FE_USER_LOGIN, information: information)
                                    }else{
                                        trackFunctionalEvent(FE_USER_SIGNUP, information: information)
                                    }
                                    checkAndReportCampaignRegisterEventIfRequired()
                                    decideAndShowNextScreen(self.navigationController!)
                                }else{
                                    if self.verificationMode == .signIn {
                                        trackFunctionalEvent(FE_USER_LOGIN, information: information,isSuccess: false)
                                    }else{
                                        trackFunctionalEvent(FE_USER_SIGNUP, information: information,isSuccess: false)
                                    }
                                }
                            }
                            self.actionButton.stopIt()
                        })
                    }
                    else{
                        trackFunctionalEvent(FE_OTP_VERIFICATION, information: information,isSuccess: false)
                        self.actionButton.stopIt()
                    }
                }
            }
        }else if actionButtonState == .timer {
            //nothing to do
        }else if actionButtonState == .reSendOTP {
            onClickOfResendOTPButton()
        }
    }
    
    func updateAppearanceOfTextField(){
        Acf().updateAppearanceOfTextFieldType1(otpTextfield)
    }
    
    func requestForOtp(){
        let isFirstTime = sendOTPOnStart
        if isInternetConnectivityAvailable(true) == false {return}
        startTimer()
        let information = ["phone":phoneNumber]
        let scm = ServerCommunicationManager()
        scm.authenticateStep1(information as NSDictionary) { (responseData) -> () in
            if let _ = responseData {
                showNotification(MESSAGE_TEXT___FOR_OTP_SENT, showOnNavigation: false, showAsError: false)
                if isFirstTime {
                    trackFunctionalEvent(FE_SEND_OTP, information: ["mobile_no":self.phoneNumber])
                }else{
                    trackFunctionalEvent(FE_RESEND_OTP, information: ["mobile_no":self.phoneNumber])
                }
            }else{
                if isFirstTime {
                    trackFunctionalEvent(FE_SEND_OTP, information: ["mobile_no":self.phoneNumber],isSuccess:false)
                }else{
                    trackFunctionalEvent(FE_RESEND_OTP, information: ["mobile_no":self.phoneNumber],isSuccess:false)
                }
            }
        }
    }
    
    @IBAction func onClickOfResendOTPButton(){
        let prompt = UIAlertController(title: MESSAGE_TITLE___RESEND_OTP_ALERT, message: MESSAGE_TEXT___RESEND_OTP_ALERT , preferredStyle: UIAlertControllerStyle.alert)
        prompt.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
        prompt.addAction(UIAlertAction(title: "ReSend", style: UIAlertActionStyle.cancel, handler: { (action) -> Void in
            self.requestForOtp()
        }))
        self.present(prompt, animated: true, completion: nil)
    }
    
    func startTimer(){
        counter = 31
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(AuthenticationStep2ViewController.updateTimerLabel), object: nil)
        self.perform(#selector(AuthenticationStep2ViewController.updateTimerLabel))
    }
    
    func updateTimerLabel(){
        counter -= 1
        if counter > 0 {
            timerText = "\(counter) Seconds"
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(AuthenticationStep2ViewController.updateTimerLabel), object: nil)
            self.perform(#selector(AuthenticationStep2ViewController.updateTimerLabel), with: nil, afterDelay: 1)
        }else{
            timerText = nil
        }
        updateActionButton()
    }
    
    func updateActionButton(){
        if otpTextfield.text?.length > 0 {
            actionInfoLabel.text = ""
            actionButton.setTitle("Verify", for: UIControlState.normal)
            actionButtonState = .verify
        }else{
            if timerText != nil {
                actionInfoLabel.text = "Please wait while we send your OTP"
                actionButton.setTitle(timerText, for: UIControlState.normal)
                actionButtonState = .timer
            }else{
                actionInfoLabel.text = "Didn't receive OTP?"
                actionButton.setTitle("Resend OTP", for: UIControlState.normal)
                actionButtonState = .reSendOTP
            }
        }
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(AuthenticationStep2ViewController.updateActionButton), object: nil)
        self.perform(#selector(AuthenticationStep2ViewController.updateActionButton), with: nil, afterDelay: 0.5)
    }
}
