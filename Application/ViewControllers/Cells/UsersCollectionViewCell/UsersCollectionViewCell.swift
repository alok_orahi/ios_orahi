//
//  UsersCollectionViewCell.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit

class UsersCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var userProfileImageView: UIImageView!
    @IBOutlet weak var userFullnameLabel: UILabel!
    @IBOutlet weak var rideStatusLabel: UILabel!
    
    fileprivate var isInitialisedOnce = false
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateUserInterfaceOnScreen()
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        DispatchQueue.main.async { () -> Void in
            setBorder(self.userProfileImageView, color: APP_THEME_LIGHT_GRAY_COLOR, width: 1, cornerRadius: self.userProfileImageView.bounds.size.width/2)
        }
    }
    
}
