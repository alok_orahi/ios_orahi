//
//  RankingOtherUsersTableViewCell.swift
//  Orahi
//
//  Created by Alok Singh on 10/02/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit

class RankingOtherUsersTableViewCell : UITableViewCell {
    
    //MARK: - variables and constants
    
    @IBOutlet weak var userProfileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var rankLabel: UILabel!
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var ridesLabel: UILabel!
    
    var userDetailsToShow = NSDictionary()
    var isInitialisedOnce = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        startupInitialisations()
        updateUserInterfaceOnScreen()
    }
    
    func startupInitialisations(){
        self.contentView.layoutIfNeeded()
        if isInitialisedOnce == false {
            setBorder(self.userProfileImageView, color: UIColor.clear, width: 0, cornerRadius: self.userProfileImageView.bounds.size.width/2)
            self.selectionStyle = UITableViewCellSelectionStyle.none
        }
        isInitialisedOnce = true
    }
    
    func updateUserInterfaceOnScreen(){
        rankLabel.text = safeString(userDetailsToShow.object(forKey: "rank"),alternate: "--")
        nameLabel.text = safeString(userDetailsToShow.object(forKey: "user_name"),alternate: "--")
        userProfileImageView.sd_setImage(with: URL(string: getUserProfilePictureUrlFromFileName(safeString(userDetailsToShow.object(forKey: "user_photo")))), placeholderImage: USER_PLACEHOLDER_IMAGE)
        levelLabel.text = safeString(userDetailsToShow.object(forKey: "level"),alternate: "--")
        ridesLabel.text = safeString(userDetailsToShow.object(forKey: "rides_shared"),alternate: "--")
    }
    
    static func getRequiredHeight()->(CGFloat){
        return 64
    }
}
