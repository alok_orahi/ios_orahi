//
//  NotificationsTableViewCell.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import Foundation
import UIKit

class NotificationsTableViewCell : UITableViewCell {
    //MARK: - variables and constants
    
    fileprivate var isInitialisedOnce = false
    var notification : Notification?
    weak var parentVC : UIViewController?
    
    @IBOutlet fileprivate weak var pictureImageView : UIImageView!
    @IBOutlet fileprivate weak var notificationTitleLabel : UILabel?
    @IBOutlet fileprivate weak var notificationLabel : UILabel?
    @IBOutlet fileprivate weak var notificationDateLabel : UILabel?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        startUpInitialisations()
        updateUserInterfaceOnScreen()
    }
    
    private func startUpInitialisations(){
        self.contentView.layoutIfNeeded()
        if isInitialisedOnce == false {
            self.selectionStyle = UITableViewCellSelectionStyle.none
        }
        isInitialisedOnce = true
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        func getFormattedDateAndTime(date:Date)->String{
            var timeAndDateString = ""
            if date.isToday(){
                timeAndDateString = "Today, \(date.toStringValue("h:mm a"))"
            }else if date.isTomorrow(){
                timeAndDateString = "Tomorrow, \(date.toStringValue("h:mm a"))"
            }else{
                timeAndDateString = "\(date.toStringValue("dd MMM")) \(date.toStringValue("h:mm a"))"
            }
            return timeAndDateString
        }
        notificationTitleLabel?.text = safeString(notification?.title)
        notificationLabel?.text = safeString(notification?.message)
        let date =  Date(timeIntervalSince1970: safeDouble(notification!.timestamp))
        notificationDateLabel?.text = getFormattedDateAndTime(date:date)
        
        let notiType = safeString(notification?.notificationType)
        
        if notiType == "sos" {
            setBorder(self.pictureImageView!, color: UIColor.clear, width: 0, cornerRadius: (self.pictureImageView?.bounds.size.width)!/2)
            pictureImageView?.image = UIImage(named: "sosButton")
        }else{
            setBorder(self.pictureImageView!, color: APP_THEME_DARK_GRAY_COLOR, width: 1.5, cornerRadius: (self.pictureImageView?.bounds.size.width)!/2)
            pictureImageView?.sd_setImage(with: URL(string: getUserProfilePictureUrlFromFileName(notification!.associatedUserPicture)), placeholderImage: UIImage(named: "orahiLogoWheel"))
        }

        if notification!.read == false {
            notificationLabel?.textColor = APP_THEME_VOILET_COLOR
        }else{
            notificationLabel?.textColor = APP_THEME_LIGHT_GRAY_COLOR
        }
        
        notificationLabel?.setAttributedTextWithOptimisationsType1()
    }
}
