//
//  PlacesControlTableViewCell.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import Foundation
import UIKit

public class PlacesControlTableViewCell : UITableViewCell {
    
    @IBOutlet weak var lineViewVerticalTop : UIView!
    @IBOutlet weak var lineViewVerticalBottom : UIView!
    @IBOutlet weak var containerView : UIView!
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var lineViewHorizontal : UIView!
    @IBOutlet weak var removeLocationButton : UIButton!
    @IBOutlet weak var removeLocationButtonInteractive : UIButton!
    @IBOutlet weak var addressLabel : UILabel!
    @IBOutlet weak var editButton : UIButton!
    @IBOutlet weak var infoButton : UIButton!
    @IBOutlet weak var entryExitSectionInformationLabel : UILabel!
    @IBOutlet weak var entryLabel : UILabel!
    @IBOutlet weak var entrySwitch : UISwitch!
    @IBOutlet weak var exitLabel : UILabel!
    @IBOutlet weak var exitSwitch : UISwitch!

    var themeColor = ThemeColor.colorType1
    var group : Group?
    var associatedPlace : AssociatedPlaces?
    var actualPlace : Place?
    var navigationController : UINavigationController?

    fileprivate var isInitialisedOnce = false
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        startUpInitialisations()
        updateUserInterfaceOnScreen()
    }
    
    private func startUpInitialisations(){
        self.contentView.layoutIfNeeded()
        if isInitialisedOnce == false {
            self.selectionStyle = UITableViewCellSelectionStyle.none
            self.containerView.layer.cornerRadius = 5.0
            self.containerView.layer.borderWidth = 0.5
            self.containerView.layer.masksToBounds = true
            self.containerView.layer.cornerRadius = 7.0
            self.removeLocationButton.layer.borderColor = UIColorHex(0xDDDDDD).cgColor
            self.removeLocationButton.layer.borderWidth = 0.5
            self.removeLocationButton.layer.masksToBounds = true
            self.removeLocationButton.tintColor = UIColorHex(0xDDDDDD)
            self.removeLocationButton.layer.cornerRadius = self.removeLocationButton.bounds.size.height/2
            self.entrySwitch?.transform = CGAffineTransform(scaleX: SWITCH_SCALE, y: SWITCH_SCALE)
            self.exitSwitch?.transform = CGAffineTransform(scaleX: SWITCH_SCALE, y: SWITCH_SCALE)
        }
        isInitialisedOnce = true
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        themeColor = ThemeColor().getColor(type: safeString(group?.themeColor))
        self.containerView.layer.borderColor = themeColor.cgColor
        self.lineViewVerticalTop.backgroundColor = themeColor
        self.lineViewVerticalBottom.backgroundColor = themeColor
        self.entrySwitch?.onTintColor = themeColor
        self.exitSwitch?.onTintColor = themeColor
        self.entryLabel?.textColor = themeColor
        self.exitLabel?.textColor = themeColor
        self.entrySwitch?.onTintColor = themeColor
        self.exitSwitch?.onTintColor = themeColor
        self.nameLabel.text = actualPlace?.name
        self.addressLabel.text = actualPlace?.placeAddress
        self.entryExitSectionInformationLabel.text = "Send entry/exit alerts to \(safeString(group?.name))"
        self.entrySwitch.isOn = safeBool(self.associatedPlace?.entrySharingEnabled)
        self.exitSwitch.isOn = safeBool(self.associatedPlace?.exitSharingEnabled)
        self.editButton.isHidden = (!safeBool(self.associatedPlace?.isAdmin))
        self.removeLocationButton.isHidden = (!safeBool(self.associatedPlace?.isAdmin))
        self.removeLocationButtonInteractive.isHidden = (!safeBool(self.associatedPlace?.isAdmin))
        updateEntryExitSwitchColors()
    }
    
    func updateEntryExitSwitchColors(){
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(PlacesControlTableViewCell.updateEntryExitSwitchColorsPrivate), object: nil)
        self.perform(#selector(PlacesControlTableViewCell.updateEntryExitSwitchColorsPrivate), with: nil, afterDelay: 0.2)
    }
    
    func updateEntryExitSwitchColorsPrivate(){
        if self.entrySwitch.isOn {
            self.entryLabel?.textColor = themeColor
        }else{
            self.entryLabel?.textColor = APP_THEME_DARK_GRAY_COLOR
        }
        if self.exitSwitch.isOn {
            self.exitLabel?.textColor = themeColor
        }else{
            self.exitLabel?.textColor = APP_THEME_DARK_GRAY_COLOR
        }
    }
    
    @IBAction fileprivate func entrySwitchValueChanged(){
        if(isInternetConnectivityAvailable(true)==false){return}
        execMain({
            let toChange = self.entrySwitch.isOn
            if toChange == false {
                self.entrySwitch.isOn = !self.entrySwitch.isOn
                let title = "DISABLE"
                let message = "\nDo you want to disable ENTRY notifications for this place?"
                let prompt = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
                prompt.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler: nil))
                prompt.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { (action) -> Void in
                    self.entrySwitch.isOn = toChange
                    self.associatedPlace?.entrySharingEnabled = toChange
                    Dbm().saveChanges()
                    self.updateEntryExitConfigurationsOnServer()
                    self.updateEntryExitSwitchColors()
                }))
                Acf().navigationController!.present(prompt, animated: true, completion: nil)
            }else{
                self.associatedPlace?.entrySharingEnabled = toChange
                Dbm().saveChanges()
                self.updateEntryExitConfigurationsOnServer()
                self.updateEntryExitSwitchColors()
            }
        }, delay: 0.3)
    }
    
    @IBAction fileprivate func exitSwitchValueChanged(){
        if(isInternetConnectivityAvailable(true)==false){return}
        execMain({
            let toChange = self.exitSwitch.isOn
            if toChange == false {
                self.exitSwitch.isOn = !self.exitSwitch.isOn
                let title = "DISABLE"
                let message = "\nDo you want to disable EXIT notifications for this place?"
                let prompt = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
                prompt.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler: nil))
                prompt.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { (action) -> Void in
                    self.exitSwitch.isOn = toChange
                    self.associatedPlace?.exitSharingEnabled = toChange
                    Dbm().saveChanges()
                    self.updateEntryExitConfigurationsOnServer()
                    self.updateEntryExitSwitchColors()
                }))
                Acf().navigationController!.present(prompt, animated: true, completion: nil)
            }else{
                self.associatedPlace?.exitSharingEnabled = toChange
                Dbm().saveChanges()
                self.updateEntryExitConfigurationsOnServer()
                self.updateEntryExitSwitchColors()
            }
        }, delay: 0.3)
    }
    
    func updateEntryExitConfigurationsOnServer(){
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(PlacesControlTableViewCell.updateEntryExitConfigurationsOnServerPrivate), object: nil)
        self.perform(#selector(PlacesControlTableViewCell.updateEntryExitConfigurationsOnServerPrivate), with: nil, afterDelay: 2.0)
    }
    
    func updateEntryExitConfigurationsOnServerPrivate(){
        if self.group != nil {
            updateEntryExitConfigurationToServer(group: self.group!, associatedPlace: self.associatedPlace!)
        }
    }
    
    @IBAction fileprivate func onClickOfRemoveLocation(){
        let alertController = UIAlertController(title: "CONFIRM!", message: "\nAre you sure you want to remove this place?", preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
        alertController.addAction(UIAlertAction(title: "Remove", style: UIAlertActionStyle.destructive, handler: { (action) -> Void in
            self.removeThisLocationFromGroup()
        }))
        self.navigationController?.present(alertController, animated: true, completion: nil)
    }

    func removeThisLocationFromGroup(){
        if(isInternetConnectivityAvailable(true)==false){return}
        let information = NSMutableDictionary()
        copyData(group?.groupId , destinationDictionary: information, destinationKey: "groupId", methodName: #function)
        copyData(associatedPlace?.placeId , destinationDictionary: information, destinationKey: "placeId", methodName: #function)
        copyData("unassociate" , destinationDictionary: information, destinationKey: "actionType", methodName: #function)
        let scm = ServerCommunicationManager()
        scm.responseErrorOption = ResponseErrorOption.dontShowErrorResponseMessage
        scm.managePlace(information) { (responseData) -> () in
            if let _ = responseData {
                //remove place manually from db
                self.group?.removeFromAssociatedPlaces(self.associatedPlace!)
                Dbm().saveChanges()
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NOTIFICATION_NEED_PLACES_UPDATE), object: nil)
                
                //sync group,places,members also in background
                Acf().syncGroupsAndPlaces(completion: {
                    Lem().doRequiredProcessing()
                })
            }
        }
    }
    
    @IBAction fileprivate func onClickOfEditLocation(){
        Acf().pushVC("AddEditPlaceViewController", navigationController: navigationController, isRootViewController: false, animated: true) { (viewController) in
            (viewController as! AddEditPlaceViewController).workMode = .editPlace
            (viewController as! AddEditPlaceViewController).placeToEdit = self.actualPlace
            (viewController as! AddEditPlaceViewController).groupAssociatedWithPlace = self.group
        }
    }

    @IBAction fileprivate func onClickOfInfoButton(){
        showAlert(MESSAGE_TITLE___POM_DETAILS, message: MESSAGE_TEXT___POM_DETAILS)
    }
}
