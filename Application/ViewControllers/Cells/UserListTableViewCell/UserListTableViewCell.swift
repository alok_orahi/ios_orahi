//
//  UserListTableViewCell.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import Foundation
import UIKit

class UserListTableViewCell : UITableViewCell {
    fileprivate var isInitialisedOnce = false
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var priorityLabel : UILabel!
    @IBOutlet fileprivate weak var descriptionLabel : UILabel!
    @IBOutlet fileprivate weak var pictureImageView : UIImageView!
    @IBOutlet fileprivate weak var pictureBgView : UIView!
    
    var userInfo : Favourites?
    var userInfoAsDictionary : NSDictionary?
    var userInfoAsQBUUser : QBUUser?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        startUpInitialisations()
        updateUserInterfaceOnScreen()
    }
    
    private func startUpInitialisations(){
        self.contentView.layoutIfNeeded()
        if isInitialisedOnce == false {
            self.selectionStyle = UITableViewCellSelectionStyle.none
            setBorder(self.pictureImageView!, color: UIColor.white, width: 2, cornerRadius: (self.pictureImageView?.bounds.size.width)!/2)
            self.pictureBgView?.makeMeRound()
        }
        isInitialisedOnce = true
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        if isNotNull(userInfo)&&isNotNull(userInfo!.userId){
            if userInfo!.isDeleted == false {
                titleLabel?.text = userInfo!.name
                descriptionLabel?.text = getStatusTextFor(["userId":userInfo!.userId!])
                pictureImageView?.sd_setImage(with: URL(string: getUserProfilePictureUrlFromFileName(userInfo!.picture)), placeholderImage: USER_PLACEHOLDER_IMAGE)
            }
        }else if isNotNull(userInfoAsDictionary){
            titleLabel?.text = userInfoAsDictionary!.object(forKey: "user_name") as? String
            descriptionLabel?.text = getStatusTextFor(userInfoAsDictionary)
            pictureImageView?.sd_setImage(with: URL(string: getUserProfilePictureUrlFromFileName(userInfoAsDictionary!.object(forKey: "user_photo") as? String)), placeholderImage: USER_PLACEHOLDER_IMAGE)
        }else if isNotNull(userInfoAsQBUUser){
            titleLabel?.text = nameToDisplay(userInfoAsQBUUser,chatDialogue:nil,onlyFirstName: false)
            descriptionLabel?.text = getStatusTextFor(["userId":Acf().getUserIdFrom(userInfoAsQBUUser!.login!)])
            if isNotNull(userInfoAsQBUUser!.website){
                pictureImageView?.sd_setImage(with: URL(string: userInfoAsQBUUser!.website!), placeholderImage: USER_PLACEHOLDER_IMAGE)
            }else{
                pictureImageView?.image = USER_PLACEHOLDER_IMAGE
            }
        }
    }
}
