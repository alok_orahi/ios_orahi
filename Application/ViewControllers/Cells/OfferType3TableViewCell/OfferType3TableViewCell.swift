//
//  OfferType3TableViewCell.swift
//  Orahi
//
//  Created by Alok Singh on 10/02/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import Foundation
import UIKit

class OfferType3TableViewCell : UITableViewCell {
    var isInitialisedOnce = false
    
    @IBOutlet weak var offerImageView : UIImageView!

    var offer : NSDictionary?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        startupInitialisations()
        updateUserInterfaceOnScreen()
    }
    func startupInitialisations(){
        self.contentView.layoutIfNeeded()
        if isInitialisedOnce == false {
            self.selectionStyle = UITableViewCellSelectionStyle.none
        }
        isInitialisedOnce = true
    }
    
    func updateUserInterfaceOnScreen(){
        if let offerImage = offer?.object(forKey: "offer_image") as? String{
            offerImageView.sd_setImage(with: getOfferImageUrlFromFileName(offerImage).asNSURL())
        }else{
            offerImageView.image = nil
        }
    }
    
    static func getRequiredHeight(_ offer:NSDictionary)->(CGFloat){
        if let offerImageName = offer.object(forKey: "offer_image") as? String{
            let offerImageUrl = getOfferImageUrlFromFileName(offerImageName)
            let imageFromCache = SDWebImageManager.shared().imageCache.imageFromDiskCache(forKey: SDWebImageManager.shared().cacheKey(for: offerImageUrl.asNSURL()))
            if imageFromCache != nil {
                let height = imageFromCache?.aspectHeightForWidth(UIScreen.main.bounds.width)
                if Int(height!) > 147 {
                    return height!
                }
            }
        }
        return CGFloat(147.0)
    }
}
