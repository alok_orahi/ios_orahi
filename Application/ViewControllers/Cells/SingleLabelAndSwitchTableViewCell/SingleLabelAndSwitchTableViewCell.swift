//
//  SingleLabelAndSwitchTableViewCell.swift
//  
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//
//

import Foundation
import UIKit

class SingleLabelAndSwitchTableViewCell : UITableViewCell {
    fileprivate var isInitialisedOnce = false
    @IBOutlet weak var titleLabel : UILabel?
    @IBOutlet weak var controlSwitch : UISwitch?
    @IBOutlet weak var seperatorView : UIView?
    override func layoutSubviews() {
        super.layoutSubviews()
        startUpInitialisations()
        updateUserInterfaceOnScreen()
    }
    private func startUpInitialisations(){
        self.contentView.layoutIfNeeded()
        if isInitialisedOnce == false {
            self.selectionStyle = UITableViewCellSelectionStyle.none
        }
        isInitialisedOnce = true
    }
    @objc private func updateUserInterfaceOnScreen(){
    }
}
