//
//  GroupSuggestionCollectionViewCell.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit

class GroupSuggestionCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet fileprivate weak var backgroundViewLevel1: UIView!
    @IBOutlet fileprivate weak var backgroundViewLevel2: UIView!
    @IBOutlet fileprivate weak var informationLabel1: UILabel!
    @IBOutlet fileprivate weak var actionRepresentingIconImageView: UIImageView!
    @IBOutlet fileprivate weak var actionButton: TKTransitionSubmitButton!
    
    var userInfo:[String:Any]?
    var group : Group?
    var navigationController : UINavigationController?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateUserInterfaceOnScreen()
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        let borderColor = UIColorHex(0x707171)
        backgroundViewLevel1.layer.masksToBounds = true
        backgroundViewLevel1.layer.borderColor = borderColor.cgColor
        backgroundViewLevel1.layer.borderWidth = 0.5
        backgroundViewLevel1.layer.masksToBounds = false

        backgroundViewLevel2.layer.masksToBounds = true
        backgroundViewLevel2.layer.borderColor = borderColor.cgColor
        backgroundViewLevel2.layer.borderWidth = 0.5
        backgroundViewLevel2.layer.masksToBounds = true

        actionButton.layer.masksToBounds = true
        
        informationLabel1.text = safeString(userInfo?["info1"])
        actionButton.setBackgroundColor(userInfo!["themeColor"] as! UIColor, forState: .normal)
        actionButton.setBackgroundColor(userInfo!["themeColor"] as! UIColor, forState: .highlighted)
        actionButton.setTitle( safeString(userInfo?["actionTitle"]), for: UIControlState.normal)
        actionRepresentingIconImageView.image = UIImage(named: safeString(userInfo?["actionIcon"]))
    }
    
    @IBAction fileprivate func onClickOfActionButton(){
        Acf().pushVC("AddEditPlaceViewController", navigationController: self.navigationController, isRootViewController: false, animated: true, modifyObject: { (viewController) in
            (viewController as! AddEditPlaceViewController).workMode = .createNewPlace
            (viewController as! AddEditPlaceViewController).groupAssociatedWithPlace = self.group
            let userInfo = Dbm().getUserInfo()
            if safeString(self.userInfo?["actionTag"]) == "addOffice"{
                (viewController as! AddEditPlaceViewController).preFilledName = "Office-\(safeString(userInfo?.name?.firstName()))"
            }else if safeString(self.userInfo?["actionTag"]) == "addHome"{
                (viewController as! AddEditPlaceViewController).preFilledName = "Home-\(safeString(userInfo?.name?.firstName()))"
            }else if safeString(self.userInfo?["actionTag"]) == "addSchool"{
                (viewController as! AddEditPlaceViewController).preFilledName = "School-\(safeString(userInfo?.name?.firstName()))"
            }else if safeString(self.userInfo?["actionTag"]) == "addGym"{
                (viewController as! AddEditPlaceViewController).preFilledName = "Gym-\(safeString(userInfo?.name?.firstName()))"
            }
        })
    }
}
