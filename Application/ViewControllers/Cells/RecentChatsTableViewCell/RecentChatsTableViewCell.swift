//
//  RecentChatsTableViewCell.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit

class RecentChatsTableViewCell: UITableViewCell{
    
    //MARK: - variables and constants
    
    @IBOutlet fileprivate weak var dialogLastMessage: UILabel!
    @IBOutlet fileprivate weak var dialogLastMessageTime: UILabel!
    @IBOutlet fileprivate weak var dialogName: UILabel!
    @IBOutlet fileprivate weak var dialogImage: UIImageView!
    @IBOutlet fileprivate weak var unreadMessageCounterLabel: UILabel!
    
    fileprivate var isInitialisedOnce = false
    var chatDialog : QBChatDialog?
    var shouldOptimise = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        startUpInitialisations()
        updateUserInterfaceOnScreen()
    }
    
    private func startUpInitialisations(){
        self.contentView.layoutIfNeeded()
        if isInitialisedOnce == false {
            setBorder(self.dialogImage, color: APP_THEME_LIGHT_GRAY_COLOR, width: 0.5, cornerRadius: self.dialogImage.bounds.size.width/2)
            setBorder(self.unreadMessageCounterLabel, color: UIColor.clear, width: 0, cornerRadius: self.unreadMessageCounterLabel.bounds.size.width/2)
            isInitialisedOnce = true
        }
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        if chatDialog!.type == .private {
            if chatDialog!.recipientID > 0 {
                dialogName?.text = nameToDisplay(ServicesManager.instance().usersService.usersMemoryStorage.user(withID: UInt(chatDialog!.recipientID)),chatDialogue: chatDialog,onlyFirstName: false)
            }else{
                dialogName?.text = displayNameFromName(chatDialog!.name)
            }
        }else{
            dialogName?.text = displayNameFromName(chatDialog!.name)
        }
        dialogLastMessage.text = chatDialog!.lastMessageText
        if isNull(dialogLastMessage.text) {
            dialogLastMessage.text = ""
        }
        if shouldOptimise {
            dialogImage.image = USER_PLACEHOLDER_IMAGE
            dialogLastMessageTime.layer.opacity = 0.2
            unreadMessageCounterLabel.isHidden = true
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(RecentChatsTableViewCell.updateOtherDetails), object: nil)
            self.perform(#selector(RecentChatsTableViewCell.updateOtherDetails), with: nil, afterDelay: 0.3)
        }else{
            updateOtherDetails()
        }
    }
    
    func updateOtherDetails() {
        if isNotNull(chatDialog!.lastMessageDate){
            dialogLastMessageTime.text = chatDialog!.lastMessageDate!.since().capitalized
        }else{
            dialogLastMessageTime.text = nil
        }
        if (chatDialog!.unreadMessagesCount > 0) {
            var trimmedUnreadMessageCount : String
            if chatDialog!.unreadMessagesCount > 99 {
                trimmedUnreadMessageCount = "99+"
            } else {
                trimmedUnreadMessageCount = String(format: "%d", chatDialog!.unreadMessagesCount)
            }
            unreadMessageCounterLabel.text = trimmedUnreadMessageCount
            unreadMessageCounterLabel.isHidden = false
        } else {
            unreadMessageCounterLabel.text = nil
            unreadMessageCounterLabel.isHidden = true
        }
        Acf().setImageWithPresence(dialogImage, chatDialog: chatDialog)
        dialogLastMessageTime.layer.opacity = 1.0
    }
}
