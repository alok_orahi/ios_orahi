//
//  SuggestedUsersTableViewCell.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit

class SuggestedUsersTableViewCell: UITableViewCell{
    
    //MARK: - variables and constants
    
    @IBOutlet fileprivate weak var statusTextLabel: UILabel!
    @IBOutlet fileprivate weak var nameLabel: UILabel!
    @IBOutlet fileprivate weak var pictureImageView: UIImageView!
    
    fileprivate var isInitialisedOnce = false
    var userInfo : NSDictionary?
    var shouldOptimise = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        startUpInitialisations()
        updateUserInterfaceOnScreen()
    }
    
    private func startUpInitialisations(){
        self.contentView.layoutIfNeeded()
        if isInitialisedOnce == false {
            setBorder(self.pictureImageView, color: APP_THEME_LIGHT_GRAY_COLOR, width: 0.5, cornerRadius: self.pictureImageView.bounds.size.width/2)
            isInitialisedOnce = true
        }
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        nameLabel.text = (userInfo!.object(forKey: "user_name") as! String)
        if shouldOptimise {
            pictureImageView.image = USER_PLACEHOLDER_IMAGE
            statusTextLabel.layer.opacity = 0.2
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(SuggestedUsersTableViewCell.updateOtherDetails), object: nil)
            self.perform(#selector(SuggestedUsersTableViewCell.updateOtherDetails), with: nil, afterDelay: 0.3)
        }else{
            updateOtherDetails()
        }
    }
    
    func updateOtherDetails() {
        let userLogin = Acf().getQuickBloxUserName(getUserId(userInfo))
        let users = ServicesManager.instance().usersService.usersMemoryStorage.users(withLogins: [userLogin])
        let user : QBUUser?
        if users.count > 0 {
            user = users[0]
        }else{
            user = nil
        }
        Acf().setImageWithPresence(pictureImageView, user: user)
        statusTextLabel.text = getStatusTextFor(userInfo!)
        statusTextLabel.layer.opacity = 1.0
    }
}
