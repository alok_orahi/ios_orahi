//
//  SingleLabelTableViewHeader.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import Foundation
import UIKit

protocol CollapsibleTableViewHeaderDelegate {
    func toggleSection(_ header: SingleLabelTableViewHeader, section: Int)
}

class SingleLabelTableViewHeader : UITableViewHeaderFooterView {
    
    @IBOutlet weak var titleLabel : UILabel?
    @IBOutlet weak var badgeLabel : UILabel?
    @IBOutlet weak var badgeLabelLeadingSpaceToSuperviewConstraint : NSLayoutConstraint?
    @IBOutlet fileprivate weak var indicatorImageView : UIImageView?
    @IBOutlet weak var separatorView : UIView?

    var delegate: CollapsibleTableViewHeaderDelegate?
    var section: Int = 0
    fileprivate var isInitialisedOnce = false
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        startUpInitialisations()
        updateUserInterfaceOnScreen()
    }
    
    private func startUpInitialisations(){
        self.contentView.layoutIfNeeded()
        if isInitialisedOnce == false {
            addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(SingleLabelTableViewHeader.tapHeader(_:))))
        }
        isInitialisedOnce = true
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        badgeLabel?.layer.cornerRadius = CGFloat(safeInt(badgeLabel?.bounds.size.height)/2)
        badgeLabel?.layer.masksToBounds = true
    }
    
    func tapHeader(_ gestureRecognizer: UITapGestureRecognizer) {
        guard let cell = gestureRecognizer.view as? SingleLabelTableViewHeader else { return }
        delegate?.toggleSection(self, section: cell.section)
    }
    
    func setCollapsed(_ collapsed: Bool) {
        UIView.animate(withDuration: 0.2) {
            if collapsed {
                self.indicatorImageView?.transform = .identity
            }else{
                self.indicatorImageView?.transform = .identity
                self.indicatorImageView?.transform = (self.indicatorImageView?.transform.rotated(by: CGFloat(M_PI/2)))!
            }
        }
    }
}
