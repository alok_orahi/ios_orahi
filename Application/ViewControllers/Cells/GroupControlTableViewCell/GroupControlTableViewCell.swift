//
//  GroupControlTableViewCell.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import Foundation
import UIKit

public class GroupControlTableViewCell : UITableViewCell {
    
    @IBOutlet weak var lineViewVertical : UIView!
    @IBOutlet weak var lineViewHorizontal : UIView!
    @IBOutlet weak var pictureImageView : UIImageView!
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var locationLabel : UILabel!
    @IBOutlet weak var locationSharingSwitch : UISwitch!
    
    var themeColor = ThemeColor.colorType1
    var group : Group?
    var member : Member?

    fileprivate var isInitialisedOnce = false
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        startUpInitialisations()
        updateUserInterfaceOnScreen()
    }
    
    private func startUpInitialisations(){
        self.contentView.layoutIfNeeded()
        if isInitialisedOnce == false {
            self.selectionStyle = UITableViewCellSelectionStyle.none
        }
        isInitialisedOnce = true
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        themeColor = ThemeColor().getColor(type: safeString(group?.themeColor))
        self.pictureImageView.makeMeRoundWith(borderColor: themeColor, width: 1.0)
        self.locationSharingSwitch.onTintColor = themeColor
        self.lineViewVertical.backgroundColor = themeColor
        self.nameLabel.text = safeString(group?.name).uppercased()
        self.pictureImageView.sd_setImage(with: getGroupPictureUrlFromFileName(safeString(group?.picture)).asNSURL(), placeholderImage: POM_GROUP_PLACEHOLDER_IMAGE)
        self.locationSharingSwitch.isOn = safeBool(group?.locationSharingEnabled)
        self.locationSharingSwitch?.transform = CGAffineTransform(scaleX: SWITCH_SCALE, y: SWITCH_SCALE)
        self.lineViewVertical.isHidden = (safeInt(self.group?.associatedPlaces?.count) == 0)
    }
    
    @IBAction fileprivate func locationSharingSwitchValueChanged(){
        execMain({
            let toChange = self.locationSharingSwitch.isOn
            if toChange == false {
                self.locationSharingSwitch.isOn = !self.locationSharingSwitch.isOn
                let title = "DISABLE"
                let message = "\nDo you want to disable location sharing for \(safeString(self.group?.name).uppercased())"
                let prompt = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
                prompt.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler: nil))
                prompt.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { (action) -> Void in
                    self.locationSharingSwitch.isOn = toChange
                    self.group?.locationSharingEnabled = toChange
                    Dbm().saveChanges()
                    self.updateGroupConfigurationOnServer()
                }))
                Acf().navigationController!.present(prompt, animated: true, completion: nil)
            }else{
                self.group?.locationSharingEnabled = toChange
                Dbm().saveChanges()
                self.updateGroupConfigurationOnServer()
            }
        }, delay: 0.3)
    }
    
    fileprivate func updateGroupConfigurationOnServer(){
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(GroupControlTableViewCell.updateGroupConfigurationOnServerPrivate), object: nil)
        self.perform(#selector(GroupControlTableViewCell.updateGroupConfigurationOnServerPrivate), with: nil, afterDelay: 2)
    }
    
    func updateGroupConfigurationOnServerPrivate(){
        if self.group != nil {
            updateLocationSharing(group: self.group!)
        }
    }
    
}
