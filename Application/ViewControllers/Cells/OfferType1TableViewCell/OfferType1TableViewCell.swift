//
//  OfferType1TableViewCell.swift
//  Orahi
//
//  Created by Alok Singh on 10/02/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import Foundation
import UIKit

class OfferType1TableViewCell : UITableViewCell {
    var isInitialisedOnce = false
    
    @IBOutlet weak var offerImageView: UIImageView!
    @IBOutlet weak var offerImageViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var offerTitleLabel : UILabel!
    @IBOutlet weak var offerDescriptionLabel : UILabel!
    @IBOutlet weak var redeemStatusLabel : UILabel!
    @IBOutlet weak var offerInstructionsLabel : UILabel!
    @IBOutlet weak var offerCodeLabel : UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var doneButton: TKTransitionSubmitButton!
    
    var offer : NSDictionary?
    var total = 0
    var redeemed = 0
    var canRedeem = 0
    var iconNamePrefix = ""
    var offerStatus = OfferStatus.notAvailable
    
    override func layoutSubviews() {
        super.layoutSubviews()
        startupInitialisations()
        updateUserInterfaceOnScreen()
    }
    func startupInitialisations(){
        self.contentView.layoutIfNeeded()
        if isInitialisedOnce == false {
            registerNib("MicroImageViewCollectionVewCell", collectionView: self.collectionView)
            self.selectionStyle = UITableViewCellSelectionStyle.none
            doneButton.layer.cornerRadius = doneButton.bounds.size.width/2
            doneButton.layer.masksToBounds = true
            doneButton.requiredBackgroundColor = UIColor(red: 100/255, green: 196/255, blue: 219/255, alpha: 1.0)
            setBorder(offerTitleLabel, color: UIColor.clear, width: 0, cornerRadius: 2)
            setBorder(offerDescriptionLabel, color: UIColor.clear, width: 0, cornerRadius: 2)
            setBorder(offerInstructionsLabel, color: UIColor.clear, width: 0, cornerRadius: 2)
            setBorder(redeemStatusLabel, color: UIColor.clear, width: 0, cornerRadius: 2)
            setBorder(offerCodeLabel, color: UIColor.clear, width: 0, cornerRadius: 2)
        }
        isInitialisedOnce = true
    }
    
    func updateUserInterfaceOnScreen(){
        
        offerStatus = Acf().getOfferStatus(offer!)
        
        offerTitleLabel.text = safeString(offer!.object(forKey: "offer_name")).uppercased()
        offerDescriptionLabel.text = safeString(offer!.object(forKey: "offer_description"))
        offerCodeLabel.text = safeString(offer!.object(forKey: "offer_code")).uppercased()
        let offerValueType = safeString(offer!.object(forKey: "offer_value_type"))
        
        total = safeInt(offer!.object(forKey: "offer_value_max"),alternate:0)
        redeemed = safeInt(offer!.value(forKeyPath: "details.redeemedValue"),alternate:0)
        canRedeem = safeInt(offer!.value(forKeyPath: "details.earnedValue"),alternate:0) - redeemed
        
        let enableShowingInstructions = false
        if enableShowingInstructions {
            offerInstructionsLabel.text = "\(redeemed) out of \(total) \(offerValueType) redeemed so far"
        }else{
            offerInstructionsLabel.text = ""
        }
        
        processLabelVisibility(offerTitleLabel)
        processLabelVisibility(offerDescriptionLabel)
        processLabelVisibility(redeemStatusLabel)
        processLabelVisibility(offerInstructionsLabel)
        
        if offerValueType.isEqual("ride"){
            iconNamePrefix = "car"
        }else{
            iconNamePrefix = "fuel"
        }
        redeemStatusLabel.text = nil
        if offerStatus == .notAvailable{
            doneButton.isSelected = true
        }else if offerStatus == .available{
            doneButton.isSelected = false
            redeemStatusLabel.text = "Redeem now"
        }else if offerStatus == .expired{
            doneButton.isSelected = true
        }else if offerStatus == .notApplicable{
            doneButton.isSelected = true
        }
        
        collectionView.reloadData()
        
        var heightRequiredByImageIfAny = 0 as CGFloat
        var needToRefreshTableViewAfterLoadingImage = false
        if let offerImageName = offer!.object(forKey: "offer_image") as? String{
            let offerImageUrl = getOfferImageUrlFromFileName(offerImageName)
            let imageFromCache = SDWebImageManager.shared().imageCache.imageFromDiskCache(forKey: SDWebImageManager.shared().cacheKey(for: offerImageUrl.asNSURL()))
            if imageFromCache != nil {
                let height = imageFromCache!.aspectHeightForWidth(UIScreen.main.bounds.width)
                heightRequiredByImageIfAny = height
            }else{
                needToRefreshTableViewAfterLoadingImage = true
            }
            offerImageView.sd_setImage(with: offerImageUrl.asNSURL(), completed: { (image, error, cacheType, url) in
                if needToRefreshTableViewAfterLoadingImage && isNotNull(image) {
                    NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_RELOAD_OFFERS), object: nil)
                }
            })
        }
        offerImageViewHeightConstraint.constant = heightRequiredByImageIfAny
    }
    
    
    // MARK: - UICollection View Delegate & Datasource
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int{
        return total
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize{
        return CGSize(width: collectionView.bounds.width/10,height: 25)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                               minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat{
        return 0.0;
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                               minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat{
        return 0.0;
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAtIndexPath indexPath: IndexPath) -> UICollectionViewCell{
        let cell: MicroImageViewCollectionVewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "MicroImageViewCollectionVewCell", for: indexPath) as! MicroImageViewCollectionVewCell
        cell.iconImageView.stopAnimation()
        var cellType:Int = 0
        if indexPath.row < redeemed{
            cell.iconImageView.image = UIImage(named:"\(iconNamePrefix)Purple")
            cellType = 0
        }else if indexPath.row >= redeemed && indexPath.row < redeemed + canRedeem {//can redeem
            cell.iconImageView.image = UIImage(named:"\(iconNamePrefix)Blue")
            cellType = 1
        }else {
            cell.iconImageView.image = UIImage(named:"\(iconNamePrefix)Gray")
            cellType = 2
        }
        execMain({[weak self] (returnedData) -> () in guard let `self` = self else { return }
            cell.iconImageView.stopAnimation()
            if cellType == 0 {//redeemed
                performAnimationEffectType2(cell.iconImageView, minS: 1.0, maS: 1.4, dur: 0.8)
            }else if cellType == 1 {//can redeem
                cell.iconImageView.pulse(toSize: 1.2, duration: 1.4, repeat: true)
            }
            },delay: 0.2)
        return cell;
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: IndexPath){
        let cell = collectionView.cellForItem(at: indexPath) as! MicroImageViewCollectionVewCell
        performAnimatedClickEffectType1(cell.iconImageView)
    }
    
    func canContinueToClaimOffer()->(Bool){
        let canContinue = true
        return canContinue
    }
    
    @IBAction func onClickOfDoneButton(){
        if(isInternetConnectivityAvailable(true)==false){return}
        if canContinueToClaimOffer(){
            doneButton.startLoadingAnimation()
            if let offerCode = (offer?.object(forKey: "offer_code") as? String){
                let information = ["by":loggedInUserId(),"offersCode":offerCode]
                let scm = ServerCommunicationManager()
                scm.showSuccessResponseMessage = true
                scm.claimOffer(information as NSDictionary) { (responseData) -> () in
                    self.doneButton.stopIt()
                    NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_RELOAD_OFFERS), object: nil)
                    if isNotNull(responseData){
                        trackFunctionalEvent(FE_OFFER_REDEEMED, information: information as? NSDictionary)
                    }else{
                        trackFunctionalEvent(FE_OFFER_REDEEMED, information: information as? NSDictionary,isSuccess: false)
                    }
                }
            }
        }
    }
    
    static func getRequiredHeight(_ offer:NSDictionary)->(CGFloat){
        var heightRequiredByImageIfAny = 0 as CGFloat
        if let offerImageName = offer.object(forKey: "offer_image") as? String{
            let offerImageUrl = getOfferImageUrlFromFileName(offerImageName)
            let imageFromCache = SDWebImageManager.shared().imageCache.imageFromDiskCache(forKey: SDWebImageManager.shared().cacheKey(for: offerImageUrl.asNSURL()))
            if imageFromCache != nil {
                let height = imageFromCache!.aspectHeightForWidth(UIScreen.main.bounds.width)
                heightRequiredByImageIfAny = height
            }
        }
        var total = 0
        if isNotNull(offer.object(forKey: "offer_value_max")){
            total = "\(offer.object(forKey: "offer_value_max")!)".toInt()
        }
        let totalHeight = ((heightRequiredByImageIfAny + 147.0) + ((CGFloat(total)/10.0) * 25.0))
        return CGFloat(totalHeight)
    }
    
    
    func processLabelVisibility(_ label:UILabel) {
        label.isHidden = true
        if let text = label.text{
            if text.length > 0{
                label.isHidden = false
            }
        }
    }

}
