//
//  GroupMemberType2TableViewCell.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import Foundation
import UIKit

public class GroupMemberType2TableViewCell : UITableViewCell {
    
    @IBOutlet weak var lineViewVertical : UIView!
    @IBOutlet weak var lineViewHorizontal : UIView!
    @IBOutlet weak var dotView : UIView!
    @IBOutlet weak var pictureImageView : UIImageView!
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var locationLabel : UILabel!
    @IBOutlet weak var lastUpdatedAtLabel : UILabel!
    @IBOutlet weak var batteryView : BatteryView!
    @IBOutlet weak var batteryPercentageLabel : UILabel!
    @IBOutlet weak var disabledLocationButton : UIButton!
    var themeColor = ThemeColor.colorType1
    var group : Group?
    var member : Member?

    fileprivate var isInitialisedOnce = false
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        startUpInitialisations()
        updateUserInterfaceOnScreen()
    }
    
    private func startUpInitialisations(){
        self.contentView.layoutIfNeeded()
        if isInitialisedOnce == false {
            self.selectionStyle = UITableViewCellSelectionStyle.none
        }
        isInitialisedOnce = true
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        themeColor = ThemeColor().getColor(type: safeString(group?.themeColor))
        self.lineViewVertical.backgroundColor = themeColor
        self.lineViewHorizontal.backgroundColor = themeColor
        self.dotView.backgroundColor = themeColor
        self.dotView.makeRound()
        self.pictureImageView.makeMeRoundWith(borderColor: themeColor, width: 0.5)
        self.nameLabel.textColor = themeColor
        self.batteryPercentageLabel.textColor = themeColor
        self.batteryView.direction = .minXEdge
        self.batteryView.highLevelColor = themeColor
        self.batteryView.lowLevelColor = APP_THEME_RED_COLOR
        self.batteryView.noLevelColor = UIColor.white
        self.disabledLocationButton.makeMeRoundWith(borderColor: APP_THEME_GRAY_COLOR, width: 0.7)

        self.nameLabel.text = safeString(member?.name).capitalized
        self.pictureImageView.sd_setImage(with: getUserProfilePictureUrlFromFileName(safeString(member?.picture)).asNSURL(), placeholderImage: USER_PLACEHOLDER_IMAGE)

        let batteryPercentage = safeInt(member?.lastKnownBatteryStatus)
        if batteryPercentage > 0 {
            self.batteryPercentageLabel.text = "\(batteryPercentage)%"
            self.batteryView.level = batteryPercentage
            self.batteryView.isHidden = false
            self.batteryPercentageLabel.isHidden = false
        }else{
            self.batteryPercentageLabel.text = ""
            self.batteryView.level = -1
            self.batteryView.isHidden = true
            self.batteryPercentageLabel.isHidden = true
        }
        
        if !safeBool(member?.locationDisplay){
            self.disabledLocationButton.isHidden = false
            self.locationLabel.text = "LOCATION DISPLAY OFF"
            self.locationLabel.font = UIFont(name: FONT_SEMI_BOLD, size: 12)
        }else{
            self.locationLabel.font = UIFont(name: FONT_REGULAR, size: 12)
            self.disabledLocationButton.isHidden = true
            self.locationLabel.text = member?.lastKnownLocationAddress
        }
        
        if safeDouble(member?.lastKnownTimeStamp) > 0 {
            self.lastUpdatedAtLabel.text = "Last updated \(Date(timeIntervalSince1970: safeDouble(member?.lastKnownTimeStamp)).timePassed())"
        }else{
            self.lastUpdatedAtLabel.text = ""
        }
    }
    
    @IBAction fileprivate func onClickOfDisabledLocationInfoButton(){
        showRequestLocationAccessPopupAndDoRequiredProcessing(group: self.group!, member: self.member!)
    }
}
