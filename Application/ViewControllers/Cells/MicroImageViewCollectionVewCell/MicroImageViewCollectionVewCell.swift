//
//  MicroImageViewCollectionVewCell.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import Foundation
import UIKit

class MicroImageViewCollectionVewCell : UICollectionViewCell {

    @IBOutlet fileprivate weak var containerView : UIView!
    @IBOutlet weak var iconImageView : UIImageView!
    @IBOutlet fileprivate weak var infoLabel : UILabel!
    
    fileprivate var circularImage = false

    override func layoutSubviews() {
        super.layoutSubviews()
        if circularImage{
            setBorder(iconImageView, color: UIColor.white, width: 0.5, cornerRadius:iconImageView.bounds.size.width/2)
            setBorder(containerView, color: UIColor.white, width: 0.0, cornerRadius:containerView.bounds.size.width/2)
        }
        if infoLabel.text?.length > 0 {
            infoLabel.isHidden = false
        }else{
            infoLabel.isHidden = true
        }
    }
}

