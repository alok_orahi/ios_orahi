//
//  InviteNewMemberTableViewCell.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import Foundation
import UIKit

public class InviteNewMemberTableViewCell : UITableViewCell {
    
    @IBOutlet fileprivate weak var lineViewVertical : UIView!
    @IBOutlet fileprivate weak var lineViewHorizontal : UIView!
    @IBOutlet fileprivate weak var dotView : UIView!
    @IBOutlet fileprivate weak var addIconImageView : UIImageView!
    @IBOutlet fileprivate weak var titleLabel : UILabel!
    @IBOutlet fileprivate weak var descriptionLabel : UILabel!
    fileprivate var themeColor = ThemeColor.colorType1
    var group : Group?

    fileprivate var isInitialisedOnce = false
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        startUpInitialisations()
        updateUserInterfaceOnScreen()
    }
    
    private func startUpInitialisations(){
        self.contentView.layoutIfNeeded()
        if isInitialisedOnce == false {
            self.selectionStyle = UITableViewCellSelectionStyle.none
        }
        isInitialisedOnce = true
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        themeColor = ThemeColor().getColor(type: safeString(group?.themeColor))
        self.lineViewVertical.backgroundColor = themeColor
        self.lineViewHorizontal.backgroundColor = themeColor
        self.dotView.backgroundColor = themeColor
        self.dotView.makeRound()
        self.addIconImageView.makeRound()
        self.addIconImageView.backgroundColor = themeColor
        self.titleLabel.textColor = themeColor
        self.descriptionLabel.text = "Add members to \(safeString(group?.name).capitalized)"
    }
}
