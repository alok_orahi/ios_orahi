//
//  GroupDetailSuggestionTableViewCell.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import Foundation
import UIKit

class GroupDetailSuggestionTableViewCell : UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource {
    @IBOutlet fileprivate weak var collectionView : UICollectionView!
    fileprivate var isInitialisedOnce = false
    var group : Group?
    var navigationController : UINavigationController?
    fileprivate var suggestions = [[String:Any]]()
    fileprivate var isFirstTime = true
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        startUpInitialisations()
        updateUserInterfaceOnScreen()
    }

    private func startUpInitialisations(){
        self.contentView.layoutIfNeeded()
        if isInitialisedOnce == false {
            self.selectionStyle = UITableViewCellSelectionStyle.none
            registerNib("GroupSuggestionCollectionViewCell", collectionView: self.collectionView)
            suggestions = [
                [
                    "info1":"Get notified when someone arrives at or leaves Office",
                    "actionTitle":"Add Office",
                    "actionTag":"addOffice",
                    "themeColor":UIColorHex(0xFDBB11),
                    "actionIcon":"AddPlaceIconOffice"
                ],
                [
                    "info1":"Get notified when someone leaves or arrives at Home",
                    "actionTitle":"Add Home",
                    "actionTag":"addHome",
                    "themeColor":UIColorHex(0x6a920e),
                    "actionIcon":"AddPlaceIconHome"
                ],
                [
                    "info1":"Get notified when someone arrives at or leaves this Place",
                    "actionTitle":"Add Place",
                    "actionTag":"addPlace",
                    "themeColor":UIColorHex(0x6F75B1),
                    "actionIcon":"AddPlaceIconGeneral"
                ],
                [
                    "info1":"Get notified when someone reaches or leaves School",
                    "actionTitle":"Add School",
                    "actionTag":"addSchool",
                    "themeColor":UIColorHex(0x5AAEEC),
                    "actionIcon":"AddPlaceIconSchool"
                ],
                [
                    "info1":"Get notified when someone arrives at or leaves Gym",
                    "actionTitle":"Add Gym",
                    "actionTag":"addGym",
                    "themeColor":UIColorHex(0x6ECBDB),
                    "actionIcon":"AddPlaceIconGym"
                ],
            ]
        }
        isInitialisedOnce = true
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        collectionView.reloadData()
        if isFirstTime {
            isFirstTime = false
            execMain({[weak self] in guard let `self` = self else { return }
                self.collectionView.scrollToItem(at: IndexPath(row: 2, section: 0), at: UICollectionViewScrollPosition.centeredHorizontally, animated: false)
                }, delay: 0.2)
        }
    }
    
    // MARK: - UICollection View Delegate & Datasource
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int{
        return suggestions.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GroupSuggestionCollectionViewCell", for: indexPath) as! GroupSuggestionCollectionViewCell
        cell.userInfo = suggestions[indexPath.row]
        cell.group = self.group
        cell.navigationController = self.navigationController
        return cell;
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize{
        let w = DEVICE_WIDTH*0.65
        return CGSize(width: w , height: w*1.6)
    }
}
