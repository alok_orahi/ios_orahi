//
//  UserMiniProfileTableViewCell.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit

class UserMiniProfileTableViewCell: UITableViewCell {
    //MARK: - variables and constants
    
    fileprivate var isInitialisedOnce = false
    weak var parentVC : UIViewController?
    var userInfo : NSDictionary?
    var rideDate : Date?
    fileprivate var leftOptionRequired = TravelRequestAvailableAction.none
    fileprivate var rightOptionRequired = TravelRequestAvailableAction.none
    fileprivate let cellSlideGestureRecogniser = DRCellSlideGestureRecognizer()
    fileprivate var processingUserId : String?

    @IBOutlet fileprivate weak var userInformationHolderView : UIView!
    @IBOutlet fileprivate weak var userStatusColorView : UIView!
    @IBOutlet fileprivate weak var profilePictureImageView : UIImageView!
    @IBOutlet fileprivate weak var ratingView : HCSStarRatingView!
    @IBOutlet fileprivate weak var ridesDoneLabel : UILabel!
    
    @IBOutlet fileprivate weak var offerContainerView : UIView!
    @IBOutlet fileprivate weak var offerViewType1 : UIView!
    @IBOutlet fileprivate weak var freeRideOfferView : UIView!
    @IBOutlet fileprivate weak var offerViewType1ValueLabel : UILabel!
    
    @IBOutlet weak var fullNameLabel : UILabel!
    @IBOutlet fileprivate weak var statusTextLabel : UILabel!
    @IBOutlet fileprivate weak var destinationNameLabel : UILabel!
    
    @IBOutlet fileprivate weak var modeLabel : UILabel!
    @IBOutlet fileprivate weak var tagLabel : UILabel!
    @IBOutlet fileprivate weak var tagBgImageView : UIImageView!
    @IBOutlet fileprivate weak var lastActiveLabel : UILabel!
    @IBOutlet fileprivate weak var travellingTime : UILabel!
    
    @IBOutlet fileprivate weak var ridingWithUserImageView1 : UIImageView!
    @IBOutlet fileprivate weak var ridingWithUserImageView2 : UIImageView!
    @IBOutlet fileprivate weak var ridingWithUserImageView3 : UIImageView!
    @IBOutlet fileprivate weak var ridingWithUserImageView4 : UIImageView!
    
    @IBOutlet weak var bottomActionButton : UIButton!
    @IBOutlet fileprivate weak var callButton : UIButton!
    @IBOutlet fileprivate weak var chatButton : UIButton!
    
    @IBOutlet fileprivate weak var processingIndicatorLabel : UILabel!
    
    @IBOutlet fileprivate weak var heightConstraintRidingWithContainerView : NSLayoutConstraint!

    override func layoutSubviews() {
        super.layoutSubviews()
        startUpInitialisations()
        updateUserInterfaceOnScreen()
        setupRequiredActions()
    }
    
    private func startUpInitialisations(){
        self.contentView.layoutIfNeeded()
        if isInitialisedOnce == false {
            self.selectionStyle = UITableViewCellSelectionStyle.none
            setBorder(profilePictureImageView, color: UIColor.clear, width: 0, cornerRadius: profilePictureImageView.bounds.size.width/2)
            setBorder(processingIndicatorLabel, color: APP_THEME_VOILET_COLOR , width: 1, cornerRadius: 1)
            userInformationHolderView.layer.masksToBounds = false
            
            ratingView.isUserInteractionEnabled = false
            
            setBorder(ridingWithUserImageView1, color: UIColor.clear, width: 0, cornerRadius: ridingWithUserImageView1.bounds.size.width/2)
            setBorder(ridingWithUserImageView2, color: UIColor.clear, width: 0, cornerRadius: ridingWithUserImageView2.bounds.size.width/2)
            setBorder(ridingWithUserImageView3, color: UIColor.clear, width: 0, cornerRadius: ridingWithUserImageView3.bounds.size.width/2)
            setBorder(ridingWithUserImageView4, color: UIColor.clear, width: 0, cornerRadius: ridingWithUserImageView4.bounds.size.width/2)            
        }
        isInitialisedOnce = true
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        updateUIElements()
    }
    
    func updateUIElements(){
        self.processingIndicatorLabel.text = ""
        self.processingIndicatorLabel.hideActivityIndicator()
        self.processingIndicatorLabel.isHidden = true

        let info = userInfo
        userStatusColorView.backgroundColor = Acf().colorForUserAsPerTravelRequestStatus(self.userInfo!)
        profilePictureImageView.sd_setImage(with: URL(string: getUserProfilePictureUrlFromFileName(info!.object(forKey: "user_photo") as? String)), placeholderImage: USER_PLACEHOLDER_IMAGE)
        if isNotNull(info!.object(forKey: "rating")){
            ratingView.value = CGFloat("\(info!.object(forKey: "rating")!)".toFloat())
        }else{
            ratingView.value = 0.0
        }
        if isNotNull(info!.object(forKey: "rides_shared")){
            ridesDoneLabel.text = "\(info!.object(forKey: "rides_shared") as! String) Rides"
        }else{
            ridesDoneLabel.text = "0 Rides"
        }
                
        fullNameLabel.text = safeString(info!.object(forKey: "user_name"))
        statusTextLabel.text = getStatusTextFor(userInfo)
        destinationNameLabel.text = safeString(info!.object(forKey: "company_name"))
        
        updateModeLabelType1(info!, modeLabel: self.modeLabel)
        
        if isNotNull(info!.object(forKey: "last_active")){
            let lastActive = (info!.object(forKey: "last_active") as! NSString)
            if lastActive.isEqual(to: "0")  {
                lastActiveLabel.text = "Active Today"
            }else{
                lastActiveLabel.text = "\(lastActive) days ago"
            }
        }else{
            lastActiveLabel.text = ""
        }
        
        travellingTime.text = getTimeFormatInAmPm(info!.object(forKey: "etd") as? String)
        
        setRidingWithDetails(info!, img1: ridingWithUserImageView1, img2: ridingWithUserImageView2, img3: ridingWithUserImageView3, img4: ridingWithUserImageView4)

        if isRidingWithDetailsExist(info){
            heightConstraintRidingWithContainerView.constant = 63
        }else{
            heightConstraintRidingWithContainerView.constant = 0
        }
        
        setUserTagDetails(info, tagImageView: self.tagBgImageView, tagLabel: self.tagLabel)
        setupOfferView()
    }
    
    func removeSwipeActions(){
        if let grs = self.gestureRecognizers {
            for gr in grs{
                self.removeGestureRecognizer(gr)
            }
        }
    }
    
    func setupRequiredActions(){
        self.superview?.viewWithTag(1212)?.removeFromSuperview()
        
        removeSwipeActions()
        
        leftOptionRequired = Acf().getLeftSwipeOption(userInfo!)
        rightOptionRequired = Acf().getRightSwipeOption(userInfo!)
        
        callButton.isEnabled = false
        
        var lra = -1
        
        if leftOptionRequired == .reject || leftOptionRequired == .cancel {
            lra = 0
        }
        
        if rightOptionRequired == .accept || rightOptionRequired == .invite || rightOptionRequired == .settle || rightOptionRequired == .feedback {
            if lra == 0 {
                lra = 2
            }else {
                lra = 1
            }
        }
        
        cellSlideGestureRecogniser.removeAllActions()
        
        var shouldAddSwipeableActions = true
        
        if let actionState = self.userInfo?.object(forKey: "actionState") as? String {
            if actionState == "success" {
                shouldAddSwipeableActions = false
            }
        }

        if shouldAddSwipeableActions {
            setAction(lra, actionLeft:
                { (tableView, indexPath) -> Void in
                    
                    NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_RESET_IDLE_TIMER_RIDE_HOME), object: nil, userInfo: nil)
                    
                    if isSystemReadyToProcessThis() {
                        if self.rightOptionRequired == .accept {
                            self.acceptInvitation()
                        }else if self.rightOptionRequired == .settle {
                            self.settlePayment()
                        }else if self.rightOptionRequired == .invite {
                            self.sendInvitation()
                        }else if self.rightOptionRequired == .feedback {
                            self.giveUserFeedback()
                        }
                    }
            })
            { (tableView, indexPath) -> Void in
                
                NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_RESET_IDLE_TIMER_RIDE_HOME), object: nil, userInfo: nil)
                
                if isSystemReadyToProcessThis(){
                    if self.leftOptionRequired == .reject {
                        self.rejectInvitation()
                    }else if self.leftOptionRequired == .cancel {
                        self.cancelRide()
                    }
                }
            }
        }
        
        let situation = Acf().getTravelRequestStatus(self.userInfo!)
        if situation == .travelRequestAccepted {
            callButton.isEnabled = true
        }else if situation == .travelRequestSent || situation == .travelRequestReceived {
            callButton.isEnabled = safeBool(self.userInfo?.object(forKey: "show_phone"))
        }else if situation == .travelCompleted {
            callButton.isEnabled = true
        }
        updateRideActionButton(bottomActionButton,userInfo: self.userInfo!)
        
        if let processingMessage = Acf().travellersListProcessingDetails.object(forKey: "\(getUserId(self.userInfo))") as? String {
            startRequest(processingMessage)
        }
    }
    
    typealias UMPTCompletionBlock = (_ tableView :UITableView?,_ indexPath :IndexPath?) ->()
    
    func setAction(_ lra:Int,actionLeft:@escaping UMPTCompletionBlock,actionRight:@escaping UMPTCompletionBlock){
        if lra == -1 {
            return
        }
        
        var attributes = Dictionary<String, AnyObject>()
        attributes[NSFontAttributeName] = UIFont.init(name: FONT_BOLD, size: 20)
        var attributedString : NSMutableAttributedString?
        
        let cellSlideActionRight = DRCellSlideAction()
        cellSlideActionRight.fraction = -0.65
        cellSlideActionRight.behavior = .pullBehavior
        cellSlideActionRight.activeBackgroundColor = UIColor.red.withAlphaComponent(0.7)
        cellSlideActionRight.inactiveBackgroundColor = UIColor.red.withAlphaComponent(0.4)
        
        
        
        let cellSlideActionLeft = DRCellSlideAction()
        cellSlideActionLeft.fraction = 0.65
        cellSlideActionLeft.behavior = .pullBehavior
        cellSlideActionLeft.activeBackgroundColor = UIColor.green.withAlphaComponent(0.7)
        cellSlideActionLeft.inactiveBackgroundColor = UIColor.green.withAlphaComponent(0.4)
        
        
        switch rightOptionRequired {
        case .invite:
            attributedString = NSMutableAttributedString(string: "Send Invite")
            break
            
        case .accept:
            attributedString = NSMutableAttributedString(string: "Yes , Thanks !")
            break
            
        case .settle:
            attributedString = NSMutableAttributedString(string: "Yes , Please !")
            break
            
        case .feedback:
            attributedString = NSMutableAttributedString(string: "Send Feedback")
            break
            
        case .none:
            attributedString = nil
            break
            
        default:
            attributedString = nil
            break
            
        }
        
        if attributedString?.string.length > 0{
            attributedString!.addAttributes(attributes, range: NSMakeRange(0,attributedString!.string.length))
        }
        
        cellSlideActionLeft.text = attributedString
        
        if lra == 0 {
            cellSlideGestureRecogniser.addActions([cellSlideActionRight])
        }else if lra == 1 {
            cellSlideGestureRecogniser.addActions([cellSlideActionLeft])
        }else if lra == 2 {
            cellSlideGestureRecogniser.addActions([cellSlideActionLeft,cellSlideActionRight])
        }
        
        switch leftOptionRequired {
            
        case .ignore:
            attributedString = NSMutableAttributedString(string: "Sorry!")
            break
            
        case .reject:
            attributedString = NSMutableAttributedString(string: "Sorry can't ride Today !")
            break
            
        case .cancel:
            attributedString = NSMutableAttributedString(string: "Sorry can't ride Today !")
            break
            
        case .none:
            attributedString = nil
            break
            
        default:
            attributedString = nil
            break
        }
        
        
        if attributedString?.string.length > 0{
            attributedString!.addAttributes(attributes, range: NSMakeRange(0,attributedString!.string.length))
        }
        
        cellSlideActionRight.text = attributedString
        cellSlideActionLeft.didTriggerBlock = actionLeft
        cellSlideActionRight.didTriggerBlock = actionRight
        self.addGestureRecognizer(cellSlideGestureRecogniser)
    }

    func setupOfferView(){
        let offerType = safeString(self.userInfo?.object(forKey: "offer_type"))
        let rideCost = safeString(self.userInfo?.object(forKey: "ride_cost"))
        offerContainerView.isHidden = false
        if offerType == "cashback" || offerType == "discount" {
            offerViewType1.isHidden = false
            freeRideOfferView.isHidden = true
            offerViewType1ValueLabel.text = "₹\(rideCost)"
            offerViewType1ValueLabel.setAttributedTextWithOptimisationsType2(fontSize:7)
        }else if offerType == "free" {
            offerViewType1.isHidden = true
            freeRideOfferView.isHidden = false
        }else if offerType == "no_offer" {
            offerViewType1.isHidden = false
            freeRideOfferView.isHidden = true
            offerViewType1ValueLabel.text = "₹\(rideCost)"
            offerViewType1ValueLabel.setAttributedTextWithOptimisationsType2(fontSize:7)
        }else{
            offerContainerView.isHidden = true
        }        
    }
    
    @IBAction func onClickOfActionButton(){
        if isSystemReadyToProcessThis(){
            let action = Acf().getRightSwipeOption(userInfo!)
            if action == .accept {
                self.acceptInvitation()
            }else if action == .settle {
                self.settlePayment()
            }else if action == .invite {
                self.sendInvitation()
            }else if action == .feedback {
                self.giveUserFeedback()
            }
        }
    }
    
    func isActionButtonActionable()->Bool{
        let action = Acf().getRightSwipeOption(userInfo!)
        if action == .accept {return true
        }else if action == .settle {return true
        }else if action == .invite {return true
        }else if action == .feedback {return true
        }
        return false
    }
    
    @IBAction func onClickOfCall(){
        if isSystemReadyToProcessThis(){
            performAnimatedClickEffectType1(callButton)
            ACETelPrompt.callPhoneNumber(userInfo!.object(forKey: "user_mob") as? String, call: { (duration) -> Void in }) { () -> Void in}
        }
    }
    
    @IBAction func onClickOfChat(){
        if isSystemReadyToProcessThis(){
            performAnimatedClickEffectType1(chatButton)
            if isNotNull(userInfo!.object(forKey: "traveller_id")){
                Acf().showChatScreen(userInfo!.object(forKey: "traveller_id") as! String)
            }
        }
    }
    
    static func getRequiredHeight(_ userInfo:NSDictionary)->(CGFloat){
        if isRidingWithDetailsExist(userInfo){
            return 223.0 + 8
        }else{
            return 223 - 63 + 8
        }
    }
    
    func finishRequestSuccessfully(){
        if self.processingUserId != nil {
            Acf().travellersListProcessingDetails.removeObject(forKey: "\(processingUserId!)")
        }
        NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_NEED_USER_UPDATE), object: nil)
        removeSwipeActions()
        self.processingIndicatorLabel.text = "Success"
        self.processingIndicatorLabel.hideActivityIndicator()
        if self.processingUserId != nil {
            NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_SET_TRAVELLER_LAST_ACTION_STATE_TO_SUCCESS), object: nil, userInfo: ["traveller_id":self.processingUserId] as! [AnyHashable : Any])
        }
    }
    
    func finishRequestFailing(){
        if self.processingUserId != nil {
            Acf().travellersListProcessingDetails.removeObject(forKey: "\(processingUserId!)")
        }
        NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_NEED_USER_UPDATE), object: nil)
        self.processingIndicatorLabel.text = "Failed !"
        self.processingIndicatorLabel.hideActivityIndicator()
        execMain({[weak self]  in guard let `self` = self else { return }
            self.processingIndicatorLabel.isHidden = true
            },delay: 0.4)
    }
    
    func startRequest(_ message:String){
        var centerPoint = self.processingIndicatorLabel.center
        centerPoint.y = centerPoint.y + 20
        self.processingIndicatorLabel.showActivityIndicatorType(centerPoint, style: .gray)
        self.processingIndicatorLabel.isHidden = false
        self.processingIndicatorLabel.text = message
        self.processingUserId = getUserId(self.userInfo)
        Acf().travellersListProcessingDetails.setObject(message, forKey: "\(processingUserId!)" as NSCopying)
    }
    
    func sendInvitation(){
        if(isInternetConnectivityAvailable(true)==false){return}
        self.startRequest("Sending Invitation")
        let userInfo = Dbm().getUserInfo()
        let information = NSMutableDictionary()
        copyData(userInfo!.userId , destinationDictionary: information, destinationKey: "userId", methodName: #function)
        if self.parentVC! is  RideHomeViewController {
            if let viewControllerObject = self.parentVC as? RideHomeViewController {
                copyData(viewControllerObject.trueForHomeToDestinationFalseforDestinationToHome ? "0" : "1" , destinationDictionary: information, destinationKey: "mode", methodName: #function)
                copyData(getUserId(self.userInfo) , destinationDictionary: information, destinationKey: "otherUserId", methodName: #function)
                copyData(viewControllerObject.trueForHomeToDestinationFalseforDestinationToHome ? "0" : "1" , destinationDictionary: information, destinationKey: "toMode", methodName: #function)
                copyData(viewControllerObject.selectedDayDate.toStringValue("yyyy-MM-dd") , destinationDictionary: information, destinationKey: "date", methodName: #function)
            }
        }else if self.parentVC! is  RideDetailViewController {
            if let viewControllerObject = self.parentVC as? RideDetailViewController {
                copyData(viewControllerObject.trueForHomeToDestinationFalseforDestinationToHome ? "0" : "1" , destinationDictionary: information, destinationKey: "mode", methodName: #function)
                copyData(getUserId(self.userInfo) , destinationDictionary: information, destinationKey: "otherUserId", methodName: #function)
                copyData(viewControllerObject.trueForHomeToDestinationFalseforDestinationToHome ? "0" : "1" , destinationDictionary: information, destinationKey: "toMode", methodName: #function)
                copyData(viewControllerObject.selectedDayDate.toStringValue("yyyy-MM-dd") , destinationDictionary: information, destinationKey: "date", methodName: #function)
            }
        }
        let scm = ServerCommunicationManager()
        scm.showSuccessResponseMessage = true
        scm.sendInviteForRide(information) { (responseData) -> () in
            if let _ = responseData {
                self.finishRequestSuccessfully()
                execMain({ (completed) in
                    self.showCallOptionPopupIfRequired()
                },delay:0.4)
            }else{
                self.finishRequestFailing()
            }
        }
    }
    
    func showCallOptionPopupIfRequired(){
        if safeBool(self.userInfo?.object(forKey: "show_phone")){
            let prompt = UIAlertController(title: "Great", message: "\nCall \(safeString(self.userInfo?.object(forKey: "user_name")).firstName()) right now to confirm your ride!", preferredStyle: UIAlertControllerStyle.alert)
            prompt.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
            prompt.addAction(UIAlertAction(title: "Call", style: UIAlertActionStyle.cancel, handler: { (action) -> Void in
                self.onClickOfCall()
            }))
            Acf().navigationController!.present(prompt, animated: true, completion: nil)
        }
    }
    
    func acceptInvitation(){
        if(isInternetConnectivityAvailable(true)==false){return}
        self.startRequest("Accepting Invitation")
        let userInfo = Dbm().getUserInfo()
        let information = NSMutableDictionary()
        copyData(userInfo!.userId , destinationDictionary: information, destinationKey: "userId", methodName: #function)
        copyData(self.userInfo?.object(forKey: "id") , destinationDictionary: information, destinationKey: "requestId", methodName: #function)
        let scm = ServerCommunicationManager()
        scm.showSuccessResponseMessage = true
        scm.messageOption = .showSuccessResponseMessageUsingPopUp
        scm.acceptInvitationForRide(information) { (responseData) -> () in
            if let _ = responseData {
                trackFunctionalEvent(FE_ACCEPT_RIDE_INVITE,information:information)
                self.finishRequestSuccessfully()
            }else{
                trackFunctionalEvent(FE_ACCEPT_RIDE_INVITE,information:information,isSuccess:false)
                self.finishRequestFailing()
            }
        }
    }
    
    func rejectInvitation(){
        if(isInternetConnectivityAvailable(true)==false){return}
        self.startRequest("Cancelling Invitation")
        let userInfo = Dbm().getUserInfo()
        let information = NSMutableDictionary()
        copyData(userInfo!.userId , destinationDictionary: information, destinationKey: "userId", methodName: #function)
        copyData(self.userInfo?.object(forKey: "id") , destinationDictionary: information, destinationKey: "requestId", methodName: #function)
        let scm = ServerCommunicationManager()
        scm.rejectInvitationForRide(information) { (responseData) -> () in
            if let _ = responseData {
                trackFunctionalEvent(FE_REJECT_RIDE_INVITE,information:information)
                self.finishRequestSuccessfully()
            }else{
                trackFunctionalEvent(FE_REJECT_RIDE_INVITE,information:information,isSuccess:false)
                self.finishRequestFailing()
            }
        }
    }
    
    func cancelRide(){
        if(isInternetConnectivityAvailable(true)==false){return}
        self.startRequest("Cancelling Ride")
        let userInfo = Dbm().getUserInfo()
        let information = NSMutableDictionary()
        copyData(userInfo!.userId , destinationDictionary: information, destinationKey: "userId", methodName: #function)
        copyData(self.userInfo?.object(forKey: "id") , destinationDictionary: information, destinationKey: "requestId", methodName: #function)
        let scm = ServerCommunicationManager()
        scm.cancelRide(information) { (responseData) -> () in
            if let _ = responseData {
                RideEventManager.sharedInstance.cancelMyRide()
                self.finishRequestSuccessfully()
            }else{
                self.finishRequestFailing()
            }
        }
    }
    
    func settlePayment(){
        if(isInternetConnectivityAvailable(true)==false){return}
        self.startRequest("Settling Payment")
        let userInfo = Dbm().getUserInfo()
        let information = NSMutableDictionary()
        copyData(userInfo!.userId , destinationDictionary: information, destinationKey: "userId", methodName: #function)
        copyData(self.userInfo?.object(forKey: "id") , destinationDictionary: information, destinationKey: "requestId", methodName: #function)
        copyData(rideDate?.toStringValue("yyyy-MM-dd") , destinationDictionary: information, destinationKey: "date", methodName: #function)
        let scm = ServerCommunicationManager()
        scm.settleRide(information) { (responseData) -> () in
            if let _ = responseData {
                if let status = (responseData?.object(forKey: "data") as? NSDictionary)?.object(forKey: "status") as? String {
                    if let payment = (responseData?.object(forKey: "data") as? NSDictionary)?.object(forKey: "payment") as? String {
                        let message = "\(status)\n\(payment)"
                        showAlert("Payment", message: message)
                    }
                }
                self.finishRequestSuccessfully()
            }else{
                self.finishRequestFailing()
            }
        }
    }
    
    func giveUserFeedback(){
        if(isInternetConnectivityAvailable(true)==false){return}
        Acf().showFeedbackScreen(self.userInfo!)
    }
    
    @IBAction func onClickOfOfferButton(){
        let offerType = safeString(self.userInfo?.object(forKey: "offer_type"))
        if isNotNull(offerType) {
            Acf().showOfferDetails(userInfo: self.userInfo!)
        }
    }

    
    
}
