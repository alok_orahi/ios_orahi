//
//  LoadingTableViewCell.swift
//  
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//
//

import Foundation
import UIKit

class LoadingTableViewCell : UITableViewCell {
    fileprivate var isInitialisedOnce = false
    @IBOutlet fileprivate weak var progressIndicatorView : RSDotsView?
    override func layoutSubviews() {
        super.layoutSubviews()
        startUpInitialisations()
        updateUserInterfaceOnScreen()
    }
    private func startUpInitialisations(){
        self.contentView.layoutIfNeeded()
        if isInitialisedOnce == false {
            progressIndicatorView?.dotsColor = APP_THEME_VOILET_COLOR
        }
        isInitialisedOnce = true
    }
    @objc private func updateUserInterfaceOnScreen(){
        progressIndicatorView?.startAnimating()
    }
    static func getRequiredHeight()->(CGFloat){
        return 40
    }
}
