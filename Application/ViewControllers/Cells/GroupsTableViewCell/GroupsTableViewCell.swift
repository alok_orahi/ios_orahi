//
//  GroupsTableViewCell.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import Foundation
import UIKit

public protocol GroupsTableViewCellDelegate : NSObjectProtocol {
    func userClickedOnGroup(cell:GroupsTableViewCell,group:Group?)
}

public class GroupsTableViewCell : UITableViewCell {
    
    @IBOutlet weak var groupName1Label : UILabel!
    @IBOutlet weak var groupName2Label : UILabel!
    @IBOutlet weak var groupPicture1ImageView : UIImageView!
    @IBOutlet weak var groupPicture2ImageView : UIImageView!
    @IBOutlet weak var groupPicture1ShadowView : UIView!
    @IBOutlet weak var groupPicture2ShadowView : UIView!

    fileprivate var isInitialisedOnce = false
    weak var delegate : GroupsTableViewCellDelegate?
    var leftGroup : Group?
    var rightGroup : Group?
    var showingDummy = false
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        startUpInitialisations()
        updateUserInterfaceOnScreen()
    }
    
    private func startUpInitialisations(){
        self.contentView.layoutIfNeeded()
        if isInitialisedOnce == false {
            self.selectionStyle = UITableViewCellSelectionStyle.none
            self.groupPicture1ImageView.clipsToBounds = true
            self.groupPicture1ImageView.layer.masksToBounds = true
            self.groupPicture1ImageView.layer.cornerRadius = 2
            self.groupPicture2ImageView.clipsToBounds = true
            self.groupPicture2ImageView.layer.masksToBounds = true
            self.groupPicture2ImageView.layer.cornerRadius = 2
            addShadow(layer: self.groupPicture1ShadowView.layer)
            addShadow(layer: self.groupPicture2ShadowView.layer)
        }
        isInitialisedOnce = true
    }
    
    func addShadow(layer:CALayer){
        layer.cornerRadius = 2
        layer.shadowColor = UIColor.black.withAlphaComponent(0.25).cgColor
        layer.shadowOffset = CGSize.zero
        layer.shadowOpacity = 0.6
        layer.shadowRadius = 3.5
        layer.borderWidth = 0.5
        layer.borderColor = UIColor.white.cgColor
        layer.backgroundColor = UIColor.white.cgColor
    }
    
    func setup(hide:Bool,isLeft:Bool){
        if isLeft {
            self.groupPicture1ImageView.isHidden = hide
            self.groupPicture1ShadowView.isHidden = hide
            self.groupName1Label.isHidden = hide
        }else{
            self.groupPicture2ImageView.isHidden = hide
            self.groupPicture2ShadowView.isHidden = hide
            self.groupName2Label.isHidden = hide
        }
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        if showingDummy {
            self.groupPicture1ImageView.layer.opacity = 0.5
        }else{
            self.groupPicture1ImageView.layer.opacity = 1.0
        }
    }
    
    @IBAction private func onClickOfLeftGroup(){
        if leftGroup != nil{
            delegate?.userClickedOnGroup(cell: self, group: leftGroup!)
        }else if showingDummy {
            delegate?.userClickedOnGroup(cell: self, group: nil)
        }
    }
    
    @IBAction private func onClickOfRightGroup(){
        if rightGroup != nil{
            delegate?.userClickedOnGroup(cell: self, group: rightGroup!)
        }
    }
}
