//
//  UserFeedbacksTableViewCell.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import Foundation
import UIKit

class UserFeedbacksTableViewCell : UITableViewCell {
    
    //MARK: - variables and constants
    @IBOutlet weak var feedbackLabel : UILabel?
    @IBOutlet fileprivate weak var nameLabel : UILabel?
    @IBOutlet fileprivate weak var dateLabel : UILabel?
    @IBOutlet weak var ratingView : HCSStarRatingView!
    
    fileprivate var isInitialisedOnce = false
    var feedbackDetails : NSDictionary?
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        startUpInitialisations()
        updateUserInterfaceOnScreen()
    }
    
    private func startUpInitialisations(){
        self.contentView.layoutIfNeeded()
        if isInitialisedOnce == false {
            self.selectionStyle = UITableViewCellSelectionStyle.none
            self.feedbackLabel?.textColor = APP_THEME_VOILET_COLOR
            
            self.nameLabel?.font = UIFont.init(name: FONT_SEMI_BOLD, size: 12)
            self.nameLabel?.textColor = UIColor.darkGray
            
            self.dateLabel?.font = UIFont.init(name: FONT_SEMI_BOLD, size: 10)
            self.dateLabel?.textColor = UIColor.gray
        }
        isInitialisedOnce = true
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        feedbackLabel?.text = feedbackDetails!.object(forKey: "feedback_text") as? String
        ratingView?.value = CGFloat("\(feedbackDetails!.object(forKey: "rating")!)".toFloat())
        nameLabel?.text =   feedbackDetails!.object(forKey: "from_user") as? String
        dateLabel?.text =   (feedbackDetails!.object(forKey: "date") as! String).dateUsingFormat(format: "yyyy-MM-dd HH:mm:ss").since()
    }
    
    static func getRequiredHeight(_ notification:NSDictionary?)->(CGFloat){
        return 94
    }
}
