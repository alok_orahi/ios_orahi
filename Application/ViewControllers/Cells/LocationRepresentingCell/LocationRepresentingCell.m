//
//  LocationRepresentingCell.m
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

#import "LocationRepresentingCell.h"

@implementation LocationRepresentingCell

@synthesize name;
@synthesize isConfigured;

- (void)awakeFromNib {
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self others];
}

- (void)others {
    if (isConfigured == NO) {
        isConfigured = YES;
        [name setFont:[UIFont fontWithName:@"SFUIText-Regular" size:11]];
        [self setBackgroundColor:[UIColor clearColor]];
        [name setBackgroundColor:[[UIColor whiteColor]colorWithAlphaComponent:0.4]];
    }
}

+ (float)getRequiredHeight {
    return 40;
}

@end
