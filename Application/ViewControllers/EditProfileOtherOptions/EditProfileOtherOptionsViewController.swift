//
//  EditProfileOtherOptionsViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class EditProfileOtherOptionsViewController: UIViewController {
    
    @IBOutlet fileprivate weak var genderTextfield: MKTextField!
    @IBOutlet fileprivate weak var carRegistrationNoTextfield: MKTextField!
    @IBOutlet fileprivate weak var carOwnerOrPassenger: UISegmentedControl!
    @IBOutlet fileprivate weak var carMakeTextfield: MKTextField!
    @IBOutlet fileprivate weak var carModelTextfield: MKTextField!
    @IBOutlet fileprivate weak var destinationNameTextfield: MKTextField!
    @IBOutlet fileprivate weak var distanceTextfield: MKTextField!
    @IBOutlet fileprivate weak var phoneNumberTextfield: MKTextField!
    @IBOutlet fileprivate weak var doneButton: TKTransitionSubmitButton!
    @IBOutlet fileprivate weak var scrollView: UIScrollView!
    @IBOutlet fileprivate weak var scrollViewSubView: UIView!
    @IBOutlet fileprivate weak var carMakeActionButton: UIButton!
    @IBOutlet fileprivate weak var carModelActionButton: UIButton!
    @IBOutlet fileprivate weak var topSpaceToViewConstraintOfDoneButton: NSLayoutConstraint!
    
    fileprivate var carMakes = NSMutableArray()
    fileprivate var carModels = NSMutableArray()
    fileprivate var destinations = NSMutableArray()
    
    fileprivate var arrayMaleFemaleOptions = NSMutableArray()
    fileprivate var arrayCarMakeOptions = NSMutableArray()
    fileprivate var arrayCarModelOptions = NSMutableArray()
    fileprivate var arrayDestinationNameOptions = NSMutableArray()
    
    fileprivate var selectedIndexMaleFemaleOptions = 0
    fileprivate var selectedIndexCarMakeOptions = 0
    fileprivate var selectedIndexCarModelOptions = 0
    fileprivate var selectedIndexDestinationNameOptions = 0
    fileprivate var destinationAddress : String?
    fileprivate var destinationLocationCoordinate : CLLocationCoordinate2D?
    
    //MARK: - view controller life cycle methods
    
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scrollViewSubView.layoutIfNeeded()
        scrollViewSubView.translatesAutoresizingMaskIntoConstraints = true
        scrollViewSubView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: scrollViewSubView.frame.size.height)
        scrollView.contentSize = scrollViewSubView.frame.size
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
        Acf().setupIQKeyboardManagerEnabled()
    }
    
    //MARK: - other methods
    private func setupForNavigationBar(){
        setAppearanceForNavigationBarType1(self.navigationController)
        setupNavigationBarTitleType1("Edit Profile",viewController: self)
        if self.isBackButtonRequired() {
            addNavigationBarButton(self, image: UIImage(named: "backarrowblack"), title: nil, isLeft: true)
        }
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    private func registerForNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(EditProfileOtherOptionsViewController.updateAppearanceOfTextField), name: NSNotification.Name.UITextViewTextDidBeginEditing, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(EditProfileOtherOptionsViewController.updateAppearanceOfTextField), name: NSNotification.Name.UITextViewTextDidEndEditing, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(EditProfileOtherOptionsViewController.updateAppearanceOfTextField), name: NSNotification.Name.UITextFieldTextDidBeginEditing, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(EditProfileOtherOptionsViewController.updateAppearanceOfTextField), name: NSNotification.Name.UITextFieldTextDidEndEditing, object: nil)
    }
    
    private func startUpInitialisations(){
        setAppearanceForViewController(self)
        setAppearanceForMKTextField(genderTextfield)
        setAppearanceForMKTextField(carMakeTextfield)
        setAppearanceForMKTextField(carModelTextfield)
        setAppearanceForMKTextField(destinationNameTextfield)
        setAppearanceForMKTextField(carRegistrationNoTextfield)
        setAppearanceForMKTextField(distanceTextfield)
        setAppearanceForMKTextField(phoneNumberTextfield)
        
        if userType().isEqual(USER_TYPE_CORPORATE){
            destinationNameTextfield.placeholder = "Company Name"
        }else if userType().isEqual(USER_TYPE_STUDENT){
            destinationNameTextfield.placeholder = "College Name"
        }
        updateAppearanceOfTextField()
        
        doneButton.layer.cornerRadius = doneButton.bounds.size.width/2
        doneButton.layer.masksToBounds = true
        doneButton.requiredBackgroundColor = UIColor(red: 100/255, green: 196/255, blue: 219/255, alpha: 1.0)
        
        trackScreenLaunchEvent(SLE_EDIT_PROFILE_OTHER_OPTIONS)
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        let userInfo = Dbm().getUserInfo()
        let settings = Dbm().getSetting()
        genderTextfield.text = (userInfo!.genderTforMaleFForFemale!.boolValue) ? "Male" : "Female"
        carRegistrationNoTextfield.text = settings.carRegistrationNo
        carMakeTextfield.text = settings.carMake
        carModelTextfield.text = settings.carModel
        destinationNameTextfield.text = userInfo!.destinationName
        distanceTextfield.text = settings.distance
        phoneNumberTextfield.text = userInfo!.phone
        arrayMaleFemaleOptions = ["Male","Female"]
        
        if safeString(genderTextfield.text) == "Male" {
            selectedIndexMaleFemaleOptions = 0
        }else{
            selectedIndexMaleFemaleOptions = 1
        }
        
        
        carMakes = Dbm().getAllCarMake()
        carModels = Dbm().getAllCarModel()
        if userType().isEqual(USER_TYPE_CORPORATE){
            destinations = Dbm().getAllCompanies()
        }else if userType().isEqual( USER_TYPE_STUDENT){
            destinations = Dbm().getAllColleges()
        }
        
        for carMake in carMakes {
            arrayCarMakeOptions.add((carMake as! CarMake).name)
            if (carMakeTextfield.text?.length > 0) && ((carMake as! CarMake).name as! NSString).isEqual(to: carMakeTextfield.text!){
                selectedIndexCarMakeOptions = arrayCarMakeOptions.count - 1
            }
        }
        for carModel in carModels{
            arrayCarModelOptions.add((carModel as! CarModel).name)
            if (carModelTextfield.text?.length > 0) && ((carModel as! CarModel).name as! NSString).isEqual(to: carModelTextfield.text!){
                selectedIndexCarModelOptions = arrayCarModelOptions.count - 1
            }
        }
        
        if userType().isEqual( USER_TYPE_CORPORATE){
            for company in destinations{
                arrayDestinationNameOptions.add((company as! Company).name)
                if (destinationNameTextfield.text?.length > 0) && ((company as! Company).name as! NSString).isEqual(to: destinationNameTextfield.text!){
                    selectedIndexDestinationNameOptions = arrayDestinationNameOptions.count - 1
                }
            }
        }else if userType().isEqual( USER_TYPE_STUDENT){
            for college in destinations{
                arrayDestinationNameOptions.add((college as! College).name)
                if (destinationNameTextfield.text?.length > 0) && ((college as! College).name as! NSString).isEqual(to: destinationNameTextfield.text!){
                    selectedIndexDestinationNameOptions = arrayDestinationNameOptions.count - 1
                }
            }
        }
        
        let trueForOwnerFalseforPassenger = (userInfo!.isPassenger! as NSString).isEqual(to: "0")
        if trueForOwnerFalseforPassenger{
            carOwnerOrPassenger.selectedSegmentIndex = 0
        }else{
            carOwnerOrPassenger.selectedSegmentIndex = 1
        }

        updateLayoutChangeDueToCarOwnerOrPassenger()
        
        self.phoneNumberTextfield.isEnabled = false
        self.phoneNumberTextfield.isUserInteractionEnabled = false
        self.phoneNumberTextfield.layer.opacity = 0.7
    }
    
    //MARK: - other functions
    
    func onClickOfLeftBarButton(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    func canContinueForProfileUpdate()->(Bool){
        var canContinue = true
        if canContinue {
            canContinue = validatePhone(phoneNumberTextfield.text, identifier: "Phone Number")
            if canContinue == false {
                phoneNumberTextfield.becomeFirstResponder()
            }
        }
        return canContinue
    }
    
    @IBAction func onClickOfDoneButton(){
        if(isInternetConnectivityAvailable(true)==false){return}
        if canContinueForProfileUpdate(){
            let userInfo = Dbm().getUserInfo()
            let settings = Dbm().getSetting()
            userInfo?.genderTforMaleFForFemale = NSNumber(value: (genderTextfield.text! as NSString).isEqual(to: "Male") as Bool)
            if isNotNull(carRegistrationNoTextfield.text){
                settings.carRegistrationNo = carRegistrationNoTextfield.text!
            }
            settings.carMake = carMakeTextfield.text!
            settings.carModel = carModelTextfield.text!
            if isNotNull(destinationNameTextfield.text){
                userInfo!.destinationName = destinationNameTextfield.text!
            }
            if isNotNull(phoneNumberTextfield.text){
                userInfo!.phone = phoneNumberTextfield.text!
            }
            if isNotNull(destinationAddress){
                settings.destinationAddress = self.destinationAddress
            }
            if destinationLocationCoordinate != nil {
                settings.destinationLocationLatitude = self.destinationLocationCoordinate!.latitude as NSNumber?
                settings.destinationLocationLongitude = self.destinationLocationCoordinate!.longitude as NSNumber?
            }
            settings.isProfileUpdated = NSNumber(value: true)
            Dbm().saveChanges()
            Acf().updateProfileToServerIfRequired({[weak self] (responseData) -> () in guard let `self` = self else { return }
            })
            showNotification("Updating your profile ... it may take couple of seconds.", showOnNavigation: false, showAsError: false,duration: 5)
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func updateAvailableCarModelList() {
        arrayCarModelOptions.removeAllObjects()
        if arrayCarMakeOptions.count > self.selectedIndexCarMakeOptions {
            let carMake = carMakes.object(at: self.selectedIndexCarMakeOptions)
            let makeId = (carMake as! CarMake).mkId
            for carModel in carModels{
                if ((carModel as! CarModel).mkId! as NSString).isEqual(to: makeId!){
                    arrayCarModelOptions.add((carModel as! CarModel).name)
                }
            }
        }
    }
    
    @IBAction func onClickOfSelectGender(){
        resignKeyboard()
        ActionSheetStringPicker.show(withTitle: "Select Gender", rows: arrayMaleFemaleOptions as [AnyObject], initialSelection: selectedIndexMaleFemaleOptions, doneBlock: { (picker, index, object) in
            if isNotNull(object){
                self.selectedIndexMaleFemaleOptions = index
                self.genderTextfield.text = object as! String
            }
            }, cancel: { (picker) in
            }, origin: self.view)
    }
    
    @IBAction func onClickOfSelectCarMake(){
        let userInfo = Dbm().getUserInfo()
        let trueForOwnerFalseforPassenger = (userInfo!.isPassenger! as NSString).isEqual(to: "0")
        if trueForOwnerFalseforPassenger {
            resignKeyboard()
            Acf().showCommonPickerScreen(arrayCarMakeOptions,titleToShow: "Your Car Make", nc: self.navigationController) { (returnedData) in
                if isNotNull(returnedData){
                    self.selectedIndexCarMakeOptions = self.arrayCarMakeOptions.index(of: returnedData!)
                    self.carMakeTextfield.text = (returnedData as! String)
                    self.selectedIndexCarModelOptions = 0
                    self.carModelTextfield.text = nil
                }
            }
        }
    }
    
    @IBAction func onClickOfSelectCarModel(){
        let userInfo = Dbm().getUserInfo()
        let trueForOwnerFalseforPassenger = (userInfo!.isPassenger! as NSString).isEqual(to: "0")
        if trueForOwnerFalseforPassenger {
            resignKeyboard()
            updateAvailableCarModelList()
            Acf().showCommonPickerScreen(arrayCarModelOptions,titleToShow: "Your Car Model", nc: self.navigationController) { (returnedData) in
                if isNotNull(returnedData){
                    self.selectedIndexCarModelOptions = self.arrayCarModelOptions.index(of: returnedData!)
                    self.carModelTextfield.text = (returnedData as! String)
                }
            }
        }
    }
    
    @IBAction func onClickOfSelectDestinationName(){
        resignKeyboard()
        let viewController = getViewController("DestinationPickerViewController") as! DestinationPickerViewController
        viewController.completion = { (returnedData) in
            if let company = returnedData as? Company{
                self.destinationAddress = nil
                self.destinationLocationCoordinate = nil
                self.selectedIndexDestinationNameOptions = self.arrayDestinationNameOptions.index(of: company)
                self.destinationNameTextfield.text = company.name
                self.destinationAddress = company.address
                self.reverseGeoCodeDestinationAddress()
            }else if let college = returnedData as? College{
                self.destinationAddress = nil
                self.destinationLocationCoordinate = nil
                self.selectedIndexDestinationNameOptions = self.arrayDestinationNameOptions.index(of: college)
                self.destinationNameTextfield.text = college.name
                self.destinationAddress = college.address
                self.reverseGeoCodeDestinationAddress()
            }
        }
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func updateAppearanceOfTextField(){
        Acf().updateAppearanceOfTextFieldType1(genderTextfield)
        Acf().updateAppearanceOfTextFieldType1(carRegistrationNoTextfield)
        Acf().updateAppearanceOfTextFieldType1(carModelTextfield)
        Acf().updateAppearanceOfTextFieldType1(carMakeTextfield)
        Acf().updateAppearanceOfTextFieldType1(destinationNameTextfield)
        Acf().updateAppearanceOfTextFieldType1(distanceTextfield)
        Acf().updateAppearanceOfTextFieldType1(phoneNumberTextfield)
    }
    
    func reverseGeoCodeDestinationAddress() {
        if self.destinationAddress?.length > 0 {
            weak var weakSelf = self
            SwiftLocation.shared.reverseAddress(.googleMaps, address: weakSelf!.destinationAddress!, region: nil, onSuccess: { (place) in
                if isNotNull(weakSelf){
                    weakSelf!.destinationLocationCoordinate = place?.location?.coordinate
                }
                }, onFail: { (error) in
            })
        }
    }
    
    @IBAction func segmentControlValueChanged(_ sender:UISegmentedControl){
        let userInfo = Dbm().getUserInfo()
        userInfo!.isPassenger = "\(sender.selectedSegmentIndex)"
        Dbm().saveChanges()
        DispatchQueue.main.async {
            self.view.layoutIfNeeded()
            UIView.animate(withDuration: 0.4, animations: {
                self.updateLayoutChangeDueToCarOwnerOrPassenger()
                self.view.layoutIfNeeded()
            }) 
        }
    }
    
    var lastSelectionCarRegistrationNo : String?
    var lastSelectionCarMake : String?
    var lastSelectionCarModel : String?
    
    func updateLayoutChangeDueToCarOwnerOrPassenger() {
        let userInfo = Dbm().getUserInfo()
        if isNotNull(carRegistrationNoTextfield.text){
            lastSelectionCarRegistrationNo = carRegistrationNoTextfield.text
        }
        if isNotNull(carMakeTextfield.text){
            lastSelectionCarMake = carMakeTextfield.text
        }
        if isNotNull(carModelTextfield.text){
            lastSelectionCarModel = carModelTextfield.text
        }
        let trueForOwnerFalseforPassenger = (userInfo!.isPassenger! as NSString).isEqual(to: "0")
        if trueForOwnerFalseforPassenger == false {
            carRegistrationNoTextfield.text = nil
            carMakeTextfield.text = nil
            carModelTextfield.text = nil
            carRegistrationNoTextfield.isEnabled = false
            carRegistrationNoTextfield.isHidden = true
            carMakeTextfield.isHidden = true
            carModelTextfield.isHidden = true
            carMakeActionButton.isHidden = true
            carModelActionButton.isHidden = true
            topSpaceToViewConstraintOfDoneButton.constant = CGFloat(-42 * 3 - 20)
        }else{
            carRegistrationNoTextfield.text = lastSelectionCarRegistrationNo
            carModelTextfield.text = lastSelectionCarModel
            carMakeTextfield.text = lastSelectionCarMake
            carRegistrationNoTextfield.isEnabled = true
            carRegistrationNoTextfield.isHidden = false
            carMakeTextfield.isHidden = false
            carModelTextfield.isHidden = false
            carMakeActionButton.isHidden = false
            carModelActionButton.isHidden = false
            topSpaceToViewConstraintOfDoneButton.constant = 22.0
        }
    }
}
