//
//  PlaceTypeMKAnnotationView.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit

class PlaceTypeMKAnnotationView : MKAnnotationView {
    
    //MARK: - variables and constants
    
    fileprivate var annotationBackground : UIButton?
    var userInfo:Place?
    var group:Group?
    
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setup() {
        if annotationBackground == nil {
            annotationBackground = UIButton(type: UIButtonType.system)
            annotationBackground?.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
            annotationBackground?.center = self.center
            annotationBackground?.contentMode = UIViewContentMode.scaleAspectFit
            annotationBackground?.isUserInteractionEnabled = true
            annotationBackground?.backgroundColor = UIColor.white
            annotationBackground?.makeMeRound()
            self.addSubview(annotationBackground!)
        }
        annotationBackground?.tintColor = ThemeColor().getColor(type: safeString(group?.themeColor))
        annotationBackground?.setImage(UIImage(named: "streetSign"), for: UIControlState.normal)
    }
}

