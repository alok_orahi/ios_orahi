//
//  DRNContentViewType10.swift
//  Orahi
//
//  Created by Alok Singh on 12/12/16.
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit

class DRNContentViewType10 : DRNContentView {
    
    @IBOutlet fileprivate weak var webview : UIWebView?

    func getRequiredHeight()->CGFloat{
        return DEVICE_HEIGHT - 100
    }

}

func getDRNContentViewType10(parent:Any)->DRNContentViewType10{
    return Bundle.main.loadNibNamed("DRNContentViewType10", owner: parent, options: nil)![0] as! DRNContentViewType10
}

func setContentViewType10(title:String?,description:String?,otherInfo:NSDictionary,superView:UIView,owner:Any)->DRNContentViewType10{
    
    let contentView = getDRNContentViewType10(parent: owner)

    contentView.titleLabel.text = title
    contentView.descriptionLabel.text = description

    let urlString = NSMutableString(string: BASE_URL_RECHARGE+"account/drnrecharge"+"?id="+loggedInUserId()+"&&amount=\(minimumRechargeValue())")
    
    for key in otherInfo.allKeys {
        urlString.append("&&\(key)=\(otherInfo.object(forKey: key)!)")
    }
    
    contentView.webview?.loadRequest(URLRequest(url:(urlString as String).asNSURL()
    ))

    contentView.titleLabel.setAttributedTextWithOptimisationsType1()
    contentView.descriptionLabel.setAttributedTextWithOptimisationsType1()

    superView.addSubview(contentView)

    contentView.translatesAutoresizingMaskIntoConstraints = false
    contentView.leadingAnchor.constraint(equalTo: superView.leadingAnchor,constant:0).isActive = true
    contentView.trailingAnchor.constraint(equalTo: superView.trailingAnchor,constant:0).isActive = true
    contentView.topAnchor.constraint(equalTo: superView.topAnchor,constant:0).isActive = true
    contentView.bottomAnchor.constraint(equalTo: superView.bottomAnchor,constant:0).isActive = true
    contentView.heightAnchor.constraint(equalToConstant: contentView.getRequiredHeight()).isActive = true
    
    return contentView
}
