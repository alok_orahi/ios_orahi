//
//  DRNContentViewType2.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit

class DRNContentViewType1 : DRNContentView {
    
    @IBOutlet fileprivate weak var imageView : UIImageView!
    
    fileprivate var isImageEnabled = false
    
    func getRequiredHeight()->CGFloat{
        var height = 38.0 as CGFloat
        if isImageEnabled {
            height = height + 143
        }
        height = height + titleLabel.textHeight(with: DEVICE_WIDTH - 105)
        height = height + descriptionLabel.textHeight(with: DEVICE_WIDTH - 105)
        return height
    }
}

func getDRNContentViewType1(parent:Any)->DRNContentViewType1{
    return Bundle.main.loadNibNamed("DRNContentViewType1", owner: parent, options: nil)![0] as!DRNContentViewType1
}

func setContentViewType1(title:String?,description:String?,imageUrl:String?,superView:UIView,owner:Any)->DRNContentViewType1{
    
    let contentView = getDRNContentViewType1(parent: owner)
    contentView.titleLabel.text = title
    contentView.descriptionLabel.text = description

    if imageUrl != nil {
        contentView.imageView.sd_setImage(with: URL(string: imageUrl!), placeholderImage: nil)
        contentView.isImageEnabled = true
    }else{
        contentView.isImageEnabled = false
    }
    
    contentView.titleLabel.setAttributedTextWithOptimisationsType1()
    contentView.descriptionLabel.setAttributedTextWithOptimisationsType1()
    
    superView.addSubview(contentView)

    contentView.translatesAutoresizingMaskIntoConstraints = false
    contentView.leadingAnchor.constraint(equalTo: superView.leadingAnchor,constant:0).isActive = true
    contentView.trailingAnchor.constraint(equalTo: superView.trailingAnchor,constant:0).isActive = true
    contentView.topAnchor.constraint(equalTo: superView.topAnchor,constant:0).isActive = true
    contentView.bottomAnchor.constraint(equalTo: superView.bottomAnchor,constant:0).isActive = true
    contentView.heightAnchor.constraint(equalToConstant: contentView.getRequiredHeight()).isActive = true

    return contentView
}
