//
//  DRNContentViewType11.swift
//  Orahi
//
//  Created by Alok Singh on 12/12/16.
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit

class DRNContentViewType11 : DRNContentView {
    
    @IBOutlet fileprivate weak var webview : UIWebView?

    func getRequiredHeight()->CGFloat{
        return DEVICE_HEIGHT - 100
    }

}

func getDRNContentViewType11(parent:Any)->DRNContentViewType11{
    return Bundle.main.loadNibNamed("DRNContentViewType11", owner: parent, options: nil)![0] as! DRNContentViewType11
}

func setContentViewType11(title:String?,description:String?,urlString:String,superView:UIView,owner:Any)->DRNContentViewType11{
    
    let contentView = getDRNContentViewType11(parent: owner)

    contentView.titleLabel.text = title
    contentView.descriptionLabel.text = description
    contentView.webview?.loadRequest(URLRequest(url:urlString.asNSURL()
    ))

    contentView.titleLabel.setAttributedTextWithOptimisationsType1()
    contentView.descriptionLabel.setAttributedTextWithOptimisationsType1()

    superView.addSubview(contentView)

    contentView.translatesAutoresizingMaskIntoConstraints = false
    contentView.leadingAnchor.constraint(equalTo: superView.leadingAnchor,constant:0).isActive = true
    contentView.trailingAnchor.constraint(equalTo: superView.trailingAnchor,constant:0).isActive = true
    contentView.topAnchor.constraint(equalTo: superView.topAnchor,constant:0).isActive = true
    contentView.bottomAnchor.constraint(equalTo: superView.bottomAnchor,constant:0).isActive = true
    contentView.heightAnchor.constraint(equalToConstant: contentView.getRequiredHeight()).isActive = true
    
    return contentView
}
