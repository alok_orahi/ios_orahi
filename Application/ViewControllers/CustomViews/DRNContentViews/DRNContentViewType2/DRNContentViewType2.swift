//
//  DRNContentViewType2.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit

class DRNContentViewType2 : DRNContentView {
    
    @IBOutlet fileprivate weak var imageView : UIImageView!
    @IBOutlet fileprivate weak var ratingView : HCSStarRatingView!
    
    func getRequiredHeight()->CGFloat{
        var height = 260.0 as CGFloat
        let requiredHeightTitle = titleLabel.textHeight(with: DEVICE_WIDTH - 80)
        if requiredHeightTitle > 18 {
            height = height + requiredHeightTitle - 18
        }
        let requiredHeightDescription = descriptionLabel.textHeight(with: DEVICE_WIDTH - 80)
        if requiredHeightDescription > 36 {
            height = height + requiredHeightDescription - 36
        }
        return height
    }
    
    override func getInputParameters()->NSDictionary{
        return ["rating":"\(ratingView.value)"]
    }
    
}

func getDRNContentViewType2(parent:Any)->DRNContentViewType2{
    return Bundle.main.loadNibNamed("DRNContentViewType2", owner: parent, options: nil)![0] as!DRNContentViewType2
}

func setContentViewType2(title:String?,description:String?,imageUrl:String?,superView:UIView,owner:Any)->DRNContentViewType2{
    
    let contentView = getDRNContentViewType2(parent: owner)
    contentView.titleLabel.text = title
    contentView.descriptionLabel.text = description
    contentView.imageView.sd_setImage(with: URL(string: imageUrl!), placeholderImage: USER_PLACEHOLDER_IMAGE)
    contentView.imageView.makeMeRoundWith(borderColor: APP_THEME_YELLOW_COLOR, width: 1.5)

    contentView.titleLabel.setAttributedTextWithOptimisationsType1()
    contentView.descriptionLabel.setAttributedTextWithOptimisationsType1()

    superView.addSubview(contentView)

    contentView.translatesAutoresizingMaskIntoConstraints = false
    contentView.leadingAnchor.constraint(equalTo: superView.leadingAnchor,constant:0).isActive = true
    contentView.trailingAnchor.constraint(equalTo: superView.trailingAnchor,constant:0).isActive = true
    contentView.topAnchor.constraint(equalTo: superView.topAnchor,constant:0).isActive = true
    contentView.bottomAnchor.constraint(equalTo: superView.bottomAnchor,constant:0).isActive = true
    contentView.heightAnchor.constraint(equalToConstant: contentView.getRequiredHeight()).isActive = true
    return contentView
}
