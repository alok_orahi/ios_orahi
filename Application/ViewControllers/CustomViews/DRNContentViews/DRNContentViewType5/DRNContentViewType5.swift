//
//  DRNContentViewType5.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit

class DRNContentViewType5 : DRNContentView {
    
    @IBOutlet fileprivate weak var imageView : UIImageView!
    @IBOutlet fileprivate weak var nameLabel : UILabel!
    @IBOutlet fileprivate weak var detailsLabel : UILabel!
    
    func getRequiredHeight()->CGFloat{
        var height = 250.0 as CGFloat
        let requiredHeightTitle = titleLabel.textHeight(with: DEVICE_WIDTH - 110)
        if requiredHeightTitle > 18 {
            height = height + requiredHeightTitle - 18
        }
        let requiredHeightDescription = descriptionLabel.textHeight(with: DEVICE_WIDTH - 110)
        if requiredHeightDescription > 36 {
            height = height + requiredHeightDescription - 36
        }
        return height
    }

}

func getDRNContentViewType5(parent:Any)->DRNContentViewType5{
    return Bundle.main.loadNibNamed("DRNContentViewType5", owner: parent, options: nil)![0] as!DRNContentViewType5
}

func setContentViewType5(title:String?,description:String?,url:String?,name:String?,details:String?,superView:UIView,owner:Any)->DRNContentViewType5{
    
    let contentView = getDRNContentViewType5(parent: owner)
    contentView.imageView.makeMeRoundWith(borderColor: APP_THEME_YELLOW_COLOR, width: 1.5)

    contentView.titleLabel.text = title
    contentView.descriptionLabel.text = description
    contentView.imageView.sd_setImage(with: URL(string: url!), placeholderImage: USER_PLACEHOLDER_IMAGE)
    contentView.nameLabel.text = name
    contentView.detailsLabel.text = details

    contentView.titleLabel.setAttributedTextWithOptimisationsType1()
    contentView.descriptionLabel.setAttributedTextWithOptimisationsType1()
    contentView.nameLabel.setAttributedTextWithOptimisationsType1()
    contentView.detailsLabel.setAttributedTextWithOptimisationsType1()
    
    superView.addSubview(contentView)

    contentView.translatesAutoresizingMaskIntoConstraints = false
    contentView.leadingAnchor.constraint(equalTo: superView.leadingAnchor,constant:0).isActive = true
    contentView.trailingAnchor.constraint(equalTo: superView.trailingAnchor,constant:0).isActive = true
    contentView.topAnchor.constraint(equalTo: superView.topAnchor,constant:0).isActive = true
    contentView.bottomAnchor.constraint(equalTo: superView.bottomAnchor,constant:0).isActive = true
    contentView.heightAnchor.constraint(equalToConstant: contentView.getRequiredHeight()).isActive = true
    
    return contentView
}
