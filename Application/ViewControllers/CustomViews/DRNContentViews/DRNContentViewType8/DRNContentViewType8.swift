//
//  DRNContentViewType8.swift
//  Orahi
//
//  Created by Alok Singh on 12/12/16.
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit

class DRNContentViewType8 : DRNContentView {
    
    @IBOutlet fileprivate weak var imageView : UIImageView!
    @IBOutlet fileprivate weak var tagLabel : UILabel!
    @IBOutlet fileprivate weak var tagBgImageView : UIImageView!
    @IBOutlet fileprivate weak var titleInfoLabel : UILabel!
    @IBOutlet fileprivate weak var descriptionInfoLabel : UILabel!
    @IBOutlet fileprivate weak var travelledWithLabelView : UIView!
    @IBOutlet fileprivate weak var otherUserImageView1 : UIImageView!
    @IBOutlet fileprivate weak var otherUserImageView2 : UIImageView!
    @IBOutlet fileprivate weak var otherUserImageView3 : UIImageView!
    @IBOutlet fileprivate weak var otherUserImageView4 : UIImageView!
    @IBOutlet fileprivate weak var otherUserNameLabel1 : UILabel!
    @IBOutlet fileprivate weak var otherUserNameLabel2 : UILabel!
    @IBOutlet fileprivate weak var otherUserNameLabel3 : UILabel!
    @IBOutlet fileprivate weak var otherUserNameLabel4 : UILabel!

    func getRequiredHeight()->CGFloat{
        var height = 220.0 as CGFloat
        height = height + titleLabel.textHeight(with: DEVICE_WIDTH - 105)
        height = height + descriptionLabel.textHeight(with: DEVICE_WIDTH - 105)
        return height
    }

}

func getDRNContentViewType8(parent:Any)->DRNContentViewType8{
    return Bundle.main.loadNibNamed("DRNContentViewType8", owner: parent, options: nil)![0] as! DRNContentViewType8
}

func setContentViewType8(title:String?,description:String?,url:String?,titleInfo:String?,descriptionInfo:String?,tag:String?,url1:String?,url2:String?,url3:String?,url4:String?,name1:String?,name2:String?,name3:String?,name4:String?,superView:UIView,owner:Any)->DRNContentViewType8{
    
    let contentView = getDRNContentViewType8(parent: owner)
    contentView.imageView.makeMeRoundWith(borderColor: APP_THEME_YELLOW_COLOR, width: 1.5)

    contentView.titleLabel.text = title
    contentView.descriptionLabel.text = description
    contentView.imageView.sd_setImage(with: URL(string: url!), placeholderImage: USER_PLACEHOLDER_IMAGE)
    contentView.titleInfoLabel.text = titleInfo
    contentView.descriptionInfoLabel.text = descriptionInfo
    setUserTagDetails(tag, tagImageView: contentView.tagBgImageView, tagLabel: contentView.tagLabel)

    if isNotNull(url1){
        contentView.otherUserImageView1.isHidden = false
        contentView.otherUserImageView1.sd_setImage(with: URL(string: url1!), placeholderImage: USER_PLACEHOLDER_IMAGE)
    }else{
        contentView.otherUserImageView1.isHidden = true
    }
    
    if isNotNull(url2){
        contentView.otherUserImageView2.isHidden = false
        contentView.otherUserImageView2.sd_setImage(with: URL(string: url2!), placeholderImage: USER_PLACEHOLDER_IMAGE)
    }else{
        contentView.otherUserImageView2.isHidden = true
    }
    
    if isNotNull(url3){
        contentView.otherUserImageView3.isHidden = false
        contentView.otherUserImageView3.sd_setImage(with: URL(string: url3!), placeholderImage: USER_PLACEHOLDER_IMAGE)
    }else{
        contentView.otherUserImageView3.isHidden = true
    }
    
    if isNotNull(url4){
        contentView.otherUserImageView4.isHidden = false
        contentView.otherUserImageView4.sd_setImage(with: URL(string: url4!), placeholderImage: USER_PLACEHOLDER_IMAGE)
    }else{
        contentView.otherUserImageView4.isHidden = true
    }
    
    contentView.otherUserNameLabel1.text = safeString(name1)
    contentView.otherUserNameLabel2.text = safeString(name2)
    contentView.otherUserNameLabel3.text = safeString(name3)
    contentView.otherUserNameLabel4.text = safeString(name4)
    
    contentView.travelledWithLabelView.isHidden = contentView.otherUserImageView1.isHidden
    
    contentView.titleLabel.setAttributedTextWithOptimisationsType1()
    contentView.descriptionLabel.setAttributedTextWithOptimisationsType1()
    contentView.titleInfoLabel.setAttributedTextWithOptimisationsType1()
    contentView.descriptionInfoLabel.setAttributedTextWithOptimisationsType1()

    superView.addSubview(contentView)

    contentView.translatesAutoresizingMaskIntoConstraints = false
    contentView.leadingAnchor.constraint(equalTo: superView.leadingAnchor,constant:0).isActive = true
    contentView.trailingAnchor.constraint(equalTo: superView.trailingAnchor,constant:0).isActive = true
    contentView.topAnchor.constraint(equalTo: superView.topAnchor,constant:0).isActive = true
    contentView.bottomAnchor.constraint(equalTo: superView.bottomAnchor,constant:0).isActive = true
    contentView.heightAnchor.constraint(equalToConstant: contentView.getRequiredHeight()).isActive = true
    
    return contentView
}
