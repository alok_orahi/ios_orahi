//
//  DRNContentViewType7.swift
//  Orahi
//
//  Created by Alok Singh on 12/12/16.
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit

class DRNContentViewType7 : DRNContentView {
    
    @IBOutlet fileprivate weak var imageView : UIImageView!
    @IBOutlet weak var titleLabel1 : UILabel!
    @IBOutlet fileprivate weak var descriptionLabel1 : UILabel!
    @IBOutlet fileprivate weak var descriptionLabel2 : UILabel!
    @IBOutlet fileprivate weak var descriptionLabel3 : UILabel!
    @IBOutlet fileprivate weak var tagLabel : UILabel!
    @IBOutlet fileprivate weak var tagBgImageView : UIImageView!

    func getRequiredHeight()->CGFloat{
        var height = 220.0 as CGFloat
        height = height + titleLabel.textHeight(with: DEVICE_WIDTH - 105)
        height = height + descriptionLabel.textHeight(with: DEVICE_WIDTH - 105)
        return height
    }

}

func getDRNContentViewType7(parent:Any)->DRNContentViewType7{
    return Bundle.main.loadNibNamed("DRNContentViewType7", owner: parent, options: nil)![0] as! DRNContentViewType7
}

func setContentViewType7(title:String?,description:String?,tag:String?,url:String?,title1:String?,description1:String?,description2:String?,description3:String?,superView:UIView,owner:Any)->DRNContentViewType7{
    
    let contentView = getDRNContentViewType7(parent: owner)
    contentView.imageView.makeMeRoundWith(borderColor: APP_THEME_YELLOW_COLOR, width: 1.5)

    contentView.titleLabel.text = title
    contentView.descriptionLabel.text = description
    contentView.imageView.sd_setImage(with: URL(string: url!), placeholderImage: USER_PLACEHOLDER_IMAGE)
    contentView.titleLabel1.text = title1
    contentView.descriptionLabel1.text = description1
    contentView.descriptionLabel2.text = description2
    contentView.descriptionLabel3.text = description3
    
    setUserTagDetails(tag, tagImageView: contentView.tagBgImageView, tagLabel: contentView.tagLabel)

    contentView.titleLabel.setAttributedTextWithOptimisationsType1()
    contentView.descriptionLabel.setAttributedTextWithOptimisationsType1()
    contentView.titleLabel1.setAttributedTextWithOptimisationsType1()
    contentView.descriptionLabel1.setAttributedTextWithOptimisationsType1()
    contentView.descriptionLabel2.setAttributedTextWithOptimisationsType1()
    contentView.descriptionLabel3.setAttributedTextWithOptimisationsType1()

    superView.addSubview(contentView)

    contentView.translatesAutoresizingMaskIntoConstraints = false
    contentView.leadingAnchor.constraint(equalTo: superView.leadingAnchor,constant:0).isActive = true
    contentView.trailingAnchor.constraint(equalTo: superView.trailingAnchor,constant:0).isActive = true
    contentView.topAnchor.constraint(equalTo: superView.topAnchor,constant:0).isActive = true
    contentView.bottomAnchor.constraint(equalTo: superView.bottomAnchor,constant:0).isActive = true
    contentView.heightAnchor.constraint(equalToConstant: contentView.getRequiredHeight()).isActive = true
    
    return contentView
}
