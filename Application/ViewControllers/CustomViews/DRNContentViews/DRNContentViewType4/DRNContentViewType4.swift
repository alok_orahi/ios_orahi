//
//  DRNContentViewType4.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit

class DRNContentViewType4 : DRNContentView {
    
    @IBOutlet fileprivate weak var imageView1 : UIImageView!
    @IBOutlet fileprivate weak var imageView2 : UIImageView!
    @IBOutlet weak var titleLabel1 : UILabel!
    @IBOutlet weak var titleLabel2 : UILabel!
    @IBOutlet fileprivate weak var descriptionLabel1 : UILabel!
    @IBOutlet fileprivate weak var descriptionLabel2 : UILabel!
    
    func getRequiredHeight()->CGFloat{
        var height = 200.0 as CGFloat
        height = height + titleLabel.textHeight(with: DEVICE_WIDTH - 105)
        height = height + descriptionLabel.textHeight(with: DEVICE_WIDTH - 105)
        return height
    }

}

func getDRNContentViewType4(parent:Any)->DRNContentViewType4{
    return Bundle.main.loadNibNamed("DRNContentViewType4", owner: parent, options: nil)![0] as!DRNContentViewType4
}

func setContentViewType4(title:String?,description:String?,ur11:String?,name1:String?,time1:String?,ur12:String?,name2:String?,time2:String?,superView:UIView,owner:Any)->DRNContentViewType4{
    
    let contentView = getDRNContentViewType4(parent: owner)
    contentView.imageView1.makeMeRoundWith(borderColor: APP_THEME_YELLOW_COLOR, width: 1.5)
    contentView.imageView2.makeMeRoundWith(borderColor: APP_THEME_BLUE_COLOR, width: 1.5)

    contentView.titleLabel.text = title
    contentView.descriptionLabel.text = description
    contentView.imageView1.sd_setImage(with: URL(string: ur11!), placeholderImage: USER_PLACEHOLDER_IMAGE)
    contentView.imageView2.sd_setImage(with: URL(string: ur12!), placeholderImage: USER_PLACEHOLDER_IMAGE)
    contentView.titleLabel1.text = name1?.firstName()
    contentView.titleLabel2.text = name2?.firstName()
    contentView.descriptionLabel1.text = time1
    contentView.descriptionLabel2.text = time2

    
    contentView.titleLabel.setAttributedTextWithOptimisationsType1()
    contentView.descriptionLabel.setAttributedTextWithOptimisationsType1()
    contentView.titleLabel1.setAttributedTextWithOptimisationsType1()
    contentView.titleLabel2.setAttributedTextWithOptimisationsType1()
    contentView.descriptionLabel1.setAttributedTextWithOptimisationsType1()
    contentView.descriptionLabel2.setAttributedTextWithOptimisationsType1()
    

    superView.addSubview(contentView)

    contentView.translatesAutoresizingMaskIntoConstraints = false
    contentView.leadingAnchor.constraint(equalTo: superView.leadingAnchor,constant:0).isActive = true
    contentView.trailingAnchor.constraint(equalTo: superView.trailingAnchor,constant:0).isActive = true
    contentView.topAnchor.constraint(equalTo: superView.topAnchor,constant:0).isActive = true
    contentView.bottomAnchor.constraint(equalTo: superView.bottomAnchor,constant:0).isActive = true
    contentView.heightAnchor.constraint(equalToConstant: contentView.getRequiredHeight()).isActive = true
    
    return contentView
}
