//
//  UserType2MKAnnotationView.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit

class UserType2MKAnnotationView : MKAnnotationView {
    
    //MARK: - variables and constants
    
    fileprivate var annotationBackgroundButton : UIButton?
    fileprivate var annotationUserImageView : UIImageView?
    fileprivate var bgImage:UIImage?
    fileprivate var userImageUrl:String?
    var userInfo:Member?
    var group:Group?
    
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setup() {
        if annotationBackgroundButton == nil {
            annotationBackgroundButton = UIButton(type: UIButtonType.system)
            annotationBackgroundButton?.frame = CGRect(x: 0, y: 0, width: 60, height: 60)
            annotationBackgroundButton?.center = self.center
            annotationBackgroundButton?.contentMode = UIViewContentMode.scaleAspectFit
            annotationBackgroundButton?.isUserInteractionEnabled = true
            self.addSubview(annotationBackgroundButton!)
        }
        if annotationUserImageView == nil {
            annotationUserImageView = UIImageView()
            annotationUserImageView?.image = bgImage
            annotationUserImageView?.frame = CGRect(x: 17, y: 10, width: 26, height: 26)
            annotationUserImageView?.contentMode = UIViewContentMode.scaleAspectFill
            setBorder(annotationUserImageView!, color: UIColor.white, width: 0.5, cornerRadius: (annotationUserImageView?.bounds.size.width)!/2)
            annotationUserImageView?.isUserInteractionEnabled = true
            annotationBackgroundButton!.addSubview(annotationUserImageView!)
        }
        if userInfo != nil {
            bgImage = UIImage(named: "locationAnnotation")
            annotationBackgroundButton?.tintColor = ThemeColor().getColor(type: safeString(group?.themeColor))
            annotationBackgroundButton?.setImage(bgImage, for: UIControlState.normal)
            annotationUserImageView?.sd_setImage(with: URL(string:getUserProfilePictureUrlFromFileName(userInfo!.picture)), placeholderImage: USER_PLACEHOLDER_IMAGE)
            annotationUserImageView?.isHidden = false
        }else{
            bgImage = UIImage(named: "genericLocationAnnotation")
            annotationBackgroundButton?.tintColor = ThemeColor().getColor(type: safeString(group?.themeColor))
            annotationBackgroundButton?.setImage(bgImage, for: UIControlState.normal)
            annotationUserImageView?.isHidden = true
        }
    }
}

