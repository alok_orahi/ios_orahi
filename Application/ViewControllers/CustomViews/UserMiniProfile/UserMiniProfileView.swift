//
//  UserMiniProfileView.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit
import MapKit
import SnapKit

typealias UMPCompletionBlock = (_ returnedData :Any?) ->()

class UserMiniProfileView : UIView {
    
    var completionBlock: UMPCompletionBlock?
    var removedBlock: UMPCompletionBlock?
    
    @IBOutlet fileprivate weak var swipeableView : ZLSwipeableView!
    @IBOutlet fileprivate weak var userInformationHolderView : UIView!
    @IBOutlet fileprivate weak var userStatusColorView : UIView!
    @IBOutlet fileprivate weak var profilePictureImageView : UIImageView!
    @IBOutlet fileprivate weak var ratingView : HCSStarRatingView!
    @IBOutlet fileprivate weak var ridesDoneLabel : UILabel!
    
    @IBOutlet fileprivate weak var fullNameLabel : UILabel!
    @IBOutlet fileprivate weak var statusTextLabel : UILabel!
    @IBOutlet fileprivate weak var destinationNameLabel : UILabel!
    
    @IBOutlet fileprivate weak var offerContainerView : UIView!
    @IBOutlet fileprivate weak var offerViewType1 : UIView!
    @IBOutlet fileprivate weak var freeRideOfferView : UIView!
    @IBOutlet fileprivate weak var offerViewType1ValueLabel : UILabel!

    @IBOutlet fileprivate weak var modeLabel : UILabel!
    @IBOutlet fileprivate weak var tagLabel : UILabel!
    @IBOutlet fileprivate weak var tagBgImageView : UIImageView!
    @IBOutlet fileprivate weak var lastActiveLabel : UILabel!
    @IBOutlet fileprivate weak var travellingTime : UILabel!
    
    @IBOutlet fileprivate weak var ridingWithUserImageView1 : UIImageView!
    @IBOutlet fileprivate weak var ridingWithUserImageView2 : UIImageView!
    @IBOutlet fileprivate weak var ridingWithUserImageView3 : UIImageView!
    @IBOutlet fileprivate weak var ridingWithUserImageView4 : UIImageView!
    
    @IBOutlet fileprivate weak var bottomActionButton : UIButton!
    @IBOutlet fileprivate weak var callButton : UIButton!
    @IBOutlet fileprivate weak var chatButton : UIButton!
    @IBOutlet fileprivate weak var heightConstraintRidingWithContainerView : NSLayoutConstraint!

    @IBOutlet fileprivate weak var processingIndicatorLabel : UILabel!
    
    fileprivate var swipeDidStarted = false
    fileprivate var userInfo : NSDictionary?
    fileprivate var rideDate : Date?
    fileprivate weak var parentVC : UIViewController?

    func startUpInitialisations(){
        startUpConfigurations()
        updateUserInterfaceOnScreen()
        setupSwipeableView()
    }
    
    func startUpConfigurations(){
        profilePictureImageView.layoutIfNeeded()
        setBorder(profilePictureImageView, color: UIColor.clear, width: 0, cornerRadius: profilePictureImageView.bounds.size.width/2)
        setBorder(processingIndicatorLabel, color: APP_THEME_VOILET_COLOR , width: 1, cornerRadius: 1)
        userInformationHolderView.layer.masksToBounds = false
        
        ratingView.isUserInteractionEnabled = false
        
        setBorder(ridingWithUserImageView1, color: UIColor.clear, width: 0, cornerRadius: ridingWithUserImageView1.bounds.size.width/2)
        setBorder(ridingWithUserImageView2, color: UIColor.clear, width: 0, cornerRadius: ridingWithUserImageView2.bounds.size.width/2)
        setBorder(ridingWithUserImageView3, color: UIColor.clear, width: 0, cornerRadius: ridingWithUserImageView3.bounds.size.width/2)
        setBorder(ridingWithUserImageView4, color: UIColor.clear, width: 0, cornerRadius: ridingWithUserImageView4.bounds.size.width/2)
        
        let tapGestureRecogniser = UITapGestureRecognizer(target: self, action: #selector(UserMiniProfileView.userTapped))
        tapGestureRecogniser.numberOfTapsRequired = 1
        userInformationHolderView?.addGestureRecognizer(tapGestureRecogniser)
    }
    
    func userTapped(){
        completionBlock!(userInfo)
        hideAnimated()
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        
        self.processingIndicatorLabel.text = ""
        self.processingIndicatorLabel.hideActivityIndicator()
        self.processingIndicatorLabel.isHidden = true

        let info = userInfo
        userStatusColorView.backgroundColor = Acf().colorForUserAsPerTravelRequestStatus(self.userInfo!)
        profilePictureImageView.sd_setImage(with: URL(string: getUserProfilePictureUrlFromFileName(info!.object(forKey: "user_photo") as? String)), placeholderImage: USER_PLACEHOLDER_IMAGE)
        if isNotNull(info!.object(forKey: "rating")){
            ratingView.value = CGFloat("\(info!.object(forKey: "rating")!)".toFloat())
        }else{
            ratingView.value = 0.0
        }
        if isNotNull(info!.object(forKey: "rides_shared")){
            ridesDoneLabel.text = "\(info!.object(forKey: "rides_shared") as! String) Rides"
        }else{
            ridesDoneLabel.text = "0 Rides"
        }
        
        fullNameLabel.text = safeString(info!.object(forKey: "user_name"))
        statusTextLabel.text = getStatusTextFor(userInfo!)
        destinationNameLabel.text = safeString(info!.object(forKey: "company_name"))
        
        updateModeLabelType1(info!, modeLabel: self.modeLabel)
        
        if isNotNull(info!.object(forKey: "last_active")){
            let lastActive = (info!.object(forKey: "last_active") as! NSString)
            if lastActive.isEqual(to: "0")  {
                lastActiveLabel.text = "Active Today"
            }else{
                lastActiveLabel.text = "\(lastActive) days ago"
            }
        }else{
            lastActiveLabel.text = ""
        }
        
        travellingTime.text = getTimeFormatInAmPm(info!.object(forKey: "etd") as? String)
        
        setRidingWithDetails(info!, img1: ridingWithUserImageView1, img2: ridingWithUserImageView2, img3: ridingWithUserImageView3, img4: ridingWithUserImageView4)
        
        if isRidingWithDetailsExist(info){
            heightConstraintRidingWithContainerView.constant = 63
        }else{
            heightConstraintRidingWithContainerView.constant = 0
        }
        
        setUserTagDetails(info, tagImageView: self.tagBgImageView, tagLabel: self.tagLabel)

        setupRequiredActions()
        setupOfferView()
    }
    
    func setupRequiredActions(){
        callButton.isEnabled = false
        let situation = Acf().getTravelRequestStatus(self.userInfo!)
        if situation == .travelRequestAccepted {
            callButton.isEnabled = true
        }else if situation == .travelRequestSent || situation == .travelRequestReceived {
            callButton.isEnabled = safeBool(self.userInfo?.object(forKey: "show_phone"))
        }else if situation == .travelCompleted {
            callButton.isEnabled = true
        }
        updateRideActionButton(bottomActionButton,userInfo: self.userInfo!,isfullWidthCard: false)
    }
    
    @IBAction func onClickOfActionButton(){
        NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_RESET_IDLE_TIMER_RIDE_HOME), object: nil, userInfo: nil)

        if isSystemReadyToProcessThis(){
            let action = Acf().getRightSwipeOption(userInfo!)
            if action == .accept {
                self.acceptInvitation()
            }else if action == .settle {
                self.settlePayment()
            }else if action == .invite {
                self.sendInvitation()
            }else if action == .feedback {
                self.giveUserFeedback()
            }
        }
    }
    
    @IBAction func onClickOfCall(){
        NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_RESET_IDLE_TIMER_RIDE_HOME), object: nil, userInfo: nil)

        if isSystemReadyToProcessThis(){
            performAnimatedClickEffectType1(callButton)
            ACETelPrompt.callPhoneNumber(userInfo!.object(forKey: "user_mob") as? String, call: { (duration) -> Void in }) { () -> Void in}
        }
    }
    
    @IBAction func onClickOfChat(){
        NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_RESET_IDLE_TIMER_RIDE_HOME), object: nil, userInfo: nil)

        if isSystemReadyToProcessThis(){
            performAnimatedClickEffectType1(chatButton)
            if isNotNull(userInfo!.object(forKey: "traveller_id")){
                Acf().showChatScreen(userInfo!.object(forKey: "traveller_id") as! String)
            }
        }
    }
    
    func setupSwipeableView(){
        swipeableView.allowedDirection = .All
        swipeableView.nextView = {
            if self.swipeDidStarted{
                self.hide()
                return nil
            }
            return self.userInformationHolderView
        }
        swipeableView.didStart = {view, location in
            self.swipeDidStarted = true
        }
    }
    
    static func showMiniProfileView(_ userDetails:NSDictionary,rideDate:Date,onView:UIView,parentVC:UIViewController,autoHide:Bool,after:TimeInterval,completionBlock:@escaping UMPCompletionBlock,removedBlock:@escaping UMPCompletionBlock)->UserMiniProfileView{
        onView.viewWithTag(3264572)?.removeFromSuperview()
        let userMiniProfileView = Bundle.main.loadNibNamed("UserMiniProfileView", owner: self, options: nil)![0] as? UserMiniProfileView
        userMiniProfileView?.userInfo = userDetails
        userMiniProfileView?.tag = 3264572
        userMiniProfileView?.startUpInitialisations()
        userMiniProfileView?.rideDate = rideDate
        userMiniProfileView?.parentVC = parentVC
        onView.addSubview(userMiniProfileView!)

        userMiniProfileView!.snp_makeConstraints { (make) in
            make.top.equalTo(94)
            make.left.equalTo(0)
            make.right.equalTo(0)
            if isRidingWithDetailsExist(userDetails){
                make.height.equalTo(223)
            }else{
                make.height.equalTo(223-63)
            }
        }

        userMiniProfileView?.layoutIfNeeded()
        userMiniProfileView?.performAppearAnimationType6()
        if autoHide {
            NSObject.cancelPreviousPerformRequests(withTarget: userMiniProfileView!)
            userMiniProfileView?.perform(#selector(UserMiniProfileView.hideAnimated), with: nil, afterDelay: after)
        }
        userMiniProfileView?.completionBlock = completionBlock
        userMiniProfileView?.removedBlock = removedBlock
        return userMiniProfileView!
    }
    
    static func hideMiniProfileView(_ onView:UIView){
        onView.viewWithTag(3264572)?.removeFromSuperview()
    }
    
    func hide(){
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(UserMiniProfileView.hidePrivate), object: nil)
        self.perform(#selector(UserMiniProfileView.hidePrivate), with: nil, afterDelay: 0.3)
    }
    
    func hidePrivate(){
        removedBlock?(true)
        self.removeFromSuperview()
    }
    
    func hideAnimated(){
        UIView.animate(withDuration: 0.6, animations: { () -> Void in
            self.layer.opacity = 0.0
        }, completion: { (status) -> Void in
            self.hide()
        }) 
    }
    
    func finishRequestSuccessfully(){
        NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_NEED_USER_UPDATE), object: nil)
        self.processingIndicatorLabel.text = "Success"
        self.processingIndicatorLabel.hideActivityIndicator()
        func showSuccess(){
            self.bottomActionButton.isEnabled = false
            self.bottomActionButton.setTitle("Success 👍🏻", for: .normal)
            self.bottomActionButton.titleLabel?.font = UIFont.init(name: FONT_REGULAR,size: 13)
            self.bottomActionButton.setTitleColor(APP_THEME_GRAY_COLOR, for:.normal)
        }
        execMain({
            showSuccess()
        }, delay: 0.2)
        execMain({
            showSuccess()
        }, delay: 0.5)
    }
    
    func finishRequestFailing(){
        NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_NEED_USER_UPDATE), object: nil)
        self.processingIndicatorLabel.text = "Failed !"
        self.processingIndicatorLabel.hideActivityIndicator()
        execMain({[weak self]  in guard let `self` = self else { return }
            self.processingIndicatorLabel.isHidden = true
            },delay: 0.4)
    }
    
    func startRequest(_ message:String){
        var centerPoint = self.processingIndicatorLabel.center
        centerPoint.y = centerPoint.y + 20
        self.processingIndicatorLabel.showActivityIndicatorType(centerPoint, style: .gray)
        self.processingIndicatorLabel.isHidden = false
        self.processingIndicatorLabel.text = message
    }
    
    func sendInvitation(){
        if(isInternetConnectivityAvailable(true)==false){return}
        self.startRequest("Sending Invitation")
        let userInfo = Dbm().getUserInfo()
        let information = NSMutableDictionary()
        copyData(userInfo!.userId , destinationDictionary: information, destinationKey: "userId", methodName: #function)
        if self.parentVC! is  RideHomeViewController  {
            if let viewControllerObject = self.parentVC as? RideHomeViewController {
                copyData(viewControllerObject.trueForHomeToDestinationFalseforDestinationToHome ? "0" : "1", destinationDictionary: information, destinationKey: "mode", methodName: #function)
                copyData(getUserId(self.userInfo) , destinationDictionary: information, destinationKey: "otherUserId", methodName: #function)
                copyData(viewControllerObject.trueForHomeToDestinationFalseforDestinationToHome ? "0" : "1", destinationDictionary: information, destinationKey: "toMode", methodName: #function)
                copyData(viewControllerObject.selectedDayDate.toStringValue("yyyy-MM-dd"), destinationDictionary: information, destinationKey: "date", methodName: #function)
            }
        }else if self.parentVC! is  RideDetailViewController  {
            if let viewControllerObject = self.parentVC as? RideDetailViewController {
                copyData(viewControllerObject.trueForHomeToDestinationFalseforDestinationToHome ? "0" : "1", destinationDictionary: information, destinationKey: "mode", methodName: #function)
                copyData(getUserId(self.userInfo) , destinationDictionary: information, destinationKey: "otherUserId", methodName: #function)
                copyData(viewControllerObject.trueForHomeToDestinationFalseforDestinationToHome ? "0" : "1", destinationDictionary: information, destinationKey: "toMode", methodName: #function)
                copyData(viewControllerObject.selectedDayDate.toStringValue("yyyy-MM-dd"), destinationDictionary: information, destinationKey: "date", methodName: #function)
            }
        }
        let scm = ServerCommunicationManager()
        scm.showSuccessResponseMessage = true
        scm.sendInviteForRide(information) { (responseData) -> () in
            if let _ = responseData {
                self.finishRequestSuccessfully()
                execMain({ (completed) in
                    self.showCallOptionPopupIfRequired()
                },delay:0.4)
            }else{
                self.finishRequestFailing()
            }
        }
    }
    
    func showCallOptionPopupIfRequired(){
        if safeBool(self.userInfo?.object(forKey: "show_phone")){
            let prompt = UIAlertController(title: "Great", message: "\nCall \(safeString(self.userInfo?.object(forKey: "user_name")).firstName()) right now to confirm your ride!", preferredStyle: UIAlertControllerStyle.alert)
            prompt.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
            prompt.addAction(UIAlertAction(title: "Call", style: UIAlertActionStyle.cancel, handler: { (action) -> Void in
                self.onClickOfCall()
            }))
            Acf().navigationController!.present(prompt, animated: true, completion: nil)
        }
    }
    
    func acceptInvitation(){
        if(isInternetConnectivityAvailable(true)==false){return}
        self.startRequest("Accepting Invitation")
        let userInfo = Dbm().getUserInfo()
        let information = NSMutableDictionary()
        copyData(userInfo!.userId , destinationDictionary: information, destinationKey: "userId", methodName: #function)
        copyData(self.userInfo?.object(forKey: "id") , destinationDictionary: information, destinationKey: "requestId", methodName: #function)
        let scm = ServerCommunicationManager()
        scm.showSuccessResponseMessage = true
        scm.messageOption = .showSuccessResponseMessageUsingPopUp
        scm.acceptInvitationForRide(information) { (responseData) -> () in
            if let _ = responseData {
                trackFunctionalEvent(FE_ACCEPT_RIDE_INVITE,information:information)
                self.finishRequestSuccessfully()
            }else{
                trackFunctionalEvent(FE_ACCEPT_RIDE_INVITE,information:information,isSuccess:false)
                self.finishRequestFailing()
            }
        }
    }
    
    func rejectInvitation(){
        if(isInternetConnectivityAvailable(true)==false){return}
        self.startRequest("Cancelling Invitation")
        let userInfo = Dbm().getUserInfo()
        let information = NSMutableDictionary()
        copyData(userInfo!.userId , destinationDictionary: information, destinationKey: "userId", methodName: #function)
        copyData(self.userInfo?.object(forKey: "id") , destinationDictionary: information, destinationKey: "requestId", methodName: #function)
        let scm = ServerCommunicationManager()
        scm.rejectInvitationForRide(information) { (responseData) -> () in
            if let _ = responseData {
                trackFunctionalEvent(FE_REJECT_RIDE_INVITE,information:information)
                self.finishRequestSuccessfully()
            }else{
                trackFunctionalEvent(FE_REJECT_RIDE_INVITE,information:information,isSuccess:false)
                self.finishRequestFailing()
            }
        }
    }
    
    func cancelRide(){
        if(isInternetConnectivityAvailable(true)==false){return}
        self.startRequest("Cancelling Ride")
        let userInfo = Dbm().getUserInfo()
        let information = NSMutableDictionary()
        copyData(userInfo!.userId , destinationDictionary: information, destinationKey: "userId", methodName: #function)
        copyData(self.userInfo?.object(forKey: "id") , destinationDictionary: information, destinationKey: "requestId", methodName: #function)
        let scm = ServerCommunicationManager()
        scm.cancelRide(information) { (responseData) -> () in
            if let _ = responseData {
                RideEventManager.sharedInstance.cancelMyRide()
                self.finishRequestSuccessfully()
            }else{
                self.finishRequestFailing()
            }
        }
    }
    
    func settlePayment(){
        if(isInternetConnectivityAvailable(true)==false){return}
        self.startRequest("Settling Payment")
        let userInfo = Dbm().getUserInfo()
        let information = NSMutableDictionary()
        copyData(userInfo!.userId , destinationDictionary: information, destinationKey: "userId", methodName: #function)
        copyData(self.userInfo?.object(forKey: "id") , destinationDictionary: information, destinationKey: "requestId", methodName: #function)
        copyData(rideDate!.toStringValue("yyyy-MM-dd") , destinationDictionary: information, destinationKey: "date", methodName: #function)
        let scm = ServerCommunicationManager()
        scm.settleRide(information) { (responseData) -> () in
            if let _ = responseData {
                if let status = (responseData?.object(forKey: "data") as? NSDictionary)?.object(forKey: "status") as? String {
                    if let payment = (responseData?.object(forKey: "data") as? NSDictionary)?.object(forKey: "payment") as? String {
                        let message = "\(status)\n\(payment)"
                        showAlert("Payment", message: message)
                    }
                }
                self.finishRequestSuccessfully()
            }else{
                self.finishRequestFailing()
            }
        }
    }
    
    func setupOfferView(){
        let offerType = safeString(self.userInfo?.object(forKey: "offer_type"))
        let rideCost = safeString(self.userInfo?.object(forKey: "ride_cost"))
        offerContainerView.isHidden = false
        if offerType == "cashback" || offerType == "discount" {
            offerViewType1.isHidden = false
            freeRideOfferView.isHidden = true
            offerViewType1ValueLabel.text = "₹\(rideCost)"
            offerViewType1ValueLabel.setAttributedTextWithOptimisationsType2(fontSize:7)
        }else if offerType == "free" {
            offerViewType1.isHidden = true
            freeRideOfferView.isHidden = false
        }else if offerType == "no_offer" {
            offerViewType1.isHidden = false
            freeRideOfferView.isHidden = true
            offerViewType1ValueLabel.text = "₹\(rideCost)"
            offerViewType1ValueLabel.setAttributedTextWithOptimisationsType2(fontSize:7)
        }else{
            offerContainerView.isHidden = true
        }
    }

    func giveUserFeedback(){
        if(isInternetConnectivityAvailable(true)==false){return}
        Acf().showFeedbackScreen(self.userInfo!)
    }
    
    
    @IBAction func onClickOfOfferButton(){
        let offerType = safeString(self.userInfo?.object(forKey: "offer_type"))
        if isNotNull(offerType) {
            Acf().showOfferDetails(userInfo: self.userInfo!)
        }
    }
    
}

