//
//  GroupDetailQuickActionsView.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit

@IBDesignable
class GroupDetailQuickActionsView: UIView {
    
    var group : Group?
    var navigationController:UINavigationController?

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        initializeSubviews()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeSubviews()
    }
    
    private func initializeSubviews() {
        let view: UIView = Bundle.main.loadNibNamed("GroupDetailQuickActionsView", owner: self, options: nil)![0] as! UIView
        self.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor,constant:0).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor,constant:0).isActive = true
        view.topAnchor.constraint(equalTo: self.topAnchor,constant:0).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor,constant:0).isActive = true
    }
    
    @IBAction private func onClickOfHelpButton(){
        Acf().pushVC("HelpViewController", navigationController: self.navigationController, isRootViewController: false, animated: true) { (viewController) in
        }
    }
    
    @IBAction private func onClickOfCheckInButton(){
        Acf().pushVC("CheckInViewController", navigationController: self.navigationController, isRootViewController: false, animated: true) { (viewController) in
            (viewController as! CheckInViewController).group = self.group
        }
    }
    
    @IBAction private func onClickOfPlacesButton(){
        Acf().pushVC("PlacesViewController", navigationController: self.navigationController, isRootViewController: false, animated: true) { (viewController) in
            (viewController as! PlacesViewController).group = self.group
        }
    }
    
}
