//
//  UserType1MKAnnotationView.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit

class UserType1MKAnnotationView : MKAnnotationView {
    
    //MARK: - variables and constants
    
    fileprivate var annotationBackgroundImageView : UIImageView?
    fileprivate var annotationUserImageView : UIImageView?
    fileprivate var bgImage:UIImage?
    fileprivate var userImageUrl:String?
    var userInfo:NSDictionary?
    
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setup() {
        if userInfo != nil {
            if annotationBackgroundImageView == nil {
                annotationBackgroundImageView = UIImageView()
                annotationBackgroundImageView?.frame = CGRect(x: 0, y: 0, width: 33, height: 47)
                annotationBackgroundImageView?.center = self.center
                annotationBackgroundImageView?.contentMode = UIViewContentMode.scaleAspectFit
                annotationBackgroundImageView?.isUserInteractionEnabled = true
                self.addSubview(annotationBackgroundImageView!)
            }
            if annotationUserImageView == nil {
                annotationUserImageView = UIImageView()
                annotationUserImageView?.image = bgImage
                annotationUserImageView?.frame = CGRect(x: 2, y: 3, width: 29, height: 29)
                annotationUserImageView?.contentMode = UIViewContentMode.scaleAspectFill
                setBorder(annotationUserImageView!, color: UIColor.clear, width: 2, cornerRadius: (annotationUserImageView?.bounds.size.width)!/2)
                annotationUserImageView?.isUserInteractionEnabled = true
                annotationBackgroundImageView!.addSubview(annotationUserImageView!)
            }
            let situation = Acf().getTravelRequestStatus(self.userInfo!)
            if situation == .notInitialisedYet {
                bgImage = UIImage(named: "locatorParty")
            }
            else if situation == .travelRequestSent {
                bgImage = UIImage(named: "locatorBParty2")
            }
            else if situation == .travelRequestReceived {
                bgImage = UIImage(named: "locatorBParty2")
            }
            else if situation == .travelRequestRejected {
                bgImage = UIImage(named: "locatorParty")
            }
            else if situation == .travelRequestRejectedByMe {
                bgImage = UIImage(named: "locatorParty")
            }
            else if situation == .travelRequestAccepted {
                bgImage = UIImage(named: "locatorBParty4")
            }
            else if situation == .travelCancelled {
                bgImage = UIImage(named: "locatorParty")
            }
            else if situation == .travelCompleted {
                bgImage = UIImage(named: "locatorBParty3")
            }else{
                bgImage = UIImage(named: "locatorParty")
            }
            
            annotationBackgroundImageView?.image = bgImage!
            annotationUserImageView?.sd_setImage(with: URL(string:getUserProfilePictureUrlFromFileName(userInfo!.object(forKey: "user_photo") as? String)), placeholderImage: USER_PLACEHOLDER_IMAGE)
        }
    }
}

