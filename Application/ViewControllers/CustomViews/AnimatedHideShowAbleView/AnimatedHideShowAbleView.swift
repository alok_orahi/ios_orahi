//
//  AnimatedHideShowAbleView.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit

class AnimatedHideShowAbleView : UIView {
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if self.layer.opacity < 0.99 {
            show()
        }
    }
    
    func show(){
        show(10.0)
    }
    
    func showIfRequired(){
        if self.layer.opacity < 0.99 {
            show(10.0)
        }
    }
    
    func show(_ hideAfter:TimeInterval){
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            self.layer.opacity = 0.99
        }, completion: { (completed) -> Void in
        }) 
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(AnimatedHideShowAbleView.hide), object: nil)
        self.perform(#selector(AnimatedHideShowAbleView.hide), with: nil, afterDelay: hideAfter)
    }

    func hide(){
        UIView.animate(withDuration: 1.6, animations: { () -> Void in
            self.layer.opacity = 0.3
            }, completion: { (completed) -> Void in
        }) 
    }
}

