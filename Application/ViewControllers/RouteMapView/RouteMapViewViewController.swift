//
//  MapViewViewController.swift
//  Orahi
//
//  Created by Alok Singh on 06/06/16.
//  Copyright (c) 2016 Orahi. All rights reserved.
//


import UIKit
import MapKit

class MapViewViewController: UIViewController,UITextFieldDelegate,MKMapViewDelegate {
    
    //MARK: - variables and constants
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var addressLabel: UILabel!
    
    var locationCoordinate: CLLocationCoordinate2D?
    var address: String?
    
    var routes: NSMutableArray?
    
    //MARK: - view controller life cycle methods
    override func prefersStatusBarHidden() -> Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.navigationBarHidden)!
        }else{
            return false
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startupInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    //MARK: - other methods
    func setupForNavigationBar(){
        setAppearanceForNavigationBarType2(self.navigationController)
        self.navigationController?.navigationBar.hidden = true
    }
    
    func registerForNotifications(){
    }
    
    func startupInitialisations(){
        setAppearanceForViewController(self)
        trackScreen(humanReadableScreenName("\(self.dynamicType)"))
        setupForMapView()
    }
    
    func updateUserInterfaceOnScreen(){
        addressLabel.text = address
    }
    
    func setupForMapView() {
        mapView.delegate = self
        if isNotNull(locationCoordinate){
            let annotation = MKPointAnnotation()
            annotation.coordinate = locationCoordinate!
            mapView.addAnnotation(annotation)
            updateMapRegion(locationCoordinate!)
        }else if isNotNull(routes){
            
        }
    }
    
    func updateMapRegion(location:CLLocationCoordinate2D){
        let latDelta:CLLocationDegrees = 0.08
        let lonDelta:CLLocationDegrees = 0.08
        let span:MKCoordinateSpan = MKCoordinateSpanMake(latDelta, lonDelta)
        let region:MKCoordinateRegion = MKCoordinateRegionMake(location, span)
        mapView.setRegion(region, animated: false)
    }
    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView?{
        var pulsatingView = self.mapView.dequeueReusableAnnotationViewWithIdentifier("location") as! SVPulsingAnnotationView?
        if isNull(pulsatingView) {
            pulsatingView = SVPulsingAnnotationView(annotation: annotation, reuseIdentifier: "location")
            pulsatingView!.annotationColor = APP_THEME_COLOR
        }
        return pulsatingView
    }
    
    //MARK: - other functions
}
