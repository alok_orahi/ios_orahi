//
//  WithdrawlViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//



import UIKit

class WithdrawlViewController: UIViewController {
    
    //MARK: - variables and constants
    @IBOutlet fileprivate weak var accountBalanceLabel : UILabel?
    @IBOutlet fileprivate weak var fullNameTextfield: MKTextField!
    @IBOutlet fileprivate weak var tokensTextfield: MKTextField!
    @IBOutlet fileprivate weak var bankNameTextfield: MKTextField!
    @IBOutlet fileprivate weak var accountNumberTextfield: MKTextField!
    @IBOutlet fileprivate weak var panNumberTextfield: MKTextField!
    @IBOutlet fileprivate weak var ifscCodeTextfield: MKTextField!
    @IBOutlet fileprivate weak var doneButton: TKTransitionSubmitButton!
    @IBOutlet fileprivate weak var scrollView: UIScrollView!
    @IBOutlet fileprivate weak var scrollViewSubView: UIView!
    var preSelectionAmount = ""

    
    
    //MARK: - view controller life cycle methods
    
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scrollViewSubView.layoutIfNeeded()
        scrollViewSubView.translatesAutoresizingMaskIntoConstraints = true
        scrollViewSubView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: scrollViewSubView.frame.size.height)
        scrollView.contentSize = scrollViewSubView.frame.size
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
        Acf().setupIQKeyboardManagerEnabled()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        CacheManager.sharedInstance.saveObject(fullNameTextfield.text , identifier: "fullNameTextfield")
        CacheManager.sharedInstance.saveObject(panNumberTextfield.text , identifier: "panNumberTextfield")
    }
    
    //MARK: - other methods
    private func setupForNavigationBar(){
        setAppearanceForNavigationBarType1(self.navigationController)
        setupNavigationBarTitleType1("Withdrawl",viewController: self)
        if self.isBackButtonRequired() {
            addNavigationBarButton(self, image: UIImage(named: "backarrowblack"), title: nil, isLeft: true)
        }
    }
    
    private func registerForNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(WithdrawlViewController.updateAppearanceOfTextField), name: NSNotification.Name.UITextViewTextDidBeginEditing, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(WithdrawlViewController.updateAppearanceOfTextField), name: NSNotification.Name.UITextViewTextDidEndEditing, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(WithdrawlViewController.updateAppearanceOfTextField), name: NSNotification.Name.UITextFieldTextDidBeginEditing, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(WithdrawlViewController.updateAppearanceOfTextField), name: NSNotification.Name.UITextFieldTextDidEndEditing, object: nil)
    }
    
    private func startUpInitialisations(){
        setAppearanceForViewController(self)
        if isNotNull(CacheManager.sharedInstance.loadObject("ACCOUNT_SUMMARY")){
            let details = CacheManager.sharedInstance.loadObject("ACCOUNT_SUMMARY") as! NSDictionary
            self.accountBalanceLabel?.text = details.object(forKey: "accountBal") as? String
        }
        
        setAppearanceForMKTextField(fullNameTextfield)
        setAppearanceForMKTextField(tokensTextfield)
        setAppearanceForMKTextField(bankNameTextfield)
        setAppearanceForMKTextField(accountNumberTextfield)
        setAppearanceForMKTextField(panNumberTextfield)
        setAppearanceForMKTextField(ifscCodeTextfield)
        
        updateAppearanceOfTextField()
        fetchAndCacheAccountSummary()
        
        doneButton.layer.cornerRadius = doneButton.bounds.size.width/2
        doneButton.layer.masksToBounds = true
        doneButton.requiredBackgroundColor = UIColor(red: 100/255, green: 196/255, blue: 219/255, alpha: 1.0)
        self.scrollView.setContentOffset(CGPoint(x: 0,y: 110),animated:false)
        
        if isNotNull(preSelectionAmount){
            tokensTextfield.text = safeString(safeInt(preSelectionAmount))
        }
        trackScreenLaunchEvent(SLE_WITHDRAWL)
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        fullNameTextfield.text = CacheManager.sharedInstance.loadObject("fullNameTextfield") as? String
        panNumberTextfield.text = CacheManager.sharedInstance.loadObject("panNumberTextfield") as? String
        if isNull(fullNameTextfield.text){
            let userInfo = Dbm().getUserInfo()
            fullNameTextfield.text = userInfo!.name
        }
    }
    
    //MARK: - other functions
    
    func onClickOfLeftBarButton(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    func canContinueForWithdrawl()->(Bool){
        var canContinue = true
        if canContinue {
            canContinue = validateName(fullNameTextfield.text, identifier: "Full Name")
            if canContinue == false {
                fullNameTextfield.becomeFirstResponder()
            }
        }
        if canContinue {
            canContinue = validateNumber(tokensTextfield.text as NSString?, identifier: "Tokens")
            if canContinue == false {
                tokensTextfield.becomeFirstResponder()
            }
        }
        if canContinue {
            canContinue = validateName(bankNameTextfield.text, identifier: "Bank Name")
            if canContinue == false {
                bankNameTextfield.becomeFirstResponder()
            }
        }
        if canContinue {
            canContinue = validateNumber(accountNumberTextfield.text as NSString?, identifier: "Bank Account Number")
            if canContinue == false {
                accountNumberTextfield.becomeFirstResponder()
            }
        }
        if canContinue {
            canContinue = validateIfNull(panNumberTextfield.text , identifier: "PAN number")
            if canContinue == false {
                panNumberTextfield.becomeFirstResponder()
            }
        }
        if canContinue {
            canContinue = validateIfNull(ifscCodeTextfield.text , identifier: "Branch IFSC Code")
            if canContinue == false {
                ifscCodeTextfield.becomeFirstResponder()
            }
        }
        
        return canContinue
    }
    
    
    @IBAction func onClickOfDoneButton(){
        if(isInternetConnectivityAvailable(true)==false){return}
        if canContinueForWithdrawl(){
            let information = NSMutableDictionary();
            copyData(fullNameTextfield.text , destinationDictionary: information, destinationKey: "name", methodName: #function)
            copyData(tokensTextfield.text , destinationDictionary: information, destinationKey: "tokens", methodName: #function)
            copyData(panNumberTextfield.text , destinationDictionary: information, destinationKey: "pan", methodName: #function)
            copyData(bankNameTextfield.text , destinationDictionary: information, destinationKey: "bankName", methodName: #function)
            copyData(accountNumberTextfield.text , destinationDictionary: information, destinationKey: "accountNumber", methodName: #function)
            copyData(ifscCodeTextfield.text , destinationDictionary: information, destinationKey: "ifsc", methodName: #function)
            doneButton.startLoadingAnimation()
            let scm = ServerCommunicationManager()
            scm.withdrawl(information) { (responseData) -> () in
                if let _ = responseData {
                    showNotification(MESSAGE_TEXT___FOR_WITHDRAWL_SUCCESSFULL  , showOnNavigation: true, showAsError: false)
                    self.navigationController?.popViewController(animated: true)
                }
                self.doneButton.stopIt()
            }
        }
    }
    
    func updateAppearanceOfTextField(){
        Acf().updateAppearanceOfTextFieldType1(fullNameTextfield)
        Acf().updateAppearanceOfTextFieldType1(tokensTextfield)
        Acf().updateAppearanceOfTextFieldType1(bankNameTextfield)
        Acf().updateAppearanceOfTextFieldType1(accountNumberTextfield)
        Acf().updateAppearanceOfTextFieldType1(panNumberTextfield)
        Acf().updateAppearanceOfTextFieldType1(ifscCodeTextfield)
        updateAppearanceOfDoneButton()
    }
    
    func updateAppearanceOfDoneButton(){
        if fullNameTextfield.text?.length > 0 && tokensTextfield.text?.length > 0 && bankNameTextfield.text?.length > 0 && accountNumberTextfield.text?.length > 0 && panNumberTextfield.text?.length > 0 && ifscCodeTextfield.text?.length > 0{
            doneButton.isEnabled = true
        }else{
            doneButton.isEnabled = false
        }
    }
}
