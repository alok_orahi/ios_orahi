//
//  AuthenticationStep5ViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit

class AuthenticationStep5ViewController: UIViewController {
    
    //MARK: - variables and constants
    @IBOutlet fileprivate weak var infoLabel: UILabel!
    @IBOutlet fileprivate weak var carRegistrationNumberTextfield: MKTextField!
    @IBOutlet fileprivate weak var carMakeTextfield: MKTextField!
    @IBOutlet fileprivate weak var carModelTextfield: MKTextField!
    @IBOutlet fileprivate weak var actionButton: TKTransitionSubmitButton!
    
    fileprivate var carMakes = NSMutableArray()
    fileprivate var carModels = NSMutableArray()

    fileprivate var arrayCarMakeOptions = NSMutableArray()
    fileprivate var arrayCarModelOptions = NSMutableArray()

    fileprivate var selectedIndexCarMakeOptions = 0
    fileprivate var selectedIndexCarModelOptions = 0

    //MARK: - view controller life cycle methods
    
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
        Acf().updateCityCompanyCollegeCarOptionsList()
    }
        
    //MARK: - other methods
    
    private func setupForNavigationBar(){
        setAppearanceForNavigationBarType2(self.navigationController)
        if self.isBackButtonRequired() {
            addNavigationBarButton(self, image: UIImage(named: "backarrowblack"), title: nil, isLeft: true,forceWhiteTint:false)
        }else{
            self.navigationItem.leftBarButtonItem = nil
            self.navigationItem.setHidesBackButton(true, animated:false)
        }
    }
    
    private func registerForNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(AuthenticationStep5ViewController.updateAppearanceOfTextField), name: NSNotification.Name.UITextViewTextDidBeginEditing, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AuthenticationStep5ViewController.updateAppearanceOfTextField), name: NSNotification.Name.UITextViewTextDidEndEditing, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AuthenticationStep5ViewController.updateAppearanceOfTextField), name: NSNotification.Name.UITextFieldTextDidBeginEditing, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AuthenticationStep5ViewController.updateAppearanceOfTextField), name: NSNotification.Name.UITextFieldTextDidEndEditing, object: nil)
    }
    
    private func startUpInitialisations(){
        setAppearanceForViewController(self)
        setAppearanceForMKTextField(carRegistrationNumberTextfield)
        setAppearanceForMKTextField(carMakeTextfield)
        setAppearanceForMKTextField(carModelTextfield)
        updateAppearanceOfTextField()
        self.actionButton.performAppearAnimationType1()
        infoLabel.attributedText = getAttributedTitleStringType3("GREAT!\nPlease provide your vehicle details", regularText: "GREAT!", anotherText: "Please provide your vehicle details")
        trackScreenLaunchEvent(SLE_ENTER_CAR_DETAILS)
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        let settings = Dbm().getSetting()
        carRegistrationNumberTextfield.text = safeString(settings.carRegistrationNo)
        carMakeTextfield.text = safeString(settings.carMake)
        carModelTextfield.text = safeString(settings.carModel)
        
        carMakes = Dbm().getAllCarMake()
        carModels = Dbm().getAllCarModel()

        for carMake in carMakes {
            arrayCarMakeOptions.add((carMake as! CarMake).name)
            if (carMakeTextfield.text?.length > 0) && ((carMake as! CarMake).name as! NSString).isEqual(to: carMakeTextfield.text!){
                selectedIndexCarMakeOptions = arrayCarMakeOptions.count - 1
            }
        }
        for carModel in carModels{
            arrayCarModelOptions.add((carModel as! CarModel).name)
            if (carModelTextfield.text?.length > 0) && ((carModel as! CarModel).name as! NSString).isEqual(to: carModelTextfield.text!){
                selectedIndexCarModelOptions = arrayCarModelOptions.count - 1
            }
        }

    }
    
    func onClickOfLeftBarButton(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    func canContinueForProfileUpdate()->(Bool){
        var canContinue = true
        if canContinue {
            canContinue = validateName(carRegistrationNumberTextfield.text, identifier: "Registration Number")
            if canContinue == false {
                carRegistrationNumberTextfield.becomeFirstResponder()
            }
        }
        if canContinue {
            canContinue = validateIfNull(carMakeTextfield.text , identifier: "Car's Make")
            if canContinue == false {
            }
        }
        if canContinue {
            canContinue = validateIfNull(carModelTextfield.text , identifier: "Car's Model")
            if canContinue == false {
            }
        }
        return canContinue
    }
    
    @IBAction func onClickOfActionButton(){
        if(isInternetConnectivityAvailable(true)==false){return}
        if canContinueForProfileUpdate(){
            let userInfo = Dbm().getUserInfo()
            let settings = Dbm().getSetting()
            settings.carRegistrationNo = carRegistrationNumberTextfield.text!
            settings.carMake = carMakeTextfield.text!
            settings.carModel = carModelTextfield.text!
            Dbm().saveChanges()
            actionButton.startLoadingAnimation()
            let information = NSMutableDictionary()
            let trueForOwnerFalseforPassenger = userInfo!.isPassenger == "0"
            copyData(userType(), destinationDictionary: information, destinationKey: "userType", methodName: #function)
            copyData(trueForOwnerFalseforPassenger ? "carowner" : "passenger" , destinationDictionary: information, destinationKey: "travelMode", methodName: #function)
            copyData(settings.carRegistrationNo , destinationDictionary: information, destinationKey: "carRegistrationNumber", methodName: #function)
            for carMake in Dbm().getAllCarMake(){
                if isNotNull(settings.carMake)&&(settings.carMake?.length > 0) && ((carMake as! CarMake).name as! NSString).isEqual(to: settings.carMake!){
                    copyData((carMake as! CarMake).mkId , destinationDictionary: information, destinationKey: "make", methodName: #function)
                    break
                }
            }
            for carModel in Dbm().getAllCarModel(){
                if isNotNull(settings.carModel)&&(settings.carModel?.length > 0) && ((carModel as! CarModel).name as! NSString).isEqual(to: settings.carModel!){
                    copyData((carModel as! CarModel).mdId , destinationDictionary: information, destinationKey: "model", methodName: #function)
                    break
                }
            }
            let scm = ServerCommunicationManager()
            scm.updateUserProfile(information) { (responseData) -> () in
                if let _ = responseData {
                    updateUserProfile(loggedInUserId(), completion: { (returnedData) in
                        if safeBool(returnedData) {
                            decideAndShowNextScreen(self.navigationController!)
                        }
                        self.actionButton.stopIt()
                    })
                }else{
                    self.actionButton.stopIt()
                }
            }
        }
    }
    
    @IBAction func onClickOfSelectCarMake(){
        resignKeyboard()
        Acf().showCommonPickerScreen(arrayCarMakeOptions,titleToShow: "Your Car Make", nc: self.navigationController) { (returnedData) in
            if isNotNull(returnedData){
                self.selectedIndexCarMakeOptions = self.arrayCarMakeOptions.index(of: returnedData!)
                self.carMakeTextfield.text = (returnedData as! String)
                self.selectedIndexCarModelOptions = 0
                self.carModelTextfield.text = nil
            }
        }
    }
    
    @IBAction func onClickOfSelectCarModel(){
        resignKeyboard()
        updateAvailableCarModelList()
        Acf().showCommonPickerScreen(arrayCarModelOptions,titleToShow: "Your Car Model", nc: self.navigationController) { (returnedData) in
            if isNotNull(returnedData){
                self.selectedIndexCarModelOptions = self.arrayCarModelOptions.index(of: returnedData!)
                self.carModelTextfield.text = (returnedData as! String)
            }
        }
    }
    
    func updateAvailableCarModelList() {
        arrayCarModelOptions.removeAllObjects()
        if arrayCarMakeOptions.count > self.selectedIndexCarMakeOptions {
            let carMake = carMakes.object(at: self.selectedIndexCarMakeOptions)
            let makeId = (carMake as! CarMake).mkId
            for carModel in carModels{
                if ((carModel as! CarModel).mkId! as NSString).isEqual(to: makeId!){
                    arrayCarModelOptions.add((carModel as! CarModel).name)
                }
            }
        }
    }
    
    func updateAppearanceOfTextField(){
        Acf().updateAppearanceOfTextFieldType1(carRegistrationNumberTextfield)
        Acf().updateAppearanceOfTextFieldType1(carMakeTextfield)
        Acf().updateAppearanceOfTextFieldType1(carModelTextfield)
    }
}
