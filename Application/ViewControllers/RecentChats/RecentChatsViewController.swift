//
//  RecentChatsViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class RecentChatsViewController: UIViewController, QMChatServiceDelegate, QMChatConnectionDelegate,DZNEmptyDataSetDelegate,DZNEmptyDataSetSource,UIViewControllerPreviewingDelegate,UITableViewDelegate,UITableViewDataSource {
    
    //MARK: - variables and constants
    @IBOutlet var searchBar : UISearchBar!
    @IBOutlet var tableView : UITableView!
    
    fileprivate var didEnterBackgroundDate: Date?
    fileprivate var suggestedUsersList = NSMutableArray()
    fileprivate var section1ContentsArray = NSMutableArray()
    fileprivate var section2ContentsArray = NSMutableArray()
    
    //MARK: - view controller life cycle methods
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
        setupForQuickBlox()
        fetchAndShowSuggestedUsersList()
        updateUIForUpdates()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
        updateUserInterfaceOnScreen()
        setupForQuickBlox()
        fetchAndShowSuggestedUsersList()
    }
    
    
    //MARK: - other methods
    private func setupForNavigationBar(){
        setAppearanceForNavigationBarType1(self.navigationController)
        setupNavigationBarTitleType1("Chats",viewController: self)
        addNavigationBarButton(self, image: UIImage(named: "menu"), title: nil, isLeft: true, observer: self)
        addNavigationBarButton(self, title: "+", titleColor: APP_THEME_VOILET_COLOR, fontSize: 32, isLeft: false)
    }
    
    func onClickOfLeftBarButton(_ sender:Any){
        Acf().sideMenuController?.presentLeftMenuViewController()
    }
    
    func onClickOfRightBarButton(_ sender:Any){
        if isInternetConnectivityAvailable(true) && Acf().isQBReadyForItsUserDependentServices() {
            if isSystemReadyToProcessThis() {
                Acf().pushVC("NewChatViewController", navigationController: self.navigationController, isRootViewController: false, animated: true, modifyObject: nil)
            }
        }
    }
    
    private func registerForNotifications(){
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIApplicationDidBecomeActive, object: nil, queue: OperationQueue.main) { (notification: Foundation.Notification) -> Void in
            if !QBChat.instance().isConnected {
                logMessage("Connecting to chat...")
            }
        }
        NotificationCenter.default.addObserver(self, selector: #selector(RecentChatsViewController.didEnterBackgroundNotification), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
    }
    
    private func startUpInitialisations(){
        setAppearanceForViewController(self)
        setupForQuickBlox()
        self.tableView!.emptyDataSetSource = self
        self.tableView!.emptyDataSetDelegate = self
        setupFor3DTouchPreviewIfAvailable()
        trackScreenLaunchEvent(SLE_RECENT_CHAT)
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        Acf().updateBadgeValues()
    }
    
    
    func setupForQuickBlox(){
        ServicesManager.instance().chatService.removeDelegate(self)
        ServicesManager.instance().chatService.addDelegate(self)
        if (QBChat.instance().isConnected) {
            self.getDialogs()
        }
        updateSection1ContentsArray()
    }
    
    //MARK: - other methods
    
    func didEnterBackgroundNotification() {
        self.didEnterBackgroundDate = Date()
    }
    
    func getDialogs() {
        updateSection1ContentsArray()
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(RecentChatsViewController.getDialogsPrivate), object: nil)
        self.perform(#selector(RecentChatsViewController.getDialogsPrivate), with: nil, afterDelay: 5.0)
    }
    
    func getDialogsPrivate() {
        if (ServicesManager.instance().lastActivityDate != nil) {
            ServicesManager.instance().chatService.fetchDialogsUpdated(from: ServicesManager.instance().lastActivityDate! as Date, andPageLimit: kDialogsPageLimit, iterationBlock: { (response: QBResponse?, dialogObjects: [QBChatDialog]?, dialogsUsersIDs: Set<NSNumber>?, stop: UnsafeMutablePointer<ObjCBool>) -> Void in
            }, completionBlock: { (response: QBResponse?) -> Void in
                guard let unwrappedResponse = response else { logMessage("fetchDialogsUpdatedFromDate error"); return }
                guard unwrappedResponse.isSuccess else {logMessage("fetchDialogsUpdatedFromDate error \(response)"); return }
                ServicesManager.instance().lastActivityDate = Date()
                self.updateSection1ContentsArray()
            })
        }else {
            ServicesManager.instance().chatService.allDialogs(withPageLimit: kDialogsPageLimit, extendedRequest: nil, iterationBlock: { (response: QBResponse?, dialogObjects: [QBChatDialog]?, dialogsUsersIDS: Set<NSNumber>?, stop: UnsafeMutablePointer<ObjCBool>) -> Void in
            }, completion: { (response: QBResponse?) -> Void in
                guard response != nil && response!.isSuccess else { return }
                ServicesManager.instance().lastActivityDate = Date()
                self.updateSection1ContentsArray()
            })
        }
        Acf().updateBadgeValues()
    }
    
    // MARK: - DataSource
    
    static func dialogs() -> Array<QBChatDialog> {
        return ServicesManager.instance().chatService.dialogsMemoryStorage.dialogsSortByUpdatedAt(withAscending: false) as Array<QBChatDialog>
    }
    
    func updateSection1ContentsArray() {
        section1ContentsArray = NSMutableArray(array: RecentChatsViewController.dialogs())
        if searchBar.text?.length > 0 {
            func isMatching(_ chatDialog:QBChatDialog,text:String)->Bool {
                if chatDialog.type == .private {
                    if chatDialog.recipientID > 0 {
                        let nameToDisplayAsString = nameToDisplay(ServicesManager.instance().usersService.usersMemoryStorage.user(withID: UInt(chatDialog.recipientID)),chatDialogue: chatDialog,onlyFirstName: false)
                        return nameToDisplayAsString.lowercased().contains(text)
                    }else{
                        return displayNameFromName(chatDialog.name)!.lowercased().contains(text)
                    }
                }else{
                    return displayNameFromName(chatDialog.name)!.lowercased().contains(text)
                }
            }
            let textToSearch = searchBar.text!.lowercased()
            section1ContentsArray = section1ContentsArray.filter({ return isMatching(($0 as! QBChatDialog), text: textToSearch) }) as! NSMutableArray
        }
    }
    
    func updateSection2ContentsArray() {
        section2ContentsArray = self.suggestedUsersList.mutableCopy() as! NSMutableArray
        if searchBar.text?.length > 0 {
            func isMatching(_ contact:NSDictionary,text:String)->Bool {
                if let name = contact.object(forKey: "user_name"){
                    return (name as AnyObject).lowercased.contains(text)
                }
                return false
            }
            let textToSearch = searchBar.text!.lowercased()
            section2ContentsArray = section2ContentsArray.filter({ return isMatching(($0 as! NSDictionary), text: textToSearch) }) as! NSMutableArray
        }
    }
    
    //MARK: - DZNEmptyDataSet Delegate & Data Source
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString!{
        let attributes = [NSFontAttributeName : UIFont.init(name: FONT_BOLD, size: 20)!]
        var message = "No chats"
        if (isInternetConnectivityAvailable(false)==false){
            message = "Unable to load chats"
        }
        if searchBar.text?.length > 0{
            message = "No Search Results"
        }
        return NSAttributedString(string: message, attributes: attributes)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString!{
        let attributes = [NSFontAttributeName : UIFont.init(name: FONT_REGULAR, size: 14)!]
        var message = "No chats available at the moment"
        if (isInternetConnectivityAvailable(false)==false){
            message = MESSAGE_TEXT___FOR_NETWORK_NOT_REACHABILITY
        }
        if searchBar.text?.length > 0{
            message = "Try searching with different name"
        }
        return NSAttributedString(string: message, attributes: attributes)
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage!{
        if !isUserLoggedIn() {
            return UIImage(named:"searchLightGray")
        }
        if (isInternetConnectivityAvailable(false)==false){
            return UIImage(named:"wifi")
        }
        if searchBar.text?.length > 0{
            return UIImage(named:"searchLightGray")
        }
        return UIImage(named:"searchLightGray")
    }
    
    // MARK: - UITableViewDataSource & UITableViewDelegate
    
    @objc func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return section1ContentsArray.count
        }else if section == 1 {
            return section2ContentsArray.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath as NSIndexPath).section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RecentChatsTableViewCell", for: indexPath) as! RecentChatsTableViewCell
            cell.chatDialog = self.section1ContentsArray[(indexPath as NSIndexPath).row] as? QBChatDialog
            cell.tag = (indexPath as NSIndexPath).row
            cell.shouldOptimise = (tableView.isDragging || tableView.isDecelerating)
            return cell
        }else if (indexPath as NSIndexPath).section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SuggestedUsersTableViewCell", for: indexPath) as! SuggestedUsersTableViewCell
            cell.userInfo = section2ContentsArray.object(at: (indexPath as NSIndexPath).row) as? NSDictionary
            cell.tag = (indexPath as NSIndexPath).row
            cell.shouldOptimise = (tableView.isDragging || tableView.isDecelerating)
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if Acf().isQBReadyForItsUserDependentServices(){
            tableView.deselectRow(at: indexPath, animated: true)
            if (indexPath as NSIndexPath).section == 0 {
                Acf().pushVC("ChatViewController", navigationController: self.navigationController, isRootViewController: false, animated: true) { (viewControllerObject) -> () in
                    (viewControllerObject as! ChatViewController).dialog = self.section1ContentsArray[(indexPath as NSIndexPath).row] as? QBChatDialog
                }
            }else if (indexPath as NSIndexPath).section == 1 {
                if isInternetConnectivityAvailable(true) == false {return}
                let userInfo = section2ContentsArray.object(at: (indexPath as NSIndexPath).row) as! NSDictionary
                Acf().showChatScreen(getUserId(userInfo),from: self)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if (indexPath as NSIndexPath).section == 0 {
            let dialog = section1ContentsArray[(indexPath as NSIndexPath).row]
            if (dialog as AnyObject).type == .publicGroup{
                return false//user can not leave from public groups
            }
            return true
        }else if (indexPath as NSIndexPath).section == 1 {
            return false
        }
        return false
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            execMain({[weak self] in guard let `self` = self else { return }

                tableView.isEditing = false
                if Acf().isQBReadyForItsUserDependentServices() {
                    if self.section1ContentsArray.count > (indexPath as NSIndexPath).row {
                        let dialog = self.section1ContentsArray[(indexPath as NSIndexPath).row] as! QBChatDialog
                        let deleteDialogBlock = { (dialog: QBChatDialog!) -> Void in
                            ServicesManager.instance().chatService.deleteDialog(withID: dialog.id!, completion: { (response: QBResponse!) -> Void in
                                if response.isSuccess {
                                } else {
                                    logMessage(response.error?.error)
                                }
                            })
                        }
                        if dialog.type == .private {
                            deleteDialogBlock(dialog)
                        } else if dialog.type == .group{
                            let occupantIDs =  dialog.occupantIDs!.filter( {$0.intValue != ServicesManager.instance().currentUser()!.id.toInt})
                            dialog.occupantIDs = occupantIDs
                            deleteDialogBlock(dialog)
                        }
                    }
                }
            })
        }
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "Leave"
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if searchBar.isFirstResponder{
            resignKeyboard()
        }
    }
    
    //MARK: - QMChatServiceDelegate
    
    func chatService(_ chatService: QMChatService, didUpdateChatDialogInMemoryStorage chatDialog: QBChatDialog) {
        updateUIForUpdates()
    }
    
    func chatService(_ chatService: QMChatService, didAddChatDialogsToMemoryStorage chatDialogs: [QBChatDialog]) {
        for dialog : QBChatDialog in chatDialogs {
            if dialog.type != QBChatDialogType.private {
                ServicesManager.instance().chatService.join(toGroupDialog: dialog, completion: { (error) in
                    if (error != nil) {
                        logMessage("Failed to join dialog with error: \(error)")
                    }
                })
            }
        }
        updateUIForUpdates()
    }
    
    func chatService(_ chatService: QMChatService, didAddChatDialogToMemoryStorage chatDialog: QBChatDialog) {
        if chatDialog.type != QBChatDialogType.private {
            ServicesManager.instance().chatService.join(toGroupDialog: chatDialog, completion: { (error) in
                if (error != nil) {
                    logMessage("Failed to join dialog with error: \(error)")
                }
            })
        }
        updateUIForUpdates()
    }
    
    func chatService(_ chatService: QMChatService, didDeleteChatDialogWithIDFromMemoryStorage chatDialogID: String) {
        updateUIForUpdates()
    }
    
    func chatService(_ chatService: QMChatService, didAddMessagesToMemoryStorage messages: [QBChatMessage], forDialogID dialogID: String) {
        updateUIForUpdates()
    }
    
    func chatService(_ chatService: QMChatService, didAddMessageToMemoryStorage message: QBChatMessage, forDialogID dialogID: String) {
        updateUIForUpdates()
    }
    
    //MARK: - QMChatConnectionDelegate
    
    func chatServiceChatDidAccidentallyDisconnect(_ chatService: QMChatService) {
    }
    
    func chatServiceChatDidConnect(_ chatService: QMChatService) {
        self.getDialogs()
    }
    
    func chatService(_ chatService: QMChatService, chatDidNotConnectWithError error: Error) {
    }
    
    func chatServiceChatDidReconnect(_ chatService: QMChatService) {
        self.getDialogs()
    }
    
    func updateUIForUpdates() {
        updateSection1ContentsArray()
        tableView.reloadData()
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(RecentChatsViewController.validateAndShowContacts), object: nil)
        self.perform(#selector(RecentChatsViewController.validateAndShowContacts), with: nil, afterDelay:2.0)
    }
    
    func validateAndShowContacts(){
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(RecentChatsViewController.validateAndShowContactsPrivate), object: nil)
        self.perform(#selector(RecentChatsViewController.validateAndShowContactsPrivate), with: nil, afterDelay:2.0)
    }
    
    func validateAndShowContactsPrivate(){
        let usersToAddOnContactList = NSMutableArray()
        let qbUsersToDownload = NSMutableArray()
        
        //step 1 : removing users without qbid
        let dialogsArray = section1ContentsArray as! [QBChatDialog]
        let dialogueIds = NSMutableDictionary()
        for dialog in dialogsArray{
            if dialog.type == .private{
                dialogueIds.setObject("1", forKey: "\(dialog.recipientID)" as NSCopying)
                /**
                 Why?
                 normally we add users into contacts coming on contact list from server , but we need to add others user those are chatting with us and they are not coming into our contacts, so here i m scanning all dialogs and adding them to my contact list , its important because we are interested in showing their online/offiline availibility.
                 */
                if isNotNull(dialog.recipientID) && dialog.recipientID > 0 {
                    let user = ServicesManager.instance().usersService.getUserWithID(dialog.recipientID.toUInt).result
                    if user != nil {
                        usersToAddOnContactList.add(user!)
                    }
                }
            }else{
                /**
                 Why?
                 normally we download and cache users coming on contact list , but we need to download user those are connected to us via group chat, so here i m scalling group occupants and if they are not cached , i am downloading them from QB
                 */
                if isNotNull(dialog.occupantIDs) {
                    for occupantId in dialog.occupantIDs! {
                        if isNull(ServicesManager.instance().usersService.usersMemoryStorage.user(withID: occupantId.uintValue)) {
                            qbUsersToDownload.add(occupantId)
                        }
                    }
                }
            }
        }
        dialogueIds.setObject("1", forKey: "0" as NSCopying)
        
        if qbUsersToDownload.count > 0{
            ServicesManager.instance().usersService.getUsersWithIDs(qbUsersToDownload as! [NSNumber])
        }
        
        ServicesManager.instance().contactListService.addUsersOnContactsList(usersToAddOnContactList as! [QBUUser])
        
        var usersToRemove = [Any]()
        if isNotNull(suggestedUsersList) && suggestedUsersList.count > 0 {
            for i in 0..<suggestedUsersList.count {
                let contact = suggestedUsersList.object(at: i)
                if isNotNull((contact as! NSDictionary).object(forKey: "qbid")){
                    if dialogueIds.object(forKey: ((contact as AnyObject).object(forKey: "qbid") as! NSString)) != nil {
                        usersToRemove.append(contact)
                    }
                }else{
                    usersToRemove.append(contact)
                }
            }
        }
        suggestedUsersList.removeObjects(in: usersToRemove)
        
        //step 2 : sort them on the basis of presence
        let enableSorting = true
        if enableSorting{
            let onlineUsers = NSMutableArray()
            let offlineUsers = NSMutableArray()
            for user in suggestedUsersList {
                let isOnline = Acf().getPresenceUser(user as! NSDictionary)
                if isOnline{
                    onlineUsers.add(user)
                }else{
                    offlineUsers.add(user)
                }
            }
            suggestedUsersList.removeAllObjects()
            suggestedUsersList.addObjects(from: onlineUsers as [AnyObject])
            suggestedUsersList.addObjects(from: offlineUsers as [AnyObject])
        }
        
        CacheManager.sharedInstance.saveObject(self.suggestedUsersList, identifier: "getContactList")
        updateSection2ContentsArray()
        tableView.reloadData()
        
        execMain ({ (completed) in
            if self.section1ContentsArray.count > 0 || self.section2ContentsArray.count > 0 {
                trackFunctionalEvent(FE_SCREEN_DATA_LOADED, information: ["screen":"chat_screen"])
            }else{
                trackFunctionalEvent(FE_SCREEN_DATA_NOT_LOADED, information: ["screen":"chat_screen"])
            }
        })
    }
    
    func fetchAndShowSuggestedUsersList(){
        ServicesManager.instance().contactListService.refreshPresence()
        let suggestedUsersListFromCache = CacheManager.sharedInstance.loadObject("getContactList") as? NSMutableArray
        if isNotNull(suggestedUsersListFromCache){
            self.suggestedUsersList = suggestedUsersListFromCache!
            validateAndShowContacts()
        }
        var forceUpdate = false
        if let lastSyncDate = CacheManager.sharedInstance.loadObject("lastSyncDateContactsList") as? Date{
            forceUpdate = lastSyncDate.isEarlierThanDate(Date().dateBySubtractingDays(2))
        }
        if forceUpdate || self.suggestedUsersList.count == 0 {
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(RecentChatsViewController.fetchAndShowSuggestedUsersListPrivate), object: nil)
            self.perform(#selector(RecentChatsViewController.fetchAndShowSuggestedUsersListPrivate), with: nil, afterDelay: 8)
        }
    }
    
    var isUpdatingContactList = false
    func fetchAndShowSuggestedUsersListPrivate(){
        if isUpdatingContactList {return;}
        if Acf().isQBReadyForItsUserDependentServices() {
            let suggestedUsersListFromCache = CacheManager.sharedInstance.loadObject("getContactList") as? NSMutableArray
            if isNotNull(suggestedUsersListFromCache){
                self.suggestedUsersList = suggestedUsersListFromCache!
                validateAndShowContacts()
            }
            isUpdatingContactList = true
            let qbId = ServicesManager.instance().currentUser()!.id
            let information : NSDictionary = ["qbId":qbId]
            let scm = ServerCommunicationManager()
            scm.responseErrorOption = ResponseErrorOption.dontShowErrorResponseMessage
            scm.returnFailureResponseAlso = true
            scm.getContactList(information) { (responseData) -> () in
                self.isUpdatingContactList = false
                NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(RecentChatsViewController.fetchAndShowSuggestedUsersListPrivate), object: nil)
                if isNotNull((responseData?.object(forKey: "data") as! NSDictionary?)?.object(forKey: "users")){
                    var contactListUsers = NSMutableArray()
                    if let users = (responseData?.object(forKey: "data") as AnyObject).object(forKey: "users") as? NSMutableArray{
                        if users.count > MAXIMUM_CONTACT_LIST_COUNT {
                            contactListUsers = NSMutableArray(array: users.subarray(with: NSMakeRange(0, MAXIMUM_CONTACT_LIST_COUNT)))
                        }else{
                            contactListUsers = NSMutableArray(array: users)
                        }
                    }
                    CacheManager.sharedInstance.saveObject(Date(), identifier:"lastSyncDateContactsList")
                    let suggestedUsersListTempArray = contactListUsers
                    let suggestedUsersListTempDictionary = NSMutableDictionary()
                    for u in suggestedUsersListTempArray{
                        if isNotNull((u as! NSDictionary).object(forKey: "qbid")){
                            suggestedUsersListTempDictionary.setObject(u, forKey: Acf().getQuickBloxUserName((u as! NSDictionary).object(forKey: "userid")! as! String) as NSCopying)
                        }
                    }
                    ServicesManager.instance().downloadUsers(suggestedUsersListTempDictionary.allKeys as NSArray, success: { (users) -> Void in
                        DispatchQueue.main.async(execute: {
                            self.suggestedUsersList.removeAllObjects()
                            if isNotNull(users){
                                for qbUser in users! {
                                    let user = suggestedUsersListTempDictionary.object(forKey: qbUser.login!)
                                    if isNotNull(user){
                                        self.suggestedUsersList.add(user!)
                                    }
                                }
                                
                                execMain({[weak self]  in guard let `self` = self else { return }
                                    ServicesManager.instance().contactListService.addUsersOnContactsList(users!)
                                    },delay: 4.0)
                                
                                CacheManager.sharedInstance.saveObject(self.suggestedUsersList, identifier: "getContactList")
                                let lastObject = self.suggestedUsersList.lastObject
                                for user in self.suggestedUsersList{
                                    execMain({[weak self]  in guard let `self` = self else { return }
                                        Dbm().addUserId(getUserId(user as? NSDictionary))
                                        Dbm().addUserStatus(["userId":(user as AnyObject).object(forKey: "userid")!,"status":(user as AnyObject).object(forKey: "status_text")!])
                                        Dbm().addUserName(["userId":(user as AnyObject).object(forKey: "userid")!,"name":(user as AnyObject).object(forKey: "user_name")!])
                                        
                                        Dbm().addUserPicture(["userId":(user as AnyObject).object(forKey: "userid")!,"url":(user as AnyObject).object(forKey: "Photo")!])
                                        
                                        if let _ = lastObject{
                                            if (lastObject! as AnyObject).isEqual(user){
                                                self.validateAndShowContacts()
                                            }
                                        }
                                    })
                                }
                                self.validateAndShowContacts()
                            }
                        })
                    }, error: { (error) -> Void in
                    })
                }
            }
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        if searchBar.text?.length > 0 {
            performSearch()
        }else{
            performSearchPrivate()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar){
        performSearchPrivate()
        resignKeyboard()
    }
    
    
    func performSearch() {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(SearchUsersViewController.performSearchPrivate), object: nil)
        self.perform(#selector(SearchUsersViewController.performSearchPrivate), with: nil, afterDelay: 1.0)
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(RecentChatsViewController.cancleSearchOperation), object: nil)
        self.perform(#selector(RecentChatsViewController.cancleSearchOperation), with: nil, afterDelay: 4.0)
    }
    
    func cancleSearchOperation(){
        resignKeyboard()
    }
    
    func performSearchPrivate() {
        updateSection1ContentsArray()
        updateSection2ContentsArray()
        tableView.reloadEmptyDataSet()
        tableView.reloadData()
    }
    
    //MARK: - 3D Touch Setup and Support
    
    func setupFor3DTouchPreviewIfAvailable() {
        execMain ({ (returnedData) in
            if self.traitCollection.forceTouchCapability == .available {
                self.registerForPreviewing(with: self, sourceView: self.tableView!)
            }
        },delay:2)
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        guard let indexPath = self.tableView!.indexPathForRow(at: location) else { return nil }
        let selectedCellFrame = tableView!.cellForRow(at: indexPath)!.frame
        var dataToPass : NSDictionary?
        if (indexPath as NSIndexPath).section == 0 {
            let userInfoAsQBChatDialog = section1ContentsArray.object(at: (indexPath as NSIndexPath).row) as! QBChatDialog
            if userInfoAsQBChatDialog.type == QBChatDialogType.private {
                if userInfoAsQBChatDialog.recipientID > 0 {
                    if let recipient = ServicesManager.instance().usersService.usersMemoryStorage.user(withID: UInt(userInfoAsQBChatDialog.recipientID)) {
                        let userId = Acf().getUserIdFrom(recipient.login)
                        dataToPass = ["userId":userId]
                    }
                }
            }
        }else if (indexPath as NSIndexPath).section == 1 {
            dataToPass = section2ContentsArray.object(at: (indexPath as NSIndexPath).row) as! NSDictionary
        }
        if isNull(dataToPass){
            return nil
        }
        let detailViewController = getViewController("ProfileOthersViewController") as! ProfileOthersViewController
        detailViewController.userInfo = dataToPass
        let gapX = 10.0 as CGFloat
        let gapY = 10.0 as CGFloat
        detailViewController.preferredContentSize = CGRect(x: gapX,y: gapY,width: DEVICE_WIDTH-2*gapX, height: DEVICE_HEIGHT-2*gapY).size
        previewingContext.sourceRect = selectedCellFrame
        return detailViewController
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        Acf().showViewControllerAsPopup(viewControllerToCommit)
    }
}
