//
//  DestinationPickerViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit

class DestinationPickerViewController : UIViewController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource {
    
    //MARK: - variables and constants
    @IBOutlet fileprivate weak var tableView : UITableView?
    fileprivate var destinationList = NSMutableArray()
    var completion : ACFCompletionBlock?
    @IBOutlet fileprivate weak var searchBar : UISearchBar!

    //MARK: - view controller life cycle methods
    
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
    }
    
    //MARK: - other methods
    private func setupForNavigationBar(){
        setAppearanceForNavigationBarType1(self.navigationController)
        if userType().isEqual( USER_TYPE_CORPORATE){
            setupNavigationBarTitleType1("Select Company",viewController: self)
        }else if userType().isEqual( USER_TYPE_STUDENT){
            setupNavigationBarTitleType1("Select College",viewController: self)
        }
        if self.isBackButtonRequired() {
            addNavigationBarButton(self, image: UIImage(named: "backarrowblack"), title: nil, isLeft: true)
        }
        addNavigationBarButton(self, image: nil, title: "Add", isLeft: false)
    }
    
    func onClickOfLeftBarButton(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    func onClickOfRightBarButton(_ sender:Any){
        if userType().isEqual( USER_TYPE_CORPORATE){
            let prompt = UIAlertController(title: "Company Name", message: MESSAGE_TEXT___ADD_INFO , preferredStyle: UIAlertControllerStyle.alert)
            prompt.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
            prompt.addAction(UIAlertAction(title: "Use", style: UIAlertActionStyle.cancel, handler: { (action) -> Void in
                let enteredText = (prompt.textFields![0] as UITextField).text
                Dbm().addCompany(["comp_name":enteredText!,"comp_id":"-1"])
                let company = Dbm().getCompany(["comp_name":enteredText!,"comp_id":"-1"])
                self.completion!(company)
                self.navigationController?.popViewController(animated: true)
            }))
            prompt.addTextField(configurationHandler: {(textField: UITextField!) in
                textField.placeholder = "Enter name"
                textField.keyboardType = UIKeyboardType.emailAddress
            })
            self.present(prompt, animated: true, completion: nil)
        }else if userType().isEqual( USER_TYPE_STUDENT){
            let prompt = UIAlertController(title: "College Name", message: MESSAGE_TEXT___ADD_INFO , preferredStyle: UIAlertControllerStyle.alert)
            prompt.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
            prompt.addAction(UIAlertAction(title: "Use", style: UIAlertActionStyle.cancel, handler: { (action) -> Void in
                let enteredText = (prompt.textFields![0] as UITextField).text
                Dbm().addCollege(["clg_name":enteredText!,"clg_id":"-1"])
                let company = Dbm().getCollege(["clg_name":enteredText!,"clg_id":"-1"])
                self.completion!(company)
                self.navigationController?.popViewController(animated: true)
            }))
            prompt.addTextField(configurationHandler: {(textField: UITextField!) in
                textField.placeholder = "Enter name"
                textField.keyboardType = UIKeyboardType.emailAddress
            })
            self.present(prompt, animated: true, completion: nil)
        }
    }
    
    private func registerForNotifications(){
        
    }
    
    private func startUpInitialisations(){
        registerNib("TitleDescriptionImageTableViewCell", tableView: tableView)
        setAppearanceForViewController(self)
        setAppearanceForTableView(tableView)
        if userType().isEqual( USER_TYPE_CORPORATE){
            destinationList = Dbm().getAllCompanies()
            searchBar.placeholder = "Search Your Company"
            trackScreenLaunchEvent(SLE_COMPANY_SELECTION_SCREEN)
        }else if userType().isEqual( USER_TYPE_STUDENT){
            destinationList = Dbm().getAllColleges()
            searchBar.placeholder = "Search Your College"
            trackScreenLaunchEvent(SLE_COLLEGE_SELECTION_SCREEN)
        }
        searchBar.returnKeyType = .done
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        if userType().isEqual( USER_TYPE_CORPORATE){

        }else if userType().isEqual( USER_TYPE_STUDENT){
            // Alok
            // as of now , college list from server is not implemented , so database is not having college entries
            // so i am hitting google api to search for college and saving it to db (simulating as if is was came from our server)
            // refreshing UI
            // so , if server is done implementing college list , similar to company list , just delete below code and its dependencies from project
            if Dbm().getAllColleges().count == 0{
                Acf().searchCollegeAndCacheThemToDatabase("College", completion: {[weak self] (responseData) -> () in guard let `self` = self else { return }
                    if self.searchBar.text?.length > 0 {
                        self.destinationList = Dbm().getColleges(self.searchBar.text!.lowercased())
                    }else{
                        self.destinationList = Dbm().getAllColleges()
                    }
                    self.tableView?.reloadData()
                })
            }
        }
    }
    
    //MARK: - UITableView Delegate & Data Source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return destinationList.count
    }
    
    @objc func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 76
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TitleDescriptionImageTableViewCell") as! TitleDescriptionImageTableViewCell
        var title = ""
        var description = ""
        if userType().isEqual(USER_TYPE_CORPORATE){
            let company = destinationList.object(at: (indexPath as NSIndexPath).row) as! Company
            title = safeString(company.name)
            description = safeString(company.address)
        }else if userType().isEqual(USER_TYPE_STUDENT){
            let college = destinationList.object(at: (indexPath as NSIndexPath).row) as! College
            title = safeString(college.name)
            description = safeString(college.address)
        }
        cell.titleLabel.text = title
        cell.descriptionLabel.text = description
        cell.iconImageView.image = UIImage(named: "locationIcon2")
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if completion != nil{
            if userType().isEqual( USER_TYPE_CORPORATE){
                let company = destinationList.object(at: (indexPath as NSIndexPath).row) as! Company
                completion!(company)
                self.navigationController?.popViewController(animated: true)
            }else if userType().isEqual( USER_TYPE_STUDENT){
                let college = destinationList.object(at: (indexPath as NSIndexPath).row) as! College
                completion!(college)
                self.navigationController?.popViewController(animated: true)
            }
        }
    }

    //MARK: - other functions
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        performSearch()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
            searchBar.resignFirstResponder()
    }

    func performSearch() {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(DestinationPickerViewController.performSearchPrivate), object: nil)
        self.perform(#selector(DestinationPickerViewController.performSearchPrivate), with: nil, afterDelay:0.4)
    }
    
    func performSearchPrivate() {
        if userType().isEqual( USER_TYPE_CORPORATE){
            if searchBar.text?.length > 0 {
                destinationList = Dbm().getCompanies(searchBar.text!.lowercased())
            }else{
                destinationList = Dbm().getAllCompanies()
            }
        }else if userType().isEqual( USER_TYPE_STUDENT){
            if searchBar.text?.length > 0 {
                destinationList = Dbm().getColleges(searchBar.text!.lowercased())
                // Alok
                // as of now , college list from server is not implemented , so database is not having college entries
                // so i am hitting google api to search for college and saving it to db (simulating as if is was came from our server)
                // refreshing UI
                // so , if server is done implementing college list , similar to company list , just delete below code and its dependencies from project
                Acf().searchCollegeAndCacheThemToDatabase(searchBar.text!.lowercased(), completion: {[weak self] (responseData) -> () in guard let `self` = self else { return }
                    if self.searchBar.text?.length > 0 {
                        self.destinationList = Dbm().getColleges(self.searchBar.text!.lowercased())
                        self.tableView?.reloadData()
                    }
                })
            }else{
                destinationList = Dbm().getAllColleges()
            }
        }
        tableView?.reloadData()
    }
    
}
