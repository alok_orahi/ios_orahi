//
//  RankingViewController.swift
//  Orahi
//
//  Created by Alok Singh on 10/02/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//


import UIKit
import DZNEmptyDataSet

class RankingViewController: UIViewController,DZNEmptyDataSetDelegate,DZNEmptyDataSetSource {
    
    //MARK: - variables and constants
    fileprivate var rankingDetailsMonthly : NSMutableDictionary?
    fileprivate var rankingDetailsOverAll : NSMutableDictionary?
    fileprivate var selfRanking : NSDictionary?
    fileprivate var othersRankings : NSArray?
    fileprivate var updateForceFully = false
    
    @IBOutlet fileprivate weak var segmentControl: UISegmentedControl!
    
    @IBOutlet weak var currentUserInfoView: UIView!
    @IBOutlet weak var userProfileImageView: UIImageView!
    @IBOutlet weak var informationLabel1: UILabel!
    @IBOutlet weak var informationLabel2: UILabel!
    @IBOutlet weak var informationLabel3: UILabel!
    
    @IBOutlet fileprivate weak var tableView : UITableView!
    
    //MARK: - view controller life cycle methods
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startupInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
        fetchAndUpdateRankingDetails()
        updateUserInterfaceOnScreen()
    }
    
    //MARK: - other methods
    func setupForNavigationBar(){
        setAppearanceForNavigationBarType1(self.navigationController)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func registerForNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(RankingViewController.updateForceFullyNow), name: NSNotification.Name(rawValue: NOTIFICATION_PROFILE_UPDATED), object: nil)
    }
    
    func startupInitialisations(){
        setAppearanceForViewController(self)
        self.userProfileImageView.makeRound()
        setAppearanceForTableView(tableView)
    }
    
    func updateForceFullyNow(){
        self.updateForceFully = true
        fetchAndUpdateRankingDetails()
    }
    
    func updateUserInterfaceOnScreen(){
        prepareDataToShow()
        tableView?.reloadEmptyDataSet()
    }
    
    //MARK: - other functions
    
    func updateCurrentUserDetails(){
        let userInfo = Dbm().getUserInfo()
        self.userProfileImageView.sd_setImage(with: URL(string: getUserProfilePictureUrlFromFileName(userInfo!.picture)), placeholderImage: USER_PLACEHOLDER_IMAGE)
        self.informationLabel1.text = safeString(self.selfRanking?.object(forKey: "rank"),alternate: "--")
        self.informationLabel2.text = safeString(self.selfRanking?.object(forKey: "level"),alternate: "--")
        self.informationLabel3.text = safeString(self.selfRanking?.object(forKey: "rides_shared"),alternate: "--")
    }
    
    @IBAction func onClickOfSideMenuButton(_ sender:Any){
        Acf().sideMenuController?.presentLeftMenuViewController()
    }
    
    //MARK: - DZNEmptyDataSet Delegate & Data Source
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString!{
        let attributes = [NSFontAttributeName : UIFont.init(name: FONT_BOLD, size: 20)!]
        var message = "No rankings"
        if (isInternetConnectivityAvailable(false)==false){
            message = "Unable to load rankings"
        }
        return NSAttributedString(string: message, attributes: attributes)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString!{
        let attributes = [NSFontAttributeName : UIFont.init(name: FONT_REGULAR, size: 14)!]
        var message = "No rankings available at the moment"
        if (isInternetConnectivityAvailable(false)==false){
            message = MESSAGE_TEXT___FOR_NETWORK_NOT_REACHABILITY
        }
        return NSAttributedString(string: message, attributes: attributes)
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage!{
        if !isUserLoggedIn() {
            return UIImage(named:"searchLightGray")
        }
        if (isInternetConnectivityAvailable(false)==false){
            return UIImage(named:"wifi")
        }
        return UIImage(named:"searchLightGray")
    }
    
    //MARK: - UITableView Delegate & Data Source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return safeInt(othersRankings?.count)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat{
        return RankingOtherUsersTableViewCell.getRequiredHeight()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView .dequeueReusableCell(withIdentifier: "RankingOtherUsersTableViewCell") as? RankingOtherUsersTableViewCell
        cell?.userDetailsToShow = othersRankings?[indexPath.row] as! NSDictionary
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        let userInfo = othersRankings?[indexPath.row] as! NSDictionary
        showUserProfile(userInfo, navigationController: self.navigationController!)
    }
    
    
    @IBAction func segmentControlValueChanged(_ sender:UISegmentedControl){
        fetchAndUpdateRankingDetails()
    }
    
    func prepareDataToShow(){
        rankingDetailsMonthly = CacheManager.sharedInstance.loadObject("rankingDetailsMonthly") as? NSMutableDictionary
        rankingDetailsOverAll = CacheManager.sharedInstance.loadObject("rankingDetailsOverAll") as? NSMutableDictionary
        extractFurtherDetailsAndShowAsRequired()
        updateCurrentUserDetails()
        tableView!.reloadData()
    }
    
    func fetchAndUpdateRankingDetails(){
        tableView?.reloadEmptyDataSet()
        if isSystemReadyToProcessThis() {
            self.prepareDataToShow()
            let scm = ServerCommunicationManager()
            scm.responseErrorOption = ResponseErrorOption.dontShowErrorResponseMessage
            scm.returnFailureResponseAlso = true
            var shouldPerformNewRequest = true
            if self.segmentControl.selectedSegmentIndex == 0 {
                if let lastCachedDateRanking = CacheManager.sharedInstance.loadObject("lastCachedDateRankingMonthly") as? Date {
                    if lastCachedDateRanking.isLaterThanDate(Date().dateBySubtractingHours(6)){
                        //cache is almost latest
                        shouldPerformNewRequest = false
                    }
                }
                if shouldPerformNewRequest || updateForceFully {
                    scm.getRankingDetails(["type":"monthly"]) {[weak self] (responseData) -> () in guard let `self` = self else { return }
                        if isNotNull(responseData) && isNotNull(responseData?.object(forKey: "data")) {
                            self.updateForceFully = false
                            CacheManager.sharedInstance.saveObject(responseData!.object(forKey: "data") , identifier: "rankingDetailsMonthly")
                            CacheManager.sharedInstance.saveObject(Date() , identifier: "lastCachedDateRankingMonthly")
                            self.prepareDataToShow()
                        }
                    }
                }
            }else{
                if let lastCachedDateRanking = CacheManager.sharedInstance.loadObject("lastCachedDateRankingOverAll") as? Date {
                    if lastCachedDateRanking.isLaterThanDate(Date().dateBySubtractingHours(6)){
                        //cache is almost latest
                        shouldPerformNewRequest = false
                    }
                }
                if shouldPerformNewRequest || updateForceFully {
                    scm.getRankingDetails(["type":"overall"]) {[weak self] (responseData) -> () in guard let `self` = self else { return }
                        if isNotNull(responseData) && isNotNull(responseData?.object(forKey: "data")) {
                            self.updateForceFully = false
                            CacheManager.sharedInstance.saveObject(responseData!.object(forKey: "data") , identifier: "rankingDetailsOverAll")
                            CacheManager.sharedInstance.saveObject(Date() , identifier: "lastCachedDateRankingOverAll")
                            self.prepareDataToShow()
                        }
                    }
                }
            }
        }
    }
    
    func extractFurtherDetailsAndShowAsRequired(){
        self.othersRankings = nil
        self.selfRanking = nil
        if self.segmentControl.selectedSegmentIndex == 0 {
            if let info = rankingDetailsMonthly?.value(forKeyPath: "self_info") as? NSDictionary {
                self.selfRanking = info
            }
            if let info = rankingDetailsMonthly?.value(forKeyPath: "others_info") as? NSArray {
                self.othersRankings = info
            }
        }else{
            if let info = rankingDetailsOverAll?.value(forKeyPath: "self_info") as? NSDictionary {
                self.selfRanking = info
            }
            if let info = rankingDetailsOverAll?.value(forKeyPath: "others_info") as? NSArray {
                self.othersRankings = info
            }
        }
        tableView?.reloadData()
    }
    
}
