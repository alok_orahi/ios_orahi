//
//  MemberDetailViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class MemberDetailViewController: UIViewController,MKMapViewDelegate,UIGestureRecognizerDelegate,UITableViewDataSource,UITableViewDelegate {
    
    //MARK: - variables and constants
    @IBOutlet fileprivate var navigationBarView : UIView!
    @IBOutlet fileprivate var tableView : UITableView!
    @IBOutlet fileprivate var titleLabel : UILabel!
    @IBOutlet fileprivate var notificationButton : UIButton!
    fileprivate var mapView : MKMapView!
    fileprivate var animateAnnotations = true
    fileprivate var mapViewGapFromBottom = 0.0 as CGFloat
    var group : Group?
    var groupId : String?
    var member : Member?
    var memberId : String?
    var canLoadTableView = false

    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
        updateUserInterfaceOnScreen()
        updateNotificationBadge(notificationButton: self.notificationButton,ax:-7,ay:2,groupId: safeString(self.groupId),memberId:safeString(self.memberId))
    }
    
    override internal func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupForNavigationBar()
        canLoadTableView = true
        execMain({
            self.tableView.reloadData()
            self.updateTableViewContentOffset()
        }, delay: 0.1)
    }
    
    //MARK: - other methods
    private func setupForNavigationBar(){
        self.navigationBarView.backgroundColor = self.group?.getGroupColor()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.titleLabel.text = safeString(member?.name).capitalized.firstName()
    }
    
    private func registerForNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(MemberDetailViewController.hideThingsToViewMapInBetterWay), name: NSNotification.Name(rawValue: NOTIFICATION_HIDE_FOR_MAP_VIEW), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(MemberDetailViewController.reLoadGroupObjectFromDb), name: NSNotification.Name(rawValue: NOTIFICATION_GROUPS_PLACES_UPDATED), object: nil)
    }
    
    func reLoadGroupObjectFromDb(){
        let updatedGroupObject = Dbm().getGroup(["groupId":safeString(groupId)])
        if isNotNull(updatedGroupObject) {
            self.group = updatedGroupObject
            if isNotNull(self.memberId) {
                if self.group!.members != nil{
                    for m in self.group!.members! {
                        if safeString((m as! Member).userId) == safeString(self.memberId){
                            self.member = m as! Member
                            break
                        }
                    }
                }
            }
            self.tableView.reloadData()
            updateUserInterfaceOnScreen()
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func didDragMap(_ sender: UIGestureRecognizer) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NOTIFICATION_HIDE_FOR_MAP_VIEW), object: nil)
    }
    
    private func startUpInitialisations(){
        setupTableView()
        setAppearanceForViewController(self)
        setupForMapView()
        groupId = safeString(group?.groupId)
        memberId = safeString(member?.userId)
    }
    
    private func setupTableView(){
        registerNib("GroupMemberType2TableViewCell", tableView: tableView)
        tableView!.separatorInset = UIEdgeInsets.zero
        tableView!.separatorStyle = UITableViewCellSeparatorStyle.none
        tableView!.layoutMargins = UIEdgeInsets.zero
        setFooterView(tableView: self.tableView,44)
        execMain({
            self.tableView.addParallax(with: self.mapView, andHeight: DEVICE_HEIGHT - self.mapViewGapFromBottom)
            execMain({
                if self.tableView.numberOfRows(inSection: 0) > 0 {
                    self.tableView.setContentOffset(CGPoint(x: 0, y: -(DEVICE_HEIGHT-140)), animated: true)
                }
            }, delay: 0.6)
        }, delay: 0.0)
    }
    
    private func setupForMapView() {
        mapView = MKMapView(x: 0, y: 0, w: DEVICE_WIDTH, h: DEVICE_HEIGHT - mapViewGapFromBottom)
        mapView.showsUserLocation = true
        mapView.delegate = self
        let memberId = safeString(self.member?.userId)
        let x = safeDouble(CacheManager.sharedInstance.loadObject("\(memberId)mapX"))
        let y = safeDouble(CacheManager.sharedInstance.loadObject("\(memberId)mapY"))
        let w = safeDouble(CacheManager.sharedInstance.loadObject("\(memberId)mapW"))
        let h = safeDouble(CacheManager.sharedInstance.loadObject("\(memberId)mapH"))
        if x > 0.0 {
            self.mapView.setVisibleMapRect(MKMapRectMake(x, y, w, h), animated: false)
        }
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(self.didDragMap(_:)))
        panGesture.delegate = self
        mapView.addGestureRecognizer(panGesture)
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        updateMapContents()
    }
    
    func updateMapContents(){
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(MemberDetailViewController.updateMapContentsPrivate), object: nil)
        self.perform(#selector(MemberDetailViewController.updateMapContentsPrivate), with: nil, afterDelay: 0.5)
    }
    
    func updateMapContentsPrivate(){
        //step 1 : remove everything from the map
        let annotationsCountBefore = mapView.annotations.count
        mapView.removeAnnotations(mapView.annotations)
        
        //step 2 : add users annotations on map
        let annotationsToAdd = NSMutableArray()
        if safeBool(self.member?.locationDisplay){
            let annotation = UserType1PinAnnotation()
            var lastKnownLocationLatitude = self.member?.lastKnownLocationLatitude
            var lastKnownLocationLongitude = self.member?.lastKnownLocationLongitude
            if lastKnownLocationLongitude != nil && lastKnownLocationLatitude != nil {
                annotation.coordinate = CLLocationCoordinate2DMake(Double(lastKnownLocationLatitude!)!, Double(lastKnownLocationLongitude!)!)
            }else{
                return
            }
            
            annotation.userInfo = member
            annotation.title = safeString(member?.name).capitalized
            annotationsToAdd.add(annotation)
        }
        self.mapView.addAnnotations(annotationsToAdd as! [MKAnnotation])
        
        //step 3 : adjust map so that every content become visible on screen
        
        execMain({[weak self]  in guard let `self` = self else { return }
            let annotationsCountAfter = self.mapView.annotations.count
            if annotationsCountAfter != annotationsCountBefore {
                self.animateAnnotations = true
                self.mapView.updateRegion(forAnnotations: self.mapView.annotations, animated: true)
                execMain({[weak self] in guard let `self` = self else { return }
                    
                    self.animateAnnotations = false
                    execMain({[weak self] in guard let `self` = self else { return }
                        
                        let x = self.mapView.visibleMapRect.origin.x
                        let y = self.mapView.visibleMapRect.origin.y
                        let w = self.mapView.visibleMapRect.size.width
                        let h = self.mapView.visibleMapRect.size.height
                        let memberId = safeString(self.member?.userId)
                        CacheManager.sharedInstance.saveObject(x, identifier: "\(memberId)mapX")
                        CacheManager.sharedInstance.saveObject(y, identifier: "\(memberId)mapY")
                        CacheManager.sharedInstance.saveObject(w, identifier: "\(memberId)mapW")
                        CacheManager.sharedInstance.saveObject(h, identifier: "\(memberId)mapH")
                        },delay: 2)
                })
            }
            },delay: 2)
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is UserType1PinAnnotation {
            var annotationView = self.mapView.dequeueReusableAnnotationView(withIdentifier: "location") as! UserType2MKAnnotationView?
            if isNull(annotationView) {
                annotationView = UserType2MKAnnotationView(annotation: annotation, reuseIdentifier: "location")
            }
            annotationView!.userInfo = (annotation as! UserType1PinAnnotation).userInfo as? Member
            annotationView!.group = self.group
            annotationView!.setup()
            annotationView?.canShowCallout = true
            if animateAnnotations {
                annotationView?.performAppearAnimationType5(Double(Float.random(3.0, 8.0)))
            }
            if (annotation as! UserType1PinAnnotation).isRecentlyMoved {
                annotationView?.transform = CGAffineTransform.identity
                annotationView?.pulse(toSize: 0.90, duration: 0.35, repeat: true)
            }else{
                annotationView?.stopAnimation()
                annotationView?.transform = CGAffineTransform(scaleX: ANNOTATION_TRANSFORM_ADJUST, y: ANNOTATION_TRANSFORM_ADJUST)
            }
            return annotationView
        }
        return nil
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
    }
    
    @IBAction func onClickOfBackButton(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickOfChatButton(_ sender:Any){
        Acf().showChatScreen(safeString(member?.userId),from:self,themeColor: self.group!.getGroupColor(),group:self.group)
    }
    
    @IBAction func onClickOfNotificationsButton(_ sender:Any){
        Acf().showNotificationScreen(nc: self.navigationController,themeColor: self.group!.getGroupColor(),group:self.group,member:self.member)
    }
    
    @IBAction func onClickOfOptionsButton(_ sender:Any){
        let actionSheet = Hokusai()
        if member!.locationDisplay {
            actionSheet.addButton("Start Navigation") {
                self.navigateToUserLocation()
            }
        }else{
            actionSheet.addButton("Request Location") {
                showRequestLocationAccessPopupAndDoRequiredProcessing(group: self.group!, member: self.member!)
            }
        }
        actionSheet.cancelButtonTitle = "Cancel"
        actionSheet.colors = self.group?.hokColor()
        actionSheet.show()
    }
    
    func navigateToUserLocation(){
        let dLat = safeString(member?.lastKnownLocationLatitude)
        let dLong = safeString(member?.lastKnownLocationLongitude)
        let urlToOpenGoogleMapApplication = "comgooglemaps://?saddr=Current%20Location&daddr=\(dLat),\(dLong)&directionsmode=driving"
        let urlToOpenGoogleMapWebPage = "http://maps.google.com?saddr=Current%20Location&daddr=\(dLat),\(dLong)&directionsmode=driving"
        if UIApplication.shared.canOpenURL(urlToOpenGoogleMapApplication.asNSURL()){
            openUrl(urlToOpenGoogleMapApplication)
        }else{
            openUrl(urlToOpenGoogleMapWebPage)
        }
    }

    func hideThingsToViewMapInBetterWay(){
        UIView.animate(withDuration: HIDE_ANIMATION_DURATION, delay: 0, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.navigationBarView.layer.opacity = OPACITY_WHEN_HIDDEN_FOR_MAP_VIEW
        }) { (completed) in }
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(MemberDetailViewController.restoreHiddenComponents), object: nil)
        self.perform(#selector(MemberDetailViewController.restoreHiddenComponents), with: nil, afterDelay: RESTORE_HIDDEN_VIEW_TIME)
    }
    
    func restoreHiddenComponents(){
        UIView.animate(withDuration: SHOW_ANIMATION_DURATION, delay: 0, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.navigationBarView.layer.opacity = 1.0
        }) { (completed) in }
    }
    
    //MARK: - UITableView Delegate & Data Source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if canLoadTableView {
            return 1
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 96
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GroupMemberType2TableViewCell") as! GroupMemberType2TableViewCell
        cell.group = self.group
        cell.member = self.member
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        updateTableViewContentOffset()
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        updateTableViewContentOffset()
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool){
        updateTableViewContentOffset()
    }
    
    func updateTableViewContentOffset(){
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(MemberDetailViewController.updateTableViewContentOffsetPrivate), object: nil)
        self.perform(#selector(MemberDetailViewController.updateTableViewContentOffsetPrivate), with: nil, afterDelay: 0.3)
    }
    
    func updateTableViewContentOffsetPrivate(){
        if self.tableView.contentOffset.y < -(DEVICE_HEIGHT-88){
            if self.canLoadTableView && self.tableView(self.tableView, numberOfRowsInSection: 0) > 0{
                self.tableView.setContentOffset(CGPoint(x: 0, y: -(DEVICE_HEIGHT-140)), animated: true)
            }else{
                self.tableView.setContentOffset(CGPoint(x: 0, y: -DEVICE_HEIGHT), animated: true)
            }
        }
    }
}
