//
//  ChatUsersInfoTableViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

class ChatUsersInfoTableViewController: UsersListTableViewController, QMChatServiceDelegate, QMChatConnectionDelegate {
    
    //MARK: - variables and constants
    
    fileprivate var occupantsIDs: [UInt] = []
    var dialog: QBChatDialog?
    fileprivate var usersUpdated = false
    var titleToDisplay : String?
    var messageToDisplay : String?

    @IBOutlet fileprivate weak var infoLabel: UILabel!

    
    //MARK: - view controller life cycle methods
    
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateUserInterfaceOnScreen()
    }
    
    
    //MARK: - other methods
    private func setupForNavigationBar(){
        setAppearanceForNavigationBarType1(self.navigationController)
        setupNavigationBarTitleType1("Details",viewController: self)
        if (self.navigationController?.viewControllers.count)! > 1 {
            addNavigationBarButton(self, image: UIImage(named: "backarrowblack"), title: nil, isLeft: true)
        }
    }
    
    func onClickOfLeftBarButton(_ sender:Any){
        if usersUpdated {
            self.navigationController?.popToRootViewController(animated: true)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func onClickOfRightBarButton(_ sender:Any){
        Acf().pushVC("NewChatViewController", navigationController: self.navigationController, isRootViewController: false, animated: true) { (viewControllerObject) -> () in
            (viewControllerObject as! NewChatViewController).dialog = self.dialog
        }
    }
    
    private func registerForNotifications(){
    }
    
    private func startUpInitialisations(){
        registerNib("UserListTableViewCell", tableView: self.tableView)
        setAppearanceForViewController(self)
        execMain({[weak self]  in guard let `self` = self else { return }
            if isNotNull(self.messageToDisplay){
                showNotification((self.messageToDisplay! as NSString) as String, showOnNavigation: false, showAsError: false, duration: 3)
            }
        },delay:1)
        trackScreenLaunchEvent(SLE_CHAT_INFORMATION)
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        if Acf().isQBReadyForItsUserDependentServices(){
            if self.dialog != nil && self.dialog!.occupantIDs!.count >= kUsersLimit {
                self.navigationItem.rightBarButtonItem = nil
            }
            ServicesManager.instance().chatService.addDelegate(self)
            if (dialog!.userID == ServicesManager.instance().currentUser()!.id) && dialog!.type == .group {
                if self.dialog != nil && self.dialog!.occupantIDs!.count < kUsersLimit {
                    addNavigationBarButton(self, image: nil, title: "Add", isLeft: false)
                }
                let ownerName = "You"
                infoLabel.text = "Group Owner : \(ownerName)\nSwipe left to delete members from the group."
            }else if dialog!.type == .group {
                var ownerName = "User"
                if let user = ServicesManager.instance().usersService.getUserWithID(dialog!.userID).result as QBUUser? {
                    ownerName = nameToDisplay(user, chatDialogue: nil)
                }
                infoLabel.text = "Group Owner : \(ownerName)"
            }else if dialog!.type == .publicGroup {
                infoLabel.text = "This is a Public Group"
            }else if dialog!.type == .private{
                infoLabel.text = "This is your Private Chat"
            }
            self.tableView.reloadData()
        }
    }
    
    func updateUsers() {
        if let _ = self.dialog  {
            self.setupUsers(ServicesManager.instance().allCachedUsers())
        }
    }
    
    override func setupUsers(_ users: [QBUUser]) {
        if Acf().isQBReadyForItsUserDependentServices() {
            var filteredUsers = users.filter({($0 as QBUUser).id != ServicesManager.instance().currentUser()!.id})
            if let _ = self.dialog  {
                filteredUsers = filteredUsers.filter({(self.dialog!.occupantIDs as! [UInt]).contains(($0 as QBUUser).id)})
            }
            super.setupUsers(filteredUsers)
        }
    }
    
    // MARK: - UITableViewDelegate & UITableViewDataSource
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 64
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserListTableViewCell", for: indexPath) as! UserListTableViewCell
        let user = self.users![(indexPath as NSIndexPath).row]
        cell.userInfoAsQBUUser = user
        cell.tag = (indexPath as NSIndexPath).row
        return cell
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if (dialog!.userID == ServicesManager.instance().currentUser()!.id) && dialog!.type == .group && (isThisAPoMGroupName(name: safeString(dialog?.name)) == false){
            return true
        }
        return false
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            if dialog!.type == .group {
                let usersToRemove = NSMutableArray()
                let user = self.users![(indexPath as NSIndexPath).row]
                usersToRemove.add(user)
                NewChatViewController.updateDialogRemoveUsers(self.dialog, oldUsers: usersToRemove as! [QBUUser], completion: { (response, dialog) in
                    if (response?.error == nil) {
                        self.updateUsers()
                        self.usersUpdated = true
                    } else {
                        showNotification((response?.error?.error?.localizedDescription)!, showOnNavigation: false, showAsError: true)
                    }
                })
            }
        }
    }
    
    func chatService(_ chatService: QMChatService, didUpdateChatDialogInMemoryStorage chatDialog: QBChatDialog) {
        if (chatDialog.id == self.dialog!.id) {
            self.dialog = chatDialog
            self.updateUsers()
            self.tableView.reloadData()
        }
    }
    
}
