//
//  TravelWithSettingsViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit

class TravelWithSettingsViewController: UIViewController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource {
    
    //MARK: - variables and constants
    @IBOutlet fileprivate weak var tableView : UITableView?
    
    
    //MARK: - view controller life cycle methods
    
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
    }
    
    //MARK: - other methods
    private func setupForNavigationBar(){
        setAppearanceForNavigationBarType1(self.navigationController)
        setupNavigationBarTitleType1("Travel Preferences",viewController: self)
        if self.isBackButtonRequired() {
            addNavigationBarButton(self, image: UIImage(named: "backarrowblack"), title: nil, isLeft: true)
        }
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    func onClickOfLeftBarButton(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    private func registerForNotifications(){
        
    }
    
    private func startUpInitialisations(){
        registerNib("SingleLabelTableViewCell", tableView: tableView)
        setAppearanceForViewController(self)
        setAppearanceForTableView(tableView)
        let settings = Dbm().getSetting()
        if settings.travelOptionType1 == nil {
            settings.travelOptionType1 = NSNumber(value: false)
        }
        if settings.travelOptionType2 == nil {
            settings.travelOptionType2 = NSNumber(value: false)
        }
        Dbm().saveChanges()
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        
    }
    
    //MARK: - UITableView Delegate & Data Source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if userType() as String == USER_TYPE_CORPORATE {
            return 5
        }else if userType() as String == USER_TYPE_STUDENT {
            return 4
        }
        return 5
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SingleLabelTableViewCell") as? SingleLabelTableViewCell
        cell?.accessoryType = UITableViewCellAccessoryType.none
        
        let settings = Dbm().getSetting()
        
        func setCellForTravelOption1(){
            if userType() as String == USER_TYPE_CORPORATE {
                cell?.titleLabel?.text = "Corporates Only"
            }else if userType() as String == USER_TYPE_STUDENT{
                cell?.titleLabel?.text = "Students Only"
            }
            if settings.travelOptionType1 != nil {
                if settings.travelOptionType1!.boolValue {
                    cell?.accessoryType = UITableViewCellAccessoryType.checkmark
                }else{
                    cell?.accessoryType = UITableViewCellAccessoryType.none
                }
            }
        }
        
        func setCellForTravelOption2(){
            if userType() as String == USER_TYPE_CORPORATE {
                cell?.titleLabel?.text = "My Company only"
            }else if userType() as String == USER_TYPE_STUDENT{
                cell?.titleLabel?.text = ""
            }
            if settings.travelOptionType2 != nil {
                if settings.travelOptionType2!.boolValue {
                    cell?.accessoryType = UITableViewCellAccessoryType.checkmark
                }else{
                    cell?.accessoryType = UITableViewCellAccessoryType.none
                }
            }
        }
        
        func setCellForERASettingOption1(){
            cell?.titleLabel?.text = "ERA: Send notfications"
            if settings.eprSettingOption1 != nil {
                if settings.eprSettingOption1!.boolValue {
                    cell?.accessoryType = UITableViewCellAccessoryType.checkmark
                }else{
                    cell?.accessoryType = UITableViewCellAccessoryType.none
                }
            }
        }
        
        func setCellForERASettingOption2(){
            cell?.titleLabel?.text = "ERA: Receive notifications"
            if settings.eprSettingOption2 != nil {
                if settings.eprSettingOption2!.boolValue {
                    cell?.accessoryType = UITableViewCellAccessoryType.checkmark
                }else{
                    cell?.accessoryType = UITableViewCellAccessoryType.none
                }
            }
        }
        
        func setCellForShowMyNumberOption(){
            cell?.titleLabel?.text = "Activate Call Me"
            if settings.showMyNumber != nil {
                if settings.showMyNumber!.boolValue {
                    cell?.accessoryType = UITableViewCellAccessoryType.checkmark
                }else{
                    cell?.accessoryType = UITableViewCellAccessoryType.none
                }
            }
        }
        
        if userType() as String == USER_TYPE_CORPORATE {
            if (indexPath as NSIndexPath).row == 0{
                setCellForTravelOption1()
            }else if (indexPath as NSIndexPath).row == 1{
                setCellForTravelOption2()
            }else if (indexPath as NSIndexPath).row == 2{
                setCellForERASettingOption1()
            }else if (indexPath as NSIndexPath).row == 3{
                setCellForERASettingOption2()
            }else if (indexPath as NSIndexPath).row == 4{
                setCellForShowMyNumberOption()
            }
        }else if userType() as String == USER_TYPE_STUDENT {
            if (indexPath as NSIndexPath).row == 0{
                setCellForTravelOption1()
            }else if (indexPath as NSIndexPath).row == 1{
                setCellForERASettingOption1()
            }else if (indexPath as NSIndexPath).row == 2{
                setCellForERASettingOption2()
            }else if (indexPath as NSIndexPath).row == 3{
                setCellForShowMyNumberOption()
            }
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (isInternetConnectivityAvailable(true)==false){return}
        let cell = tableView.cellForRow(at: indexPath) as? SingleLabelTableViewCell
        performAnimatedClickEffectType1(cell!.titleLabel!)
        if isSystemReadyToProcessThis() {
            let settings = Dbm().getSetting()
            func setSettingForTravelOption1(){
                if settings.travelOptionType1 != nil {
                    if settings.travelOptionType1!.boolValue {
                        settings.travelOptionType1 = NSNumber(value: false)
                    }else{
                        settings.travelOptionType1 = NSNumber(value: true)
                    }
                }else{
                    settings.travelOptionType1 = NSNumber(value: true)
                }
            }
            
            func setSettingForTravelOption2(){
                if settings.travelOptionType2 != nil {
                    if settings.travelOptionType2!.boolValue {
                        settings.travelOptionType2 = NSNumber(value: false)
                    }else{
                        settings.travelOptionType2 = NSNumber(value: true)
                    }
                }else{
                    settings.travelOptionType2 = NSNumber(value: true)
                }
            }
            
            func setSettingForERASettingOption1(){
                if settings.eprSettingOption1 != nil {
                    if settings.eprSettingOption1!.boolValue {
                        settings.eprSettingOption1 = NSNumber(value: false)
                    }else{
                        settings.eprSettingOption1 = NSNumber(value: true)
                    }
                }else{
                    settings.eprSettingOption1 = NSNumber(value: true)
                }
            }
            
            func setSettingForERASettingOption2(){
                if settings.eprSettingOption2 != nil {
                    if settings.eprSettingOption2!.boolValue {
                        settings.eprSettingOption2 = NSNumber(value: false)
                    }else{
                        settings.eprSettingOption2 = NSNumber(value: true)
                    }
                }else{
                    settings.eprSettingOption2 = NSNumber(value: true)
                }
            }
            
            func setSettingForShowMyNumberOption(){
                if settings.showMyNumber != nil {
                    if settings.showMyNumber!.boolValue {
                        settings.showMyNumber = NSNumber(value: false)
                    }else{
                        settings.showMyNumber = NSNumber(value: true)
                    }
                }else{
                    settings.showMyNumber = NSNumber(value: true)
                }
            }
            
            if userType() as String == USER_TYPE_CORPORATE {
                if (indexPath as NSIndexPath).row == 0 {
                    setSettingForTravelOption1()
                }else if (indexPath as NSIndexPath).row == 1 {
                    setSettingForTravelOption2()
                }else if (indexPath as NSIndexPath).row == 2 {
                    setSettingForERASettingOption1()
                }else if (indexPath as NSIndexPath).row == 3 {
                    setSettingForERASettingOption2()
                }else if (indexPath as NSIndexPath).row == 4 {
                    setSettingForShowMyNumberOption()
                }
            }else if userType() as String == USER_TYPE_STUDENT {
                if (indexPath as NSIndexPath).row == 0 {
                    setSettingForTravelOption1()
                }else if (indexPath as NSIndexPath).row == 1 {
                    setSettingForERASettingOption1()
                }else if (indexPath as NSIndexPath).row == 2 {
                    setSettingForERASettingOption2()
                }else if (indexPath as NSIndexPath).row == 3 {
                    setSettingForShowMyNumberOption()
                }
            }
            execMain({[weak self] in guard let `self` = self else { return }

                if userType() as String == USER_TYPE_CORPORATE {
                    if indexPath.row < 2 {
                        if !settings.travelOptionType1!.boolValue && !settings.travelOptionType2!.boolValue {//00
                            showNotification("You will travel with any one like coorporates , students etc", showOnNavigation: false, showAsError: false)
                        }else if !settings.travelOptionType1!.boolValue && settings.travelOptionType2!.boolValue{//01
                            showNotification("You will travel with persons from your company", showOnNavigation: false, showAsError: false)
                        }else if settings.travelOptionType1!.boolValue && !settings.travelOptionType2!.boolValue{//10
                            showNotification("You will travel with corporate persons only", showOnNavigation: false, showAsError: false)
                        }else if settings.travelOptionType1!.boolValue && settings.travelOptionType2!.boolValue{//11
                            showNotification("You will travel with colleagues only", showOnNavigation: false, showAsError: false)
                        }
                    }else if indexPath.row == 2 {
                        if settings.eprSettingOption1!.boolValue {
                            showNotification(MESSAGE_TEXT___ERA_SEND_TRUE, showOnNavigation: true, showAsError: false)
                        }else {
                            showNotification(MESSAGE_TEXT___ERA_FALSE, showOnNavigation: true, showAsError: false)
                        }
                    }else if indexPath.row == 3 {
                        if settings.eprSettingOption2!.boolValue {
                            showNotification(MESSAGE_TEXT___ERA_RECEIVE_TRUE, showOnNavigation: true, showAsError: false)
                        }else {
                            showNotification(MESSAGE_TEXT___ERA_FALSE, showOnNavigation: true, showAsError: false)
                        }
                    }else if indexPath.row == 4 {
                        if settings.showMyNumber!.boolValue {
                            showNotification(MESSAGE_TEXT___ACM_TRUE, showOnNavigation: true, showAsError: false)
                        }else {
                            showNotification(MESSAGE_TEXT___ACM_FALSE, showOnNavigation: true, showAsError: false)
                        }
                    }
                }else if userType() as String == USER_TYPE_STUDENT {
                    if indexPath.row < 1 {
                        if !settings.travelOptionType1!.boolValue && !settings.travelOptionType2!.boolValue {//00
                            showNotification("You will travel with any one like students, coorporates etc", showOnNavigation: false, showAsError: false)
                        }else if !settings.travelOptionType1!.boolValue && settings.travelOptionType2!.boolValue{//01
                            showNotification("You will travel with persons (students , teachers etc) from your college", showOnNavigation: false, showAsError: false)
                        }else if settings.travelOptionType1!.boolValue && !settings.travelOptionType2!.boolValue{//10
                            showNotification("You will travel with students only", showOnNavigation: false, showAsError: false)
                        }else if settings.travelOptionType1!.boolValue && settings.travelOptionType2!.boolValue{//11
                            showNotification("You will travel with students of your college", showOnNavigation: false, showAsError: false)
                        }
                    }else if indexPath.row == 1 {
                        if settings.eprSettingOption1!.boolValue {
                            showNotification(MESSAGE_TEXT___ERA_SEND_TRUE, showOnNavigation: true, showAsError: false)
                        }else {
                            showNotification(MESSAGE_TEXT___ERA_FALSE, showOnNavigation: true, showAsError: false)
                        }
                    }else if indexPath.row == 2 {
                        if settings.eprSettingOption2!.boolValue {
                            showNotification(MESSAGE_TEXT___ERA_RECEIVE_TRUE, showOnNavigation: true, showAsError: false)
                        }else {
                            showNotification(MESSAGE_TEXT___ERA_FALSE, showOnNavigation: true, showAsError: false)
                        }
                    }else if indexPath.row == 3 {
                        if settings.showMyNumber!.boolValue {
                            showNotification(MESSAGE_TEXT___ACM_TRUE, showOnNavigation: true, showAsError: false)
                        }else {
                            showNotification(MESSAGE_TEXT___ACM_FALSE, showOnNavigation: true, showAsError: false)
                        }
                    }
                }
            },delay: 0.2)

            Dbm().saveChanges()
            tableView.reloadData()
            Acf().updateTravelWithSettingsOnServer()
            Acf().updateExpertRideAlertsSettingOnServer()
            Acf().updateShowMyNumberSettingOnServer()
        }
    }
    
    //MARK: - other functions
    
}
