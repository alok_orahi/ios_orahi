//
//  TextInputViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit

typealias TICompletionBlock = (_ enteredText :String?) ->()

class TextInputViewController: UIViewController , YCInputBarDelegate{
    
    var completion : TICompletionBlock?
    var text : String?
    var limit = 1024
    var editTitle : String?
    var actionButtonTitle : String?
    fileprivate var inputBar : YCInputBar?
    
    //MARK: - view controller life cycle methods
    
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        Acf().setupIQKeyboardManagerEnabled()
    }
    
    //MARK: - other methods
    private func setupForNavigationBar(){
        self.navigationController?.isNavigationBarHidden = true
    }
    
    private func registerForNotifications(){
    }
    
    private func startUpInitialisations(){
        setAppearanceForViewController(self)
        self.view.backgroundColor = UIColor.clear
        Acf().setupIQKeyboardManagerDisabled()
        inputBar = YCInputBar(bar: self.view, sendButtonTitle: actionButtonTitle, maxTextLength: limit, isHideOnBottom: true, buttonColor: APP_THEME_VOILET_COLOR)
        inputBar!.placeholder = editTitle;
        inputBar!.delegate = self;
        inputBar!.txtInput.text = text
        inputBar!.txtInput.autocorrectionType = .no
        inputBar!.txtInput.autocapitalizationType = .none
        inputBar!.txtInput.becomeFirstResponder()
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        Acf().hidePopupViewController()
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        Acf().hidePopupViewController()
    }
    
    //MARK: - YCInputBarDelegate
    
    func sendButtonClick(_ textView: UITextView!) -> Bool{
        completion!(textView.text)
        Acf().hidePopupViewController()
        return true
    }
}
