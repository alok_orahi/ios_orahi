//
//  WebViewViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit

enum WorkingMode {
    case recharge
    case termsAndConditions
    case aboutUs
    case faq
    case contactUs
    case orahiOnFacebook
    case orahiOnTwitter
    case orahiOnLinkedIn
    case customUrl
}

class WebViewViewController: UIViewController,UITextFieldDelegate,UIWebViewDelegate {
    
    //MARK: - variables and constants
    var workingMode = WorkingMode.termsAndConditions
    fileprivate var isLoaded = false
    var customUrl: String?
    var customTitle: String?
    var amountToRecharge = "400"
    
    @IBOutlet fileprivate weak var webview : UIWebView?
    
    //MARK: - view controller life cycle methods
    
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        updateUserInterfaceOnScreen()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        windowObject()!.hideActivityIndicator()
    }
    
    //MARK: - other methods
    private func setupForNavigationBar(){
        setAppearanceForNavigationBarType1(self.navigationController)
        if self.isBackButtonRequired() {
            addNavigationBarButton(self, image: UIImage(named: "backarrowblack"), title: nil, isLeft: true)
        }
        if workingMode == WorkingMode.termsAndConditions {
            setupNavigationBarTitleType1("Terms and Conditions",viewController: self)
        }else if workingMode == WorkingMode.orahiOnFacebook {
            setupNavigationBarTitleType1("Orahi on Facebook",viewController: self)
        }else if workingMode == WorkingMode.orahiOnTwitter {
            setupNavigationBarTitleType1("Orahi on Twitter",viewController: self)
        }else if workingMode == WorkingMode.orahiOnLinkedIn {
            setupNavigationBarTitleType1("Orahi on LinkedIn",viewController: self)
        }else if workingMode == WorkingMode.aboutUs {
            setupNavigationBarTitleType1("About Us",viewController: self)
        }else if workingMode == WorkingMode.faq {
            setupNavigationBarTitleType1("FAQ's",viewController: self)
        }else if workingMode == WorkingMode.recharge {
            setupNavigationBarTitleType1("Recharge",viewController: self)
        }else if workingMode == WorkingMode.contactUs {
            setupNavigationBarTitleType1("Contact Us",viewController: self)
        }else if workingMode == WorkingMode.customUrl {
            setupNavigationBarTitleType1(customTitle,viewController: self)
        }
    }
    
    func onClickOfLeftBarButton(_ sender:Any){
        windowObject()!.hideActivityIndicator()
        self.navigationController?.popViewController(animated: true)
    }
    
    private func registerForNotifications(){
        
    }
    
    private func startUpInitialisations(){
        setAppearanceForViewController(self)
        animateWebViewAppearance()
        if workingMode == WorkingMode.termsAndConditions {
            trackScreenLaunchEvent(SLE_TERMS_AND_CONDITIONS)
        }else if workingMode == WorkingMode.orahiOnFacebook {
            trackScreenLaunchEvent(SLE_ORAHI_ON_FACEBOOK)
        }else if workingMode == WorkingMode.orahiOnTwitter {
            trackScreenLaunchEvent(SLE_ORAHI_ON_TWITTER)
        }else if workingMode == WorkingMode.orahiOnLinkedIn {
            trackScreenLaunchEvent(SLE_ORAHI_ON_LINKEDIN)
        }else if workingMode == WorkingMode.aboutUs {
            trackScreenLaunchEvent(SLE_ABOUT_US)
        }else if workingMode == WorkingMode.faq {
            trackScreenLaunchEvent(SLE_FAQ)
        }else if workingMode == WorkingMode.recharge {
        }else if workingMode == WorkingMode.contactUs {
            trackScreenLaunchEvent(SLE_CONTACT_US)
        }else if workingMode == WorkingMode.customUrl {
        }
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        if isLoaded {
            return
        }
        isLoaded = true
        if workingMode == WorkingMode.termsAndConditions {
            self.webview?.loadRequest(URLRequest(url: URL(string:APP_TERMS_OF_USE_AND_PRIVACY_POLICY)!))
        }else if workingMode == WorkingMode.orahiOnFacebook {
            self.webview?.loadRequest(URLRequest(url: URL(string:APP_FACEBOOK_PAGE_LINK)!))
        }else if workingMode == WorkingMode.orahiOnTwitter {
            self.webview?.loadRequest(URLRequest(url: URL(string:APP_TWITTER_PAGE_LINK)!))
        }else if workingMode == WorkingMode.orahiOnLinkedIn {
            self.webview?.loadRequest(URLRequest(url: URL(string:APP_LINKEDIN_PAGE_LINK)!))
        }else if workingMode == WorkingMode.aboutUs {
            self.webview?.loadRequest(URLRequest(url: URL(string:APP_ABOUT_US_PAGE_LINK)!))
        }else if workingMode == WorkingMode.faq {
            self.webview?.loadRequest(URLRequest(url: URL(string:APP_FAQ_PAGE_LINK)!))
        }else if workingMode == WorkingMode.recharge {
            let userInfo = Dbm().getUserInfo()
            self.webview?.loadRequest(URLRequest(url: URL(string:BASE_URL_RECHARGE+"account/mobrecharge"+"?id="+loggedInUserId()+"&&amount=\(amountToRecharge)")!))
        }else if workingMode == WorkingMode.contactUs {
            self.webview?.loadRequest(URLRequest(url: URL(string:APP_CONTACT_US_PAGE_LINK)!))
        }else if workingMode == WorkingMode.customUrl {
            self.webview?.loadRequest(URLRequest(url: URL(string:customUrl!)!))
        }
    }
    
    //MARK: - other functions
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool{
        webview?.isHidden = true
        logMessage(request.url)
        return true
    }
    
    func webViewDidStartLoad(_ webView: UIWebView){
        self.view.showActivityIndicator()
        webview?.isHidden = true
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView){
        animateWebViewAppearance2()
        self.view.hideActivityIndicator()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error){
        self.view.hideActivityIndicator()
    }
    
    func animateWebViewAppearance(){
        webview?.isHidden = true
        self.webview?.layer.opacity = 0
        UIView.animate(withDuration: 0.6, delay:0.0, options: UIViewAnimationOptions(), animations: { () -> Void in
            self.webview?.layer.opacity = 1
            }, completion: nil)
    }
    func animateWebViewAppearance2(){
        webview?.isHidden = false
        self.webview?.layer.opacity = 0
        UIView.animate(withDuration: 0.3, delay:0.0, options: UIViewAnimationOptions(), animations: { () -> Void in
            self.webview?.layer.opacity = 1
            }, completion: nil)
    }
}
