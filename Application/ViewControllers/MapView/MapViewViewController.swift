//
//  MapViewViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit
import MapKit

class MapViewViewController: UIViewController,UITextFieldDelegate,MKMapViewDelegate {
    
    //MARK: - variables and constants
    @IBOutlet fileprivate weak var mapView: MKMapView!
    @IBOutlet fileprivate weak var addressLabel: UILabel!
    
    var locationCoordinate: CLLocationCoordinate2D?
    var address: String?
        
    //MARK: - view controller life cycle methods
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    //MARK: - other methods
    private func setupForNavigationBar(){
        setAppearanceForNavigationBarType1(self.navigationController)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    private func registerForNotifications(){
    }
    
    private func startUpInitialisations(){
        setAppearanceForViewController(self)
        setupForMapView()
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        addressLabel.text = address
    }
    
    func setupForMapView() {
        mapView.delegate = self
        let annotation = MKPointAnnotation()
        annotation.coordinate = locationCoordinate!
        mapView.addAnnotation(annotation)
        updateMapRegion(locationCoordinate!)
    }
    
    func updateMapRegion(_ location:CLLocationCoordinate2D){
        let latDelta:CLLocationDegrees = 0.08
        let lonDelta:CLLocationDegrees = 0.08
        let span:MKCoordinateSpan = MKCoordinateSpanMake(latDelta, lonDelta)
        let region:MKCoordinateRegion = MKCoordinateRegionMake(location, span)
        mapView.setRegion(region, animated: false)
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?{
        var pulsatingView = self.mapView.dequeueReusableAnnotationView(withIdentifier: "location") as! SVPulsingAnnotationView?
        if isNull(pulsatingView) {
            pulsatingView = SVPulsingAnnotationView(annotation: annotation, reuseIdentifier: "location")
            pulsatingView!.annotationColor = APP_THEME_VOILET_COLOR
        }
        return pulsatingView
    }
    
    //MARK: - other functions
}
