//
//  FeedbackViewController.swift
//  Orahi
//
//  Created by Alok Singh on 06/06/16.
//  Copyright (c) 2016 Orahi. All rights reserved.
//


import UIKit
import IQKeyboardManagerSwift

class DailyRideNotificationViewController: UIViewController {
    
    //MARK: - variables and constants
    
    var notificationDetails: NSDictionary?
    var actionsArray = NSMutableArray()
    var imageUrlString : String?
    var messageString : String?
    var dateString : String?
    var otherDetails: NSMutableDictionary?
    
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var heightConstraintTableView: NSLayoutConstraint!
    
    //MARK: - view controller life cycle methods
    
    override func prefersStatusBarHidden() -> Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.navigationBarHidden)!
        }else{
            return false
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startupInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    //MARK: - other methods
    func setupForNavigationBar(){
        setAppearanceForNavigationBarType1(self.navigationController)
    }
    
    func registerForNotifications(){
    }
    
    func startupInitialisations(){
        setAppearanceForTableView(self.tableView)
        tableView.scrollEnabled = false
        fetchDetails()
        trackScreen(humanReadableScreenName("\(self.dynamicType)"))
        performAnimatedClickEffectType1(self.navigationController!.view)
        AppCommonFunctions.sharedInstance.playDefaultAlertSound()
    }
    
    func updateUserInterfaceOnScreen(){
        if isNotNull(imageUrlString){
            imageView.sd_setImageWithURL(NSURL(string: imageUrlString!), placeholderImage: nil)
        }
        descriptionLabel.text = messageString
    }
    
    func fetchDetails() {
        log("\nDaily Ride Notifications : \n\n\(notificationDetails)\n\n")
        prepareOptions()
        extractOtherDetails()
    }
    
    func prepareOptions() {
        var counter = 1
        while isNotNull(notificationDetails!.objectForKey("opt\(counter)")) {
            let optionDetails = NSMutableDictionary()
            optionDetails.setObject(nameForOption(notificationDetails!.objectForKey("opt\(counter)")! as! String), forKey: "name")
            optionDetails.setObject(notificationDetails!.objectForKey("url\(counter)")!, forKey: "actionUrl")
            if (notificationDetails!.objectForKey("tag") as! String).toInt() == 10 {
                optionDetails.setObject("inAppRequest", forKey: "actionType")
            }else if (notificationDetails!.objectForKey("tag") as! String).toInt() == 11 {
                optionDetails.setObject("webView", forKey: "actionType")
            }
            actionsArray.addObject(optionDetails)
            counter += 1
        }
        heightConstraintTableView.constant = CGFloat(actionsArray.count * 48)
        tableView.reloadData()
    }
    
    func extractOtherDetails() {
        imageUrlString = getDRNPictureUrlFromFileName(notificationDetails!.objectForKey("image_name")  as? String)
        messageString = (notificationDetails!.objectForKey("message")  as? String)
        copyData(getUserId(notificationDetails), destinationDictionary: otherDetails, destinationKey: "userId", methodName: #function)
    }
    
    func nameForOption(optionCode:String)->String{
        if ((optionCode as NSString).lowercaseString as NSString).isEqualToString("fixmyride"){
            return "Fix My Ride"
        }
        else if ((optionCode as NSString).lowercaseString as NSString).isEqualToString("n"){
            return "No"
        }
        else if ((optionCode as NSString).lowercaseString as NSString).isEqualToString("maybe"){
            return "May be"
        }else{
            return optionCode.enhancedString()
        }
    }
    
    internal static func getRequiredHeightDRNVC(notificationDetails:NSDictionary?)-> CGFloat{
        var counter = 0
        var indexing = 1
        while isNotNull(notificationDetails!.objectForKey("opt\(indexing)")) {
            counter += 1
            indexing += 1
        }
        if counter == 0 {
            return MAX_DRN_VIEW_HEIGHT - 48 * 3 + 10
        }
        else if counter == 1 {
            return MAX_DRN_VIEW_HEIGHT - 48 * 2
        }
        else if counter == 2 {
            return MAX_DRN_VIEW_HEIGHT - 48 * 1
        }
        else if counter == 3 {
            return MAX_DRN_VIEW_HEIGHT
        }else{
            return MAX_DRN_VIEW_HEIGHT
        }
    }
    
    //MARK: - UITableView Delegate & Data Source
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return actionsArray.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat{
        return DailyRideNotificationOptionsTableViewCell.getRequiredHeight()
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView .dequeueReusableCellWithIdentifier("DailyRideNotificationOptionsTableViewCell") as? DailyRideNotificationOptionsTableViewCell
        cell!.option = actionsArray.objectAtIndex(indexPath.row) as! NSDictionary
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath) as! DailyRideNotificationOptionsTableViewCell
        performAnimatedClickEffectType1(cell.contentView)
        if(isInternetConnectivityAvailable(true)==false){return}
        let action  = actionsArray.objectAtIndex(indexPath.row) as! NSDictionary
        //some special cases
        let actionName = (action.objectForKey("name") as! String).lowercaseString
        if actionName.contains("no"){
            AppCommonFunctions.sharedInstance.hidePopupViewController();return
        }
        if (action.objectForKey("actionType") as! NSString).isEqualToString("inAppRequest"){
            cell.infoLabel.startLoadingAnimation()
            let information = NSMutableDictionary()
            let webService = WebServices()
            webService.responseErrorOption = ResponseErrorOption.DontShowErrorResponseMessage
            webService.showSuccessResponseMessage = true
            webService.requestGet(action.objectForKey("actionUrl")! as! String,information: information) { (responseData) -> () in
                if let _ = responseData {
                    AppCommonFunctions.sharedInstance.hidePopupViewController()
                }
                cell.infoLabel.stopIt()
            }
        }
        else if (action.objectForKey("actionType") as! NSString).isEqualToString("webView"){
            UIApplication.sharedApplication().openURL(NSURL(string:(action.objectForKey("actionUrl")! as! String))!)
        }
    }
}
