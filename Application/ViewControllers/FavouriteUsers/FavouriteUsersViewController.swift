//
//  FavouriteUsersViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class FavouriteUsersViewController: UIViewController,DZNEmptyDataSetDelegate,DZNEmptyDataSetSource,UITableViewDelegate,UITableViewDataSource {
    
    //MARK: - variables and constants
    @IBOutlet fileprivate weak var tableView : UITableView?
    
    fileprivate var favouriteUsers = NSMutableArray()
    
    //MARK: - view controller life cycle methods
    
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        Dbm().saveChanges()
        Acf().updateFavouriteUsersPrioritySettingsOnServerIfRequired()
    }
        
    //MARK: - other methods
    private func setupForNavigationBar(){
        setAppearanceForNavigationBarType1(self.navigationController)
        setupNavigationBarTitleType1("Favourites",viewController: self)
        if self.isBackButtonRequired() {
            addNavigationBarButton(self, image: UIImage(named: "backarrowblack"), title: nil, isLeft: true)
        }
        addNavigationBarButton(self, image: UIImage(named: "searchIcon"), title: nil, isLeft: false)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    func onClickOfLeftBarButton(_ sender:Any){
        Acf().updateFavouriteUsersPrioritySettingsOnServerIfRequired()
        self.navigationController?.popViewController(animated: true)
    }
    
    func onClickOfRightBarButton(_ sender:Any){
        Acf().pushVC("SearchOrahiUsersViewController", navigationController: self.navigationController, isRootViewController: false, animated: true, modifyObject: nil)
    }
    
    private func registerForNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(FavouriteUsersViewController.updateUserInterfaceOnScreen), name: NSNotification.Name(rawValue: NOTIFICATION_FAVOURITES_UPDATED), object: nil)
    }
    
    private func startUpInitialisations(){
        registerNib("UserListTableViewCell", tableView: tableView)
        setAppearanceForViewController(self)
        setAppearanceForTableView(tableView)
        self.tableView!.isLongPressReorderEnabled = true
        Acf().getFavouritesUsers { (returnedData) in
            self.favouriteUsers = Dbm().getAllFavourites()
            self.tableView?.reloadData()
        }
        trackScreenLaunchEvent(SLE_FAVOURITES)
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        self.favouriteUsers = Dbm().getAllFavourites()
        self.tableView?.reloadData()
    }
    
    
    //MARK: - DZNEmptyDataSet Delegate & Data Source
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString!{
        let attributes = [NSFontAttributeName : UIFont.init(name: FONT_BOLD, size: 20)!]
        var message = "No favourites"
        if (isInternetConnectivityAvailable(false)==false){
            message = "Unable to load favourites"
        }
        if !isUserLoggedIn() {
            message = "Not Signed In"
        }
        return NSAttributedString(string: message, attributes: attributes)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString!{
        let attributes = [NSFontAttributeName : UIFont.init(name: FONT_REGULAR, size: 14)!]
        var message = "No favourites available at the moment"
        if (isInternetConnectivityAvailable(false)==false){
            message = MESSAGE_TEXT___FOR_NETWORK_NOT_REACHABILITY
        }
        if !isUserLoggedIn() {
            message = "You are not signed in yet.\nPlease Sign In"
        }
        return NSAttributedString(string: message, attributes: attributes)
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage!{
        if !isUserLoggedIn() {
            return UIImage(named:"searchLightGray")
        }
        if (isInternetConnectivityAvailable(false)==false){
            return UIImage(named:"wifi")
        }
        return UIImage(named:"searchLightGray")
    }
    
    //MARK: - UITableView Delegate & Data Source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favouriteUsers.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 64
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserListTableViewCell") as? UserListTableViewCell
        cell?.userInfo = favouriteUsers.object(at: (indexPath as NSIndexPath).row) as? Favourites
        cell?.userInfo?.priority = NSNumber(value: Int32((indexPath as NSIndexPath).row+1) as Int32)
        if (indexPath as NSIndexPath).row < 10 {
            cell?.priorityLabel?.font = UIFont.init(name: FONT_BOLD, size: 12)
            cell?.priorityLabel?.text = "\((indexPath as NSIndexPath).row+1)"
        }else{
            cell?.priorityLabel?.text = ""
        }
        return cell!
    }
    
    @objc func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let user =  favouriteUsers.object(at: (indexPath as NSIndexPath).row) as? Favourites
        let userInfo =  user!.dictionaryWithValues(forKeys: Array(user!.entity.attributesByName.keys))
        showUserProfile(userInfo as NSDictionary,navigationController: self.navigationController!)
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath){
        let object = favouriteUsers.object(at: (sourceIndexPath as NSIndexPath).row)
        favouriteUsers.removeObject(at: (sourceIndexPath as NSIndexPath).row)
        favouriteUsers.insert(object, at: (destinationIndexPath as NSIndexPath).row)
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(FavouriteUsersViewController.updateAndReload), object: nil)
        self.perform(#selector(FavouriteUsersViewController.updateAndReload), with: nil, afterDelay: 2)
    }
    
    func updateAndReload(){
        trackFunctionalEvent(FE_FAVOURITE_USERS_RE_ARRANGED, information: nil)
        let settings = Dbm().getSetting()
        settings.isFavouriteListUpdated = NSNumber(value: true)
        tableView!.reloadData()
        isInternetConnectivityAvailable(true)
    }
}
