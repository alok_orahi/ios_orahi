//
//  PlacesViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit

class PlacesViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    //MARK: - variables and constants
    @IBOutlet fileprivate weak var tableView : UITableView!
    @IBOutlet fileprivate weak var addPlacesButton : UIButton!
    @IBOutlet fileprivate weak var addPlacesButtonBottom : UIButton!
    @IBOutlet fileprivate weak var emptyPlacesPlaceholderView : UIView!
    
    var group : Group?
    var groupId : String?

    //MARK: - view controller life cycle methods
    override internal var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
        updateUserInterfaceOnScreen()
    }
    
    //MARK: - other methods
    private func setupForNavigationBar(){
        setAppearanceForNavigationBarType1(self.navigationController,color: self.group?.getGroupColor())
        setupNavigationBarTitleType1("Places",viewController: self)
        if self.isBackButtonRequired() {
            addNavigationBarButton(self, image: UIImage(named: "backarrowblack"), title: nil, isLeft: true)
        }
        addNavigationBarButton(self, image: UIImage(named: "sync"), title: nil, isLeft: false)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        addPlacesButton.tintColor = self.group?.getGroupColor()
        addPlacesButtonBottom.tintColor = self.group?.getGroupColor()
        tableView.reloadData()
    }
    
    func onClickOfLeftBarButton(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    func onClickOfRightBarButton(_ sender:Any){
        animateSyncButton()
        if(isInternetConnectivityAvailable(true)==false){return}
        Acf().syncGroupsAndPlacesFromServer()
    }
    
    func animateSyncButton(){
        let syncButton = self.navigationItem.rightBarButtonItem?.customView
        syncButton?.transform = CGAffineTransform.identity
        syncButton?.rotate(toAngle: CGFloat(M_PI * 2.0), duration: 2.0 , direction: .right, repeatCount: 1 , autoreverse: false)
    }
    
    private func registerForNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(PlacesViewController.updateUserInterfaceOnScreen), name: NSNotification.Name(rawValue: NOTIFICATION_NEED_PLACES_UPDATE), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(PlacesViewController.reLoadGroupObjectFromDb), name: NSNotification.Name(rawValue: NOTIFICATION_GROUPS_PLACES_UPDATED), object: nil)
    }
    
    func reLoadGroupObjectFromDb(){
        let updatedGroupObject = Dbm().getGroup(["groupId":safeString(groupId)])
        if isNotNull(updatedGroupObject) {
            self.group = updatedGroupObject
            updateUserInterfaceOnScreen()
        }
    }
    
    private func startUpInitialisations(){
        setupTableView()
        setAppearanceForViewController(self)
        groupId = safeString(group?.groupId)
    }
    
    func setupTableView(){
        registerNib("SingleLabelTableViewCellType2", tableView: tableView)
        registerNib("GroupControlTableViewCell", tableView: tableView)
        registerNib("PlacesControlTableViewCell", tableView: tableView)
        tableView!.separatorInset = UIEdgeInsets.zero
        tableView!.separatorStyle = UITableViewCellSeparatorStyle.none
        tableView!.layoutMargins = UIEdgeInsets.zero
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let placesCount = safeInt(self.group?.associatedPlaces?.count)
        addPlacesButton.isHidden = (placesCount == 0)
        emptyPlacesPlaceholderView.isHidden = !(placesCount == 0)
        return placesCount + 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        if indexPath.row == 0 {
            return 44
        }else if indexPath.row == 1 {
            return 128
        }else{
            return 175
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SingleLabelTableViewCellType2") as! SingleLabelTableViewCellType2
            cell.titleLabel?.text = "PLACES LINKED WITH \(safeString(group?.name).uppercased())"
            return cell
        }else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "GroupControlTableViewCell") as! GroupControlTableViewCell
            cell.group = self.group
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "PlacesControlTableViewCell") as! PlacesControlTableViewCell
            cell.group = self.group
            if safeInt(self.group?.associatedPlaces?.count) > (indexPath.row-2){
                let associatedPlace = self.group?.associatedPlaces?[indexPath.row-2] as! AssociatedPlaces
                cell.associatedPlace = associatedPlace
                cell.actualPlace = Dbm().getCorrespondingPlace(associatedPlace)
            }
            cell.navigationController = self.navigationController
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    @IBAction func onClickOfAddPlaces(){
        Acf().pushVC("AddEditPlaceViewController", navigationController: self.navigationController, isRootViewController: false, animated: true, modifyObject: { (viewController) in
            (viewController as! AddEditPlaceViewController).workMode = .createNewPlace
            (viewController as! AddEditPlaceViewController).groupAssociatedWithPlace = self.group
        })
    }
}
