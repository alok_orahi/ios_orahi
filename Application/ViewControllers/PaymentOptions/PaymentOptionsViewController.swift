//
//  SettingsViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class PaymentOptionsViewController: UIViewController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource {
    
    //MARK: - variables and constants
    @IBOutlet fileprivate weak var tableView : UITableView?
    
    
    //MARK: - view controller life cycle methods
    
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
    }
    
    //MARK: - other methods
    private func setupForNavigationBar(){
        setAppearanceForNavigationBarType1(self.navigationController)
        setupNavigationBarTitleType1("Payments",viewController: self)
        if self.isBackButtonRequired() {
            addNavigationBarButton(self, image: UIImage(named: "backarrowblack"), title: nil, isLeft: true)
        }
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    func onClickOfLeftBarButton(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    private func registerForNotifications(){
        
    }
    
    private func startUpInitialisations(){
        registerNib("SingleLabelTableViewCell", tableView: tableView)
        setAppearanceForViewController(self)
        setAppearanceForTableView(tableView)
        fetchAndCacheAccountSummary()
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        
    }
    
    //MARK: - UITableView Delegate & Data Source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SingleLabelTableViewCell") as? SingleLabelTableViewCell
        cell?.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        if (indexPath as NSIndexPath).row == 0{
            cell?.titleLabel?.text = "Account Summary"
        }
        if (indexPath as NSIndexPath).row == 1{
            cell?.titleLabel?.text = "Recharge"
        }
        if (indexPath as NSIndexPath).row == 2{
            cell?.titleLabel?.text = "Withdrawl"
        }
        return cell!
    }
    
    @objc func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as? SingleLabelTableViewCell
        performAnimatedClickEffectType1(cell!.titleLabel!)
        if isSystemReadyToProcessThis() {
            if (indexPath as NSIndexPath).row == 0{
                Acf().pushVC("AccountSummaryViewController", navigationController: self.navigationController, isRootViewController: false, animated: true) { (viewControllerObject) -> () in
                }
            }
            else if (indexPath as NSIndexPath).row == 1{
                Acf().pushVC("RechargeViewController", navigationController: self.navigationController, isRootViewController: false, animated: true) { (viewControllerObject) -> () in
                }
            }else if (indexPath as NSIndexPath).row == 2{
                Acf().pushVC("WithdrawlViewController", navigationController: self.navigationController, isRootViewController: false, animated: true) { (viewControllerObject) -> () in
                }
            }
        }
    }
    
    //MARK: - other functions
    
}
