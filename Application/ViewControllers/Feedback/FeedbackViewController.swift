//
//  FeedbackViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit

class FeedbackViewController: UIViewController,UITextFieldDelegate,UITagsViewDelegate {
    
    //MARK: - variables and constants
    @IBOutlet fileprivate weak var infoLabel: UILabel!
    @IBOutlet fileprivate weak var commentsTextField: MKTextField!
    @IBOutlet fileprivate weak var starRatingView: HCSStarRatingView!
    @IBOutlet fileprivate weak var emojiRateView: EmojiRateView!
    @IBOutlet fileprivate weak var scrollView: UIScrollView!
    @IBOutlet fileprivate weak var scrollViewSubView: UIView!
    @IBOutlet fileprivate weak var tagsView: UITags!
    @IBOutlet fileprivate weak var doneButton: TKTransitionSubmitButton!

    fileprivate var textPartOneDynamic = "Your feedback for"
    fileprivate var textPartTwoDynamic = ""
    var userInfo: NSDictionary!
    fileprivate var tags = ["PUNCTUAL","VETERAN","HELPFUL","PLEASANT","TRUSTWORTHY"]
    fileprivate var selectedTag = ""
    
    //MARK: - view controller life cycle methods
    
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scrollViewSubView.layoutIfNeeded()
        scrollViewSubView.translatesAutoresizingMaskIntoConstraints = true
        scrollViewSubView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: scrollViewSubView.frame.size.height)
        scrollView.contentSize = scrollViewSubView.frame.size
    }
    
    //MARK: - other methods
    private func setupForNavigationBar(){
        setAppearanceForNavigationBarType1(self.navigationController)
    }
    
    private func registerForNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(FeedbackViewController.updateAppearanceOfTextField), name: NSNotification.Name.UITextViewTextDidBeginEditing, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(FeedbackViewController.updateAppearanceOfTextField), name: NSNotification.Name.UITextViewTextDidEndEditing, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(FeedbackViewController.updateAppearanceOfTextField), name: NSNotification.Name.UITextFieldTextDidBeginEditing, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(FeedbackViewController.updateAppearanceOfTextField), name: NSNotification.Name.UITextFieldTextDidEndEditing, object: nil)
    }
    
    private func startUpInitialisations(){
        setAppearanceForViewController(self)
        setAppearanceForMKTextField(commentsTextField)
        updateAppearanceOfTextField()
        doneButton.layoutIfNeeded()
        doneButton.layer.cornerRadius = doneButton.bounds.size.width/2
        doneButton.layer.masksToBounds = true
        doneButton.requiredBackgroundColor = UIColor(red: 100/255, green: 196/255, blue: 219/255, alpha: 1.0)
        trackScreenLaunchEvent(SLE_FEEDBACK)
        
        emojiRateView.rateValueChangeCallback = {(rateValue: Float) -> Void in
            self.starRatingView.value = CGFloat(rateValue)
            self.updateUserInterfaceOnScreen()
        }
        
        emojiRateView.rateValue = 4.0
        starRatingView.value = 4.0
        tagsView.tags = tags
        tagsView.delegate = self
        execMain ({ (returnedData) in
            self.animateTopInfoLabel()
        },delay: 1)
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        let textPartOne = textPartOneDynamic
        let textPartTwo = textPartTwoDynamic
        let combinedText = textPartOne+textPartTwo
        let attributesBig = [NSFontAttributeName:UIFont.init(name: FONT_BOLD, size: 23)!,
                             NSForegroundColorAttributeName:APP_THEME_VOILET_COLOR] as NSDictionary
        let attributesSmall = [NSFontAttributeName:UIFont.init(name: FONT_REGULAR, size: 13)!,
                               NSForegroundColorAttributeName:APP_THEME_VOILET_COLOR] as NSDictionary
        let attributedString = NSMutableAttributedString(string:combinedText)
        attributedString.setAttributes(attributesSmall as? [String : Any], range: NSMakeRange(0, textPartOne.length))
        attributedString.setAttributes(attributesBig as? [String : Any], range: NSMakeRange(textPartOne.length, textPartTwo.length))
        infoLabel.attributedText = attributedString
        UIView.animate(withDuration: 0.4, animations: {
            if self.starRatingView.value == 5.0 {
                self.tagsView.isHidden = false
                self.commentsTextField.isHidden = true
            }else{
                self.tagsView.isHidden = true
                self.commentsTextField.isHidden = false
            }
        }) 
    }
    
    //MARK: - other functions
    
    func onClickOfLeftBarButton(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    func canContinueForSendingFeedback()->(Bool){
        let canContinue = true
        return canContinue
    }
    
    @IBAction func onClickOfDoneButton(){
        if(isInternetConnectivityAvailable(false)==false){return}
        if canContinueForSendingFeedback(){
            let information = NSMutableDictionary()
            copyData(self.userInfo?.object(forKey: "id") , destinationDictionary: information, destinationKey: "requestId", methodName: #function)
            copyData("\(starRatingView.value)" , destinationDictionary: information, destinationKey: "rating", methodName: #function)
            copyData(getUserId(userInfo) , destinationDictionary: information, destinationKey: "toUser", methodName: #function)
            copyData(loggedInUserId() , destinationDictionary: information, destinationKey: "fromUser", methodName: #function)
            
            if self.starRatingView.value > 4 {
                copyData(selectedTag , destinationDictionary: information, destinationKey: "comment", methodName: #function)
            }else{
                copyData(commentsTextField.text , destinationDictionary: information, destinationKey: "comment", methodName: #function)
            }
            
            Acf().sendFeedback(information)
            Acf().navigationController!.dismissPopupViewController(SLpopupViewAnimationType.fade)
        }
    }
    
    func updateAppearanceOfTextField(){
        Acf().updateAppearanceOfTextFieldType1(commentsTextField)
        updateAppearanceOfDoneButton()
    }
    
    func updateAppearanceOfDoneButton(){
        if commentsTextField.text?.length > 0 || starRatingView.value > 0{
            doneButton.isEnabled = true
        }else{
            doneButton.isEnabled = false
        }
    }
    
    @IBAction func valueChanged(){
        emojiRateView.rateValue = Float(starRatingView.value)
        updateUserInterfaceOnScreen()
    }
    
    var condition = true
    func animateTopInfoLabel() {
        if condition {
            textPartOneDynamic = "Your feedback for"
            textPartTwoDynamic = "\n\(userInfo.object(forKey: "user_name")!)"
        }else{
            textPartOneDynamic = "Tap on stars to rate"
            textPartTwoDynamic = "\n\(userInfo.object(forKey: "user_name")!)"
        }
        updateUserInterfaceOnScreen()
        infoLabel.layer.opacity = 0.0
        UIView.animate(withDuration: 0.5, animations: {
            self.infoLabel.layer.opacity = 1.0
        }) 
        condition = !condition
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(FeedbackViewController.animateTopInfoLabel), object: nil)
        self.perform(#selector(FeedbackViewController.animateTopInfoLabel), with: nil , afterDelay: 3)
    }

    //MARK: - UITagsViewDelegate
    
    func tagSelected(atIndex index:Int) -> Void {
        selectedTag = tags[index]
        execMain ({ (returnedData) in
            self.tagsView.selectedTags = [index]
            self.tagsView.collectionView?.reloadData()
        },delay: 0.1)
    }
    
    func tagDeselected(atIndex index:Int) -> Void {
        selectedTag = ""
    }
}
