
//
//  RideDetailViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit
import MapKit

class RideDetailViewController: UIViewController,UITextFieldDelegate,MKMapViewDelegate,UIGestureRecognizerDelegate,UITableViewDataSource,UITableViewDelegate,UIViewControllerPreviewingDelegate {
    
    //MARK: - variables and constants
    @IBOutlet fileprivate weak var backToHomeButton: UIButton!
    @IBOutlet fileprivate weak var rideDetailButton: UIButton!
    @IBOutlet fileprivate weak var rideTimeLineButton: UIButton!
    @IBOutlet fileprivate weak var rideChatButton: UIButton!
    @IBOutlet fileprivate weak var infoLabel4: UILabel!
    @IBOutlet fileprivate var bottomInfoHolderView: UIView!
    @IBOutlet fileprivate weak var collectionView: UICollectionView!
    @IBOutlet fileprivate weak var tableView: UITableView!
    fileprivate var mapView: MKMapView!
    fileprivate var mapViewGapFromBottom = 140.0 as CGFloat
    fileprivate var shouldFitMapToAnnotations = true
    
    weak var tab1ParentViewController: Tab1ViewController?
    
    fileprivate var users : NSMutableArray?
    var selectedDayDate = Date()
    var selectedDayTime = Date()
    var trueForOwnerFalseforPassenger = false
    var trueForHomeToDestinationFalseforDestinationToHome = false
    fileprivate var annotationHome : MKPointAnnotation?
    fileprivate var annotationDestination : MKPointAnnotation?
    fileprivate var mapManager = MapManager()
    fileprivate var usersAnnotationsAddedOnMap = NSMutableArray()
    
    fileprivate var startLocationCoordinate : CLLocationCoordinate2D?
    fileprivate var endLocationCoordinate : CLLocationCoordinate2D?
    fileprivate var currentLocation: CLLocation?
    fileprivate var groupChatDialogue: QBChatDialog?
    
    // next ride related variable
    var allUsers = NSMutableArray()
    var usersForRide = NSMutableArray()
    var rideStatus = RideStatus.none
    var isTodaysRide = false
    var trueIfLessThanOneHourLeft = false
    var dateOfRide = Date().dateByAddingDays(2)
    fileprivate var carAnnotationView : MKAnnotationView?
    
    //MARK: - view controller life cycle methods
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        updateUserLocationIfNeeded()
    }
    
    //MARK: - other methods
    private func setupForNavigationBar(){
    }
    
    private func registerForNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(RideDetailViewController.statusBarFrameDidChange), name: NSNotification.Name.UIApplicationDidChangeStatusBarFrame, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(RideDetailViewController.hideThingsToViewMapInBetterWay), name: NSNotification.Name(rawValue: NOTIFICATION_HIDE_FOR_MAP_VIEW), object: nil)
    }
    
    func statusBarFrameDidChange(_ frame:CGRect){
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
    }
    
    func trueForRideWaitingFalseforStarted() -> Bool {
        return  Date().isEarlierThanDate(dateOfRide)
    }
    
    func isAtleast30MinutesLeftForRideStart() -> Bool {
        return  Date().isLaterThanDate(dateOfRide.dateBySubtractingMinutes(30))
    }
    
    private func startUpInitialisations(){
        setupTableView()
        setAppearanceForViewController(self)
        setupForMapView()
        updateUserInterfaceOnScreen()
        setupFor3DTouchPreviewIfAvailable()
        setBorder(backToHomeButton, color: APP_THEME_VOILET_COLOR, width: 1, cornerRadius: 1)
        setBorder(rideChatButton, color: APP_THEME_VOILET_COLOR, width: 1, cornerRadius: rideChatButton.frame.size.height/2)
        backToHomeButton.setTitleColor(APP_THEME_VOILET_COLOR, for:UIControlState.normal)
        updateStartAndEndLocation()
        Acf().rideDetailVC = self
        trackScreenLaunchEvent(SLE_RIDE_DETAIL)
    }
    
    private func setupTableView(){
        registerNib("UserMiniProfileTableViewCell", tableView: tableView)
        tableView!.separatorInset = UIEdgeInsets.zero
        tableView!.separatorStyle = UITableViewCellSeparatorStyle.none
        tableView!.layoutMargins = UIEdgeInsets.zero
        tableView!.tableHeaderView = self.bottomInfoHolderView
        setFooterView(tableView: self.tableView,44)
        execMain({
            self.tableView.addParallax(with: self.mapView, andHeight: DEVICE_HEIGHT - self.mapViewGapFromBottom)
        }, delay: 0.3)
    }
    
    func setupForMapView() {
        mapView = MKMapView(x: 0, y: 0, w: DEVICE_WIDTH, h: DEVICE_HEIGHT - mapViewGapFromBottom)
        mapView.showsUserLocation = true
        mapView.delegate = self
        mapView.showsUserLocation = true
        mapView.delegate = self
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(self.didDragMap(_:)))
        panGesture.delegate = self
        mapView.addGestureRecognizer(panGesture)
    }
    
    //MARK: - 3D Touch Setup and Support
    
    func setupFor3DTouchPreviewIfAvailable() {
        execMain ({ (returnedData) in
            if self.traitCollection.forceTouchCapability == .available {
                self.registerForPreviewing(with: self, sourceView: self.tableView!)
            }
        },delay:2)
    }
    
    //MARK: - other functions
    
    func setDataFromArray(_ results:NSMutableArray?){
        if isNotNull(results)&&results!.count>0{
            users =  results
        }else{
            switchBackToHome()
            users = NSMutableArray()
        }
        if isNotNull(users)&&users!.count>0{
            //hmmm it seems ride is still running
        }else{
            //it seems ride is completed , so switch back to home
            switchBackToHome()
        }
        updateUserInterfaceOnScreen()
        tableView.reloadData()
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        if Acf().isAddressAdded() {
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(RideDetailViewController.updateUserInterfaceOnScreenPrivate), object: nil)
            self.perform(#selector(RideDetailViewController.updateUserInterfaceOnScreenPrivate), with: nil, afterDelay: 1)
        }
        collectionView.reloadData()
    }
    
    func updateUserLocationsOnMap(){
        if isSystemReadyToProcessThis() {
            self.mapView.removeAnnotations(usersAnnotationsAddedOnMap as! [MKAnnotation])
            usersAnnotationsAddedOnMap.removeAllObjects()
            if isNotNull(users) && users!.count > 0{
                for user in users! {
                    Dbm().addUserId(getUserId(user as? NSDictionary))
                    let annotation = UserType1PinAnnotation()
                    annotation.coordinate = userLocationToShow(user as! NSDictionary)
                    annotation.userInfo = user
                    usersAnnotationsAddedOnMap.add(annotation)
                }
                self.mapView.addAnnotations(usersAnnotationsAddedOnMap as! [MKAnnotation])
            }
            
            if shouldFitMapToAnnotations {
                shouldFitMapToAnnotations = false
                execMain({[weak self]  in guard let `self` = self else { return }
                    self.mapView.updateRegion(forAnnotations: self.mapView.annotations, animated: true)
                    },delay: 2.0)
            }
            
            if (rideStatus == .confirmed || rideStatus == .confirmedButDelayed) {
                blinkTheCar()
            }
            
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(RideDetailViewController.updateUserLocationsOnMap), object: nil)
            self.perform(#selector(RideDetailViewController.updateUserLocationsOnMap), with: nil, afterDelay: 3)
        }
    }
    
    func blinkTheCar(){
        if carAnnotationView != nil {
            UIView.animate(withDuration: 0.4, animations: {
                self.carAnnotationView!.layer.opacity = 0.2
                self.carAnnotationView!.origin.y = self.carAnnotationView!.origin.y + 1
            }, completion: { (completed) in
                UIView.animate(withDuration: 0.4, animations: {
                    self.carAnnotationView?.layer.opacity = 1
                    self.carAnnotationView!.origin.y = self.carAnnotationView!.origin.y - 1
                }, completion: { (completed) in
                })
            })
        }
    }
    
    func updateUserInterfaceOnScreenPrivate(){
        if isSystemReadyToProcessThis() {
            //step 1 : remove everything from the map
            mapView.removeAnnotations(mapView.annotations)
            
            //step 2 : add home and destination locations on map
            addHomeAndDestinationAnnotation()
            
            //step 3 : add users annotations on map
            updateUserLocationsOnMap()
            
            //step 4 : decide and draw route on map
            addUpdatedRouteInMap()
            
            //step 5 : fetch details about upcoming or currently running ride
            checkAndProcessTheRide()
            
            //step 6 :
            updateUserLocationIfNeeded()
            
            //step 7 :
            setupRideRelatedChatGroupIfRequired()
            
            //step 8 :
            monitorRideEventsIfNeeded()
            
            //step 9 :
            collectionView.reloadData()
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if trueForOwnerFalseforPassenger && annotation is UserType1PinAnnotation{
            //if i am a owner
            //annotation is of custom type
            //then it is for passenger
            var annotationView = self.mapView.dequeueReusableAnnotationView(withIdentifier: "location") as! UserType1MKAnnotationView?
            if isNull(annotationView) {
                annotationView = UserType1MKAnnotationView(annotation: annotation, reuseIdentifier: "location")
            }
            annotationView!.userInfo = (annotation as! UserType1PinAnnotation).userInfo as? NSDictionary
            annotationView!.canShowCallout = false;
            annotationView!.setup()
            annotationView?.transform = CGAffineTransform(scaleX: ANNOTATION_TRANSFORM_ADJUST, y: ANNOTATION_TRANSFORM_ADJUST)
            return annotationView
        }else if (trueForOwnerFalseforPassenger == false) && annotation is UserType1PinAnnotation{
            //if i am a passenger
            //annotation is of custom type
            //then it is for owner/other passengers
            var annotationView = self.mapView.dequeueReusableAnnotationView(withIdentifier: "passenger") as MKAnnotationView?
            if isNull(annotationView) {
                annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "annotationView")
            }
            annotationView!.canShowCallout = false;
            if trueForRideWaitingFalseforStarted() {
                annotationView?.image = UIImage(named: "carNormal")
            }else{
                annotationView?.image = UIImage(named: "carActive")
            }
            annotationView?.size = CGSize(width: 33, height: 33)
            carAnnotationView = annotationView
            carAnnotationView?.transform = CGAffineTransform(scaleX: ANNOTATION_TRANSFORM_ADJUST, y: ANNOTATION_TRANSFORM_ADJUST)
            return annotationView
        }else if annotation is MKUserLocation && trueForOwnerFalseforPassenger {
            //if i am a owner
            //annotation is of MKUserLocation type
            //then it is for owner . i.e me and i must modify its default look to car icon
            var annotationView = self.mapView.dequeueReusableAnnotationView(withIdentifier: "driverLocation") as MKAnnotationView?
            if isNull(annotationView) {
                annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "annotationView")
            }
            annotationView!.canShowCallout = false;
            if trueForRideWaitingFalseforStarted() {
                annotationView?.image = UIImage(named: "carNormal")
            }else{
                annotationView?.image = UIImage(named: "carActive")
            }
            annotationView?.size = CGSize(width: 33, height: 33)
            carAnnotationView = annotationView
            carAnnotationView?.transform = CGAffineTransform(scaleX: ANNOTATION_TRANSFORM_ADJUST, y: ANNOTATION_TRANSFORM_ADJUST)
            return annotationView
        }else if annotation is MKUserLocation && (trueForOwnerFalseforPassenger == false) {
            //if i am a passenger
            //annotation is of MKUserLocation type
            //i have to show default view for annotation , so returning nil
            return nil
        }else if annotation is MKPointAnnotation{//home and destination location annotation
            if let annotationObject = annotation as? MKPointAnnotation {
                var annotationView = self.mapView.dequeueReusableAnnotationView(withIdentifier: "homeDestinationLocation") as MKAnnotationView?
                if isNull(annotationView) {
                    annotationView = MKAnnotationView(annotation: annotationObject, reuseIdentifier: "homeDestinationLocation")
                }
                if annotationObject == annotationHome {
                    annotationView?.image = UIImage(named: "home")
                }else if annotationObject == annotationDestination {
                    annotationView?.image = UIImage(named: "office")
                }
                annotationView?.canShowCallout = true
                annotationView?.size = CGSize(width: 33, height: 33)
                annotationView?.transform = CGAffineTransform(scaleX: ANNOTATION_TRANSFORM_ADJUST, y: ANNOTATION_TRANSFORM_ADJUST)
                return annotationView
            }
        }
        return nil
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_RESET_IDLE_TIMER_RIDE_HOME), object: nil, userInfo: nil)
        if let annotation = view.annotation as? UserType1PinAnnotation {
            performAnimatedClickEffectType1(view)
            UserMiniProfileView.showMiniProfileView(annotation.userInfo as! NSDictionary,rideDate: selectedDayDate, onView: self.view,parentVC: self,autoHide: true,after:10 ,completionBlock:{[weak self] (returnedData) -> () in guard let `self` = self else { return }
                showUserProfile(returnedData as! NSDictionary, navigationController: Acf().navigationController!)
                },removedBlock:{(returnedData) -> () in })
            //show user information on popup view
        }
        hideThingsToViewMapInBetterWay()
    }
    
    func addUpdatedRouteInMap(){
        updateStartAndEndLocation()
        if (rideStatus == .confirmed || rideStatus == .confirmedButDelayed) {
            if startLocationCoordinate != nil && endLocationCoordinate != nil {
                mapManager.directionsUsingGoogle(startLocationCoordinate!, to: endLocationCoordinate!, directionCompletionHandler: { (route, directionInformation, boundingRegion, error) in
                    execMain({[weak self] in guard let `self` = self else { return }
                        
                        if(error != nil){
                            logMessage(error!)
                        }
                        else{
                            self.mapView.add(route!)
                            self.mapView.setVisibleMapRect(boundingRegion!, animated: true)
                        }
                    })
                })
            }else{
                logMessage("\n\n\nunexpected case , please fix it here ==> addUpdatedRouteInMap \n\n")
            }
        }
    }
    
    func addHomeAndDestinationAnnotation(){
        if Acf().isAddressAdded(){
            let settings = Dbm().getSetting()
            
            annotationHome = MKPointAnnotation()
            annotationHome!.coordinate = CLLocationCoordinate2DMake(settings.homeLocationLatitude!.doubleValue,settings.homeLocationLongitude!.doubleValue)
            annotationHome?.title = "Home"
            self.mapView.addAnnotation(annotationHome!)
            
            annotationDestination = MKPointAnnotation()
            annotationDestination!.coordinate = CLLocationCoordinate2DMake(settings.destinationLocationLatitude!.doubleValue,settings.destinationLocationLongitude!.doubleValue)
            annotationDestination?.title = "\(destinationCapitalizedName())"
            self.mapView.addAnnotation(annotationDestination!)
        }
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool){
        
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MKPolyline {
            let polylineRenderer = MKPolylineRenderer(overlay: overlay)
            if trueForRideWaitingFalseforStarted() {
                polylineRenderer.strokeColor = APP_THEME_VOILET_COLOR
            }else{
                polylineRenderer.strokeColor = APP_THEME_RED_COLOR
            }
            polylineRenderer.lineWidth = 3
            return polylineRenderer
        }
        return MKOverlayRenderer()
    }
    
    
    //MARK: - Helpers
    
    func checkAndProcessTheRide(){
        updateRideTimeLineButton()
        updateRideDetailButton()
        updateInfoLabels()
    }
    
    func updateInfoLabels(){
        if rideStatus == .completed {
            infoLabel4.text = " Ride completed"
        }else if (rideStatus == .confirmed || rideStatus == .confirmedButDelayed) {
            infoLabel4.text = " Ride confirmed with"
        }else if rideStatus == .invitationSent {
            infoLabel4.text = " Invitation sent"
        }else if rideStatus == .invitationReceived {
            infoLabel4.text = " Invitation received"
        }
        if isNotNull(usersForRide){
            if usersForRide.count > 0{
                infoLabel4.backgroundColor = Acf().colorForUserAsPerTravelRequestStatus(usersForRide.firstObject as! NSDictionary)
            }else{
                infoLabel4.backgroundColor = UIColor.gray
            }
        }else{
            infoLabel4.backgroundColor = UIColor.gray
        }
    }
    
    
    func updateRideDetailButton(){
        rideDetailButton.stopPulseEffect()
        if trueForRideWaitingFalseforStarted() && trueIfLessThanOneHourLeft {
            if rideStatus == .confirmed {
                rideDetailButton.setBackgroundColor(APP_THEME_LIGHT_GRAY_COLOR, forState: UIControlState.normal)
                rideDetailButton.setTitle("Ride in "+remaningTime(Date(), endDate: dateOfRide), for: UIControlState.normal)
            }
        }else if trueForRideWaitingFalseforStarted() && !trueIfLessThanOneHourLeft {
            if rideStatus == .confirmed {
                rideDetailButton.setBackgroundColor(APP_THEME_VOILET_COLOR, forState: UIControlState.normal)
                rideDetailButton.setTitle("Ride in "+remaningTime(Date(), endDate: dateOfRide), for: UIControlState.normal)
                rideDetailButton.startPulse(with: APP_THEME_GREEN_COLOR, offset: CGSize(width: 0,height: 0), frequency: 4.0)
            }
        }else if (rideStatus == .confirmed || rideStatus == .confirmedButDelayed) {
            if (rideStatus == .confirmedButDelayed){
                rideDetailButton.setBackgroundColor(UIColor.red, forState: UIControlState.normal)
                rideDetailButton.setTitle("Ride Completed ? Settle it !!!", for: UIControlState.normal)
                rideDetailButton.stopPulseEffect()
            }else{
                rideDetailButton.setBackgroundColor(APP_THEME_RED_COLOR, forState: UIControlState.normal)
                rideDetailButton.setTitle("Active Ride "+elapsedTime(dateOfRide), for: UIControlState.normal)
                rideDetailButton.startPulse(with: UIColor.red, offset: CGSize(width: 0,height: 0), frequency:1.0)
            }
        }else if rideStatus == .completed {
            rideDetailButton.setBackgroundColor(APP_THEME_GREEN_COLOR, forState: UIControlState.normal)
            rideDetailButton.setTitle("Ride Completed", for: UIControlState.normal)
            rideDetailButton.stopPulseEffect()
        }
        
        if rideStatus == .invitationSent {
            rideDetailButton.setBackgroundColor(UIColor.orange, forState: UIControlState.normal)
            if Date().isEarlierThanDate(dateOfRide){
                rideDetailButton.setTitle("Invite Sent for Ride in "+remaningTime(Date(), endDate: dateOfRide), for: UIControlState.normal)
            }else{
                rideDetailButton.setTitle("Invite Sent for Ride at " + dateOfRide.stringTimeOnly_AM_PM_FormatValue(), for: UIControlState.normal)
            }
        }else if rideStatus == .invitationReceived {
            rideDetailButton.setBackgroundColor(UIColor.orange, forState: UIControlState.normal)
            if Date().isEarlierThanDate(dateOfRide){
                rideDetailButton.setTitle("Invite Received for Ride in "+remaningTime(Date(), endDate: dateOfRide), for: UIControlState.normal)
            }else{
                rideDetailButton.setTitle("Invite Received for Ride at " + dateOfRide.stringTimeOnly_AM_PM_FormatValue(), for: UIControlState.normal)
            }
        }
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(RideDetailViewController.updateRideDetailButton), object: nil)
        if rideDetailButton.isHidden == false {
            self.perform(#selector(RideDetailViewController.updateRideDetailButton), with: nil, afterDelay: 1)
        }
        updateRideTimeLineButton()
    }
    
    func updateRideTimeLineButton(){
        rideTimeLineButton.isHidden = true
        if trueForRideWaitingFalseforStarted() == false && (rideStatus == .confirmed || rideStatus == .confirmedButDelayed) {
            if isNotNull(usersForRide) &&
                isNotNull(groupChatDialogue){
                rideTimeLineButton.isHidden = false
            }
        }
    }
    
    func clean(){
        usersAnnotationsAddedOnMap.removeAllObjects()
        shouldFitMapToAnnotations = true
        mapView.removeAnnotations(mapView.annotations)
        users?.removeAllObjects()
        selectedDayDate = Date()
        selectedDayTime = Date()
        trueForOwnerFalseforPassenger = false
        trueForHomeToDestinationFalseforDestinationToHome = false
        annotationHome = nil
        annotationDestination = nil
        startLocationCoordinate = nil
        endLocationCoordinate = nil
        groupChatDialogue = nil
        usersForRide = NSMutableArray()
        allUsers = NSMutableArray()
        rideStatus = .none
        trueIfLessThanOneHourLeft = false
        dateOfRide = Date()
    }
    
    //MARK: - User Actions
    
    @IBAction func onClickOfSeeRideEventsButton(){
        if trueForRideWaitingFalseforStarted() == false && (rideStatus == .confirmed || rideStatus == .confirmedButDelayed) {
            if isNotNull(usersForRide) &&
                isNotNull(groupChatDialogue){
                Acf().showRideEventViewerScreen(self.usersForRide, isOwner: trueForOwnerFalseforPassenger, isHomeToDestination: trueForHomeToDestinationFalseforDestinationToHome,selectedDayDate: selectedDayDate, groupChatDialogue: groupChatDialogue!)
            }
        }
    }
    
    @IBAction func onClickOfHomeButton(){
        switchBackToHome()
    }
    
    func switchBackToHome() {
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        tab1ParentViewController?.switchToViewController(true)
    }
    
    //MARK: - User Location
    
    func shouldUseCurrentLocation() -> Bool {
        if trueForRideWaitingFalseforStarted() == false && (rideStatus == .confirmed || rideStatus == .confirmedButDelayed){
            return true
        }
        return false
    }
    
    func userLocationToShow(_ user:NSDictionary)->CLLocationCoordinate2D{
        let locationCoordinate : CLLocationCoordinate2D?
        if shouldUseCurrentLocation(){
            let locationInfo = Acf().locationCache.object(forKey: getUserId(user)) as? NSDictionary
            if isNotNull(locationInfo){
                //if user location on riders group is received
                let latitude = "\(locationInfo!.object(forKey: "lat")!)"
                let longitude = "\(locationInfo!.object(forKey: "lng")!)"
                locationCoordinate = CLLocationCoordinate2DMake(Double(latitude)!, Double(longitude)!)
                logMessage("updating user location on map -> using current location mode -> for  \(locationInfo)")
            }else{
                //if user location on riders group is not received yet , so we will show user based upon destination and home address.
                logMessage("updating user location on map -> using address location mode -> for \(user.object(forKey: "user_name"))")
                locationCoordinate = getUserLocationBasedOnAddress(user)
            }
        }else{
            locationCoordinate = getUserLocationBasedOnAddress(user)
        }
        return locationCoordinate!
    }
    
    
    func userLiveLocation(_ user:NSDictionary)->CLLocationCoordinate2D?{
        var locationCoordinate : CLLocationCoordinate2D?
        if shouldUseCurrentLocation(){
            let locationInfo = Acf().locationCache.object(forKey: getUserId(user)) as? NSDictionary
            if isNotNull(locationInfo){
                let latitude = "\(locationInfo!.object(forKey: "lat")!)"
                let longitude = "\(locationInfo!.object(forKey: "lng")!)"
                locationCoordinate = CLLocationCoordinate2DMake(Double(latitude)!, Double(longitude)!)
            }
        }
        return locationCoordinate
    }
    
    func getUserLocationBasedOnAddress(_ user:NSDictionary)->CLLocationCoordinate2D{
        var destinationLocationLatitude : String?
        var destinationLocationLongitude : String?
        var homeLocationLatitude : String?
        var homeLocationLongitude : String?
        if trueForHomeToDestinationFalseforDestinationToHome {
            if isNotNull(user.object(forKey: "home_lat")){
                homeLocationLatitude = user.object(forKey: "home_lat")as? String
                homeLocationLongitude = user.object(forKey: "home_lng") as? String
            }else{
                if isNotNull(user.object(forKey: "ofc_lat")){
                    destinationLocationLatitude = user.object(forKey: "ofc_lat")as? String
                    destinationLocationLongitude = user.object(forKey: "ofc_lng") as? String
                }else{
                    if isNotNull(user.object(forKey: "par_home_lat")){
                        homeLocationLatitude = user.object(forKey: "par_home_lat")as? String
                        homeLocationLongitude = user.object(forKey: "par_home_lng") as? String
                    }else{
                        if isNotNull(user.object(forKey: "par_ofc_lat")){
                            destinationLocationLatitude = user.object(forKey: "par_ofc_lat")as? String
                            destinationLocationLongitude = user.object(forKey: "par_ofc_lng") as? String
                        }
                    }
                }
            }
        }else{
            if isNotNull(user.object(forKey: "ofc_lat")){
                destinationLocationLatitude = user.object(forKey: "ofc_lat")as? String
                destinationLocationLongitude = user.object(forKey: "ofc_lng") as? String
            }
            else{
                if isNotNull(user.object(forKey: "home_lat")){
                    homeLocationLatitude = user.object(forKey: "home_lat")as? String
                    homeLocationLongitude = user.object(forKey: "home_lng") as? String
                }else{
                    if isNotNull(user.object(forKey: "par_ofc_lat")){
                        destinationLocationLatitude = user.object(forKey: "par_ofc_lat")as? String
                        destinationLocationLongitude = user.object(forKey: "par_ofc_lng") as? String
                    }else{
                        if isNotNull((user).object(forKey: "par_home_lat")){
                            homeLocationLatitude = (user).object(forKey: "par_home_lat") as? String
                            homeLocationLongitude = (user).object(forKey: "par_home_lng") as? String
                        }
                    }
                }
            }
        }
        if isNotNull(homeLocationLatitude){
            return CLLocationCoordinate2DMake(Double(homeLocationLatitude!)!, Double(homeLocationLongitude!)!)
        }else if isNotNull(destinationLocationLatitude){
            return CLLocationCoordinate2DMake(Double(destinationLocationLatitude!)!, Double(destinationLocationLongitude!)!)
        }
        return CLLocationCoordinate2D(latitude: 0,longitude: 0)
    }
    
    func updateStartAndEndLocation(){
        if trueForRideWaitingFalseforStarted() || (rideStatus != .confirmed && rideStatus != .confirmedButDelayed) {// in this case we have to show route between home to destination / destination to home
            if trueForHomeToDestinationFalseforDestinationToHome {
                startLocationCoordinate = homeLocationCoordinate()
                endLocationCoordinate = destinationLocationCoordinate()
            }else{
                startLocationCoordinate = destinationLocationCoordinate()
                endLocationCoordinate = homeLocationCoordinate()
            }
        } else {// in this case we have to show route between user current location and home / destination location
            if trueForHomeToDestinationFalseforDestinationToHome {
                startLocationCoordinate = currentLocation?.coordinate
                endLocationCoordinate = destinationLocationCoordinate()
            }else{
                startLocationCoordinate = currentLocation?.coordinate
                endLocationCoordinate = homeLocationCoordinate()
            }
            if startLocationCoordinate == nil {//handling a case when current location can be nil , in this case , map will show route from destination.home instead form current location
                if trueForHomeToDestinationFalseforDestinationToHome {
                    startLocationCoordinate = homeLocationCoordinate()
                }else{
                    startLocationCoordinate = destinationLocationCoordinate()
                }
            }
        }
    }
    
    
    func getUsersForRideTracking() -> NSMutableArray {
        let confirmedUsers = NSMutableArray()
        for user in usersForRide {
            if Acf().getTravelRequestStatus(user as! NSDictionary) == .travelRequestAccepted {
                confirmedUsers.add(user)
            }
        }
        return confirmedUsers
    }
    
    
    func monitorRideEventsIfNeeded(){
        setupRideRelatedChatGroupIfRequired()
        if isAtleast30MinutesLeftForRideStart() && (rideStatus == .confirmed || rideStatus == .confirmedButDelayed){
            if isNotNull(usersForRide) &&
                isNotNull(groupChatDialogue){
                RideEventManager.sharedInstance.startEventTrackingProcess(getUsersForRideTracking(), isOwner: self.trueForOwnerFalseforPassenger, isHomeToDestination: self.trueForHomeToDestinationFalseforDestinationToHome,dateOfRide: self.dateOfRide, groupChatDialogue: groupChatDialogue!)
            }
        }else{
            let ENABLE_STOPPING_THE_TRACKING_IF_STARTED = false
            if ENABLE_STOPPING_THE_TRACKING_IF_STARTED {
                RideEventManager.sharedInstance.stopTrackingRegions()
            }
        }
    }
    
    func updateUserLocationIfNeeded(){
        setupRideRelatedChatGroupIfRequired()
        if isAtleast30MinutesLeftForRideStart() && (rideStatus == .confirmed || rideStatus == .confirmedButDelayed){
            if isNotNull(usersForRide) &&
                isNotNull(groupChatDialogue){
                RideLocationUpdateManager.sharedInstance.startLocationUpdatingProcessIfRequired(getUsersForRideTracking(), isOwner: self.trueForOwnerFalseforPassenger, isHomeToDestination: self.trueForHomeToDestinationFalseforDestinationToHome,dateOfRide: self.dateOfRide,rideStatus: rideStatus,rideId: getRideId(),groupChatDialogue: groupChatDialogue!)
            }
        }
    }
    
    
    //MARK: - Chat
    
    func getRideId() -> String {
        return "\(selectedDayDate.day())\(selectedDayDate.month())\(selectedDayDate.year())\(trueForHomeToDestinationFalseforDestinationToHome == true ? "0" : "1")"
    }
    
    @IBAction func onClickOfRideChatButton(){
        if trueForOwnerFalseforPassenger {
            Acf().showRiderGroup()
        }else{
            Acf().showGroupChatScreen(Acf().ridersGroupNameCarOwner(getCarOwnerId(usersForRide)))
        }
    }
    
    func setupRideRelatedChatGroupIfRequired(){
        if isTodaysRide && (rideStatus == .confirmed || rideStatus == .confirmedButDelayed) {
            if isNull(groupChatDialogue) {
                if trueForOwnerFalseforPassenger {// if i am owner , then i will work with my riders group
                    self.groupChatDialogue = Acf().getRidersGroup()
                }else {// if i am passenger , then other user is owner only
                    self.groupChatDialogue = Acf().getRidersGroupCarOwner(getCarOwnerId(usersForRide))
                }
            }
        }
        decideAndSetRideChatButtonVisibility()
    }
    
    func decideAndSetRideChatButtonVisibility() {
        if rideStatus != .none {
            if (rideStatus == .confirmed || rideStatus == .confirmedButDelayed) {
                if trueForOwnerFalseforPassenger {
                    if Acf().isQBReadyForItsUserDependentServices(){
                        if let _ = Acf().getRidersGroup(){
                            rideChatButton.isHidden = false
                        }else{
                            rideChatButton.isHidden = true
                        }
                    }else{
                        rideChatButton.isHidden = true
                    }
                }else{
                    if Acf().isQBReadyForItsUserDependentServices(){
                        if let _ = Acf().getRidersGroupCarOwner(getCarOwnerId(usersForRide)) {
                            rideChatButton.isHidden = false
                        }else{
                            rideChatButton.isHidden = true
                        }
                    }
                    else{
                        rideChatButton.isHidden = true
                    }
                }
            }else{
                rideChatButton.isHidden = true
            }
        }else{
            rideChatButton.isHidden = true
        }
    }
    
    // MARK: - UICollection View Delegate & Datasource
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int{
        return usersForRide.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAtIndexPath indexPath: IndexPath) -> UICollectionViewCell{
        let cell: UsersCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "UsersCollectionViewCell", for: indexPath) as! UsersCollectionViewCell
        let userInfo = usersForRide.object(at: (indexPath as NSIndexPath).row) as! NSDictionary
        cell.userFullnameLabel?.text = userInfo.object(forKey: "user_name") as! String
        Acf().decideAndSetUserRideStatus(userInfo, statusLabel: cell.rideStatusLabel)
        cell.userProfileImageView?.sd_setImage(with: URL(string: getUserProfilePictureUrlFromFileName(userInfo.object(forKey: "user_photo") as! String)), placeholderImage: USER_PLACEHOLDER_IMAGE)
        return cell;
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize{
        return CGSize(width: collectionView.frame.size.height, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat{
        return 0.0;
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat{
        return 0.0;
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: IndexPath){
        let userInfo = usersForRide.object(at: (indexPath as NSIndexPath).row) as! NSDictionary
        UserMiniProfileView.showMiniProfileView(userInfo,rideDate: selectedDayDate, onView: self.view,parentVC: self ,autoHide: true,after:10,completionBlock:{[weak self] (returnedData) -> () in guard let `self` = self else { return }
            showUserProfile(returnedData as! NSDictionary ,navigationController: Acf().navigationController!)
            },removedBlock:{(returnedData) -> () in })
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func didDragMap(_ sender: UIGestureRecognizer) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NOTIFICATION_HIDE_FOR_MAP_VIEW), object: nil)
    }
    
    func hideThingsToViewMapInBetterWay(){
        UIView.animate(withDuration: HIDE_ANIMATION_DURATION, delay: 0, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.backToHomeButton.layer.opacity = OPACITY_WHEN_HIDDEN_FOR_MAP_VIEW
            self.rideTimeLineButton.layer.opacity = OPACITY_WHEN_HIDDEN_FOR_MAP_VIEW
            self.rideDetailButton.layer.opacity = OPACITY_WHEN_HIDDEN_FOR_MAP_VIEW
            self.rideChatButton.layer.opacity = OPACITY_WHEN_HIDDEN_FOR_MAP_VIEW
            Acf().carpoolHomeScreenController?.tabBar.layer.opacity = OPACITY_WHEN_HIDDEN_FOR_MAP_VIEW
        }) { (completed) in }
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(RideDetailViewController.restoreHiddenComponents), object: nil)
        self.perform(#selector(RideDetailViewController.restoreHiddenComponents), with: nil, afterDelay: RESTORE_HIDDEN_VIEW_TIME)
    }
    
    func restoreHiddenComponents(){
        UIView.animate(withDuration: SHOW_ANIMATION_DURATION, delay: 0, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.backToHomeButton.layer.opacity = 1.0
            self.rideTimeLineButton.layer.opacity = 1.0
            self.rideDetailButton.layer.opacity = 1.0
            self.rideChatButton.layer.opacity = 1.0
            Acf().carpoolHomeScreenController?.tabBar.layer.opacity = 1.0
        }) { (completed) in }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return safeInt(self.users?.count)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return UserMiniProfileTableViewCell.getRequiredHeight(self.users![(indexPath as NSIndexPath).row] as! NSDictionary)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView .dequeueReusableCell(withIdentifier: "UserMiniProfileTableViewCell") as? UserMiniProfileTableViewCell
        cell?.userInfo = self.users?[(indexPath as NSIndexPath).row] as? NSDictionary
        cell?.parentVC = self
        cell?.rideDate = selectedDayDate
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? UserMiniProfileTableViewCell {
            performAnimatedClickEffectType1(cell.fullNameLabel!)
            showUserProfile(self.users?[(indexPath as NSIndexPath).row] as! NSDictionary, navigationController: Acf().navigationController!)
        }
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        guard let indexPath = self.tableView!.indexPathForRow(at: location) else { return nil }
        if let cell = tableView!.cellForRow(at: indexPath) as? UserMiniProfileTableViewCell {
            let selectedCellFrame = tableView!.cellForRow(at: indexPath)!.frame
            let detailViewController = getViewController("ProfileOthersViewController") as! ProfileOthersViewController
            detailViewController.userInfo = self.users?[(indexPath as NSIndexPath).row] as? NSDictionary
            let gapX = 10.0 as CGFloat
            let gapY = 10.0 as CGFloat
            detailViewController.preferredContentSize = CGRect(x: gapX,y: gapY,width: DEVICE_WIDTH-2*gapX, height: DEVICE_HEIGHT-2*gapY).size
            previewingContext.sourceRect = selectedCellFrame
            let previewActions = NSMutableArray()
            if cell.bottomActionButton.isEnabled {
                if cell.isActionButtonActionable(){
                    if let action = cell.bottomActionButton.titleLabel?.text {
                        previewActions.add(UIPreviewAction(title: action , style: .default, handler: {[weak self] action, viewController in guard let `self` = self else { return }
                            if let cell = self.tableView!.cellForRow(at: indexPath) as? UserMiniProfileTableViewCell {
                                if cell.isActionButtonActionable(){
                                    cell.onClickOfActionButton()
                                }
                            }
                        }))
                    }
                }
            }
            if previewActions.count > 0 {
                detailViewController.preViewingActions = previewActions
            }
            return detailViewController
        }
        return nil
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        Acf().showViewControllerAsPopup(viewControllerToCommit)
    }
    
    
}
