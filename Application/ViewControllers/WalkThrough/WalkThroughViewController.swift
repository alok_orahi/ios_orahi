
//
//  WalkThroughViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

class WalkThroughViewController: UIViewController {
    
    //MARK: - variables and constants
    @IBOutlet fileprivate weak var walkthroughCollectionView: UICollectionView!
    @IBOutlet fileprivate weak var pageControl: UIPageControl!
    @IBOutlet fileprivate weak var skipButton: UIButton!
    
    fileprivate var contentsArray = [[String:String]]()
    
    //MARK: - view controller life cycle methods
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
    }
    
    //MARK: - other methods
    private func setupForNavigationBar(){
        self.navigationController?.isNavigationBarHidden = true;
    }
    
    private func registerForNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(WalkThroughViewController.updateUserInterfaceOnScreen), name: NSNotification.Name(rawValue: NOTIFICATION_APP_DOWNLOAD_REFFERAL_UPDATED), object: nil)
    }
    
    private func startUpInitialisations(){
        prepareContentsArray()
        setAppearanceForViewController(self)
        walkthroughCollectionView.reloadData()
        autoScrollWalkTrough()
        trackScreenLaunchEvent(SLE_WALKTHROUGH)
        self.skipButton.performAppearAnimationType1()
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        prepareContentsArray()
        pageControl.numberOfPages = contentsArray.count
        walkthroughCollectionView.reloadData()
    }
    
    func prepareContentsArray(){
        let userBoadingType = getUserOnBoardingType()
        if userBoadingType == .regular {
            contentsArray = [
                ["title": "Car owner gets Rs 3/km per passenger",
                 "desc": "Exchange money in online wallets",
                 "img": "CP0"],
                ["title": "Co-passengers pay Rs 3.5/km",
                 "desc": "Travel in comfort of a car",
                 "img": "CP1"],
                ["title": "Carpool with verified community",
                 "desc": "3 levels of security for a safe community",
                 "img": "CP2"],
                ["title": "Expand your network",
                 "desc": "Travel with professionals only",
                 "img": "CP3"],
                ["title": "Try peace of Mind",
                 "desc": "for your family and friends",
                 "img": "CP4"],
            ]
        }else if userBoadingType == .carpool {
            contentsArray = [
                ["title": "Car owner gets Rs 3/km per passenger",
                 "desc": "Exchange money in online wallets",
                 "img": "CP0"],
                ["title": "Co-passengers pay Rs 3.5/km",
                 "desc": "Travel in comfort of a car",
                 "img": "CP1"],
                ["title": "Carpool with verified community",
                 "desc": "3 levels of security for a safe community",
                 "img": "CP2"],
                ["title": "Expand your network",
                 "desc": "Travel with professionals only",
                 "img": "CP3"],
            ]
        }else if userBoadingType == .peaceOfMind {
            contentsArray = [
                ["title": "peace of mind",
                 "desc": "for your family and friends",
                 "img": "PM0"],
                ["title": "leaving office late?",
                 "desc": "notify your husband when you exit office",
                 "img": "PM1"],
                ["title": "about to reach home?",
                 "desc": "notify your wife when you enter your colony",
                 "img": "PM2"],
                ["title": "daughter goes to tution?",
                 "desc": "stay notified when she reaches safely",
                 "img": "PM3"],
                ["title": "grandpa goes for a walk?",
                 "desc": "stay notified when he reaches back home",
                 "img": "PM4"],
            ]
        }
    }
    
    // MARK: - UICollection View Delegate & Datasource
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int{
        return contentsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAtIndexPath indexPath: IndexPath) -> UICollectionViewCell{
        let cell: WalkThroughCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "WalkThroughCollectionViewCell", for: indexPath) as! WalkThroughCollectionViewCell;
        let object:[String:Any] = contentsArray[(indexPath as NSIndexPath).item] as [String : Any]
        cell.titleLabel.text = safeString(object["title"]).uppercased()
        cell.descriptionLabel.text = safeString(object["desc"]).uppercased()
        cell.titleLabel.performAppearAnimationType2(0.3)
        cell.descriptionLabel.performAppearAnimationType2(0.7)
        cell.walkThroughImage.image = UIImage(named:object["img"] as! String)
        return cell;
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize{
        return CGSize(width: DEVICE_WIDTH, height: collectionView.bounds.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat{
        return 0.0;
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat{
        return 0.0;
    }
    
    //MARK: -ScrollView Delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView){
        let pageWidth : CGFloat = walkthroughCollectionView.frame.size.width;
        let page: CGFloat = floor((walkthroughCollectionView.contentOffset.x - pageWidth/2)/pageWidth)+1;
        pageControl.currentPage = Int(page);
        counter = pageControl.currentPage
        if counter >= contentsArray.count - 1 {
            highlightSkipButton()
        }
        self.perform(#selector(WalkThroughViewController.autoScrollWalkTrough), with: nil, afterDelay: 4)
    }
    
    func highlightSkipButton(){
        skipButton.titleLabel?.font = UIFont(name: FONT_SEMI_BOLD, size: 12)
        skipButton.setTitle("GET STARTED", for: UIControlState.normal)
    }
    
    //MARK: - other functions
    var counter = -1
    func autoScrollWalkTrough(){
        if counter >= contentsArray.count - 1 {
            counter = -1
            highlightSkipButton()
            return
        }
        counter += 1
        let offsetRequired = CGPoint(x: DEVICE_WIDTH*CGFloat(self.counter), y: 0)
        UIView.animate(withDuration: 1.1, delay: 0, options: .curveEaseInOut, animations: {
            self.walkthroughCollectionView.contentOffset = offsetRequired
            self.view.layoutIfNeeded()
        }) { (completed) in }
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(WalkThroughViewController.autoScrollWalkTrough), object: nil)
        self.perform(#selector(WalkThroughViewController.autoScrollWalkTrough), with: nil, afterDelay: 9)
    }
    
    @IBAction func onClickOfSkipButton(){
        trackFunctionalEvent(FE_GET_STARTED, information: nil)
        Acf().pushVC("AuthenticationStep1ViewController", navigationController: self.navigationController, isRootViewController: false, animated: true) { (viewControllerObject) in
            (viewControllerObject as! AuthenticationStep1ViewController).workingMode = .signUp
            (viewControllerObject as! AuthenticationStep1ViewController).animationRequired = true
        }
    }
}
