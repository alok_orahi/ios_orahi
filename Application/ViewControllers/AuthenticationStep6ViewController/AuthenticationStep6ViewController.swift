//
//  AuthenticationStep6ViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class AuthenticationStep6ViewController: UIViewController {
    
    @IBOutlet fileprivate weak var infoLabel: UILabel!
    @IBOutlet fileprivate weak var homeAddressLabel: UILabel!
    @IBOutlet fileprivate weak var homeAddressPlaceholderLabel: UILabel!
    @IBOutlet fileprivate weak var homeAddressIconImageView: UIImageView!
    @IBOutlet fileprivate weak var timeLabel: UILabel!
    @IBOutlet fileprivate weak var amLabel: UILabel!
    @IBOutlet fileprivate weak var pmLabel: UILabel!
    @IBOutlet fileprivate weak var infoLabelTitle: UILabel!
    @IBOutlet fileprivate weak var infoLabelDescription: UILabel!
    
    @IBOutlet fileprivate weak var actionButton: TKTransitionSubmitButton!
    
    fileprivate var homeTimingDate = Date().change( nil, month: nil, day: nil, hour: 9, minute: 0, second: 0)
    fileprivate var homeLocationCoordinate: CLLocationCoordinate2D?
    fileprivate var homeLocationAddress: String?
    fileprivate var locationPicker : ALPlacesViewController?
    
    //MARK: - view controller life cycle methods
    
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
        updateContentsOnScreen()
    }
    
    //MARK: - other methods
    
    private func setupForNavigationBar(){
        setAppearanceForNavigationBarType2(self.navigationController)
        if self.isBackButtonRequired() {
            addNavigationBarButton(self, image: UIImage(named: "backarrowblack"), title: nil, isLeft: true,forceWhiteTint:false)
        }else{
            self.navigationItem.leftBarButtonItem = nil
            self.navigationItem.setHidesBackButton(true, animated:false)
        }
    }
    
    private func registerForNotifications(){
        
    }
    
    private func startUpInitialisations(){
        setAppearanceForViewController(self)
        self.actionButton.performAppearAnimationType1()
        let userInfo = Dbm().getUserInfo()
        let trueForOwnerFalseforPassenger = userInfo!.isPassenger!.isEqual("0")
        if trueForOwnerFalseforPassenger{
            infoLabel.attributedText = getAttributedTitleStringType3("GET ₹ 3/KM PER PASSENGER\nYOU ARE JUST A STEP AWAY", regularText: "GET ₹ 3/KM PER PASSENGER!", anotherText:"YOU ARE JUST A STEP AWAY")
        }else{
            infoLabel.attributedText = getAttributedTitleStringType3("FIRST 2 FREE RIDES\nYOU ARE JUST A STEP AWAY", regularText: "FIRST 2 FREE RIDES", anotherText:"YOU ARE JUST A STEP AWAY")
        }
        homeAddressPlaceholderLabel.text = "Enter home address"
        homeAddressIconImageView.image = UIImage(named: "homeAddress")
        addShadow(layer: timeLabel.layer,cornerRadius:timeLabel.bounds.size.height/2.0)
        updateContentsOnScreen()
    }
    
    func addShadow(layer:CALayer,cornerRadius:CGFloat){
        layer.cornerRadius = cornerRadius
        layer.shadowColor = APP_THEME_LIGHT_GRAY_COLOR.cgColor
        layer.shadowOffset = CGSize.zero
        layer.shadowOpacity = 0.3
        layer.shadowRadius = 5.0
        layer.borderWidth = 0.5
        layer.borderColor = UIColor.white.cgColor
        layer.backgroundColor = UIColor.white.cgColor
    }
    
    @objc private func updateUserInterfaceOnScreen(){
    }
    
    func onClickOfLeftBarButton(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    func canContinueForProfileUpdate()->(Bool){
        var canContinue = true
        if canContinue {
            canContinue = validateIfNull(homeLocationCoordinate , identifier: "Home address")
        }
        return canContinue
    }
    
    @IBAction func onClickOfHomeAddressButton(){
        resignKeyboard()
        Acf().setupIQKeyboardManagerDisabled()
        func onLocationPicked(_ name: String?, address: String?, coordinate: CLLocationCoordinate2D?, error: NSError?) {
            if error != nil {
                Acf().setupIQKeyboardManagerEnabled()
                return
            }
            Acf().navigationController!.dismiss(animated: true, completion: nil)
            Acf().setupIQKeyboardManagerEnabled()
            if isNotNull(address) && coordinate != nil {
                self.homeLocationAddress = address
                self.homeLocationCoordinate = coordinate
                self.updateContentsOnScreen()
                trackFunctionalEvent(FE_TIME_AND_ADDRESS_PICKER_HOME_SUGGESTION_SELECTED, information: nil)
            }
        }
        
        self.locationPicker = ALPlacesViewController(APIKey: GOOGLE_API_KEY, completion: onLocationPicked)
        self.locationPicker?.placeholderText = "Enter home address"
        Acf().navigationController?.present(self.locationPicker!, animated: true, completion: nil)
    }
    
    @IBAction func onClickOfActionButton(){
        if(isInternetConnectivityAvailable(true)==false){return}
        if canContinueForProfileUpdate(){
            updateInformationOnDatabase()
            decideAndShowNextScreen(self.navigationController!)
        }
    }
    
    @IBAction func onClickOfUpdateTimeHoursButton(){
        showDateTimePicker(homeTimingDate!, mode: .time) { (selectedDate) in
            self.homeTimingDate = selectedDate
            self.updateContentsOnScreen()
        }
    }
    
    @IBAction func onClickOfUpdateTimeAMButton(){
        if self.homeTimingDate!.hour() > 12 {
            self.homeTimingDate = self.homeTimingDate?.change(hour: self.homeTimingDate!.hour()-12)
        }
        self.updateContentsOnScreen()
    }
    
    @IBAction func onClickOfUpdateTimePMButton(){
        if self.homeTimingDate!.hour() < 12 {
            self.homeTimingDate = self.homeTimingDate?.change(hour: self.homeTimingDate!.hour()+12)
        }
        self.updateContentsOnScreen()
    }
    
    func updateContentsOnScreen(){
        let customisedDarkGrayColor = UIColor(colorLiteralRed: 77/255, green: 77/255, blue: 77/255, alpha: 1.0)
        
        if self.homeTimingDate!.hour() >= 12 {
            pmLabel.makeMeRoundWith(borderColor: customisedDarkGrayColor, width: 0.0)
            pmLabel.backgroundColor = APP_THEME_VOILET_COLOR
            pmLabel.textColor = UIColor.white
            
            amLabel.makeMeRoundWith(borderColor: customisedDarkGrayColor, width: 1.0)
            amLabel.backgroundColor = UIColor.white
            amLabel.textColor = customisedDarkGrayColor
        }else{
            amLabel.makeMeRoundWith(borderColor: customisedDarkGrayColor, width: 0.0)
            amLabel.backgroundColor = APP_THEME_VOILET_COLOR
            amLabel.textColor = UIColor.white
            
            pmLabel.makeMeRoundWith(borderColor: customisedDarkGrayColor, width: 1.0)
            pmLabel.backgroundColor = UIColor.white
            pmLabel.textColor = customisedDarkGrayColor
        }
        
        timeLabel.text = self.homeTimingDate!.stringTimeComponentOnly()
        
        infoLabelTitle.text = "Home departure time:"
        infoLabelDescription.text = "\(self.homeTimingDate!.stringTimeOnly_AM_PM_FormatValue())"
        
        if isNotNull(homeLocationAddress){
            homeAddressLabel.text = homeLocationAddress
        }
        homeAddressPlaceholderLabel.isHidden = safeString(homeAddressLabel.text).length > 0
    }
    
    func updateInformationOnDatabase(){
        let settings = Dbm().getSetting()
        if isNotNull(homeLocationAddress){
            settings.homeAddress = homeLocationAddress
        }
        if (homeLocationCoordinate!.latitude != nil && homeLocationCoordinate!.latitude != 0){
            settings.homeLocationLatitude = NSNumber(value: homeLocationCoordinate!.latitude as Double)
        }
        if (homeLocationCoordinate?.longitude != nil && homeLocationCoordinate!.longitude != 0){
            settings.homeLocationLongitude = NSNumber(value: homeLocationCoordinate!.longitude as Double)
        }
        if isNotNull(homeTimingDate){
            settings.homeTime = homeTimingDate?.stringValue()
        }
        settings.isProfileUpdated = NSNumber(value: true)
        Dbm().saveChanges()
    }
}
