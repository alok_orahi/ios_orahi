//
//  PeaceOfMindHomeViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class PeaceOfMindHomeViewController: UIViewController,GroupsTableViewCellDelegate,UITableViewDelegate,UITableViewDataSource {
    
    //MARK: - variables and constants
    @IBOutlet fileprivate var syncButton : UIButton!
    @IBOutlet fileprivate var notificationButton : UIButton!
    @IBOutlet fileprivate weak var tableView : UITableView!
    @IBOutlet fileprivate weak var helpView : HelpBottomBannerView!

    private var groups = [Group]()
    
    //MARK: - view controller life cycle methods
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
        updateUserInterfaceOnScreen()
        Acf().fetchDialogs()
        Lem().doRequiredProcessing()
        updateNotificationBadge(notificationButton: self.notificationButton,ax:-7,ay:2)
    }
    
    //MARK: - other methods
    
    private func setupForNavigationBar(){
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    @IBAction func onClickOfLeftBarButton(_ sender:Any){
        Acf().sideMenuController?.presentLeftMenuViewController()
    }
    
    @IBAction func onClickOfSyncButton(_ sender:Any){
        animateSyncButton()
        if(isInternetConnectivityAvailable(true)==false){return}
        Acf().syncGroupsAndPlacesFromServer()
    }
    
    @IBAction func onClickOfNotificationsButton(_ sender:Any){
        Acf().showNotificationScreen(nc: self.navigationController)
    }
    
    func animateSyncButton(){
        self.syncButton!.transform = CGAffineTransform.identity
        self.syncButton!.rotate(toAngle: CGFloat(M_PI * 2.0), duration: 2.0 , direction: .right, repeatCount: 1 , autoreverse: false)
    }
    

    private func registerForNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(PeaceOfMindHomeViewController.updateUserInterfaceOnScreen), name: NSNotification.Name(rawValue: NOTIFICATION_GROUPS_PLACES_UPDATED), object: nil)
    }
    
    private func startUpInitialisations(){
        registerNib("SingleLabelTableViewCellType2", tableView: tableView)
        registerNib("CreateNewGroupTableViewCell", tableView: tableView)
        registerNib("GroupsTableViewCell", tableView: tableView)
        setAppearanceForViewController(self)
        setAppearanceForTableView(tableView)
        helpView.navigationController = self.navigationController
        tableView.reloadData()
        Acf().syncGroupsAndPlacesFromServer()
        animateSyncButton()
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        groups = Dbm().getAllGroups()
        tableView.reloadData()
    }
    
    //MARK: - UITableView Delegate & Data Source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count = 2
        if groups.count > 0 {
            if groups.count%2 == 0 {
                count = 2 + Int(groups.count/2)
            }else{
                count = 2 + Int(groups.count/2) + 1
            }
        }else{
            count = 3
        }
        return count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        if indexPath.row == 0 {
            return 44
        }else if indexPath.row == 1 {
            return 194
        }else{
            return (DEVICE_WIDTH - 94)/2 + 45
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SingleLabelTableViewCellType2") as! SingleLabelTableViewCellType2
            cell.titleLabel?.text = "MY GROUPS"
            return cell
        }else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CreateNewGroupTableViewCell") as! CreateNewGroupTableViewCell
            cell.navigationController = self.navigationController
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "GroupsTableViewCell") as! GroupsTableViewCell
            let index = indexPath.row-2
            let leftGroupIndex = 2*index
            let rightGroupIndex = 2*index+1
            if groups.count > leftGroupIndex {
                cell.setup(hide: false, isLeft: true)
                let group1Data = groups[leftGroupIndex]
                cell.groupName1Label.text = safeString(group1Data.name).uppercased()
                cell.groupPicture1ImageView.sd_setImage(with: getGroupPictureUrlFromFileName(safeString(group1Data.picture)).asNSURL(), placeholderImage: POM_GROUP_PLACEHOLDER_IMAGE)
                cell.leftGroup = group1Data
                cell.showingDummy = false
            }else{
                if groups.count > 0 {
                    cell.setup(hide: true, isLeft: true)
                    cell.groupName1Label.text = ""
                    cell.groupPicture1ImageView.image = nil
                    cell.leftGroup = nil
                    cell.showingDummy = false
                }else{
                    cell.setup(hide: false, isLeft: true)
                    cell.groupName1Label.text = DUMMY_GROUP_NAME
                    cell.groupPicture1ImageView.image = UIImage(named:DUMMY_GROUP_IMAGE_NAME)
                    cell.leftGroup = nil
                    cell.showingDummy = true
                }
            }
            if groups.count > rightGroupIndex {
                cell.setup(hide: false, isLeft: false)
                let group2Data = groups[rightGroupIndex]
                cell.groupName2Label.text = safeString(group2Data.name).uppercased()
                cell.groupPicture2ImageView.sd_setImage(with: getGroupPictureUrlFromFileName(safeString(group2Data.picture)).asNSURL(), placeholderImage: POM_GROUP_PLACEHOLDER_IMAGE)
                cell.rightGroup = group2Data
            }else{
                cell.setup(hide: true, isLeft: false)
                cell.groupName2Label.text = ""
                cell.groupPicture2ImageView.image = nil
                cell.rightGroup = nil
            }
            cell.delegate = self
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func userClickedOnGroup(cell:GroupsTableViewCell,group:Group?){
        if cell.showingDummy {
            Acf().pushVC("AddEditGroupViewController", navigationController: self.navigationController, isRootViewController: false, animated: true, modifyObject: { (vc) in
                execMain({
                    let aeg = (vc as! AddEditGroupViewController)
                    aeg.groupNameTextField.text = DUMMY_GROUP_NAME
                    aeg.groupPictureImageView.image = UIImage(named:DUMMY_GROUP_IMAGE_NAME)
                    aeg.groupPictureInformationLabel.isHidden = true
                    aeg.pictureAddedOrUpdated = true
                    execMain({
                        aeg.groupNameTextField.becomeFirstResponder()
                    },delay: 0.6)
                },delay: 0.2)
            })
        }else{
            showGroupDetailScreen(group: group!, navigationController: self.navigationController!)
        }
    }
    
}
