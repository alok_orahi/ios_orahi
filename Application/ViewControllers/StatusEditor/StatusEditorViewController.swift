//
//  StatusEditorViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit

class StatusEditorViewController: UIViewController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource {
    
    //MARK: - variables and constants
    @IBOutlet fileprivate weak var statusTextfield: MKTextField!
    @IBOutlet fileprivate weak var tableView: UITableView!
    @IBOutlet fileprivate weak var doneButton: TKTransitionSubmitButton!
    fileprivate var statusOptions = NSMutableArray()
    
    
    //MARK: - view controller life cycle methods
    
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
        Acf().setupIQKeyboardManagerEnabled()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        resignKeyboard()
    }
    
    //MARK: - other methods
    private func setupForNavigationBar(){
        self.navigationController?.isNavigationBarHidden = true
    }
    
    private func registerForNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(StatusEditorViewController.updateAppearanceOfTextField), name: NSNotification.Name.UITextViewTextDidBeginEditing, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(StatusEditorViewController.updateAppearanceOfTextField), name: NSNotification.Name.UITextViewTextDidEndEditing, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(StatusEditorViewController.updateAppearanceOfTextField), name: NSNotification.Name.UITextFieldTextDidBeginEditing, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(StatusEditorViewController.updateAppearanceOfTextField), name: NSNotification.Name.UITextFieldTextDidEndEditing, object: nil)
    }
    
    private func startUpInitialisations(){
        registerNib("SingleLabelTableViewCell", tableView: tableView)
        setAppearanceForViewController(self)
        statusTextfield.placeholder = "Your orahi status 😜"
        statusTextfield.text = getStatusText()
        statusTextfield.delegate = self
        if statusTextfield.text?.length > 0 {
            Dbm().addStatus(["status":statusTextfield.text!])
        }
        setAppearanceForMKTextField(statusTextfield)
        setAppearanceForTableView(self.tableView)
        updateAppearanceOfTextField()
        
        doneButton.layer.cornerRadius = doneButton.bounds.size.width/2
        doneButton.layer.masksToBounds = true
        doneButton.requiredBackgroundColor = UIColor(red: 100/255, green: 196/255, blue: 219/255, alpha: 1.0)
        
        DispatchQueue.main.async { () -> Void in
            self.statusTextfield.becomeFirstResponder()
        }
        
        let footerView = UIView()
        footerView.backgroundColor = UIColor.clear
        footerView.frame = CGRect(x: 0, y: 0, width: DEVICE_WIDTH, height: 110)
        tableView.tableFooterView = footerView
        trackScreenLaunchEvent(SLE_STATUS_EDITOR)
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        statusOptions = Dbm().getAllStatus()
        tableView.reloadData()
    }
    
    //MARK: - other functions
    
    func canContinueForStatusUpdate()->(Bool){
        var canContinue = true
        if canContinue {
            canContinue = validateIfNull(statusTextfield.text , identifier: "status")
        }
        return canContinue
    }
    
    //MARK: - UITableView Delegate & Data Source

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return statusOptions.count
    }
    
    @objc func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 44
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SingleLabelTableViewCell") as? SingleLabelTableViewCell
        cell?.accessoryType = UITableViewCellAccessoryType.none
        cell?.titleLabel?.text = (statusOptions.object(at: (indexPath as NSIndexPath).row) as! Status).status
        if cell?.titleLabel?.text == statusTextfield.text {
            cell?.accessoryType = .checkmark
        }else{
            cell?.accessoryType = .none
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as? SingleLabelTableViewCell
        performAnimatedClickEffectType1(cell!.titleLabel!)
        statusTextfield.text = (statusOptions.object(at: (indexPath as NSIndexPath).row) as! Status).status
        updateAppearanceOfDoneButton()
        tableView.reloadData()
    }
    
    
    @IBAction func onClickOfDoneButton(){
        resignKeyboard()
        if(isInternetConnectivityAvailable(true)==false){return}
        if canContinueForStatusUpdate(){
            Dbm().addStatus(["status":statusTextfield.text!])
            Dbm().addUserStatus(["status":statusTextfield.text!,"userId":loggedInUserId()])
            Dbm().saveChanges()
            let information = NSMutableDictionary()
            copyData(statusTextfield.text! , destinationDictionary: information, destinationKey: "status", methodName: #function)
            let scm = ServerCommunicationManager()
            scm.responseErrorOption = ResponseErrorOption.dontShowErrorResponseMessage
            scm.returnFailureResponseAlso = true
            scm.setStatus(information, completionBlock: {[weak self] (responseData) -> () in guard let `self` = self else { return }
            })
            NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_UPDATE_PROFILE_SCREEN), object: nil)
            Acf().navigationController!.dismissPopupViewController(SLpopupViewAnimationType.fade)
        }
    }
    
    func updateAppearanceOfTextField(){
        Acf().updateAppearanceOfTextFieldType1(statusTextfield)
        updateAppearanceOfDoneButton()
    }
    
    func updateAppearanceOfDoneButton(){
        if statusTextfield.text?.length > 0{
            doneButton.isEnabled = true
        }else{
            doneButton.isEnabled = false
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
}
