//
//  AuthenticationStep3ViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit

class AuthenticationStep3ViewController: UIViewController,UITextFieldDelegate {
    
    //MARK: - variables and constants
    @IBOutlet fileprivate weak var infoLabel: UILabel!
    @IBOutlet fileprivate weak var fullNameTextfield: MKTextField!
    @IBOutlet fileprivate weak var emailIdTextfield: MKTextField!
    @IBOutlet fileprivate weak var thirdLeftOptionButton: UIButton!
    @IBOutlet fileprivate weak var thirdRightOptionButton: UIButton!
    @IBOutlet fileprivate weak var actionButton: TKTransitionSubmitButton!
    
    fileprivate var isGenderSelected = false
    
    //MARK: - view controller life cycle methods
    
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NSObject.cancelPreviousPerformRequests(withTarget: self)
    }
    
    //MARK: - other methods
    
    private func setupForNavigationBar(){
        setAppearanceForNavigationBarType2(self.navigationController)
        if self.isBackButtonRequired() {
            addNavigationBarButton(self, image: UIImage(named: "backarrowblack"), title: nil, isLeft: true,forceWhiteTint:false)
        }else{
            self.navigationItem.leftBarButtonItem = nil
            self.navigationItem.setHidesBackButton(true, animated:false)
        }
    }
    
    private func registerForNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(AuthenticationStep3ViewController.updateAppearanceOfTextField), name: NSNotification.Name.UITextViewTextDidBeginEditing, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AuthenticationStep3ViewController.updateAppearanceOfTextField), name: NSNotification.Name.UITextViewTextDidEndEditing, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AuthenticationStep3ViewController.updateAppearanceOfTextField), name: NSNotification.Name.UITextFieldTextDidBeginEditing, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AuthenticationStep3ViewController.updateAppearanceOfTextField), name: NSNotification.Name.UITextFieldTextDidEndEditing, object: nil)
    }
    
    private func startUpInitialisations(){
        setAppearanceForViewController(self)
        setAppearanceForMKTextField(fullNameTextfield)
        setAppearanceForMKTextField(emailIdTextfield)
        updateAppearanceOfTextField()
        self.actionButton.performAppearAnimationType1()
        infoLabel.attributedText = getAttributedTitleStringType3("OTP VERIFIED SUCCESSFULLY!\nPlease provide your personal details", regularText: "OTP VERIFIED SUCCESSFULLY!", anotherText: "Please provide your personal details")
        trackScreenLaunchEvent(SLE_BASIC_PROFILE_DETAILS)
        if getUserOnBoardingType() == .carpool {
            trackKochavaEvent(KE_CARPOOL_OTP_VALIDATION_COMPLETED)
            CacheManager.sharedInstance.saveObject("true", identifier: KEY_CARPOOL_SIGNUP_COMPLETED)
        }else if getUserOnBoardingType() == .peaceOfMind {
            trackKochavaEvent(KE_POM_OTP_VALIDATION_COMPLETED)
            CacheManager.sharedInstance.saveObject("true", identifier: KEY_POM_SIGNUP_COMPLETED)
        }
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        let userInfo = Dbm().getUserInfo()
        fullNameTextfield.text = safeString(userInfo?.name)
        emailIdTextfield.text = safeString(userInfo?.email)
        if isGenderSelected {
            if safeBool(userInfo?.genderTforMaleFForFemale){
                thirdLeftOptionButton.isSelected = true
                thirdRightOptionButton.isSelected = false
            }else{
                thirdLeftOptionButton.isSelected = false
                thirdRightOptionButton.isSelected = true
            }
        }
    }
    
    func onClickOfLeftBarButton(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    func canContinueForProfileUpdate()->(Bool){
        var canContinue = true
        if canContinue {
            canContinue = validateName(fullNameTextfield.text, identifier: "Full name")
            if canContinue == false {
                fullNameTextfield.becomeFirstResponder()
            }
        }
        if canContinue {
            canContinue = validateEmail(emailIdTextfield.text, identifier: "Email")
            if canContinue == false {
                emailIdTextfield.becomeFirstResponder()
            }
        }
        if canContinue {
            canContinue = isGenderSelected
            if canContinue == false {
                showToast("Please select Gender")
                thirdLeftOptionButton.shakeHorizontally()
                thirdRightOptionButton.shakeHorizontally()
            }
        }
        return canContinue
    }
    
    @IBAction func onClickOfActionButton(){
        if(isInternetConnectivityAvailable(true)==false){return}
        if canContinueForProfileUpdate(){
            actionButton.startLoadingAnimation()
            let information = NSMutableDictionary()
            copyData(fullNameTextfield.text , destinationDictionary: information, destinationKey: "name", methodName: #function)
            copyData(emailIdTextfield.text , destinationDictionary: information, destinationKey: "email", methodName: #function)
            copyData(thirdLeftOptionButton.isSelected ? "male" : "female" , destinationDictionary: information, destinationKey: "gender", methodName: #function)
            let scm = ServerCommunicationManager()
            scm.updateUserProfile(information) { (responseData) -> () in
                if let _ = responseData {
                    updateUserProfile(loggedInUserId(), completion: { (returnedData) in
                        if safeBool(returnedData) {
                            trackFunctionalEvent(FE_PERSONAL_DETAILS_SAVED, information: information)
                            decideAndShowNextScreen(self.navigationController!)
                        }else{
                            trackFunctionalEvent(FE_PERSONAL_DETAILS_SAVED, information: information,isSuccess: false)
                        }
                        self.actionButton.stopIt()
                    })
                }else{
                    self.actionButton.stopIt()
                }
            }
        }
    }
    
    @IBAction func onClickOfThirdOptionLeftButton(){
        performAnimatedClickEffectType1(thirdLeftOptionButton)
        thirdLeftOptionButton.isSelected = true
        thirdRightOptionButton.isSelected = false
        isGenderSelected = true
    }
    
    @IBAction func onClickOfThirdOptionRightButton(){
        performAnimatedClickEffectType1(thirdRightOptionButton)
        thirdLeftOptionButton.isSelected = false
        thirdRightOptionButton.isSelected = true
        isGenderSelected = true
    }
    
    func updateAppearanceOfTextField(){
        Acf().updateAppearanceOfTextFieldType1(fullNameTextfield)
        Acf().updateAppearanceOfTextFieldType1(emailIdTextfield)
    }
}
