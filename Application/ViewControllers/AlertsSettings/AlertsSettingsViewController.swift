//
//  AlertsSettingsViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit

class AlertsSettingsViewController : UIViewController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource {
    
    //MARK: - variables and constants
    @IBOutlet fileprivate weak var tableView : UITableView?
    


//MARK: - view controller life cycle methods
    
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
    }
    
    //MARK: - other methods
    private func setupForNavigationBar(){
        setAppearanceForNavigationBarType1(self.navigationController)
        setupNavigationBarTitleType1("Alerts",viewController: self)
        if self.isBackButtonRequired() {
            addNavigationBarButton(self, image: UIImage(named: "backarrowblack"), title: nil, isLeft: true)
        }
    }
    
    func onClickOfLeftBarButton(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    private func registerForNotifications(){
        
    }
    
    private func startUpInitialisations(){
        registerNib("SingleLabelAndSwitchTableViewCell", tableView: tableView)
        setAppearanceForViewController(self)
        setAppearanceForTableView(tableView)
        setAppearanceForViewController(self)
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        
    }
    
    //MARK: - UITableView Delegate & Data Source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    @objc func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 88
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SingleLabelAndSwitchTableViewCell") as? SingleLabelAndSwitchTableViewCell
        var title = ""
        var description = ""
        
        if (indexPath as NSIndexPath).row == 0{
            title = "Sounds"
            description = "plays sound when receive any in app notification."
        }else if (indexPath as NSIndexPath).row == 1{
            title = "Vibrations"
            description = "vibrate when receive any in app notification."
        }
        
        let attributesTitle = [NSFontAttributeName:UIFont.init(name: FONT_BOLD, size: 15)!,
            NSForegroundColorAttributeName:UIColor.darkGray] as NSDictionary
        let attributesDescription = [NSFontAttributeName:UIFont.init(name: FONT_REGULAR, size: 13)!,
            NSForegroundColorAttributeName:UIColor.gray] as NSDictionary
        
        let attributedString = NSMutableAttributedString(string:title+"\n"+description)
        attributedString.setAttributes(attributesTitle as? [String : Any], range: NSMakeRange(0, title.length))
        attributedString.setAttributes(attributesDescription as? [String : Any], range: NSMakeRange(title.length, attributedString.length-title.length))
        cell?.titleLabel?.attributedText = attributedString
        
        let setting = Dbm().getSetting()
        if (indexPath as NSIndexPath).row == 0{
            cell?.controlSwitch?.isOn = (setting.notificationSounds?.boolValue)!
        }else if (indexPath as NSIndexPath).row == 1{
            cell?.controlSwitch?.isOn = (setting.notificationVibrations?.boolValue)!
        }
        
        cell?.controlSwitch?.addTarget(self, action: #selector(AlertsSettingsViewController.onValueChangeOfSwitch(_:)), for: UIControlEvents.valueChanged)
        cell?.controlSwitch?.tag = (indexPath as NSIndexPath).row
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as? SingleLabelAndSwitchTableViewCell
        performAnimatedClickEffectType1(cell!.titleLabel!)
        cell?.controlSwitch?.setOn((cell?.controlSwitch?.isOn == false), animated: true)
        onValueChangeOfSwitch((cell?.controlSwitch)!)
    }
    
    func onValueChangeOfSwitch(_ sender:UISwitch){
        let setting = Dbm().getSetting()
        if sender.tag == 0{
            setting.notificationSounds = NSNumber(value: sender.isOn as Bool)
        }else if sender.tag == 1{
            setting.notificationVibrations = NSNumber(value: sender.isOn as Bool)
        }
        Dbm().saveChangesLazily()
    }
    
    //MARK: - other functions
    
}
