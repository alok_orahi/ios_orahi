//
//  SettingsViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class SettingsViewController: UIViewController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource {
    
    //MARK: - variables and constants
    @IBOutlet fileprivate weak var tableView : UITableView?
    
    
    //MARK: - view controller life cycle methods
    
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
    }
    
    //MARK: - other methods
    private func setupForNavigationBar(){
        setAppearanceForNavigationBarType1(self.navigationController)
        setupNavigationBarTitleType1("Settings",viewController: self)
        if self.isBackButtonRequired() {
            addNavigationBarButton(self, image: UIImage(named: "backarrowblack"), title: nil, isLeft: true)
        }
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    func onClickOfLeftBarButton(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    private func registerForNotifications(){
        
    }
    
    private func startUpInitialisations(){
        registerNib("SingleLabelTableViewCell", tableView: tableView)
        setAppearanceForViewController(self)
        setAppearanceForTableView(tableView)
        trackScreenLaunchEvent(SLE_SETTINGS)
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        
    }
    
    //MARK: - UITableView Delegate & Data Source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SingleLabelTableViewCell") as? SingleLabelTableViewCell
        cell?.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        if (indexPath as NSIndexPath).row == 0{
            cell?.titleLabel?.text = "Verification"
        }else if (indexPath as NSIndexPath).row == 1{
            cell?.titleLabel?.text = "Notifications"
        }else if (indexPath as NSIndexPath).row == 2{
            cell?.titleLabel?.text = "Do not Disturb"
        }else if (indexPath as NSIndexPath).row == 3{
            cell?.titleLabel?.text = "Travel Preferences"
        }else if (indexPath as NSIndexPath).row == 4{
            cell?.titleLabel?.text = "Alerts"
        }else if (indexPath as NSIndexPath).row == 5{
            cell?.titleLabel?.text = "Blocked Users"
        }else if (indexPath as NSIndexPath).row == 6{
            cell?.titleLabel?.text = "Favourites"
        }else if (indexPath as NSIndexPath).row == 7{
            cell?.titleLabel?.text = "Search"
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as? SingleLabelTableViewCell
        performAnimatedClickEffectType1(cell!.titleLabel!)
        if isSystemReadyToProcessThis() {
            if (indexPath as NSIndexPath).row == 0{
                Acf().pushVC("VerificationOptionsViewController", navigationController: self.navigationController, isRootViewController: false, animated: true) { (viewControllerObject) -> () in
                }
            }else if (indexPath as NSIndexPath).row == 1{
                Acf().pushVC("NotificationSettingsViewController", navigationController: self.navigationController, isRootViewController: false, animated: true) { (viewControllerObject) -> () in
                }
            }
            else if (indexPath as NSIndexPath).row == 2{
                Acf().pushVC("DoNotDisturbSettingsViewController", navigationController: self.navigationController, isRootViewController: false, animated: true) { (viewControllerObject) -> () in
                }
            }else if (indexPath as NSIndexPath).row == 3{
                Acf().pushVC("TravelWithSettingsViewController", navigationController: self.navigationController, isRootViewController: false, animated: true) { (viewControllerObject) -> () in
                }
            }else if (indexPath as NSIndexPath).row == 4{
                Acf().pushVC("AlertsSettingsViewController", navigationController: self.navigationController, isRootViewController: false, animated: true) { (viewControllerObject) -> () in
                }
            }else if (indexPath as NSIndexPath).row == 5{
                Acf().pushVC("BlockedUsersViewController", navigationController: self.navigationController, isRootViewController: false, animated: true) { (viewControllerObject) -> () in
                }
            }else if (indexPath as NSIndexPath).row == 6{
                Acf().pushVC("FavouriteUsersViewController", navigationController: self.navigationController, isRootViewController: false, animated: true) { (viewControllerObject) -> () in
                }
            }else if (indexPath as NSIndexPath).row == 7{
                Acf().pushVC("SearchOrahiUsersViewController", navigationController: self.navigationController, isRootViewController: false, animated: true) { (viewControllerObject) -> () in
                }
            }
        }
    }
    
    //MARK: - other functions
    
}
