//
//  OfferDetailsType1ViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit
import CTFeedback

class OfferDetailsType1ViewController: UIViewController,UITextFieldDelegate {
    
    //MARK: - variables and constants
    var userInfo:NSDictionary!
    @IBOutlet fileprivate weak var containerView : UIView!
    @IBOutlet fileprivate weak var topBannerImageView : UIImageView!
    @IBOutlet fileprivate weak var discountedCostLabel : UILabel!
    @IBOutlet fileprivate weak var privateTaxiCostLabel : UILabel!
    @IBOutlet fileprivate weak var privateTaxiRateLabel : UILabel!
    @IBOutlet fileprivate weak var taxiSharingCostLabel : UILabel!
    @IBOutlet fileprivate weak var taxiSharingRateLabel : UILabel!
    @IBOutlet fileprivate weak var activityIndicatorView : UIActivityIndicatorView!

    //MARK: - view controller life cycle methods
    
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
    }
    
    //MARK: - other methods
    private func setupForNavigationBar(){
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    private func registerForNotifications(){
        
    }
    
    private func startUpInitialisations(){
        func addShadow(layer:CALayer){
            layer.cornerRadius = OFFER_VIEW_CORNER_RADIUS
            layer.shadowColor = UIColor.darkGray.cgColor
            layer.shadowOffset = CGSize.zero
            layer.shadowOpacity = 0.6
            layer.shadowRadius = 5.0
            layer.borderWidth = 0.5
            layer.borderColor = UIColor.white.cgColor
            layer.backgroundColor = UIColor.white.cgColor
        }
        addShadow(layer: containerView.layer)
        setAppearanceForViewController(self)
        execMain ({ (completed) in
            self.topBannerImageView.roundCorners([.topLeft,.topRight], radius: OFFER_VIEW_CORNER_RADIUS)
            addShadow(layer: self.containerView.layer)
        },delay:0)
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        func updateDetails(response:NSDictionary?){
            if isNotNull(response) {
                discountedCostLabel.text = "\(safeString(response!["final_ride_cost"]))"
                privateTaxiCostLabel.text = "₹\(safeString(response!["ride_cost_private"]))"
                taxiSharingCostLabel.text = "₹\(safeString(response!["ride_cost_sharing"]))"
                privateTaxiRateLabel.text = "Ride charge: ₹\(safeString(response!["ride_rate_private"]))/km"
                taxiSharingRateLabel.text = "Ride charge: ₹\(safeString(response!["ride_rate_sharing"]))/km"
            }else{
                discountedCostLabel.text = "--"
                privateTaxiCostLabel.text = "₹--"
                taxiSharingCostLabel.text = "₹--"
                privateTaxiRateLabel.text = "Ride charge: ₹--/km"
                taxiSharingRateLabel.text = "Ride charge: ₹--/km"
            }
            privateTaxiCostLabel.setAttributedTextWithOptimisationsType2()
            taxiSharingCostLabel.setAttributedTextWithOptimisationsType2()
            privateTaxiRateLabel.setAttributedTextWithOptimisationsType2(fontSize: 4)
            taxiSharingRateLabel.setAttributedTextWithOptimisationsType2(fontSize: 4)
        }
        updateDetails(response: nil)
        activityIndicatorView.startAnimating()
        let offerId = safeString(self.userInfo.object(forKey: "offer_id"))
        Acf().getOfferDetails(withUserId: getUserId(self.userInfo),offerId:offerId) { (response) in
            execMain({[weak self] in guard let `self` = self else { return }

                self.activityIndicatorView.stopAnimating()
                if isNotNull(response){
                    updateDetails(response: (response as! NSDictionary).object(forKey: "data") as? NSDictionary)
                }
            },delay:0.1)
        }
    }
    
    @IBAction func onClickOfCloseButton(){
        Acf().hidePopupViewController()
    }
}
