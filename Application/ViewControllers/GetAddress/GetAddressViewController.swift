//
//  GetAddressViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

typealias GAContinueBlock = (_ continueAction :Bool) ->()

class GetAddressViewController: UIViewController,UITextFieldDelegate {
    
    //MARK: - variables and constants
    @IBOutlet fileprivate weak var infoLabel: UILabel!
    @IBOutlet fileprivate weak var doneButton: TKTransitionSubmitButton!
    @IBOutlet fileprivate weak var homeTiming: MKTextField!
    @IBOutlet fileprivate weak var homeLocation: MKTextField!
    @IBOutlet fileprivate weak var destinationNameTextField: MKTextField!
    @IBOutlet fileprivate weak var destinationTiming: MKTextField!
    @IBOutlet fileprivate weak var destinationLocation: MKTextField!
    
    fileprivate var homeTimingDate : Date?
    fileprivate var homeLocationCoordinate: CLLocationCoordinate2D?
    fileprivate var homeLocationAddress: String?
    fileprivate var destinationTimingDate : Date?
    fileprivate var destinationLocationCoordinate: CLLocationCoordinate2D?
    fileprivate var destinationLocationAddress: String?
    fileprivate var locationPicker : ALPlacesViewController?
    fileprivate var isAddressUpdateWarningAlertShown = false
    fileprivate var arrayDestinationNameOptions = NSMutableArray()
    fileprivate var selectedIndexDestinationNameOptions = 0
    
    //MARK: - view controller life cycle methods
    
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        Acf().updateProfileToServerIfRequired { (returnedData) in}
    }
    
    //MARK: - other methods
    private func setupForNavigationBar(){
        setAppearanceForNavigationBarType1(self.navigationController)
        setupNavigationBarTitleType1("Update",viewController: self)
        if self.isBackButtonRequired() {
            addNavigationBarButton(self, image: UIImage(named: "backarrowblack"), title: nil, isLeft: true)
        }
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }

    private func registerForNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(GetAddressViewController.updateAppearanceOfTextField), name: NSNotification.Name.UITextViewTextDidBeginEditing, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(GetAddressViewController.updateAppearanceOfTextField), name: NSNotification.Name.UITextViewTextDidEndEditing, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(GetAddressViewController.updateAppearanceOfTextField), name: NSNotification.Name.UITextFieldTextDidBeginEditing, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(GetAddressViewController.updateAppearanceOfTextField), name: NSNotification.Name.UITextFieldTextDidEndEditing, object: nil)
    }
    
    private func startUpInitialisations(){
        setAppearanceForViewController(self)
        setAppearanceForMKTextField(homeTiming)
        setAppearanceForMKTextField(homeLocation)
        setAppearanceForMKTextField(destinationNameTextField)
        setAppearanceForMKTextField(destinationTiming)
        setAppearanceForMKTextField(destinationLocation)
        
        homeLocation.placeholder = "Home Address"
        homeTiming.placeholder = "Time when you leave from home?"
        destinationLocation.placeholder = "\(destinationCapitalizedName()) Address"
        destinationTiming.placeholder = "Time when you leave from \(destinationCapitalizedName())?"
        
        if userType().isEqual( USER_TYPE_CORPORATE){
            destinationNameTextField.placeholder = "Company Name"
        }else if userType().isEqual( USER_TYPE_STUDENT){
            destinationNameTextField.placeholder = "College Name"
        }
        
        updateAppearanceOfTextField()
        
        doneButton.layoutIfNeeded()
        
        let settings = Dbm().getSetting()
        if isNotNull(settings.homeTime){
            homeTimingDate = (settings.homeTime?.dateValue() as Date?)
        }
        if isNotNull(settings.destinationTime){
            destinationTimingDate = (settings.destinationTime?.dateValue() as Date?)
        }
        if isNotNull(settings.homeAddress){
            homeLocationAddress = settings.homeAddress
        }
        if isNotNull(settings.destinationAddress){
            destinationLocationAddress = settings.destinationAddress
        }
        
        let userInfo = Dbm().getUserInfo()
        destinationNameTextField.text = userInfo!.destinationName
        
        if isNotNull(settings.homeLocationLatitude){
            homeLocationCoordinate = CLLocationCoordinate2DMake(settings.homeLocationLatitude!.doubleValue, settings.homeLocationLongitude!.doubleValue)
        }
        if isNotNull(settings.destinationLocationLongitude){
            destinationLocationCoordinate = CLLocationCoordinate2DMake(settings.destinationLocationLatitude!.doubleValue, settings.destinationLocationLongitude!.doubleValue)
        }
        
        if isNull(settings.homeAddress) || isNull(settings.destinationTime){
            showNotification(MESSAGE_TEXT___WHY_TO_SELECT_LOCATION, showOnNavigation: false, showAsError: false)
        }
        
        updateContentsOnScreen()
        trackScreenLaunchEvent(SLE_TIME_AND_ADDRESS_PICKER)
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        infoLabel.text = "Please provide your home and \(destinationName()) address and timings to commute.\nYou can change these details anytime you want."        
    }
    
    //MARK: - other functions
    
    func onClickOfLeftBarButton(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    func canContinuetoSaveInformation()->(Bool){
        var canContinue = true
        if canContinue {
            canContinue = validateIfNull(homeTiming.text , identifier: "home timing")
        }
        if canContinue {
            canContinue = validateIfNull(homeLocation.text , identifier: "home location")
        }
        if canContinue {
            canContinue = validateIfNull(destinationNameTextField.text , identifier: "\(destinationName()) Name")
        }
        if canContinue {
            canContinue = validateIfNull(destinationTiming.text , identifier: "\(destinationName()) timing")
        }
        if canContinue {
            canContinue = validateIfNull(destinationLocation.text , identifier: "\(destinationName()) location")
        }
        return canContinue
    }
    
    @IBAction func onClickOfDoneButton(){
        if(isInternetConnectivityAvailable(true)==false){return}
        if canContinuetoSaveInformation(){
            updateInformationOnDatabase()
            if self.isBackButtonRequired() {
                self.navigationController?.popViewController(animated: true)
            }else{
                Acf().navigationController!.dismissPopupViewController(SLpopupViewAnimationType.fade)
            }
            NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_ADDRESS_AND_TIMING_UPDATED), object: nil)
            trackFunctionalEvent(FE_TIME_AND_ADDRESS_PICKER_CLOSED, information: nil)
            trackFunctionalEvent(FE_LOC_CHANGE, information: nil)
            trackFunctionalEvent(FE_LOCATIONS_DETAILS_CAPTURED, information: nil)
        }
    }
    
    func updateAppearanceOfTextField(){
        Acf().updateAppearanceOfTextFieldType1(homeTiming)
        Acf().updateAppearanceOfTextFieldType1(homeLocation)
        Acf().updateAppearanceOfTextFieldType1(destinationNameTextField)
        Acf().updateAppearanceOfTextFieldType1(destinationTiming)
        Acf().updateAppearanceOfTextFieldType1(destinationLocation)
    }
    
    @IBAction func onClickOfHomeLocation(){
        showChangeAddressWarningIfApplicable { (continueAction) in
            if continueAction {
                
                resignKeyboard()
                Acf().setupIQKeyboardManagerDisabled()
                func onLocationPicked(_ name: String?, address: String?, coordinate: CLLocationCoordinate2D?, error: NSError?) {
                    if error != nil {
                        Acf().setupIQKeyboardManagerEnabled()
                        showNotification("some error occured , try again", showOnNavigation: false, showAsError: true)
                        return
                    }
                    Acf().navigationController!.dismiss(animated: true, completion: nil)
                    Acf().setupIQKeyboardManagerEnabled()
                    if isNotNull(address) && coordinate != nil {
                        self.homeLocationAddress = address
                        if isNotNull(self.homeLocationAddress) && isNotNull(self.destinationLocationAddress) && self.homeLocationAddress! == self.destinationLocationAddress {
                            showNotification(MESSAGE_TEXT___FOR_SAME_ADDRESS_ERROR, showOnNavigation: false, showAsError: true)
                            self.homeLocationAddress = nil
                            Acf().setupIQKeyboardManagerEnabled()
                            return
                        }
                        self.homeLocationCoordinate = coordinate
                        self.updateContentsOnScreen()
                        trackFunctionalEvent(FE_TIME_AND_ADDRESS_PICKER_HOME_SUGGESTION_SELECTED, information: nil)
                    }
                }
                
                self.locationPicker = ALPlacesViewController(APIKey: GOOGLE_API_KEY, completion: onLocationPicked)
                Acf().navigationController?.present(self.locationPicker!, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func onClickOfDestinationLocation(){
        showChangeAddressWarningIfApplicable { (continueAction) in
            if continueAction {
                resignKeyboard()
                Acf().setupIQKeyboardManagerDisabled()
                func onLocationPicked(_ name: String?, address: String?, coordinate: CLLocationCoordinate2D?, error: NSError?) {
                    if error != nil {
                        Acf().setupIQKeyboardManagerEnabled()
                        return
                    }
                    Acf().navigationController!.dismiss(animated: true, completion: nil)
                    Acf().setupIQKeyboardManagerEnabled()
                    if isNotNull(address) && coordinate != nil {
                        self.destinationLocationAddress = address
                        if isNotNull(self.homeLocationAddress) && isNotNull(self.destinationLocationAddress) && self.homeLocationAddress! == self.destinationLocationAddress {
                            showNotification(MESSAGE_TEXT___FOR_SAME_ADDRESS_ERROR, showOnNavigation: false, showAsError: true)
                            self.destinationLocationAddress = nil
                            Acf().setupIQKeyboardManagerEnabled()
                            return
                        }
                        self.destinationLocationCoordinate = coordinate
                        self.updateContentsOnScreen()
                        trackFunctionalEvent(FE_TIME_AND_ADDRESS_PICKER_DESTINATION_SUGGESTION_SELECTED, information: nil)
                    }
                }
                
                self.locationPicker = ALPlacesViewController(APIKey: GOOGLE_API_KEY, completion: onLocationPicked)
                Acf().navigationController?.present(self.locationPicker!, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func onClickOfHomeTiming(){
        resignKeyboard()
        ActionSheetDatePicker.show(withTitle: "Home to \(destinationCapitalizedName())", datePickerMode:.time, selectedDate: homeTimingDate ?? Date().change( nil, month: nil, day: nil, hour: 9, minute: 0, second: 0), doneBlock: { (picker, date, origin) -> Void in
            self.homeTimingDate = date as?Date
            self.updateContentsOnScreen()
        }, cancel: { (picker) -> Void in
            
        }, origin: self.view)
    }
    
    @IBAction func onClickOfDestinationTiming(){
        resignKeyboard()
        ActionSheetDatePicker.show(withTitle: "\(destinationCapitalizedName()) to Home", datePickerMode:.time, selectedDate: destinationTimingDate ?? Date().change( nil, month: nil, day: nil, hour: 18, minute: 0, second: 0), doneBlock: { (picker, date, origin) -> Void in
            self.destinationTimingDate = date as? Date
            self.updateContentsOnScreen()
        }, cancel: { (picker) -> Void in
            
        }, origin: self.view)
    }
    
    @IBAction func onClickOfDestinationName(){
        showChangeAddressWarningIfApplicable { (continueAction) in
            if continueAction {
                resignKeyboard()
                let viewController = getViewController("DestinationPickerViewController") as! DestinationPickerViewController
                viewController.completion = { (returnedData) in
                    if let company = returnedData as? Company{
                        self.selectedIndexDestinationNameOptions = self.arrayDestinationNameOptions.index(of: company)
                        self.destinationNameTextField.text = company.name
                    }else if let college = returnedData as? College{
                        self.selectedIndexDestinationNameOptions = self.arrayDestinationNameOptions.index(of: college)
                        self.destinationNameTextField.text = college.name
                    }
                }
                self.navigationController?.pushViewController(viewController, animated: true)
            }
        }
    }
    
    func updateContentsOnScreen(){
        if isNotNull(destinationTimingDate){
            destinationTiming.text = timeDateFormatter.string(from: destinationTimingDate!)
        }
        if isNotNull(homeTimingDate){
            homeTiming.text = timeDateFormatter.string(from: homeTimingDate!)
        }
        if isNotNull(destinationLocationAddress){
            destinationLocation.text = destinationLocationAddress
        }
        if isNotNull(homeLocationAddress){
            homeLocation.text = homeLocationAddress
        }
        updateAppearanceOfTextField()
        updateInformationOnDatabase()
    }
    
    
    func updateInformationOnDatabase(){
        let settings = Dbm().getSetting()
        if isNotNull(homeLocationAddress){
            settings.homeAddress = homeLocationAddress
        }
        if isNotNull(destinationLocationAddress){
            settings.destinationAddress = destinationLocationAddress
        }
        if (homeLocationCoordinate?.latitude != nil && homeLocationCoordinate!.latitude != 0){
            settings.homeLocationLatitude = NSNumber(value: homeLocationCoordinate!.latitude as Double)
        }
        if (homeLocationCoordinate?.longitude != nil && homeLocationCoordinate!.longitude != 0){
            settings.homeLocationLongitude = NSNumber(value: homeLocationCoordinate!.longitude as Double)
        }
        if (destinationLocationCoordinate?.latitude != nil && destinationLocationCoordinate!.latitude != 0){
            settings.destinationLocationLatitude = NSNumber(value: destinationLocationCoordinate!.latitude as Double)
        }
        if (destinationLocationCoordinate?.longitude != nil && destinationLocationCoordinate!.longitude != 0){
            settings.destinationLocationLongitude = NSNumber(value: destinationLocationCoordinate!.longitude as Double)
        }
        if isNotNull(homeTimingDate){
            settings.homeTime = homeTimingDate?.stringValue()
        }
        if isNotNull(destinationTimingDate){
            settings.destinationTime = destinationTimingDate?.stringValue()
        }
        
        let userInfo = Dbm().getUserInfo()
        if isNotNull(destinationNameTextField.text){
            userInfo!.destinationName = destinationNameTextField.text!
        }
        
        settings.isProfileUpdated = NSNumber(value: true)
        Dbm().saveChanges()
        if isUserLoggedIn() {
            NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_UPDATE_PROFILE_SCREEN), object: nil)
        }
    }
    
    func showChangeAddressWarningIfApplicable(continueBlock:@escaping GAContinueBlock){
        if !isAddressUpdateWarningAlertShown {
            let settings = Dbm().getSetting()
            if isNotNull(settings.homeAddress) && isNotNull(settings.destinationAddress) {
                isAddressUpdateWarningAlertShown = true
                execMain({
                    UIAlertController.showAlert(in: self, withTitle: MESSAGE_TITLE___ADDRESS_UPDATE_WARNING, message: MESSAGE_TEXT___ADDRESS_UPDATE_WARNING, cancelButtonTitle: "Cancel", destructiveButtonTitle: "Edit", otherButtonTitles: nil, tap: { (alert, action, index) in
                        if index == alert.destructiveButtonIndex {
                            continueBlock(true)
                        }else{
                            continueBlock(false)
                        }
                    })
                }, delay: 0.1)
            }else{
                continueBlock(true)
            }
        }else{
            continueBlock(true)
        }
    }
    
    var timeDateFormatter: DateFormatter {
        struct Static {
            static let instance : DateFormatter = {
                let formatter = DateFormatter()
                formatter.dateFormat = "h:mm a"
                return formatter
            }()
        }
        return Static.instance
    }
    
}
