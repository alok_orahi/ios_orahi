
//
//  TuitorialViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

class TuitorialViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    
    //MARK: - variables and constants
    @IBOutlet fileprivate weak var tuitorialCollectionView: UICollectionView!
    @IBOutlet fileprivate weak var pageControl: UIPageControl!
    @IBOutlet fileprivate weak var skipButton: UIButton!
    
    fileprivate var contentsArray = [[String:String]]()
    
    //MARK: - view controller life cycle methods
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
    }
    
    //MARK: - other methods
    private func setupForNavigationBar(){
        self.navigationController?.isNavigationBarHidden = true;
    }
    
    private func registerForNotifications(){
    }
    
    private func startUpInitialisations(){
        prepareContentsArray()
        setAppearanceForViewController(self)
        tuitorialCollectionView.reloadData()
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        prepareContentsArray()
        pageControl.numberOfPages = contentsArray.count
        tuitorialCollectionView.reloadData()
    }
    
    func prepareContentsArray(){
        if DEVICE_HEIGHT <= 480{
            contentsArray = [
                ["img": "PMT035"],
                ["img": "PMT135"],
                ["img": "PMT235"],
                ["img": "PMT335"],
                ["img": "PMT435"],
                ["img": "PMT535"],
                ["img": "PMT635"],
            ]
        }else{
            contentsArray = [
                ["img": "PMT0"],
                ["img": "PMT1"],
                ["img": "PMT2"],
                ["img": "PMT3"],
                ["img": "PMT4"],
                ["img": "PMT5"],
                ["img": "PMT6"],
            ]
        }
        tuitorialCollectionView.reloadData()
    }
    
    // MARK: - UICollection View Delegate & Datasource
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int{
        return contentsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        let cell: TuitorialCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "TuitorialCollectionViewCell", for: indexPath) as! TuitorialCollectionViewCell;
        let object:[String:Any] = contentsArray[(indexPath as NSIndexPath).item] as [String : Any]
        cell.tuitorialImage.image = UIImage(named:object["img"] as! String)
        return cell;
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize{
        return CGSize(width: DEVICE_WIDTH, height: DEVICE_HEIGHT)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat{
        return 0.0;
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat{
        return 0.0;
    }
    
    //MARK: -ScrollView Delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView){
        let pageWidth : CGFloat = tuitorialCollectionView.frame.size.width;
        let page: CGFloat = floor((tuitorialCollectionView.contentOffset.x - pageWidth/2)/pageWidth)+1;
        pageControl.currentPage = Int(page);
    }
    
    @IBAction func onClickOfSkipButton(){
        self.navigationController?.popViewController(animated: true)
    }
}
