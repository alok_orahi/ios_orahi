//
//  NotificationsViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class NotificationsViewController: UIViewController,UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,DZNEmptyDataSetDelegate,DZNEmptyDataSetSource {
    
    //MARK: - variables and constants
    @IBOutlet fileprivate weak var tableView : UITableView?
    fileprivate var notifications = NSMutableArray()
    var themeColor = APP_THEME_VOILET_COLOR
    var group : Group?
    var member : Member?
    
    //MARK: - view controller life cycle methods
    
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
        updateUserInterfaceOnScreen()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        for notification in notifications {
            if let n = notification as? Notification {
                n.read = true
            }
        }
        Dbm().saveChanges()
        NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_IN_APP_NOTIFICATION_COUNT_UPDATED), object: nil)
    }
    
    //MARK: - other methods
    private func setupForNavigationBar(){
        setAppearanceForNavigationBarType1(self.navigationController,color: themeColor)
        setupNavigationBarTitleType1("Notifications",viewController: self)
        if self.isBackButtonRequired() {
            addNavigationBarButton(self, image: UIImage(named: "backarrowblack"), title: nil, isLeft: true)
        }
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    func onClickOfLeftBarButton(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    private func registerForNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(NotificationsViewController.updateUserInterfaceOnScreen), name: NSNotification.Name(rawValue: NOTIFICATION_NEED_NOTIFICATIONS_UPDATE), object: nil)
    }
    
    private func startUpInitialisations(){
        registerNib("LoadingTableViewCell", tableView: tableView)
        registerNib("NotificationsTableViewCell", tableView: tableView)
        setAppearanceForViewController(self)
        setAppearanceForTableView(tableView)
        updateUserInterfaceOnScreen()
        trackScreenLaunchEvent(SLE_NOTIFICATIONS)
        Acf().syncNotifications {}
        
        let viewControllers = self.navigationController!.viewControllers
        var viewControllersAnotherCopy = [UIViewController]()
        for (index,v) in viewControllers.enumerated() {
            viewControllersAnotherCopy.append(v)
            if v is NotificationsViewController {
                if index < viewControllers.count-1 {
                    viewControllersAnotherCopy.removeLast()
                    viewControllersAnotherCopy.append(self)
                    self.navigationController?.viewControllers = viewControllersAnotherCopy
                    break
                }
            }
        }
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        if isNotNull(member) && isNotNull(group) {
            notifications = Dbm().getAllNotifications(groupId: safeString(self.group?.groupId), memberId: safeString(self.member?.userId))
        }else if isNotNull(group){
            notifications = Dbm().getAllNotifications(groupId: safeString(self.group?.groupId))
        }else{
            notifications = Dbm().getAllNotifications()
        }
        notifications = notifications.reversed() as! NSMutableArray
        tableView?.reloadData()
    }
    
    //MARK: - DZNEmptyDataSet Delegate & Data Source
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString!{
        let attributes = [NSFontAttributeName : UIFont.init(name: FONT_BOLD, size: 20)!]
        var message = "No notifications"
        if (isInternetConnectivityAvailable(false)==false){
            message = "Unable to load notifications"
        }
        if !isUserLoggedIn() {
            message = "Not Signed In"
        }
        return NSAttributedString(string: message, attributes: attributes)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString!{
        let attributes = [NSFontAttributeName : UIFont.init(name: FONT_REGULAR, size: 14)!]
        var message = "Don't worry , we will notify you as soon as new notification comes in"
        if (isInternetConnectivityAvailable(false)==false){
            message = MESSAGE_TEXT___FOR_NETWORK_NOT_REACHABILITY
        }
        if !isUserLoggedIn() {
            message = "Please Sign In to see notifications"
        }
        return NSAttributedString(string: message, attributes: attributes)
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage!{
        if !isUserLoggedIn() {
            return UIImage(named:"searchLightGray")
        }
        if (isInternetConnectivityAvailable(false)==false){
            return UIImage(named:"wifi")
        }
        return UIImage(named:"searchLightGray")
    }
    
    func emptyDataSetDidTap(_ scrollView: UIScrollView!){
    }
    
    //MARK: - UITableView Delegate & Data Source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifications.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 78.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView .dequeueReusableCell(withIdentifier: "NotificationsTableViewCell") as? NotificationsTableViewCell
        let notification = self.notifications.object(at: (indexPath as NSIndexPath).row) as! Notification
        cell?.notification = notification
        cell?.parentVC = self
        cell?.accessoryType = canShowThisNotification(notification: notification) ? .disclosureIndicator : .none
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let notification = self.notifications.object(at: (indexPath as NSIndexPath).row) as! Notification
        let canShowNotification = canShowThisNotification(notification: notification)
        let notiType = safeString(notification.notificationType)
        if canShowNotification {
            let mID = safeString(notification.associatedUserId)
            let gID = safeString(notification.groupId)
            let nc = self.navigationController
            if notiType == "carpool" {
                let rideDate = Date(timeIntervalSince1970: safeDouble(notification.travelDayDate))
                let rideTime = Date(timeIntervalSince1970: safeDouble(notification.travelDayTime))
                let rideMode = (rideTime.hour() > 12) ? "1" : "0"
                Acf().openRideHomePage(rideDate,timeDate:rideTime,mode:rideMode)
            }else if notiType == "place_exit" {
                openMemberDetail(memberId:mID , groupId:gID, navigationController:nc!)
            }else if notiType == "place_entry" {
                openMemberDetail(memberId:mID , groupId:gID, navigationController:nc!)
            }else if notiType == "checkin" {
                openMemberDetail(memberId:mID , groupId:gID, navigationController:nc!)
            }else if notiType == "member_added" {
                openMemberDetail(memberId:mID , groupId:gID, navigationController:nc!)
            }else if notiType == "place_added" {
                openPlaces(groupId:gID, navigationController:nc!)
            }else if notiType == "location_request" {
                openPlaces(groupId:gID, navigationController:nc!)
            }else if notiType == "sos" {
                openMemberDetail(memberId:mID , groupId:gID, navigationController:nc!)
            }else{
                if isNotNull(gID) {
                    openMemberDetail(memberId:mID , groupId:gID, navigationController:nc!)
                }
            }
        }
    }
}
