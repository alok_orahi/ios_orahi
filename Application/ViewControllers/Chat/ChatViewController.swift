//
//  ChatViewController.swift
//  Orahi
//
//  Created by Alok Singh on 28/07/16.
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import DZNEmptyDataSet
import ActionSheetPicker_3_0

fileprivate var messageTimeDateFormatter: DateFormatter {
    struct Static {
        static let instance : DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateFormat = "h:mm a"
            return formatter
        }()
    }
    return Static.instance
}

class ChatViewController: QMChatViewController, QMChatServiceDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate , QMChatAttachmentServiceDelegate, QMChatConnectionDelegate, QMChatCellDelegate {
    
    //MARK: - variables and constants
    
    var dialog: QBChatDialog?
    fileprivate var didBecomeActiveHandler : Any?
    fileprivate var didEnterBackgroundHandler : Any?
    fileprivate var attachmentCellsMap : [String : QMChatAttachmentCell] = [String : QMChatAttachmentCell]()
    fileprivate var detailedCells: Set<String> = []
    fileprivate var typingTimer : Timer?
    fileprivate var showDetailedCellByDefault = true
    fileprivate var unreadMessages: [QBChatMessage]?
    fileprivate let chatBubbleBgColorOutGoing = UIColor(colorLiteralRed: 242.0/255.0, green: 231.0/255.0, blue: 244.0/255.0, alpha: 1.0)
    fileprivate let chatBubbleBgColorInComing = UIColor(colorLiteralRed: 229.0/255.0, green: 223.0/255.0, blue: 213.0/255.0, alpha: 1.0)
    fileprivate lazy var imagePickerViewController : UIImagePickerController = {
        let imagePickerViewController = UIImagePickerController()
        imagePickerViewController.delegate = self
        return imagePickerViewController
    }()
    var themeColor = APP_THEME_VOILET_COLOR
    var group:Group?

    //MARK: - view controller life cycle methods
    
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
        registerForNotifications()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        actionWhenViewDidAppear()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        actionWhenViewDidDisappear()
    }
    
    //MARK: - other methods
    
    private func setupForNavigationBar(){
        setAppearanceForNavigationBarType1(self.navigationController,color: themeColor)
        if self.isBackButtonRequired() {
            addNavigationBarButton(self, image: UIImage(named: "backarrowblack"), title: nil, isLeft: true)
            addNavigationBarButton(self, image: UIImage(named: "actionButton"), title: nil, isLeft: false)
        }
        updateTitle()
    }
    
    func onClickOfLeftBarButton(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    func onClickOfRightBarButton(_ sender:Any){
        resignKeyboard()
        func showOptionsForPrivateChat(){
            let isSystemReadyToPerformBlockUnBlockOperation = ServicesManager.instance().contactListService.isSystemReadyToPerformBlockUnBlockOperation()
            let isUserBlocked = ServicesManager.instance().contactListService.isUserBlocked(self.dialog!.recipientID.toUInt)
            let actionSheet = Hokusai()
            if isSystemReadyToPerformBlockUnBlockOperation {
                actionSheet.addButton(isUserBlocked ?"Un Block" : "Block") {
                    func showFailureError(){
                        showNotification(MESSAGE_TEXT___BLOCK_UNBLOCK_NOT_READY, showOnNavigation: false, showAsError: true)
                    }
                    if (isInternetConnectivityAvailable(true)==false){return}
                    if (Acf().isQBReadyForItsUserDependentServices() == false){showFailureError();return;}
                    if (ServicesManager.instance().isAuthorized()==false){showFailureError();return;}
                    if self.dialog?.type == QBChatDialogType.private {
                        if self.dialog!.recipientID > 0 {
                            if let recipient = ServicesManager.instance().usersService.usersMemoryStorage.user(withID: UInt(self.dialog!.recipientID)) {
                                let user = recipient
                                let login = user.login
                                let userId = Acf().getUserIdFrom(login)
                                Acf().block(["userId":userId], isBlock: !isUserBlocked)
                                self.dialog?.leave(completionBlock: {[weak self] (error) in guard let `self` = self else { return }})
                                self.navigationController?.popViewController(animated: true)
                            }else{
                                showActivityIndicator("Processing")
                                hideActivityIndicatorAfter(60)
                                execMain({[weak self]  in guard let `self` = self else { return }
                                    ServicesManager.instance().searchUsersWithIDs([NSNumber(value:self.dialog!.recipientID)], success: { (users) in
                                        hideActivityIndicator()
                                        if isNotNull(users) && users!.count > 0{
                                            let user = users?[0]
                                            let login = user?.login
                                            let userId = Acf().getUserIdFrom(login)
                                            Acf().block(["userId":userId], isBlock: !isUserBlocked)
                                            self.dialog?.leave(completionBlock: {[weak self] (error) in guard let `self` = self else { return }})
                                            self.navigationController?.popViewController(animated: true)
                                        }else{
                                            showFailureError()
                                        }
                                    }, error: { (error) in
                                        logMessage("\(error)")
                                        hideActivityIndicator()
                                        showFailureError()
                                    })
                                    },delay: 0.2)
                            }
                        }
                    }
                }
            }
            actionSheet.addButton("See Details") {
                Acf().pushVC("ChatUsersInfoTableViewController", navigationController: self.navigationController, isRootViewController: false, animated: true) { (viewControllerObject) -> () in
                    (viewControllerObject as! ChatUsersInfoTableViewController).dialog = self.dialog
                }
            }
            actionSheet.cancelButtonTitle = "Cancel"
            actionSheet.colors = hokColor(color: themeColor)
            actionSheet.show()
        }
        func showOptionsForGroupChat(){
            let actionSheet = Hokusai()
            actionSheet.addButton("See Details") {
                Acf().pushVC("ChatUsersInfoTableViewController", navigationController: self.navigationController, isRootViewController: false, animated: true) { (viewControllerObject) -> () in
                    (viewControllerObject as! ChatUsersInfoTableViewController).dialog = self.dialog
                    if self.title == "\(RIDE_GROUP_DISPLAY_NAME_SELF_PREFIX) \(RIDE_GROUP_DISPLAY_NAME_SELF_SUFFIX)"{
                        (viewControllerObject as! ChatUsersInfoTableViewController).messageToDisplay = MESSAGE_TEXT___THIS_IS_YOUR_MY_RIDERS_GROUP
                        (viewControllerObject as! ChatUsersInfoTableViewController).titleToDisplay = MESSAGE_TITLE___THIS_IS_YOUR_MY_RIDERS_GROUP
                    }
                }
            }
            actionSheet.cancelButtonTitle = "Cancel"
            actionSheet.colors = hokColor(color: themeColor)
            actionSheet.show()
        }
        if self.dialog?.type == QBChatDialogType.private {
            showOptionsForPrivateChat()
        }else{
            showOptionsForGroupChat()
        }
    }
    
    private func registerForNotifications(){
        self.didBecomeActiveHandler = NotificationCenter.default.addObserver(forName: NSNotification.Name.UIApplicationDidBecomeActive, object: nil, queue: OperationQueue.main) { (notification: Foundation.Notification) -> Void in
        }
        self.didEnterBackgroundHandler = NotificationCenter.default.addObserver(forName: NSNotification.Name.UIApplicationDidEnterBackground, object: nil, queue: OperationQueue.main, using: {
            [weak self] (notification: Foundation.Notification!) -> Void in
            self?.fireSendStopTypingIfNecessary()
            })
    }
    
    func unRegisterForNotifications(){
        if let didBecomeActiveHandler = self.didBecomeActiveHandler {
            NotificationCenter.default.removeObserver(didBecomeActiveHandler)
        }
        if let didEnterBackgroundHandler = self.didEnterBackgroundHandler {
            NotificationCenter.default.removeObserver(didEnterBackgroundHandler)
        }
    }
    
    private func startUpInitialisations(){
        setAppearanceForViewController(self)
        setupForQuickBlox()
        trackScreenLaunchEvent(SLE_CHAT)
        let information = NSMutableDictionary()
        if (self.dialog?.type == .private){
            information.setObject("Private", forKey: "chatType" as NSCopying)
        }else{
            information.setObject("Group", forKey: "chatType" as NSCopying)
            information.setObject("", forKey: "chattingOn" as NSCopying)
        }
        trackFunctionalEvent(FE_START_CHAT, information: information)
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        self.updateMessages()
    }
    
    func actionWhenViewDidAppear(){
        Acf().updateQBUsersCacheFromServerIfRequired(nil)
        Acf().validateAndUpdateEventMessagesDateSent()
        if self.storedMessages() != nil {
            if self.chatSectionManager.totalMessagesCount != UInt(self.storedMessages()!.count) {
                self.chatSectionManager.add(self.storedMessages()!)
            }
        }
        ServicesManager.instance().chatService.addDelegate(self)
        ServicesManager.instance().chatService.chatAttachmentService.delegate = self

        let viewControllers = self.navigationController!.viewControllers
        var viewControllersAnotherCopy = [UIViewController]()
        for (index,v) in viewControllers.enumerated() {
            viewControllersAnotherCopy.append(v)
            if v is ChatViewController {
                if index < viewControllers.count-1 {
                    viewControllersAnotherCopy.removeLast()
                    viewControllersAnotherCopy.append(self)
                    self.navigationController?.viewControllers = viewControllersAnotherCopy
                    break
                }
            }
        }
        if let dialog = self.dialog {
            ServicesManager.instance().currentDialogID = dialog.id!
        }
        checkGroupValidityAndInformToUserIfRequired()
    }
    
    func actionWhenViewDidDisappear(){
        unRegisterForNotifications()
        ServicesManager.instance().currentDialogID = ""
        ServicesManager.instance().chatService.removeDelegate(self);
        self.dialog?.clearTypingStatusBlocks()
    }
    
    
    func setupForQuickBlox(){
        self.dialog?.onUserIsTyping = {
            [weak self] (userID)-> Void in
            if ServicesManager.instance().currentUser()!.id == userID {
                return
            }
            let user = ServicesManager.instance().usersService.usersMemoryStorage.user(withID: UInt(userID))
            setupNavigationBarTitleType1("\(nameToDisplay(user,chatDialogue: self!.dialog)) is typing ...",viewController: self)
        }
        self.dialog?.onUserStoppedTyping = {
            [weak self] (userID)-> Void in
            if ServicesManager.instance().currentUser()!.id == userID {
                return
            }
            self!.updateTitle()
        }
        self.senderID = ServicesManager.instance().currentUser()!.id
        self.senderDisplayName = nameToDisplay(ServicesManager.instance().currentUser(),chatDialogue: self.dialog)
        self.heightForSectionHeader = 40.0
        self.updateTitle()
        self.collectionView?.backgroundColor = UIColor.white
        self.inputToolbar?.contentView?.backgroundColor = UIColor.white
        self.inputToolbar?.contentView?.textView?.placeHolder = "Message"
        if ENABLE_ATTACHMENT_BUTTON_ON_CHAT_SCREEN == false {
            self.inputToolbar?.contentView?.leftBarButtonItem = nil
        }
    }
    
    func checkGroupValidityAndInformToUserIfRequired(){
        if dialog!.type != .private {
            if isInternetConnectivityAvailable(false){
                ServicesManager.instance().chatService.fetchDialog(withID: dialog!.id!, completion: { (dialogue) in
                    if isNull(dialogue){
                        showInformationBannerOnBottom(self, style: .blurLight, duration: 360, title: MESSAGE_TITLE___YOU_ARE_NOT_PARTICIPANT_ON_THIS_GROUP, message: MESSAGE_TEXT___YOU_ARE_NOT_PARTICIPANT_ON_THIS_GROUP)
                    }
                })
            }
            if isNull(ServicesManager.instance().chatService.dialogsMemoryStorage.chatDialog(withID: dialog!.id!)){
                showInformationBannerOnBottom(self, style: .blurLight, duration: 360, title: MESSAGE_TITLE___YOU_ARE_NOT_PARTICIPANT_ON_THIS_GROUP, message: MESSAGE_TEXT___YOU_ARE_NOT_PARTICIPANT_ON_THIS_GROUP)
            }
        }else{
            let isUserBlocked = ServicesManager.instance().contactListService.isUserBlocked(self.dialog!.recipientID.toUInt)
            if isUserBlocked {
                showInformationBannerOnBottom(self, style: .blurLight, duration: 360, title: MESSAGE_TITLE___YOU_HAVE_BLOCKED_THIS_USER, message: MESSAGE_TEXT___YOU_HAVE_BLOCKED_THIS_USER)
            }
        }
    }
    
    deinit{
        NotificationCenter.default.removeObserver(self)
    }
    
    func updateTitle() {
        if self.dialog?.type != QBChatDialogType.private {
            if dialog!.recipientID < 0 {
                setupNavigationBarTitleType1(displayNameFromName(dialog!.name),viewController: self)
            }else if let recipient = ServicesManager.instance().usersService.usersMemoryStorage.user(withID: UInt(dialog!.recipientID)) {
                let name = nameToDisplay(recipient,chatDialogue: self.dialog)
                if isNotNull(name){
                    setupNavigationBarTitleType1(nameToDisplay(recipient,chatDialogue: self.dialog),viewController: self)
                }else{
                    setupNavigationBarTitleType1(dialog?.name,viewController: self)
                }
            }
        } else {
            if dialog!.recipientID < 0 {
                setupNavigationBarTitleType1("Chat",viewController: self)
            }else if let recipient = ServicesManager.instance().usersService.usersMemoryStorage.user(withID: UInt(dialog!.recipientID)) {
                setupNavigationBarTitleType1(nameToDisplay(recipient,chatDialogue: self.dialog),viewController: self)
            }else{
                setupNavigationBarTitleType1("Chat",viewController: self)
            }
        }
    }
    
    func storedMessages() -> [QBChatMessage]? {
        return ServicesManager.instance().chatService.messagesMemoryStorage.messages(withDialogID: self.dialog!.id!) as [QBChatMessage]?
    }
    
    func loadMessages() {
        guard let currentDialogID = self.dialog!.id else {
            return
        }
        ServicesManager.instance().chatService.messages(withChatDialogID: currentDialogID) { [weak self] (response, messages) in
            guard let strongSelf = self else { return }
            guard response.error == nil else {
                return
            }
            if messages!.count > 0 {
                strongSelf.chatSectionManager.add(messages!)
                Acf().validateAndUpdateEventMessagesDateSent()
            }
        }
    }
    
    func updateMessages() {
        if (self.storedMessages()!.count > 0 && self.chatSectionManager.totalMessagesCount == 0) {
            self.chatSectionManager.update(self.storedMessages()!)
            self.loadMessages()
        } else {
            if self.chatSectionManager.totalMessagesCount == 0 {
            }
            ServicesManager.instance().cachedMessages(withDialogID: self.dialog!.id!, block: { (collection) in
                SwiftTryCatch.tryThis({
                    if (collection!.count > 0) {
                        self.chatSectionManager.add(collection as! [QBChatMessage])
                    }
                    self.loadMessages()
                    }, catchThis: {error in},finally:{})
            })
        }
    }
    
    func sendReadStatusForMessage(_ message: QBChatMessage) {
        guard QBSession.current().currentUser != nil else {
            return
        }
        guard message.senderID != QBSession.current().currentUser?.id else {
            return
        }
        let currentUserID = NSNumber(value: QBSession.current().currentUser!.id as UInt)
        if (message.readIDs == nil || !message.readIDs!.contains(currentUserID)) {
            ServicesManager.instance().chatService.read(message, completion: { (error) in
                Acf().updateBadgeValues()
            })
        }
    }
    
    func readMessages(_ messages: [QBChatMessage]) {
        if QBChat.instance().isConnected {
            ServicesManager.instance().chatService.read(messages, forDialogID: self.dialog!.id!, completion: nil)
        } else {
            self.unreadMessages = messages
        }
        var messageIDs = [String]()
        for message in messages {
            messageIDs.append(message.id!)
        }
    }
    
    override func didPickAttachmentImage(_ image: UIImage!) {
        if(isInternetConnectivityAvailable(true)==false){return}
        if self.dialog?.type == QBChatDialogType.private {
            let isUserBlocked = ServicesManager.instance().contactListService.isUserBlocked(self.dialog!.recipientID.toUInt)
            if isUserBlocked {
                showPopupAlertMessage(MESSAGE_TITLE___YOU_HAVE_BLOCKED_THIS_USER, message: MESSAGE_TEXT___YOU_HAVE_BLOCKED_THIS_USER, messageType: PopupMessageType.information)
                return
            }
        }
        let message = QBChatMessage()
        message.senderID = ServicesManager.instance().currentUser()!.id
        message.dialogID = self.dialog?.id
        message.dateSent = Date()
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {
            [unowned self] () -> Void in
            var newImage : UIImage! = image
            if self.imagePickerViewController.sourceType == UIImagePickerControllerSourceType.camera {
                newImage = newImage.fixOrientation()
            }
            
            let largestSide = newImage.size.width > newImage.size.height ? newImage.size.width : newImage.size.height
            let scaleCoeficient = largestSide/560.0
            let newSize = CGSize(width: newImage.size.width/scaleCoeficient, height: newImage.size.height/scaleCoeficient)
            
            // create smaller image
            UIGraphicsBeginImageContext(newSize)
            newImage.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
            let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            // Sending attachment.
            execMain({[weak self] in guard let `self` = self else { return }

                ServicesManager.instance().chatService.sendAttachmentMessage(message, to: self.dialog!, withAttachmentImage: resizedImage!, completion: {[weak self] (error) in
                    if self != nil {
                        self!.attachmentCellsMap.removeValue(forKey: message.id!)
                    }
                    if error != nil {
                        showNotification(error!.localizedDescription, showOnNavigation: true, showAsError: true)
                        ServicesManager.instance().chatService.deleteMessageLocally(message)
                        if self != nil {
                            self?.chatSectionManager.delete(message)
                        }
                    }
                })
                })
            })
    }
    
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: UInt, senderDisplayName: String!, date: Date!) {
        if(isInternetConnectivityAvailable(true)==false){return}
        if self.dialog?.type == QBChatDialogType.private {
            let isUserBlocked = ServicesManager.instance().contactListService.isUserBlocked(self.dialog!.recipientID.toUInt)
            if isUserBlocked {
                showPopupAlertMessage(MESSAGE_TITLE___YOU_HAVE_BLOCKED_THIS_USER, message: MESSAGE_TEXT___YOU_HAVE_BLOCKED_THIS_USER, messageType: PopupMessageType.information)
                return
            }
        }
        if isSystemReadyToProcessThis() && isNotNull(text){
            self.fireSendStopTypingIfNecessary()
            let message = QBChatMessage()
            message.text = text;
            message.senderID = self.senderID
            message.deliveredIDs = [NSNumber(value:self.senderID)]
            message.readIDs = [NSNumber(value:self.senderID)]
            message.markable = true
            message.dateSent = date
            if self.dialog?.type == QBChatDialogType.publicGroup {
                let userInfo = Dbm().getUserInfo()
                message.customParameters = [MESSAGE_TYPE_KEY:"PublicGroup","sendersName":userInfo!.name!.firstName(),"userId":loggedInUserId()]
            }
            else if self.dialog?.type == QBChatDialogType.private {
                let userInfo = Dbm().getUserInfo()
                message.customParameters = [MESSAGE_TYPE_KEY:"PrivateChat","sendersName":userInfo!.name!.firstName(),"userId":loggedInUserId()]
            }
            else if self.dialog?.type == QBChatDialogType.group {
                let userInfo = Dbm().getUserInfo()
                message.customParameters = [MESSAGE_TYPE_KEY:"Group","sendersName":userInfo!.name!.firstName(),"userId":loggedInUserId()]
            }
            self.sendMessage(message)
        }
    }
    
    
    func sendMessage(_ message: QBChatMessage ,scrollToMessage:Bool = true) {
        Dbm().deleteFailedQBMessage(["messageId":message.id!])
        ServicesManager.instance().chatService.send(message, to: self.dialog!, saveToHistory: true, saveToStorage: true) { (error) in
            if (error != nil) {
                Acf().loginToQuickBlox()
                Dbm().addFailedQBMessage(["messageId":message.id!])
            }
            self.collectionView.reloadData()
        }
        if scrollToMessage {
            self.finishSendingMessage(animated: true)
        }
    }
    
    //MARK: - Helper
    func statusStringFromMessage(_ message: QBChatMessage) -> String {
        let shouldNotShow = self.dialog?.type == .publicGroup
        if shouldNotShow{//In case of public groups , there can be more than 100 peoples , so lets not show status of each user , because it will fill the Screen unnecessarily with lots of users.
            return ""
        }
        let shouldAppendName = self.dialog?.type == .group
        var statusString : String = ""
        var currentUserID = 0
        if Acf().isQBReadyForItsUserDependentServices(){
            currentUserID = Int(ServicesManager.instance().currentUser()!.id)
        }
        var readersLogin = [String]()
        if message.readIDs != nil {
            let messageReadIDs = (message.readIDs as! [Int]).filter { (element : Int) -> Bool in
                return element != currentUserID
            }
            if !messageReadIDs.isEmpty {
                var Counter = 0
                for readID : Int in messageReadIDs {
                    let user = ServicesManager.instance().usersService.usersMemoryStorage.user(withID: UInt(readID))
                    if user != nil {
                        Counter += 1
                        if Counter <= MAXIMUM_NO_OF_NAMES_AT_A_TIME{
                            if Counter == MAXIMUM_NO_OF_NAMES_AT_A_TIME{
                                readersLogin.append("\(nameToDisplay(user,chatDialogue: self.dialog)) ...")
                            }else{
                                readersLogin.append(nameToDisplay(user,chatDialogue: self.dialog))
                            }
                        }else{
                            break
                        }
                    }
                }
                if shouldAppendName && readersLogin.count > 0 {
                    statusString += message.isMediaMessage() ? "Seen" : "Read" + ": " + readersLogin.joined(separator: ", ")
                }else{
                    statusString += message.isMediaMessage() ? "Seen" : "Read"
                }
            }
        }
        if message.deliveredIDs != nil {
            var deliveredLogin = [String]()
            let messageDeliveredIDs = (message.deliveredIDs as! [Int]).filter { (element : Int) -> Bool in
                return element != currentUserID
            }
            if !messageDeliveredIDs.isEmpty {
                var Counter = 0
                for deliveredID : Int in messageDeliveredIDs {
                    let user = ServicesManager.instance().usersService.usersMemoryStorage.user(withID: UInt(deliveredID))
                    if user != nil {
                        if readersLogin.contains(nameToDisplay(user,chatDialogue: self.dialog)) {
                            continue
                        }
                        Counter += 1
                        if Counter <= MAXIMUM_NO_OF_NAMES_AT_A_TIME{
                            if Counter == MAXIMUM_NO_OF_NAMES_AT_A_TIME{
                                deliveredLogin.append("\(nameToDisplay(user,chatDialogue: self.dialog)) ...")
                            }else{
                                deliveredLogin.append(nameToDisplay(user,chatDialogue: self.dialog))
                            }
                        }else{
                            break
                        }
                    }
                }
                if readersLogin.count > 0 && deliveredLogin.count > 0 {
                    statusString += "\n"
                }
                if deliveredLogin.count > 0 {
                    if shouldAppendName && deliveredLogin.count > 0 {
                        statusString += "Delievered" + ": " + deliveredLogin.joined(separator: ", ")
                    }else{
                        statusString += "Delievered"
                    }
                }
            }
        }
        if isNotNull(Dbm().getFailedQBMessage(["messageId":message.id!])) {
            statusString = "Failed"
        }else if statusString.isEmpty {
            statusString = "Sent"
        }
        
        statusString = statusString.replacingOccurrences(of: "Sent", with: " ✓ ")
        statusString = statusString.replacingOccurrences(of: "Delievered", with: " ✓✓ ")
        statusString = statusString.replacingOccurrences(of: "Read", with: " ✓✓✓ ")
        statusString = statusString.replacingOccurrences(of: "Seen", with: " ✓✓✓ ")
        
        return statusString
    }
    
    //MARK: - Override
    override func viewClass(forItem item: QBChatMessage) -> AnyClass? {
        if let messageType = item.customParameters?.object(forKey: MESSAGE_TYPE_KEY) as? String {
            if messageType == GroupEvents.rideGroupEvent ||
                messageType == GroupEvents.groupUpdateEvent {
                return QMChatNotificationCell.self
            }
        }
        if (item.senderID != self.senderID) {
            if (item.attachments != nil && item.attachments!.count > 0) || item.attachmentStatus != QMMessageAttachmentStatus.notLoaded {
                return QMChatAttachmentIncomingCell.self
            } else {
                return QMChatIncomingCell.self
            }
        } else {
            if (item.attachments != nil && item.attachments!.count > 0) || item.attachmentStatus != QMMessageAttachmentStatus.notLoaded {
                return QMChatAttachmentOutgoingCell.self
            } else {
                return QMChatOutgoingCell.self
            }
        }
    }
    
    //MARK: - Collection View Delegate & Datasource
    
    override func collectionView(_ collectionView: QMChatCollectionView!, dynamicSizeAt indexPath: IndexPath!, maxWidth: CGFloat) -> CGSize {
        var size = CGSize.zero
        if let item : QBChatMessage = self.chatSectionManager.message(for: indexPath) {
            if self.viewClass(forItem: item) === QMChatAttachmentIncomingCell.self {
                size = CGSize(width: min(200, maxWidth), height: 200)
            } else if self.viewClass(forItem: item) === QMChatAttachmentOutgoingCell.self {
                let attributedString = self.bottomLabelAttributedString(forItem: item)
                let bottomLabelSize = TTTAttributedLabel.sizeThatFitsAttributedString(attributedString, withConstraints: CGSize(width: min(200, maxWidth), height: CGFloat.greatestFiniteMagnitude), limitedToNumberOfLines: 0)
                size = CGSize(width: min(200, maxWidth), height: 200 + ceil(bottomLabelSize.height))
            } else {
                let messageTextLabelAttributedString = self.attributedString(forItem: item)
                let topLabelAttributedString = self.topLabelAttributedString(forItem: item)
                let options: NSStringDrawingOptions = [NSStringDrawingOptions.usesLineFragmentOrigin, NSStringDrawingOptions.usesFontLeading]
                let messageTextLabelRect = messageTextLabelAttributedString?.boundingRect(with: CGSize(width: maxWidth, height: CGFloat.greatestFiniteMagnitude), options: options, context: nil)
                let topLabelTextLabelRect = topLabelAttributedString?.boundingRect(with: CGSize(width: maxWidth, height: CGFloat.greatestFiniteMagnitude), options: options, context: nil)
                var desiredAttributedString = messageTextLabelAttributedString
                if messageTextLabelRect!.size.width < topLabelTextLabelRect!.size.width {
                    desiredAttributedString = topLabelAttributedString
                }
                size = TTTAttributedLabel.sizeThatFitsAttributedString(desiredAttributedString, withConstraints: CGSize(width: maxWidth, height: CGFloat.greatestFiniteMagnitude), limitedToNumberOfLines: 0)
            }
        }
        return size
    }
    
    override func collectionView(_ collectionView: QMChatCollectionView!, minWidthAt indexPath: IndexPath!) -> CGFloat {
        var size = CGSize.zero
        if let item : QBChatMessage = self.chatSectionManager.message(for: indexPath) {
            if self.detailedCells.contains(item.id!) || showDetailedCellByDefault {
                let str = self.bottomLabelAttributedString(forItem: item)
                let frameWidth = collectionView.frame.width
                let maxHeight = CGFloat.greatestFiniteMagnitude;
                
                size = TTTAttributedLabel.sizeThatFitsAttributedString(str, withConstraints: CGSize(width:frameWidth - kMessageContainerWidthPadding, height: maxHeight), limitedToNumberOfLines:0)
            }
            if self.dialog?.type != QBChatDialogType.private {
                let topLabelSize = TTTAttributedLabel.sizeThatFitsAttributedString(self.topLabelAttributedString(forItem: item), withConstraints: CGSize(width: collectionView.frame.width - kMessageContainerWidthPadding, height: CGFloat.greatestFiniteMagnitude), limitedToNumberOfLines:0)
                if topLabelSize.width > size.width {
                    size = topLabelSize
                }
            }
        }
        return size.width
    }
    
    override func collectionView(_ collectionView: QMChatCollectionView!, layoutModelAt indexPath: IndexPath!) -> QMChatCellLayoutModel {
        var layoutModel : QMChatCellLayoutModel = super.collectionView(collectionView, layoutModelAt: indexPath)
        layoutModel.avatarSize = CGSize(width: 0, height:0)
        layoutModel.spaceBetweenTextViewAndBottomLabel = 5
        layoutModel.topLabelHeight = 0.0
        if let item : QBChatMessage = self.chatSectionManager.message(for: indexPath) {
            let viewClass : AnyClass = self.viewClass(forItem: item)! as AnyClass
            if viewClass === QMChatOutgoingCell.self || viewClass === QMChatAttachmentOutgoingCell.self {
            } else if viewClass == QMChatIncomingCell.self || viewClass == QMChatAttachmentIncomingCell.self {
                if self.dialog?.type != QBChatDialogType.private {
                    let topAttributedString = self.topLabelAttributedString(forItem: item)
                    let size = TTTAttributedLabel.sizeThatFitsAttributedString(topAttributedString, withConstraints: CGSize(width: collectionView.frame.width - kMessageContainerWidthPadding, height: CGFloat.greatestFiniteMagnitude), limitedToNumberOfLines:1)
                    layoutModel.topLabelHeight = size.height
                }
                layoutModel.spaceBetweenTopLabelAndTextView = 5
                if self.dialog?.type == QBChatDialogType.private {
                    layoutModel.avatarSize = CGSize(width: 0, height:0)
                }else{
                    layoutModel.avatarSize = CGSize(width: 0, height:0)
                }
            }
            var size = CGSize.zero
            if self.detailedCells.contains(item.id!) || showDetailedCellByDefault {
                let bottomAttributedString = self.bottomLabelAttributedString(forItem: item)
                size = TTTAttributedLabel.sizeThatFitsAttributedString(bottomAttributedString, withConstraints: CGSize(width: collectionView.frame.width - kMessageContainerWidthPadding, height: CGFloat.greatestFiniteMagnitude), limitedToNumberOfLines:0)
            }
            layoutModel.bottomLabelHeight = floor(size.height)
        }
        return layoutModel
    }
    
    override func collectionView(_ collectionView: QMChatCollectionView!, configureCell cell: UICollectionViewCell!, for indexPath: IndexPath!) {
        super.collectionView(collectionView, configureCell: cell, for: indexPath)
        // subscribing to cell delegate
        (cell as! QMChatCell).delegate = self
        if let attachmentCell = cell as? QMChatAttachmentCell {
            if let chatIncomingCell = attachmentCell as? QMChatAttachmentIncomingCell {
                Acf().setShowImageOfImageView(imageView: chatIncomingCell.attachmentImageView)
                chatIncomingCell.attachmentImageView?.layer.cornerRadius = 3
                chatIncomingCell.attachmentImageView?.layer.masksToBounds = true
            }
            else if let chatOutgoingCell = attachmentCell as? QMChatAttachmentOutgoingCell {
                Acf().setShowImageOfImageView(imageView:chatOutgoingCell.attachmentImageView)
                chatOutgoingCell.attachmentImageView?.layer.cornerRadius = 3
                chatOutgoingCell.attachmentImageView?.layer.masksToBounds = true
            }
            if attachmentCell is  QMChatAttachmentIncomingCell {
                (cell as! QMChatCell).containerView?.bgColor = chatBubbleBgColorInComing
            } else if attachmentCell is  QMChatAttachmentOutgoingCell {
                (cell as! QMChatCell).containerView?.bgColor = chatBubbleBgColorOutGoing
            }
            let message : QBChatMessage = self.chatSectionManager.message(for: indexPath)
            if let attachments = message.attachments {
                if let attachment: QBChatAttachment = attachments.first {
                    for (existingAttachmentID, existingAttachmentCell) in self.attachmentCellsMap {
                        if existingAttachmentCell === attachmentCell  {
                            if existingAttachmentID == attachment.id {
                                continue
                            } else {
                                self.attachmentCellsMap.removeValue(forKey: existingAttachmentID)
                            }
                        }
                    }
                    self.attachmentCellsMap[attachment.id!] = attachmentCell
                    attachmentCell.attachmentID = attachment.id
                    // Getting image from chat attachment cache.
                    ServicesManager.instance().chatService.chatAttachmentService.getImageForAttachmentMessage(message, completion: { (error, image) in
                        if attachmentCell.attachmentID != attachment.id {
                            return
                        }
                        self.attachmentCellsMap.removeValue(forKey: attachment.id!)
                        if error != nil {
                            showNotification(error!.localizedDescription, showOnNavigation: false, showAsError: true)
                        } else {
                            if image != nil {
                                attachmentCell.setAttachmentImage(image)
                                cell.updateConstraints()
                            }
                        }
                    })
                }
            }
        } else if cell is QMChatIncomingCell  || cell is  QMChatAttachmentIncomingCell  {
            (cell as! QMChatCell).containerView?.bgColor = chatBubbleBgColorInComing
        } else if cell is QMChatOutgoingCell  || cell is  QMChatAttachmentOutgoingCell  {
            (cell as! QMChatCell).containerView?.bgColor = chatBubbleBgColorOutGoing
        }else if cell is QMChatNotificationCell {
            let message : QBChatMessage = self.chatSectionManager.message(for: indexPath)
            let messageType = message.customParameters?.object(forKey: MESSAGE_TYPE_KEY) as? String
            (cell as! QMChatNotificationCell).containerView?.bgColor = APP_THEME_LIGHT_GRAY_COLOR.withAlphaComponent(0.08)
            (cell as! QMChatNotificationCell).containerView?.highlightColor = APP_THEME_LIGHT_GRAY_COLOR.withAlphaComponent(0.2)
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any!) -> Bool {
        let item : QBChatMessage = self.chatSectionManager.message(for: indexPath)
        let viewClass : AnyClass = self.viewClass(forItem: item)! as AnyClass
        if viewClass === QMChatAttachmentIncomingCell.self || viewClass === QMChatAttachmentOutgoingCell.self {
            return false
        }
        return super.collectionView(collectionView, canPerformAction: action, forItemAt: indexPath, withSender: sender)
    }
    
    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any!) {
        if action == #selector(copy(_:)) {
            let item : QBChatMessage = self.chatSectionManager.message(for: indexPath)
            let viewClass : AnyClass = self.viewClass(forItem: item)! as AnyClass
            if viewClass === QMChatAttachmentIncomingCell.self || viewClass === QMChatAttachmentOutgoingCell.self {
                return
            }
            UIPasteboard.general.string = item.text
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let lastSection = (self.collectionView?.numberOfSections)! - 1
        if ((indexPath as NSIndexPath).section == lastSection && (indexPath as NSIndexPath).item == (self.collectionView?.numberOfItems(inSection: lastSection))! - 1) {
            guard let dialogID = self.dialog!.id else {
                logMessage("DialogID is nil")
                return super.collectionView(collectionView, cellForItemAt: indexPath)
            }
            ServicesManager.instance().chatService.loadEarlierMessages(withChatDialogID: dialogID).continue({
                [weak self] (task: BFTask!) -> AnyObject! in
                guard let strongSelf = self else { return nil }
                if task.result != nil && task.result!.count > 0 {
                    strongSelf.chatSectionManager.add(task.result as! [QBChatMessage]!)
                }
                return nil
                })
        }
        if let message = self.chatSectionManager.message(for: indexPath) {
            self.sendReadStatusForMessage(message)
        }
        return super.collectionView(collectionView, cellForItemAt: indexPath)
    }
    
    //MARK: -  Strings builder
    
    override func attributedString(forItem messageItem: QBChatMessage!) -> NSAttributedString? {
        if messageItem == nil ||  messageItem.text == nil {
            return nil
        }
        if let messageType = messageItem.customParameters?.object(forKey: MESSAGE_TYPE_KEY) as? String {
            if messageType == GroupEvents.rideGroupEvent ||
                messageType == GroupEvents.groupUpdateEvent {
                var eventDate = messageItem.dateSent
                if let eD = messageItem.customParameters?.object(forKey: "eventDate") as? String {
                    eventDate = eD.dateValueFromMilliSeconds() as Date
                }
                
                var attributesDate = Dictionary<String, AnyObject>()
                attributesDate[NSForegroundColorAttributeName] = APP_THEME_VOILET_COLOR
                attributesDate[NSFontAttributeName] = UIFont.init(name: FONT_BOLD, size: 15)
                
                var attributesMessage = Dictionary<String, AnyObject>()
                attributesMessage[NSForegroundColorAttributeName] = APP_THEME_VOILET_COLOR
                attributesMessage[NSFontAttributeName] = UIFont.init(name: FONT_REGULAR, size: 11)
                
                let date = eventDate != nil ? "\(messageTimeDateFormatter.string(from: eventDate!))\n" : ""
                let attributedString = NSMutableAttributedString(string: date+messageItem.text!)
                attributedString.addAttributes(attributesDate, range: NSMakeRange(0, date.length))
                attributedString.addAttributes(attributesMessage, range: NSMakeRange(date.length, messageItem.text!.length))
                return attributedString
            }
        }
        
        let textColor = messageItem.senderID == self.senderID ? UIColor.black : UIColor.black
        var attributes = Dictionary<String, AnyObject>()
        attributes[NSForegroundColorAttributeName] = textColor
        attributes[NSFontAttributeName] = UIFont.init(name: FONT_REGULAR, size: 14)
        let attributedString = NSAttributedString(string: messageItem.text!, attributes: attributes)
        return attributedString
    }
    
    
    override func topLabelAttributedString(forItem messageItem: QBChatMessage!) -> NSAttributedString? {
        if isNull(messageItem) || messageItem.senderID == self.senderID || self.dialog?.type == QBChatDialogType.private {
            return NSAttributedString(string: "", attributes: nil)
        }
        var attributes = Dictionary<String, AnyObject>()
        attributes[NSForegroundColorAttributeName] = UIColor.black
        attributes[NSFontAttributeName] = UIFont.init(name: FONT_REGULAR, size: 12)
        var topLabelAttributedString : NSAttributedString?
        if let sendersName = messageItem.customParameters?.object(forKey: "sendersName") as? String{
            topLabelAttributedString = NSAttributedString(string: sendersName, attributes: attributes)
            return topLabelAttributedString
        }
        let user = ServicesManager.instance().usersService.usersMemoryStorage.user(withID: messageItem.senderID)
        let topLabelText = nameToDisplay(user,chatDialogue: self.dialog)
        topLabelAttributedString = NSAttributedString(string: topLabelText, attributes: attributes)
        return topLabelAttributedString
    }
    
    
    override func bottomLabelAttributedString(forItem messageItem: QBChatMessage!) -> NSAttributedString! {
        var text = messageItem.dateSent != nil ? messageTimeDateFormatter.string(from: messageItem.dateSent!) : ""
        if messageItem.senderID == self.senderID {
            text = text + " " + self.statusStringFromMessage(messageItem)
        }
        
        var attributesNormal = Dictionary<String, AnyObject>()
        attributesNormal[NSForegroundColorAttributeName] = APP_THEME_GRAY_COLOR
        attributesNormal[NSFontAttributeName] = UIFont.init(name: FONT_REGULAR, size: 9)
        
        var attributesBlueTick = Dictionary<String, AnyObject>()
        attributesBlueTick[NSForegroundColorAttributeName] = APP_THEME_BLUE_COLOR
        attributesBlueTick[NSFontAttributeName] = UIFont.init(name: FONT_BOLD, size: 9)
        
        var attributesGrayTick = Dictionary<String, AnyObject>()
        attributesGrayTick[NSForegroundColorAttributeName] = APP_THEME_GRAY_COLOR
        attributesGrayTick[NSFontAttributeName] = UIFont.init(name: FONT_BOLD, size: 9)
        
        
        let bottomLabelAttributedString = NSMutableAttributedString(string: text)
        bottomLabelAttributedString.addAttributes(attributesNormal, range: NSMakeRange(0,text.length))
        
        bottomLabelAttributedString.replaceOccuranceOf(" ✓ ", withString: " ✓ ", attributes: attributesGrayTick as NSDictionary)
        bottomLabelAttributedString.replaceOccuranceOf(" ✓✓ ", withString: " ✓✓ ", attributes: attributesGrayTick as NSDictionary)
        bottomLabelAttributedString.replaceOccuranceOf(" ✓✓✓ ", withString: " ✓✓ ", attributes: attributesBlueTick as NSDictionary)
        bottomLabelAttributedString.replaceOccuranceOf("Failed", withString: "Not sent", attributes: attributesNormal as NSDictionary)
        
        return bottomLabelAttributedString
    }
    
    //MARK: - QMChatCellDelegate
    
    func chatCellDidTapContainer(_ cell: QMChatCell!) {
        let indexPath = self.collectionView?.indexPath(for: cell)
        if let currentMessage = self.chatSectionManager.message(for: indexPath) {
            if self.detailedCells.contains(currentMessage.id!) {
                self.detailedCells.remove(currentMessage.id!)
            } else {
                self.detailedCells.insert(currentMessage.id!)
            }
            self.collectionView?.collectionViewLayout.removeSizeFromCache(forItemID: currentMessage.id)
            self.collectionView?.performBatchUpdates(nil, completion: nil)
            if isNotNull(Dbm().getFailedQBMessage(["messageId":currentMessage.id!])) {
                self.sendMessage(currentMessage,scrollToMessage: false)
            }
        }
    }
    
    func chatCell(_ cell: QMChatCell!, didPerformAction action: Selector, withSender sender: Any!) {
    }
    
    func chatCell(_ cell: QMChatCell!, didTapAtPosition position: CGPoint) {
        
    }
    
    func chatCellDidTapAvatar(_ cell: QMChatCell!) {
    }
    
    // MARK: QMChatServiceDelegate
    
    func chatService(_ chatService: QMChatService, didAddMessageToMemoryStorage message: QBChatMessage, forDialogID dialogID: String) {
        if self.dialog?.id == dialogID {
            self.chatSectionManager.add(message)
        }
    }
    
    func chatService(_ chatService: QMChatService, didUpdateChatDialogInMemoryStorage chatDialog: QBChatDialog) {
        if self.dialog?.id == chatDialog.id {
            self.dialog = chatDialog
            self.updateTitle()
        }
    }
    
    func chatService(_ chatService: QMChatService, didUpdate message: QBChatMessage, forDialogID dialogID: String) {
        if self.dialog?.id == dialogID {
            self.chatSectionManager.update(message)
        }
    }
    
    //MARK: -  UITextViewDelegate
    override func textViewShouldBeginEditing(_ textView: UITextView) -> Bool{
        textView.autocorrectionType = .no
        return true
    }
    
    override func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if let timer = self.typingTimer {
            timer.invalidate()
            self.typingTimer = nil
        } else {
            self.sendBeginTyping()
        }
        self.typingTimer = Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(ChatViewController.fireSendStopTypingIfNecessary), userInfo: nil, repeats: false)
        return true
    }
    
    override func textViewDidEndEditing(_ textView: UITextView) {
        super.textViewDidEndEditing(textView)
        self.fireSendStopTypingIfNecessary()
    }
    
    func fireSendStopTypingIfNecessary() -> Void {
        if let timer = self.typingTimer {
            timer.invalidate()
            self.typingTimer = nil
            self.sendStopTyping()
        }
    }
    
    func sendBeginTyping() -> Void {
        if let dialog = self.dialog {
            dialog.sendUserIsTyping()
        }
    }
    
    func sendStopTyping() -> Void {
        if let dialog = self.dialog {
            dialog.sendUserStoppedTyping()
        }
    }
    
    //MARK: -  QMChatAttachmentServiceDelegate
    
    func configure(_ avatarImageView: QMImageView!, forMessage messageItem: QBChatMessage!) {
        
    }
    
    func chatAttachmentService(_ chatAttachmentService: QMChatAttachmentService, didChange status: QMMessageAttachmentStatus, for message: QBChatMessage) {
        if message.dialogID == self.dialog?.id {
            self.chatSectionManager.update(message)
        }
    }
    
    func chatAttachmentService(_ chatAttachmentService: QMChatAttachmentService, didChangeLoadingProgress progress: CGFloat, for attachment: QBChatAttachment) {
        if let attachmentCell = self.attachmentCellsMap[attachment.id!] {
            attachmentCell.updateLoadingProgress(progress)
        }
    }
    
    func chatAttachmentService(_ chatAttachmentService: QMChatAttachmentService, didChangeUploadingProgress progress: CGFloat, for message: QBChatMessage) {
        var cell = self.attachmentCellsMap[message.id!]
        if cell == nil && progress < 1.0 {
            let indexPath = self.chatSectionManager.indexPath(for: message)
            cell = self.collectionView?.cellForItem(at: indexPath!) as? QMChatAttachmentCell
            self.attachmentCellsMap[message.id!] = cell
        }
        if cell != nil {
            cell!.updateLoadingProgress(progress)
        }
    }
    
    //MARK: -  QMChatConnectionDelegate
    func refreshAndReadMessages() {
        self.loadMessages()
        if let messagesToRead = self.unreadMessages {
            self.readMessages(messagesToRead)
        }
        self.unreadMessages = nil
    }
    
    func chatServiceChatDidConnect(_ chatService: QMChatService) {
        self.refreshAndReadMessages()
    }
    
    func chatServiceChatDidReconnect(_ chatService: QMChatService) {
        self.refreshAndReadMessages()
    }
}
