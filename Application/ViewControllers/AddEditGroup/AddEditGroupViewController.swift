//
//  AddEditGroupViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit
import DZNEmptyDataSet
import RSKImageCropper

enum AEGWorkingMode {
    case createNewGroup
    case editGroup
}

class AddEditGroupViewController: UIViewController,RSKImageCropViewControllerDelegate {
    
    //MARK: - variables and constants
    @IBOutlet fileprivate weak var curvedTopBackgroundView : UIView!
    @IBOutlet             weak var groupPictureImageView : UIImageView!
    @IBOutlet             weak var groupPictureInformationLabel : UILabel!
    @IBOutlet fileprivate weak var cameraIconHolderView : UIView!
    @IBOutlet             weak var groupNameTextField : UITextField!
    @IBOutlet fileprivate weak var colorPickerColorType1 : UIView!
    @IBOutlet fileprivate weak var colorPickerColorType1CheckMark : UIButton!
    @IBOutlet fileprivate weak var colorPickerColorType2 : UIView!
    @IBOutlet fileprivate weak var colorPickerColorType2CheckMark : UIButton!
    @IBOutlet fileprivate weak var colorPickerColorType3 : UIView!
    @IBOutlet fileprivate weak var colorPickerColorType3CheckMark : UIButton!
    @IBOutlet fileprivate weak var colorPickerColorType4 : UIView!
    @IBOutlet fileprivate weak var colorPickerColorType4CheckMark : UIButton!
    @IBOutlet fileprivate weak var colorPickerColorType5 : UIView!
    @IBOutlet fileprivate weak var colorPickerColorType5CheckMark : UIButton!
    @IBOutlet fileprivate weak var colorPickerColorType6 : UIView!
    @IBOutlet fileprivate weak var colorPickerColorType6CheckMark : UIButton!
    @IBOutlet fileprivate weak var actionButton: TKTransitionSubmitButton!
    
    var workMode = AEGWorkingMode.createNewGroup
    var groupToEdit : Group?
    var pictureAddedOrUpdated = false
    var groupId : String?

    //MARK: - view controller life cycle methods
    override internal var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
        updateUserInterfaceOnScreen()
    }
    
    //MARK: - other methods
    private func setupForNavigationBar(){
        if workMode == .createNewGroup {
            setAppearanceForNavigationBarType1(self.navigationController,color:ThemeColor().getColor(type: getSelectedColor()))
            self.curvedTopBackgroundView.backgroundColor = ThemeColor().getColor(type: getSelectedColor())
            setupNavigationBarTitleType1("Create New Group",viewController: self)
        }else{
            setAppearanceForNavigationBarType1(self.navigationController,color:ThemeColor().getColor(type: getSelectedColor()))
            self.curvedTopBackgroundView.backgroundColor = ThemeColor().getColor(type: getSelectedColor())
            setupNavigationBarTitleType1("Edit Group",viewController: self)
        }
        if self.isBackButtonRequired() {
            addNavigationBarButton(self, image: UIImage(named: "backarrowblack"), title: nil, isLeft: true)
        }
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    func onClickOfLeftBarButton(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    private func registerForNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(AddEditGroupViewController.reLoadGroupObjectFromDb), name: NSNotification.Name(rawValue: NOTIFICATION_GROUPS_PLACES_UPDATED), object: nil)
    }
    
    func reLoadGroupObjectFromDb(){
        let updatedGroupObject = Dbm().getGroup(["groupId":safeString(groupId)])
        if isNotNull(updatedGroupObject) {
            self.groupToEdit = updatedGroupObject
        }
    }

    private func startUpInitialisations(){
        setAppearanceForViewController(self)
        curvedTopBackgroundView.roundCorners([UIRectCorner.bottomLeft,UIRectCorner.bottomRight], radius: 64)
        groupPictureImageView.makeMeRoundWith(borderColor: APP_THEME_YELLOW_COLOR_2, width: 2)
        cameraIconHolderView.makeMeRoundWith(borderColor: APP_THEME_YELLOW_COLOR_2, width: 0.8)
        
        if workMode == .createNewGroup {
            self.actionButton.setTitle("DONE", for: UIControlState.normal)
        }else{
            self.actionButton.setTitle("UPDATE", for: UIControlState.normal)
        }
        self.actionButton.performAppearAnimationType1()
        
        colorPickerColorType1.backgroundColor = ThemeColor.colorType1
        colorPickerColorType2.backgroundColor = ThemeColor.colorType2
        colorPickerColorType3.backgroundColor = ThemeColor.colorType3
        colorPickerColorType4.backgroundColor = ThemeColor.colorType4
        colorPickerColorType5.backgroundColor = ThemeColor.colorType5
        colorPickerColorType6.backgroundColor = ThemeColor.colorType6
        
        colorPickerColorType1.makeMeRound()
        colorPickerColorType2.makeMeRound()
        colorPickerColorType3.makeMeRound()
        colorPickerColorType4.makeMeRound()
        colorPickerColorType5.makeMeRound()
        colorPickerColorType6.makeMeRound()
        
        if workMode == .createNewGroup {
            setSelectedColor(type: 1)
        }else{
            groupNameTextField.text = safeString(groupToEdit?.name)
            groupPictureImageView.sd_setImage(with: getGroupPictureUrlFromFileName(safeString(groupToEdit?.picture)).asNSURL())
            groupPictureInformationLabel.isHidden = true
            setSelectedColor(type: ThemeColor().getIndexColorName(color: safeString(groupToEdit?.themeColor)))
        }
        self.actionButton.performAppearAnimationType1()
        groupId = safeString(groupToEdit?.groupId)
    }
    
    
    @objc private func updateUserInterfaceOnScreen(){
        if safeInt(groupNameTextField.text?.length)>0{
            actionButton.isHidden = false
        }else{
            actionButton.isHidden = true
        }
    }
    
    @IBAction private func onClickOfSetPicture(){
        if(isInternetConnectivityAvailable(true)==false){return}
        let pickerController = DKImagePickerController()
        pickerController.allowMultipleTypes=false
        pickerController.singleSelect=true
        pickerController.assetType=DKImagePickerControllerAssetType.allPhotos
        pickerController.didSelectAssets = { [weak self] (assets: [DKAsset]) in guard let `self` = self else { return }
            for asset in assets{
                asset.fetchOriginalImageWithCompleteBlock({[weak self] (_image, info) in guard let `self` = self else { return }
                    execMain({[weak self] in guard let `self` = self else { return }
                        
                        if isNotNull(_image) {
                            let image = fixrotation(_image!)
                            let imageCropperViewController = RSKImageCropViewController(image: image, cropMode: .square)
                            imageCropperViewController.avoidEmptySpaceAroundImage = true
                            imageCropperViewController.setZoomScale(1.8)
                            imageCropperViewController.delegate = self;
                            self.present(imageCropperViewController, animated: true, completion: nil)
                        }
                    })
                })
            }
        }
        self.present(pickerController, animated: true, completion: nil)
    }
    
    func imageCropViewControllerDidCancelCrop(_ controller: RSKImageCropViewController){
        self.dismiss(animated: true)
    }
    
    func imageCropViewController(_ controller: RSKImageCropViewController, didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect){
        self.dismiss(animated: true)
        execMain({[weak self]  in guard let `self` = self else { return }
            self.groupPictureImageView.image = croppedImage
            self.groupPictureInformationLabel.isHidden = true
            self.pictureAddedOrUpdated = true
            },delay: 0.6)
    }
    
    func canContinueForAction()->(Bool){
        var canContinue = true
        if canContinue {
            canContinue = validateName(groupNameTextField.text, identifier: "Group's Name")
            if canContinue == false {
                groupNameTextField.becomeFirstResponder()
            }
        }
        return canContinue
    }
    
    @IBAction func onClickOfActionButton(){
        if(isInternetConnectivityAvailable(true)==false){return}
        if canContinueForAction(){
            if workMode == .createNewGroup {
                actionButton.startLoadingAnimation()
                let information = NSMutableDictionary()
                copyData(groupNameTextField.text, destinationDictionary: information, destinationKey: "name", methodName:#function)
                copyData(getSelectedColor(), destinationDictionary: information, destinationKey: "themeColor", methodName:#function)
                if pictureAddedOrUpdated {
                    copyData(groupPictureImageView.image, destinationDictionary: information, destinationKey: "picture", methodName:#function)
                }
                copyData("create", destinationDictionary: information, destinationKey: "actionType", methodName:#function)
                let scm = ServerCommunicationManager()
                scm.manageGroup(information) { (responseData) -> () in
                    if let _ = responseData {
                        let groupId = safeString(responseData?.value(forKeyPath: "data.group_id"))
                        Acf().syncGroupsAndPlaces(completion: {
                            Acf().fetchDialogs()
                            self.actionButton.stopIt()
                            let group = Dbm().getGroup(["groupId":groupId])
                            if group != nil {
                                var viewControllers = self.navigationController!.viewControllers
                                viewControllers.removeLast()
                                let gd = getViewController("GroupDetailViewController") as! GroupDetailViewController
                                gd.group = group
                                viewControllers.append(gd)
                                self.navigationController?.setViewControllers(viewControllers, animated: true)
                            }else{
                                self.navigationController?.popViewController(animated: true)
                            }
                        })
                    }else{
                        self.actionButton.stopIt()
                    }
                }
            }else{
                actionButton.startLoadingAnimation()
                let information = NSMutableDictionary()
                copyData(groupNameTextField.text, destinationDictionary: information, destinationKey: "name", methodName:#function)
                copyData(getSelectedColor(), destinationDictionary: information, destinationKey: "themeColor", methodName:#function)
                if pictureAddedOrUpdated {
                    copyData(groupPictureImageView.image, destinationDictionary: information, destinationKey: "picture", methodName:#function)
                }
                copyData(groupToEdit?.groupId, destinationDictionary: information, destinationKey: "groupId", methodName:#function)
                copyData("update", destinationDictionary: information, destinationKey: "actionType", methodName:#function)
                let scm = ServerCommunicationManager()
                scm.manageGroup(information) { (responseData) -> () in
                    if let _ = responseData {
                        Acf().syncGroupsAndPlaces(completion: {
                            self.groupToEdit?.name = self.groupNameTextField.text
                            self.groupToEdit?.themeColor = self.getSelectedColor()
                            Dbm().saveChanges()
                            Acf().fetchDialogs()
                            self.navigationController?.popViewController(animated: true)
                            self.actionButton.stopIt()
                        })
                    }else{
                        self.actionButton.stopIt()
                    }
                }
            }
        }
    }
    
    @IBAction func onClickOfEditName(){
        groupNameTextField.becomeFirstResponder()
    }
    
    @IBAction func onClickOfSelectColor(sender:UIButton){
        performAnimatedClickEffectType1(sender)
        setSelectedColor(type: sender.tag)
        setupForNavigationBar()
    }
    
    func setSelectedColor(type:Int){
        self.colorPickerColorType1CheckMark.isHidden = !(type == 1)
        self.colorPickerColorType2CheckMark.isHidden = !(type == 2)
        self.colorPickerColorType3CheckMark.isHidden = !(type == 3)
        self.colorPickerColorType4CheckMark.isHidden = !(type == 4)
        self.colorPickerColorType5CheckMark.isHidden = !(type == 5)
        self.colorPickerColorType6CheckMark.isHidden = !(type == 6)
    }
    
    func getSelectedColor()->String{
        if !self.colorPickerColorType1CheckMark.isHidden {return ThemeColor().getThemeColorName(index: 1)}
        else if !self.colorPickerColorType2CheckMark.isHidden {return ThemeColor().getThemeColorName(index: 2)}
        else if !self.colorPickerColorType3CheckMark.isHidden {return ThemeColor().getThemeColorName(index: 3)}
        else if !self.colorPickerColorType4CheckMark.isHidden {return ThemeColor().getThemeColorName(index: 4)}
        else if !self.colorPickerColorType5CheckMark.isHidden {return ThemeColor().getThemeColorName(index: 5)}
        else if !self.colorPickerColorType6CheckMark.isHidden {return ThemeColor().getThemeColorName(index: 6)}
        else{return "1"}
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        updateUserInterfaceOnScreen()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        resignKeyboard()
        return false
    }
    
}
