//
//  WeeklyPlannerViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit

class WeeklyPlannerViewController: UIViewController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource {
    
    //MARK: - variables and constants
    @IBOutlet fileprivate weak var tableView : UITableView?
    fileprivate var plans:NSMutableArray?
    
    
    //MARK: - view controller life cycle methods
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
        fetchPlannerData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let plans = self.plans {// we are assuming , all notification are read now
            Acf().setWeeklyPlanNotificationsAsRead(plans)
        }
    }
    
    //MARK: - other methods
    private func setupForNavigationBar(){
        setAppearanceForNavigationBarType1(self.navigationController)
        setupNavigationBarTitleType1("Weekly Planner",viewController: self)
        addNavigationBarButton(self, image: UIImage(named: "close"), title: nil, isLeft: false)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    func onClickOfRightBarButton(_ sender:Any){
        Acf().hidePopupViewController()
    }
    
    private func registerForNotifications(){
        
    }
    
    private func startUpInitialisations(){
        registerNib("WeeklyPlannerDayInfoTableViewCell", tableView: self.tableView)
        setAppearanceForViewController(self)
        setAppearanceForTableView(tableView)
        self.tableView?.backgroundColor = UIColor.white
        self.tableView!.canCancelContentTouches = false
        trackScreenLaunchEvent(SLE_WEEKLY_PLANNER)
    }
    
    func fetchPlannerData() {
        func prepareDataForDisplay(){
            var plansFromServer = CacheManager.sharedInstance.loadObject("WEEKLY_RIDE_PLAN_CACHE") as? NSMutableArray
            if isNull(plansFromServer){
                plansFromServer = NSMutableArray()
            }
            let weeklyPlansTemplate = NSMutableArray()
            for i in 0...13 {
                let isMorning = (i%2 == 0)
                let date = Date().dateByAddingDays(Int(i/2))
                let plan = NSMutableDictionary()
                plan.setObject(date.weekdayToString(), forKey: "day" as NSCopying)
                plan.setObject(date.toStringValue("yyyy-MM-dd"), forKey: "date" as NSCopying)
                for p in plansFromServer! {
                    let wpDate = plan.object(forKey: "date") as? String
                    let pDate = (p as AnyObject).object(forKey: "date") as? String
                    let pEtd = (p as AnyObject).object(forKey: "etd") as? String
                    let mode = (p as AnyObject).object(forKey: "mode") as! NSString
                    if isNotNull(pDate) && isNotNull(pEtd) && isNotNull(mode){
                        if isMorning{
                            if (pDate as! NSString).isEqual(to: wpDate!) && mode.isEqual(to: "0"){
                                if mode.isEqual(to: "0"){
                                    plan.removeAllObjects()
                                    plan.addEntries(from: p as! [AnyHashable: Any])
                                    break
                                }
                            }
                        }else{
                            if (pDate as! NSString).isEqual(to: wpDate!) && mode.isEqual(to: "1"){
                                plan.removeAllObjects()
                                plan.addEntries(from: p as! [AnyHashable: Any])
                                break
                            }
                        }
                    }
                }
                weeklyPlansTemplate.add(plan)
            }
            self.plans = weeklyPlansTemplate
            Acf().checkAndUpdateRequiredWeeklyViewNotifications(plans!)
            self.tableView?.reloadData()
        }
        if isSystemReadyToProcessThis() {
            trackFunctionalEvent(FE_MANUAL_REFRESH_TRAVELLERS,information:nil)
            tableView?.showActivityIndicator()
            let scm = ServerCommunicationManager()
            scm.responseErrorOption = .dontShowErrorResponseMessage
            scm.getWeeklyRidePlan([:]) { (responseData) -> () in
                if let _ = responseData {
                    let plansFromServer = responseData?.object(forKey: "data") as? NSMutableArray
                    CacheManager.sharedInstance.saveObject(plansFromServer, identifier: "WEEKLY_RIDE_PLAN_CACHE")
                    prepareDataForDisplay()
                }
                self.tableView?.hideActivityIndicator()
            }
        }
        prepareDataForDisplay()
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        
    }
    
    //MARK: - UITableView Delegate & Data Source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isNotNull(plans){
            return plans!.count/2
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 120
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WeeklyPlannerDayInfoTableViewCell") as? WeeklyPlannerDayInfoTableViewCell
        cell?.plan1 = plans?.object(at: 2*(indexPath as NSIndexPath).row) as? NSMutableDictionary
        cell?.plan2 = plans?.object(at: 2*(indexPath as NSIndexPath).row+1) as? NSMutableDictionary
        cell?.updateUserInterfaceOnScreen()
        cell?.rideStatusButton1?.tag = (2*(indexPath as NSIndexPath).row)
        cell?.rideStatusButton2?.tag = (2*(indexPath as NSIndexPath).row+1)
        cell?.viewButton1?.tag = (2*(indexPath as NSIndexPath).row)
        cell?.viewButton2?.tag = (2*(indexPath as NSIndexPath).row+1)
        cell?.rideStatusButton1?.addTarget(self, action: #selector(WeeklyPlannerViewController.onClickOfRideStatusButton(_:)), for: .touchUpInside)
        cell?.rideStatusButton2?.addTarget(self, action: #selector(WeeklyPlannerViewController.onClickOfRideStatusButton(_:)), for: .touchUpInside)
        cell?.viewButton1?.addTarget(self, action: #selector(WeeklyPlannerViewController.onClickOfViewDetailButton(_:)), for: .touchUpInside)
        cell?.viewButton2?.addTarget(self, action: #selector(WeeklyPlannerViewController.onClickOfViewDetailButton(_:)), for: .touchUpInside)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func onClickOfViewDetailButton(_ sender:UIButton) {
        NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_RESET_IDLE_TIMER_RIDE_HOME), object: nil, userInfo: nil)
        performAnimatedClickEffectType1(sender)
        let plan = plans?.object(at: sender.tag) as! NSDictionary
        if Acf().isAddressInputScreenNeeded() == false {
            if isSystemReadyToProcessThis() {
                let isPlannerSetForTheDay = isNotNull(plan.object(forKey: "mode")) && isNotNull(plan.object(forKey: "etd"))
                if isPlannerSetForTheDay {
                    let selectedDayDate = (plan.object(forKey: "date") as! String).dateValueType1()
                    let selectedTimeDate = getDate(plan.object(forKey: "etd") as? String)
                    let mode = plan.object(forKey: "mode") as! String
                    Acf().openRideHomePage(selectedDayDate, timeDate: selectedTimeDate,mode:mode)
                }else{
                    showNotification(MESSAGE_TEXT___SET_RIDE_TIME_FIRST, showOnNavigation: false, showAsError: true)
                    let settings = Dbm().getSetting()
                    let trueForHomeToDestinationFalseforDestinationToHome = (sender.tag%2 == 0)
                    let selectedDayDate = (plan.object(forKey: "date") as! String).dateValueType1()
                    var selectedTimeDate = Date()
                    if trueForHomeToDestinationFalseforDestinationToHome {
                        selectedTimeDate = settings.homeTime!.dateValue() as Date
                    }else{
                        selectedTimeDate = settings.destinationTime!.dateValue() as Date
                    }
                    let selectedDateAndTimeCombined = Date.date(selectedDayDate.year(), month: selectedDayDate.month(), day: selectedDayDate.day(), hour: selectedTimeDate.hour(), minute: selectedTimeDate.minute(), second: selectedTimeDate.second)
                    showDateTimePicker(selectedDateAndTimeCombined, mode: .dateAndTime) { (selectedDateAndTime) in
                        if selectedDateAndTime.isInPast() {
                            showNotification("Please select future date", showOnNavigation: false, showAsError: true)
                        }else{
                            self.updateUsersRideRelatedSelection(selectedDateAndTime, selectedDayTime: selectedDateAndTime,updatePlanner: true)
                        }
                    }
                }
            }
        }
    }
    
    func updateUsersRideRelatedSelection(_ selectedDayDate : Date , selectedDayTime : Date , updatePlanner:Bool){
        if(isInternetConnectivityAvailable(true)==false){return}
        var trueForHomeToDestinationFalseforDestinationToHome = true
        if Acf().estimateUserExpectedTravelModeForDate(date: selectedDayTime) == .morning{
            trueForHomeToDestinationFalseforDestinationToHome = true
        }else{
            trueForHomeToDestinationFalseforDestinationToHome = false
        }
        let settings = Dbm().getSetting()
        if trueForHomeToDestinationFalseforDestinationToHome {
            settings.homeTime = selectedDayTime.stringValue()
        }else{
            settings.destinationTime = selectedDayTime.stringValue()
        }
        Dbm().saveChanges()
        let userInfo = Dbm().getUserInfo()
        let trueForOwnerFalseforPassenger = settings.selection1CachedValue!.intValue == 0
        let information = NSMutableDictionary()
        copyData(userInfo!.userId , destinationDictionary: information, destinationKey: "userId", methodName: #function)
        copyData(NSNumber(value: trueForOwnerFalseforPassenger as Bool), destinationDictionary: information, destinationKey: "isOwner", methodName: #function)
        copyData(NSNumber(value: !trueForHomeToDestinationFalseforDestinationToHome), destinationDictionary: information, destinationKey: "mode", methodName: #function)
        copyData(selectedDayTime.stringTimeOnly24HFormatValue() , destinationDictionary: information, destinationKey: "time", methodName: #function)
        copyData(selectedDayDate.toStringValue("yyyy-MM-dd") , destinationDictionary: information, destinationKey: "date", methodName: #function)
        copyData(true , destinationDictionary: information, destinationKey: "autoFixEnabled", methodName: #function)
        copyData("top" , destinationDictionary: information, destinationKey: "autoFixType", methodName: #function)
        updateTravelPlan(information, selectedDayDate: selectedDayDate, selectedDayTime: selectedDayTime, isHomeToDestination:trueForHomeToDestinationFalseforDestinationToHome, updatePlanner: updatePlanner,completion: { (selectedDayDate, selectedDayTime, isHomeToDestination) -> () in
            self.fetchPlannerData()
        })
    }
    
    func onClickOfRideStatusButton(_ sender:TKTransitionSubmitButton) {
        NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_RESET_IDLE_TIMER_RIDE_HOME), object: nil, userInfo: nil)
        let plan = plans?.object(at: sender.tag) as! NSDictionary
        let situation = Acf().getTravelRequestStatus(plan)
        if situation == .notInitialisedYet ||
            situation == .travelRequestRejectedByMe {// action is fix my ride
            if(isInternetConnectivityAvailable(true)==false){return}
            if Acf().isAddressInputScreenNeeded() == false {
                if isSystemReadyToProcessThis() {
                    let isPlannerSetForTheDay = isNotNull(plan.object(forKey: "mode")) && isNotNull(plan.object(forKey: "etd"))
                    if isPlannerSetForTheDay {
                        let userInfo = Dbm().getUserInfo()
                        let settings = Dbm().getSetting()
                        var via = "top"
                        let selectedDayDate = (plan.object(forKey: "date") as! String).dateValueType1()
                        func performFixMyRideAction(){
                            let information = ["userId":loggedInUserId(),
                                               "date":selectedDayDate.toStringValue("yyyy-MM-dd"),
                                               "mode":plan.object(forKey: "mode") as! String,
                                               "via":via
                                ] as NSDictionary
                            sender.normalBackgroundColor = APP_THEME_VOILET_COLOR
                            sender.startLoadingAnimation()
                            let scm = ServerCommunicationManager()
                            scm.responseErrorOption = ResponseErrorOption.dontShowErrorResponseMessage
                            scm.setMyRide(information) { (responseData) -> () in
                                if let _ = responseData {
                                    if let messageToShow = responseData?.object(forKey: "message") as? String {
                                        if messageToShow.contains("sent"){
                                            showPopupAlertMessage("Fix my ride", message: messageToShow, messageType: .success)
                                        }else{
                                            showPopupAlertMessage("Fix my ride", message: messageToShow, messageType: .error)
                                        }
                                    }
                                }
                                sender.stopIt()
                                sender.normalBackgroundColor = UIColor.clear
                                self.fetchPlannerData()
                            }
                            trackFunctionalEvent(FE_FIX_MY_RIDE,information: ["mobileNo":userInfo!.phone!,"From":"Weekly Planner"])
                        }
                        func showAlert(){
                            if let ridesDone = Dbm().getUserInfo()!.ridesDone?.toInt() as Int?{
                                //we done want to show confirmation pop up for new users
                                if ridesDone < MINIMUM_RIDE_COUNT_FOR_ADVANCED_USER{
                                    performFixMyRideAction();return;
                                }
                            }
                            var dayText = "on \(selectedDayDate.toStringValue("dd MMM"))"
                            if selectedDayDate.isToday(){
                                dayText = "Today"
                            }else if selectedDayDate.isTomorrow(){
                                dayText = "Tomorrow"
                            }
                            var sourceToDestination = "Home to \(destinationCapitalizedName())"
                            if (plan.object(forKey: "mode") as! NSString).isEqual(to: "1") {
                                sourceToDestination = "\(destinationCapitalizedName()) to Home"
                            }
                            let time = getTimeFormatInAmPm(plan.object(forKey: "etd") as? String)
                            var travelAs = "Car Owner"
                            if (plan.object(forKey: "travel_status") as! NSString).isEqual(to: "0") {
                                travelAs = "Passenger"
                            }
                            let message = "\nPlease confirm !\nYou want to ride \(dayText) \nat \(time)\nfrom \(sourceToDestination) \nas \(travelAs)"
                            let prompt = UIAlertController(title: "Fix my ride", message: message, preferredStyle: UIAlertControllerStyle.alert)
                            prompt.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive, handler: nil))
                            prompt.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.cancel, handler: { (action) -> Void in
                                performFixMyRideAction()
                            }))
                            present(prompt, animated: true, completion: nil)
                        }
                        showAlert()
                    }else{
                        showNotification(MESSAGE_TEXT___SET_RIDE_TIME_FIRST, showOnNavigation: false, showAsError: true)
                        let settings = Dbm().getSetting()
                        let trueForHomeToDestinationFalseforDestinationToHome = (sender.tag%2 == 0)
                        let selectedDayDate = (plan.object(forKey: "date") as! String).dateValueType1()
                        var selectedTimeDate = Date()
                        if trueForHomeToDestinationFalseforDestinationToHome {
                            selectedTimeDate = settings.homeTime!.dateValue() as Date
                        }else{
                            selectedTimeDate = settings.destinationTime!.dateValue() as Date
                        }
                        let selectedDateAndTimeCombined = Date.date(selectedDayDate.year(), month: selectedDayDate.month(), day: selectedDayDate.day(), hour: selectedTimeDate.hour(), minute: selectedTimeDate.minute(), second: selectedTimeDate.second)
                        showDateTimePicker(selectedDateAndTimeCombined, mode: .dateAndTime) { (selectedDateAndTime) in
                            if selectedDateAndTime.isInPast() {
                                showNotification("Please select future date", showOnNavigation: false, showAsError: true)
                            }else{
                                self.updateUsersRideRelatedSelection(selectedDateAndTime, selectedDayTime: selectedDateAndTime,updatePlanner: true)
                            }
                        }
                    }
                }
            }
        }
    }
}

//MARK: - other functions
