//
//  SigninViewController.swift
//  Orahi
//
//  Created by Alok Singh on 10/02/16.
//  Copyright (c) 2016 Orahi. All rights reserved.
//


import UIKit

class RideViewController: UIViewController,UITextFieldDelegate {
    
    //MARK: - variables and constants
    
    //MARK: - view controller life cycle methods
    override func prefersStatusBarHidden() -> Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.navigationBarHidden)!
        }else{
            return false
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startupInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        resignKeyboard()
    }
    
    //MARK: - other methods
    func setupForNavigationBar(){
        setAppearanceForNavigationBarType1(self.navigationController)
        self.navigationController?.navigationBar.hidden = true
    }
    
    func registerForNotifications(){
    }
    
    func startupInitialisations(){
        setAppearanceForViewController(self)
        trackScreen(humanReadableScreenName("\(self.dynamicType)"));
    }
    
    func updateUserInterfaceOnScreen(){
    }
    
    //MARK: - other functions
    
    func onClickOfLeftBarButton(sender:AnyObject){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func onClickOfSideMenuButton(sender:AnyObject){
        AppCommonFunctions.sharedInstance.sideMenuController?.presentLeftMenuViewController()
    }

    
}
