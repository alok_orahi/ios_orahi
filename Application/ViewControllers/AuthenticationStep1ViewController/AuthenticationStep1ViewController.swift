//
//  AuthenticationStep1ViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit

enum AS1WorkingMode {
    case signIn
    case signUp
}

class AuthenticationStep1ViewController: UIViewController {
    
    //MARK: - variables and constants
    @IBOutlet fileprivate weak var infoLabel: UILabel!
    @IBOutlet fileprivate weak var phoneNumberTextfield: MKTextField!
    @IBOutlet fileprivate weak var actionButtonType1: TKTransitionSubmitButton!
    @IBOutlet fileprivate weak var actionButtonType2: UIButton!
    
    var workingMode = AS1WorkingMode.signIn
    var animationRequired = false
    
    //MARK: - view controller life cycle methods
    
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
    }
    
    //MARK: - other methods
    private func setupForNavigationBar(){
        setAppearanceForNavigationBarType2(self.navigationController)
        if self.isBackButtonRequired() {
            addNavigationBarButton(self, image: UIImage(named: "backarrowblack"), title: nil, isLeft: true,forceWhiteTint:false)
        }else{
            self.navigationItem.leftBarButtonItem = nil
            self.navigationItem.setHidesBackButton(true, animated:false)
        }
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    private func registerForNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(AuthenticationStep1ViewController.updateAppearanceOfTextField), name: NSNotification.Name.UITextViewTextDidBeginEditing, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AuthenticationStep1ViewController.updateAppearanceOfTextField), name: NSNotification.Name.UITextViewTextDidEndEditing, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AuthenticationStep1ViewController.updateAppearanceOfTextField), name: NSNotification.Name.UITextFieldTextDidBeginEditing, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AuthenticationStep1ViewController.updateAppearanceOfTextField), name: NSNotification.Name.UITextFieldTextDidEndEditing, object: nil)
    }
    
    private func startUpInitialisations(){
        setAppearanceForViewController(self)
        setAppearanceForMKTextField(phoneNumberTextfield)
        updateAppearanceOfTextField()

        if workingMode == .signIn{
            infoLabel.attributedText = getAttributedTitleStringType1("LOGIN")
            actionButtonType2.setTitle("Not an Orahi User? Register.", for: UIControlState.normal)
            trackScreenLaunchEvent(SLE_SIGN_IN)
        }else if workingMode == .signUp {
            infoLabel.attributedText = getAttributedTitleStringType1("REGISTER AS A NEW USER")
            actionButtonType2.setTitle("Already a user? Login.", for: UIControlState.normal)
            trackScreenLaunchEvent(SLE_SIGN_UP)
        }
                
        if animationRequired{
            self.actionButtonType1.performAppearAnimationType1()
        }
        Acf().setupDeveloperSecretOption(self.view)
    }
    
    @objc private func updateUserInterfaceOnScreen(){
    }
    
    func onClickOfLeftBarButton(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - other functions
    func canContinueForAuthentication()->(Bool){
        var canContinue = true
        if canContinue {
            canContinue = validatePhone(phoneNumberTextfield.text, identifier: "Mobile number")
            if canContinue == false {
                phoneNumberTextfield.becomeFirstResponder()
            }
        }
        return canContinue
    }
    
    @IBAction func onClickOfActionButtonType1(){
        if(isInternetConnectivityAvailable(true)==false){return}
        if canContinueForAuthentication(){
            if workingMode == .signIn {
                trackScreenLaunchEvent(FE_SIGNIN_MOBILE_ENTERED)
            }else{
                trackScreenLaunchEvent(FE_SIGNUP_MOBILE_ENTERED)
            }
            Acf().pushVC("AuthenticationStep2ViewController", navigationController: self.navigationController, isRootViewController: false, animated: true, modifyObject: { (viewControllerObject) in
                (viewControllerObject as! AuthenticationStep2ViewController).phoneNumber = self.phoneNumberTextfield.text!
                (viewControllerObject as! AuthenticationStep2ViewController).sendOTPOnStart = true
                (viewControllerObject as! AuthenticationStep2ViewController).verificationMode = self.workingMode
            })
        }
    }
    
    @IBAction func onClickOfActionButtonType2(){
        if workingMode == .signIn {
            let viewController = getViewController("AuthenticationStep1ViewController") as! AuthenticationStep1ViewController
            viewController.workingMode = .signUp
            self.navigationController!.pushViewController(viewController, animated: false)
            UIView.transition(from: self.view, to: viewController.view, duration: 1.0, options: UIViewAnimationOptions.transitionFlipFromLeft, completion: nil)
        }else if workingMode == .signUp {
            let viewController = getViewController("AuthenticationStep1ViewController") as! AuthenticationStep1ViewController
            viewController.workingMode = .signIn
            self.navigationController!.pushViewController(viewController, animated: false)
            UIView.transition(from: self.view, to: viewController.view, duration: 1.0, options: UIViewAnimationOptions.transitionFlipFromRight, completion: nil)
        }
    }
    
    @IBAction func onClickOfTermsAndConditions(){
        Acf().showTermsAndConditionsScreen()
    }
    
    func updateAppearanceOfTextField(){
        Acf().updateAppearanceOfTextFieldType1(phoneNumberTextfield)
    }
}
