//
//  RideHomeViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit
import MapKit
import DZNEmptyDataSet

class RideHomeViewController: UIViewController,UITextFieldDelegate,MKMapViewDelegate,UIGestureRecognizerDelegate,UITableViewDataSource,UITableViewDelegate,UIViewControllerPreviewingDelegate {
    //MARK: - variables and constants
    @IBOutlet fileprivate weak var infoLabel1: UILabel! // date
    @IBOutlet fileprivate weak var infoLabel2: UILabel! // home to destination / destination to home
    @IBOutlet fileprivate weak var infoLabel3: UILabel! // time in am/pm
    @IBOutlet fileprivate weak var rideDetailButton: UIButton!
    @IBOutlet fileprivate weak var rideChatButton: UIButton!
    @IBOutlet fileprivate weak var changeModeButton: UIButton!
    @IBOutlet fileprivate weak var refreshButton: UIButton!
    @IBOutlet fileprivate weak var notificationButton: UIButton!
    @IBOutlet fileprivate weak var headerView1: UIView!
    @IBOutlet fileprivate weak var headerView2: UIView!
    @IBOutlet fileprivate weak var tableView: UITableView!
    
    weak var tab1ParentViewController: Tab1ViewController?
    weak var rideDetailsViewController: RideDetailViewController?
    var selectedDayDate = Date()
    var selectedDayTime = Date()
    var trueForOwnerFalseforPassenger = false
    var trueForHomeToDestinationFalseforDestinationToHome = false
    
    fileprivate var setMyRideButton: TKTransitionSubmitButton!
    fileprivate var mapView: MKMapView!
    fileprivate var users : NSMutableArray?
    fileprivate var isRideDetailsModified = false
    fileprivate var referenceLocationCoordinate = CLLocationCoordinate2DMake(22.387868768, 77.1267576)
    fileprivate var annotationHome : MKPointAnnotation?
    fileprivate var annotationDestination : MKPointAnnotation?
    fileprivate var isComingFirstTime = true
    fileprivate var lastDateWhenEnteredBackground = Date()
    fileprivate var animateAnnotations = true
    fileprivate var cleanLoadOnAppear = false
    
    // next ride related variable
    fileprivate var usersForRide = NSMutableArray()
    fileprivate var trueIfLessThanOneHourLeft = false
    fileprivate var dateOfRide = Date()
    fileprivate var rideStatus = RideStatus.none
    fileprivate var mapViewGapFromBottom = 0.0 as CGFloat
    
    //MARK: - view controller life cycle methods
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if cleanLoadOnAppear {
            loadCleanPrivate()
            cleanLoadOnAppear = false
        }
        performUpdateIfRequired()
    }
    
    func performUpdateIfRequired() {
        headerView1.setNeedsLayout()
        headerView1.layoutIfNeeded()
        UserTrackingManager.sharedInstance.startTrackingProcessIfNeeded()
        loadUsersOnMap()
        updateNotificationBadgeCountOnNotificationButton()
    }
    
    private func setupForNavigationBar(){
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    private func registerForNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(RideHomeViewController.loadUsersOnMap), name: NSNotification.Name(rawValue: NOTIFICATION_ADDRESS_AND_TIMING_UPDATED), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(RideHomeViewController.loadUsersOnMap), name: NSNotification.Name(rawValue: NOTIFICATION_NEED_USER_UPDATE), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(RideHomeViewController.updateUserInterfaceOnScreen), name: NSNotification.Name(rawValue: NOTIFICATION_ADDRESS_AND_TIMING_UPDATED), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(RideHomeViewController.updateUserInterfaceOnScreen), name: NSNotification.Name(rawValue: NOTIFICATION_PUSH_NOTIFICATION_RECEIVED), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(RideHomeViewController.loadUsersOnMap), name: NSNotification.Name(rawValue: NOTIFICATION_PUSH_NOTIFICATION_RECEIVED), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(RideHomeViewController.resetTimerToSetCurrentRide), name: NSNotification.Name(rawValue: NOTIFICATION_RESET_IDLE_TIMER_RIDE_HOME), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(RideHomeViewController.appDidEnterBackground), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(RideHomeViewController.appWillEnterForeground), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(RideHomeViewController.statusBarFrameDidChange), name: NSNotification.Name.UIApplicationDidChangeStatusBarFrame, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(RideHomeViewController.loadUsersOnMap), name: NSNotification.Name(rawValue: NOTIFICATION_DRN_FINISHED), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(RideHomeViewController.hideThingsToViewMapInBetterWay), name: NSNotification.Name(rawValue: NOTIFICATION_HIDE_FOR_MAP_VIEW), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(RideHomeViewController.updateNotificationBadgeCountOnNotificationButton), name: NSNotification.Name(rawValue: NOTIFICATION_IN_APP_NOTIFICATION_COUNT_UPDATED), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(RideHomeViewController.setTravellerStateToSuccess(notification:)), name: NSNotification.Name(rawValue: NOTIFICATION_SET_TRAVELLER_LAST_ACTION_STATE_TO_SUCCESS), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(RideHomeViewController.loadClean), name: NSNotification.Name(rawValue: NOTIFICATION_DO_CLEAN_REFRESH), object: nil)
    }
    
    func statusBarFrameDidChange(_ frame:CGRect){
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
    }
    
    private func startUpInitialisations(){
        setupTableView()
        setAppearanceForViewController(self)
        setupForMapView()
        execMain ({ (returnedData) in
            Acf().askForPermissionsAndUpdateUserLocationOnMap(self.mapView)
        },delay: 5)
        rideDetailButton.layer.masksToBounds = false
        setBorder(rideChatButton, color: APP_THEME_VOILET_COLOR, width: 1, cornerRadius: rideChatButton.frame.size.height/2)
        setBorder(rideDetailButton, color: UIColor.white, width: 1, cornerRadius: 3)
        rideDetailButton.setTitleColor(UIColor.white, for:UIControlState.normal)
        Acf().rideHomeVC = self
        trackScreenLaunchEvent(SLE_RIDE_HOME)
        resetTimerToSetCurrentRide()
        setupForFixMyRideButton()
        setupFor3DTouchPreviewIfAvailable()
    }
    
    private func setupTableView(){
        registerNib("UserMiniProfileTableViewCell", tableView: tableView)
        tableView!.separatorInset = UIEdgeInsets.zero
        tableView!.separatorStyle = UITableViewCellSeparatorStyle.none
        tableView!.layoutMargins = UIEdgeInsets.zero
        setFooterView(tableView: self.tableView,100)
        execMain({
            self.tableView.addParallax(with: self.mapView, andHeight: DEVICE_HEIGHT - self.mapViewGapFromBottom)
        }, delay: 0.3)
    }
    
    func setupForMapView() {
        mapView = MKMapView(x: 0, y: 0, w: DEVICE_WIDTH, h: DEVICE_HEIGHT - self.mapViewGapFromBottom)
        mapView.showsUserLocation = true
        mapView.delegate = self
        let x = safeDouble(CacheManager.sharedInstance.loadObject("mapX"))
        let y = safeDouble(CacheManager.sharedInstance.loadObject("mapY"))
        let w = safeDouble(CacheManager.sharedInstance.loadObject("mapW"))
        let h = safeDouble(CacheManager.sharedInstance.loadObject("mapH"))
        if x > 0.0 {
            self.mapView.setVisibleMapRect(MKMapRectMake(x, y, w, h), animated: false)
        }
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(self.didDragMap(_:)))
        panGesture.delegate = self
        mapView.addGestureRecognizer(panGesture)
    }
    
    //MARK: - 3D Touch Setup and Support
    
    func setupFor3DTouchPreviewIfAvailable() {
        execMain ({ (returnedData) in
            if self.traitCollection.forceTouchCapability == .available {
                self.registerForPreviewing(with: self, sourceView: self.tableView!)
            }
        },delay:2)
    }
    
    func setupForFixMyRideButton(){
        execMain ({ (returnedData) in
            self.setMyRideButton = TKTransitionSubmitButton(frame: CGRect(x: (DEVICE_WIDTH - 173)/2, y: DEVICE_HEIGHT - 108 , width: 173 , height: 32))
            self.setMyRideButton.setBackgroundColor(APP_THEME_VOILET_COLOR, forState: UIControlState.normal)
            self.setMyRideButton.setBackgroundColor(APP_THEME_VOILET_COLOR.withAlphaComponent(0.9), forState: .highlighted)
            self.setMyRideButton.setTitle("FIX MY RIDE", for: UIControlState.normal)
            self.setMyRideButton.addTarget(self, action: #selector(RideHomeViewController.onClickOfFixMyRideButton), for: .touchUpInside)
            self.setMyRideButton.titleLabel?.font = UIFont(name: FONT_LIGHT, size: 16)
            self.view.addSubview(self.setMyRideButton)
            self.setMyRideButton.layer.opacity = 0.9
            self.setMyRideButton.layer.masksToBounds = false
            self.setMyRideButton.layer.borderWidth = 0.0
            self.setMyRideButton.layer.shadowColor = APP_THEME_GRAY_COLOR.cgColor
            self.setMyRideButton.layer.shadowOpacity = 0.8
            self.setMyRideButton.layer.shadowRadius = 12
            self.setMyRideButton.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
            self.setMyRideButton.performAppearAnimationType1()
        },delay: 0.4)
    }
    
    //MARK: - other methods
    
    @objc private func updateUserInterfaceOnScreen(){
        let settings = Dbm().getSetting()
        if isNull(settings.homeTime) || isNull(settings.destinationTime) || isNull(settings.homeAddress)
            || isNull(settings.destinationAddress){
            /**
             *
             * If user haven't entered any information before.
             *
             **/
        }else{
            if isRideDetailsModified == false {
                /**
                 *
                 * User havn't selected any specific time and date , so lets show time and date based on
                 * probability of what user wants to see.
                 *
                 **/
                let estimate = Acf().estimateUserExpectedTravelMode()
                if (estimate == .morning){
                    /**
                     *
                     * as per current time , user might want to see todays morning ride
                     * so lets set it
                     *
                     **/
                    selectedDayDate = Date()
                    selectedDayTime = settings.homeTime!.dateValue() as Date
                    trueForHomeToDestinationFalseforDestinationToHome = true
                    if let time = Acf().getTodaysEstimatedTimeForModeAsPerWeeklyPlanner(trueForHomeToDestinationFalseforDestinationToHome){
                        selectedDayTime = time
                    }
                    
                    /**
                     *
                     * but wait , lets check if user has already completed morning ride , if yes then he might
                     * not want to see the same again
                     *
                     **/
                    if Acf().isTodaysRideCompleted(trueForHomeToDestinationFalseforDestinationToHome){
                        /**
                         *
                         * yes , user completed his morning ride
                         * so ,let put todays evening time for user
                         *
                         **/
                        
                        selectedDayDate = Date()
                        selectedDayTime = settings.destinationTime!.dateValue() as Date
                        trueForHomeToDestinationFalseforDestinationToHome = false
                        if let time = Acf().getTodaysEstimatedTimeForModeAsPerWeeklyPlanner(trueForHomeToDestinationFalseforDestinationToHome){
                            selectedDayTime = time
                        }
                        
                        /**
                         *
                         * but wait , lets check if user has already completed evening ride , if yes then he might
                         * not want to see the same again
                         *
                         **/
                        if Acf().isTodaysRideCompleted(trueForHomeToDestinationFalseforDestinationToHome){
                            /**
                             *
                             * yes , user completed his morning ride
                             * so ,let put tommorows morning time for user
                             *
                             **/
                            selectedDayDate = Date.tomorrow()
                            selectedDayTime = settings.homeTime!.dateValue() as Date
                            trueForHomeToDestinationFalseforDestinationToHome = true
                            if let time = Acf().getTomorrowsEstimatedTimeForModeAsPerWeeklyPlanner(trueForHomeToDestinationFalseforDestinationToHome){
                                selectedDayTime = time
                            }
                        }
                    }
                }else if (estimate == .evening){
                    /**
                     *
                     * as per current time , user might want to see todays evening ride
                     * so lets set it
                     *
                     **/
                    selectedDayDate = Date()
                    selectedDayTime = settings.destinationTime!.dateValue() as Date
                    trueForHomeToDestinationFalseforDestinationToHome = false
                    if let time = Acf().getTodaysEstimatedTimeForModeAsPerWeeklyPlanner(trueForHomeToDestinationFalseforDestinationToHome){
                        selectedDayTime = time
                    }
                    
                    /**
                     *
                     * but wait , lets check if user has already completed evening ride , if yes then he might
                     * not want to see the same again
                     *
                     **/
                    if Acf().isTodaysRideCompleted(trueForHomeToDestinationFalseforDestinationToHome){
                        /**
                         *
                         * yes , user completed his evening ride
                         * so ,let put tommorows morning time for user
                         *
                         **/
                        selectedDayDate = Date.tomorrow()
                        selectedDayTime = settings.homeTime!.dateValue() as Date
                        trueForHomeToDestinationFalseforDestinationToHome = true
                        if let time = Acf().getTomorrowsEstimatedTimeForModeAsPerWeeklyPlanner(trueForHomeToDestinationFalseforDestinationToHome){
                            selectedDayTime = time
                        }
                    }
                }
                trueForOwnerFalseforPassenger = settings.selection1CachedValue!.intValue == 0
            }else{
                /**
                 *
                 * no need to change , lets keep it as it is , i mean whatever user has modified it last time
                 *
                 **/
            }
        }
        checkAndProcessNextRideIfAny()
        updateInformationLabels()
        updateNotificationBadgeCountOnNotificationButton()
    }
    
    func updateNotificationBadgeCountOnNotificationButton() {
        updateNotificationBadge(notificationButton: self.notificationButton,ax:-2,ay:2)
    }
    
    func updateInformationLabels(){
        let settings = Dbm().getSetting()
        if isNull(settings.homeTime) || isNull(settings.destinationTime) || isNull(settings.homeAddress)
            || isNull(settings.destinationAddress){// if user haven't entered any information before.
            setupRideInfoLabels(infoLabel1, l2: infoLabel2, l3: infoLabel3,day: selectedDayDate, time: selectedDayTime, towardsDestination: nil, isOwner: nil,changeModeButton:changeModeButton)
        }else{
            if isSystemReadyToProcessThis(){
                setupRideInfoLabels(infoLabel1, l2: infoLabel2, l3: infoLabel3,day: selectedDayDate, time: selectedDayTime, towardsDestination: trueForHomeToDestinationFalseforDestinationToHome, isOwner: trueForOwnerFalseforPassenger,changeModeButton:changeModeButton)
            }else{
                setupRideInfoLabels(infoLabel1, l2: infoLabel2, l3: infoLabel3, day: selectedDayDate, time: selectedDayTime, towardsDestination: trueForHomeToDestinationFalseforDestinationToHome, isOwner: nil,changeModeButton:changeModeButton)
            }
        }
        updateNotificationBadgeCountOnNotificationButton()
    }
    
    func trueForRideWaitingFalseforStarted() -> Bool {
        return  Date().isEarlierThanDate(dateOfRide)
    }
    
    
    //MARK: - other functions
    
    @IBAction func onClickOfSideMenuButton(_ sender:Any){
        Acf().sideMenuController?.presentLeftMenuViewController()
    }
    
    func loadUsing(_ dayDate:Date,timeDate:Date,mode:String) {
        self.selectedDayTime = timeDate
        self.selectedDayDate = dayDate
        self.trueForHomeToDestinationFalseforDestinationToHome = (mode as NSString).isEqual(to: "0")
        let settings = Dbm().getSetting()
        self.trueForOwnerFalseforPassenger = settings.selection1CachedValue!.intValue == 0
        self.isRideDetailsModified = true
        self.updateUserInterfaceOnScreen()
        self.loadUsersOnMap()
    }
    
    func onClickOfFixMyRideButton(skipAlert:Bool=false){
        if !isSystemReadyToProcessThis() { return }
        if(isInternetConnectivityAvailable(true)==false){return}
        let userInfo = Dbm().getUserInfo()
        trackFunctionalEvent(FE_FIX_MY_RIDE,information: ["mobileNo":userInfo!.phone!,"From":"Ride Screen"])
        var via = "top"
        func showAlert(){
            if let ridesDone = Dbm().getUserInfo()!.ridesDone?.toInt() as Int?{
                //we dont want to show confirmation pop up for new users
                if ridesDone < MINIMUM_RIDE_COUNT_FOR_ADVANCED_USER || skipAlert {
                    performFixMyRideAction();return;
                }
            }
            var dayText = "on \(selectedDayDate.toStringValue("dd MMM"))"
            if selectedDayDate.isToday(){
                dayText = "Today"
            }else if selectedDayDate.isTomorrow(){
                dayText = "Tomorrow"
            }
            var sourceToDestination = "Home to \(destinationCapitalizedName())"
            if trueForHomeToDestinationFalseforDestinationToHome == false {
                sourceToDestination = "\(destinationCapitalizedName()) to Home"
            }
            let time = selectedDayTime.toStringValue("h:mm a")
            
            var travelAs = "Car Owner"
            if trueForOwnerFalseforPassenger == false {
                travelAs = "Passenger"
            }
            let message = "\nPlease confirm !\nYou want to ride \(dayText) \nat \(time)\nfrom \(sourceToDestination) \nas \(travelAs)"
            let prompt = UIAlertController(title: "Fix my ride", message: message, preferredStyle: UIAlertControllerStyle.alert)
            prompt.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive, handler: nil))
            prompt.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.cancel, handler: { (action) -> Void in
                performFixMyRideAction()
            }))
            present(prompt, animated: true, completion: nil)
        }
        func performFixMyRideAction(){
            let information = ["userId":loggedInUserId(),
                               "date":selectedDayDate.toStringValue("yyyy-MM-dd"),
                               "mode":trueForHomeToDestinationFalseforDestinationToHome ? "0" : "1",
                               "via":via
                ] as NSDictionary
            
            func disbleSetMyRideButtonShadow(){
                self.setMyRideButton.layer.masksToBounds = true
                self.setMyRideButton.layer.borderWidth = 0.0
                self.setMyRideButton.layer.shadowColor = APP_THEME_GRAY_COLOR.cgColor
                self.setMyRideButton.layer.shadowOpacity = 0.0
                self.setMyRideButton.layer.shadowRadius = 0
                self.setMyRideButton.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
            }
            func enableSetMyRideButtonShadow(){
                self.setMyRideButton.layer.masksToBounds = false
                self.setMyRideButton.layer.borderWidth = 0.0
                self.setMyRideButton.layer.shadowColor = APP_THEME_GRAY_COLOR.cgColor
                self.setMyRideButton.layer.shadowOpacity = 0.8
                self.setMyRideButton.layer.shadowRadius = 12
                self.setMyRideButton.layer.shadowOffset = CGSize(width: 12.0, height: 12.0)
            }
            
            
            disbleSetMyRideButtonShadow()
            self.setMyRideButton.startLoadingAnimation()
            let scm = ServerCommunicationManager()
            scm.setMyRide(information) { (responseData) -> () in
                if let _ = responseData {
                    if let messageToShow = responseData?.object(forKey: "message") as? String {
                        if messageToShow.contains("sent"){
                            showPopupAlertMessage("Fix my ride", message: messageToShow, messageType: .success)
                        }else{
                            showPopupAlertMessage("Fix my ride", message: messageToShow, messageType: .error)
                        }
                    }
                    self.loadUsersOnMap()
                }
                DispatchQueue.main.async(execute: {
                    self.setMyRideButton.stopIt()
                    enableSetMyRideButtonShadow()
                })
            }
        }
        showAlert()
    }
    
    func loadUsersOnMap(){
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(RideHomeViewController.loadUsersOnMapPrivate), object: nil)
        self.perform(#selector(RideHomeViewController.loadUsersOnMapPrivate), with: nil, afterDelay:0.4)
    }
    
    func loadClean(){
        loadCleanPrivate()
        cleanLoadOnAppear = true
    }
    
    func loadCleanPrivate(){
        Acf().hidePopupViewController()
        CacheManager.sharedInstance.saveObject(nil, identifier: "lastCachedResults")
        self.setDataFromArray(NSMutableArray())
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(RideHomeViewController.loadUsersOnMapPrivate), object: nil)
        self.perform(#selector(RideHomeViewController.loadUsersOnMapPrivate), with: nil, afterDelay:6)
    }
    
    func loadUsersOnMapPrivate(){
        if isInternetConnectivityAvailable(false)==false{return}
        if Acf().isAddressAdded() {
            if Acf().isAddressInputScreenNeeded() == false {
                loadAndShowUsersBasedOnCurrentUserDetails()
            }
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(RideHomeViewController.loadUsersOnMapPrivate), object: nil)
            self.perform(#selector(RideHomeViewController.loadUsersOnMapPrivate), with: nil, afterDelay:isInForeground() ?GET_TRAVELLERS_AUTO_REFRESH_TIME_WHEN_SIGNED_IN_FOREGROUND_APP : GET_TRAVELLERS_AUTO_REFRESH_TIME_WHEN_SIGNED_IN_BACKGROUND_APP)
        }else{
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(RideHomeViewController.loadUsersOnMapPrivate), object: nil)
            self.perform(#selector(RideHomeViewController.loadUsersOnMapPrivate), with: nil, afterDelay:isInForeground() ?GET_TRAVELLERS_AUTO_REFRESH_TIME_WHEN_NOT_SIGNED_IN_FOREGROUND_APP : GET_TRAVELLERS_AUTO_REFRESH_TIME_WHEN_NOT_SIGNED_IN_BACKGROUND_APP)
        }
    }
    
    func showSyncingLoadingIndicator() {
        self.infoLabel2.superview!.showActivityIndicatorType(self.infoLabel2.superview!.bounds.center, style: UIActivityIndicatorViewStyle.gray)
    }
    
    func hideSyncingLoadingIndicator() {
        self.infoLabel2.superview!.hideActivityIndicator()
    }
    
    func loadAndShowUsersBasedOnCurrentUserDetails(){
        animateSyncButton()
        var information = NSDictionary()
        if isNotNull(Lem().lm.location) {
            let c = Lem().lm.location!.coordinate
            information = ["date":datePart(selectedDayDate.stringValue() as NSString),"mode":trueForHomeToDestinationFalseforDestinationToHome ? "0" : "1","lat":"\(c.latitude)","lng":"\(c.longitude)"] as NSDictionary
        }else{
            information = ["date":datePart(selectedDayDate.stringValue() as NSString),"mode":trueForHomeToDestinationFalseforDestinationToHome ? "0" : "1"] as NSDictionary
        }
        showSyncingLoadingIndicator()
        
        func processData(responseData:NSDictionary,canShowTimeUpdateOption:Bool=true){
            if ServerCommunicationManager.isSuccess(responseData){
                self.validateUserAction(responseData)
                self.setDataFromArray(getValidTravellersArray(responseData.value(forKeyPath: "data.SEARCH_RESULT") as? NSMutableArray))
            }else if ServerCommunicationManager.isFailure(responseData){
                var errorMessage : NSString?
                if (responseData.object(forKey: ServerCommunicationConstants.RESPONSE_MESSAGE_KEY) != nil) {
                    if ((responseData.object(forKey: ServerCommunicationConstants.RESPONSE_MESSAGE_KEY) as AnyObject) is  NSDictionary) {
                        errorMessage = responseData.object(forKey: ServerCommunicationConstants.RESPONSE_MESSAGE_KEY) as? NSString
                    }else{
                        errorMessage = responseData.description as NSString?
                    }
                }
                if isNotNull(errorMessage)&&errorMessage!.lowercased.contains("preferences"){
                    //server is throwing error for not having any ride preferences saved , so lets user show the ride planner popup , so that user can edit/update user preferences.
                    if Acf().canContinueCase2() && canShowTimeUpdateOption && isSomethingPresented() == false{
                        Acf().switchHomeScreenToCarpool(hideSideMenu: true)
                        self.onClickOfChangeTime()
                    }
                }
            }
        }
        
        let lastCachedResultsDate = CacheManager.sharedInstance.loadObject("lastCachedResultsDate")
        if lastCachedResultsDate is Date {
            if (lastCachedResultsDate as! Date).isLaterThanDate(Date().dateBySubtractingMinutes(10)){
                if let selectionIdentifier = CacheManager.sharedInstance.loadObject("selectionIdentifier") as? String{
                    if selectionIdentifier == currentSelectionIdentifier() {
                        if let responseData = CacheManager.sharedInstance.loadObject("lastCachedResults") as? NSDictionary{
                            processData(responseData: responseData,canShowTimeUpdateOption:false)
                        }
                    }
                }
            }
        }
        
        let systemIdentifierWhileHitting = currentSelectionIdentifier()
        let scm = ServerCommunicationManager()
        scm.responseErrorOption = ResponseErrorOption.dontShowErrorResponseMessage
        scm.returnFailureResponseAlso = true
        scm.getAvailableUsers(information, completionBlock: {[weak self] (responseData) -> () in guard let `self` = self else { return }
            if let _ = responseData {
                if systemIdentifierWhileHitting == self.currentSelectionIdentifier() {
                    Acf().travellersListProcessingDetails.removeAllObjects()
                    processData(responseData: responseData!)
                    CacheManager.sharedInstance.saveObject(responseData!, identifier: "lastCachedResults")
                    CacheManager.sharedInstance.saveObject(Date(), identifier: "lastCachedResultsDate")
                    CacheManager.sharedInstance.saveObject(self.currentSelectionIdentifier(), identifier: "selectionIdentifier")
                }
            }
            self.hideSyncingLoadingIndicator()
        })
    }
    
    func setTravellerStateToSuccess(notification:NSNotification){
        if let userInfo = notification.userInfo as? NSDictionary {
            let userId = getUserId(userInfo)
            if let cachedResults = CacheManager.sharedInstance.loadObject("lastCachedResults") as? NSDictionary {
                let mutableCachedResults = NSMutableDictionary(dictionary: cachedResults)
                let travellers = mutableCachedResults.value(forKeyPath: "data.SEARCH_RESULT") as! NSArray
                let mutableTravellers = NSMutableArray(array: travellers)
                var travellerToModify : NSDictionary?
                var mutableTravellerToModify : NSMutableDictionary?
                for traveller in mutableTravellers {
                    if userId == getUserId(traveller as! NSDictionary) {
                        travellerToModify = traveller as! NSDictionary
                        break
                    }
                }
                if travellerToModify != nil {
                    mutableTravellerToModify = NSMutableDictionary(dictionary: travellerToModify!)
                    mutableTravellerToModify?.setObject("success", forKey: "actionState" as NSCopying)
                    let index = mutableTravellers.index(of: travellerToModify!)
                    mutableTravellers.replaceObject(at: index, with: mutableTravellerToModify!)
                    
                    let mutableData = NSMutableDictionary(dictionary: mutableCachedResults.object(forKey: "data") as! NSDictionary)
                    mutableData.setObject(mutableTravellers, forKey: "SEARCH_RESULT" as NSCopying)
                    mutableCachedResults.setObject(mutableData, forKey: "data" as NSCopying)
                    CacheManager.sharedInstance.saveObject(mutableCachedResults, identifier: "lastCachedResults")
                    self.setDataFromArray(getValidTravellersArray(mutableCachedResults.value(forKeyPath: "data.SEARCH_RESULT") as? NSMutableArray) ,onlySetData: true)
                }
            }
        }
    }
    
    func currentSelectionIdentifier()->String{
        return "\(selectedDayDate.day())\(trueForOwnerFalseforPassenger)\(trueForHomeToDestinationFalseforDestinationToHome)"
    }
    
    func validateUserAction(_ response:NSDictionary){
        if isNotNull(response.value(forKeyPath: "data.USER.PLANNER")){
            let planner = (response.value(forKeyPath: "data.USER.PLANNER") as! NSArray).object(at: 0) as! NSDictionary
            let date = "\(planner.object(forKey: "DATE")!)" as NSString
            let time = "\(planner.object(forKey: "ETD")!)" as NSString
            let mode = "\(planner.object(forKey: "MODE")!)" as NSString
            let status = "\(planner.object(forKey: "STATUS")!)" as NSString
            let dateOfRide = Acf().dateFormatter.date(from: date as String)
            let timeOfRide = Acf().timeFormatter.date(from: time as String)
            let isExpectedPlanNotFound = Int(dateOfRide!.day()) != Int(self.selectedDayDate.day())
            if isExpectedPlanNotFound {
                self.selectedDayDate = dateOfRide ?? (self.selectedDayDate ?? Date())
                
                let time = timeOfRide ?? (self.selectedDayTime ?? Date())
                self.selectedDayTime = Date().setTimeOfDate(time.hour(), minute: time.minute(), second: time.second)
                
                self.trueForHomeToDestinationFalseforDestinationToHome = mode.isEqual(to: "0")
                self.trueForOwnerFalseforPassenger = status.isEqual(to: "1")
                let settings = Dbm().getSetting()
                if trueForOwnerFalseforPassenger {
                    settings.selection1CachedValue! = NSNumber(value: 0 as Int32)
                }else{
                    settings.selection1CachedValue! = NSNumber(value: 1 as Int32)
                }
                self.isRideDetailsModified = true
                self.updateUserInterfaceOnScreen()
                let sourceToDestination : String?
                if trueForHomeToDestinationFalseforDestinationToHome {
                    sourceToDestination = "Home to \(destinationCapitalizedName())"
                }else{
                    sourceToDestination = "\(destinationCapitalizedName()) to Home"
                }
                
                if Acf().canContinueCase2() {
                    showAlert("We need your attention!", message: "It seems , you haven't decided about your ride on \(date) , while travelling from \(sourceToDestination!).\nPlease Settle or Cancel the ride!")
                }
            }else{
                self.selectedDayDate = dateOfRide ?? (self.selectedDayDate)
                
                let time = timeOfRide ?? (self.selectedDayTime)
                self.selectedDayTime = Date().setTimeOfDate(time.hour(), minute: time.minute(), second: time.second)
                
                self.trueForHomeToDestinationFalseforDestinationToHome = mode.isEqual(to: "0")
                self.trueForOwnerFalseforPassenger = status.isEqual(to: "1")
                self.isRideDetailsModified = true
                let settings = Dbm().getSetting()
                if trueForOwnerFalseforPassenger {
                    settings.selection1CachedValue! = NSNumber(value: 0 as Int32)
                }else{
                    settings.selection1CachedValue! = NSNumber(value: 1 as Int32)
                }
                self.updateUserInterfaceOnScreen()
            }
        }
    }
    
    func setDataFromArray(_ results:NSMutableArray?,onlySetData:Bool=false){
        let wasInitiallyZero = (safeInt(self.users?.count) == 0)
        var travellersSuggestions:NSMutableArray?
        if let users = results{
            if users.count > MAXIMUM_TRAVELLERS_LIST_SUGGESTION {
                travellersSuggestions = NSMutableArray(array: users.subarray(with: NSMakeRange(0, MAXIMUM_TRAVELLERS_LIST_SUGGESTION)))
            }else{
                travellersSuggestions = NSMutableArray(array: users)
            }
        }
        
        if isNotNull(travellersSuggestions)&&travellersSuggestions!.count > 0 {
            users =  travellersSuggestions
            trackFunctionalEvent(FE_SCREEN_DATA_LOADED, information: ["screen":"ride_screen"])
        }else{
            users = NSMutableArray()
            trackFunctionalEvent(FE_SCREEN_DATA_NOT_LOADED, information: ["screen":"ride_screen"])
        }
        updateMapContents()
        
        if rideStatus != .none {
            updateInformationOnRideDetailScreen()
        }
        updateUserInterfaceOnScreen()
        if !onlySetData {
            checkAndShowRideSettleAlertIfRequired()
            checkAndShowFeedbackPopupIfRequired()
        }
        self.tableView.reloadData()
        updateTableViewContentOffset()
    }
    
    func updateTableViewContentOffset(){
        execMain({
            if self.tableView.contentOffset.y < -(DEVICE_HEIGHT-88){
                if safeInt(self.users?.count)>1{
                    self.tableView.setContentOffset(CGPoint(x: 0, y: -(DEVICE_HEIGHT-237)), animated: true)
                }else if safeInt(self.users?.count)>0{
                    self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableViewScrollPosition.bottom, animated: true)
                }else{
                    self.tableView.setContentOffset(CGPoint(x: 0, y: -DEVICE_HEIGHT), animated: true)
                }
            }
        }, delay: 0)
    }
    
    func updateInformationOnRideDetailScreen(_ cleanFirst:Bool=false){
        if cleanFirst {
            rideDetailsViewController?.clean()
        }
        rideDetailsViewController?.selectedDayDate = selectedDayDate
        rideDetailsViewController?.selectedDayTime = selectedDayTime
        rideDetailsViewController?.trueForHomeToDestinationFalseforDestinationToHome = trueForHomeToDestinationFalseforDestinationToHome
        rideDetailsViewController?.trueForOwnerFalseforPassenger=trueForOwnerFalseforPassenger
        rideDetailsViewController?.usersForRide = usersForRide
        rideDetailsViewController?.allUsers = users ?? NSMutableArray()
        rideDetailsViewController?.rideStatus = rideStatus
        rideDetailsViewController?.trueIfLessThanOneHourLeft = trueIfLessThanOneHourLeft
        rideDetailsViewController?.isTodaysRide = (Date().day() == selectedDayDate.day()) && (Date().month() == selectedDayDate.month())
        rideDetailsViewController?.dateOfRide = dateOfRide
        rideDetailsViewController?.setDataFromArray(getUsersForRideDetail())
    }
    
    func updateMapContents(){
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(RideHomeViewController.updateMapContentsPrivate), object: nil)
        self.perform(#selector(RideHomeViewController.updateMapContentsPrivate), with: nil, afterDelay: 0.2)
    }
    
    func updateMapContentsPrivate(){
        //step 1 : remove everything from the map
        let annotationsCountBefore = mapView.annotations.count
        
        mapView.removeAnnotations(mapView.annotations)
        
        //step 2 : add home and destination locations on map
        addHomeAndDestinationAnnotation()
        
        //step 3 : add users annotations on map
        let annotationsToAdd = NSMutableArray()
        for user in users! {
            Dbm().addUserId(getUserId(user as? NSDictionary))
            if annotationsToAdd.count >= MAXIMUM_ANNOTATIONS_COUNT_LIMIT{break;}
            let annotation = UserType1PinAnnotation()
            var destinationLocationLatitude : String?
            var destinationLocationLongitude : String?
            var homeLocationLatitude : String?
            var homeLocationLongitude : String?
            if trueForHomeToDestinationFalseforDestinationToHome {
                if isNotNull((user as! NSDictionary).object(forKey: "home_lat")){
                    homeLocationLatitude = (user as! NSDictionary).object(forKey: "home_lat")as! String
                    homeLocationLongitude = (user as! NSDictionary).object(forKey: "home_lng") as! String
                }else{
                    if isNotNull((user as! NSDictionary).object(forKey: "ofc_lat")){
                        destinationLocationLatitude = (user as! NSDictionary).object(forKey: "ofc_lat")as! String
                        destinationLocationLongitude = (user as! NSDictionary).object(forKey: "ofc_lng") as! String
                    }else{
                        if isNotNull((user as! NSDictionary).object(forKey: "par_home_lat")){
                            homeLocationLatitude = (user as! NSDictionary).object(forKey: "par_home_lat")as! String
                            homeLocationLongitude = (user as! NSDictionary).object(forKey: "par_home_lng") as! String
                        }else{
                            if isNotNull((user as! NSDictionary).object(forKey: "par_ofc_lat")){
                                destinationLocationLatitude = (user as! NSDictionary).object(forKey: "par_ofc_lat")as! String
                                destinationLocationLongitude = (user as! NSDictionary).object(forKey: "par_ofc_lng") as! String
                            }
                        }
                    }
                }
            }else{
                if isNotNull((user as! NSDictionary).object(forKey: "ofc_lat")){
                    destinationLocationLatitude = (user as! NSDictionary).object(forKey: "ofc_lat")as! String
                    destinationLocationLongitude = (user as! NSDictionary).object(forKey: "ofc_lng") as! String
                }
                else{
                    if isNotNull((user as! NSDictionary).object(forKey: "home_lat")){
                        homeLocationLatitude = (user as! NSDictionary).object(forKey: "home_lat")as! String
                        homeLocationLongitude = (user as! NSDictionary).object(forKey: "home_lng") as! String
                    }else{
                        if isNotNull((user as! NSDictionary).object(forKey: "par_ofc_lat")){
                            destinationLocationLatitude = (user as! NSDictionary).object(forKey: "par_ofc_lat")as! String
                            destinationLocationLongitude = (user as! NSDictionary).object(forKey: "par_ofc_lng") as! String
                        }else{
                            if isNotNull((user as! NSDictionary).object(forKey: "par_home_lat")){
                                homeLocationLatitude = (user as! NSDictionary).object(forKey: "par_home_lat")as! String
                                homeLocationLongitude = (user as! NSDictionary).object(forKey: "par_home_lng") as! String
                            }
                        }
                    }
                }
            }
            if isNotNull(destinationLocationLatitude){
                annotation.coordinate = CLLocationCoordinate2DMake(Double(destinationLocationLatitude!)!, Double(destinationLocationLongitude!)!)
            }else if isNotNull(homeLocationLatitude){
                annotation.coordinate = CLLocationCoordinate2DMake(Double(homeLocationLatitude!)!, Double(homeLocationLongitude!)!)
            }else{
                continue
            }
            annotationsToAdd.add(annotation)
            annotation.userInfo = user
            annotation.title = (user as? NSDictionary)?.object(forKey: "user_name") as? String
        }
        self.mapView.addAnnotations(annotationsToAdd as! [MKAnnotation])
        
        //step 4 : adjust map so that every content become visible on screen
        
        execMain({[weak self]  in guard let `self` = self else { return }
            let annotationsCountAfter = self.mapView.annotations.count
            if annotationsCountAfter != annotationsCountBefore {
                logMessage("annotationsCountBefore \(annotationsCountBefore)")
                logMessage("annotationsCountAfter \(annotationsCountAfter)")
                self.animateAnnotations = true
                self.mapView.updateRegion(forAnnotations: self.mapView.annotations, animated: true)
                execMain({[weak self] in guard let `self` = self else { return }
                    
                    self.animateAnnotations = false
                    execMain({[weak self] in guard let `self` = self else { return }
                        
                        let x = self.mapView.visibleMapRect.origin.x
                        let y = self.mapView.visibleMapRect.origin.y
                        let w = self.mapView.visibleMapRect.size.width
                        let h = self.mapView.visibleMapRect.size.height
                        CacheManager.sharedInstance.saveObject(x, identifier: "mapX")
                        CacheManager.sharedInstance.saveObject(y, identifier: "mapY")
                        CacheManager.sharedInstance.saveObject(w, identifier: "mapW")
                        CacheManager.sharedInstance.saveObject(h, identifier: "mapH")
                        },delay: 2)
                })
            }
            },delay: 2)
        
        //step 5 : fetch details about upcoming or currently running ride
        checkAndProcessNextRideIfAny()
        
        //step 6 : notify users if needed
        checkForAnyNotificationsToShow()
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is UserType1PinAnnotation {
            var annotationView = self.mapView.dequeueReusableAnnotationView(withIdentifier: "location") as! UserType1MKAnnotationView?
            if isNull(annotationView) {
                annotationView = UserType1MKAnnotationView(annotation: annotation, reuseIdentifier: "location")
            }
            annotationView!.userInfo = (annotation as! UserType1PinAnnotation).userInfo as? NSDictionary
            annotationView!.setup()
            annotationView?.canShowCallout = true
            if animateAnnotations {
                annotationView?.performAppearAnimationType5(Double(Float.random(3.0, 8.0)))
            }
            annotationView?.transform = CGAffineTransform(scaleX: ANNOTATION_TRANSFORM_ADJUST, y: ANNOTATION_TRANSFORM_ADJUST)
            return annotationView
        }
        if annotation is MKPointAnnotation{
            if let annotationObject = annotation as? MKPointAnnotation {
                var annotationView = self.mapView.dequeueReusableAnnotationView(withIdentifier: "homeDestinationLocation") as MKAnnotationView?
                if isNull(annotationView) {
                    annotationView = MKAnnotationView(annotation: annotationObject, reuseIdentifier: "homeDestinationLocation")
                }
                if annotationObject == annotationHome {
                    annotationView?.image = UIImage(named: "home")
                }else if annotationObject == annotationDestination {
                    annotationView?.image = UIImage(named: "office")
                }
                annotationView?.canShowCallout = true
                annotationView?.size = CGSize(width: 33, height: 33)
                annotationView?.transform = CGAffineTransform(scaleX: ANNOTATION_TRANSFORM_ADJUST, y: ANNOTATION_TRANSFORM_ADJUST)
                return annotationView
            }
        }
        return nil
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_RESET_IDLE_TIMER_RIDE_HOME), object: nil, userInfo: nil)
        if let annotation = view.annotation as? UserType1PinAnnotation {
            performAnimatedClickEffectType1(view)
            UserMiniProfileView.showMiniProfileView(annotation.userInfo as! NSDictionary,rideDate: selectedDayDate, onView: self.view,parentVC: self ,autoHide: true,after:10,completionBlock:{[weak self] (returnedData) -> () in guard let `self` = self else { return }
                showUserProfile(returnedData as! NSDictionary ,navigationController: Acf().navigationController!)
                },removedBlock:{(returnedData) -> () in })
            //show user information on popup view
        }
        hideThingsToViewMapInBetterWay()
    }
    
    func addHomeAndDestinationAnnotation(){
        if Acf().isAddressAdded(){
            let settings = Dbm().getSetting()
            
            annotationHome = MKPointAnnotation()
            annotationHome!.coordinate = CLLocationCoordinate2DMake(settings.homeLocationLatitude!.doubleValue,settings.homeLocationLongitude!.doubleValue)
            annotationHome?.title = "Home"
            self.mapView.addAnnotation(annotationHome!)
            
            annotationDestination = MKPointAnnotation()
            annotationDestination!.coordinate = CLLocationCoordinate2DMake(settings.destinationLocationLatitude!.doubleValue,settings.destinationLocationLongitude!.doubleValue)
            annotationDestination?.title = "\(destinationCapitalizedName())"
            self.mapView.addAnnotation(annotationDestination!)
        }
    }
    
    func checkForAnyNotificationsToShow(){
        if let _ = users {
            for user in users! {
                if Acf().getTravelRequestStatus(user as! NSDictionary) == .travelRequestReceived {
                    trackFunctionalEvent(FE_INVITE_RECEIVED, information: ["sent-from":"\(getUserId(user as! NSDictionary))"])
                    if Drnm().isDRNActive() == false {
                        showNotification(MESSAGE_TEXT___PENDING_RIDE_REQUEST, showOnNavigation: false, showAsError: false)
                    }
                }
            }
        }
    }
    
    func checkAndProcessNextRideIfAny(){
        //step 1: reset settings to no ride
        rideDetailButton.isHidden = true
        
        rideStatus = Acf().getRideStatus(users,dateOfRide: dateOfRide)
        
        //step 2: find next ride details
        usersForRide = NSMutableArray()
        
        if let _ = users {
            for user in users! {
                let travelRequestStatus = Acf().getTravelRequestStatus(user as! NSDictionary)
                if travelRequestStatus == .travelRequestAccepted {
                    usersForRide.add(user)
                }
                if travelRequestStatus == .travelRequestSent {
                    usersForRide.add(user)
                }
                if travelRequestStatus == .travelRequestReceived {
                    usersForRide.add(user)
                }
            }
        }
        
        dateOfRide = Date.date(selectedDayDate.year(), month: selectedDayDate.month(), day: selectedDayDate.day(), hour: selectedDayTime.hour(), minute: selectedDayTime.minute(), second: selectedDayTime.second)
        
        if rideStatus != .none {
            //step 3: find if ride started or waiting and update UI components on screen
            trueIfLessThanOneHourLeft = Date().dateByAddingMinutes(60).isEarlierThanDate(dateOfRide)
            rideDetailButton.isHidden = false
            updateRideDetailButton()
        }else{
            trueIfLessThanOneHourLeft = false
            rideDetailButton.isHidden = true
        }
        updateRidersGroupIfRequired()
        decideAndSetRideChatButtonVisibility()
    }
    
    func decideAndSetRideChatButtonVisibility() {
        if rideStatus != .none {
            if (rideStatus == .confirmed || rideStatus == .confirmedButDelayed) {
                if trueForOwnerFalseforPassenger {
                    if Acf().isQBReadyForItsUserDependentServices(){
                        if let _ = Acf().getRidersGroup(){
                            rideChatButton.isHidden = false
                        }else{
                            rideChatButton.isHidden = true
                        }
                    }else{
                        rideChatButton.isHidden = true
                    }
                }else{
                    if Acf().isQBReadyForItsUserDependentServices(){
                        if let _ = Acf().getRidersGroupCarOwner(getCarOwnerId(usersForRide)) {
                            rideChatButton.isHidden = false
                        }else{
                            rideChatButton.isHidden = true
                        }
                    }
                    else{
                        rideChatButton.isHidden = true
                    }
                }
            }else{
                rideChatButton.isHidden = true
            }
        }else{
            rideChatButton.isHidden = true
        }
    }
    
    func updateRideDetailButton(){
        if selectedDayDate.day() > Date().day() {
            rideDetailButton.isHidden = true
        }else{
            rideDetailButton.titleLabel?.minimumScaleFactor = 0.5
            rideDetailButton.stopPulseEffect()
            if trueForRideWaitingFalseforStarted() && trueIfLessThanOneHourLeft {
                if rideStatus == .confirmed {
                    rideDetailButton.setBackgroundColor(APP_THEME_LIGHT_GRAY_COLOR, forState: UIControlState.normal)
                    rideDetailButton.setTitle("Ride in "+remaningTime(Date(), endDate: dateOfRide), for: UIControlState.normal)
                }
            }else if trueForRideWaitingFalseforStarted() && !trueIfLessThanOneHourLeft {
                if rideStatus == .confirmed {
                    rideDetailButton.setBackgroundColor(APP_THEME_VOILET_COLOR, forState: UIControlState.normal)
                    rideDetailButton.setTitle("Ride in "+remaningTime(Date(), endDate: dateOfRide), for: UIControlState.normal)
                    rideDetailButton.startPulse(with: APP_THEME_GREEN_COLOR, offset: CGSize(width: 0,height: 0), frequency: 4.0)
                }
            }
            else if (rideStatus == .confirmed || rideStatus == .confirmedButDelayed) {
                if (rideStatus == .confirmedButDelayed){
                    rideDetailButton.setBackgroundColor(UIColor.red, forState: UIControlState.normal)
                    rideDetailButton.setTitle("Ride Completed ? Settle it !!!", for: UIControlState.normal)
                    rideDetailButton.stopPulseEffect()
                }else{
                    rideDetailButton.setBackgroundColor(APP_THEME_RED_COLOR, forState: UIControlState.normal)
                    rideDetailButton.setTitle("Active Ride "+elapsedTime(dateOfRide), for: UIControlState.normal)
                    rideDetailButton.startPulse(with: UIColor.red, offset: CGSize(width: 0,height: 0), frequency:1.0)
                }
                if isComingFirstTime {
                    isComingFirstTime = false
                    execMain({[weak self]  in guard let `self` = self else { return }
                        self.onClickOfRideDetailButton()
                        },delay: 3)
                }
            }
            else if rideStatus == .completed {
                rideDetailButton.setBackgroundColor(APP_THEME_GREEN_COLOR, forState: UIControlState.normal)
                rideDetailButton.setTitle("Ride Completed", for: UIControlState.normal)
                rideDetailButton.startPulse(with: UIColor.red, offset: CGSize(width: 0,height: 0), frequency:1.0)
                if isComingFirstTime {
                    isComingFirstTime = false
                    execMain({[weak self]  in guard let `self` = self else { return }
                        self.onClickOfRideDetailButton()
                        },delay: 3)
                }
            }
            if rideStatus == .invitationSent {
                rideDetailButton.setBackgroundColor(UIColor.orange, forState: UIControlState.normal)
                if Date().isEarlierThanDate(dateOfRide){
                    rideDetailButton.setTitle("Invite Sent for Ride in "+remaningTime(Date(), endDate: dateOfRide), for: UIControlState.normal)
                }else{
                    rideDetailButton.setTitle("Invite Sent for Ride at " + dateOfRide.stringTimeOnly_AM_PM_FormatValue(), for: UIControlState.normal)
                }
            }else if rideStatus == .invitationReceived {
                rideDetailButton.setBackgroundColor(UIColor.orange, forState: UIControlState.normal)
                if Date().isEarlierThanDate(dateOfRide){
                    rideDetailButton.setTitle("Invite Received for Ride in "+remaningTime(Date(), endDate: dateOfRide), for: UIControlState.normal)
                }else{
                    rideDetailButton.setTitle("Invite Received for Ride at " + dateOfRide.stringTimeOnly_AM_PM_FormatValue(), for: UIControlState.normal)
                }
            }
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(RideHomeViewController.updateRideDetailButton), object: nil)
            if rideDetailButton.isHidden == false {
                self.perform(#selector(RideHomeViewController.updateRideDetailButton), with: nil, afterDelay: 1)
            }
        }
    }
    
    func updateRidersGroupIfRequired(){
        if trueForOwnerFalseforPassenger && isSystemReadyToProcessThis() && selectedDayDate.day() <= Date().day() && (rideStatus == .confirmed || rideStatus == .confirmedButDelayed) {
            func performUpdateforGroupChatIfRequired(_ users:NSArray){
                if users.count > 0 {
                    if self.trueForOwnerFalseforPassenger {
                        if(isInternetConnectivityAvailable(false)==false){return}
                        let userIds = NSMutableArray()
                        for user in users {
                            let travelRequestStatus = Acf().getTravelRequestStatus(user as! NSDictionary)
                            if travelRequestStatus == .travelRequestAccepted {
                                //group should consist of confirmed users only
                                userIds.add(getUserId(user as? NSDictionary))
                            }
                        }
                        if userIds.count > 0 {
                            Acf().manageRidersGroup(userIds,dateOfRide:dateOfRide)
                        }
                    }
                }
            }
            performUpdateforGroupChatIfRequired(usersForRide)
        }
    }
    
    func getUsersForRideDetail() -> NSMutableArray {
        let usersToReturn = NSMutableArray()
        if let _ = users {
            for user in users! {
                if Acf().getTravelRequestStatus(user as! NSDictionary) == .travelRequestAccepted {
                    usersToReturn.add(user)
                }
                else if Acf().getTravelRequestStatus(user as! NSDictionary) == .travelRequestReceived {
                    usersToReturn.add(user)
                }
                else if Acf().getTravelRequestStatus(user as! NSDictionary) == .travelRequestSent {
                    usersToReturn.add(user)
                }
            }
        }
        return usersToReturn
    }
    
    func getUsersForFeedbacks() -> NSMutableArray {
        let usersToReturn = NSMutableArray()
        if let _ = users {
            for user in users! {
                if Acf().getTravelRequestStatus(user as! NSDictionary) == .travelCompleted {
                    if isAlreadyProvidedFeedback(user as! NSDictionary) == false {
                        usersToReturn.add(user)
                    }
                }
            }
        }
        return usersToReturn
    }
    
    
    func confirmedUsers() -> NSMutableArray {
        let usersToReturn = NSMutableArray()
        if let _ = users {
            for user in users! {
                if Acf().getTravelRequestStatus(user as! NSDictionary) == .travelRequestAccepted {
                    usersToReturn.add(user)
                }
            }
        }
        return usersToReturn
    }
    
    func checkAndShowRideSettleAlertIfRequired(){
        if Acf().isPopUpViewControllerShowing() == false && isSomethingPresented() == false {
            func filterUsers(_ users:NSMutableArray,userIds:NSMutableArray)->NSMutableArray{
                let filteredUsers = NSMutableArray()
                for u in users{
                    let userId = getUserId(u as! NSDictionary) as NSString
                    for uid in userIds{
                        if userId.isEqual(to: uid as! String){
                            filteredUsers.add(u)
                        }
                    }
                }
                return filteredUsers
            }
            
            if Acf().carpoolHomeScreenController?.selectedIndex == RIDE_VIEW_TAB_INDEX && (Acf().sideMenuController?.leftMenuVisible == false) {
                if (rideStatus == .confirmed || rideStatus == .confirmedButDelayed) {
                    if trueForRideWaitingFalseforStarted() == false {
                        if trueForOwnerFalseforPassenger {
                            if let ridersGroup = Acf().getRidersGroup() {
                                RideValidationManager.sharedInstance.checkMyRideCompletionStatus(self.trueForOwnerFalseforPassenger, isHomeToDestination:  self.trueForHomeToDestinationFalseforDestinationToHome, dateOfRide: dateOfRide, groupChatDialogue: ridersGroup,completion:{ (completed, users) in
                                    if completed && users != nil && users!.count > 0 {
                                        let passengers = filterUsers(self.confirmedUsers(), userIds: users!)
                                        if passengers.count > 0 {
                                            Acf().showSettleRidePopup(passengers, isCarOwner: false,rideDate: self.dateOfRide)
                                            self.resetTimerToSetCurrentRide()
                                        }
                                    }
                                })
                            }
                        }else{
                            if let ridersGroup = Acf().getRidersGroupCarOwner(getCarOwnerId(usersForRide)) {
                                RideValidationManager.sharedInstance.checkMyRideCompletionStatus(self.trueForOwnerFalseforPassenger, isHomeToDestination:  self.trueForHomeToDestinationFalseforDestinationToHome, dateOfRide: dateOfRide, groupChatDialogue: ridersGroup,completion:{ (completed, users) in
                                    if completed && users != nil && users!.count > 0 {
                                        if let carOwner = getCarOwner(self.usersForRide) {
                                            if (getUserId(carOwner) as NSString).isEqual(to: users![0] as! String){
                                                Acf().showSettleRidePopup([carOwner], isCarOwner: true,rideDate: self.dateOfRide)
                                                self.resetTimerToSetCurrentRide()
                                            }
                                        }
                                    }
                                })
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    func getRideId() -> String {
        return "\(selectedDayDate.day())\(selectedDayDate.month())\(selectedDayDate.year())\(trueForHomeToDestinationFalseforDestinationToHome == true ? "0" : "1")"
    }
    
    func checkAndShowFeedbackPopupIfRequired(){
        if Acf().canContinueCase1() {
            let users = getUsersForFeedbacks()
            for u in users {
                if Acf().showFeedbackScreenIfNotAlreadyAsked(u as! (NSDictionary)){
                    break
                }
            }
        }
    }
    
    func appDidEnterBackground() {
        lastDateWhenEnteredBackground = Date()
    }
    
    func appWillEnterForeground() {
        resetTimerToSetCurrentRide()
        if lastDateWhenEnteredBackground.isEarlierThanDate(Date().dateBySubtractingSeconds(Int(DELAY_TIME_TO_RESET_USER_LIST))) || isInternetConnectivityAvailable(false) == false {
            setDataFromArray(NSMutableArray())
        }
        if lastDateWhenEnteredBackground.isEarlierThanDate(Date().dateBySubtractingSeconds(Int(DELAY_TIME_TO_RESET_TO_CURRENT_PLANNER))){
            loadCurrentRide()
        }
    }
    
    func resetTimerToSetCurrentRide(){
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(RideHomeViewController.loadCurrentRide), object: nil)
        self.perform(#selector(RideHomeViewController.loadCurrentRide), with: nil, afterDelay: DELAY_TIME_TO_RESET_TO_CURRENT_PLANNER)
    }
    
    func loadCurrentRide() {
        let modificationStatus = isRideDetailsModified
        isRideDetailsModified = false
        updateUserInterfaceOnScreen()
        if modificationStatus {
            if Acf().canContinueCase1() {
                Acf().hidePopupViewController()
            }
            onClickOfRefreshButton()
        }
        resetTimerToSetCurrentRide()
    }
    
    func animateSyncButton(){
        self.refreshButton!.transform = CGAffineTransform.identity
        self.refreshButton!.rotate(toAngle: CGFloat(M_PI * 2.0), duration: 2.0 , direction: .right, repeatCount: 1 , autoreverse: false)
    }
    
    func updateUsersRideRelatedSelection(_ selectedDayDate : Date , selectedDayTime : Date , updatePlanner:Bool){
        if(isInternetConnectivityAvailable(true)==false){return}
        if Acf().estimateUserExpectedTravelModeForDate(date: selectedDayTime) == .morning{
            self.trueForHomeToDestinationFalseforDestinationToHome = true
        }else{
            self.trueForHomeToDestinationFalseforDestinationToHome = false
        }
        
        let settings = Dbm().getSetting()
        if self.trueForHomeToDestinationFalseforDestinationToHome {
            settings.homeTime = selectedDayTime.stringValue()
        }else{
            settings.destinationTime = selectedDayTime.stringValue()
        }
        Dbm().saveChanges()
        let userInfo = Dbm().getUserInfo()
        let trueForOwnerFalseforPassenger = settings.selection1CachedValue!.intValue == 0
        let information = NSMutableDictionary()
        copyData(userInfo!.userId , destinationDictionary: information, destinationKey: "userId", methodName: #function)
        copyData(NSNumber(value: trueForOwnerFalseforPassenger as Bool), destinationDictionary: information, destinationKey: "isOwner", methodName: #function)
        copyData(NSNumber(value: !self.trueForHomeToDestinationFalseforDestinationToHome), destinationDictionary: information, destinationKey: "mode", methodName: #function)
        copyData(selectedDayTime.stringTimeOnly24HFormatValue() , destinationDictionary: information, destinationKey: "time", methodName: #function)
        copyData(selectedDayDate.toStringValue("yyyy-MM-dd") , destinationDictionary: information, destinationKey: "date", methodName: #function)
        copyData(true , destinationDictionary: information, destinationKey: "autoFixEnabled", methodName: #function)
        copyData("top" , destinationDictionary: information, destinationKey: "autoFixType", methodName: #function)
        updateTravelPlan(information, selectedDayDate: selectedDayDate, selectedDayTime: selectedDayTime, isHomeToDestination:trueForHomeToDestinationFalseforDestinationToHome, updatePlanner: updatePlanner,completion: { (selectedDayDate, selectedDayTime, isHomeToDestination) -> () in
            self.selectedDayTime = selectedDayTime
            self.selectedDayDate = selectedDayDate
            self.trueForHomeToDestinationFalseforDestinationToHome = isHomeToDestination
            let settings = Dbm().getSetting()
            self.trueForOwnerFalseforPassenger = settings.selection1CachedValue!.intValue == 0
            self.isRideDetailsModified = true
            self.updateUserInterfaceOnScreen()
            self.loadUsersOnMap()
        })
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func didDragMap(_ sender: UIGestureRecognizer) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NOTIFICATION_HIDE_FOR_MAP_VIEW), object: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return safeInt(self.users?.count)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return UserMiniProfileTableViewCell.getRequiredHeight(self.users![(indexPath as NSIndexPath).row] as! NSDictionary)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView .dequeueReusableCell(withIdentifier: "UserMiniProfileTableViewCell") as? UserMiniProfileTableViewCell
        cell?.userInfo = self.users?[(indexPath as NSIndexPath).row] as? NSDictionary
        cell?.parentVC = self
        cell?.rideDate = selectedDayDate
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? UserMiniProfileTableViewCell {
            performAnimatedClickEffectType1(cell.fullNameLabel!)
            showUserProfile(self.users?[(indexPath as NSIndexPath).row] as! NSDictionary, navigationController: Acf().navigationController!)
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        updateTableViewContentOffset()
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        updateTableViewContentOffset()
    }
    
    func hideThingsToViewMapInBetterWay(){
        UIView.animate(withDuration: HIDE_ANIMATION_DURATION, delay: 0, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.headerView1.layer.opacity = OPACITY_WHEN_HIDDEN_FOR_MAP_VIEW
            self.headerView2.layer.opacity = OPACITY_WHEN_HIDDEN_FOR_MAP_VIEW
            self.setMyRideButton.layer.opacity = OPACITY_WHEN_HIDDEN_FOR_MAP_VIEW
            self.rideDetailButton.layer.opacity = OPACITY_WHEN_HIDDEN_FOR_MAP_VIEW
            self.rideChatButton.layer.opacity = OPACITY_WHEN_HIDDEN_FOR_MAP_VIEW
            Acf().carpoolHomeScreenController?.tabBar.layer.opacity = OPACITY_WHEN_HIDDEN_FOR_MAP_VIEW
        }) { (completed) in }
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(RideHomeViewController.restoreHiddenComponents), object: nil)
        self.perform(#selector(RideHomeViewController.restoreHiddenComponents), with: nil, afterDelay: RESTORE_HIDDEN_VIEW_TIME)
    }
    
    func restoreHiddenComponents(){
        UIView.animate(withDuration: SHOW_ANIMATION_DURATION, delay: 0, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.headerView1.layer.opacity = 1.0
            self.headerView2.layer.opacity = 1.0
            self.setMyRideButton.layer.opacity = 1.0
            self.rideDetailButton.layer.opacity = 1.0
            self.rideChatButton.layer.opacity = 1.0
            Acf().carpoolHomeScreenController?.tabBar.layer.opacity = 1.0
        }) { (completed) in }
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        guard let indexPath = self.tableView!.indexPathForRow(at: location) else { return nil }
        if let cell = tableView!.cellForRow(at: indexPath) as? UserMiniProfileTableViewCell {
            let selectedCellFrame = tableView!.cellForRow(at: indexPath)!.frame
            let detailViewController = getViewController("ProfileOthersViewController") as! ProfileOthersViewController
            detailViewController.userInfo = self.users?[(indexPath as NSIndexPath).row] as? NSDictionary
            let gapX = 10.0 as CGFloat
            let gapY = 10.0 as CGFloat
            detailViewController.preferredContentSize = CGRect(x: gapX,y: gapY,width: DEVICE_WIDTH-2*gapX, height: DEVICE_HEIGHT-2*gapY).size
            previewingContext.sourceRect = selectedCellFrame
            let previewActions = NSMutableArray()
            if cell.bottomActionButton.isEnabled {
                if cell.isActionButtonActionable(){
                    if let action = cell.bottomActionButton.titleLabel?.text {
                        previewActions.add(UIPreviewAction(title: action , style: .default, handler: {[weak self] action, viewController in guard let `self` = self else { return }
                            if let cell = self.tableView!.cellForRow(at: indexPath) as? UserMiniProfileTableViewCell {
                                if cell.isActionButtonActionable(){
                                    cell.onClickOfActionButton()
                                }
                            }
                        }))
                    }
                }
            }
            if previewActions.count > 0 {
                detailViewController.preViewingActions = previewActions
            }
            return detailViewController
        }
        return nil
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        Acf().showViewControllerAsPopup(viewControllerToCommit)
    }
    
    
    @IBAction func onClickOfChangeDate(){
        if isSystemReadyToProcessThis() == false {return}
        if Drnm().isDRNActive() {return}
        trackFunctionalEvent(FE_CHANGE_DATE_TIME, information: nil)
        showDateTimePicker(self.selectedDayDate, mode: .date) { (selectedDate) in
            self.updateUsersRideRelatedSelection(selectedDate, selectedDayTime: self.selectedDayTime,updatePlanner: false)
        }
    }
    
    @IBAction func onClickOfChangeModeButton(){
        if isSystemReadyToProcessThis() == false {return}
        if Drnm().isDRNActive() {return}
        trackFunctionalEvent(FE_CHANGE_MODE, information: nil)
        trackScreenLaunchEvent(SLE_SELECT_TRAVEL_MODE)
        let actionSheet = Hokusai()
        actionSheet.addButton("Car Owner") {
            Dbm().getSetting().selection1CachedValue = NSNumber(value: 0)
            Dbm().saveChanges()
            self.updateUsersRideRelatedSelection(self.selectedDayDate, selectedDayTime: self.selectedDayTime,updatePlanner: true)
        }
        actionSheet.addButton("Passenger") {
            Dbm().getSetting().selection1CachedValue = NSNumber(value: 1)
            Dbm().saveChanges()
            self.updateUsersRideRelatedSelection(self.selectedDayDate, selectedDayTime: self.selectedDayTime,updatePlanner: true)
        }
        actionSheet.cancelButtonTitle = "Cancel"
        actionSheet.show()
    }
    
    @IBAction func onClickOfChangeTime(){
        if isSystemReadyToProcessThis() == false {return}
        if Drnm().isDRNActive() {return}
        trackFunctionalEvent(FE_CHANGE_DATE_TIME, information: nil)
        showDateTimePicker(self.selectedDayTime, mode: .time) { (selectedDate) in
            if self.selectedDayDate.isToday() && selectedDate.isInPast() {
                showNotification("Please select future date", showOnNavigation: false, showAsError: true)
            }else{
                self.updateUsersRideRelatedSelection(self.selectedDayDate, selectedDayTime: selectedDate,updatePlanner: true)
            }
        }
    }
    
    @IBAction func onClickOfRideDetailButton(){
        if !isSystemReadyToProcessThis() { return }
        NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_RESET_IDLE_TIMER_RIDE_HOME), object: nil, userInfo: nil)
        updateInformationOnRideDetailScreen(true)
        tab1ParentViewController?.switchToViewController(false)
    }
    
    @IBAction func onClickOfRideChatButton(){
        if !isSystemReadyToProcessThis() { return }
        NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_RESET_IDLE_TIMER_RIDE_HOME), object: nil, userInfo: nil)
        if trueForOwnerFalseforPassenger {
            Acf().showRiderGroup()
        }else{
            Acf().showGroupChatScreen(Acf().ridersGroupNameCarOwner(getCarOwnerId(usersForRide)))
        }
    }
    
    @IBAction func onClickOfRefreshButton(){
        if !isSystemReadyToProcessThis() { return }
        trackFunctionalEvent(FE_MANUAL_REFRESH_TRAVELLERS,information:nil)
        NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_RESET_IDLE_TIMER_RIDE_HOME), object: nil, userInfo: nil)
        Acf().fetchWeeklyPlannerDataAndUpdateNotifications()
        animateSyncButton()
        loadUsersOnMapPrivate()
    }
    
    
    @IBAction func onClickOfNotificationButton(){
        if !isSystemReadyToProcessThis() { return }
        Acf().showNotificationScreen(nc: self.navigationController)
    }
    
    @IBAction func onClickOfWeeklyPlannerButton(){
        if !isSystemReadyToProcessThis() { return }
        Acf().showWeeklyPlannerScreen()
    }
}
