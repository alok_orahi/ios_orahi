//
//  CommonPickerViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit

class CommonPickerViewController : UIViewController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource {
    
    //MARK: - variables and constants
    @IBOutlet fileprivate weak var tableView : UITableView?
    @IBOutlet fileprivate weak var searchBar : UISearchBar!

    var optionList = NSMutableArray()
    fileprivate var optionListCopy = NSMutableArray()
    var titleToShow = "Select"
    var completion : ACFCompletionBlock?

    //MARK: - view controller life cycle methods
    
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
    }
    
    //MARK: - other methods
    private func setupForNavigationBar(){
        setAppearanceForNavigationBarType1(self.navigationController)
        setupNavigationBarTitleType1(titleToShow,viewController: self)
        if self.isBackButtonRequired() {
            addNavigationBarButton(self, image: UIImage(named: "backarrowblack"), title: nil, isLeft: true)
        }
    }
    
    func onClickOfLeftBarButton(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    private func registerForNotifications(){
        
    }
    
    private func startUpInitialisations(){
        registerNib("SingleLabelTableViewCell", tableView: tableView)
        setAppearanceForViewController(self)
        setAppearanceForTableView(tableView)
        searchBar.returnKeyType = .done
        optionListCopy.addObjects(from: optionList as [AnyObject])
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        
    }
    
    //MARK: - UITableView Delegate & Data Source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return optionList.count
    }
    
    @objc func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SingleLabelTableViewCell") as? SingleLabelTableViewCell
        cell?.isInitialisedOnce = true
        let title = optionList.object(at: (indexPath as NSIndexPath).row) as! String
        let attributesTitle = [NSFontAttributeName:UIFont.init(name: FONT_BOLD, size: 14)!,
            NSForegroundColorAttributeName:UIColor.darkGray] as NSDictionary
        let attributedString = NSMutableAttributedString(string:title)
        attributedString.setAttributes(attributesTitle as? [String : Any], range: NSMakeRange(0, title.length))
        cell?.titleLabel?.attributedText = attributedString
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if completion != nil{
            completion!(optionList.object(at: (indexPath as NSIndexPath).row))
            self.navigationController?.popViewController(animated: true)
        }
    }

    //MARK: - other functions
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        performSearch()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
            searchBar.resignFirstResponder()
    }

    func performSearch() {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(CommonPickerViewController.performSearchPrivate), object: nil)
        self.perform(#selector(CommonPickerViewController.performSearchPrivate), with: nil, afterDelay:0.4)
    }
    
    func performSearchPrivate() {
        if searchBar.text?.length > 0 {
            optionList.removeAllObjects()
            for o in optionListCopy {
                if (o as! NSString).lowercased.contains(searchBar.text!.lowercased()){
                    optionList.add((o as! NSString).copy())
                }
            }
        }else{
            optionList.addObjects(from: optionListCopy.copy() as! [Any])
        }
        tableView?.reloadData()
    }
    
}
