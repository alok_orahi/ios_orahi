//
//  BlockedUsersViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class BlockedUsersViewController: UIViewController,DZNEmptyDataSetDelegate,DZNEmptyDataSetSource,UIViewControllerPreviewingDelegate,UITableViewDelegate,UITableViewDataSource {
    
    //MARK: - variables and constants
    @IBOutlet fileprivate weak var tableView : UITableView?
    
    fileprivate var blockedUsers = NSMutableArray()
    
    //MARK: - view controller life cycle methods
    
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
    }
            
    //MARK: - other methods
    private func setupForNavigationBar(){
        setAppearanceForNavigationBarType1(self.navigationController)
        setupNavigationBarTitleType1("Blocked Users",viewController: self)
        if self.isBackButtonRequired() {
            addNavigationBarButton(self, image: UIImage(named: "backarrowblack"), title: nil, isLeft: true)
        }
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    func onClickOfLeftBarButton(_ sender:Any){
        Acf().updateFavouriteUsersPrioritySettingsOnServerIfRequired()
        self.navigationController?.popViewController(animated: true)
    }
    
    private func registerForNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(BlockedUsersViewController.updateUserInterfaceOnScreen), name: NSNotification.Name(rawValue: NOTIFICATION_BLOCK_LIST_UPDATED), object: nil)
    }
    
    private func startUpInitialisations(){
        registerNib("UserListTableViewCell", tableView: tableView)
        setAppearanceForViewController(self)
        setAppearanceForTableView(tableView)
        trackScreenLaunchEvent(SLE_BLOCKED_USERS)
        setupFor3DTouchPreviewIfAvailable()
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        self.blockedUsers = Acf().getBlockedUsers()
        self.tableView?.reloadData()
    }
    
    //MARK: - DZNEmptyDataSet Delegate & Data Source
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString!{
        let attributes = [NSFontAttributeName : UIFont.init(name: FONT_BOLD, size: 20)!]
        var message = "No blocked users"
        if (isInternetConnectivityAvailable(false)==false){
            message = "Unable to load blocked users"
        }
        if !isUserLoggedIn() {
            message = "Not Signed In"
        }
        return NSAttributedString(string: message, attributes: attributes)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString!{
        let attributes = [NSFontAttributeName : UIFont.init(name: FONT_REGULAR, size: 14)!]
        var message = "No block list available at the moment"
        if (isInternetConnectivityAvailable(false)==false){
            message = MESSAGE_TEXT___FOR_NETWORK_NOT_REACHABILITY
        }
        if !isUserLoggedIn() {
            message = "You are not signed in yet.\nPlease Sign In"
        }
        return NSAttributedString(string: message, attributes: attributes)
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage!{
        if !isUserLoggedIn() {
            return UIImage(named:"searchLightGray")
        }
        if (isInternetConnectivityAvailable(false)==false){
            return UIImage(named:"wifi")
        }
        return UIImage(named:"searchLightGray")
    }
    
    //MARK: - UITableView Delegate & Data Source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return blockedUsers.count
    }
    
    @objc func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 64
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserListTableViewCell") as? UserListTableViewCell
        cell?.userInfoAsQBUUser = blockedUsers.object(at: (indexPath as NSIndexPath).row) as? QBUUser
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    @objc func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String?{
        return "Un Block"
    }
    
    @objc func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            if isInternetConnectivityAvailable(true) == false {return}
            let user = blockedUsers.object(at: (indexPath as NSIndexPath).row) as! QBUUser
            let login = user.login
            let userId = Acf().getUserIdFrom(login)
            Acf().block(["userId":userId], isBlock:false)
            blockedUsers.removeObject(at: (indexPath as NSIndexPath).row)
            self.tableView?.reloadData()
        }
    }

    //MARK: - 3D Touch Setup and Support
    
    func setupFor3DTouchPreviewIfAvailable() {
        execMain ({ (returnedData) in
            if self.traitCollection.forceTouchCapability == .available {
                self.registerForPreviewing(with: self, sourceView: self.tableView!)
            }
            },delay:2)
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        guard let indexPath = self.tableView!.indexPathForRow(at: location) else { return nil }
        if let _ = tableView!.cellForRow(at: indexPath) as? UserListTableViewCell {
            let selectedCellFrame = tableView!.cellForRow(at: indexPath)!.frame
            let detailViewController = getViewController("ProfileOthersViewController") as! ProfileOthersViewController
            let qBUUser = self.blockedUsers[(indexPath as NSIndexPath).row] as! QBUUser
            let orahiId = Acf().getUserIdFromUserLogin(qBUUser.login!)
            detailViewController.userInfo = ["userId":orahiId]
            let gapX = 10.0 as CGFloat
            let gapY = 10.0 as CGFloat
            detailViewController.preferredContentSize = CGRect(x: gapX,y: gapY,width: DEVICE_WIDTH-2*gapX, height: DEVICE_HEIGHT-2*gapY).size
                previewingContext.sourceRect = selectedCellFrame
            return detailViewController
        }
        return nil
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        Acf().showViewControllerAsPopup(viewControllerToCommit)
    }
}
