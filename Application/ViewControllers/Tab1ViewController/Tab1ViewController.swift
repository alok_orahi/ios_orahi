//
//  Tab1ViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit

class Tab1ViewController: UIViewController {
    
    //MARK: - variables and constants
    fileprivate var rideHomeViewController:RideHomeViewController?
    fileprivate var rideDetailViewController:RideDetailViewController?
    
    //MARK: - view controller life cycle methods
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
    }
    
    //MARK: - other methods
    private func setupForNavigationBar(){
    }

    private func registerForNotifications(){
    }
    
    private func startUpInitialisations(){
        setAppearanceForViewController(self)
        execMain ({ (returnedData) in
            self.switchToViewController(true)
        },delay:0.2)
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        
    }
    
    func switchToViewController(_ trueForHomeFalseForDetail:Bool){
        addRideHomeViewController()
        addRideDetailViewController()
        rideHomeViewController?.rideDetailsViewController = rideDetailViewController
        if trueForHomeFalseForDetail {
            rideDetailViewController?.willMove(toParentViewController: nil)
            rideDetailViewController?.view.removeFromSuperview()
            rideDetailViewController?.removeFromParentViewController()
            
            addChildViewController(rideHomeViewController!)
            view.addSubview(rideHomeViewController!.view)
            rideHomeViewController?.didMove(toParentViewController: self)
        }else{
            rideHomeViewController?.willMove(toParentViewController: nil)
            rideHomeViewController?.view.removeFromSuperview()
            rideHomeViewController?.removeFromParentViewController()
            
            addChildViewController(rideDetailViewController!)
            view.addSubview(rideDetailViewController!.view)
            rideDetailViewController?.didMove(toParentViewController: self)
        }
    }
    
    func addRideHomeViewController(){
        if isNull(rideHomeViewController){
            rideHomeViewController = getViewController("RideHomeViewController") as? RideHomeViewController
            rideHomeViewController!.tab1ParentViewController = self
            self.addChildViewController(rideHomeViewController!)
        }
    }
    
    func addRideDetailViewController(){
        if isNull(rideDetailViewController){
            rideDetailViewController = getViewController("RideDetailViewController") as! RideDetailViewController
            rideDetailViewController!.tab1ParentViewController = self
            self.addChildViewController(rideDetailViewController!)
        }
    }
    
}
