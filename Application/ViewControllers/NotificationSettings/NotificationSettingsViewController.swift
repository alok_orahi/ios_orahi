//
//  NotificationSettingsViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit

class NotificationSettingsViewController: UIViewController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource {
    
    //MARK: - variables and constants
    fileprivate var segmentedControl : HMSegmentedControl?
    @IBOutlet fileprivate weak var tableView : UITableView?
    
    
    //MARK: - view controller life cycle methods
    
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
        Acf().syncSetting()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        Acf().syncSetting()
    }
    
    //MARK: - other methods
    private func setupForNavigationBar(){
        setAppearanceForNavigationBarType1(self.navigationController)
        setupNavigationBarTitleType1("Notification",viewController: self)
        if self.isBackButtonRequired() {
            addNavigationBarButton(self, image: UIImage(named: "backarrowblack"), title: nil, isLeft: true)
        }
    }
    
    func setupTopMenu(){
        if segmentedControl == nil {
            segmentedControl = HMSegmentedControl(sectionTitles: ["Mobile","Website","Email","Sms  "])
            segmentedControl?.frame = CGRect(x: 0,y: 0,width: self.view.frame.size.width - Acf().getRequiredPopupSideGap("")*2, height: 44)
            segmentedControl?.selectionIndicatorHeight = 2.0;
            segmentedControl?.backgroundColor = UIColor.white;
            segmentedControl?.selectedTitleTextAttributes = [NSForegroundColorAttributeName: APP_THEME_VOILET_COLOR,NSFontAttributeName:UIFont.init(name: FONT_SEMI_BOLD, size: 14)!]
            segmentedControl?.titleTextAttributes = [NSForegroundColorAttributeName: APP_THEME_LIGHT_GRAY_COLOR,NSFontAttributeName:UIFont.init(name: FONT_SEMI_BOLD, size: 14)!]
            segmentedControl!.selectionIndicatorColor = APP_THEME_VOILET_COLOR;
            segmentedControl!.selectionStyle = .fullWidthStripe;
            segmentedControl!.selectedSegmentIndex = 0;
            segmentedControl!.selectionIndicatorLocation = .down;
            segmentedControl!.shouldAnimateUserSelection = true;
            segmentedControl!.addTarget(self, action: #selector(NotificationSettingsViewController.segmentedControlValueChanged), for: UIControlEvents.valueChanged)
            self.view.addSubview(segmentedControl!)
        }
        tableView?.reloadData()
    }
    
    func onClickOfLeftBarButton(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    private func registerForNotifications(){
        
    }
    
    private func startUpInitialisations(){
        registerNib("SingleLabelAndSwitchTableViewCell", tableView: tableView)
        setAppearanceForViewController(self)
        setAppearanceForTableView(tableView)
        setupTopMenu()
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        
    }
    
    //MARK: - UITableView Delegate & Data Source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 13
    }
    
    @objc func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SingleLabelAndSwitchTableViewCell") as? SingleLabelAndSwitchTableViewCell
        var title = ""
        
        if (indexPath as NSIndexPath).row == 0{
            title = "Co-traveller picked up"
        }else if (indexPath as NSIndexPath).row == 1{
            title = "Feedback received"
        }else if (indexPath as NSIndexPath).row == 2{
            title = "In Sufficient balance"
        }else if (indexPath as NSIndexPath).row == 3{
            title = "Recharge"
        }else if (indexPath as NSIndexPath).row == 4{
            title = "Ride cancelled"
        }else if (indexPath as NSIndexPath).row == 5{
            title = "Ride completion pending"
        }else if (indexPath as NSIndexPath).row == 6{
            title = "Ride invite accepted"
        }else if (indexPath as NSIndexPath).row == 7{
            title = "Ride invite received"
        }else if (indexPath as NSIndexPath).row == 8{
            title = "Ride payment settled"
        }else if (indexPath as NSIndexPath).row == 9{
            title = "Sorry can't ride today"
        }else if (indexPath as NSIndexPath).row == 10{
            title = "Tokens received"
        }else if (indexPath as NSIndexPath).row == 11{
            title = "Withdrawl request"
        }else if (indexPath as NSIndexPath).row == 12{
            title = "Chat notification"
        }
        
        let attributesTitle = [NSFontAttributeName:UIFont.init(name: FONT_SEMI_BOLD, size: 13)!,
            NSForegroundColorAttributeName:UIColor.darkGray] as NSDictionary
        
        let attributedString = NSMutableAttributedString(string:title)
        attributedString.setAttributes(attributesTitle as? [String : Any], range: NSMakeRange(0, title.length))
        cell?.titleLabel?.attributedText = attributedString
        
        let setting = Dbm().getSetting()
        if segmentedControl?.selectedSegmentIndex == 0{//mobile
            if (indexPath as NSIndexPath).row == 0{
                cell?.controlSwitch?.isOn = (setting.notificationForMobileOption1?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 1{
                cell?.controlSwitch?.isOn = (setting.notificationForMobileOption2?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 2{
                cell?.controlSwitch?.isOn = (setting.notificationForMobileOption3?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 3{
                cell?.controlSwitch?.isOn = (setting.notificationForMobileOption4?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 4{
                cell?.controlSwitch?.isOn = (setting.notificationForMobileOption5?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 5{
                cell?.controlSwitch?.isOn = (setting.notificationForMobileOption6?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 6{
                cell?.controlSwitch?.isOn = (setting.notificationForMobileOption7?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 7{
                cell?.controlSwitch?.isOn = (setting.notificationForMobileOption8?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 8{
                cell?.controlSwitch?.isOn = (setting.notificationForMobileOption9?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 9{
                cell?.controlSwitch?.isOn = (setting.notificationForMobileOption10?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 10{
                cell?.controlSwitch?.isOn = (setting.notificationForMobileOption11?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 11{
                cell?.controlSwitch?.isOn = (setting.notificationForMobileOption12?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 12{
                cell?.controlSwitch?.isOn = (setting.notificationForMobileOption13?.boolValue)!
            }
        }else if segmentedControl?.selectedSegmentIndex == 1{//website
            if (indexPath as NSIndexPath).row == 0{
                cell?.controlSwitch?.isOn = (setting.notificationForWebsiteOption1?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 1{
                cell?.controlSwitch?.isOn = (setting.notificationForWebsiteOption2?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 2{
                cell?.controlSwitch?.isOn = (setting.notificationForWebsiteOption3?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 3{
                cell?.controlSwitch?.isOn = (setting.notificationForWebsiteOption4?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 4{
                cell?.controlSwitch?.isOn = (setting.notificationForWebsiteOption5?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 5{
                cell?.controlSwitch?.isOn = (setting.notificationForWebsiteOption6?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 6{
                cell?.controlSwitch?.isOn = (setting.notificationForWebsiteOption7?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 7{
                cell?.controlSwitch?.isOn = (setting.notificationForWebsiteOption8?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 8{
                cell?.controlSwitch?.isOn = (setting.notificationForWebsiteOption9?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 9{
                cell?.controlSwitch?.isOn = (setting.notificationForWebsiteOption10?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 10{
                cell?.controlSwitch?.isOn = (setting.notificationForWebsiteOption11?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 11{
                cell?.controlSwitch?.isOn = (setting.notificationForWebsiteOption12?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 12{
                cell?.controlSwitch?.isOn = (setting.notificationForWebsiteOption13?.boolValue)!
            }
        }else if segmentedControl?.selectedSegmentIndex == 2{//Email
            if (indexPath as NSIndexPath).row == 0{
                cell?.controlSwitch?.isOn = (setting.notificationForEmailOption1?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 1{
                cell?.controlSwitch?.isOn = (setting.notificationForEmailOption2?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 2{
                cell?.controlSwitch?.isOn = (setting.notificationForEmailOption3?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 3{
                cell?.controlSwitch?.isOn = (setting.notificationForEmailOption4?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 4{
                cell?.controlSwitch?.isOn = (setting.notificationForEmailOption5?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 5{
                cell?.controlSwitch?.isOn = (setting.notificationForEmailOption6?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 6{
                cell?.controlSwitch?.isOn = (setting.notificationForEmailOption7?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 7{
                cell?.controlSwitch?.isOn = (setting.notificationForEmailOption8?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 8{
                cell?.controlSwitch?.isOn = (setting.notificationForEmailOption9?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 9{
                cell?.controlSwitch?.isOn = (setting.notificationForEmailOption10?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 10{
                cell?.controlSwitch?.isOn = (setting.notificationForEmailOption11?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 11{
                cell?.controlSwitch?.isOn = (setting.notificationForEmailOption12?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 12{
                cell?.controlSwitch?.isOn = (setting.notificationForEmailOption13?.boolValue)!
            }
        }else if segmentedControl?.selectedSegmentIndex == 3{//SMS
            if (indexPath as NSIndexPath).row == 0{
                cell?.controlSwitch?.isOn = (setting.notificationForSMSOption1?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 1{
                cell?.controlSwitch?.isOn = (setting.notificationForSMSOption2?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 2{
                cell?.controlSwitch?.isOn = (setting.notificationForSMSOption3?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 3{
                cell?.controlSwitch?.isOn = (setting.notificationForSMSOption4?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 4{
                cell?.controlSwitch?.isOn = (setting.notificationForSMSOption5?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 5{
                cell?.controlSwitch?.isOn = (setting.notificationForSMSOption6?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 6{
                cell?.controlSwitch?.isOn = (setting.notificationForSMSOption7?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 7{
                cell?.controlSwitch?.isOn = (setting.notificationForSMSOption8?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 8{
                cell?.controlSwitch?.isOn = (setting.notificationForSMSOption9?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 9{
                cell?.controlSwitch?.isOn = (setting.notificationForSMSOption10?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 10{
                cell?.controlSwitch?.isOn = (setting.notificationForSMSOption11?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 11{
                cell?.controlSwitch?.isOn = (setting.notificationForSMSOption12?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 12{
                cell?.controlSwitch?.isOn = (setting.notificationForSMSOption13?.boolValue)!
            }
        }
        cell?.controlSwitch?.addTarget(self, action: #selector(NotificationSettingsViewController.onValueChangeOfSwitch(_:)), for: UIControlEvents.valueChanged)
        cell?.controlSwitch?.tag = (indexPath as NSIndexPath).row
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as? SingleLabelAndSwitchTableViewCell
        performAnimatedClickEffectType1(cell!.titleLabel!)
        cell?.controlSwitch?.setOn((cell?.controlSwitch?.isOn == false), animated: true)
        onValueChangeOfSwitch((cell?.controlSwitch)!)
    }
    
    func onValueChangeOfSwitch(_ sender:UISwitch){
        let setting = Dbm().getSetting()
        if segmentedControl?.selectedSegmentIndex == 0{//mobile
            if sender.tag == 0{
                setting.notificationForMobileOption1 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 1{
                setting.notificationForMobileOption2 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 2{
                setting.notificationForMobileOption3 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 3{
                setting.notificationForMobileOption4 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 4{
                setting.notificationForMobileOption5 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 5{
                setting.notificationForMobileOption6 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 6{
                setting.notificationForMobileOption7 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 7{
                setting.notificationForMobileOption8 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 8{
                setting.notificationForMobileOption9 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 9{
                setting.notificationForMobileOption10 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 10{
                setting.notificationForMobileOption11 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 11{
                setting.notificationForMobileOption12 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 12{
                setting.notificationForMobileOption13 = NSNumber(value: sender.isOn as Bool)
            }
            setting.isNotificationsSettingsMobileUpdated = NSNumber(value: true)
        }else if segmentedControl?.selectedSegmentIndex == 1{//website
            if sender.tag == 0{
                setting.notificationForWebsiteOption1 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 1{
                setting.notificationForWebsiteOption2 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 2{
                setting.notificationForWebsiteOption3 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 3{
                setting.notificationForWebsiteOption4 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 4{
                setting.notificationForWebsiteOption5 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 5{
                setting.notificationForWebsiteOption6 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 6{
                setting.notificationForWebsiteOption7 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 7{
                setting.notificationForWebsiteOption8 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 8{
                setting.notificationForWebsiteOption9 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 9{
                setting.notificationForWebsiteOption10 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 10{
                setting.notificationForWebsiteOption11 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 11{
                setting.notificationForWebsiteOption12 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 12{
                setting.notificationForWebsiteOption13 = NSNumber(value: sender.isOn as Bool)
            }
            setting.isNotificationsSettingsWebsiteUpdated = NSNumber(value: true)
        }else if segmentedControl?.selectedSegmentIndex == 2{//Email
            if sender.tag == 0{
                setting.notificationForEmailOption1 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 1{
                setting.notificationForEmailOption2 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 2{
                setting.notificationForEmailOption3 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 3{
                setting.notificationForEmailOption4 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 4{
                setting.notificationForEmailOption5 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 5{
                setting.notificationForEmailOption6 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 6{
                setting.notificationForEmailOption7 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 7{
                setting.notificationForEmailOption8 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 8{
                setting.notificationForEmailOption9 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 9{
                setting.notificationForEmailOption10 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 10{
                setting.notificationForEmailOption11 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 11{
                setting.notificationForEmailOption12 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 12{
                setting.notificationForEmailOption13 = NSNumber(value: sender.isOn as Bool)
            }
            setting.isNotificationsSettingsEmailUpdated = NSNumber(value: true)
        }else if segmentedControl?.selectedSegmentIndex == 3{//SMS
            if sender.tag == 0{
                setting.notificationForSMSOption1 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 1{
                setting.notificationForSMSOption2 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 2{
                setting.notificationForSMSOption3 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 3{
                setting.notificationForSMSOption4 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 4{
                setting.notificationForSMSOption5 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 5{
                setting.notificationForSMSOption6 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 6{
                setting.notificationForSMSOption7 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 7{
                setting.notificationForSMSOption8 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 8{
                setting.notificationForSMSOption9 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 9{
                setting.notificationForSMSOption10 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 10{
                setting.notificationForSMSOption11 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 11{
                setting.notificationForSMSOption12 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 12{
                setting.notificationForSMSOption13 = NSNumber(value: sender.isOn as Bool)
            }
            setting.isNotificationsSettingsSMSUpdated = NSNumber(value: true)
        }
        Dbm().saveChangesLazily()
    }
    
    //MARK: - other functions
    
    func segmentedControlValueChanged(){
        tableView?.reloadData()
    }
    
}
