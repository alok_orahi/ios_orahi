//
//  SearchOrahiUsersViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

//MARK: - SearchStatus
enum SearchStatus {
    case notSearched
    case searchedWithNoResults
    case searchedWithResults
}

class SearchOrahiUsersViewController: UIViewController,DZNEmptyDataSetDelegate,DZNEmptyDataSetSource,UITableViewDelegate,UITableViewDataSource {
    
    //MARK: - variables and constants
    @IBOutlet fileprivate weak var tableView : UITableView!
    @IBOutlet fileprivate weak var searchBar : UISearchBar!
    
    fileprivate var searchStatus : SearchStatus = SearchStatus.notSearched
    fileprivate var searchResults = NSMutableArray()
    
    //MARK: - view controller life cycle methods
    
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
    }
    
    //MARK: - other methods
    private func setupForNavigationBar(){
        setAppearanceForNavigationBarType1(self.navigationController)
        setupNavigationBarTitleType1("Search",viewController: self)
        if self.isBackButtonRequired() {
            addNavigationBarButton(self, image: UIImage(named: "backarrowblack"), title: nil, isLeft: true)
        }
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    func onClickOfLeftBarButton(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    private func registerForNotifications(){
        
    }
    
    private func startUpInitialisations(){
        registerNib("UserListTableViewCell", tableView: tableView)
        setAppearanceForViewController(self)
        setAppearanceForTableView(tableView)
        trackScreenLaunchEvent(SLE_SEARCH_ORAHI_USERS)
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        tableView?.reloadData()
    }
    
    //MARK: - DZNEmptyDataSet Delegate & Data Source
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString!{
        let attributes = [NSFontAttributeName : UIFont.init(name: FONT_SEMI_BOLD, size: 20)!]
        var message = ""
        if searchStatus == SearchStatus.notSearched {
            message = ""
        }else if searchStatus == SearchStatus.searchedWithNoResults {
            message = "No search results"
            if (isInternetConnectivityAvailable(false)==false){
                message = "Unable to perform search"
            }
        }else if searchStatus == SearchStatus.searchedWithResults {
            message = ""
        }
        return NSAttributedString(string: message, attributes: attributes)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString!{
        let attributes = [NSFontAttributeName : UIFont.init(name: FONT_SEMI_BOLD, size: 14)!]
        var message = ""
        if searchStatus == SearchStatus.notSearched {
            message = "you can search users by their names"
        }else if searchStatus == SearchStatus.searchedWithNoResults {
            message = "try searching with different name"
            if (isInternetConnectivityAvailable(false)==false){
                message = MESSAGE_TEXT___FOR_NETWORK_NOT_REACHABILITY
            }
        }else if searchStatus == SearchStatus.searchedWithResults {
            message = ""
        }
        return NSAttributedString(string: message, attributes: attributes)
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage!{
        if searchStatus == SearchStatus.notSearched {
            return UIImage(named:"searchLightGray")
        }else if searchStatus == SearchStatus.searchedWithNoResults {
            if (isInternetConnectivityAvailable(false)==false){
                return UIImage(named:"wifi")
            }
            return UIImage(named:"searchLightGray")
        }else if searchStatus == SearchStatus.searchedWithResults {
            return UIImage(named:"searchLightGray")
        }
        return nil
    }
    
    //MARK: - UITableView Delegate & Data Source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResults.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 64
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserListTableViewCell") as? UserListTableViewCell
        cell?.userInfoAsDictionary = searchResults.object(at: (indexPath as NSIndexPath).row) as? NSDictionary
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let userInfo =  searchResults.object(at: (indexPath as NSIndexPath).row) as? NSDictionary
        showUserProfile(userInfo!,navigationController: self.navigationController!)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool){
        searchBar.resignFirstResponder()
    }
    
    //MARK: - Action methods
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        if searchText.length>0 {
            performSearch()
            self.tableView?.reloadEmptyDataSet()
        } else {
            self.searchStatus = SearchStatus.notSearched
            self.tableView?.reloadEmptyDataSet()
            searchResults.removeAllObjects()
            tableView?.reloadData()
        }
    }
    
    func performSearch() {
        loadFromCache()
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(SearchOrahiUsersViewController.performSearchPrivate), object: nil)
        self.perform(#selector(SearchOrahiUsersViewController.performSearchPrivate), with: nil, afterDelay: 1.2)
    }
    
    func loadFromCache() {
        if searchBar.text?.length > 0 {
            if let cachedSearchResults = CacheManager.sharedInstance.loadObject("CACHED_SEARCH_TEXT_\(searchBar.text!)") as? NSMutableArray {
                self.setDataFromArray(cachedSearchResults)
            }
        }
    }
    
    func performSearchPrivate() {
        if (isInternetConnectivityAvailable(false)==false){
            self.searchStatus = SearchStatus.searchedWithNoResults;
            self.tableView?.reloadData()
        }else {
            self.tableView?.reloadData()
            if searchBar.text?.length > 0 {
                self.searchBar.showActivityIndicator()
                let cachedSearchResults = CacheManager.sharedInstance.loadObject("CACHED_SEARCH_TEXT_\(searchBar.text!)")
                if isNotNull(cachedSearchResults){
                    self.setDataFromArray(cachedSearchResults as! NSMutableArray)
                }
                let information = ["keyword":(searchBar.text)!] as NSDictionary
                let scms = ServerCommunicationManager()
                scms.returnFailureResponseAlso = true
                scms.responseErrorOption = .dontShowErrorResponseMessage
                scms.searchUsers(information) { (responseData) -> () in
                    if let _ = responseData {
                        if self.searchBar?.text?.length > 0 {
                            if let searchResult = responseData?.object(forKey: "SEARCH_RESULT") as? NSMutableArray{
                                self.setDataFromArray(searchResult)
                                CacheManager.sharedInstance.saveObject(searchResult, identifier: "CACHED_SEARCH_TEXT_\(self.searchBar.text!)")
                            }
                        }
                    }
                    self.searchBar?.hideActivityIndicator()
                    if self.searchResults.count > 0 {
                        self.searchStatus = SearchStatus.searchedWithResults
                    }else{
                        self.searchStatus = SearchStatus.searchedWithNoResults
                    }
                }
            }
        }
    }
    
    func setDataFromArray(_ results:NSMutableArray?){
        self.searchResults = results!
        self.tableView?.reloadData()
    }
}
