//
//  OtherOptionsViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit
import CTFeedback

class OtherOptionsViewController: UIViewController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource {
    
    //MARK: - variables and constants
    @IBOutlet fileprivate weak var tableView : UITableView?
    
    
    //MARK: - view controller life cycle methods
    
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
    }
    
    //MARK: - other methods
    private func setupForNavigationBar(){
        setAppearanceForNavigationBarType1(self.navigationController)
        setupNavigationBarTitleType1("Help",viewController: self)
        if self.isBackButtonRequired() {
            addNavigationBarButton(self, image: UIImage(named: "backarrowblack"), title: nil, isLeft: true)
        }
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    func onClickOfLeftBarButton(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    private func registerForNotifications(){
        
    }
    
    private func startUpInitialisations(){
        registerNib("SingleLabelTableViewCell", tableView: tableView)
        setAppearanceForViewController(self)
        setAppearanceForTableView(tableView)
        Acf().setupDeveloperSecretOption(self.tableView!)
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        
    }
    
    //MARK: - UITableView Delegate & Data Source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ENABLE_SECRET_RESET_BUTTON ? 4 : 3
    }
    
    @objc func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SingleLabelTableViewCell") as? SingleLabelTableViewCell
        cell?.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        if (indexPath as NSIndexPath).row == 0{
            cell?.titleLabel?.text = "Support"
        }
        if (indexPath as NSIndexPath).row == 1{
            cell?.titleLabel?.text = "About Us"
        }
        if (indexPath as NSIndexPath).row == 2{
            cell?.titleLabel?.text = "Terms & Conditions"
        }
        if (indexPath as NSIndexPath).row == 3{
            cell?.titleLabel?.text = "Reset"
        }
        return cell!
    }
    
    open func selectRow(indexPath: IndexPath){
        self.tableView(self.tableView!, didSelectRowAt: indexPath)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? SingleLabelTableViewCell {
            performAnimatedClickEffectType1(cell.titleLabel!)
        }
        if (indexPath as NSIndexPath).row == 0{
            if !isSystemReadyToProcessThis() { return }
            let actionSheet = Hokusai()
            actionSheet.addButton("Rate Us") {
                Acf().askUserForHisExperience()
            }
            actionSheet.addButton("Feedback") {
                Acf().hidePopupViewController()
                execMain({[weak self] in guard let `self` = self else { return }
                    
                    trackScreenLaunchEvent(SLE_SUPPORT)
                    let feedbackVC = CTFeedbackViewController(topics: CTFeedbackViewController.defaultTopics(),localizedTopics: CTFeedbackViewController.defaultLocalizedTopics())
                    feedbackVC?.toRecipients = [SUPPORT_EMAIL]
                    feedbackVC?.useHTML = false
                    Acf().navigationController?.present(UINavigationController(rootViewController: feedbackVC!), animated: true, completion: nil)
                    },delay:0.5)
            }
            actionSheet.addButton("Contact Us") {
                if(isInternetConnectivityAvailable(true)==false){return}
                Acf().pushVC("WebViewViewController", navigationController: self.navigationController, isRootViewController: false, animated: true) { (viewControllerObject) -> () in
                    (viewControllerObject as! WebViewViewController).workingMode = WorkingMode.contactUs
                }
            }
            actionSheet.addButton("FAQ's") {
                if(isInternetConnectivityAvailable(true)==false){return}
                Acf().pushVC("WebViewViewController", navigationController: self.navigationController, isRootViewController: false, animated: true) { (viewControllerObject) -> () in
                    (viewControllerObject as! WebViewViewController).workingMode = WorkingMode.faq
                }
            }
            actionSheet.cancelButtonTitle = "Cancel"
            actionSheet.show()
        }else if (indexPath as NSIndexPath).row == 1{
            if(isInternetConnectivityAvailable(true)==false){return}
            let actionSheet = Hokusai()
            actionSheet.addButton("About Orahi") {
                if(isInternetConnectivityAvailable(true)==false){return}
                Acf().pushVC("WebViewViewController", navigationController: self.navigationController, isRootViewController: false, animated: true) { (viewControllerObject) -> () in
                    (viewControllerObject as! WebViewViewController).workingMode = WorkingMode.aboutUs
                }
            }
            actionSheet.addButton("Orahi On Facebook") {
                Acf().pushVC("WebViewViewController", navigationController: self.navigationController, isRootViewController: false, animated: true) { (viewControllerObject) -> () in
                    (viewControllerObject as! WebViewViewController).workingMode = WorkingMode.orahiOnFacebook
                }
            }
            actionSheet.addButton("Orahi On Twitter") {
                Acf().pushVC("WebViewViewController", navigationController: self.navigationController, isRootViewController: false, animated: true) { (viewControllerObject) -> () in
                    (viewControllerObject as! WebViewViewController).workingMode = WorkingMode.orahiOnTwitter
                }
            }
            actionSheet.addButton("Orahi On YouTube") {
                trackScreenLaunchEvent(SLE_ORAHI_ON_YOUTUBE)
                openUrl(APP_YOUTUBE_PAGE_LINK)
            }
            actionSheet.addButton("Orahi On LinkedIn") {
                Acf().pushVC("WebViewViewController", navigationController: self.navigationController, isRootViewController: false, animated: true) { (viewControllerObject) -> () in
                    (viewControllerObject as! WebViewViewController).workingMode = WorkingMode.orahiOnLinkedIn
                }
            }
            actionSheet.cancelButtonTitle = "Cancel"
            actionSheet.show()
        }else if (indexPath as NSIndexPath).row == 2{
            if(isInternetConnectivityAvailable(true)==false){return}
            Acf().pushVC("WebViewViewController", navigationController: self.navigationController, isRootViewController: false, animated: true) { (viewControllerObject) -> () in
                (viewControllerObject as! WebViewViewController).workingMode = WorkingMode.termsAndConditions
            }
        }else if indexPath.row == 3{
            let actionSheet = Hokusai()
            actionSheet.addButton("Reset") {
                Acf().reset()
            }
            actionSheet.cancelButtonTitle = "Cancel"
            actionSheet.show()
        }
    }
    
    //MARK: - other functions
    
}
