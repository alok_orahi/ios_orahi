//
//  AccountSummaryViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit

class AccountSummaryViewController: UIViewController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource {
    
    //MARK: - variables and constants
    @IBOutlet fileprivate weak var tableView : UITableView?
    @IBOutlet fileprivate weak var accountBalanceLabel : UILabel?
    
    fileprivate var transactions = NSMutableArray()


    //MARK: - view controller life cycle methods
    
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
    }
    
    //MARK: - other methods
    private func setupForNavigationBar(){
        setAppearanceForNavigationBarType1(self.navigationController)
        setupNavigationBarTitleType1("Account Summary",viewController: self)
        if self.isBackButtonRequired() {
            addNavigationBarButton(self, image: UIImage(named: "backarrowblack"), title: nil, isLeft: true)
        }
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    func onClickOfLeftBarButton(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    private func registerForNotifications(){
        
    }
    
    private func startUpInitialisations(){
        registerNib("AccountSummaryTableViewCell", tableView: tableView)
        setAppearanceForViewController(self)
        setAppearanceForTableView(tableView)
        fetchAndShowAccountSummary()
        trackScreenLaunchEvent(SLE_ACCOUNT_SUMMARY)
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        
    }
    
    func fetchAndShowAccountSummary(){
        if isNotNull(CacheManager.sharedInstance.loadObject("ACCOUNT_SUMMARY")){
            let details = CacheManager.sharedInstance.loadObject("ACCOUNT_SUMMARY") as! NSDictionary
            self.accountBalanceLabel?.text = details.object(forKey: "accountBal") as? String
            if details.object(forKey: "summary_table") is NSArray {
                self.transactions = details.object(forKey: "summary_table") as! NSMutableArray
            }else{
                self.transactions = NSMutableArray()
            }
            self.tableView?.reloadData()
        }
        self.view?.showActivityIndicator()
        let information = NSMutableDictionary()
        copyData(loggedInUserId() , destinationDictionary: information, destinationKey: "userId", methodName: #function)
        let scm = ServerCommunicationManager()
        scm.responseErrorOption = ResponseErrorOption.dontShowErrorResponseMessage
        scm.returnFailureResponseAlso = true
        scm.getAccountSummary(information, completionBlock: {[weak self] (responseData) -> () in guard let `self` = self else { return }
            if isNotNull(responseData?.object(forKey: "data")){
                trackFunctionalEvent(FE_SCREEN_DATA_LOADED, information: ["screen":"account_summary_screen"])
                let data = (responseData?.object(forKey: "data")) as! NSDictionary
                self.accountBalanceLabel?.text = data.object(forKey: "accountBal") as? String
                if data.object(forKey: "summary_table") is NSArray {
                    self.transactions = data.object(forKey: "summary_table") as! NSMutableArray
                }else{
                    self.transactions = NSMutableArray()
                }
                CacheManager.sharedInstance.saveObject(data, identifier: "ACCOUNT_SUMMARY")
                self.tableView?.reloadData()
                if let balance = self.accountBalanceLabel?.text?.toFloat() {
                    if balance < 100.0 {
                        trackFunctionalEvent(FE_LOW_BALANCE, information: ["balance":"\(self.accountBalanceLabel!.text)"])
                    }
                }
            }else{
                trackFunctionalEvent(FE_SCREEN_DATA_NOT_LOADED, information: ["screen":"account_summary_screen"])
            }
            self.view?.hideActivityIndicator()
        })
    }
    
    //MARK: - DZNEmptyDataSet Delegate & Data Source
    
    func titleForEmptyDataSet(_ scrollView: UIScrollView!) -> NSAttributedString!{
        let attributes = [NSFontAttributeName : UIFont.init(name: FONT_BOLD, size: 20)!]
        var message = "No summary"
        if (isInternetConnectivityAvailable(false)==false){
            message = "Unable to load summary"
        }
        return NSAttributedString(string: message, attributes: attributes)
    }
    
    func descriptionForEmptyDataSet(_ scrollView: UIScrollView!) -> NSAttributedString!{
        let attributes = [NSFontAttributeName : UIFont.init(name: FONT_REGULAR, size: 14)!]
        var message = "No summary available at the moment"
        if (isInternetConnectivityAvailable(false)==false){
            message = MESSAGE_TEXT___FOR_NETWORK_NOT_REACHABILITY
        }
        return NSAttributedString(string: message, attributes: attributes)
    }
    
    func imageForEmptyDataSet(_ scrollView: UIScrollView!) -> UIImage!{
        if !isUserLoggedIn() {
            return UIImage(named:"searchLightGray")
        }
        if (isInternetConnectivityAvailable(false)==false){
            return UIImage(named:"wifi")
        }
        return UIImage(named:"searchLightGray")
    }
    
    func emptyDataSetDidTapView(_ scrollView: UIScrollView!){
    }
    
    //MARK: - UITableView Delegate & Data Source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return transactions.count + 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 56
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AccountSummaryTableViewCell") as? AccountSummaryTableViewCell
        if (indexPath as NSIndexPath).row == 0{
            cell?.infoLabel1?.text = "Date"
            cell?.infoLabel2?.text = "Dr/Cr"
            cell?.infoLabel3?.text = "Balance"
            cell?.infoLabel4?.text = "Remarks"
            cell?.infoLabel1?.font = UIFont.init(name: FONT_SEMI_BOLD, size: 13)
            cell?.infoLabel1?.textColor = APP_THEME_VOILET_COLOR
            cell?.infoLabel2?.font = UIFont.init(name: FONT_SEMI_BOLD, size: 13)
            cell?.infoLabel2?.textColor = APP_THEME_VOILET_COLOR
            cell?.infoLabel3?.font = UIFont.init(name: FONT_SEMI_BOLD, size: 13)
            cell?.infoLabel3?.textColor = APP_THEME_VOILET_COLOR
            cell?.infoLabel4?.font = UIFont.init(name: FONT_SEMI_BOLD, size: 13)
            cell?.infoLabel4?.textColor = APP_THEME_VOILET_COLOR
        }else{
            let transaction = transactionDictionary(transactions.object(at: (indexPath as NSIndexPath).row-1) as! NSArray)
            cell?.infoLabel1?.text = transaction.object(forKey: "date") as? String
            cell?.infoLabel2?.text = transaction.object(forKey: "amount") as? String
            cell?.infoLabel3?.text = transaction.object(forKey: "balance") as? String
            cell?.infoLabel4?.text = transaction.object(forKey: "system") as? String
            
            var textInfoLabel2 = cell!.infoLabel2!.text!
            
            textInfoLabel2 = textInfoLabel2.replacingOccurrences(of: " ", with: "")
            textInfoLabel2 = textInfoLabel2.replacingOccurrences(of: "[", with: "")
            textInfoLabel2 = textInfoLabel2.replacingOccurrences(of: "]", with: "")
            textInfoLabel2 = textInfoLabel2.substring(from: textInfoLabel2.characters.index(textInfoLabel2.startIndex, offsetBy: textInfoLabel2.length-1)) + textInfoLabel2
            textInfoLabel2 = textInfoLabel2.substring(to: textInfoLabel2.characters.index(textInfoLabel2.startIndex, offsetBy: textInfoLabel2.length-1))
            cell?.infoLabel2?.text = textInfoLabel2
            
            cell?.infoLabel1?.font = UIFont.init(name: FONT_SEMI_BOLD, size: 10)
            cell?.infoLabel1?.textColor = UIColor.darkGray
            cell?.infoLabel2?.font = UIFont.init(name: FONT_SEMI_BOLD, size: 13)
            cell?.infoLabel2?.textColor = UIColor.darkGray
            cell?.infoLabel3?.font = UIFont.init(name: FONT_SEMI_BOLD, size: 10)
            cell?.infoLabel3?.textColor = UIColor.gray
            cell?.infoLabel4?.font = UIFont.init(name: FONT_SEMI_BOLD, size: 13)
            cell?.infoLabel4?.textColor = UIColor.gray
            if (transaction.object(forKey: "amount") as! String).contains("+") {
                cell?.infoLabel2?.textColor = APP_THEME_VOILET_COLOR
            }else{
                cell?.infoLabel2?.textColor = APP_THEME_RED_COLOR
            }
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }

    
    
    func transactionDictionary(_ transaction:NSArray)->NSDictionary{
        let transactionAsDictionary = NSMutableDictionary()
        transactionAsDictionary.setObject(transaction.object(at: 0) as! String, forKey: "date" as NSCopying)
        transactionAsDictionary.setObject(transaction.object(at: 6) as! String, forKey: "system" as NSCopying)
        transactionAsDictionary.setObject(transaction.object(at: 4) as! String, forKey: "amount" as NSCopying)
        transactionAsDictionary.setObject(transaction.object(at: 5) as! String, forKey: "balance" as NSCopying)
        return transactionAsDictionary
    }
    
    //MARK: - other functions
    
}
