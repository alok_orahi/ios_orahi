//
//  SearchUsersViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

typealias SUVCCompletionBlock = (_ returnedData :Any?) ->()

class SearchUsersViewController: UsersListTableViewController {
    //MARK: - variables and constants
    
    @IBOutlet fileprivate weak var searchBar : UISearchBar?
    fileprivate var selections = NSMutableDictionary()
    var completionBlock : SUVCCompletionBlock?

    //MARK: - view controller life cycle methods
    
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
        super.viewDidLoad()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
        updateUserInterfaceOnScreen()
    }
    
    
    //MARK: - other methods
    private func setupForNavigationBar(){
        setAppearanceForNavigationBarType1(self.navigationController)
        setupNavigationBarTitleType1("Search",viewController: self.parent)
        if (self.navigationController?.viewControllers.count)! > 1 {
            addNavigationBarButton(self, image: UIImage(named: "backarrowblack"), title: nil, isLeft: true)
        }
        updateRightNavigationBarButton()
    }
    
    func updateRightNavigationBarButton() {
        if selections.allKeys.count > 0 {
            addNavigationBarButton(self, image: nil, title: "Done", isLeft: false, observer: self)
        }else{
            self.navigationItem.rightBarButtonItems = nil
        }
    }
    
    func onClickOfLeftBarButton(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    func onClickOfRightBarButton(_ sender:Any){
        if(isInternetConnectivityAvailable(true)==false){return}
        let selectedIDs = selections.allValues as! [String]
        let users: [QBUUser] = Acf().getUsersWithQBIDs(selectedIDs)
        ServicesManager.instance().usersService.addUsers(onCache: users)
        if completionBlock != nil {
            completionBlock!(users)
        }
        NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_UPDATE_USERS), object: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    private func registerForNotifications(){
    }
    
    private func startUpInitialisations(){
        registerNib("UserListTableViewCell", tableView: self.tableView)
        setAppearanceForViewController(self)
        setupForQuickBlox()
        isSearchingMode = true
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        self.tableView.reloadData()
    }
    
    func setupForQuickBlox(){
    }
    
    func updateUsers() {
    }
    
    override func setupUsers(_ users: [QBUUser]) {
        if isNotNull(users) && users.count>0 && Acf().isQBReadyForItsUserDependentServices(){
            let filteredUsers = users.filter({($0 as QBUUser).id != ServicesManager.instance().currentUser()!.id})
            super.setupUsers(filteredUsers)
        }
    }
    
    // MARK: - UITableViewDelegate & UITableViewDataSource
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 64
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserListTableViewCell", for: indexPath) as! UserListTableViewCell
        let user = self.users![(indexPath as NSIndexPath).row]
        cell.userInfoAsQBUUser = user
        cell.tag = (indexPath as NSIndexPath).row
        if isNotNull(selections.object(forKey: "\(user.id)")){
            cell.isSelected = true
            cell.accessoryType = .checkmark
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        }else{
            cell.accessoryType = .none
            cell.isSelected = false
            tableView.deselectRow(at: indexPath, animated: false)
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let user = self.users![(indexPath as NSIndexPath).row]
        selections.setObject("\(user.id)", forKey: "\(user.id)" as NSCopying)
        tableView.reloadRows(at: [indexPath], with: .automatic)
        self.updateRightNavigationBarButton()
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let user = self.users![(indexPath as NSIndexPath).row]
        selections.removeObject(forKey: "\(user.id)")
        tableView.reloadRows(at: [indexPath], with: .automatic)
        self.updateRightNavigationBarButton()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        performSearch()
        self.tableView?.reloadEmptyDataSet()
    }
    
    func performSearch() {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(SearchUsersViewController.performSearchPrivate), object: nil)
        self.perform(#selector(SearchUsersViewController.performSearchPrivate), with: nil, afterDelay: 1.2)
    }
    
    func performSearchPrivate() {
        setupForUsers(searchBar?.text)
    }
}
