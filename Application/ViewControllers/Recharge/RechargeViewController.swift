//
//  RechargeViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit

class RechargeViewController: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource {
    
    //MARK: - view controller life cycle methods
    @IBOutlet fileprivate weak var directOption1Button : UIButton!
    @IBOutlet fileprivate weak var directOption2Button : UIButton!
    @IBOutlet fileprivate weak var directOption3Button : UIButton!
    @IBOutlet fileprivate weak var directOption1CheckBoxImageView : UIImageView!
    @IBOutlet fileprivate weak var directOption2CheckBoxImageView : UIImageView!
    @IBOutlet fileprivate weak var directOption3CheckBoxImageView : UIImageView!
    @IBOutlet fileprivate weak var allOptionsPickerView : UIPickerView!
    
    fileprivate var rechargeOptions = [String]()
    fileprivate var selection = 1
    fileprivate var selectionIndexOf400 = 1
    var preSelectionAmount = ""
    
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
    }
    
    //MARK: - other methods
    private func setupForNavigationBar(){
        setAppearanceForNavigationBarType1(self.navigationController)
        setupNavigationBarTitleType1("Recharge",viewController: self)
        if self.isBackButtonRequired() {
            addNavigationBarButton(self, image: UIImage(named: "backarrowblack"), title: nil, isLeft: true)
        }
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    func onClickOfLeftBarButton(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    private func registerForNotifications(){
        
    }
    
    private func startUpInitialisations(){
        let minimumRechargeOption = minimumRechargeValue()
        var amountSelected = defaultRechargeValue()
        if isNotNull(preSelectionAmount){
            amountSelected = safeInt(preSelectionAmount, alternate: defaultRechargeValue())
        }
        rechargeOptions = ["200","400","600","800","1000","1200","1400","1600","1800","2000"]
        if minimumRechargeOption < safeInt(rechargeOptions[0]) {
            rechargeOptions = ["\(minimumRechargeOption)","200","400","600","800","1000","1200","1400","1600","1800","2000"]
        }
        selection = 1
        selectionIndexOf400 = 1
        for (index,rv) in rechargeOptions.enumerated() {
            if rv == "\(amountSelected)" {
                selection = index
            }
            if rv == "400" {
                selectionIndexOf400 = index
            }
        }
        addShadow(layer: self.directOption1Button.layer)
        addShadow(layer: self.directOption2Button.layer)
        addShadow(layer: self.directOption3Button.layer)
        allOptionsPickerView.reloadAllComponents()
        setAppearanceForViewController(self)
        trackScreenLaunchEvent(SLE_RECHARGE)
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        
        func configure(button:UIButton, string:String,highlight:Bool){
            if highlight {
                performAnimatedClickEffectType1(button)
                button.titleLabel?.font = UIFont(name: FONT_REGULAR, size: 19)
            }else{
                button.titleLabel?.font = UIFont(name: FONT_REGULAR, size: 17)
            }
            let mutableAttributedString = NSMutableAttributedString(string: string, attributes: [NSForegroundColorAttributeName:APP_THEME_LIGHT_GRAY_COLOR])
            mutableAttributedString.applyAttributesBySearching("₹", attributes: [NSFontAttributeName:UIFont(name: FONT_SEMI_BOLD, size: 13),NSForegroundColorAttributeName:UIColor.gray,NSBaselineOffsetAttributeName:3])
            button.setAttributedTitle(mutableAttributedString, for: .normal)
        }
        
        configure(button: directOption1Button, string: "₹\(rechargeOptions[selectionIndexOf400 + 0])", highlight: selection == (selectionIndexOf400 + 0))
        configure(button: directOption2Button, string: "₹\(rechargeOptions[selectionIndexOf400 + 1])", highlight: selection == (selectionIndexOf400 + 1))
        configure(button: directOption3Button, string: "₹\(rechargeOptions[selectionIndexOf400 + 2])", highlight: selection == (selectionIndexOf400 + 2))

        directOption1CheckBoxImageView.isHidden = selection != selectionIndexOf400
        directOption2CheckBoxImageView.isHidden = selection != (selectionIndexOf400 + 1)
        directOption3CheckBoxImageView.isHidden = selection != (selectionIndexOf400 + 2)
        
        execMain({[weak self] in guard let `self` = self else { return }

            self.allOptionsPickerView.selectRow(self.selection, inComponent: 0, animated: true)
        }, delay: 0.3)
    }
    
    func addShadow(layer:CALayer){
        layer.cornerRadius = 2
        layer.shadowColor = APP_THEME_LIGHT_GRAY_COLOR.cgColor
        layer.shadowOffset = CGSize.zero
        layer.shadowOpacity = 0.3
        layer.shadowRadius = 5.0
        layer.borderWidth = 0.5
        layer.borderColor = UIColor.white.cgColor
        layer.backgroundColor = UIColor.white.cgColor
    }
    
    @IBAction func onClickOfDirectOption1Button(){
        selection = (selectionIndexOf400 + 0)
        updateUserInterfaceOnScreen()
    }
    
    @IBAction func onClickOfDirectOption2Button(){
        selection = (selectionIndexOf400 + 1)
        updateUserInterfaceOnScreen()
    }
    
    @IBAction func onClickOfDirectOption3Button(){
        selection = (selectionIndexOf400 + 2)
        updateUserInterfaceOnScreen()
    }
    
    @IBAction func onClickOfSubmitButton(){
        trackFunctionalEvent(FE_RECHARGE_AMOUNT, information: ["recharge_amt":rechargeOptions[selection]])
        Acf().pushVC("WebViewViewController", navigationController: self.navigationController, isRootViewController: false, animated: true) { (viewControllerObject) -> () in
            (viewControllerObject as! WebViewViewController).workingMode = WorkingMode.recharge
            (viewControllerObject as! WebViewViewController).amountToRecharge = self.rechargeOptions[self.selection]
        }
    }
    
    //MARK: - UIPickerViewDelegate methods

    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        func configure(label:UILabel, string:String,highlight:Bool){
            if highlight {
                label.font = UIFont(name: FONT_REGULAR, size: 21)
                label.layer.opacity = 1.0
            }else{
                label.layer.opacity = 0.7
                label.font = UIFont(name: FONT_REGULAR, size: 18)
            }
            let mutableAttributedString = NSMutableAttributedString(string: string, attributes: [NSForegroundColorAttributeName:APP_THEME_LIGHT_GREEN_COLOR.darkerColorWithPercentage(0.15)])
            mutableAttributedString.applyAttributesBySearching("₹", attributes: [NSFontAttributeName:UIFont(name: FONT_SEMI_BOLD, size: 13),NSForegroundColorAttributeName:APP_THEME_LIGHT_GREEN_COLOR.darkerColorWithPercentage(0.3),NSBaselineOffsetAttributeName:3])
            label.attributedText = mutableAttributedString
            label.textAlignment = .center
        }
        let string = "₹\(rechargeOptions[row])                                   "
        if let titleLabel = view as? UILabel {
            configure(label: titleLabel, string: string, highlight: selection == row)
            return titleLabel
        } else {
            let titleLabel = UILabel()
            configure(label: titleLabel, string: string, highlight: selection == row)
            return titleLabel
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        selection = row
        updateUserInterfaceOnScreen()
        pickerView.reloadComponent(component)
    }
    
    //MARK: - UIPickerViewDataSource methods
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return rechargeOptions.count
    }
}
