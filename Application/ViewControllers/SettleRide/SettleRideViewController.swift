//
//  SettleRideViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit

class SettleRideViewController: UIViewController,UITextFieldDelegate {
    
    //MARK: - variables and constants
    @IBOutlet fileprivate weak var infoLabel1: UILabel!
    @IBOutlet fileprivate weak var infoLabel2: UILabel!
    @IBOutlet fileprivate weak var imageView: UIImageView!
    @IBOutlet fileprivate weak var doneButton: TKTransitionSubmitButton!
    
    var users: NSMutableArray!
    var isCarOwner = true
    var rideDate : Date?
    fileprivate var index = 0
    
    //MARK: - view controller life cycle methods
    
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    //MARK: - other methods
    private func setupForNavigationBar(){
        setAppearanceForNavigationBarType1(self.navigationController)
    }
    
    private func registerForNotifications(){
    }
    
    private func startUpInitialisations(){
        setAppearanceForViewController(self)
        setBorder(imageView, color: APP_THEME_GREEN_COLOR, width: 3, cornerRadius: imageView.bounds.size.width/2)
        doneButton.layer.cornerRadius = doneButton.bounds.size.width/2
        doneButton.layer.masksToBounds = true
        doneButton.requiredBackgroundColor = UIColor(red: 100/255, green: 196/255, blue: 219/255, alpha: 1.0)
        trackScreenLaunchEvent(SLE_SETTLE_RIDE)
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        if let userInfo = users.object(at: index) as? NSDictionary {
            imageView.sd_setImage(with: URL(string: getUserProfilePictureUrlFromFileName(userInfo.object(forKey: "user_photo") as? String)), placeholderImage: USER_PLACEHOLDER_IMAGE)
            infoLabel1.text = (userInfo.object(forKey: "user_name") as? String)
            infoLabel2.text = isCarOwner ? "Car Owner" : "Passenger"
        }
    }
    
    func loadNextUser() {
        index += 1
        animateContentsForLoading()
        updateUserInterfaceOnScreen()
    }
    
    //MARK: - other functions
    
    func onClickOfLeftBarButton(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickOfDoneButton(){
        NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_RESET_IDLE_TIMER_RIDE_HOME), object: nil, userInfo: nil)
        if(isInternetConnectivityAvailable(true)==false){return}
        doneButton.startLoadingAnimation()
        if let user = users.object(at: index) as? NSDictionary {
            let userInfo = Dbm().getUserInfo()
            let information = NSMutableDictionary()
            copyData(userInfo!.userId , destinationDictionary: information, destinationKey: "userId", methodName: #function)
            copyData(user.object(forKey: "id") , destinationDictionary: information, destinationKey: "requestId", methodName: #function)
            copyData(rideDate!.toStringValue("yyyy-MM-dd") , destinationDictionary: information, destinationKey: "date", methodName: #function)
            let scm = ServerCommunicationManager()
            scm.responseErrorOption = ResponseErrorOption.dontShowErrorResponseMessage
            scm.settleRide(information) { (responseData) -> () in
                if let _ = responseData {
                    if let status = (responseData?.object(forKey: "data") as? NSDictionary)?.object(forKey: "status") as? String {
                        if let payment = (responseData?.object(forKey: "data") as? NSDictionary)?.object(forKey: "payment") as? String {
                            let message = "\(status)\n\(payment)"
                            if self.users.count > (self.index + 1){
                                showNotification(message, showOnNavigation: true, showAsError: false)
                                self.loadNextUser()
                            }else{
                                showAlert("Payment", message: message)
                                Acf().hidePopupViewController()
                            }
                        }
                    }
                }
                self.doneButton.stopIt()
            }
        }
    }
    
    func animateContentsForLoading() {
        let animationDuration = 0.2
        let delay = 0.2
        UIView.animate(withDuration: animationDuration, delay: 0, options: [.curveEaseIn], animations: {
            self.imageView.layer.opacity = 0.0
            }, completion: { (completed) in
                UIView.animate(withDuration: animationDuration, delay: 0, options: [.curveEaseIn], animations: {
                    self.infoLabel1.layer.opacity = 0.0
                    }, completion: { (completed) in
                        UIView.animate(withDuration: animationDuration, delay: 0, options: [.curveEaseIn], animations: {
                            self.infoLabel2.layer.opacity = 0.0
                            }, completion: { (completed) in
                                UIView.animate(withDuration: animationDuration, delay: delay , options: [.curveEaseIn], animations: {
                                    self.infoLabel2.layer.opacity = 1.0
                                    }, completion: { (completed) in
                                        UIView.animate(withDuration: animationDuration, delay: 0, options: [.curveEaseIn], animations: {
                                            self.infoLabel1.layer.opacity = 1.0
                                            }, completion: { (completed) in
                                                UIView.animate(withDuration: animationDuration, delay: 0, options: [.curveEaseIn], animations: {
                                                    self.imageView.layer.opacity = 1.0
                                                    }, completion: { (completed) in
                                                        
                                                })
                                        })
                                })
                        })
                })
        })
    }
}
