//
//  RideEventViewerViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit

class RideEventViewerViewController : UIViewController,AHStepperDataSource {
    
    //MARK: - variables and constants
    var usersForRide = NSMutableArray()
    var trueForOwnerFalseforPassenger = false
    var trueForHomeToDestinationFalseforDestinationToHome = false
    var groupChatDialogue: QBChatDialog?
    var messages = NSMutableArray()
    var selectedDayDate = Date()
    fileprivate var expectedHeightRequiredByStepperControl = 0
    
    @IBOutlet fileprivate weak var scrollView: UIScrollView!
    @IBOutlet fileprivate weak var stepperControl: AHStepperControl!
    @IBOutlet fileprivate weak var informationLabel1: UILabel!
    @IBOutlet fileprivate weak var informationLabel2: UILabel!
    @IBOutlet fileprivate weak var informationLabel3: UILabel!
    @IBOutlet fileprivate weak var informationLabel4: UILabel!
    
    fileprivate var nodes = NSMutableArray()
    
    //MARK: - view controller life cycle methods
    
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        stepperControl.translatesAutoresizingMaskIntoConstraints = true
        stepperControl.frame = CGRect(x: 0, y: 0, width: scrollView.bounds.size.width,height: CGFloat(expectedHeightRequiredByStepperControl))
        scrollView.contentSize = stepperControl.frame.size
    }

    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_RESET_IDLE_TIMER_RIDE_HOME), object: nil, userInfo: nil)
        setupForNavigationBar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_RESET_IDLE_TIMER_RIDE_HOME), object: nil, userInfo: nil)
    }
    
    //MARK: - other methods
    private func setupForNavigationBar(){
        setAppearanceForNavigationBarType1(self.navigationController)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    private func registerForNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(RideEventViewerViewController.updateUserInterfaceOnScreen), name: NSNotification.Name(rawValue: NOTIFICATION_RIDE_EVENT_UPDATED), object: nil)
    }
    
    private func startUpInitialisations(){
        setAppearanceForViewController(self)
        DispatchQueue.main.async {
            self.view.showActivityIndicator()
        }
        ServicesManager.instance().chatService.earlierMessages(withChatDialogID: self.groupChatDialogue!.id!) { (response, messages) in
            
            if isNotNull(messages) && messages!.count > 0{
                ServicesManager.instance().chatService.earlierMessages(withChatDialogID: self.groupChatDialogue!.id!) { (response, messages) in

                    if isNotNull(messages) && messages!.count > 0{
                        ServicesManager.instance().chatService.earlierMessages(withChatDialogID: self.groupChatDialogue!.id!) { (response, messages) in
                            
                            if isNotNull(messages) && messages!.count > 0{
                                ServicesManager.instance().chatService.earlierMessages(withChatDialogID: self.groupChatDialogue!.id!) { (response, messages) in
                                    self.view.hideActivityIndicator()
                                    self.updateUserInterfaceOnScreen()
                                }
                            }else{
                                self.view.hideActivityIndicator()
                                self.updateUserInterfaceOnScreen()
                            }
                        }
                    }else{
                        self.view.hideActivityIndicator()
                        self.updateUserInterfaceOnScreen()
                    }
                }

            }else{
                self.view.hideActivityIndicator()
                self.updateUserInterfaceOnScreen()
            }
        }
        stepperControl.nodeCircumference = 18
        stepperControl.linkLength = 12
        stepperControl.linkThickness = 1.25
        stepperControl.nodeStrokeWidth = 1.25
        stepperControl.innerNodeOffset = 4
        stepperControl.leftTextFont = UIFont.init(name: FONT_BOLD, size:10)!
        stepperControl.rightTextFont = UIFont.init(name: FONT_REGULAR, size:10)!
        stepperControl.leftTextColor  = APP_THEME_VOILET_COLOR
        stepperControl.rightTextColor = APP_THEME_GRAY_COLOR
        stepperControl.stepperFillColor = APP_THEME_VOILET_COLOR.withAlphaComponent(0.7)
        self.navigationController?.view.backgroundColor = UIColor.clear
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        nodes.removeAllObjects()
        messages.removeAllObjects()
        
        if let messages = ServicesManager.instance().chatService.messagesMemoryStorage.messages(withDialogID: self.groupChatDialogue!.id!) as [QBChatMessage]? {
            for m in messages{
                if let messageType = m.customParameters?.object(forKey: MESSAGE_TYPE_KEY){
                    if messageType as! String == GroupEvents.rideGroupEvent {
                        if let rideId = m.customParameters?.object(forKey: "rideId"){
                            if rideId as! String == getRideId(){
                                self.messages.add(m)
                            }
                        }
                    }
                }
            }
        }
        
        messages = NSMutableArray(array:(messages).sorted(by: { (m1, m2) -> Bool in
            return ((m1 as! QBChatMessage).dateSent!.isEarlierThanDate((m2 as! QBChatMessage).dateSent!))
        }))
        
        for m in messages{
            let leftText = (m as! QBChatMessage).dateSent!.toStringValue(.none, timeStyle: .medium)
            let rightText = (m as! QBChatMessage).text!
            nodes.add(AHStepperNode(isSelected: true, leftText: leftText, rightText: rightText))
        }
        
        nodes.add(AHStepperNode(isSelected: false, leftText: "--:--", rightText: "-------------"))
        
        expectedHeightRequiredByStepperControl = nodes.count * (Int(stepperControl.nodeCircumference) +
            Int(stepperControl.linkLength))

        var attributesTitle = Dictionary<String, AnyObject>()
        attributesTitle[NSForegroundColorAttributeName] = APP_THEME_VOILET_COLOR
        attributesTitle[NSFontAttributeName] = UIFont.init(name: FONT_BOLD, size: 12)
        
        var attributesDescription = Dictionary<String, AnyObject>()
        attributesDescription[NSForegroundColorAttributeName] = APP_THEME_GREEN_COLOR
        attributesDescription[NSFontAttributeName] = UIFont.init(name: FONT_SEMI_BOLD, size: 11)
        
        let settings = Dbm().getSetting()
        
        let attributedString1 = NSMutableAttributedString(string: "            Date    \(selectedDayDate.toString(.full, timeStyle: .none))")
        let attributedString2 = NSMutableAttributedString(string: "      Travelling   \(trueForHomeToDestinationFalseforDestinationToHome ? "Home to \(destinationCapitalizedName())" : "\(destinationCapitalizedName()) to Home") as \(trueForOwnerFalseforPassenger ? "Car Owner" : "Passenger")")
        let attributedString3 = NSMutableAttributedString(string: "           Speed   0 Km/h")
        let attributedString4 = NSMutableAttributedString(string: "        Distance   \(settings.distance ?? "--") Km")
        
        
        let constant = 19
        
        attributedString1.addAttributes(attributesTitle, range: NSMakeRange(0, constant))
        attributedString1.addAttributes(attributesDescription, range: NSMakeRange(constant, attributedString1.length-constant))
        
        attributedString2.addAttributes(attributesTitle, range: NSMakeRange(0, constant))
        attributedString2.addAttributes(attributesDescription, range: NSMakeRange(constant, attributedString2.length-constant))
        
        attributedString3.addAttributes(attributesTitle, range: NSMakeRange(0, constant))
        attributedString3.addAttributes(attributesDescription, range: NSMakeRange(constant, attributedString3.length-constant))
        
        attributedString4.addAttributes(attributesTitle, range: NSMakeRange(0, constant))
        attributedString4.addAttributes(attributesDescription, range: NSMakeRange(constant, attributedString4.length-constant))

        informationLabel1.attributedText = attributedString1
        informationLabel2.attributedText = attributedString2
        informationLabel3.attributedText = attributedString3
        informationLabel4.attributedText = attributedString4
        
        do{
            try SwiftLocation.shared.currentLocation(.block, timeout: 30, onSuccess: { (location) in
                if let speed = location?.speed{
                if speed > 0{
                    let attributedString3 = NSMutableAttributedString(string: "           Speed   \(String(format:"%.2f",speed*3.6)) Km/h")
                    attributedString3.addAttributes(attributesTitle, range: NSMakeRange(0, constant))
                    attributedString3.addAttributes(attributesDescription, range: NSMakeRange(constant, attributedString3.length-constant))
                    self.informationLabel3.attributedText = attributedString3
                }
                }
                }, onFail: { (error) in
            })
        }catch{
        }
        
        DispatchQueue.main.async {
            self.stepperControl.setNeedsLayout()
            self.stepperControl.setNeedsDisplay()
            self.view.setNeedsLayout()
            self.view.setNeedsDisplay()
        }
    }
    
    func stepperNodeAtIndex(_ index: Int) -> AHStepperNode {
        return nodes[index] as! AHStepperNode
    }
    
    func stepperNodeCount() -> Int {
        return nodes.count
    }
    
    func getRideId() -> String {
        return "\(selectedDayDate.day())\(selectedDayDate.month())\(selectedDayDate.year())\(trueForHomeToDestinationFalseforDestinationToHome == true ? "0" : "1")"
    }
}
