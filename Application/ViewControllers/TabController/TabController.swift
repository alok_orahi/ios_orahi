//
//  TabController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit

class TabController: UITabBarController {
    
    //MARK: - view controller life cycle methods
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    //MARK: - other methods
    private func setupForNavigationBar(){
        setAppearanceForNavigationBarType1(self.navigationController)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    private func registerForNotifications(){
        
    }
    
    private func startUpInitialisations(){
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        setupTabs()
        loadAllTabsInAdvanceToImprovePerformance()
    }
    
    //MARK: - other functions
    
    func setupTabs(){
        
        UITabBar.appearance().tintColor = UIColor.white
        
        tabBar.frame.size.width = self.view.frame.width + 4
        tabBar.frame.origin.x = -2
        
        let tabBarItems = self.tabBar.items
        for index in 0...3 {
            let imageName = "tabBar\(index+1)"
            let item = tabBarItems![index] as UITabBarItem
            item.image = UIImage(named: imageName)?.tranlucent(withAlpha: 0.9)
            item.selectedImage = UIImage(named: "tabBar\(index+1)")
            if index == 0 {
                item.title = "Ride"
            }
            if index == 1 {
                item.title = "Chats"
            }
            if index == 2 {
                item.title = "Ranking"
            }
            if index == 3 {
                item.title = "Offers"
            }
        }
        
        let numberOfItems = CGFloat(tabBar.items!.count)
        let tabBarItemSize = CGSize(width: tabBar.frame.width / numberOfItems, height: tabBar.frame.height)
        tabBar.selectionIndicatorImage = UIImage.imageWithColor(APP_THEME_VOILET_COLOR, size: tabBarItemSize).resizableImage(withCapInsets: UIEdgeInsets.zero)
    }
    
    func loadAllTabsInAdvanceToImprovePerformance(){
        execMain({[weak self]  in guard let `self` = self else { return }
            for nc in self.viewControllers as! [UINavigationController] {
                let view = nc.viewControllers[0].view
                view?.frame
            }
        },delay:4)
    }
}
