//
//  DoNotDisturbSettingsViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit

class DoNotDisturbSettingsViewController: UIViewController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource {
    
    //MARK: - variables and constants
    fileprivate var segmentedControl : HMSegmentedControl?
    @IBOutlet fileprivate weak var tableView : UITableView?
    
    
    //MARK: - view controller life cycle methods
    
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        Acf().updateSettingsToServerIfRequired()
    }
    
    //MARK: - other methods
    private func setupForNavigationBar(){
        setAppearanceForNavigationBarType1(self.navigationController)
        setupNavigationBarTitleType1("Do not Disturb",viewController: self)
        if self.navigationController != nil && self.navigationController!.viewControllers.count > 1 {
            addNavigationBarButton(self, image: UIImage(named: "backarrowblack"), title: nil, isLeft: true)
        }
    }
    
    func setupTopMenu(){
        if segmentedControl == nil {
            segmentedControl = HMSegmentedControl(sectionTitles: ["Day of the week","Weekly"])
            segmentedControl?.frame = CGRect(x: 0,y: 0,width: self.view.frame.size.width - Acf().getRequiredPopupSideGap("")*2, height: 44)
            segmentedControl?.selectionIndicatorHeight = 2.0;
            segmentedControl?.backgroundColor = UIColor.white;
            segmentedControl?.selectedTitleTextAttributes = [NSForegroundColorAttributeName: APP_THEME_VOILET_COLOR,NSFontAttributeName:UIFont.init(name: FONT_SEMI_BOLD, size: 14)!]
            segmentedControl?.titleTextAttributes = [NSForegroundColorAttributeName: APP_THEME_LIGHT_GRAY_COLOR,NSFontAttributeName:UIFont.init(name: FONT_SEMI_BOLD, size: 14)!]
            segmentedControl!.selectionIndicatorColor = APP_THEME_VOILET_COLOR;
            segmentedControl!.selectionStyle = .fullWidthStripe;
            segmentedControl!.selectedSegmentIndex = 0;
            segmentedControl!.selectionIndicatorLocation = .down;
            segmentedControl!.shouldAnimateUserSelection = true;
            segmentedControl!.addTarget(self, action: #selector(DoNotDisturbSettingsViewController.segmentedControlValueChanged), for: UIControlEvents.valueChanged)
            self.view.addSubview(segmentedControl!)
        }
        tableView?.reloadData()
    }
    
    func onClickOfLeftBarButton(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    private func registerForNotifications(){
        
    }
    
    private func startUpInitialisations(){
        registerNib("SingleLabelAndSwitchTableViewCell", tableView: tableView)
        setAppearanceForViewController(self)
        setAppearanceForTableView(tableView)
        setupTopMenu()
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        
    }
    
    //MARK: - UITableView Delegate & Data Source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SingleLabelAndSwitchTableViewCell") as? SingleLabelAndSwitchTableViewCell
        var title = ""
        if segmentedControl?.selectedSegmentIndex == 0{//mobile
            title =  Date().dateByAddingDays(Int((indexPath as NSIndexPath).row)).toString(.full, timeStyle: .none)
        }else{
            if (indexPath as NSIndexPath).row == 0{
                title = "Monday"
            }else if (indexPath as NSIndexPath).row == 1{
                title = "Tuesday"
            }else if (indexPath as NSIndexPath).row == 2{
                title = "Wednesday"
            }else if (indexPath as NSIndexPath).row == 3{
                title = "Thursday"
            }else if (indexPath as NSIndexPath).row == 4{
                title = "Friday"
            }else if (indexPath as NSIndexPath).row == 5{
                title = "Saturday"
            }else if (indexPath as NSIndexPath).row == 6{
                title = "Sunday"
            }
        }
        let attributesTitle = [NSFontAttributeName:UIFont.init(name: FONT_SEMI_BOLD, size: 13)!,
            NSForegroundColorAttributeName:UIColor.darkGray] as NSDictionary
        
        let attributedString = NSMutableAttributedString(string:title)
        attributedString.setAttributes(attributesTitle as? [String : Any], range: NSMakeRange(0, title.length))
        cell?.titleLabel?.attributedText = attributedString
        
        let setting = Dbm().getSetting()
        if segmentedControl?.selectedSegmentIndex == 0{//mobile
            if (indexPath as NSIndexPath).row == 0{
                cell?.controlSwitch?.isOn = (setting.doNotDisturbForDailyOption1?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 1{
                cell?.controlSwitch?.isOn = (setting.doNotDisturbForDailyOption2?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 2{
                cell?.controlSwitch?.isOn = (setting.doNotDisturbForDailyOption3?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 3{
                cell?.controlSwitch?.isOn = (setting.doNotDisturbForDailyOption4?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 4{
                cell?.controlSwitch?.isOn = (setting.doNotDisturbForDailyOption5?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 5{
                cell?.controlSwitch?.isOn = (setting.doNotDisturbForDailyOption6?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 6{
                cell?.controlSwitch?.isOn = (setting.doNotDisturbForDailyOption7?.boolValue)!
            }
        }else if segmentedControl?.selectedSegmentIndex == 1{//website
            if (indexPath as NSIndexPath).row == 0{
                cell?.controlSwitch?.isOn = (setting.doNotDisturbForWeeklyOption1?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 1{
                cell?.controlSwitch?.isOn = (setting.doNotDisturbForWeeklyOption2?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 2{
                cell?.controlSwitch?.isOn = (setting.doNotDisturbForWeeklyOption3?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 3{
                cell?.controlSwitch?.isOn = (setting.doNotDisturbForWeeklyOption4?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 4{
                cell?.controlSwitch?.isOn = (setting.doNotDisturbForWeeklyOption5?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 5{
                cell?.controlSwitch?.isOn = (setting.doNotDisturbForWeeklyOption6?.boolValue)!
            }else if (indexPath as NSIndexPath).row == 6{
                cell?.controlSwitch?.isOn = (setting.doNotDisturbForWeeklyOption7?.boolValue)!
            }
        }
        cell?.controlSwitch?.addTarget(self, action: #selector(DoNotDisturbSettingsViewController.onValueChangeOfSwitch(_:)), for: UIControlEvents.valueChanged)
        cell?.controlSwitch?.tag = (indexPath as NSIndexPath).row
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as? SingleLabelAndSwitchTableViewCell
        performAnimatedClickEffectType1(cell!.titleLabel!)
        cell?.controlSwitch?.setOn((cell?.controlSwitch?.isOn == false), animated: true)
        onValueChangeOfSwitch((cell?.controlSwitch)!)
    }
    
    func onValueChangeOfSwitch(_ sender:UISwitch){
        let setting = Dbm().getSetting()
        if segmentedControl?.selectedSegmentIndex == 0{//daily
            if sender.tag == 0{
                setting.doNotDisturbForDailyOption1 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 1{
                setting.doNotDisturbForDailyOption2 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 2{
                setting.doNotDisturbForDailyOption3 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 3{
                setting.doNotDisturbForDailyOption4 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 4{
                setting.doNotDisturbForDailyOption5 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 5{
                setting.doNotDisturbForDailyOption6 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 6{
                setting.doNotDisturbForDailyOption7 = NSNumber(value: sender.isOn as Bool)
            }
        }else if segmentedControl?.selectedSegmentIndex == 1{//weekly
            if sender.tag == 0{
                setting.doNotDisturbForWeeklyOption1 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 1{
                setting.doNotDisturbForWeeklyOption2 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 2{
                setting.doNotDisturbForWeeklyOption3 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 3{
                setting.doNotDisturbForWeeklyOption4 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 4{
                setting.doNotDisturbForWeeklyOption5 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 5{
                setting.doNotDisturbForWeeklyOption6 = NSNumber(value: sender.isOn as Bool)
            }else if sender.tag == 6{
                setting.doNotDisturbForWeeklyOption7 = NSNumber(value: sender.isOn as Bool)
            }
        }
        setting.isDoNotDisturbSettingsUpdated = NSNumber(value: true)
        Dbm().saveChangesLazily()
    }
    
    //MARK: - other functions
    
    func segmentedControlValueChanged(){
        tableView?.reloadData()
    }
    
}
