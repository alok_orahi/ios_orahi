//
//  AuthenticationStep4ViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit

enum AS4WorkingMode {
    case userType
    case travellerMode
}

class AuthenticationStep4ViewController: UIViewController {
    
    //MARK: - variables and constants
    @IBOutlet fileprivate weak var infoLabel: UILabel!
    @IBOutlet fileprivate weak var actionButton1: TKTransitionSubmitButton!
    @IBOutlet fileprivate weak var actionButton2: TKTransitionSubmitButton!
    @IBOutlet fileprivate weak var actionButton3: TKTransitionSubmitButton!
    
    var workingMode = AS4WorkingMode.userType
    var showCarpoolOptionOnly = false
    
    //MARK: - view controller life cycle methods
    
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
        updateUserInterfaceOnScreen()
    }
    
    //MARK: - other methods
    private func setupForNavigationBar(){
        setAppearanceForNavigationBarType2(self.navigationController)
        if self.isBackButtonRequired() {
            addNavigationBarButton(self, image: UIImage(named: "backarrowblack"), title: nil, isLeft: true,forceWhiteTint:false)
        }else{
            self.navigationItem.leftBarButtonItem = nil
            self.navigationItem.setHidesBackButton(true, animated:false)
        }
    }
    
    private func registerForNotifications(){
    }
    
    private func startUpInitialisations(){
        setAppearanceForViewController(self)
        self.actionButton1.performAppearAnimationType1()
        self.actionButton2.performAppearAnimationType1()
        self.actionButton3.performAppearAnimationType1()
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        if workingMode == .userType {
            infoLabel.attributedText = getAttributedTitleStringType3("CATEGORY\nKnowing you will help us to \nfind the best Co-Traveller for you", regularText: "CATEGORY", anotherText: "Knowing you will help us to \nfind the best Co-Traveller for you")
            actionButton1.setTitle("COLLEGE STUDENT", for: UIControlState.normal)
            actionButton2.setTitle("WORKING PROFESSIONAL", for: UIControlState.normal)
            actionButton3.setTitle("OTHER", for: UIControlState.normal)
            actionButton3.isHidden = true
            if getUserOnBoardingType() == .regular {
                if !showCarpoolOptionOnly {
                    actionButton3.isHidden = false
                }
            }
            trackScreenLaunchEvent(SLE_SELECT_USER_TYPE)
        }else if workingMode == .travellerMode {
            infoLabel.attributedText = getAttributedTitleStringType3("HOW WOULD LIKE TO CARPOOL!\nyou can change your \nride preferences any time", regularText: "HOW WOULD LIKE TO CARPOOL!", anotherText: "you can change your \nride preferences any time")
            actionButton1.setTitle("CAR OWNER", for: UIControlState.normal)
            actionButton2.setTitle("PASSENGER", for: UIControlState.normal)
            actionButton3.setTitle("", for: UIControlState.normal)
            actionButton3.isHidden = true
            trackScreenLaunchEvent(SLE_SELECT_USER_MODE)
        }
        updateActionButtonAppearance()
    }
    
    func updateActionButtonAppearance(){
        func setbuttonAppearance(_ button:TKTransitionSubmitButton){
            if button.isSelected {
                setBorder(button, color: APP_THEME_VOILET_COLOR, width: 0.5, cornerRadius: 0)
                button.titleLabel?.textColor = UIColor.white
                button.normalBackgroundColor = APP_THEME_VOILET_COLOR
            }else{
                setBorder(button, color:APP_THEME_VOILET_COLOR, width: 0.5, cornerRadius: 0)
                button.titleLabel?.textColor = APP_THEME_VOILET_COLOR
                button.normalBackgroundColor = UIColor.white
            }
        }
        setbuttonAppearance(actionButton1)
        setbuttonAppearance(actionButton2)
        setbuttonAppearance(actionButton3)
    }
    
    //MARK: - other functions
    
    func onClickOfLeftBarButton(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickOfActionButton1(){
        if(isInternetConnectivityAvailable(true)==false){return}
        actionButton1.isSelected = true
        actionButton2.isSelected = false
        actionButton3.isSelected = false
        updateActionButtonAppearance()
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(AuthenticationStep4ViewController.updateToServer), object: nil)
        self.perform(#selector(AuthenticationStep4ViewController.updateToServer), with: nil, afterDelay: 0.6)
    }
    
    @IBAction func onClickOfActionButton2(){
        if(isInternetConnectivityAvailable(true)==false){return}
        actionButton1.isSelected = false
        actionButton2.isSelected = true
        actionButton3.isSelected = false
        updateActionButtonAppearance()
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(AuthenticationStep4ViewController.updateToServer), object: nil)
        self.perform(#selector(AuthenticationStep4ViewController.updateToServer), with: nil, afterDelay: 0.6)
    }
    
    @IBAction func onClickOfActionButton3(){
        if(isInternetConnectivityAvailable(true)==false){return}
        actionButton1.isSelected = false
        actionButton2.isSelected = false
        actionButton3.isSelected = true
        updateActionButtonAppearance()
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(AuthenticationStep4ViewController.updateToServer), object: nil)
        self.perform(#selector(AuthenticationStep4ViewController.updateToServer), with: nil, afterDelay: 0.6)
    }
    
    func updateToServer(){
        if(isInternetConnectivityAvailable(true)==false){return}
        if workingMode == .userType {
            var shouldAskTravelMode = false
            let settings = Dbm().getSetting()
            settings.isProfileUpdated = NSNumber(value: true)
            if actionButton1.isSelected {
                shouldAskTravelMode = true
                settings.userType = USER_TYPE_STUDENT
                trackFunctionalEvent(FE_USER_TYPE_STUDENT_SELECTED, information: nil)
                trackFunctionalEvent(FE_TRAVEL_PREFFERENCE, information: ["user_type":USER_TYPE_STUDENT])
            }else if actionButton2.isSelected {
                shouldAskTravelMode = true
                settings.userType = USER_TYPE_CORPORATE
                trackFunctionalEvent(FE_USER_TYPE_CORPORATE_SELECTED, information: nil)
                trackFunctionalEvent(FE_TRAVEL_PREFFERENCE, information: ["user_type":USER_TYPE_CORPORATE])
            }else if actionButton3.isSelected {
                shouldAskTravelMode = false
                settings.userType = USER_TYPE_OTHER
                trackFunctionalEvent(FE_USER_TYPE_OTHER_SELECTED, information: nil)
                trackFunctionalEvent(FE_TRAVEL_PREFFERENCE, information: ["user_type":USER_TYPE_OTHER])
            }
            Dbm().saveChanges()
            if shouldAskTravelMode {
                Acf().pushVC("AuthenticationStep4ViewController", navigationController: self.navigationController, isRootViewController: false, animated: true, modifyObject: { (viewControllerObject) in
                    (viewControllerObject as! AuthenticationStep4ViewController).workingMode = .travellerMode
                })
            }else{
                decideAndShowNextScreen(self.navigationController!)
            }
        }else if workingMode == .travellerMode {
            let settings = Dbm().getSetting()
            settings.isProfileUpdated = NSNumber(value: true)
            let userInfo = Dbm().getUserInfo()
            if actionButton1.isSelected {
                userInfo?.isPassenger = "0"
                trackFunctionalEvent(FE_MODE_CAR_OWNER_SELECTED, information: nil)
            }else{
                userInfo?.isPassenger = "1"
                trackFunctionalEvent(FE_MODE_PASSENGER_SELECTED, information: nil)
            }
            Dbm().saveChanges()
            if actionButton1.isSelected {
                Acf().pushVC("AuthenticationStep5ViewController", navigationController: self.navigationController, isRootViewController: false, animated: true, modifyObject: nil)
            }else{
                actionButton2.startLoadingAnimation()
                let trueForOwnerFalseforPassenger = userInfo!.isPassenger == "0"
                let information = NSMutableDictionary()
                copyData(userType(), destinationDictionary: information, destinationKey: "userType", methodName: #function)
                copyData(trueForOwnerFalseforPassenger ? "carowner" : "passenger" , destinationDictionary: information, destinationKey: "travelMode", methodName: #function)
                let scm = ServerCommunicationManager()
                scm.updateUserProfile(information) { (responseData) -> () in
                    if let _ = responseData {
                        updateUserProfile(loggedInUserId(), completion: { (returnedData) in
                            if safeBool(returnedData) {
                                decideAndShowNextScreen(self.navigationController!)
                            }
                            self.actionButton2.stopIt()
                        })
                    }else{
                        self.actionButton2.stopIt()
                    }
                }
            }
        }
    }
}
