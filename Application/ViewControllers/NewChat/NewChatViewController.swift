//
//  NewChatViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

class NewChatViewController: UsersListTableViewController, QMChatServiceDelegate, QMChatConnectionDelegate {
    
    //MARK: - variables and constants
    var dialog: QBChatDialog?
    fileprivate var selections = NSMutableDictionary()
    
    //MARK: - view controller life cycle methods
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
        updateUserInterfaceOnScreen()
    }
    
    
    //MARK: - other methods
    private func setupForNavigationBar(){
        setAppearanceForNavigationBarType1(self.navigationController)
        if let _ = self.dialog {
            setupNavigationBarTitleType1("Add Occupants",viewController: self)
        } else {
            setupNavigationBarTitleType1("New Chat",viewController: self)
        }
        if (self.navigationController?.viewControllers.count)! > 1 {
            addNavigationBarButton(self, image: UIImage(named: "backarrowblack"), title: nil, isLeft: true, observer: self)
        }
        updateRightNavigationBarButton()
    }
    
    func updateRightNavigationBarButton() {
        if selections.allKeys.count > 0 {
            if let _ = self.dialog {
                addNavigationBarButton(self, image: nil, title: "Done", isLeft: false, observer: self)
            } else {
                if selections.allKeys.count == 1 {
                    addNavigationBarButton(self, image: nil, title: "Chat", isLeft: false, observer: self)
                }else{
                    addNavigationBarButton(self, image: nil, title: "Group", isLeft: false, observer: self)
                }
            }
        }else{
            self.navigationItem.rightBarButtonItems = nil
        }
    }
    
    func onClickOfLeftBarButton(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    func onClickOfRightBarButton(_ sender:Any){
        if(isInternetConnectivityAvailable(true)==false){return}
        (sender as! UIButton).isEnabled = false
        let selectedIDs = selections.allValues
        var users: [QBUUser] = Acf().getUsersWithQBIDs(selectedIDs as! [String])
        weak var weakSelf = self
        if let dialog = self.dialog {
            if dialog.type == .group || dialog.type == .publicGroup {
                NewChatViewController.updateDialogAddUsers(self.dialog!, newUsers:users, completion: { (response, dialog) -> Void in
                    if let rightBarButtonItem = weakSelf?.navigationItem.rightBarButtonItem {
                        rightBarButtonItem.isEnabled = true
                    }
                    if (response?.error == nil) {
                        weakSelf?.processeNewDialog(dialog)
                    } else {
                        (sender as! UIButton).isEnabled = true
                    }
                })
            } else {
                let usersWithoutCurrentUser : [QBUUser]? = ((ServicesManager.instance().usersService.usersMemoryStorage.unsortedUsers()).filter({$0.id != ServicesManager.instance().currentUser()!.id}))
                let primaryUsers = usersWithoutCurrentUser?.filter({(dialog.occupantIDs as! [UInt]).contains(($0 as QBUUser).id)})
                if primaryUsers != nil && primaryUsers!.count > 0 {
                    users.append(contentsOf: primaryUsers! as [QBUUser])
                }
                let chatName = "Group"
                NewChatViewController.createChat(chatName, users: users, completion: { (response, createdDialog) in
                    (sender as! UIButton).isEnabled = true
                    if createdDialog != nil {
                        weakSelf?.processeNewDialog(createdDialog)
                    }
                })
            }
            
        } else {
            if users.count == 1 {
                NewChatViewController.createChat(nameToDisplay(users[0], chatDialogue: nil), users: users, completion: { (response, createdDialog) in
                    (sender as! UIButton).isEnabled = true
                    if createdDialog != nil {
                        weakSelf?.processeNewDialog(createdDialog)
                    }
                })
            } else {
                _ = AlertViewWithTextField(title: "Enter group name", message: nil, showOver:self, didClickOk: { (text) -> Void in
                    var chatName = text
                    if chatName!.trimmingCharacters(in: CharacterSet.whitespaces).isEmpty {
                        chatName = "Group"
                    }
                    NewChatViewController.createChat(chatName!, users: users, completion: { (response, createdDialog) in
                        (sender as! UIButton).isEnabled = true
                        if createdDialog != nil {
                            weakSelf?.processeNewDialog(createdDialog)
                        }
                    })}){() -> Void in
                        (sender as! UIButton).isEnabled = true
                }
            }
        }
    }
    
    private func registerForNotifications(){
        NotificationCenter.default.addObserver(self, selector: Selector("setupForUsers"), name: NSNotification.Name(rawValue: NOTIFICATION_UPDATE_USERS), object: nil)
    }
    
    private func startUpInitialisations(){
        registerNib("UserListTableViewCell", tableView: self.tableView)
        setAppearanceForViewController(self)
        setupForQuickBlox()
        if let _ = self.dialog {
            trackScreenLaunchEvent(SLE_ADD_UPDATE_CHAT_GROUP_MEMBERS)
        }else{
            trackScreenLaunchEvent(SLE_NEW_CHAT)
        }
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        updateUsers()
        self.tableView.reloadData()
    }
    
    func setupForQuickBlox(){
        ServicesManager.instance().chatService.addDelegate(self)
    }
    
    @IBAction func onClickOfSearchButton(_ sender:Any){
        Acf().pushVC("SearchUsersViewController", navigationController: self.navigationController, isRootViewController: false, animated: true) { (viewControllerObject) in
            (viewControllerObject as! SearchUsersViewController).completionBlock = { (returnedData) in
                if let selectedUsers = returnedData as? [QBUUser]{
                    for user in selectedUsers {
                        self.selections.setObject("\(user.id)", forKey: "\(user.id)" as NSCopying)
                    }
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    func updateUsers() {
        if let _ = self.dialog  {
            self.setupUsers(ServicesManager.instance().allCachedUsers())
        }
    }
    
    override func setupUsers(_ users: [QBUUser]) {
        if isNotNull(users) && users.count>0{
            var filteredUsers = users.filter({($0 as QBUUser).id != ServicesManager.instance().currentUser()!.id})
            if let _ = self.dialog  {
                filteredUsers = filteredUsers.filter({!(self.dialog!.occupantIDs as! [UInt]).contains(($0 as QBUUser).id)})
            }
            let sortedUsers = filteredUsers.sorted(by: { (user1, user2) -> Bool in
                if isNotNull(self.selections.object(forKey: "\(user1.id)")){
                    return true
                }else{
                    return false
                }
            })
            super.setupUsers(sortedUsers)
        }
    }
    
    static func updateDialogAddUsers(_ dialog:QBChatDialog!, newUsers users:[QBUUser], completion: ((_ response: QBResponse?, _ dialog: QBChatDialog?) -> Void)?) {
        let usersIDs = users.map{ NSNumber(value:$0.id) }
        ServicesManager.instance().chatService.joinOccupants(withIDs: usersIDs, to: dialog) { (response, dialog) in
            if (response.error == nil) {
                completion?(response, dialog)
                ServicesManager.instance().chatService.sendSystemMessageAboutAdding(to: dialog!, toUsersIDs: usersIDs, withText: self.updatedMessageWithUsers(users))
            } else {
                completion?(response, nil)
            }
        }
    }
    
    static func updateDialogRemoveUsers(_ dialog:QBChatDialog!, oldUsers users:[QBUUser], completion: ((_ response: QBResponse?, _ dialog: QBChatDialog?) -> Void)?) {
        let usersIDs = users.map{ NSNumber(value:$0.id) }
        ServicesManager.instance().chatService.removeOccupants(withIDs: usersIDs, to: dialog) { (response, dialog) in
            if (response?.error == nil) {
                completion?(response, nil)
            } else {
                completion?(response, nil)
            }
        }
    }
    
    static func updatedMessageWithUsers(_ users: [QBUUser]) -> String {
        var message: String = "added" + " "
        for user: QBUUser in users {
            message = "\(message)\(nameToDisplay(user,chatDialogue:nil)),"
        }
        message = message.substring(to: message.characters.index(before: message.endIndex))
        return message
    }
    
    static func createChat(_ name: String, users:[QBUUser], completion: ((_ response: QBResponse?, _ createdDialog: QBChatDialog?) -> Void)? , createGroup:Bool = false) {
        if(isInternetConnectivityAvailable(true)==false){return}
        if users.count == 1 && ( createGroup == false){
            ServicesManager.instance().chatService.createPrivateChatDialog(withOpponent: users.first!, completion: { (response, chatDialog) in
                hideActivityIndicator()
                completion?(response, chatDialog)
            })
        } else {
            ServicesManager.instance().chatService.createGroupChatDialog(withName: name, photo: DEVICE_TYPE, occupants: users, completion: { (response, chatDialog) in
                if (chatDialog != nil) {
                    
                    ServicesManager.instance().chatService.sendSystemMessageAboutAdding(to: chatDialog!, toUsersIDs: chatDialog!.occupantIDs!, completion: { (error) in
                        completion?(response, chatDialog)
                    })
                }
            })
        }
    }
    
    func processeNewDialog(_ dialog: QBChatDialog!) {
        self.dialog = dialog
        if Acf().isQBReadyForItsUserDependentServices(){
            Acf().pushVC("ChatViewController", navigationController: self.navigationController, isRootViewController: false, animated: true) { (viewControllerObject) -> () in
                (viewControllerObject as! ChatViewController).dialog = self.dialog
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 64
    }
    
    // MARK: - UITableViewDelegate & UITableViewDataSource
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserListTableViewCell", for: indexPath) as! UserListTableViewCell
        let user = self.users![(indexPath as NSIndexPath).row]
        cell.userInfoAsQBUUser = user
        cell.tag = (indexPath as NSIndexPath).row
        if isNotNull(selections.object(forKey: "\(user.id)")){
            cell.isSelected = true
            cell.accessoryType = .checkmark
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        }else{
            cell.accessoryType = .none
            cell.isSelected = false
            tableView.deselectRow(at: indexPath, animated: false)
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let user = self.users![(indexPath as NSIndexPath).row]
        selections.setObject("\(user.id)", forKey: "\(user.id)" as NSCopying)
        tableView.reloadRows(at: [indexPath], with: .automatic)
        self.updateRightNavigationBarButton()
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let user = self.users![(indexPath as NSIndexPath).row]
        selections.removeObject(forKey: "\(user.id)")
        tableView.reloadRows(at: [indexPath], with: .automatic)
        self.updateRightNavigationBarButton()
    }
    
    // MARK: - QMChatServiceDelegate
    
    func chatService(_ chatService: QMChatService!, didUpdateChatDialogInMemoryStorage chatDialog: QBChatDialog!) {
        if (chatDialog.id == self.dialog?.id) {
            self.dialog = chatDialog
            self.updateUsers()
            self.tableView.reloadData()
        }
    }
}
