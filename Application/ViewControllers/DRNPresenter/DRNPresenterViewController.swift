//
//  DRNTestPanelViewController.swift
//  Orahi
//
//  Created by Alok Singh on 04/11/16.
//  Copyright (c) 2016 Orahi. All rights reserved.
//

import UIKit

class DRNTestPanelViewController: UIViewController,UITextFieldDelegate {
    
    //MARK: - variables and constants
    @IBOutlet weak var tableView : UITableView?
    
    let titles = ["Do you want to ride Today?"]
    //MARK: - view controller life cycle methods
    
    override var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startupInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
    }
    
    //MARK: - other methods
    func setupForNavigationBar(){
        setAppearanceForNavigationBarType2(self.navigationController)
        setupNavigationBarTitleType2("DRN test panel",viewController: self)
        if self.navigationController != nil && self.navigationController!.viewControllers.count > 1 {
            addNavigationBarButton(self, image: UIImage(named: "backarrowblack"), title: nil, isLeft: true)
        }
        self.navigationController?.navigationBar.isHidden = false
    }
    
    func onClickOfLeftBarButton(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    func registerForNotifications(){
        
    }
    
    func startupInitialisations(){
        registerNib("SingleLabelTableViewCell", tableView: tableView)
        setAppearanceForViewController(self)
        setAppearanceForTableView(tableView)
    }
    
    func updateUserInterfaceOnScreen(){
        
    }
    
    //MARK: - UITableView Delegate & Data Source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat{
        return 44
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SingleLabelTableViewCell") as? SingleLabelTableViewCell
        cell?.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        cell?.titleLabel?.text = titles[indexPath.row]
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        let title = "Orahi"
        let message = titles[indexPath.row]
        let date = Date().dateByAddingSeconds(5) as NSDate

        if (indexPath as NSIndexPath).row == 0{
            let userInfo = NSMutableDictionary()
            userInfo.setObject("20:25:16", forKey: "etd" as NSCopying)
            userInfo.setObject("2016-11-22", forKey: "date" as NSCopying)
            userInfo.setObject("1001", forKey: "drn_id" as NSCopying)
            
            LocalNotificationHelper.sharedInstance().scheduleNotification(title: title, message: message, date: date, userInfo: userInfo)
        }
    }
    
    //MARK: - other functions
    
}
