//
//  HelpViewController.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit
import EPContactsPicker

class HelpViewController: UIViewController,EPPickerDelegate {
    
    //MARK: - variables and constants
    @IBOutlet fileprivate weak var contactImageView1 : UIImageView!
    @IBOutlet fileprivate weak var contactEditIconImageView1 : UIImageView!
    @IBOutlet fileprivate weak var contactArrowIndicatorImageView1 : UIImageView!
    @IBOutlet fileprivate weak var contactImageView2 : UIImageView!
    @IBOutlet fileprivate weak var contactEditIconImageView2 : UIImageView!
    @IBOutlet fileprivate weak var contactArrowIndicatorImageView2 : UIImageView!
    @IBOutlet fileprivate weak var contactImageView3 : UIImageView!
    @IBOutlet fileprivate weak var contactEditIconImageView3 : UIImageView!
    @IBOutlet fileprivate weak var contactArrowIndicatorImageView3 : UIImageView!
    @IBOutlet fileprivate weak var sosButtonImageView : UIImageView!
    
    var editingContact = 1
    var emergencyContacts = [Dictionary<String, String>]()
    
    //MARK: - view controller life cycle methods
    override internal var prefersStatusBarHidden : Bool {
        if (self.navigationController != nil) {
            return (self.navigationController?.isNavigationBarHidden)!
        }else{
            return false
        }
    }
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForNavigationBar()
        updateUserInterfaceOnScreen()
    }
    
    override internal func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        sosButtonImageView.stopAnimation()
    }
    
    //MARK: - other methods
    private func setupForNavigationBar(){
        setAppearanceForNavigationBarType1(self.navigationController)
        setupNavigationBarTitleType1("Help",viewController: self)
        if self.isBackButtonRequired() {
            addNavigationBarButton(self, image: UIImage(named: "backarrowblack"), title: nil, isLeft: true)
        }
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        if let cacheEmergencyContacts = (CacheManager.sharedInstance.loadObject("cacheEmergencyContacts") as? [Dictionary<String, String>]){
            emergencyContacts = cacheEmergencyContacts
        }
        contactImageView1.image = UIImage(named:"helpContactsPlaceHolder")
        contactImageView2.image = UIImage(named:"helpContactsPlaceHolder")
        contactImageView3.image = UIImage(named:"helpContactsPlaceHolder")
        for (index,ec) in emergencyContacts.enumerated() {
            if index == 0 {
                contactImageView1.setImageWith(ec["name"], color: APP_THEME_RED_COLOR_TYPE_2)
            }else if index == 1 {
                contactImageView2.setImageWith(ec["name"], color: APP_THEME_RED_COLOR_TYPE_2)
            }else if index == 2 {
                contactImageView3.setImageWith(ec["name"], color: APP_THEME_RED_COLOR_TYPE_2)
            }
        }
        sosButtonImageView.pulse(toSize: 0.85, duration: 0.25, repeat: true)
    }
    
    func onClickOfLeftBarButton(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    private func registerForNotifications(){
    }
    
    private func startUpInitialisations(){
        setAppearanceForViewController(self)
        contactImageView1.makeMeRoundWith(borderColor: UIColorHex(0xEE344A), width: 2)
        contactImageView2.makeMeRoundWith(borderColor: UIColorHex(0xEE344A), width: 2)
        contactImageView3.makeMeRoundWith(borderColor: UIColorHex(0xEE344A), width: 2)
        contactEditIconImageView1.makeMeRoundWith(borderColor: APP_THEME_GRAY_COLOR, width: 0.5)
        contactEditIconImageView2.makeMeRoundWith(borderColor: APP_THEME_GRAY_COLOR, width: 0.5)
        contactEditIconImageView3.makeMeRoundWith(borderColor: APP_THEME_GRAY_COLOR, width: 0.5)
        contactArrowIndicatorImageView1.rotate(CGFloat(-Double.pi*24/180))
        contactArrowIndicatorImageView3.rotate(CGFloat(Double.pi*24/180))
        fetchAndUpdateEmergencyContacts(forceUpdate: true)
    }
    
    @IBAction fileprivate func onClickOfSOSButton(){
        if self.emergencyContacts.count > 0 {
            if isInternetConnectivityAvailable(true)==false{return}
            Acf().pushVC("SOSViewController", navigationController: self.navigationController, isRootViewController: false, animated: true) { (viewController) in
            }
        }else{
            showNotification("Please select one or more emergency contacts first", showOnNavigation: true, showAsError: true)
        }
    }
    
    func askForRemovalOrEditing(){
        let contact = self.emergencyContacts[self.editingContact-1]
        let name = safeString(contact["name"])
        let number = safeString(contact["mobile"])
        let prompt = UIAlertController(title: "\(name)", message: "\n\(number)", preferredStyle: UIAlertControllerStyle.actionSheet)
        prompt.addAction(UIAlertAction(title: "Change", style: UIAlertActionStyle.default, handler: { (action) -> Void in
            self.showContactsPicker()
        }))
        prompt.addAction(UIAlertAction(title: "Remove", style: UIAlertActionStyle.destructive, handler: { (action) -> Void in
            self.emergencyContacts.remove(at: self.editingContact-1)
            self.saveToCacheAndUpdateToServer()
        }))
        prompt.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: { (action) -> Void in
        }))
        Acf().navigationController!.present(prompt, animated: true, completion: nil)
    }
    
    @IBAction fileprivate func onClickOfEditContact1Button(){
        editingContact = 1
        if emergencyContacts.count > 0{
            askForRemovalOrEditing()
        }else{
            showContactsPicker()
        }
    }
    
    @IBAction fileprivate func onClickOfEditContact2Button(){
        editingContact = 2
        if emergencyContacts.count > 1{
            askForRemovalOrEditing()
        }else{
            showContactsPicker()
        }
    }
    
    @IBAction fileprivate func onClickOfEditContact3Button(){
        editingContact = 3
        if emergencyContacts.count > 2{
            askForRemovalOrEditing()
        }else{
            showContactsPicker()
        }
    }
    
    func showContactsPicker(){
        if isInternetConnectivityAvailable(true)==false{return}
        let contactPickerViewController = EPContactsPicker(delegate: self, multiSelection:false, subtitleCellType: SubtitleCellValue.phoneNumber)
        let navigationController = UINavigationController(rootViewController: contactPickerViewController)
        self.present(navigationController, animated: true, completion: nil)
    }
    
    func epContactPicker(_: EPContactsPicker, didSelectContact contact : EPContact){
        if contact.phoneNumbers.count > 0{
            let mobile = enhancePhoneNumber(number: safeString(contact.phoneNumbers[0].phoneNumber))
            let name = enhanceName(name:safeString(contact.displayName(),alternate: "Unknown"))
            for c in self.emergencyContacts {
                if c["mobile"] == mobile {
                    showNotification("📱number already exist", showOnNavigation: true, showAsError: true);return
                }
            }
            setEmergencyContact(name: name, number: mobile, index: editingContact-1)
        }else{
            showNotification("No 📱number associated", showOnNavigation: true, showAsError: true)
        }
    }
    
    func enhancePhoneNumber(number:String)->String{
        var enhanced = number
        enhanced = enhanced.replacingOccurrences(of: "+91", with: "")
        enhanced = enhanced.replacingOccurrences(of: "+", with: "")
        enhanced = safeString(enhanced.removingPercentEncoding)
        enhanced = enhanced.replacingOccurrences(of: "[^0-9]", with: "", options: .regularExpression, range: nil)
        return enhanced
    }
    
    
    func enhanceName(name:String)->String{
        var enhanced = name
        enhanced = enhanced.replacingOccurrences(of: "+91", with: "")
        enhanced = enhanced.replacingOccurrences(of: "+", with: "")
        enhanced = safeString(enhanced.removingPercentEncoding)
        enhanced = enhanced.enhancedString()
        return enhanced
    }
    
    func setEmergencyContact(name:String,number:String,index:Int){
        if self.emergencyContacts.count > index {
            self.emergencyContacts[index] = ["name":name,"mobile":number]
        }else{
            self.emergencyContacts.append(["name":name,"mobile":number])
        }
        saveToCacheAndUpdateToServer()
    }
    
    func saveToCacheAndUpdateToServer(){
        CacheManager.sharedInstance.saveObject(self.emergencyContacts, identifier: "cacheEmergencyContacts")
        updateUserInterfaceOnScreen()
        let information = NSMutableDictionary()
        var contactsAsJson = "["
        for c in self.emergencyContacts {
            contactsAsJson.append("{\"name\":\"\(safeString(c["name"]))\",\"mobile\":\"\(safeString(c["mobile"]))\"},")
        }
        if contactsAsJson.length > 1 {
            contactsAsJson = contactsAsJson.substring(0, to: contactsAsJson.length-2)
        }
        contactsAsJson.append("]")
        contactsAsJson = contactsAsJson.aURLReady()
        copyData(contactsAsJson, destinationDictionary: information, destinationKey: "contacts", methodName: #function)
        let scm = ServerCommunicationManager()
        scm.responseErrorOption = ResponseErrorOption.dontShowErrorResponseMessage
        scm.updateEmergencyContacts(information) { (responseData) -> () in
            self.fetchAndUpdateEmergencyContacts(forceUpdate: true)
        }
    }
    
    func fetchAndUpdateEmergencyContacts(forceUpdate:Bool=false){
        if isSystemReadyToProcessThis() {
            if isInternetConnectivityAvailable(false){
                var shouldPerformNewRequest = true
                if let lastCachedDateOffers = CacheManager.sharedInstance.loadObject("lastCachedDateEmergencyContacts") as? Date {
                    if lastCachedDateOffers.isLaterThanDate(Date().dateBySubtractingMinutes(1)){
                        //cache is almost latest
                        shouldPerformNewRequest = false
                    }
                }
                if forceUpdate {
                    shouldPerformNewRequest = true
                }
                if shouldPerformNewRequest {
                    let scm = ServerCommunicationManager()
                    scm.responseErrorOption = ResponseErrorOption.dontShowErrorResponseMessage
                    scm.getEmergencyContacts(NSDictionary()) { (responseData) -> () in
                        if let emergencyContactsArray = responseData?.value(forKeyPath: "data.contacts") as? [Dictionary<String, String>]{
                            CacheManager.sharedInstance.saveObject(emergencyContactsArray, identifier: "cacheEmergencyContacts")
                            CacheManager.sharedInstance.saveObject(Date() , identifier: "lastCachedDateEmergencyContacts")
                            self.updateUserInterfaceOnScreen()
                        }
                    }
                }
            }
        }
    }
    
    
}
