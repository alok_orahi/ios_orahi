//
//  OrahiSpecificConstants.h
//  Extension1
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import UIKit

// constants strings
let FONT_BOLD = "SFUIText-Semibold"
let FONT_SEMI_BOLD = "SFUIText-Medium"
let FONT_REGULAR = "SFUIText-Regular"
let KEY_USERID = "userId"
let KEY_WEEKLY_PLANNER = "WEEKLY_RIDE_PLAN_CACHE"
let USER_DEFAULTS_EXTENSION_GROUP_ID = "group.orahi.extension"
let CONTAINER_APP_URL = "fb162084094285925"

// constants color
let APP_THEME_VOILET_COLOR = UIColor(red: 75/255, green: 19/255, blue: 82/255, alpha: 1.0)
let APP_THEME_RED_COLOR = UIColor(red: 230/255, green: 112/255, blue: 99/255, alpha: 1.0)
let APP_THEME_GREEN_COLOR = UIColor(red: 22/255, green: 168/255, blue: 120/255, alpha: 1.0)
let APP_THEME_BLUE_COLOR = UIColor(red: 51/255, green: 86/255, blue: 214/255, alpha: 1.0)
let APP_THEME_YELLOW_COLOR = UIColor(red: 236/255, green: 182/255, blue: 72/255, alpha:0.95)
let APP_THEME_GRAY_COLOR = UIColor.gray.withAlphaComponent(0.95)
let APP_THEME_LIGHT_GRAY_COLOR = UIColor(red: 241/255, green: 241/255, blue: 241/255, alpha:1.0)

// constants descriptions
let MESSAGE_TEXT___FOR_SERVER_NOT_REACHABILITY = "Connection failed!. Please try again!"

// constant feature enable/disable flags
let ENABLE_LOGGING = false
