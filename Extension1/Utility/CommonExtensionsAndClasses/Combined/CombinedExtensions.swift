import UIKit

var urlEncodingAllowedCharacterSets : NSMutableCharacterSet?

public extension NSAttributedString {
    func heightWithConstrainedWidth(_ width: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, context: nil)
        return ceil(boundingBox.height)
    }
}

public extension UILabel{
    func requiredHeight() -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = self.font
        label.text = self.text
        label.sizeToFit()
        return label.frame.height
    }
}

public extension String {
    var length: Int { return (self as NSString).length }
    func heightWithConstrainedWidth(_ width: CGFloat,maxHeight: CGFloat, font: UIFont) -> CGRect {
        let constraintRect = CGSize(width: width, height: maxHeight)
        let boundingBox = self.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        return boundingBox
    }
}

extension Double {
    // If you don't want your code crash on each overflow, use this function that operates on optionals
    // E.g.: Int(Double(Int.max) + 1) will crash:
    // fatal error: floating point value can not be converted to Int because it is greater than Int.max
    func toInt() -> Int? {
        if self > Double(Int.min) && self < Double(Int.max) {
            return Int(self)
        } else {
            return nil
        }
    }
}

extension Float {
    func toInt() -> Int? {
        if self > Float(Int.min) && self < Float(Int.max) {
            return Int(self)
        } else {
            return nil
        }
    }
}

extension String {
    func indexOf(_ target: String) -> Int? {
        let range = (self as NSString).range(of: target)
        guard range.toRange() != nil else {
            return nil
        }
        return range.location
    }
    func lastIndexOf(_ target: String) -> Int? {
        let range = (self as NSString).range(of: target, options: NSString.CompareOptions.backwards)
        guard range.toRange() != nil else {
            return nil
        }
        return self.length - range.location - 1
        
    }
    func contains(_ s: String) -> Bool {
        return (self.range(of: s) != nil) ? true : false
    }
    func trimmedString()->String {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
}

extension String {
    func chopPrefix(_ count: Int = 1) -> String {
        return self.substring(from: self.characters.index(self.startIndex, offsetBy: count))
    }
    
    func chopSuffix(_ count: Int = 1) -> String {
        return self.substring(to: self.characters.index(self.endIndex, offsetBy: -count))
    }
    
    func enhancedString()->String {
        var string = self
        let pattern = "^\\s+|\\s+$|\\s+(?=\\s)"
        string = string.replacingOccurrences(of: pattern, with: "", options: .regularExpression)
        string = string.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        return string.capitalized
    }
    
    func firstName()->String {
        let string = self
        let components = string.components(separatedBy: " ")
        if (components as NSArray).count > 0{
            return components[0]
        }
        return string
    }
    
    func asNSURL()->URL {
        return URL(string: self.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)!)!
    }
    
    mutating func separateStringWithCaps(){
        var index = 1
        let mutableString = NSMutableString(string: self)
        while(index < mutableString.length){
            if CharacterSet.uppercaseLetters.contains(UnicodeScalar(mutableString.character(at: index))!){
                mutableString.insert(" ", at: index)
                index += 1
            }
            index += 1
        }
        self = String(mutableString)
    }
    
    func dateValue()->Date{
        var date : Date?
        if isNotNull(self){
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
            date = dateFormatter.date(from: self)!
        }
        return date!
    }
    
    func dateValueType1()->Date{
        var date : Date?
        if isNotNull(self){
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            date = dateFormatter.date(from: self)
            if isNull(date){
                dateFormatter.dateFormat = "dd-MM-yyyy"
                date = dateFormatter.date(from: self)
            }
            if isNull(date){
                dateFormatter.dateFormat = "dd MM yyyy"
                date = dateFormatter.date(from: self)
            }
            if isNull(date){
                dateFormatter.dateFormat = "yyyy MM dd"
                date = dateFormatter.date(from: self)
            }
        }
        if isNull(date){
            return Date()
        }
        return date!
    }
}

extension NSMutableString {
    func separateStringWithCaps(){
        var index = 1
        while(index < self.length){
            if CharacterSet.uppercaseLetters.contains(UnicodeScalar(self.character(at: index))!){
                self.insert(" ", at: index)
                index += 1
            }
            index += 1
        }
    }
}

extension NSMutableAttributedString {
    func applyAttributesBySearching(_ subString:String,attributes:NSDictionary) {
        do{
            let expression = try NSRegularExpression(pattern: subString,options:[.caseInsensitive])
            expression.enumerateMatches(in: self.string, options: [.reportProgress], range:  NSMakeRange(0, self.string.length), using: { (result, flags, stop) in
                if isNotNull(result){
                    let range = result!.rangeAt(0)
                    self.addAttributes(attributes as! [String : Any], range:range)
                }
            })
        }catch{}
    }
    func replaceOccuranceOf(_ subString:String,withImage:UIImage) {
        do{
            let expression = try NSRegularExpression(pattern: subString,options:[.caseInsensitive])
            expression.enumerateMatches(in: self.string, options: [.reportProgress], range:  NSMakeRange(0, self.string.length), using: { (result, flags, stop) in
                if isNotNull(result){
                    let range = result!.rangeAt(0)
                    let attachment = NSTextAttachment()
                    attachment.image = withImage
                    self.replaceCharacters(in: range, with: NSAttributedString(attachment: attachment))
                }
            })
        }catch{}
    }
    func replaceOccuranceOf(_ subString:String,withString:String,attributes:NSDictionary) {
        do{
            let stringToReplaceWith = NSAttributedString(string:withString,attributes: attributes as? [String : Any])
            let expression = try NSRegularExpression(pattern: subString,options:[.caseInsensitive])
            expression.enumerateMatches(in: self.string, options: [.reportProgress], range:  NSMakeRange(0, self.string.length), using: { (result, flags, stop) in
                if isNotNull(result){
                    let range = result!.rangeAt(0)
                    self.replaceCharacters(in: range, with:stringToReplaceWith)
                }
            })
        }catch{}
    }
}

extension UIImage {
    convenience init(view: UIView) {
        UIGraphicsBeginImageContextWithOptions(view.frame.size, false, UIScreen.main.scale)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.init(cgImage: image!.cgImage!)
    }
    convenience init(scrollView: UIScrollView) {
        let initialScrollViewFrame = scrollView.frame as CGRect
        let initialContentOffset = scrollView.contentOffset as CGPoint
        
        scrollView.contentOffset = CGPoint.zero;
        scrollView.frame = CGRect(x: 0,y: 0,width: scrollView.contentSize.width,height: scrollView.contentSize.height);
        UIGraphicsBeginImageContextWithOptions(scrollView.contentSize, false, UIScreen.main.scale)
        scrollView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        scrollView.frame = initialScrollViewFrame
        scrollView.contentOffset = initialContentOffset
        self.init(cgImage: image!.cgImage!)
    }
}

extension UIView {
    func showActivityIndicatorAtPoint(_ point:CGPoint) {
        hideActivityIndicator()
        let activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        activityIndicatorView.startAnimating()
        activityIndicatorView.center = point
        activityIndicatorView.tag = 102345
        activityIndicatorView.transform = CGAffineTransform(scaleX: 0.6,y: 0.6)
        self.addSubview(activityIndicatorView)
        activityIndicatorView.isHidden = true;
        DispatchQueue.main.async { () -> Void in
            activityIndicatorView.center = point
            activityIndicatorView.isHidden = false;
        }
    }
    func showActivityIndicatorType(_ point:CGPoint,style:UIActivityIndicatorViewStyle) {
        hideActivityIndicator()
        let activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: style)
        activityIndicatorView.startAnimating()
        activityIndicatorView.center = point
        activityIndicatorView.tag = 102345
        activityIndicatorView.transform = CGAffineTransform(scaleX: 0.6,y: 0.6)
        self.addSubview(activityIndicatorView)
        activityIndicatorView.isHidden = true;
        DispatchQueue.main.async { () -> Void in
            activityIndicatorView.center = point
            activityIndicatorView.isHidden = false;
        }
    }
    func showActivityIndicator() {
        hideActivityIndicator()
        let activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        activityIndicatorView.startAnimating()
        activityIndicatorView.center = self.center
        activityIndicatorView.center = self.center
        activityIndicatorView.tag = 102345
        activityIndicatorView.transform = CGAffineTransform(scaleX: 0.6,y: 0.6)
        self.addSubview(activityIndicatorView)
        activityIndicatorView.isHidden = true;
        DispatchQueue.main.async { () -> Void in
            activityIndicatorView.center = self.center
            activityIndicatorView.isHidden = false;
        }
    }
    func showActivityIndicatorWhite() {
        hideActivityIndicator()
        let activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
        activityIndicatorView.startAnimating()
        activityIndicatorView.center = self.center
        activityIndicatorView.tag = 102345
        activityIndicatorView.transform = CGAffineTransform(scaleX: 0.6,y: 0.6)
        self.addSubview(activityIndicatorView)
        activityIndicatorView.isHidden = true;
        DispatchQueue.main.async { () -> Void in
            activityIndicatorView.center = self.center
            activityIndicatorView.isHidden = false;
        }
    }
    func hideActivityIndicator() {
        self .viewWithTag(102345)?.removeFromSuperview()
    }
    
    func makeMeRound(){
        self.layer.cornerRadius = self.bounds.size.width/2
        self.layer.borderWidth = 0
        self.layer.masksToBounds = true
    }
}

extension UIImage {
    static func fromColor(_ color: UIColor ,size: CGSize) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor)
        context!.fill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}

extension NSDictionary {
    func removeNullValues()->NSMutableDictionary {
        let mutableCopySelf = self.mutableCopy() as! NSMutableDictionary
        let nullSet = mutableCopySelf.keysOfEntries(options: [.concurrent]) { (key, object, stop) -> Bool in
            return isNull(object)
        }
        mutableCopySelf.removeObjects(forKeys: Array(nullSet))
        return mutableCopySelf
    }
}

extension String {
    public func substring(_ from:Int = 0, to:Int = -1) -> String {
        var to = to
        if to < 0 {
            to = self.length + to
        }
        return self.substring(with: (self.characters.index(self.startIndex, offsetBy: from) ..< self.characters.index(self.startIndex, offsetBy: to+1)))
    }
    public func substring(_ from:Int = 0, length:Int) -> String {
        return self.substring(with: (self.characters.index(self.startIndex, offsetBy: from) ..< self.characters.index(self.startIndex, offsetBy: from+length)))
    }
}

extension UIButton {
    /// EZSwiftExtensions
    
    // swiftlint:disable function_parameter_count
    public convenience init(x: CGFloat, y: CGFloat, w: CGFloat, h: CGFloat, target: AnyObject, action: Selector) {
        self.init(frame: CGRect(x: x, y: y, width: w, height: h))
        addTarget(target, action: action, for: UIControlEvents.touchUpInside)
    }
    // swiftlint:enable function_parameter_count
    
    /// EZSwiftExtensions
    public func setBackgroundColor(_ color: UIColor, forState: UIControlState) {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        UIGraphicsGetCurrentContext()!.setFillColor(color.cgColor)
        UIGraphicsGetCurrentContext()!.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        let colorImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.setBackgroundImage(colorImage, for: forState)
    }
}

