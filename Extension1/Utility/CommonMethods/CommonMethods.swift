//
//  CommonMethods.swift
//  Extension1
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import Foundation
import UIKit

public func documentsDirectory() -> String {
    let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory,.userDomainMask, true)[0]
    return documentsPath
}

public func copyData(_ sourceDictionary:NSDictionary?,sourceKey:String?,destinationDictionary:NSDictionary?,destinationKey:String?,methodName:String?,asString:Bool = false) {
    if sourceDictionary != nil && sourceKey != nil && destinationDictionary != nil && destinationKey != nil && sourceDictionary!.object(forKey: sourceKey! as String) != nil {
        if asString {
            destinationDictionary?.setValue("\(sourceDictionary!.object(forKey: sourceKey!)!)", forKey: destinationKey!)
        }else{
            destinationDictionary?.setValue(sourceDictionary?.object(forKey: sourceKey!), forKey: destinationKey!)
        }
    }else{
        #if DEBUG
            reportMissingParameter(sourceKey!, methodName: methodName!)
        #endif
    }
}

public func copyData(_ data:Any?,destinationDictionary:NSDictionary?,destinationKey:String?,methodName:String?) {
    if data != nil && destinationDictionary != nil && destinationKey != nil{
        destinationDictionary?.setValue(data!, forKey: destinationKey!)
    }else{
        #if DEBUG
            reportMissingParameter(destinationKey!, methodName: methodName!)
        #endif
    }
}

public func printErrorMessage (_ error : Error? , methodName : String?) -> () {
    logMessage("\nERROR MESSAGE :--- \(error?.localizedDescription) ---IN METHOD : \(methodName)\n")
}

public func reportMissingParameter (_ missingParameter : String , methodName : String) -> () {
    logMessage("\nMISSING PARAMETER :--- \(missingParameter) ---IN METHOD : \(methodName)\n")
}

public func parsedJson (_ data : Data?,methodName: String) -> (AnyObject?) {
    let parsedData : Any?
    do {
        parsedData =  try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
    } catch {
        parsedData = nil
    }
    return parsedData as (AnyObject?)
}

public func parsedJsonFrom (_ data : Data?,methodName: String) -> (AnyObject?) {
    let parsedData : Any?
    do {
        parsedData =  try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
    } catch {
        parsedData = nil;
    }
    #if DEBUG
        DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async(execute: {
            let dataAsString : NSString? = NSString(data: data!,encoding: String.Encoding.utf8.rawValue);
            if parsedData != nil {
                logMessage("\n\nRECEIVED DATA AFTER PARSING IS \n\n\(methodName)\n\n\(parsedData!)\n\n\n")
            }else{
                if dataAsString != nil {
                    logMessage("\n\nRECEIVED DATA BEFORE PARSING IS \n\n\(methodName)\n\n\(dataAsString!)\n\n\n")
                }else{
                    logMessage("\n\nRECEIVED DATA BEFORE PARSING IS \n\n\(methodName)\n\n\(dataAsString)\n\n\n")
                }
                logMessage("\n\nRECEIVED DATA AFTER PARSING IS \n\n\(methodName)\n\n\(parsedData)\n\n\n")
            }
        })
    #endif
    return parsedData as (AnyObject?)
}

func UIColorFromRGB(_ rgbValue: UInt) -> UIColor {
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
   )
}

func isNull(_ object:Any?)->(Bool){
    if object != nil{
        if object! is NSNull {
            return true
        }else if object! is NSString {
            if object as! String == "<null>" || object as! String == "" || object as! String == "null" {
                return true
            }
        }
        return false
    }
    return true
}

func isNotNull(_ object:Any?)->(Bool){
    return !isNull(object)
}

func printFonts() {
    let fontFamilyNames = UIFont.familyNames
    for familyName in fontFamilyNames {
        logMessage("------------------------------")
        logMessage("Font Family Name = [\(familyName)]")
        let names = UIFont.fontNames(forFamilyName: familyName)
        logMessage("Font Names = [\(names)]")
    }
}

func setAppearanceForViewController(_ viewController : UIViewController?){
    if isNotNull(viewController){
        viewController!.edgesForExtendedLayout = UIRectEdge()
    }
}

func setBorder(_ view:UIView ,color:UIColor, width:CGFloat, cornerRadius:CGFloat,masksToBounds:Bool = true){
    view.layer.cornerRadius = cornerRadius
    view.layer.borderColor = color.cgColor
    view.layer.borderWidth = width
    view.layer.masksToBounds = masksToBounds
}

func addBottomBorder(_ view:UIView ,color:UIColor, height:CGFloat){
    view.viewWithTag(1024)?.removeFromSuperview()
    let border = UIView()
    border.tag = 1024
    border.frame = CGRect(x: 0, y: view.frame.size.height - height, width: view.frame.size.width, height: height)
    border.backgroundColor = color
    view.addSubview(border)
}

func setAppearanceForTableView(_ tableView : UITableView?){
    tableView!.separatorInset = UIEdgeInsets.zero
    tableView!.separatorStyle = UITableViewCellSeparatorStyle.none
    tableView!.backgroundColor = UIColor.clear
}

func registerNib(_ nibName:NSString,tableView:UITableView?){
    tableView?.register(UINib(nibName: nibName as String, bundle: nil), forCellReuseIdentifier: nibName as String)
}

func registerNib(_ nibName:NSString,collectionView:UICollectionView?){
    collectionView?.register(UINib(nibName: nibName as String, bundle: nil), forCellWithReuseIdentifier: nibName as String)
}

public func encodeStringToBase64(_ normal: Data)->NSString
{
    let base64Encoded = normal.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
    return base64Encoded as NSString
}

public func addBorderToButton(_ Color :UIColor , button:UIButton , borderWidth :CGFloat){
    button.layer.borderColor = Color.cgColor
    button.layer.borderWidth = borderWidth
}

public func storyBoardObject()->(UIStoryboard){
    return UIStoryboard(name: "Main", bundle: nil)
}

func viewController(_ identifier:NSString)->(UIViewController){
    return storyBoardObject().instantiateViewController(withIdentifier: identifier as String)
}

func dateformatterDateTime(_ date: Date) -> NSString{
    let dateFormatter: DateFormatter = DateFormatter()
    dateFormatter.dateFormat = "dd-MM-yyyy "
    return dateFormatter.string(from: date) as NSString
}

func dateformatterDateTimeServer(_ date: Date) -> NSString{
    let dateFormatter: DateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd"
    return dateFormatter.string(from: date) as NSString
}

func date(_ string:NSString)->Date {
    let dateFormatter: DateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    var date: Date = dateFormatter.date(from: string as String) as Date!
    let currentTimeZone:Foundation.TimeZone  = Foundation.TimeZone.autoupdatingCurrent;
    let utcTimeZone:Foundation.TimeZone  = Foundation.TimeZone(abbreviation: "UTC")!;
    let currentGMTOffset:NSInteger  = currentTimeZone.secondsFromGMT(for: date)
    let gmtOffset:NSInteger  = utcTimeZone.secondsFromGMT(for: date)
    let gmtInterval:TimeInterval = Double(currentGMTOffset - gmtOffset);
    date = Date(timeInterval: gmtInterval, since: date)
    return date
}

func dateYYYYMMDD(_ string:NSString)->Date {
    let dateFormatter: DateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd"
    var date: Date = dateFormatter.date(from: string as String) as Date!
    let currentTimeZone:Foundation.TimeZone  = Foundation.TimeZone.autoupdatingCurrent;
    let utcTimeZone:Foundation.TimeZone  = Foundation.TimeZone(abbreviation: "UTC")!;
    let currentGMTOffset:NSInteger  = currentTimeZone.secondsFromGMT(for: date)
    let gmtOffset:NSInteger  = utcTimeZone.secondsFromGMT(for: date)
    let gmtInterval:TimeInterval = Double(currentGMTOffset - gmtOffset)
    date = Date(timeInterval: gmtInterval, since: date)
    return dateFormatter.date(from: string as String) as Date!
}

func getHeightFor(_ text:String, font:UIFont, width:CGFloat) -> CGFloat{
    let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
    label.numberOfLines = 0
    label.lineBreakMode = NSLineBreakMode.byWordWrapping
    label.font = font
    label.text = text
    label.sizeToFit()
    return label.frame.height
}

func getHeightFor(_ text:String?, font:UIFont? , width:CGFloat, minimumHeight:CGFloat, maximumHeight:CGFloat) -> CGFloat{
    let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
    label.numberOfLines = 0
    label.lineBreakMode = NSLineBreakMode.byWordWrapping
    label.font = font
    label.text = text
    label.sizeToFit()
    if label.frame.height < minimumHeight{
        return minimumHeight
    }
    else if label.frame.height > maximumHeight{
        return maximumHeight
    }else{
        return label.frame.height
    }
}

func truncateAndAppendString(_ content:String, limit: Int, appendString:String)->String{
    var contentObject : String = content
    let range = contentObject.startIndex..<contentObject.characters.index(contentObject.startIndex, offsetBy: limit)
    contentObject = contentObject[range]
    contentObject = "\(contentObject) \(appendString)"
    return contentObject
}

func convertStringIntoDate(_ dateString: String!, with formatString: String) -> Date{
    let dateFormatter: DateFormatter = DateFormatter()
    dateFormatter.dateFormat = formatString
    var date: Date = dateFormatter.date(from: dateString) as Date!
    let currentTimeZone:Foundation.TimeZone  = Foundation.TimeZone.autoupdatingCurrent;
    let utcTimeZone:Foundation.TimeZone  = Foundation.TimeZone(abbreviation: "UTC")!;
    let currentGMTOffset:NSInteger  = currentTimeZone.secondsFromGMT(for: date)
    let gmtOffset:NSInteger  = utcTimeZone.secondsFromGMT(for: date)
    let gmtInterval:TimeInterval = Double(currentGMTOffset - gmtOffset);
    date = Date(timeInterval: gmtInterval, since: date)
    return date
}

func orientationChange(){
    let value:NSNumber = NSNumber(value: UIInterfaceOrientation.portrait.rawValue as Int)
    UIDevice.current.setValue(value, forKeyPath: "orientation")
    UIViewController.attemptRotationToDeviceOrientation()
}

func extractYoutubeID(_ youtubeURL: String) -> String{
    let regex = "(?<=v(=|/))([-a-zA-Z0-9_]+)|(?<=youtu.be/)([-a-zA-Z0-9_]+)"
    let regexPattern = try! NSRegularExpression(pattern: regex, options: NSRegularExpression.Options.caseInsensitive)
    let match: NSTextCheckingResult? = regexPattern.firstMatch(in: youtubeURL, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, youtubeURL.length))
    let videoID = (youtubeURL as NSString).substring(with: match!.range).replacingOccurrences(of: "v=", with: "")
    return videoID
}

func isDocumentDownloaded(_ fileName: String) -> Bool {
    let fileMgr: FileManager = FileManager.default
    let documentsDirectory: String = NSHomeDirectory() + "/Documents/"
    let currentFile: String = documentsDirectory + fileName
    let fileExists: Bool = fileMgr.fileExists(atPath: currentFile)
    return fileExists
}

func getExistingDocumentPathOnDocumentDirectory(_ fileName: String) -> String {
    let documentsDirectory: String = NSHomeDirectory() + "//Documents/"
    let currentFile: String = documentsDirectory + fileName
    return currentFile
}

func resizeImage(_ imageObj: UIImage, sizeChange: CGSize) -> UIImage {
    let hasAlpha = false
    let scale: CGFloat = 0.0
    UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
    imageObj.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
    let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
    return scaledImage!
}

func getFilesOnDevice() ->  [Any]{
    let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
    let documentDirectory = paths[0]
    let manager = FileManager.default
    var items: NSArray = NSArray()
    if let allItems = try? manager.contentsOfDirectory(atPath: documentDirectory) {
        items = allItems as NSArray
    }
    return items as! [Any]
}

func getDistance(_ one:CGPoint,two:CGPoint)->Double{
    let value1:Double = (Double(two.x) - Double(one.x)) * (Double(two.x) - Double(one.x))
    let value2:Double = (Double(two.y) - Double(one.y)) * (Double(two.y) - Double(one.y))
    return sqrt(value1 + value2);
}

func deviceTokenUsingData(_ tokenData:Data)-> String {
    let tokenChars = (tokenData as NSData).bytes.bindMemory(to: CChar.self, capacity: tokenData.count)
    var tokenString = ""
    for i in 0 ..< tokenData.count {
        tokenString += String(format: "%02.2hhx", arguments: [tokenChars[i]])
    }
    return tokenString
}

func deviceToken()->String{
    if let deviceToken = UserDefaults.standard.object(forKey: "deviceToken") as? String{
        return deviceToken
    }else{
        return "PLACE HOLDER DEVICE TOKEN"
    }
}

func setDeviceToken(_ token:String?){
    UserDefaults.standard.set(token, forKey: "deviceToken")
}

func fixrotation(_ image:UIImage)->UIImage{
    if (image.imageOrientation == UIImageOrientation.up){
        return image;
    }
    var transform:CGAffineTransform = CGAffineTransform.identity;
    switch image.imageOrientation{
    case UIImageOrientation.down:
        transform = transform.translatedBy(x: image.size.width, y: image.size.height);
        transform = transform.rotated(by: CGFloat(M_PI))
        break;
    case UIImageOrientation.downMirrored:
        transform = transform.translatedBy(x: image.size.width, y: image.size.height);
        transform = transform.rotated(by: CGFloat(M_PI))
        break;
    case UIImageOrientation.left:
        transform = transform.translatedBy(x: image.size.width, y: 0);
        transform = transform.rotated(by: CGFloat(M_PI_2));
        break;
    case UIImageOrientation.leftMirrored:
        transform = transform.translatedBy(x: image.size.width, y: 0);
        transform = transform.rotated(by: CGFloat(M_PI_2));
        break;
    case UIImageOrientation.right:
        transform = transform.translatedBy(x: 0, y: image.size.height);
        transform = transform.rotated(by: CGFloat(-M_PI_2));
        break;
    case UIImageOrientation.rightMirrored:
        transform = transform.translatedBy(x: 0, y: image.size.height);
        transform = transform.rotated(by: CGFloat(-M_PI_2));
        break;
    default:
        break;
    }
    
    switch (image.imageOrientation){
    case UIImageOrientation.upMirrored:
        transform = transform.translatedBy(x: image.size.width, y: 0);
        transform = transform.scaledBy(x: -1, y: 1);
        break;
    case UIImageOrientation.downMirrored:
        transform = transform.translatedBy(x: image.size.width, y: 0);
        transform = transform.scaledBy(x: -1, y: 1);
        break;
        
    case UIImageOrientation.leftMirrored:
        transform = transform.translatedBy(x: image.size.height, y: 0);
        transform = transform.scaledBy(x: -1, y: 1);
        break;
    case UIImageOrientation.rightMirrored:
        transform = transform.translatedBy(x: image.size.height, y: 0);
        transform = transform.scaledBy(x: -1, y: 1);
        break;
    default:
        break;
    }
    
    // Now we draw the underlying CGImage into a new context, applying the transform
    // calculated above.
    let  ctx:CGContext = CGContext(data: nil, width: Int(image.size.width), height: Int(image.size.height), bitsPerComponent: image.cgImage!.bitsPerComponent, bytesPerRow: 0, space: image.cgImage!.colorSpace!, bitmapInfo: image.cgImage!.bitmapInfo.rawValue)!
    ctx.concatenate(transform);
    switch (image.imageOrientation){
    case UIImageOrientation.left:
        ctx.draw(image.cgImage!, in: CGRect(x: 0,y: 0,width: image.size.height,height: image.size.width));
    case UIImageOrientation.leftMirrored:
        ctx.draw(image.cgImage!, in: CGRect(x: 0,y: 0,width: image.size.height,height: image.size.width));
    case UIImageOrientation.right:
        ctx.draw(image.cgImage!, in: CGRect(x: 0,y: 0,width: image.size.height,height: image.size.width));
    case UIImageOrientation.rightMirrored:
        ctx.draw(image.cgImage!, in: CGRect(x: 0,y: 0,width: image.size.height,height: image.size.width));
        break;
        
    default:
        ctx.draw(image.cgImage!, in: CGRect(x: 0,y: 0,width: image.size.width,height: image.size.height));
        break;
    }
    let cgimg:CGImage = ctx.makeImage()!;
    let img:UIImage = UIImage(cgImage: cgimg)
    return img;
}

func humanReadableScreenName(_ screenName:String)->(String){
    var humanReadableName = NSMutableString(string: screenName)
    humanReadableName = NSMutableString(string:humanReadableName.replacingOccurrences(of: "ViewController", with: ""))
    humanReadableName.separateStringWithCaps()
    return humanReadableName as (String)
}

func performAnimatedClickEffectType1(_ view:UIView){
    UIView.animate(withDuration: 0.08, animations: { () -> Void in
        view.transform = CGAffineTransform(scaleX: 0.95, y: 0.95)
    }, completion: { (status) -> Void in
        UIView.animate(withDuration: 0.08, animations: { () -> Void in
            view.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
        }, completion: { (status) -> Void in
            UIView.animate(withDuration: 0.08, animations: { () -> Void in
                view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            }, completion: { (status) -> Void in
            }) 
        }) 
    }) 
}

func performAnimationEffectType2(_ view:UIView,minS:CGFloat,maS:CGFloat,dur:Double){
    UIView.animate(withDuration: dur, animations: { () -> Void in
        view.transform = CGAffineTransform(scaleX: minS, y: minS)
    }, completion: { (status) -> Void in
        UIView.animate(withDuration: dur, animations: { () -> Void in
            view.transform = CGAffineTransform(scaleX: maS, y: maS)
        }, completion: { (status) -> Void in
            UIView.animate(withDuration: dur, animations: { () -> Void in
                view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            }, completion: { (status) -> Void in
            }) 
        }) 
    }) 
}

func timePart(_ dateAsString:NSString)->String{
    return dateAsString.substring(with: NSRange(location: 11,length: 5))
}

func datePart(_ dateAsString:NSString)->String{
    return dateAsString.substring(with: NSRange(location: 0,length: 10))
}

func getArray(_ key:String,from:NSDictionary)->NSMutableArray{
    var result = NSMutableArray()
    if isNotNull(from[key])&&(from[key]! as AnyObject) is NSMutableArray{
        result = from[key] as! NSMutableArray
    }
    return result
}

func dictionary(_ from:Any?)->NSDictionary{
    if from is NSArray {
        let fromArray = from as! NSArray
        let result = NSMutableDictionary()
        for content in fromArray {
            result.setObject("1", forKey:(content as! String as NSCopying))
        }
        return result
    }
    if from is NSDictionary {
        return from as! NSDictionary
    }
    return NSDictionary()
}

func generateRandomDouble(_ sN:Double,bN:Double)->Double {
    let diff = bN - sN
    return (((Double) (Double(arc4random()).truncatingRemainder(dividingBy: (Double(RAND_MAX) + 1))) / Double(RAND_MAX)) * diff) + sN;
}

func currentTimeStamp() -> Int64{
    return Int64(Date().timeIntervalSince1970)
}

func remaningTime(_ startDate:Date ,endDate:Date)->String {
    let calendar = Calendar.current
    let components = (calendar as NSCalendar).components([.day,.hour,.minute,.second], from: startDate, to: endDate, options: [])
    if components.day! > 0 {
        return "\(components.day)d : \(components.hour)h : \(components.minute)m : \(components.second)s"
    }else{
        if components.hour! > 0 {
            return "\(components.hour)h : \(components.minute)m : \(components.second)s"
        }else{
            return "\(components.minute)m : \(components.second)s"
        }
    }
}

func elapsedTime(_ startDate:Date)->String {
    let calendar = Calendar.current
    let components = (calendar as NSCalendar).components([.day,.hour,.minute,.second], from: startDate, to: Date(), options: [])
    if components.day! > 0 {
        return "\(components.day)d : \(components.hour)h : \(components.minute)m : \(components.second)s"
    }else{
        if components.hour! > 0 {
            return "\(components.hour)h : \(components.minute)m : \(components.second)s"
        }else{
            return "\(components.minute)m : \(components.second)s"
        }
    }
}

public func isInternetConnectivityAvailable () -> Bool {
    return true
}

func logMessage(_ data:Any?) {
    if ENABLE_LOGGING {
        if data != nil {
            logMessage("\(data!)")
        }else{
            logMessage("nil")
        }
    }
}

