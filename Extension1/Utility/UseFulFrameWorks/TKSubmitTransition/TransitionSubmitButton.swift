import Foundation
import UIKit

let PINK = UIColor(red:0.992157, green: 0.215686, blue: 0.403922, alpha: 1)
let DARK_PINK = UIColor(red:0.798012, green: 0.171076, blue: 0.321758, alpha: 1)

@IBDesignable
open class TKTransitionSubmitButton : UIButton, UIViewControllerTransitioningDelegate , CAAnimationDelegate {
    
    open var didEndFinishAnimation : (()->())? = nil
    
    let springGoEase = CAMediaTimingFunction(controlPoints: 0.45, -0.36, 0.44, 0.92)
    let shrinkCurve = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
    let expandCurve = CAMediaTimingFunction(controlPoints: 0.95, 0.02, 1, 0.05)
    let shrinkDuration: CFTimeInterval  = 0.1
    
    lazy var spiner: SpinerLayerS! = {
        let s = SpinerLayerS(frame: self.frame)
        self.layer.addSublayer(s)
        return s
    }()
    
    @IBInspectable open var highlightedBackgroundColor: UIColor? = DARK_PINK {
        didSet {
            self.setBackgroundColor()
        }
    }
    @IBInspectable open var normalBackgroundColor: UIColor? = PINK {
        didSet {
            self.setBackgroundColor()
        }
    }
    
    var cachedTitle: String?
    var cachedCornerRadius: CGFloat?
    var cachedImageDefault: UIImage?
    var cachedImageSelected: UIImage?
    var cachedImageHighlighted: UIImage?
    var cachedBackgroundColor: UIColor?
    var requiredBackgroundColor: UIColor?
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    override open var isHighlighted: Bool {
        didSet {
            self.setBackgroundColor()
        }
    }
    
    public required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.setup()
    }
    
    func setup() {
        self.clipsToBounds = true
        self.setBackgroundColor()
    }
    
    func setBackgroundColor() {
        if (isHighlighted) {
            self.backgroundColor = highlightedBackgroundColor
        }else {
            self.backgroundColor = normalBackgroundColor
        }
    }
    
    open func startLoadingAnimation() {
        self.cachedCornerRadius = self.layer.cornerRadius
        self.layer.cornerRadius = self.frame.height / 2
        self.cachedTitle = title(for: UIControlState.normal)
        self.cachedImageDefault = self.image(for: UIControlState.normal)
        self.cachedImageSelected = self.image(for: .selected)
        self.cachedImageHighlighted = self.image(for: .highlighted)
        self.cachedBackgroundColor = self.normalBackgroundColor
        if isNotNull(self.requiredBackgroundColor){
            self.normalBackgroundColor = self.requiredBackgroundColor
        }
        self.setImage(nil, for: UIControlState.normal)
        self.setImage(nil, for: .selected)
        self.setImage(nil, for: .highlighted)
        self.setTitle("", for: UIControlState.normal)
        self.shrink()
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64((shrinkDuration - 0.25) * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) {
            self.spiner.animation()
        }
    }
    
    open func startFinishAnimation(_ delay: TimeInterval, completion:(()->())?) {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) {
            self.didEndFinishAnimation = completion
            self.expand()
            self.spiner.stopAnimation()
        }
    }
    
    open func animate(_ duration: TimeInterval, completion:(()->())?) {
        startLoadingAnimation()
        startFinishAnimation(duration, completion: completion)
    }
    
    open func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        let a = anim as! CABasicAnimation
        if a.keyPath == "transform.scale" {
            didEndFinishAnimation?()
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) {
                self.returnToOriginalState()
            }
        }
    }
    
    open func returnToOriginalState() {
        self.layer.removeAllAnimations()
        self.setTitle(self.cachedTitle, for: UIControlState.normal)
        self.setImage(cachedImageDefault, for: UIControlState.normal)
        self.setImage(cachedImageSelected, for: .selected)
        self.setImage(cachedImageHighlighted, for: .highlighted)
        self.normalBackgroundColor = self.cachedBackgroundColor
        self.layer.cornerRadius = self.cachedCornerRadius!
    }
    
    open func stopIt() {
        self.layer.removeAllAnimations()
        self.setTitle(self.cachedTitle, for: UIControlState.normal)
        self.setImage(cachedImageDefault, for: UIControlState.normal)
        self.setImage(cachedImageSelected, for: .selected)
        self.setImage(cachedImageHighlighted, for: .highlighted)
        self.normalBackgroundColor = self.cachedBackgroundColor
        self.spiner.stopAnimation()
        self.layer.cornerRadius = self.cachedCornerRadius!
    }
    
    func shrink() {
        let shrinkAnim = CABasicAnimation(keyPath: "bounds.size.width")
        shrinkAnim.fromValue = frame.width
        shrinkAnim.toValue = frame.height
        shrinkAnim.duration = shrinkDuration
        shrinkAnim.timingFunction = shrinkCurve
        shrinkAnim.fillMode = kCAFillModeForwards
        shrinkAnim.isRemovedOnCompletion = false
        layer.add(shrinkAnim, forKey: shrinkAnim.keyPath)
    }
    
    func expand() {
        let expandAnim = CABasicAnimation(keyPath: "transform.scale")
        expandAnim.fromValue = 1.0
        expandAnim.toValue = 26.0
        expandAnim.timingFunction = expandCurve
        expandAnim.duration = 0.3
        expandAnim.delegate = self
        expandAnim.fillMode = kCAFillModeForwards
        expandAnim.isRemovedOnCompletion = false
        layer.add(expandAnim, forKey: expandAnim.keyPath)
    }
    
}
