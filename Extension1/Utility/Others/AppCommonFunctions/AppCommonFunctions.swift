//
//  AppCommonFunctions.swift
//  Extension1
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Copyright © 2016 Orahi. All rights reserved.
//

//MARK: - AppCommonFunctions : This singleton class implements some app specific functions which are frequently needed in application.

import Foundation
import UIKit

//MARK: - Completion block
typealias ACFCompletionBlock = (_ returnedData :Any?) ->()
typealias ACFModificationBlock = (_ viewControllerObject :Any?) ->()

enum NextRideEstimatedMoment{
    case morning
    case evening
}

enum TravelRequestStatus {
    case notInitialisedYet
    case travelRequestSent
    case travelRequestReceived
    case travelRequestRejected
    case travelRequestRejectedByMe
    case travelRequestAccepted
    case travelCancelled
    case travelCompleted
}

enum TravelRequestAvailableAction {
    case invite
    case ignore
    case accept
    case reject
    case cancel
    case settle
    case feedback
    case none
}

enum UserType {
    case carOwner
    case passenger
    case unIdentified
}

enum RideStatus {
    case none
    case invitationSent
    case invitationReceived
    case confirmed
    case confirmedButDelayed
    case completed
}

enum OfferStatus {
    case available
    case notApplicable
    case notAvailable
    case expired
}

func Acf()->AppCommonFunctions{
    return AppCommonFunctions.sharedInstance
}

class AppCommonFunctions: NSObject{
    
    static let sharedInstance : AppCommonFunctions = {
        let instance = AppCommonFunctions()
        return instance
    }()
    
    fileprivate override init() {
        
    }
    
    func colorForRideStatus(_ user:NSDictionary)->UIColor{
        let situation = Acf().getTravelRequestStatus(user)
        if situation == .notInitialisedYet {
            return APP_THEME_VOILET_COLOR
        }else if situation == .travelRequestSent {
            return  APP_THEME_GRAY_COLOR
        }else if situation == .travelRequestReceived {
            return  APP_THEME_VOILET_COLOR
        }else if situation == .travelRequestRejected {
            return  APP_THEME_GRAY_COLOR
        }else if situation == .travelRequestRejectedByMe {
            return APP_THEME_VOILET_COLOR
        }else if situation == .travelRequestAccepted {
            return  APP_THEME_GRAY_COLOR
        }else if situation == .travelCancelled {
            return  APP_THEME_GRAY_COLOR
        }else if situation == .travelCompleted {
            return  APP_THEME_VOILET_COLOR
        }else{
            return UIColor.clear
        }
    }
    
    func getRideStatus(_ users:NSMutableArray?,dateOfRide:Date)->RideStatus{
        var invitationSent = false
        var invitationReceived = false
        var confirmed = false
        var completed = false
        if let _ = users {
            for user in users! {
                if isNotNull((user as AnyObject).object(forKey: "rs_status")){
                    let rs_status = (user as AnyObject).object(forKey: "rs_status") as! NSString
                    if rs_status.isEqual(to: "0"){
                        invitationSent = true
                    }
                    else if rs_status.isEqual(to: "1"){
                        invitationReceived = true
                    }
                    else if rs_status.isEqual(to: "3"){
                        confirmed = true
                    }
                    else if rs_status.isEqual(to: "6"){
                        completed = true
                    }
                }
            }
        }
        if completed && !confirmed {
            return RideStatus.completed
        }
        if confirmed {
            if dateOfRide.isEarlierThanDate(Date().dateBySubtractingHours(6)){
                return RideStatus.confirmedButDelayed
            }
            return RideStatus.confirmed
        }
        if invitationSent {
            return RideStatus.invitationSent
        }
        if invitationReceived {
            return RideStatus.invitationReceived
        }
        return RideStatus.none
    }
    
    func decideAndSetUserRideStatusForWeeklyPlannerCell(_ user:NSDictionary,button:UIButton,colorStatusView:UIView){
        var font = UIFont.init(name: FONT_SEMI_BOLD, size: 13)
        var title = ""
        var enabled = false
        var textColor = APP_THEME_VOILET_COLOR
        var statusColor = UIColor.clear
        var backGroundColor = UIColor.white.withAlphaComponent(0.2)
        let situation = Acf().getTravelRequestStatus(user)
        if situation == .notInitialisedYet || situation == .travelRequestRejected || situation == .travelRequestRejectedByMe || situation == .travelCancelled {
            font = UIFont.init(name: FONT_BOLD, size: 16)
            title = "Fix My Ride"
            textColor = APP_THEME_VOILET_COLOR
            statusColor = APP_THEME_LIGHT_GRAY_COLOR
            backGroundColor = UIColor.clear
            enabled = true
        }else if situation == .travelRequestSent {
            font = UIFont.init(name: FONT_SEMI_BOLD, size: 13)
            title = "Awaiting"
            textColor = UIColor.white
            statusColor = APP_THEME_YELLOW_COLOR
            enabled = false
        }else if situation == .travelRequestReceived {
            font = UIFont.init(name: FONT_SEMI_BOLD, size: 13)
            title = "Received"
            textColor = UIColor.white
            statusColor = APP_THEME_YELLOW_COLOR
            enabled = false
        }else if situation == .travelRequestAccepted {
            font = UIFont.init(name: FONT_SEMI_BOLD, size: 13)
            title = "Fixed"
            textColor = UIColor.white
            statusColor = APP_THEME_GREEN_COLOR
            enabled = false
        }else if situation == .travelCompleted {
            font = UIFont.init(name: FONT_SEMI_BOLD, size: 13)
            title = "Completed"
            textColor = UIColor.white
            statusColor = APP_THEME_VOILET_COLOR
            enabled = false
        }
        colorStatusView.backgroundColor = statusColor
        button.setTitle(title, for: UIControlState.normal)
        button.setTitleColor(textColor, for: UIControlState.normal)
        button.setBackgroundColor(backGroundColor, forState: UIControlState.normal)
        button.setBackgroundColor(backGroundColor, forState: .highlighted)
        button.setBackgroundColor(backGroundColor, forState: .disabled)
        button.isEnabled = enabled
        button.titleLabel!.font = font
        button.setNeedsDisplay()
        button.layoutIfNeeded()
    }
    
    func getTravelRequestStatus(_ user:NSDictionary)->TravelRequestStatus{
        let status = TravelRequestStatus.notInitialisedYet
        if isNotNull(user.object(forKey: "rs_status")){
            let rs_status = user.object(forKey: "rs_status") as! NSString
            if rs_status.isEqual(to: "0"){
                return TravelRequestStatus.travelRequestSent
            }
            else if rs_status.isEqual(to: "1"){
                return TravelRequestStatus.travelRequestReceived
            }
            else if rs_status.isEqual(to: "2"){
                return TravelRequestStatus.travelRequestRejected
            }
            else if rs_status.isEqual(to: "3"){
                return TravelRequestStatus.travelRequestAccepted
            }
            else if rs_status.isEqual(to: "4"){
                return TravelRequestStatus.travelRequestRejectedByMe
            }
            else if rs_status.isEqual(to: "5"){
                return TravelRequestStatus.travelCancelled
            }
            else if rs_status.isEqual(to: "6"){
                return TravelRequestStatus.travelCompleted
            }
        }
        return status
    }
}

func getTimeFormatInAmPm(_ time:String?)->String{
    if isNotNull(time) {
        let components = time!.components(separatedBy: ":")
        if components.count >= 3{
            let hours = Int(components[0])
            let minutes =  Int(components[1])
            let seconds =  Int(components[2])
            return Date().dateAtStartOfDay().change( year: nil, month: nil, day: nil, hour: hours, minute: minutes, second: seconds).toStringValue(format:"h:mm a")
        }else{
            return time ?? ""
        }
    }else{
        return ""
    }
}

func isUserLoggedIn()->Bool{
    return !(loggedInUserId() as NSString).isEqual(to: "0")
}

func loggedInUserId()->String{
    if let userId = getValueFromUserDefaultsForExtensionsAccess(KEY_USERID) as? String{
        return userId
    }
    return "0"
}

func saveThisDataForExtensionsAccess(_ data:Any?,key:String) {
    func performTask(){
        if let userDefaults = UserDefaults(suiteName: USER_DEFAULTS_EXTENSION_GROUP_ID) {
            if isNotNull(data){
                userDefaults.set(NSKeyedArchiver.archivedData(withRootObject: data!), forKey: key)
            }else{
                userDefaults.removeObject(forKey: key)
            }
            userDefaults.synchronize()
        }
    }
    performTask()
}

func getValueFromUserDefaultsForExtensionsAccess(_ key:String)->Any? {
    if let userDefaults = UserDefaults(suiteName: USER_DEFAULTS_EXTENSION_GROUP_ID) {
        if let contentAsData = userDefaults.object(forKey: key) as? Data{
            return NSKeyedUnarchiver.unarchiveObject(with: contentAsData)
        }
    }
    return nil
}

