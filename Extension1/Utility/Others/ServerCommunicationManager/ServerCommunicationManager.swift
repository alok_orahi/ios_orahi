//
//  ServerCommunicationManager.swift
//  Extension1
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Copyright © 2016 Orahi. All rights reserved.
//

//MARK: - ServerCommunicationManager : This class handles communication of application with its Server.

import Foundation
import AFNetworking

//MARK: - ServerCommunicationConstants
struct ServerCommunicationConstants {
    static let RESPONSE_CODE_KEY = "status"
    static let RESPONSE_CODE_SUCCESS_VALUE = "Optional(1)"
    static let RESPONSE_CODE_FAILURE_VALUE = "Optional(0)"
    static let RESPONSE_MESSAGE_KEY = "message"
}

//MARK: - Completion block
typealias WSCompletionBlock = (_ responseData :Any?) ->()
typealias WSCompletionBlockForFile = (_ responseData :Data?) ->()

//MARK: - Custom methods
extension ServerCommunicationManager {
    //MARK: - WEEKLY RIDE PLAN
    func getWeeklyRidePlan(_ information: NSDictionary ,completionBlock: @escaping WSCompletionBlock) -> () {
        let parameters = NSMutableDictionary()
        copyData(loggedInUserId(), destinationDictionary: parameters, destinationKey: "id", methodName:#function)
        performGetRequest(parameters, urlString: BASE_URL+ServerSupportURLS.WEEKLY_RIDE_PLAN, completionBlock: completionBlock,methodName:#function)
    }
    
    //MARK: - RIDE
    
    func setMyRide(_ information: NSDictionary ,completionBlock: @escaping WSCompletionBlock) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "userId", destinationDictionary: parameters, destinationKey: "id", methodName:#function)
        copyData(information, sourceKey: "date", destinationDictionary: parameters, destinationKey: "date", methodName:#function)
        copyData(information, sourceKey: "mode", destinationDictionary: parameters, destinationKey: "mode", methodName:#function)
        copyData(information, sourceKey: "via", destinationDictionary: parameters, destinationKey: "via", methodName:#function)
        performPostRequest(parameters, urlString: BASE_URL+ServerSupportURLS.SET_MY_RIDE, completionBlock: completionBlock,methodName:#function)
    }
}

//MARK: - Private
class ServerCommunicationManager: NSObject {
    
    var completionBlock: WSCompletionBlock?
    var returnFailureResponseAlso = false
    var returnFailureUnParsedDataIfParsingFails = false
    var returnJSONData = false
    
    /// Check for cache and return from cache if possible.
    ///
    /// - parameter body: parameters to set in Body of the request
    /// - returns: bool (wether cache was used or not)
    func loadFromCacheIfPossible(_ body:NSDictionary? , urlString:NSString? ,completionBlock: WSCompletionBlock? , maxAgeInSeconds:Float)->(Bool){
        return false
    }
    
    func updateSecurityPolicy(_ manager:AFHTTPSessionManager){
        let securityPolicy = AFSecurityPolicy(pinningMode: AFSSLPinningMode.none)
        securityPolicy.validatesDomainName = false
        securityPolicy.allowInvalidCertificates = true
        manager.securityPolicy = securityPolicy
    }

    /// Perform  GET Request.
    ///
    /// - parameter body: parameters to set in Body of the request
    /// - returns: parsed server response via completionBlock
    func performGetRequest(_ body:NSDictionary? , urlString:String ,completionBlock: WSCompletionBlock?,methodName:String)->(){
        DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async(execute: {
            if isInternetConnectivityAvailable()==false {
                return;
            }
            let url = URL(string: urlString)
            
            logMessage("\n\n\n                  HITTING URL\n\n \(url!.absoluteString)\n\n\n                  WITH GET REQUEST\n\n\(body)\n\n")
            let manager = AFHTTPSessionManager()
            manager.responseSerializer = AFHTTPResponseSerializer()
            self.updateSecurityPolicy(manager)
            manager.get(urlString, parameters: body, progress: nil, success: { (urlSessionDataTask, responseObject) in
                self.verifyServerResponse(responseObject , error: nil, completionBlock: completionBlock,methodName: methodName)
            }, failure: { (urlSessionDataTask, error) in
                self.verifyServerResponse(nil, error: error as NSError?, completionBlock: completionBlock,methodName: methodName)
            })
        })
    }

    /// Perform Get Request For Downloading file data.
    ///
    /// - parameter url: url to download file
    /// - returns: fileData via completionBlock
    func performDownloadGetRequest(_ urlString:String ,completionBlock: WSCompletionBlockForFile?,methodName:String)->(){
        DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async(execute: {
            if isInternetConnectivityAvailable()==false {
                return;
            }
            let url = URL(string: urlString)
            logMessage("\n\n\n                  HITTING URL TO DOWNLOAD FILE\n\n \((url!.absoluteString))\n\n\n")
            let manager = AFHTTPSessionManager()
            manager.responseSerializer = AFHTTPResponseSerializer()
            manager.requestSerializer = AFHTTPRequestSerializer()
            self.updateSecurityPolicy(manager)
            self.addCommonInformationInHeader(manager.requestSerializer)
            manager.get(urlString , parameters: nil, progress: { (progress) -> Void in
                }, success: { (urlSessionDataTask, responseObject) -> Void in
                    completionBlock?(responseObject as! Data?)
            }){(urlSessionDataTask, error) -> Void in
            }
        })
    }

    /// Perform Json Encoded Post Request.
    ///
    /// - parameter body: parameters to set in Body of the request
    /// - returns: parsed server response via completionBlock
    func performJsonPostRequest(_ body:NSDictionary? , urlString:String ,completionBlock: WSCompletionBlock?,methodName:String)->(){
        DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async(execute: {
            if isInternetConnectivityAvailable()==false {
                return;
            }
            let url = URL(string: urlString)
            logMessage("\n\n\n                  HITTING URL\n\n \((url!.absoluteString))\n\n\n                  WITH POST JSON BODY\n\n\(body!)\n\n")
            
            let manager = AFHTTPSessionManager()
            manager.responseSerializer = AFHTTPResponseSerializer()
            manager.requestSerializer = AFJSONRequestSerializer()
            self.updateSecurityPolicy(manager)
            self.addCommonInformationInHeader(manager.requestSerializer)
            manager.post(urlString , parameters: body, progress: { (progress) -> Void in
                }, success: { (urlSessionDataTask, responseObject) in
                    self.verifyServerResponse(responseObject , error: nil, completionBlock: completionBlock,methodName: methodName)
            }) { (urlSessionDataTask, error) in
                self.verifyServerResponse(nil, error: error as NSError?, completionBlock: completionBlock,methodName: methodName)
            }
        })
    }
    /// Perform Post Request.
    ///
    /// - parameter body: parameters to set in Body of the request
    /// - returns: parsed server response via completionBlock
    func performPostRequest(_ body:NSDictionary? , urlString:String ,completionBlock: WSCompletionBlock? ,methodName:String?)->(){
        DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async(execute: {
            if isInternetConnectivityAvailable()==false {
                return;
            }
            let url = URL(string: urlString)
            logMessage("\n\n\n                  HITTING URL\n\n \((url!.absoluteString))\n\n\n                  WITH POST BODY\n\n\(body!)\n\n")
            let manager = AFHTTPSessionManager()
            manager.responseSerializer = AFHTTPResponseSerializer()
            manager.requestSerializer = AFHTTPRequestSerializer()
            self.updateSecurityPolicy(manager)
            self.addCommonInformationInHeader(manager.requestSerializer)
            manager.post(urlString, parameters: body, progress: { (progress) -> Void in
                }, success: { (urlSessionDataTask, responseObject) in
                    self.verifyServerResponse(responseObject , error: nil, completionBlock: completionBlock,methodName: methodName)
            }) { (urlSessionDataTask, error) in
                self.verifyServerResponse(nil, error: error as NSError?, completionBlock: completionBlock,methodName: methodName)
            }
        })
    }
    /// Perform Multipart Post Request.
    ///
    /// - parameter body: parameters to set in Body of the request
    /// - returns: parsed server response via completionBlock
    func performMultipartPostRequest(_ body:NSDictionary? , urlString:String? , constructBody:((AFMultipartFormData) -> Void)?,completionBlock: WSCompletionBlock? ,methodName:String?)->(){
        DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async(execute: {
            if isInternetConnectivityAvailable()==false {
                return;
            }
            let url = URL(string: urlString!)
            logMessage("\n\n\n                  HITTING URL\n\n \((url!.absoluteString))\n\n\n                  WITH POST BODY\n\n\(body!)\n\n")
            let manager = AFHTTPSessionManager()
            manager.responseSerializer = AFHTTPResponseSerializer()
            manager.requestSerializer = AFHTTPRequestSerializer()
            self.updateSecurityPolicy(manager)
            self.addCommonInformationInHeader(manager.requestSerializer)
            manager.post(urlString!, parameters: body, constructingBodyWith: constructBody, progress: { (progress) -> Void in
                }, success: { (urlSessionDataTask, responseObject) in
                    self.verifyServerResponse(responseObject , error: nil, completionBlock: completionBlock,methodName: methodName)
            }) { (urlSessionDataTask, error)  in
                self.verifyServerResponse(nil, error: error, completionBlock: completionBlock,methodName: methodName)
            }
        })
    }
    
    /// Add commonly used parameters to all request.
    ///
    /// - parameter information: pass the dictionary object here that you created to hold parameters required. This function will add commonly used parameter into it.
    func addCommonInformation(_ information:NSMutableDictionary?)->(){
        copyData(loggedInUserId(), destinationDictionary: information, destinationKey: "id", methodName: #function)
    }
    func addCommonInformationInHeader(_ requestSerialiser:AFHTTPRequestSerializer?)->(){
    }

    /// To check wether the server operation succeeded or not.
    /// - returns: bool
    func isSuccess(_ response:NSDictionary?)->(Bool){
        if response != nil{
            if "\(response?.object(forKey: ServerCommunicationConstants.RESPONSE_CODE_KEY))".isEqual(ServerCommunicationConstants.RESPONSE_CODE_SUCCESS_VALUE){
                return true
            }
        }
        return false
    }
    /// To check wether the server operation failed or not.
    /// - returns: bool
    func isFailure(_ response:NSDictionary?)->(Bool){
        if response != nil{
            if "\(response?.object(forKey: ServerCommunicationConstants.RESPONSE_CODE_KEY))".isEqual(ServerCommunicationConstants.RESPONSE_CODE_FAILURE_VALUE){
                return true
            }
        }
        return false
    }
    /// To verify the server response received and perform action on basis of that.
    /// - parameter response: data received from server in the form of NSData
    func verifyServerResponse(_ response:Any?,error:Error?,completionBlock: WSCompletionBlock?,methodName:String?)->(){
        if error != nil {
            printErrorMessage(error, methodName: #function)
            DispatchQueue.main.async(execute: {
                completionBlock?(nil)
            })
        }else if response != nil {
            DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async(execute: {
                let responseDictionary = parsedJsonFrom(response as? Data,methodName: methodName!)
                if (isNotNull(responseDictionary) && responseDictionary is NSDictionary) {
                    if self.isSuccess(responseDictionary as! NSDictionary?){
                        DispatchQueue.main.async(execute: {
                            if self.returnJSONData {
                                let jsonResponse : NSString? = NSString(data: response! as! Data,encoding: String.Encoding.utf8.rawValue)
                                completionBlock?(jsonResponse)
                            }else{
                                completionBlock?(responseDictionary as? NSDictionary)
                            }
                        })
                    }
                    else if self.isFailure(responseDictionary as! NSDictionary?){
                        if self.returnFailureResponseAlso {
                            DispatchQueue.main.async(execute: {
                                completionBlock?(responseDictionary as? NSDictionary);
                            })
                        }else{
                            DispatchQueue.main.async(execute: {
                                completionBlock?(nil)
                            })
                        }
                    }
                    else {
                        if self.returnFailureResponseAlso {
                            DispatchQueue.main.async(execute: {
                                completionBlock?(responseDictionary as? NSDictionary)
                            })
                        }else{
                            DispatchQueue.main.async(execute: {
                                completionBlock?(nil)
                            })
                        }
                    }
                }
                else {
                    if self.returnFailureUnParsedDataIfParsingFails {
                        DispatchQueue.main.async(execute: {
                            completionBlock?(["failedParsingResponseReceived":String(data: response as! Data!,encoding: String.Encoding.utf8)!])
                        })
                    }else{
                        DispatchQueue.main.async(execute: {
                            completionBlock?(nil)
                        })
                    }
                }
            })
        }else {
            DispatchQueue.main.async(execute: {
                completionBlock?(nil)
            })
        }
    }
}

