//
//  WeeklyPlannerDayInfoTableViewCell.swift
//  Orahi
//
//  Created by Alok Singh on 26/05/17.
//  Skype           : alok.singh.confident
//  Phone/Whatsapp  : 8287757210
//  Email           : alok.singh.confident@gmail.com
//  Official Email  : alok.k.singh@orahi.com
//  Github          : https://github.com/aryansbtloe
//  LinkedIn        : https://in.linkedin.com/in/alok-kumar-singh-09141164
//  Facebook        : https://www.facebook.com/aryansbtloe
//  Stack OverFlow  : http://stackoverflow.com/users/911270/alok-singh
//  CocoaControls   : https://www.cocoacontrols.com/authors/aryansbtloe
//  Copyright (c) 2017 Orahi. All rights reserved.
//

import Foundation
import UIKit

class WeeklyPlannerDayInfoTableViewCell : UITableViewCell {
    var isInitialisedOnce = false
    @IBOutlet fileprivate weak var dayLabel : UILabel?
    
    @IBOutlet weak var containerView1 : UIView?
    @IBOutlet weak var rideStatusButton1 : TKTransitionSubmitButton?
    @IBOutlet weak var timeButton1 : UIButton?
    @IBOutlet weak var viewButton1 : UIButton?
    
    @IBOutlet weak var containerView2 : UIView?
    @IBOutlet weak var rideStatusButton2 : TKTransitionSubmitButton?
    @IBOutlet weak var timeButton2 : UIButton?
    @IBOutlet weak var viewButton2 : UIButton?
    
    var plan1 : NSDictionary?
    var plan2 : NSDictionary?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        startUpInitialisations()
        updateUserInterfaceOnScreen()
    }
    
    private func startUpInitialisations(){
        self.contentView.layoutIfNeeded()
        if isInitialisedOnce == false {
            self.selectionStyle = UITableViewCellSelectionStyle.none
            dayLabel?.numberOfLines = 2
            setBorder(containerView1!, color: UIColor.clear, width: 0, cornerRadius: 6)
            setBorder(containerView2!, color: UIColor.clear, width: 0, cornerRadius: 6)
            setBorder(timeButton1!.superview!, color: UIColor.clear, width: 0, cornerRadius: 6)
            setBorder(timeButton2!.superview!, color: UIColor.clear, width: 0, cornerRadius: 6)
        }
        setBorder(rideStatusButton1!, color: UIColor.clear, width: 0, cornerRadius: 6)
        setBorder(rideStatusButton2!, color: UIColor.clear, width: 0, cornerRadius: 6)
        isInitialisedOnce = true
    }
    
    func updateUserInterfaceOnScreen(){
        let attributesBig = [NSFontAttributeName:UIFont.init(name: FONT_SEMI_BOLD, size: 22)!,
                                 NSForegroundColorAttributeName:UIColor.darkGray] as NSDictionary
        let attributesSmall = [NSFontAttributeName:UIFont.init(name: FONT_REGULAR, size: 11)!,
                              NSForegroundColorAttributeName:UIColor.gray] as NSDictionary
        
        if isNotNull(plan1?.object(forKey: "day")){
            let day = (plan1!.object(forKey: "day") as! NSString).capitalized.substring(0, to: 2)
            let date = (plan1!.object(forKey: "date") as! String).dateValueType1()
            let dateToShow : String?
            if date.isToday(){
                dateToShow = "Today"
            }else{
                dateToShow = date.toStringValue(format:"dd MMM")
            }
            let infoToShow = "\(day)\n\(dateToShow!)"
            let attributedString = NSMutableAttributedString(string:infoToShow)
            attributedString.setAttributes(attributesBig as? [String : Any], range: NSMakeRange(0, 1))
            attributedString.setAttributes(attributesSmall as? [String : Any], range: NSMakeRange(1,infoToShow.length-1))
            dayLabel?.attributedText = attributedString
        }else{
            dayLabel?.attributedText = nil
        }
        
        Acf().decideAndSetUserRideStatusForWeeklyPlannerCell(plan1!, button: rideStatusButton1!,colorStatusView: containerView1!)
        if isNotNull(plan1!.object(forKey: "etd")){
            timeButton1?.setTitle(getTimeFormatInAmPm(plan1!.object(forKey: "etd") as? String), for: UIControlState.normal)
        }else{
            timeButton1?.setTitle("--:--", for: UIControlState.normal)
        }
        viewButton1?.setTitle("VIEW", for: UIControlState.normal)
        Acf().decideAndSetUserRideStatusForWeeklyPlannerCell(plan2!, button: rideStatusButton2!,colorStatusView: containerView2!)

        if isNotNull(plan2!.object(forKey: "etd")){
            timeButton2?.setTitle(getTimeFormatInAmPm(plan2!.object(forKey: "etd") as? String), for: UIControlState.normal)
        }else{
            timeButton2?.setTitle("--:--", for: UIControlState.normal)
        }
        viewButton2?.setTitle("VIEW", for: UIControlState.normal)
    }
}
