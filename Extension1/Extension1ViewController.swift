//
//  Extension1ViewController.swift
//  Extension1
//
//  Created by Orahi on 11/08/16.
//  Copyright © 2016 Orahi. All rights reserved.
//

import UIKit
import NotificationCenter

class Extension1ViewController: UIViewController, NCWidgetProviding,UITableViewDelegate,UITableViewDataSource {
    
    //MARK: - variables and constants
    @IBOutlet fileprivate weak var tableView : UITableView?
    @IBOutlet fileprivate weak var containerView : UIView?
    @IBOutlet fileprivate weak var infoLabel : UILabel?
    
    fileprivate var plans:NSMutableArray?

    //MARK: - view controller life cycle methods
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        startUpInitialisations()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    func widgetMarginInsets(forProposedMarginInsets defaultMarginInsets: UIEdgeInsets) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    //MARK: - other methods
    private func registerForNotifications(){
        let userDefaults = UserDefaults(suiteName: USER_DEFAULTS_EXTENSION_GROUP_ID)
        userDefaults!.addObserver(self, forKeyPath: KEY_WEEKLY_PLANNER, options: NSKeyValueObservingOptions.new, context: nil)
        userDefaults!.addObserver(self, forKeyPath: KEY_USERID, options: NSKeyValueObservingOptions.new, context: nil)
    }
    
    private func startUpInitialisations(){
        registerNib("WeeklyPlannerDayInfoTableViewCell", tableView: self.tableView)
        setAppearanceForTableView(tableView)
        self.tableView?.backgroundColor = UIColor.white
        self.tableView!.canCancelContentTouches = false
        self.containerView?.layer.cornerRadius = 6
        self.containerView?.layer.masksToBounds = true
        self.infoLabel?.layer.opacity = 1.0
        self.infoLabel?.isHidden = false
    }
    
    func loadPlannerData() {
        func prepareDataForDisplay(){
            var plansFromServer : NSMutableArray?
            if let jsonResponseCached = UserDefaults.standard.object(forKey: KEY_WEEKLY_PLANNER) as? String {
                if let responseDictionary = parsedJson(jsonResponseCached.data(using: String.Encoding.utf8)!,methodName: #function) as? NSDictionary {
                    if let cachedPlans = responseDictionary.object(forKey: "data") as? NSMutableArray {
                        plansFromServer = cachedPlans
                    }
                }
            }
            if plansFromServer == nil {
                plansFromServer = NSMutableArray()
            }
            let weeklyPlansTemplate = NSMutableArray()
            for i in 0...1 {
                let isMorning = (i%2 == 0)
                let date = Date().dateByAddingDays(Int(i/2))
                let plan = NSMutableDictionary()
                plan.setObject(date.weekdayToString(), forKey: "day" as NSCopying)
                plan.setObject(date.toStringValue(format: "yyyy-MM-dd"), forKey: "date" as NSCopying)
                for p in plansFromServer! {
                    let wpDate = plan.object(forKey: "date") as? String
                    let pDate = (p as AnyObject).object(forKey: "date") as? String
                    let pEtd = (p as AnyObject).object(forKey: "etd") as? String
                    let mode = (p as AnyObject).object(forKey: "mode") as! NSString
                    if isNotNull(pDate) && isNotNull(pEtd) && isNotNull(mode){
                        if isMorning{
                            if pDate!.isEqual(wpDate!) && mode.isEqual(to: "0"){
                                if mode.isEqual(to: "0"){
                                    plan.removeAllObjects()
                                    plan.addEntries(from: p as! [AnyHashable: Any])
                                    break
                                }
                            }
                        }else{
                            if pDate!.isEqual(wpDate!) && mode.isEqual(to: "1"){
                                plan.removeAllObjects()
                                plan.addEntries(from: p as! [AnyHashable: Any])
                                break
                            }
                        }
                    }
                }
                weeklyPlansTemplate.add(plan)
            }
            self.plans = weeklyPlansTemplate
            self.tableView?.reloadData()
        }
        prepareDataForDisplay()
        self.tableView?.reloadData()
    }
    
    func fetchWeeklyPlannerDataAndUpdate() {
        if isUserLoggedIn(){
            if self.plans != nil && self.plans!.count == 0 {
                self.view.showActivityIndicator()
            }
            let scm = ServerCommunicationManager()
            scm.returnJSONData = true
            scm.getWeeklyRidePlan([:]) {[weak self] (jsonResponseReceived) -> () in guard let `self` = self else { return }
                self.view.hideActivityIndicator()
                if jsonResponseReceived is String {
                    UserDefaults.standard.set(jsonResponseReceived, forKey: KEY_WEEKLY_PLANNER)
                    self.loadPlannerData()
                }
            }
        }else{
            UserDefaults.standard.removeObject(forKey: KEY_WEEKLY_PLANNER)
        }
        loadPlannerData()
    }
    
    @objc private func updateUserInterfaceOnScreen(){
        
    }
    
    //MARK: - UITableView Delegate & Data Source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isNotNull(plans){
            if self.infoLabel!.isHidden == false {
                UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.curveLinear, animations: {
                    self.infoLabel?.layer.opacity = 0.0
                }) { (completed) in
                    self.infoLabel?.isHidden = true
                }
            }
            return plans!.count/2
        }else{
            if self.infoLabel!.isHidden {
                UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.curveLinear, animations: {
                    self.infoLabel?.layer.opacity = 1.0
                }) { (completed) in
                    self.infoLabel?.isHidden = false
                }
            }
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 98
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WeeklyPlannerDayInfoTableViewCell") as? WeeklyPlannerDayInfoTableViewCell
        cell?.plan1 = plans?.object(at: 2*(indexPath as NSIndexPath).row) as? NSMutableDictionary
        cell?.plan2 = plans?.object(at: 2*(indexPath as NSIndexPath).row+1) as? NSMutableDictionary
        cell?.updateUserInterfaceOnScreen()
        cell?.rideStatusButton1?.tag = (2*(indexPath as NSIndexPath).row)
        cell?.rideStatusButton2?.tag = (2*(indexPath as NSIndexPath).row+1)
        cell?.viewButton1?.tag = (2*(indexPath as NSIndexPath).row)
        cell?.viewButton2?.tag = (2*(indexPath as NSIndexPath).row+1)
        cell?.rideStatusButton1?.addTarget(self, action: #selector(Extension1ViewController.onClickOfRideStatusButton), for: .touchUpInside)
        cell?.rideStatusButton2?.addTarget(self, action: #selector(Extension1ViewController.onClickOfRideStatusButton), for: .touchUpInside)
        cell?.viewButton1?.addTarget(self, action: #selector(Extension1ViewController.onClickOfViewDetailButton(_:)), for: .touchUpInside)
        cell?.viewButton2?.addTarget(self, action: #selector(Extension1ViewController.onClickOfViewDetailButton(_:)), for: .touchUpInside)
        return cell!
    }
    
    @objc func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func onClickOfViewDetailButton(_ sender:UIButton) {
        openContainerApplication()
    }
    
    func onClickOfRideStatusButton(_ sender:TKTransitionSubmitButton) {
        let plan = plans?.object(at: sender.tag) as! NSDictionary
        let situation = Acf().getTravelRequestStatus(plan)
        if situation == .notInitialisedYet ||
            situation == .travelRequestRejectedByMe {// action is fix my ride
            if(isInternetConnectivityAvailable()==false){return}
            let isPlannerSetForTheDay = isNotNull(plan.object(forKey: "mode")) && isNotNull(plan.object(forKey: "etd"))
            if isPlannerSetForTheDay {
                var via = "top"
                var trueForRequestToServerFalseForLetUserChoosePeoples = true
                let selectedDayDate = (plan.object(forKey: "date") as! String).dateValueType1()
                func performFixMyRideAction(){
                    let information = ["userId":loggedInUserId(),
                                       "date":selectedDayDate.toStringValue(format:"yyyy-MM-dd"),
                                       "mode":plan.object(forKey: "mode") as! String,
                                       "via":via
                        ] as NSDictionary
                    sender.normalBackgroundColor = APP_THEME_VOILET_COLOR
                    sender.startLoadingAnimation()
                    let scm = ServerCommunicationManager()
                    scm.setMyRide(information) {[weak self] (responseData) -> () in guard let `self` = self else { return }
                        sender.stopIt()
                        sender.normalBackgroundColor = UIColor.clear
                        self.fetchWeeklyPlannerDataAndUpdate()
                    }
                }
                performFixMyRideAction()
            }else{
                openContainerApplication()
            }
        }
    }
    
    func openContainerApplication() {
        extensionContext?.open(URL(string:"\(CONTAINER_APP_URL)://actionFrom:Extension1")!, completionHandler: nil)
    }
    
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        completionHandler(NCUpdateResult.newData)
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(Extension1ViewController.fetchWeeklyPlannerDataAndUpdate), object: nil)
        self.perform(#selector(Extension1ViewController.fetchWeeklyPlannerDataAndUpdate), with: nil, afterDelay: 3)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath != nil {
            if keyPath!.isEqual(KEY_WEEKLY_PLANNER){
                NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(Extension1ViewController.loadPlannerData), object: nil)
                self.perform(#selector(Extension1ViewController.loadPlannerData), with: nil, afterDelay: 3)
            }else if keyPath!.isEqual(KEY_USERID){
                NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(Extension1ViewController.fetchWeeklyPlannerDataAndUpdate), object: nil)
                self.perform(#selector(Extension1ViewController.fetchWeeklyPlannerDataAndUpdate), with: nil, afterDelay: 3)
            }
        }
    }
}
